/*
 * Common Methods
 **************************************************************************************************/

var currentUrl = "http://test.terminix.com";


function IsObject(a) {
	return Object.prototype.toString.call(a) == '[object Object]';
}


function IsArray(a) {
	return Object.prototype.toString.call(a) == '[object Array]';
}


function IsObjectEmpty(a) {
	for (var i in a) {
		return false;
	}
	
	return true;
}


function GetWindowWidth() {
	return window.innerWidth;
}


function ScrollTo(pos, speed) {
	$('html, body').animate({ scrollTop: pos }, speed);
}


/**
 * Returns whether a superset array contains all items of a subset array
 *
 * @param {Array} superset
 * @param {Array} subset
 * @return {Boolean}
 **/
function ArrayContainsArray(superset, subset) {
    if (!subset.length) return false;

    return subset.every(function(item) {
        return superset.indexOf(item) > -1;
    });
}


/**
 * Browser feature detection
 *
 * If more features are added, consider creating an object rather than several global variables.
 */
function IsStorageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
                // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
    }
}


var caniuse = (function(main) {
    main.history = window.history && typeof history.pushState == 'function' && typeof history.replaceState == 'function' && typeof history.back == 'function';

    main.sessionStorage = IsStorageAvailable('sessionStorage');
    main.localStorage = IsStorageAvailable('localStorage');

    return main;
})({});





/**
 * Gets a cookie value
 * 
 * @param {String} name    The cookie name
 * @return {String|null} The value of the cookie, or null if doesn't exist
 */
function GetCookie(name) {
    if (!name) {
        console.log('[GetCookie] Name is required to get the value of a cookie.');
        return null;
    }

    name += '=';

    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);

        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }

    return null;
}


/**
 * Sets a cookie
 * 
 * @param {String} name    The cookie name.
 * @param {String} value   The cookie value.
 * @param {String} days    The cookie expiration.
 *
 * @return {Boolean} Whether the creation/modification of a cookie succeeded or failed
 */
function SetCookie(name, value, days) {
    if (!name || !value) {
        console.log('[SetCookie] Name and value are both required to set a cookie.');
        return false;
    }

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

        var expires = '; expires=' + date.toUTCString();
    } else {
        var expires = '';
    }

    document.cookie = name + '=' + value + expires + '; path=/';

    return true;
}


/**
 * Opens the modal using bootstrap or materialize.
 * 
 * @param {String} modalId	The modal id
 */
function OpenModal(modalId) {
	'use strict';

	// Check the parameters
	if (!modalId || (typeof modalId !== 'string') || (modalId.indexOf('#') > -1)) return;
	
	if (_MATERIALIZE) {
		$('#' + modalId).modal('open');
	} else {
		$('#' + modalId).modal('show');
	}
}


/**
 * Close the modal using bootstrap or materialize.
 * 
 * @param {String} modalId	The modal id
 */
function CloseModal(modalId) {
	'use strict';

	// Check the parameters
	if (!modalId || (typeof modalId !== 'string') || (modalId.indexOf('#') > -1)) return;
	
	if (_MATERIALIZE) {
		$('#' + modalId).modal('close');
	} else {
		$('#' + modalId).modal('hide');
	}
}


/**
 * Returns whether the user is on iOS mobile device
 * 
 * @return {Boolean}
 */
function is_iOS() {
	'use strict';

	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];
	
	while (iDevices.length) {
		if (navigator.platform === iDevices.pop()) return true;
	}
	
	return false;
}


/**
 * Returns a passed parameter from the url
 *
 * @return {String} The string of the parameter or null
 */
function GetURLParameter(param) {
	'use strict';

	if (!param) throw '[GetURLParameter] param is required to pull a parameter.';

	var value = null;

	var pageUrl = decodeURIComponent(window.location.search.substring(1)),
		urlVariables = pageUrl.split('&'),
        parameterName,
        i;

	for (i = 0; i < urlVariables.length; i++) {
		parameterName = urlVariables[i].split('=');

		if (parameterName[0] === param) {
			value = parameterName[1] === undefined ? null : parameterName[1];
			break;
		}
	}

	return value;
}


/**
 * Returns the target pest
 *
 * @return {String} The string of the target pest or null
 */
function GetTargetPest() {
	'use strict';

	var target = null;

	// Check the url for a target pest
	var param = GetURLParameter('targetPest');
	if (param) {
		target = param;

	// Check session storage for a target pest
	} else if (caniuse.sessionStorage && sessionStorage.getItem('targetPest')) {
        target = sessionStorage.getItem('targetPest').toUpperCase();
    }

    return target;
}


/**
 * Returns whether the email address is valid
 * 
 * @return {Boolean}
 */
function IsValidEmailAddress(email) {
	'use strict';
	email = email ? email : "";
	return /^\w+([\.%#$&*+_-]\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(email);
}


/**
 * Returns whether the ABA (bank) routing number is valid
 * 
 * @param number routing number (of type string from input field) to be validated
 * @return {Boolean}
 */
function IsValidRoutingNumber(routingNumber) {
	'use strict';
	if (!routingNumber || routingNumber.length !== 9 || !$.isNumeric(routingNumber)) {
		return false;
	}

	var checksumTotal = (3 * (parseInt(routingNumber.charAt(0), 10) + parseInt(routingNumber.charAt(3), 10) + parseInt(routingNumber.charAt(6), 10)))
					  + (7 * (parseInt(routingNumber.charAt(1), 10) + parseInt(routingNumber.charAt(4), 10) + parseInt(routingNumber.charAt(7), 10)))
					  + (1 * (parseInt(routingNumber.charAt(2), 10) + parseInt(routingNumber.charAt(5), 10) + parseInt(routingNumber.charAt(8), 10)));

	return checksumTotal % 10 === 0;
}


/**
 * Gets the regex pattern in the Global UI Settings for input field
 * 
 * @param type The pattern type
 * @returns {String} The pattern if found
 */
function GetGlobalUIPattern(type) {
	var pattern = '';
	if (type)
	{
		pattern = _GLOBAL_UI_SETTINGS ? _GLOBAL_UI_SETTINGS['inputRegexPattern'][type.toLowerCase()] : '';
	}
	return pattern;
}


/**
 * Fires a GA event
 * 
 * @param {String} category The category for GA event
 * @param {String} action The action for GA category
 * @param {String} label The label for GA actionx
 * @param {String} value The value for GA label
 */
function FireGAEvent(category, action, label, value) {
	label = label ? label : '';
	value = value ? value : '';

	if (!category) {
		console.log('[FireGAEvent] Event Category is required to fire a GA event.');
		return;
	}

	if (!action) {
		console.log('[FireGAEvent] Event Action is required to fire a GA event.');
		return;
	}
	
	
	console.log('[FireGAEvent] ' + category + ' :: ' + action + ' :: ' + label + ' :: ' + value);
	
	
	
	try {
		var linkData = {
			ga_event_category: category,
			ga_event_action: action,
			ga_event_label: label,
			ga_event_value: value
		};

		utag.link(linkData);
	} catch (e) {
		console.log(e);
	}
}


/**
 * Retrieves product content from Endeca
 *
 * @param {Object} data The product information to pull (productCode, templateCode, folder)
 * @param {Function} callback The function to call when done pulling content
 */
function GetEndecaContent(data, callback) {
    var HandleCallbackData = function(data) {
        callback(data);
    };

    if (!data) throw '[GetEndecaContent] Data is required.';
    if (!data.productCode || !data.templateCode || !data.folder) {
    	console.log('[GetEndecaContent] Product Code, Template Code, and folder are all required in data.');
        HandleCallbackData({});
        return;
	}
//    var url = 'http://test.terminix.com/includes/common/getEndecaContent.jsp?productCode=' + data.productCode + '&templateCode=' + data.templateCode + '&folder=' + data.folder;
   var url = currentUrl +  '/includes/common/getEndecaContent.jsp?productCode=' + data.productCode + '&templateCode=' + data.templateCode + '&folder=' + data.folder;

    $.ajax({
        type: 'GET',
        url: url,
        timeout: 2 * 60 * 10000,
        complete: function(res) {
            var callbackData;

            try {
                callbackData = JSON.parse(res.responseText);
            }
            catch (e) {
                callbackData = { error: "An error has occurred." };
                console.log("Error from server request: " + e.message);
            }

            var returnData = {
            	templateCode: data.templateCode,
				content: callbackData
			};

            HandleCallbackData(returnData);
        }
    });
}



/**
 * Common Bootstrap modal
 * 
 * @param {Object} options A list of options for the modal
 * @param {Function} confirmCallback Fired when the user confirms an action
 * @param {Function=} cancelCallback Fired when the user cancels an action
 */
function CommonModal(options, confirmCallback, cancelCallback) {
	var _confirmCallback = confirmCallback || null,
        _cancelCallback = cancelCallback || null,
		_title = options.title || '',
		_body = options.body || '',
		_confirm = options.confirm || false,
		_btnCancelText = options.btnCancelText || 'Cancel',
		_btnConfirmText = options.btnConfirmText || 'Ok';

	var	_modal = $('#commonModal'),
        _modalOptions = {},
		_btnConfirm = _modal.find('.cta.primary'),
		_btnCancel = _modal.find('.cta.secondary, button.close');

	if (!_modal) { throw '[CommonModal] No common modal id found on the page.'; }

	// Set modal properties
	_modal.find('.modal-title').text(_title);
	_modal.find('.modal-body').html(_body);
	_btnConfirm.text(_btnConfirmText);
	_modal.find('.cta.secondary').text(_btnCancelText);
    _modalOptions.show = true;


	// Is it a confirmation modal
	if (_confirm) {
        _modalOptions.backdrop = 'static';
		_btnConfirm.show();
	} else {
		_btnConfirm.hide();
	}


	// Fire when modal is shown
	_modal.on('show.bs.modal', function(e) {
		if (_confirm) {
			// Callback required for confirm popup
			if (!confirmCallback) { throw '[CommonModal] Callback method required for confirmation modal.'; }

			_btnConfirm.off().on('click', function(e) {
				return confirmCallback();
			});

            _btnCancel.off().on('click', function(e) {
            	if (typeof cancelCallback == 'function') {
                    return cancelCallback();
                }
            });
		}
	});


	// Fire when modal is closed
	_modal.on('hide.bs.modal', function(e) {
		_confirmCallback = _cancelCallback = null;
	});


	// Show the modal
	_modal.modal(_modalOptions);
}





/*
 * Materialize Methods
 **************************************************************************************************/
/**
 * Throws a toast message
 *
 * @param {String} message The message to throw
 **/
function ThrowToast(message) {
	return; // Set temporarily so we know it doesn't break anything else
	'use strict';

	// If we aren't on a Materialize page, return
	if (!_MATERIALIZE) return;

	// Check the parameter
	if (!message || (typeof message !== 'string')) throw '[ThrowToast] String parameter message is required.';

	Materialize.toast(message, 4500);
}


/*
 * Credit Card Methods
 **************************************************************************************************/
// Accepted credit card types and regular expressions
var _CreditCardTypes = [{
	name: 'Visa',
	regex: /^4/,
	value: 'visa'
}, {
	name: 'Master Card',
	regex: /^(5[1-5]|2(2(2[1-9][0-9]{2}|[3-9])|[3-6]|7([01]|20[0-9]{2})))/,
	value: 'masterCard'
}, {
	name: 'American Express',
	regex: /^3[47]/,
	value: 'americanExpress'
}, {
	name: 'Discover',
	regex: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5])|64[4-9]|65)/,
	value: 'discover'
}];


/**
 * Returns the credit card type based on the number
 *
 * @param {String} cardNumber The credit card number
 * @return {Object} An object with the credit card type or null
 **/
function GetCreditCardType(cardNumber) {
	'use strict';

	// Check the parameters
	if (!cardNumber || (typeof cardNumber !== 'string')) return null;


	// Loop through and check for a match
	for (var i = 0; i < _CreditCardTypes.length; i++)
	{
		if (_CreditCardTypes[i].regex.test(cardNumber))
		{
			var method = _CreditCardTypes[i];

			// Return an object with the card type
			return { name: method.name, value: method.value };
		}
	}

	return null;
}


/**
 * Validates a credit card number based on type and regex
 *
 * @param {String} cardNumber The credit card number
 * @param {String} cardType The tyoe of credit card
 **/
function ValidateCreditCardNumber(cardNumber, cardType) {
	'use strict';

	// Check the parameters
	if (!cardNumber || (typeof cardNumber !== 'string')) throw '[ValidateCreditCardNumber] String parameter cardNumber is required.';
	if (!cardType || (typeof cardType !== 'string')) throw '[ValidateCreditCardNumber] String parameter cardType is required.';


	// Test the credit card based on type
	for (var i = 0; i < _CreditCardTypes.length; i++)
	{
		if (_CreditCardTypes[i].value.toLowerCase() === cardType.toLowerCase())
		{
			if (_CreditCardTypes[i].regex.test(cardNumber))
				return true;
		}
	}

	return false;
}


/**
 * Escapes a string to be used in a regular expression
 * 
 * @param {String} str The string to be escaped
 * @return {String} The escaped string
 */
RegExp.escape = function(str) {
	return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
};


/**
 * Replaces all values of {search} with {replacement} in the string
 * 
 * @param {String} search The substring to be replaced
 * @param {String} replacement The substring to replace the search string
 * @return {String} The string with all replacements done
 */
String.prototype.replaceAll = function(search, replacement) {
	return this.replace(new RegExp(RegExp.escape(search), 'g'), replacement);
};