var _SCHEDULE_SLOTS = [];

$(document).ready(function () {

    /*	Global Navigation
    	=========================================*/
    window.nav = new(function () {
        'use strict';

        var self = this,
            $moreListItem = $('li.more'),
            $shrink1 = $('li.shrink-1'),
            $subNav1 = $('.subnav .shrink-1'),
            $shrink2 = $('li.shrink-2'),
            $subNav2 = $('.subnav .shrink-2'),
            $mobileNav = $('.header-container.mobile'),
            $desktopNav = $('.header-container.desktop');

        self.states = [0, 1, 2, 3];
        self.currentState = self.states[2];

        self.SetNav = function () {
            var windowWidth = GetWindowWidth();

            if (windowWidth < 992) {
                self.currentState = self.states[3];
            } else if (windowWidth < 1380) {
                self.currentState = self.states[2];
            } else if (windowWidth < 1575) {
                self.currentState = self.states[1];
            } else {
                self.currentState = self.states[0];
            }

            self.UpdateNav();
        };

        self.UpdateNav = function () {
            //console.log(self.currentState);
            switch (self.currentState) {
                case 0:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.hide();
                    $shrink1.show();
                    $subNav1.hide();

                    $shrink2.show();
                    $subNav2.hide();
                    break;

                case 1:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.show();
                    $shrink1.hide();
                    $subNav1.show();

                    $shrink2.show();
                    $subNav2.hide();
                    break;

                case 3:
                    $mobileNav.show();
                    $desktopNav.hide();
                    break;

                case 2:
                default:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.show();
                    $shrink1.hide();
                    $subNav1.show();

                    $shrink2.hide();
                    $subNav2.show();
                    break;
            }
        };

        return self;
    })();


    nav.SetNav();
    $(window).resize(nav.SetNav);


    $('.mobile .nav-icon').on('click', ToggleMobileNav);
    var $mobileNavItems = $('.mobile .mainnav > li');

    function ToggleMobileNav() {
        $('.mobile .nav-icon a').parents('.header-container.mobile').toggleClass('open');
    }

    $mobileNavItems.on('click', function (e) {
        var $this = $(this);

        if ($this.hasClass('active')) {
            $this.toggleClass('active');
        } else {
            $mobileNavItems.removeClass('active');
            $this.toggleClass('active');
        }
    });
    /*	=========================================
    	End Global Navigation */





    /*	Common Methods
    	=========================================*/
    $('.scroll-to-top').on('click', function (e) {
        ScrollTo(0, 500);
    });



    // Set the default phone number to display in header and footer
    var phoneNumberToDisplay = '1.877.837.6464';

    // Check for branch_phone_number_cookie toggle
    if (_BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE) {
        // Get the cookie branch_phone_number value
        var cookiePhoneNumber = GetCookie('branch_phone_number');
        phoneNumberToDisplay = cookiePhoneNumber ? cookiePhoneNumber : phoneNumberToDisplay;
    }

    // Set branch phone number cookie value in header and footer 
    $('.phone-number-container > span').text(phoneNumberToDisplay);
    $('.call-phone > a').text(phoneNumberToDisplay).attr('href', 'tel:' + phoneNumberToDisplay.replace(/\./g, ''));
    $('.phone-link > a').text(phoneNumberToDisplay).attr('href', 'tel:' + phoneNumberToDisplay.replace(/\./g, ''));


    // Toggle display of secondary content
    $('.more-less-toggle').on('click', function (e) {
        $('.more-less-content').collapse('toggle');
        $('.more-less-toggle').text(function (index, text) {
            return text === 'less' ? 'more' : 'less';
        });
    });

    // Dynamically update current year, particularly for copyright
    var currentYear = new Date().getFullYear();
    $('.current-year').text(currentYear);

    // Generate json-ld WebPage type for TMXCommonPage template
    var script = document.createElement('script');
    script.type = "application/ld+json";
    script.text = JSON.stringify({
        "@context": "http://schema.org/",
        "@type": "WebPage",
        "author": "http://test.terminix.com/",
        "datePublished": new Date().toISOString(),
        "headline": document.querySelector('.jsonld-webpage-headline').text,
        "publisher": "http://test.terminix.com/",
        "description": document.querySelector('.jsonld-webpage-description').getAttribute('content')
    });
    document.querySelector('head').appendChild(script);


    // For all funnel CTA, store in session storage to know where
    // the user came from before going to the funnel
    if (caniuse.sessionStorage) {
        var FUNNEL_INTEREST_AREAS_BY_URL = {
            "/additional-pest-solutions/attic-insulation/": "ATTIC INSULATION",
            "/additional-pest-solutions/attic-insulation/california/": "ATTIC INSULATION",
            "/bed-bug-control/": "BED BUGS",
            "/additional-pest-solutions/crawl-space-services/": "CRAWL SPACE",
            "/pest-control/": "PEST CONTROL",
            "/pest-control/ants/": "ANTS",
            "/pest-control/cockroaches/": "COCKROACHES",
            "/pest-control/crickets/": "CRICKETS",
            "/pest-control/fleas/": "FLEAS",
            "/pest-control/mosquitoes/": "MOSQUITOES",
            "/pest-control/mosquitoes/atsb/": "MOSQUITOES",
            "/pest-control/moths/": "CLOTHES MOTHS",
            "/pest-control/rodents/": "RODENTS",
            "/pest-control/scorpions/": "SCORPIONS",
            "/pest-control/silverfish/": "SILVERFISH",
            "/pest-control/spiders/": "SPIDERS",
            "/pest-control/ticks/": "TICKS",
            "/pest-control/wasps/": "PAPER WASPS",
            "/pest-control/wildlife/": "WILDLIFE",
            "/termite-control/": "TERMITES",
            "/additional-pest-solutions/": "OTHER SERVICES"
        };

        $('body').on('click', 'a[href^="/free-inspection"], a[href^="/buyonline"]', function (e) {
            var url = window.location.pathname;

            // Make sure there's a trailing slash since all URLs in the map have one
            if (url.length && url.charAt(url.length - 1) != '/') {
                url += "/";
            }

            // Convert to lower case for case insensitive matching
            url = url.toLowerCase();

            // Check if URL exists in the map of interest areas
            // If so, store in the session storage. Otherwise, clear out one that may already exist
            if (url in FUNNEL_INTEREST_AREAS_BY_URL) {
                sessionStorage.setItem("interestArea", FUNNEL_INTEREST_AREAS_BY_URL[url]);
            } else {
                sessionStorage.removeItem("interestArea");
            }
        });
    }
    /*	=========================================
    	End Common Methods */





    /*	Live Chat
    	=========================================*/
    // NOTE: At some point we need to move the FAB methods here from general.js
    $(document).on('ShowMobileChatLink', function (e) {
        ShowMobileChatLink();
    });

    function ShowMobileChatLink() {
        $('.header-container.mobile .chat-link').show();
    }
    // END FAB ITEMS



    // Generic chat link events and methods
    $(document).on('ShowGenericChatLink', function (e) {
        ShowGenericChatLink();
    });
    $(document).on('HideGenericChatLink', function (e) {
        HideGenericChatLink();
    });

    function ShowGenericChatLink() {
        $('.chat-link').show();
    }

    function HideGenericChatLink() {
        $('.chat-link').hide();
    }


    // Hide all chat-links by default
    $('.header-container .chat-link').hide();

    // Apply chat link event
    $(document).on('click', '.chat-link', function (e) {
        e.preventDefault();
        LC_API.open_chat_window();
    });
    /*	=========================================
    	End Live Chat */





    /*	Scheduler Events
    	=========================================*/

    var $schedulerContainer = $('.scheduler-container'),
        $schedulerSelect = $('.scheduler-select'),
        $timeSlot = $('.time-slot'),
        $representativeText = $('.representative-text'),
        $loadingContainer = $('.loading-container'),
        $noteText = $('.note-text'),
        $javaTimeSlot = $('.timeslothidden');

    $(document).on('SetNewSchedulerDates', function (e, slotArray) {
        // Empty the schedule slots
        _SCHEDULE_SLOTS = [];


        // If we got a slotArray from the scheduler, build the slots array
        if (slotArray) {
            _SCHEDULE_SLOTS = GetDatesObject(slotArray);
        }


        // If we have slots, build the blocks and show the appropriate sections
        if (_SCHEDULE_SLOTS != null && _SCHEDULE_SLOTS.length > 0) {
            BuildDateSlots(_SCHEDULE_SLOTS, function () {
                $schedulerContainer.show();
                $noteText.show();
                $loadingContainer.hide();
            });

            $(document).trigger('SetDefaultDateAndTime');

            // If we don't have slots, hide the scheduler blocks, show text to user, and select none
        } else {
            $schedulerContainer.hide();
            $representativeText.show();
            $noteText.show();
            $loadingContainer.hide();
            $('#scheduler-none').trigger('click');
        }
    });


    $('#scheduler-none').on('change', function (e) {
        var $this = $(this);

        if ($this.prop('checked')) {
            $schedulerSelect.hide();
            $representativeText.show();
        } else {
            $schedulerSelect.show();
            $representativeText.hide();
        }

        UpdateSelectedTimeSlot();
    });


    $(document).on('click', '.scheduler.dates > li > a', function (e) {
        e.preventDefault();

        var $this = $(this),
            $datesList = $('.scheduler.dates > li > a');

        // Select the date and build the time slots for the selected date
        if (!$this.hasClass('active')) {
            $datesList.removeClass('active');
            $this.addClass('active');

            BuildTimeSlots($this.data('date'), function () {
                $timeSlot.show();
            });
        }

        UpdateSelectedTimeSlot();
    });


    $(document).on('click', '.scheduler.times > li > a', function (e) {
        e.preventDefault();

        var $this = $(this),
            $timesList = $('.scheduler.times > li > a');

        // Update the time slot key
        if (!$this.hasClass('active')) {
            $timesList.removeClass('active');
            $this.addClass('active');

            UpdateSelectedTimeSlot();
        }
    });


    $(document).on('SetDefaultDateAndTime', function () {
        $('.scheduler.dates > li:first-child > a').trigger('click');
        $('.scheduler.times > li:first-child > a').trigger('click');
    });


    function UpdateSelectedTimeSlot() {
        var $noneSelector = $('#scheduler-none'),
            $dateSelector = $('.scheduler.dates > li > a.active'),
            $timeSelector = $('.scheduler.times > li > a.active'),
            slotKey = '';

        // Get the slot key if available
        if ($noneSelector.prop('checked')) {
            slotKey = 'None';
        } else if ($dateSelector.length && $timeSelector.length) {
            slotKey = $timeSelector.data('slotkey');
        }

        $javaTimeSlot.val(slotKey);
    }


    // Loops through the slotArray and returns a unique date/time array
    function GetDatesObject(slotArray) {
        var availableDates = new Array(0);
        for (var i = 0; i < slotArray.length; i++) {
            var obj = null,
                loopItem = slotArray[i],
                foundDate = false,
                foundTime = false,
                foundTimeIndex = -1;

            for (var j = 0; j < availableDates.length; j++) {
                if (loopItem.schDate === availableDates[j].dateString) {
                    obj = availableDates[j];
                    foundDate = true;

                    for (var k = 0; k < obj.times.length; k++) {
                        if (loopItem.schFrmTime === obj.times[k].timeStart) {
                            foundTime = true;
                            foundTimeIndex = k;
                            break;
                        }
                    }

                    break;
                }
            }


            // Initialize our object of dates
            if (obj === null) {
                obj = {
                    dateString: loopItem.schDate,
                    times: []
                };
            }


            // It's a new time
            if (!foundTime) {
                obj.times.push({
                    timeStart: loopItem.schFrmTime,
                    timeStop: loopItem.schToTime,
                    data: loopItem
                });
            } else {
                // Use the lowest drive time slot
                if (parseInt(loopItem.schDriveTime) < parseInt(obj.times[foundTimeIndex].data.schDriveTime)) {
                    obj.times[foundTimeIndex].data = loopItem;
                }
            }


            // It's a new date, add it to available dates
            if (!foundDate) {
                availableDates.push(obj);
            }
        }


        // Sort the times
        for (var s = 0; s < availableDates.length; s++) {
            availableDates[s].times.sort(function (a, b) {
                var a = parseInt(a.timeStart.toString().substring(0, 2)),
                    b = parseInt(b.timeStart.toString().substring(0, 2));

                return a - b;
            });
        }

        return availableDates;
    }


    // Gets a date object by dateString
    function GetDateObj(date) {
        var obj = null;

        for (var i = 0; i < _SCHEDULE_SLOTS.length; i++) {
            if (_SCHEDULE_SLOTS[i].dateString === date) {
                obj = _SCHEDULE_SLOTS[i];
            }
        }

        return obj;
    }


    // Builds the blocks for dates
    function BuildDateSlots(dates, callback) {
        var $schedulerDatesList = $('ul.scheduler.dates'),
            monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        for (var i = 0; i < dates.length; i++) {
            var date = new Date(dates[i].dateString);
            var item = '<li><a href="#" class="schedule-date" data-date="' + dates[i].dateString + '"><span class="month">' + monthNames[date.getMonth()] + '</span><span class="date">' + date.getDate() + '</span></a></li>';
            $schedulerDatesList.append(item);
        }

        callback();
    }


    // Builds the blocks for times based on selected/passed date
    function BuildTimeSlots(date, callback) {
        var $schedulerTimesList = $('ul.scheduler.times'),
            dateObj = GetDateObj(date);

        $schedulerTimesList.empty();

        for (var i = 0; i < dateObj.times.length; i++) {
            var start = Get12HourAMPMTime(dateObj.times[i].timeStart.toString().substring(0, 2)),
                stop = Get12HourAMPMTime(dateObj.times[i].timeStop.toString().substring(0, 2));

            var item = '<li><a href="#" class="schedule-time" data-slotKey="' + dateObj.times[i].data.schSlotKey + '"><span>' + start + ' - ' + stop + '</span></a></li>';
            $schedulerTimesList.append(item);
        }

        callback();
    }


    // Gets a 12-hour time
    function Get12HourAMPMTime(timeString) {
        var hours = timeString,
            amPm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12;

        return hours + ' ' + amPm;
    }
    /*	=========================================
    	End New Scheduler */
});





$(window).on('load', function () {
    $('.AB_EVENT').each(function () {
        ga('create', utag.sender[15].data.account[0], 'auto');
        ga('send', 'event', '_setABTest', 'sucessfullySet', {
            'dimension2': $(this).attr('id')
        });
    });
});
