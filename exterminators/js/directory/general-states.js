<script type="text/javascript">
        $(document).ready(function() {

            var states = {
                "AL": {
                    "url": "al",
                    "fill": "#D1D3D4"
                },
                "AZ": {
                    "url": "az",
                    "fill": "#D1D3D4"
                },
                "AR": {
                    "url": "ar",
                    "fill": "#D1D3D4"
                },
                "CA": {
                    "url": "ca",
                    "fill": "#D1D3D4"
                },
                "CO": {
                    "url": "co",
                    "fill": "#D1D3D4"
                },
                "CT": {
                    "url": "ct",
                    "fill": "#D1D3D4"
                },
                "DE": {
                    "url": "de",
                    "fill": "#D1D3D4"
                },
                "FL": {
                    "url": "fl",
                    "fill": "#D1D3D4"
                },
                "GA": {
                    "url": "ga",
                    "fill": "#D1D3D4"
                },
                "HI": {
                    "url": "hi",
                    "fill": "#D1D3D4"
                },
                "ID": {
                    "url": "id",
                    "fill": "#D1D3D4"
                },
                "IL": {
                    "url": "il",
                    "fill": "#D1D3D4"
                },
                "IN": {
                    "url": "in",
                    "fill": "#D1D3D4"
                },
                "IA": {
                    "url": "ia",
                    "fill": "#D1D3D4"
                },
                "KS": {
                    "url": "ks",
                    "fill": "#D1D3D4"
                },
                "KY": {
                    "url": "ky",
                    "fill": "#D1D3D4"
                },
                "LA": {
                    "url": "la",
                    "fill": "#D1D3D4"
                },
                "ME": {
                    "url": "me",
                    "fill": "#D1D3D4"
                },
                "MD": {
                    "url": "md",
                    "fill": "#D1D3D4"
                },
                "MA": {
                    "url": "ma",
                    "fill": "#D1D3D4"
                },
                "MI": {
                    "url": "mi",
                    "fill": "#D1D3D4"
                },
                "MN": {
                    "url": "mn",
                    "fill": "#D1D3D4"
                },
                "MS": {
                    "url": "ms",
                    "fill": "#D1D3D4"
                },
                "MO": {
                    "url": "mo",
                    "fill": "#D1D3D4"
                },
                "NE": {
                    "url": "ne",
                    "fill": "#D1D3D4"
                },
                "NV": {
                    "url": "nv",
                    "fill": "#D1D3D4"
                },
                "NH": {
                    "url": "nh",
                    "fill": "#D1D3D4"
                },
                "NJ": {
                    "url": "nj",
                    "fill": "#D1D3D4"
                },
                "NM": {
                    "url": "nm",
                    "fill": "#D1D3D4"
                },
                "NY": {
                    "url": "ny",
                    "fill": "#D1D3D4"
                },
                "NC": {
                    "url": "nc",
                    "fill": "#D1D3D4"
                },
                "OH": {
                    "url": "oh",
                    "fill": "#D1D3D4"
                },
                "OK": {
                    "url": "ok",
                    "fill": "#D1D3D4"
                },
                "OR": {
                    "url": "or",
                    "fill": "#D1D3D4"
                },
                "PA": {
                    "url": "pa",
                    "fill": "#D1D3D4"
                },
                "RI": {
                    "url": "ri",
                    "fill": "#D1D3D4"
                },
                "SC": {
                    "url": "sc",
                    "fill": "#D1D3D4"
                },
                "TN": {
                    "url": "tn",
                    "fill": "#D1D3D4"
                },
                "TX": {
                    "url": "tx",
                    "fill": "#D1D3D4"
                },
                "UT": {
                    "url": "ut",
                    "fill": "#D1D3D4"
                },
                "VA": {
                    "url": "va",
                    "fill": "#D1D3D4"
                },
                "WA": {
                    "url": "wa",
                    "fill": "#D1D3D4"
                },
                "WV": {
                    "url": "wv",
                    "fill": "#D1D3D4"
                },
                "WI": {
                    "url": "wi",
                    "fill": "#D1D3D4"
                },
                "WY": {
                    "url": "wy",
                    "fill": "#D1D3D4"
                },
            };

            var statesHoverStyles = {
                "AL": {
                    "fill": "#0C8F43"
                },
                "AZ": {
                    "fill": "#0C8F43"
                },
                "AR": {
                    "fill": "#0C8F43"
                },
                "CA": {
                    "fill": "#0C8F43"
                },
                "CO": {
                    "fill": "#0C8F43"
                },
                "CT": {
                    "fill": "#0C8F43"
                },
                "DE": {
                    "fill": "#0C8F43"
                },
                "FL": {
                    "fill": "#0C8F43"
                },
                "GA": {
                    "fill": "#0C8F43"
                },
                "HI": {
                    "fill": "#0C8F43"
                },
                "ID": {
                    "fill": "#0C8F43"
                },
                "IL": {
                    "fill": "#0C8F43"
                },
                "IN": {
                    "fill": "#0C8F43"
                },
                "IA": {
                    "fill": "#0C8F43"
                },
                "KS": {
                    "fill": "#0C8F43"
                },
                "KY": {
                    "fill": "#0C8F43"
                },
                "LA": {
                    "fill": "#0C8F43"
                },
                "ME": {
                    "fill": "#0C8F43"
                },
                "MD": {
                    "fill": "#0C8F43"
                },
                "MA": {
                    "fill": "#0C8F43"
                },
                "MI": {
                    "fill": "#0C8F43"
                },
                "MN": {
                    "fill": "#0C8F43"
                },
                "MS": {
                    "fill": "#0C8F43"
                },
                "MO": {
                    "fill": "#0C8F43"
                },
                "NE": {
                    "fill": "#0C8F43"
                },
                "NV": {
                    "fill": "#0C8F43"
                },
                "NH": {
                    "fill": "#0C8F43"
                },
                "NJ": {
                    "fill": "#0C8F43"
                },
                "NM": {
                    "fill": "#0C8F43"
                },
                "NY": {
                    "fill": "#0C8F43"
                },
                "NC": {
                    "fill": "#0C8F43"
                },
                "OH": {
                    "fill": "#0C8F43"
                },
                "OK": {
                    "fill": "#0C8F43"
                },
                "OR": {
                    "fill": "#0C8F43"
                },
                "PA": {
                    "fill": "#0C8F43"
                },
                "RI": {
                    "fill": "#0C8F43"
                },
                "SC": {
                    "fill": "#0C8F43"
                },
                "TN": {
                    "fill": "#0C8F43"
                },
                "TX": {
                    "fill": "#0C8F43"
                },
                "UT": {
                    "fill": "#0C8F43"
                },
                "VA": {
                    "fill": "#0C8F43"
                },
                "WA": {
                    "fill": "#0C8F43"
                },
                "WV": {
                    "fill": "#0C8F43"
                },
                "WI": {
                    "fill": "#0C8F43"
                },
                "WY": {
                    "fill": "#0C8F43"
                },
            };

            $('#mappie').usmap({
                showLabels: false,
                stateStyles: {
                    fill: '#eee',
                    stroke: '#fff',
                    'stroke-width': 0
                },
                stateHoverStyles: {
                    fill: '#eee'
                },
                stateSpecificStyles: states,
                stateSpecificHoverStyles: statesHoverStyles
            });
            $('#mappie').on('usmapclick', function(event, data) {
                if (states[data.name] != undefined) {
                    window.location = "" + states[data.name].url;
                }
            });
        });
    </script>
    