$("#menu-toggle, .menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled")
});
if ($("#user-city").text().length > 0) $(".add-city").append(" IN " + $("#user-city").text().toUpperCase() + ", " + $("#user-state").text().toUpperCase());
$(".expand-icon").click(function (e) {
    e.preventDefault();
    var rt = $(this).offset().left + $(this).outerWidth();
    if (e.clientX > rt - 80 || $(this).hasClass("collapsed")) {
        $("#" + $(this).attr("data-target")).collapse("toggle");
        return false
    } else {
        e.stopPropagation();
        location.href = $(this).attr("href")
    }
});
$(".main , #page-wrap").click(function (e) {
    if ($("#wrapper").hasClass("toggled")) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled")
    }
});
$(".print").click(function () {
    window.print()
});
$("ul.highlight a").click(function () {
    $(".highlighted").removeClass("highlighted");
    $(this).addClass("highlighted")
});
$("a").mousedown(function (event) {
    if (event.which == 2) $(this).attr("target", "_blank");
    return true
});
$("#collapse-nav > li > a").each(function () {
    var pathname_root = $(this).attr("href").split("/")[1];
    if (window.location.href.indexOf(pathname_root) > -1) {
        $(this).addClass("highlighted-nav-bar");
        $("#" + pathname_root).addClass("orange")
    }
});
$("#pest-submit-button").click(function () {
    if ($("#pest-submit-button #zipCode").val() == "") procEvent("Home Page Info Form", $("#pest-submit-button #pestSelect").val(), "Zip Code Empty");
    else if ($("#pest-submit-button #pestSelect").val() == "") procEvent("Home Page Info Form", $("#pest-submit-button #zipCode").val(), "Pest Select Empty");
    else procEvent("Home Page Info Form", $("#pest-submit-button #pestSelect").val(), "No Error")
});
$(".pest-submit-button").click(function () {
    if ($(".pest-submit-button #zipCode").val() == "") procEvent("Stick Info Form", $(".pest-submit-button #pestSelect").val(), "Zip Code Empty");
    else if ($(".pest-submit-button #pestSelect").val() == "") procEvent("Stick Info Form", $(".pest-submit-button #zipCode").val(), "Pest Select Empty");
    else procEvent("Stick Info Form", $(".pest-submit-button #pestSelect").val(), "Submitted")
});

function populateCityAndState(zip) {
    if (!_ENABLE_CITY_STATE_TOGGLE) {
        if (!isNaN(zip) && zip.length == 5)
            if (_ENABLE_SMARTY_ZIP_TOGGLE) smartyStreetsGetCityAndState(zip);
            else ziptasticGetCityAndState(zip)
    } else showCityAndState()
}

function ziptasticGetCityAndState(zip) {
    var zipRequest = $.getJSON("https://zip.getziptastic.com/v2/US/" + zip);
    zipRequest.done(function (data) {
        $("#city_hidden, .city_hidden").val(data.city);
        $("#state_hidden, .state_hidden").val(data.state_short)
    });
    zipRequest.fail(function () {
        showCityAndState()
    })
}

function smartyStreetsGetCityAndState(zip) {
    var zipRequest = $.getJSON("/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=" + zip);
    zipRequest.done(function (result) {
        if (result.data.validResponse == "true") {
            $("#city_hidden, .city_hidden").val(result.data.city);
            $("#state_hidden, .state_hidden").val(result.data.state)
        } else showCityAndState()
    });
    zipRequest.fail(function () {
        showCityAndState()
    })
}

function showCityAndState() {
    $("#city_hidden[required], .city_hidden[required]").parents(".hiddenInputs").removeClass("hidden hide");
    $("#state_hidden[required], .state_hidden[required]").parents(".hiddenInputs").removeClass("hidden hide");
    $(".city-state-block").show();
    $(".city-state-block #city_hidden").focus()
}

function removeLightBox(lightbox) {
    lightbox.addClass("hidden");
    lightbox.addClass("dismissed")
}

function showLightBox(lightbox) {
    if (!lightbox.hasClass("dismissed")) {
        lightbox.removeClass("hide");
        lightbox.removeClass("hidden")
    }
}

function scrollToLegalFooter() {
    $("html, body").animate({
        scrollTop: $(document).height()
    }, "slow")
}
$(document).ready(function () {
    var idleUser = new Date;
    $(document).click(function (e) {
        idleUser = new Date
    });
    setInterval(function () {
        var now = new Date;
        var diff = (now - idleUser) / 1E3 / 60;
        if (diff > 1)
            if (document.location.pathname.indexOf("/buyonline/") == 0) {
                $("#clickToCallBox").hide();
                $("#clickToCallBoxMat").hide()
            } else if ($("#materialD").length) showLightBox($("#clickToCallBoxMat"));
        else showLightBox($("#clickToCallBox"))
    }, 6E4);
    $(window).scroll(function () {
        var hideStickForm = true;
        if ($(document).width() >= 975) {
            if ($(window).scrollTop() >=
                350) hideStickForm = false
        } else if ($(document).width() >= 500) {
            if ($(window).scrollTop() >= 500) hideStickForm = false
        } else if ($(window).scrollTop() >= 600) hideStickForm = false;
        if (hideStickForm || is_iOS()) $("#stick-form").addClass("hidden");
        else $("#stick-form").removeClass("hidden")
    });
    $("#zipCodeStick, .pestSelect").focus(function () {
        $(this).addClass("iphoneInputfix")
    });
    $("#zipCodeStick, .pestSelect").blur(function () {
        $(this).removeClass("iphoneInputfix")
    });
    $(window).trigger("scroll");
    if (_FAB_FEATURE_TOGGLE) {
        var HideOfficialLiveChatBubble =
            function () {
                $("#livechat-compact-container").hide()
            };
        var HideFABChatButton = function () {
            $("li.chat-bubble").hide()
        };
        var ShowFABChatButton = function () {
            $("li.chat-bubble").show()
        };
        $(document).on("InitialGreetingInitiated", function (e) {
            HideOfficialLiveChatBubble();
            var $FAB = $(".fixed-action-btn.click-to-toggle"),
                $chatBubble = $("li.chat-bubble"),
                greeting = "Hello, how can we help you?";
            $chatBubble.children("span.mobile-fab-tip").html(greeting);
            $chatBubble.children("a").prop("data-tooltip", greeting);
            if (!$FAB.hasClass("active")) $FAB.children("a").trigger("click.fabClickToggle")
        });
        $(document).on("ShowFABChatButton", function (e) {
            ShowFABChatButton()
        });
        $(document).on("HideFABChatButton", function (e) {
            HideFABChatButton()
        });
        $(document).on("HideOfficialLiveChatBubble", function (e) {
            HideOfficialLiveChatBubble()
        });
        $(document).on("click", ".fixed-action-btn.click-to-toggle ul a", function (e) {
            $(".fixed-action-btn.click-to-toggle > a").trigger("click.fabClickToggle");
            var $this = $(this);
            var $button = $this.parent();
            if ($button.hasClass("call-bubble")) procEvent("FAB Trigger", "click to call");
            else if ($button.hasClass("chat-bubble")) procEvent("FAB Trigger",
                "click to chat")
        });
        $(document).on("click", "li.chat-bubble > a", function (e) {
            e.preventDefault();
            parent.LC_API.open_chat_window({
                source: "minimized"
            })
        });
        $(document).on("click", ".fixed-action-btn.click-to-toggle > a", function (e) {
            var $this = $(this);
            var $menu = $this.parent();
            if ($menu.hasClass("active")) procEvent("FAB Trigger", "FAB was opened")
        })
    }
});
$(window).on("load", function () {
    $(window).trigger("scroll")
});
