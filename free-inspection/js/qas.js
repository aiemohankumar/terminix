var QAS_Variables = {
    PROXY_PATH: "https://www.terminix.com/includes/qas_atg_proxy.jsp",
    ADD_PROXY_PATH: "https://www.terminix.com/includes/add_atg_proxy.jsp",
    TD_PROXY_PATH: "https://www.terminix.com/includes/email_atg_proxy.jsp",
    QAS_LAYOUT: "ATG",
    PRE_ON_CLICK: null,
    POST_ON_CLICK: function () {
        handleGetFreeQuote()
    },
    BUTTON_ID: "",
    ADDRESS_FIELD_IDS: [["address1", "address2", "city_hidden", "state_hidden", "zipCode"]],
    modifyIncomingValueRules: {},
    COUNTRY_FIELD_IDS: ["false"],
    DATA_SETS: ["USA", "CAN"],
    ADD_DATA_SETS: ["BRA", "CHN", "HKG", "IND", "IDN", "ITA", "JPN", "KOR", "MEX", "RUS", "ESP", "UKR", "VNM"],
    DEFAULT_DATA: "USA",
    COUNTRY_MAP: [["US", "USA"], ["U.S.", "USA"], ["U.S.A.", "USA"], ["United States", "USA"], ["United States of America", "USA"], ["Canada", "CAN"], ["CA", "CAN"], ["China", "CHN"], ["Brazil", "BRA"], ["Hong Kong", "HKG"], ["India", "IND"], ["Indonesia", "IDN"], ["Italy", "ITA"], ["Japan", "JPN"], ["Korea", "KOR"], ["Mexico", "MEX"]],
    PHONE_VALIDATE_COUNTRY: ["AIA", "ATG", "BHS", "BRB", "BMU", "CAN", "CYM", "DMA", "DOM", "GRD", "JAM", "MSR", "KNA", "LCA", "TCA", "TTA", "USA", "VCT", "VGB", "VIR"],
    LVR: 7,
    EMAIL_FIELD_IDS: ["qas_filler"],
    EMAIL_ERR_FIELD_IDS: ["email_error"],
    PHONE_FIELD_IDS: ["qas_filler"],
    PHONE_ERR_FIELD_IDS: ["phone_error"],
    DISPLAY_CUSTOM_EMAIL_ERR: false,
    DISPLAY_CUSTOM_PHONE_ERR: false,
    EMAIL_VAL_LEVEL: "2",
    EMAIL_PHONE_NUM_SUBMITS: 2,
    ADDRESS_INTERACTION: true,
    EMAIL_PHONE_INTERACTION: true,
    EMAIL_PHONE_USEDIALOG: true,
    DISPLAY_ERRORS: false || window.location.search.indexOf("debug=1") > -1,
    TIMEOUT: 6E3,
    TIMEOUT_EMAILPHONE: 6E3,
    DISPLAY_LINES: 5
};
var QAS_PROMPTS = {
    "InteractionRequired": {
        "header": "<b>We think that your address may be incorrect or incomplete.</b><br />To proceed, please choose one of the options below.",
        "prompt": "We recommend:",
        "button": "Use suggested address"
    },
    "PremisesPartial": {
        "header": "<b>Sorry, we think your apartment/suite/unit is missing or wrong</b><br />To proceed, please enter your apartment/suite/unit or use your address as entered",
        "prompt": "Confirm your Apartment/Suite/Unit number:",
        "button": "Confirm number",
        "showPicklist": "Show all potential matches",
        "invalidRange": "Secondary information not within valid range"
    },
    "StreetPartial": {
        "header": "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
        "prompt": "Confirm your House/Building number:",
        "button": "Confirm number",
        "showPicklist": "Show all potential matches",
        "invalidRange": "Primary information not within valid range"
    },
    "DPVPartial": {
        "header": "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
        "prompt": "Confirm your House/Building number:",
        "button": "Confirm number"
    },
    "AptAppend": {
        "header": "<b>Sorry, we think your apartment/suite/unit may be missing.</b><br />To proceed, please check and choose from one of the options below.",
        "prompt": "Confirm Apt/Ste:",
        "button": "Continue",
        "noApt": "I do not have an apt or suite"
    },
    "Multiple": {
        "header": "<b>We found more than one match for your address.</b><br />To proceed, please choose one of the options below.",
        "prompt": "Our suggested matches:"
    },
    "None": {
        "header": "<b>Sorry, we could not find a match for your address.</b><br />To proceed, please choose one of the options below."
    },
    "RightSide": {
        "prompt": "You Entered:",
        "edit": "Edit",
        "button": "Use Address As Entered",
        "warning": "<b>*Your address may be undeliverable</b>"
    },
    "ConfirmEmailPhone": {
        "header": "<b>Sorry we could not confirm your e-mail address and phone number</b><br />To proceed, please confirm your e-mail address and phone number below.",
        "headerPhone": "<b>Sorry we could not confirm your phone number</b><br />To proceed, please confirm your phone number below.",
        "headerEmail": "<b>Sorry we could not confirm your e-mail address</b><br />To proceed, please confirm your e-mail address below.",
        "promptEmail": "Confirm or edit your e-mail address",
        "promptPhone": "Confirm or edit your phone number"
    },
    "waitMessage": "Please wait, your details are being verified",
    "title": "VERIFY YOUR ADDRESS DETAILS",
    "emailphoneTitle": "Verify your contact details"
};
var EMAIL_ERR_MESSAGES = {
    5: "Validation Timeout",
    10: "Syntax OK",
    20: "Syntax OK and domain valid according to the domain database",
    30: "Syntax OK and domain exists",
    40: "Syntax OK, domain exists, and domain can receive email",
    50: "Syntax OK, domain exists, and mailbox does not reject mail",
    100: "Email has a general syntax error",
    110: "Email has an invalid character",
    115: "Email domain syntax is invalid",
    120: "Email username syntax is invalid",
    125: "Username syntax is invalid for that domain",
    130: "Email is too long",
    135: "Incorrect parentheses, brackets, or quotes",
    140: "Email does not have a username",
    145: "Email does not have a domain",
    150: "Email does not have an @ sign",
    155: "Email has more than one @ sign",
    200: "Email has an invalid top-level-domain",
    205: "Email cannot have an IP address as domain",
    210: "Email address contains space or extra text",
    215: "Email has unquoted spaces",
    310: "Email domain is invalid",
    315: "Email domain IP address is not valid",
    325: "Email domain cannot receive email",
    400: "Email username is invalid or nonexistent",
    410: "Email mailbox is full",
    420: "Email is not accepted for this domain",
    500: "Email username is not permitted",
    505: "Emails domain is not permitted",
    510: "Email is suppressed and not permitted"
};
var PHONE_ERR_MESSAGES = {
    5: "Validation Timeout",
    10: "Successfully Parsed and Standardized, Area Code and Exchange Match",
    100: "Area code contains invalid exchange digits",
    110: "Invalid area code and exchange",
    120: "Phone number has too few digits",
    130: "Phone number has too many digits",
    133: "Phone exchange and number not allowed",
    134: "Phone number exchange not allowed",
    135: "Phone number not allowed",
    140: "Extension greater than 5 digits",
    150: "Toll free number was entered",
    160: "900 numbers was entered"
};
var QAS_TEMP_VARS = {
    NUM_EMAIL_PHONE_SUBMITS: 0,
    EMAIL_PHONE_POS: 0
};

function Address() {
    var ids = QAS_Variables.ADDRESS_FIELD_IDS;
    var cIds = QAS_Variables.COUNTRY_FIELD_IDS;
    var addresses = [];
    var uniqueAddresses = [];
    var uniqueTracker = new Array(ids.length);
    var searchStrings = [];
    var searchCountries = [];
    var cleanedAddresses = [];
    var cList = QAS_Variables.DATA_SETS;
    var addCList = QAS_Variables.ADD_DATA_SETS;
    var i, j, cIndex;
    var getAddresses = function () {
        for (i = 0; i < ids.length; i++) {
            var tempAddress = [];
            for (j = 0; j < ids[i].length; j++) {
                var incomingValue = $("#" + ids[i][j]).val();
                var fieldValue = encodeURIComponent(incomingValue);
                if (fieldValue === undefined) {
                    if (QAS_Variables.DISPLAY_ERRORS) alert("ID '" + ids[i][j] + "' is undefined")
                } else fieldValue = fieldValue.replace(/^\s+|\s+$/g, "");
                tempAddress.push(fieldValue)
            }
            var c3 = $("#" + cIds[i]).val();
            if (c3 === "" || c3 === undefined) c3 = QAS_Variables.DEFAULT_DATA;
            for (cIndex = 0; cIndex < QAS_Variables.COUNTRY_MAP.length; cIndex++)
                if (c3.toLowerCase() === QAS_Variables.COUNTRY_MAP[cIndex][0].toString().toLowerCase()) c3 = QAS_Variables.COUNTRY_MAP[cIndex][1].toString();
            tempAddress.push(c3);
            addresses.push(tempAddress)
        }
    };
    var getUnique = function () {
        var isUnique = true;
        var j = 0;
        for (i = 0; i < addresses.length; i++) {
            uniqueTracker[i] = uniqueAddresses.length;
            isUnique = true;
            j = 0;
            while (isUnique && j < uniqueAddresses.length) {
                if (addresses[i].toString().toLowerCase() === uniqueAddresses[j].toString().toLowerCase()) {
                    isUnique = false;
                    uniqueTracker[i] = j
                }
                j++
            }
            if (isUnique) uniqueAddresses.push(addresses[i])
        }
    };
    var cleanCheck = function (address, country) {
        var addNotEmpty = false;
        var j = 0,
            k = 0;
        while (j < address.length) {
            if (address[j] !== "") addNotEmpty = true;
            if (address[j] ===
                undefined) return false;
            j++
        }
        if (addNotEmpty) {
            for (k = 0; k < cList.length; k++)
                if (country === cList[k]) return true;
            for (k = 0; k < addCList.length; k++)
                if (country === addCList[k]) return true
        }
        return false
    };
    var buildSearchStrings = function () {
        for (i = 0; i < uniqueAddresses.length; i++) {
            searchCountries.push(uniqueAddresses[i].pop());
            if (cleanCheck(uniqueAddresses[i], searchCountries[i])) searchStrings.push(uniqueAddresses[i].join("|"));
            else searchStrings.push(false)
        }
    };
    var returnAddresses = function () {
        for (i = 0; i < ids.length; i++)
            if (cleanedAddresses[uniqueTracker[i]] !==
                undefined)
                for (j = 0; j < ids[i].length; j++)
                    if (QAS_Variables.modifyIncomingValueRules[ids[i][j]]) {
                        var pcode = decodeURIComponent(cleanedAddresses[uniqueTracker[i]][j]);
                        $("#" + ids[i][j]).val(QAS_Variables.modifyIncomingValueRules[ids[i][j]](pcode))
                    } else $("#" + ids[i][j]).val(decodeURIComponent(cleanedAddresses[uniqueTracker[i]][j]))
    };
    this.getSearchStrings = function () {
        return searchStrings
    };
    this.getSearchCountries = function () {
        return searchCountries
    };
    this.getOriginalAddresses = function () {
        return uniqueAddresses
    };
    this.storeCleanedAddress =
        function (cleanAddress) {
            cleanedAddresses.push(cleanAddress)
        };
    this.returnCleanAddresses = function () {
        returnAddresses()
    };
    getAddresses();
    getUnique();
    buildSearchStrings()
}

function Clean(searchString, country_3, ajaxErr) {
    var me = this;
    var m_ajaxErr = ajaxErr;
    var m_ajaxTimeout = QAS_Variables.TIMEOUT;
    var premClean = false;
    var strClean = false;
    var partialAddress = "";
    var m_callback, k;
    var origSearchString = searchString;
    var chooseProxy = function (country) {
        for (k = 0; k < QAS_Variables.ADD_DATA_SETS.length; k++)
            if (country === QAS_Variables.ADD_DATA_SETS[k]) return QAS_Variables.ADD_PROXY_PATH;
        return QAS_Variables.PROXY_PATH
    };
    var proxyToUse = chooseProxy(country_3);
    var stripPostCode = function (str) {
        switch (me.country) {
            case "AUS":
                str =
                    str.replace(/\d{4}$/, "");
                break;
            case "DEU":
                str = str.replace(/\d{5}-\d{5}$/, "");
                break;
            case "DNK":
                str = str.replace(/\s\d{4}\s/, " ");
                break;
            case "FRA":
                str = str.replace(/\s\d{5}\s/, " ");
                break;
            case "GBR":
                str = str.replace(/\w{1,2}\d{1,2}\w?\s\d\w{2}$/, "");
                break;
            case "LUX":
                str = str.replace(/\s\d{4}\s/, "");
                break;
            case "NLD":
                str = str.replace(/\s\d{4}\s\w{2}\s/, " ");
                break;
            case "NZL":
                str = str.replace(/\d{4}$/, "");
                break;
            case "SGP":
                str = str.replace(/\d{6}$/, "");
                break;
            case "USA":
                str = str.replace(/-\d{4}$/, "");
                break
        }
        return str
    };
    var saveAddress = function () {
        me.result.push($(this).text())
    };
    var savePickList = function () {
        var partialText = $(this).find("partialtext").text();
        var addressText = $(this).find("addresstext").text();
        var postCode = $(this).find("postcode").text();
        var moniker = $(this).find("moniker").text();
        var fulladdress = $(this).find("fulladdress").text();
        me.result.push({
            "partialText": partialText,
            "addressText": addressText,
            "postCode": postCode,
            "moniker": moniker,
            "fulladdress": fulladdress
        })
    };
    var getPartialAddress = function () {
        var i;
        for (i = 0; i < me.result.length; i++)
            if (me.result[i].fulladdress.toString().toLowerCase() === "false") return me.result[i].partialText;
        return null
    };
    var saveResult = function (xml) {
        me.verifylevel = $(xml).find("verifylevel").text();
        me.dpv = $(xml).find("dpvstatus").text();
        me.error = $(xml).find("error").text();
        if (me.error !== "" && QAS_Variables.DISPLAY_ERRORS) {
            m_ajaxErr(xml, me.error, "Error");
            return
        }
        if (premClean && me.verifylevel === "PremisesPartial") premClean = false;
        else if (strClean && me.verifylevel === "StreetPartial") strClean =
            false;
        else {
            me.result = [];
            premClean = false;
            strClean = false;
            me.missingsubprem = false;
            if (me.verifylevel === "Verified" || me.verifylevel === "VerifiedStreet" || me.verifylevel === "VerifiedPlace" || me.verifylevel === "InteractionRequired") {
                $(xml).find("line").each(saveAddress);
                me.missingsubprem = $(xml).find("missingsubprem").text()
            } else {
                $(xml).find("picklistitem").each(savePickList);
                if (me.verifylevel === "PremisesPartial" || me.verifylevel === "StreetPartial") {
                    partialAddress = getPartialAddress();
                    if (partialAddress === null) me.verifylevel =
                        "Multiple"
                }
            }
        }
        m_callback()
    };
    var ajaxCall = function (parameters) {
        $.ajax({
            type: "POST",
            url: proxyToUse,
            async: true,
            data: parameters,
            dataType: "xml",
            success: saveResult,
            timeout: m_ajaxTimeout,
            error: m_ajaxErr
        })
    };
    var doSearch = function (address, c3) {
        var ajaxParams = {
            "action": "search",
            "addlayout": QAS_Variables.QAS_LAYOUT,
            "country": c3,
            "searchstring": address
        };
        ajaxCall(ajaxParams)
    };
    var doFormat = function (moniker) {
        var ajaxParams = {
            "action": "GetFormattedAddress",
            "addlayout": QAS_Variables.QAS_LAYOUT,
            "moniker": moniker
        };
        ajaxCall(ajaxParams)
    };
    var doRefine = function (moniker) {
        var ajaxParams = {
            "action": "refine",
            "addlayout": QAS_Variables.QAS_LAYOUT,
            "moniker": moniker,
            "refinetext": ""
        };
        ajaxCall(ajaxParams)
    };
    this.result = [];
    this.verifylevel = "";
    this.dpv = "";
    this.error = "";
    this.missingsubprem = false;
    this.country = country_3;
    this.search = function (callback) {
        m_callback = callback;
        doSearch(origSearchString, me.country)
    };
    this.searchPremisesPartial = function (aptNo, callback) {
        m_callback = callback;
        premClean = true;
        var noPost = stripPostCode(decodeURIComponent(partialAddress));
        var aptAddress = encodeURIComponent(noPost.replace(/,/, " # " + aptNo + ","));
        doSearch(aptAddress, me.country)
    };
    this.searchStreetPartial = function (buildingNo, callback) {
        m_callback = callback;
        strClean = true;
        var noPost = stripPostCode(decodeURIComponent(partialAddress));
        var buildAddress = encodeURIComponent(buildingNo + " " + noPost);
        doSearch(buildAddress, me.country)
    };
    this.searchDPVPartial = function (buildingNo, callback) {
        m_callback = callback;
        var wholeAddress = me.result.join("|");
        wholeAddress = encodeURIComponent(wholeAddress.replace(/\|?\d+\w*\s/,
            "|" + buildingNo + " "));
        doSearch(wholeAddress, me.country)
    };
    this.formatAddress = function (moniker, callback) {
        m_callback = callback;
        doFormat(moniker)
    };
    this.refineAddress = function (moniker, callback) {
        m_callback = callback;
        doRefine(moniker)
    }
}

function Business(callback, clean, orig, inter) {
    var me = this;
    var m_callback = callback;
    var m_clean = clean;
    var m_orig = orig;
    var m_inter = inter;
    var previousMatch = "";
    var count = 0;
    var aptCheck = function (lvrLine) {
        var isApt = "";
        isApt = m_clean.result[lvrLine];
        if (isApt) {
            var wholeAddress = m_clean.result.join("|");
            if (wholeAddress.search(/\|?\d+\s*-\s*\d+/) !== -1) return true;
            else return false
        } else return true
    };
    this.noInteraction = function () {
        if (m_clean.verifylevel === "Verified" || me.verifylevel === "VerifiedStreet" || me.verifylevel ===
            "VerifiedPlace" || m_clean.verifylevel === "InteractionRequired") m_callback();
        else me.useOriginal()
    };
    this.processResult = function () {
        count++;
        switch (m_clean.verifylevel) {
            case "Verified":
            case "VerifiedStreet":
                if (m_clean.country === "USA")
                    if (clean.dpv === "DPVNotConfirmed") {
                        m_inter.setDPVPartial(m_orig, QAS_PROMPTS.DPVPartial, me.refineDPV, me.useOriginal);
                        m_inter.display()
                    } else if (clean.dpv === "DPVConfirmedMissingSec") {
                    m_inter.setInterReq(m_clean.result, m_orig, QAS_PROMPTS.InteractionRequired, me.acceptInter, me.useOriginal);
                    m_inter.display()
                } else m_callback();
                else if (m_clean.country === "CAN")
                    if (!aptCheck(QAS_Variables.LVR - 1)) {
                        m_inter.setAptAppend(m_orig, QAS_PROMPTS.AptAppend, me.appendApt, m_callback, me.useOriginal);
                        m_inter.display()
                    } else m_callback();
                else m_callback();
                break;
            case "VerifiedPlace":
            case "InteractionRequired":
                if (m_clean.country === "CAN" && !aptCheck(QAS_Variables.LVR - 1)) {
                    m_inter.setAptAppend(m_orig, QAS_PROMPTS.AptAppend, me.appendApt, m_callback, me.useOriginal);
                    m_inter.display()
                } else if (count > 1) m_callback();
                else {
                    m_inter.setInterReq(m_clean.result,
                        m_orig, QAS_PROMPTS.InteractionRequired, me.acceptInter, me.useOriginal);
                    m_inter.display()
                }
                break;
            case "PremisesPartial":
                m_inter.setPartial(m_clean.result, m_orig, QAS_PROMPTS.PremisesPartial, me.refineApt, me.acceptMoniker, me.useOriginal);
                m_inter.display();
                if (previousMatch === "PremisesPartial") alert(QAS_PROMPTS.PremisesPartial.invalidRange);
                previousMatch = "PremisesPartial";
                break;
            case "StreetPartial":
                m_inter.setPartial(m_clean.result, m_orig, QAS_PROMPTS.StreetPartial, me.refineBuild, me.acceptMoniker, me.useOriginal);
                m_inter.display();
                if (previousMatch === "StreetPartial") alert(QAS_PROMPTS.StreetPartial.invalidRange);
                previousMatch = "StreetPartial";
                break;
            case "Multiple":
                m_inter.setMultiple(m_clean.result, m_orig, QAS_PROMPTS.Multiple, me.acceptMoniker, me.refineMult, me.useOriginal);
                m_inter.display();
                break;
            case "None":
                m_inter.setNone(m_orig, QAS_PROMPTS.None, me.useOriginal);
                m_inter.display();
                break
        }
    };
    this.acceptInter = function () {
        m_callback()
    };
    this.acceptMoniker = function (moniker) {
        m_clean.formatAddress(moniker, m_callback)
    };
    this.refineApt = function () {
        var aptNo = $("#QAS_RefineText").val();
        m_clean.searchPremisesPartial(aptNo, me.processResult)
    };
    this.refineBuild = function () {
        var buildNo = $("#QAS_RefineText").val();
        m_clean.searchStreetPartial(buildNo, me.processResult)
    };
    this.refineDPV = function () {
        var buildNo = $("#QAS_RefineText").val();
        m_clean.searchDPVPartial(buildNo, me.processResult)
    };
    this.appendApt = function () {
        var aptNo = $("#QAS_RefineText").val();
        var aptIndex = 0;
        var aptLine = false;
        while (!aptLine && aptIndex < m_clean.result.length) {
            if (decodeURIComponent(m_clean.result[aptIndex]).search(/^\d+\s/) !==
                -1) {
                aptLine = true;
                m_clean.result[aptIndex] = aptNo + "-" + m_clean.result[aptIndex]
            }
            aptIndex++
        }
        m_callback()
    };
    this.refineMult = function (moniker) {
        m_clean.refineAddress(moniker, me.processResult)
    };
    this.useOriginal = function () {
        m_clean.result = m_orig;
        m_callback()
    }
}

function Interface(editCall) {
    var m_editCall = editCall;
    var m_pickList;
    var m_orig;
    var m_message;
    var m_pickHtml = "";
    var buildPick = function () {
        var i;
        m_pickHtml = "";
        for (i = 0; i < m_pickList.length; i++)
            if (m_pickList[i].fulladdress.toString().toLowerCase() === "true") m_pickHtml += "<tr><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].postCode) +
                "</a></td></tr>";
            else m_pickHtml += "<tr><td NOWRAP>" + decodeURIComponent(m_pickList[i].addressText) + "</td><td NOWRAP>" + decodeURIComponent(m_pickList[i].postCode) + "</td></tr>"
    };
    var buildMultPick = function () {
        var i;
        m_pickHtml = "";
        for (i = 0; i < m_pickList.length; i++)
            if (m_pickList[i].fulladdress.toString().toLowerCase() === "true") m_pickHtml += "<tr><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" +
                m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].postCode) + "</a></td></tr>";
            else m_pickHtml += "<tr><td NOWRAP><a href='#' class='QAS_Refine' moniker='" + m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_Refine' moniker='" + m_pickList[i].moniker + "'>" + decodeURIComponent(m_pickList[i].postCode) + "</a></td></tr>"
    };
    var buildRightSide = function (callback) {
        var origHtml = "";
        var i;
        for (i = 0; i < m_orig.length; i++) origHtml += "<tr><td>" + decodeURIComponent(m_orig[i]) +
            "</td></tr>";
        $(".QAS_RightDetails").html("<div class='QAS_RightSidePrompt'>" + "<div class='QAS_RightSidePromptText'>" + QAS_PROMPTS.RightSide.prompt + "<span class='QAS_EditLink'>[<a href='#' id='QAS_Edit'>" + QAS_PROMPTS.RightSide.edit + "</a>]</span>" + "</div>" + "</div>" + "<table>" + origHtml + "</table>" + "<input type='button' id='QAS_AcceptOriginal' value='" + QAS_PROMPTS.RightSide.button + "' />");
        $("#QAS_AcceptOriginal").button();
        $("#QAS_AcceptOriginal").click(function () {
            $("#QAS_Dialog").dialog("close");
            callback()
        });
        $("#QAS_Edit").click(function () {
            $("#QAS_Dialog").dialog("close");
            m_editCall()
        })
    };
    var load = function () {
        $("#QAS_Dialog").remove();
        $("#QAS_Wait").remove();
        $(document.body).append("<div id='QAS_Dialog' title='" + QAS_PROMPTS.title + "'>" + "  <div class='QAS_Header ui-state-highlight'></div>" + "  <div class='QAS_LeftDetails'>" + "  <div class='QAS_Prompt'>" + "    <div class='QAS_PromptText'></div>" + "    <div class='QAS_PromptData'></div>" + "         <div class='QAS_Input'></div>" + "  </div>" + "  <div class='QAS_Picklist'>" +
            "    <div class='QAS_MultPick'></div>" + "    <div class='QAS_ShowPick'></div>" + "    <div class='QAS_Pick'></div>" + "  </div>" + "  </div>" + "  <div class='QAS_RightDetails'></div>" + "</div>" + "<div id='QAS_Wait' title = '" + QAS_PROMPTS.waitMessage + "'></div>");
        $("#QAS_Dialog").dialog({
            modal: true,
            width: 850,
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            draggable: false
        });
        $("#QAS_Wait").dialog({
            modal: true,
            height: 100,
            width: 200,
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            draggable: false
        });
        $(".QAS_ShowPick").click(function () {
            $(".QAS_Pick").slideToggle("slow")
        });
        $(window).resize(function () {
            $("#QAS_Dialog").dialog("option", "position", "center")
        })
    };
    this.waitOpen = function () {
        $("#QAS_Wait").dialog("open");
        $(".ui-dialog-titlebar-close").css("display", "none");
        $(".ui-dialog-content").hide()
    };
    this.waitClose = function () {
        $("#QAS_Wait").dialog("close")
    };
    this.display = function () {
        window.scroll(0, 0);
        $("#QAS_Dialog").dialog("open");
        $(".ui-dialog-titlebar-close").css("display", "none");
        $("#QAS_RefineBtn").blur();
        $(".QAS_Header").focus()
    };
    this.setInterReq = function (cleaned, orig,
        message, acceptCallback, origCallback) {
        m_orig = orig;
        m_message = message;
        var cleanedHtml = "",
            i;
        buildRightSide(origCallback);
        for (i = 0; i < QAS_Variables.DISPLAY_LINES; i++) cleanedHtml += "<tr><td>" + decodeURIComponent(cleaned[i]) + "</td></tr>";
        $(".QAS_Header").html(message.header);
        $(".QAS_PromptText").html(message.prompt);
        $(".QAS_PromptData").html("<table>" + cleanedHtml + "</table>");
        $(".QAS_Input").html("<input type='button' id='QAS_RefineBtn' value='" + message.button + "' />");
        $(".QAS_MultPick").html("");
        $(".QAS_ShowPick").html("");
        $(".QAS_Pick").html("");
        $(".QAS_MultPick").hide();
        $("#QAS_RefineBtn").button();
        $("#QAS_RefineBtn").click(function () {
            $("#QAS_Dialog").dialog("close");
            acceptCallback()
        })
    };
    this.setPartial = function (pickList, orig, message, refineCallback, monikerCallback, origCallback) {
        m_pickList = pickList;
        m_orig = orig;
        m_message = message;
        buildPick();
        buildRightSide(origCallback);
        $(".QAS_Header").html(message.header);
        $(".QAS_PromptText").html(message.prompt);
        $(".QAS_PromptData").html("");
        $(".QAS_Input").html("<input type='text' id='QAS_RefineText' />" +
            "<input type='button' id='QAS_RefineBtn' value='" + message.button + "' />");
        $(".QAS_MultPick").html("");
        $(".QAS_ShowPick").html("<a href='#'>" + message.showPicklist + "</a>");
        $(".QAS_Pick").html("<table>" + m_pickHtml + "</table>");
        $(".QAS_MultPick").hide();
        $("#QAS_RefineBtn").button();
        $("#QAS_RefineBtn").click(function () {
            if ($("#QAS_RefineText").val() === "") alert("No value entered");
            else {
                $("#QAS_Dialog").dialog("close");
                refineCallback()
            }
        });
        $(".QAS_StepIn").click(function () {
            $("#QAS_Dialog").dialog("close");
            var mon =
                $(this).attr("moniker");
            monikerCallback(mon)
        })
    };
    this.setDPVPartial = function (orig, message, refineCallback, origCallback) {
        m_orig = orig;
        m_message = message;
        buildRightSide(origCallback);
        $(".QAS_Header").html(message.header);
        $(".QAS_PromptText").html(message.prompt);
        $(".QAS_PromptData").html("");
        $(".QAS_Input").html("<input type='text' id='QAS_RefineText' />" + "<input type='button' id='QAS_RefineBtn' value='" + message.button + "' />");
        $(".QAS_MultPick").html("");
        $(".QAS_MultPick").hide();
        $("#QAS_RefineBtn").button();
        $("#QAS_RefineBtn").click(function () {
            if ($("#QAS_RefineText").val() === "") alert("No value entered");
            else {
                $("#QAS_Dialog").dialog("close");
                refineCallback()
            }
        })
    };
    this.setAptAppend = function (orig, message, refineCallback, noAptCallback, origCallback) {
        m_orig = orig;
        m_message = message;
        buildRightSide(origCallback);
        $(".QAS_Header").html(message.header);
        $(".QAS_PromptText").html(message.prompt);
        $(".QAS_PromptData").html("");
        $(".QAS_Input").html("<input type='text' id='QAS_RefineText' />" + "<input type='button' id='QAS_RefineBtn' value='" +
            message.button + "' />" + "<br />" + "<input type='button' id='QAS_NoApt' value='" + message.noApt + "' />");
        $(".QAS_MultPick").html("");
        $(".QAS_MultPick").hide();
        $("#QAS_RefineBtn").button();
        $("#QAS_NoApt").button();
        $("#QAS_RefineBtn").click(function () {
            if ($("#QAS_RefineText").val() === "") alert("No value entered");
            else {
                $("#QAS_Dialog").dialog("close");
                refineCallback()
            }
        });
        $("#QAS_NoApt").click(function () {
            $("#QAS_Dialog").dialog("close");
            noAptCallback()
        })
    };
    this.setMultiple = function (pickList, orig, message, formatCallback,
        refineCallback, origCallback) {
        m_pickList = pickList;
        m_orig = orig;
        m_message = message;
        buildMultPick();
        buildRightSide(origCallback);
        $(".QAS_Header").html(message.header);
        $(".QAS_PromptText").html(message.prompt);
        $(".QAS_PromptData").html("");
        $(".QAS_Input").html("");
        $(".QAS_MultPick").html("<table>" + m_pickHtml + "</table>");
        $(".QAS_ShowPick").html("");
        $(".QAS_Pick").html("");
        $(".QAS_MultPick").show();
        $(".QAS_StepIn").click(function () {
            $("#QAS_Dialog").dialog("close");
            var mon = $(this).attr("moniker");
            formatCallback(mon)
        });
        $(".QAS_Refine").click(function () {
            $("#QAS_Dialog").dialog("close");
            var mon = $(this).attr("moniker");
            refineCallback(mon)
        })
    };
    this.setNone = function (orig, message, origCallback) {
        m_orig = orig;
        m_message = message;
        buildRightSide(origCallback);
        $(".QAS_Header").html(message.header);
        $(".QAS_Prompt").remove();
        $(".QAS_Input").remove();
        $(".QAS_MultPick").html("");
        $(".QAS_ShowPick").remove();
        $(".QAS_Pick").remove();
        $(".QAS_RightDetails").css("float", "left");
        $(".QAS_MultPick").hide()
    };
    load()
}

function EmailPhoneInterface(editCall) {
    var thisResult;
    this.waitOpen = function () {
        $("#QAS_Wait").dialog("open");
        $(".ui-dialog-titlebar-close").css("display", "none");
        $(".ui-dialog-content").hide()
    };
    this.waitClose = function () {
        $("#QAS_Wait").dialog("close")
    };
    this.display = function () {
        window.scroll(0, 0);
        $("#QAS_Dialog").dialog("open");
        $(".ui-dialog-titlebar-close").css("display", "none");
        $("#QAS_RefineBtn").blur();
        $(".QAS_Header").focus()
    };
    this.displayResult = function (tdResult, callback) {
        var emailSuggestionsHTML;
        var numDivs = 0;
        var grp = $("#QAS_Dialog").children();
        var cnt = grp.length;
        var emailPos = 0;
        var phonePos = 0;
        var tempDiv, tempVal, iCounter;
        var procEmail = false;
        var procPhone = false;
        thisResult = tdResult;
        for (iCounter = 0; iCounter < cnt; iCounter++) {
            tempDiv = grp[iCounter];
            if ($(tempDiv).attr("class") === "QAS_EmailPrompt") emailPos = iCounter;
            else if ($(tempDiv).attr("class") === "QAS_PhonePrompt") phonePos = iCounter
        }
        $(".QAS_Header").html("");
        $(".QAS_EmailPromptText").html("");
        $(".QAS_EmailErrText").html("");
        $(".QAS_EmailInput").html("");
        $(".QAS_EmailPromptData").html("");
        $(".QAS_PhonePromptText").html("");
        $(".QAS_PhoneErrText").html("");
        $(".QAS_PhoneInput").html("");
        $(".QAS_EmailSuggPrompt").html("");
        if (thisResult.email !== undefined && thisResult.email !== null && thisResult.email.ok === false) {
            procEmail = true;
            $(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.headerEmail);
            $(".QAS_EmailPromptText").html(QAS_PROMPTS.ConfirmEmailPhone.promptEmail);
            if (QAS_Variables.DISPLAY_CUSTOM_EMAIL_ERR === true) $(".QAS_EmailErrText").html(EMAIL_ERR_MESSAGES[thisResult.email.status_code]);
            $(".QAS_EmailInput").html("<input type='text' id='QAS_EmailRefineText' value='" + thisResult.email.address + "' />");
            if (thisResult.email.corrections !== undefined && thisResult.email.corrections !== null && thisResult.email.corrections.length > 0) {
                $(".QAS_EmailSuggPrompt").html("Suggestions:");
                emailSuggestionsHTML = "<table id='QAS_EmailSuggestions'><tbody>";
                for (iCounter = 0; iCounter < thisResult.email.corrections.length; iCounter++) emailSuggestionsHTML += "<tr><td>" + thisResult.email.corrections[iCounter] + "</td></tr>";
                emailSuggestionsHTML += "</tbody></table>";
                $(".QAS_EmailPromptData").html(emailSuggestionsHTML)
            }
            if (emailPos > phonePos) {
                tempDiv = grp[emailPos];
                grp[emailPos] = grp[phonePos];
                grp[phonePos] = tempDiv
            }
            numDivs++
        }
        if (thisResult.phone !== undefined && thisResult.phone !== null && thisResult.phone.ok === false) {
            procPhone = true;
            $(".QAS_PhonePromptText").html(QAS_PROMPTS.ConfirmEmailPhone.promptPhone);
            if (QAS_Variables.DISPLAY_CUSTOM_PHONE_ERR === true) $(".QAS_PhoneErrText").html(PHONE_ERR_MESSAGES[tdResult.phone.status_code]);
            $(".QAS_PhoneInput").html("<input type='text' id='QAS_PhoneRefineText' value='" +
                $("input#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val() + "' />");
            if (numDivs === 1) {
                $(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.header);
                $(".QAS_PhonePrompt").css("float", "right")
            } else {
                $(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.headerPhone);
                $(".QAS_PhonePrompt").css("float", "left");
                if (emailPos < phonePos) {
                    tempDiv = grp[emailPos];
                    grp[emailPos] = grp[phonePos];
                    grp[phonePos] = tempDiv
                }
            }
            numDivs++
        }
        $(grp).remove();
        $("#QAS_Dialog").append($(grp));
        if (procEmail === true) $("tbody td").click(function (e) {
            $("input#QAS_EmailRefineText").val($(this).text())
        });
        if (numDivs === 1) {
            $("#QAS_Dialog").dialog({
                width: 550
            });
            $(".QAS_EmailInput").focus()
        } else $("#QAS_Dialog").dialog({
            width: 800
        });
        if (numDivs > 0) {
            $(".QAS_EmailPhoneContinue").html("<input type='button' id='QAS_TDContinue' value='Continue' />");
            $("#QAS_TDContinue").button();
            $("#QAS_TDContinue").click(function () {
                var searchString = "action=validate";
                if (thisResult.phone !== undefined && thisResult.phone !== null && thisResult.phone.ok === false) {
                    $("#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val($("#QAS_PhoneRefineText").val());
                    searchString = searchString + "&phone=" + $("#QAS_PhoneRefineText").val()
                }
                if (thisResult.email !== undefined && thisResult.email !== null && thisResult.email.ok === false) {
                    $("#" + QAS_Variables.EMAIL_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val($("#QAS_EmailRefineText").val());
                    searchString = searchString + "&email=" + $("#QAS_EmailRefineText").val();
                    searchString = searchString + "&emaillev=" + QAS_Variables.EMAIL_VAL_LEVEL
                }
                $("#QAS_Dialog").dialog("close");
                callback(searchString)
            })
        }
    };
    var load = function () {
        $("#QAS_Wait").remove();
        $("#QAS_Dialog").remove();
        $(document.body).append("<div id='QAS_Dialog' title='" + QAS_PROMPTS.emailphoneTitle + "'>" + "  <div class='QAS_Header ui-state-highlight'></div>" + "  <div class='QAS_EmailPrompt'>" + "    <div class='QAS_EmailPromptText'></div>" + "    <div class='QAS_EmailErrText'></div>" + "    <div class='QAS_EmailInput'></div>" + "    <div class='QAS_EmailSuggPrompt'></div>" + "    <div class='QAS_EmailPromptData'></div>" + "  </div>" + "  <div class='QAS_PhonePrompt'>" + "    <div class='QAS_PhonePromptText'></div>" +
            "    <div class='QAS_PhoneErrText'></div>" + "    <div class='QAS_PhoneInput'></div>" + "  </div>" + "  <div class='QAS_EmailPhoneContinue'></div>" + "</div>" + "<div id='QAS_Wait' title='" + QAS_PROMPTS.waitMessage + "'></div>");
        $("#QAS_Wait").dialog({
            modal: true,
            height: 100,
            width: 200,
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            draggable: false
        });
        $("#QAS_Dialog").dialog({
            modal: true,
            width: 800,
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            draggable: false
        });
        $(window).resize(function () {
            $("#QAS_Dialog").dialog("option",
                "position", "center")
        })
    };
    load()
}

function EmailPhoneValidation(clickEvent, buttonID, nextProcess) {
    this.records = [];
    var searchString, m_click = clickEvent,
        m_button = buttonID,
        m_nextProcess = nextProcess,
        inter, m_submit = false;
    var ajaxError = function (xml, text, msg) {
        inter.waitClose();
        if (QAS_Variables.DISPLAY_ERRORS) alert(text + "\n Error with AJAX call. Check to make sure the service is configured and running correctly.")
    };
    var ajaxEmailPhoneProc = function (funcForCallback) {
        $.ajax({
            type: "POST",
            url: QAS_Variables.TD_PROXY_PATH,
            async: true,
            data: searchString,
            dataType: "json",
            success: funcForCallback,
            timeout: QAS_Variables.TIMEOUT_EMAILPHONE,
            error: ajaxError,
            cache: false
        })
    };
    var emailPhoneFinish = function () {
        $("select").css("visibility", "");
        inter.waitClose();
        if (m_nextProcess !== null && m_nextProcess !== undefined) nextProcess(m_click, m_button);
        else {
            if (m_click !== null && m_click !== undefined) m_click();
            if (m_button !== "") {
                $("#" + m_button).attr("onclick", "");
                $("#" + m_button).parent("form").attr("onsubmit", "");
                $("#" + m_button).click()
            }
        }
    };
    var checkForValidSearch = function () {
        var iCounter;
        for (iCounter = 0; iCounter < QAS_Variables.EMAIL_FIELD_IDS.length; iCounter++)
            if ($("input#" + QAS_Variables.EMAIL_FIELD_IDS[iCounter]).val() !== "") {
                m_submit = true;
                break
            } for (iCounter = 0; m_submit === false && iCounter < QAS_Variables.PHONE_FIELD_IDS.length; iCounter++)
            if ($("input#" + QAS_Variables.PHONE_FIELD_IDS[iCounter]).val() !== "") {
                m_submit = true;
                break
            }
    };
    var prepareSearch = function () {
        var tempSearch, cindex, submitphone = false,
            validSearch = false;
        if (QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.EMAIL_FIELD_IDS.length && QAS_TEMP_VARS.EMAIL_PHONE_POS >=
            QAS_Variables.PHONE_FIELD_IDS.length) return false;
        searchString = "action=validate";
        searchString = searchString + "&emaillev=" + QAS_Variables.EMAIL_VAL_LEVEL;
        tempSearch = encodeURIComponent($("input#" + QAS_Variables.EMAIL_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val());
        if (tempSearch !== "") {
            validSearch = true;
            searchString = searchString + "&email=" + tempSearch
        }
        if (QAS_Variables.PHONE_VALIDATE_COUNTRY[0] === "ALL" || QAS_Variables.COUNTRY_FIELD_IDS.length < QAS_TEMP_VARS.EMAIL_PHONE_POS) submitphone = true;
        else {
            tempSearch = $("#" +
                QAS_Variables.COUNTRY_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val();
            for (cindex = 0; cindex < QAS_Variables.PHONE_VALIDATE_COUNTRY.length; cindex++)
                if (tempSearch === QAS_Variables.PHONE_VALIDATE_COUNTRY[cindex]) {
                    submitphone = true;
                    break
                }
        }
        if (submitphone === true) {
            tempSearch = encodeURIComponent($("input#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val());
            if (tempSearch !== "") {
                validSearch = true;
                searchString = searchString + "&phone=" + tempSearch
            }
        }
        return validSearch
    };
    var processUIContinue = function (newSearchString) {
        if (QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS <
            QAS_Variables.EMAIL_PHONE_NUM_SUBMITS || QAS_Variables.EMAIL_PHONE_NUM_SUBMITS === 0) {
            searchString = newSearchString;
            inter.waitOpen();
            QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS++;
            ajaxEmailPhoneProc(emailPhoneUICallback)
        } else {
            QAS_TEMP_VARS.EMAIL_PHONE_POS++;
            processNext()
        }
    };
    var emailPhoneUICallback = function (response) {
        var processedResult = false;
        inter.waitClose();
        if (response !== undefined || response !== null) {
            if (response.email !== undefined)
                if (response.email.ok === false) {
                    inter.displayResult(response, processUIContinue);
                    inter.display();
                    processedResult = true
                } if (response.phone !== undefined && processedResult === false)
                if (response.phone.ok === false) {
                    inter.displayResult(response, processUIContinue);
                    inter.display();
                    processedResult = true
                }
        } else if (response.error !== undefined) {
            processedResult = true;
            if (QAS_Variables.DISPLAY_ERRORS) alert(response.error + "\n Error with AJAX call. Check to make sure the service is configured and running correctly.")
        }
        if (processedResult === false) {
            QAS_TEMP_VARS.EMAIL_PHONE_POS++;
            processNext()
        }
    };
    var emailPhoneStdCallback = function (response) {
        if (response.email !==
            undefined && response.email !== null && response.email.ok === false) {
            $("label#" + QAS_Variables.EMAIL_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).show();
            if (QAS_Variables.DISPLAY_CUSTOM_EMAIL_ERR === true) {
                message = EMAIL_ERR_MESSAGES[response.email.status_code];
                if (message !== undefined && message !== null) $("label#" + QAS_Variables.EMAIL_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).text(message)
            }
        }
        if (response.phone !== undefined && response.phone !== null && response.phone.ok === false) {
            $("label#" + QAS_Variables.PHONE_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).show();
            if (QAS_Variables.DISPLAY_CUSTOM_PHONE_ERR === true) {
                message = PHONE_ERR_MESSAGES[response.phone.status_code];
                if (message !== undefined && message !== null) $("label#" + QAS_Variables.PHONE_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).text(message)
            }
        }
        QAS_TEMP_VARS.EMAIL_PHONE_POS++;
        processNext()
    };
    var processNext = function () {
        var funcForCallback;
        QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS = 0;
        if (QAS_Variables.EMAIL_PHONE_USEDIALOG === true) funcForCallback = emailPhoneUICallback;
        else funcForCallback = emailPhoneStdCallback;
        if (prepareSearch() ===
            false) {
            QAS_TEMP_VARS.EMAIL_PHONE_POS++;
            if (QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.EMAIL_FIELD_IDS.length && QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.PHONE_FIELD_IDS.length) {
                emailPhoneFinish();
                return
            } else processNext()
        } else {
            QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS++;
            ajaxEmailPhoneProc(funcForCallback)
        }
    };
    this.process = function () {
        if (m_submit === false) emailPhoneFinish();
        else {
            $("select").css("visibility", "hidden");
            inter.waitOpen();
            processNext()
        }
    };
    inter = new EmailPhoneInterface(null);
    QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS =
        0;
    QAS_TEMP_VARS.EMAIL_PHONE_POS = 0;
    checkForValidSearch()
}

function Main(clickEvent, buttonID) {
    var me = this;
    var m_click = clickEvent;
    var m_button = buttonID;
    var add = new Address;
    var strings = add.getSearchStrings();
    var countries = add.getSearchCountries();
    var orig = add.getOriginalAddresses();
    var inter, clean;
    var procIndex = 0;
    this.process = function () {
        inter = new Interface(me.returnEarly);
        clean = new Clean(strings[procIndex], countries[procIndex], me.ajaxError);
        if (strings[procIndex]) {
            inter.waitOpen();
            clean.search(me.process2)
        } else {
            clean.result = orig[procIndex];
            me.next()
        }
    };
    this.process2 =
        function () {
            inter.waitClose();
            var business = new Business(me.next, clean, orig[procIndex], inter);
            if (!QAS_Variables.ADDRESS_INTERACTION) business.noInteraction();
            else business.processResult()
        };
    this.next = function () {
        clean.result.push(clean.verifylevel);
        add.storeCleanedAddress(clean.result);
        procIndex++;
        if (procIndex < strings.length) me.process();
        else me.finish()
    };
    this.finish = function () {
        add.returnCleanAddresses();
        if (m_click !== null) m_click();
        if (m_button !== "") {
            $("#" + m_button).attr("onclick", "");
            $("#" + m_button).parent("form").attr("onsubmit",
                "");
            $("#" + m_button).click()
        }
    };
    this.returnEarly = function () {
        $("select").css("visibility", "");
        add.returnCleanAddresses()
    };
    this.ajaxError = function (xml, text, msg) {
        if (text === "timeout") {
            clean.verifylevel = "Timeout";
            text = "Timeout"
        } else clean.verifylevel = "Error";
        inter.waitClose();
        if (QAS_Variables.DISPLAY_ERRORS) alert(text + "\n Error with AJAX call. Check to make sure the service is configured and running correctly.");
        clean.result = orig[procIndex];
        me.next()
    }
}

function QAS_Verify() {
    var preOnclick = QAS_Variables.PRE_ON_CLICK;
    var postOnclick = QAS_Variables.POST_ON_CLICK;
    var buttonID = QAS_Variables.BUTTON_ID;
    var mProcAdd = null;
    var mProcEmailPhone = null;
    $(".error").hide();
    if (preOnclick === null) {
        mProcAdd = new Main(postOnclick, buttonID);
        mProcEmailPhone = new EmailPhoneValidation(postOnclick, buttonID, mProcAdd.process);
        mProcEmailPhone.process()
    } else if (preOnclick()) {
        mProcAdd = new Main(postOnclick, buttonID);
        mProcEmailPhone = new EmailPhoneValidation(postOnclick, buttonID, mProcAdd.process);
        mProcEmailPhone.process()
    }
    return false
}

function QAS_Verify_Address() {
    var preOnclick = QAS_Variables.PRE_ON_CLICK;
    var postOnclick = QAS_Variables.POST_ON_CLICK;
    var buttonID = QAS_Variables.BUTTON_ID;
    var m = null;
    if (preOnclick === null) {
        m = new Main(postOnclick, buttonID);
        m.process()
    } else if (preOnclick()) {
        m = new Main(postOnclick, buttonID);
        m.process()
    }
    return false
}

function QAS_Verify_EmailPhone() {
    var preOnclick = QAS_Variables.PRE_ON_CLICK;
    var postOnclick = QAS_Variables.POST_ON_CLICK;
    var buttonID = QAS_Variables.BUTTON_ID;
    var mProc = null;
    $(".error").hide();
    if (preOnclick === null) {
        mProc = new EmailPhoneValidation(postOnclick, buttonID);
        mProc.process()
    } else if (preOnclick()) {
        mProc = new EmailPhoneValidation(postOnclick, buttonID);
        mProc.process()
    }
    return false
}
window.QAS_Verify = QAS_Verify;
window.QAS_Verify_Address = QAS_Verify_Address;
window.QAS_Verify_EmailPhone = QAS_Verify_EmailPhone;
