var LOCAPI_CONFIG = {
    SUBMIT_PATH: "https://www.terminix.com/includes/endpointLocApi.jsp",
    PROP_SUBMIT_PATH: "https://www.terminix.com/includes/endpointLocApiProp.jsp",
    AFTER_VERIFY: function () {
        if (document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD)) LOCAPI_PROP_DATA();
        else handleGetFreeQuote()
    },
    PROP_AFTER_VERIFY: function (wait) {
        handleGetFreeQuote()
    },
    PROP_PROCESS_RESPONSE: function (res) {
        var sqFtList = [{
            min: 0,
            max: 3999,
            value: "3999"
        }, {
            min: 4E3,
            max: 5999,
            value: "5999"
        }, {
            min: 6E3,
            max: 7999,
            value: "7999"
        }, {
            min: 8E3,
            max: 1E16,
            value: "8000"
        }];
        var lotSizeList = [{
                min: 0,
                max: .5,
                value: "0.5"
            }, {
                min: .501,
                max: 1,
                value: "1.0"
            }, {
                min: 1.001,
                max: 1.5,
                value: "1.5"
            }, {
                min: 1.501,
                max: 2,
                value: "2.0"
            }, {
                min: 2.001,
                max: 2.5,
                value: "2.5"
            }, {
                min: 2.501,
                max: 3,
                value: "3.0"
            }, {
                min: 3.001,
                max: 3.5,
                value: "3.5"
            }, {
                min: 3.501,
                max: 4,
                value: "4.0"
            }, {
                min: 4.001,
                max: 4.5,
                value: "4.5"
            }, {
                min: 4.501,
                max: 5,
                value: "5.0"
            }, {
                min: 5.001,
                max: 5.5,
                value: "5.5"
            }, {
                min: 5.501,
                max: 6,
                value: "6.0"
            }, {
                min: 6.001,
                max: 6.5,
                value: "6.5"
            }, {
                min: 6.501,
                max: 7,
                value: "7.0"
            }, {
                min: 7.001,
                max: 7.5,
                value: "7.5"
            }, {
                min: 7.501,
                max: 8,
                value: "8.0"
            }, {
                min: 8.001,
                max: 8.5,
                value: "8.5"
            },
            {
                min: 8.501,
                max: 9,
                value: "9.0"
            }, {
                min: 9.001,
                max: 1E16,
                value: "9.5"
            }];
        if (res && res.svmSquareFootage && res.svmSquareFootage !== "")
            for (var i = 0; i < sqFtList.length; i++) {
                if (res.svmSquareFootage < sqFtList[i].max && res.svmSquareFootage >= sqFtList[i].min) {
                    $("#sqftSize").val(sqFtList[i].value);
                    break
                }
            } else $("#sqftSize").val(sqFtList[0].value);
        if (res && res.svmLotSize && res.svmLotSize !== "")
            for (var i = 0; i < lotSizeList.length; i++) {
                if (res.svmLotSize < lotSizeList[i].max && res.svmLotSize >= lotSizeList[i].min) {
                    $("#lotSize").val(lotSizeList[i].value);
                    break
                }
            } else $("#lotSize").val(lotSizeList[0].value);
        handleGetFreeQuote()
    },
    OPEN_MODAL: function () {
        if (_MATERIALIZE) $("#locApiModal").modal("open");
        else $("#locApiModal").modal("show")
    },
    CLOSE_MODAL: function () {
        if (_MATERIALIZE) $("#locApiModal").modal("close");
        else $("#locApiModal").modal("hide")
    },
    REMOVE_HIDDEN: function (elem) {
        if (_MATERIALIZE) elem.classList.remove("hide");
        else elem.classList.remove("hidden")
    },
    ADD_HIDDEN: function (elem) {
        if (_MATERIALIZE) elem.classList.add("hide");
        else elem.classList.add("hidden")
    },
    ADDRESS_ID_FIELD: "addressId_hidden",
    ADDRESS_FIELD: "address1",
    SUITE_APT_FIELD: "address2",
    CITY_FIELD: "city_hidden",
    STATE_FIELD: "state_hidden",
    POSTALCODE_FIELD: "zipCode",
    BRAND: "TMX.COM",
    POST_VERIFY_FIELD_MODS: {
        POSTALCODE_FIELD: function (pcode) {
            return pcode.replace(/-\d{4}/, "")
        }
    },
    SHOW_CORRECTIONS: true,
    PROCESSING_BODY_ID: "locApiModalProcessingBody",
    EDIT_BODY_ID: "locApiModalModBody",
    MODIFY_ID: "locApiModify",
    USE_AS_ENTERED_ID: "locApiUseAsEntered",
    MAX_SUGGESTIONS: 3,
    MATCH_TAG: "p",
    PURE_JS_MODAL: true
};
var LOCAPI_VERIFYLEVELS = {
    "InteractionRequired": {
        "locWarning": "<b>We think that your address may be incorrect or incomplete.</b><br />To proceed, please choose one of the options below.",
        "locPrompt": "We recommend:",
        "locSubmit": "Use suggested address",
        "inputBind": "NONE"
    },
    "PremisesPartial": {
        "locWarning": "<b>Sorry, we think your apartment/suite/unit is missing or wrong</b><br />To proceed, please enter your apartment/suite/unit or use your address as entered",
        "locPrompt": "Confirm your Apartment/Suite/Unit number:",
        "locSubmit": "Confirm number",
        "inputBind": "SUITE_APT_FIELD"
    },
    "StreetPartial": {
        "locWarning": "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
        "locPrompt": "Confirm your House/Building number:",
        "locSubmit": "Confirm number",
        "inputBind": "ADDRESS_FIELD"
    },
    "DPVPartial": {
        "locWarning": "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
        "locPrompt": "Confirm your House/Building number:",
        "locSubmit": "Confirm number",
        "inputBind": "ADDRESS_FIELD"
    },
    "AptAppend": {
        "locWarning": "<b>Sorry, we think your apartment/suite/unit may be missing.</b><br />To proceed, please check and choose from one of the options below.",
        "locPrompt": "Confirm Apt/Ste:",
        "locSubmit": "Continue",
        "inputBind": "SUITE_APT_FIELD"
    },
    "Multiple": {
        "locWarning": "<b>We found more than one match for your address.</b><br />To proceed, please choose one of the options below.",
        "locPrompt": "Our suggested matches:",
        "locSubmit": "NONE",
        "inputBind": "NONE"
    },
    "None": {
        "locWarning": "<b>Sorry, we could not find a match for your address.</b><br />To proceed, please choose one of the options below.",
        "inputBind": "NONE",
        "locSubmit": "Edit Address"
    },
    "UseAsEntered": {
        "locWarning": "You Entered:",
        "locSubmit": "Use Address As Entered",
        "locPrompt": "Edit"
    },
    "processing": "Please wait, your details are being verified"
};
