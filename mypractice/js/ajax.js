$(function () {

    $.ajax({
        type: "GET",
        url: "js/test.json",
        data: {
            page: 1,
            pagelimit: 10
        },
        success: function (response) {
            console.log(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
});
