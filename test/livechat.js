var g_isHistorySupported = window.history && typeof history.pushState == "function" && typeof history.replaceState == "function" && typeof history.back == "function";

function FormatNumber(value, numDecimals, useCommas) {
    var decimals = "";
    if (numDecimals > 0) {
        for (var i = 0; i < numDecimals; i++) decimals += "0";
        var decimalFactor = Math.pow(10, numDecimals);
        decimals += Math.floor(Math.round(value * decimalFactor)) % decimalFactor;
        decimals = "." + decimals.substr(decimals.length - numDecimals)
    }
    var whole = "" + Math.abs((value >= 0 ? Math.floor(value) : Math.ceil(value)) || 0);
    if (useCommas === true) whole = whole.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return whole + decimals
}

function GetLastFourDigits(value) {
    return value.substr(value.length - 4)
}

function GetMonthName(month) {
    var months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return month >= 0 && month < months.length ? months[month] : ""
}

function GetMonthNameShort(month) {
    return GetMonthName(month).substr(0, 3)
}

function StringToLower(str) {
    return (str ? "" + str : "").toLowerCase()
}

function StringToUpper(str) {
    return (str ? "" + str : "").toUpperCase()
}

function GetAddressLine(address) {
    var address1 = ko.unwrap(address.address1),
        address2 = ko.unwrap(address.address2);
    return address2 ? address1 + " " + address2 : address1
}

function GetCityAndStateFromZip(zip, callback) {
    if (_ENABLE_SMARTY_ZIP_TOGGLE == true) {
        if (typeof callback == "function") SmartyStreetsGetCityAndState(zip, callback)
    } else if (typeof callback == "function") ZiptasticGetCityAndState(zip, callback)
}

function ZiptasticGetCityAndState(zip, callback) {
    $.ajax({
        type: "GET",
        url: "https://zip.getziptastic.com/v2/US/" + zip,
        dataType: "json"
    }).done(function (data) {
        callback({
            model: {
                city: data.city,
                state: data.state_short
            }
        })
    }).fail(function () {
        callback({
            error: "An error has occurred."
        })
    })
}

function SmartyStreetsGetCityAndState(zip, callback) {
    $.ajax({
        type: "GET",
        url: "/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=" + zip,
        dataType: "json"
    }).done(function (result) {
        if (result.data.validResponse && result.data.validResponse === "true") callback({
            model: {
                city: result.data.city,
                state: result.data.state
            }
        });
        else callback({
            error: "Not a valid response was received. Asking Customer for their city and state."
        })
    }).fail(function () {
        callback({
            error: "An error has occurred while calling getCityAndStateFromZip."
        })
    })
}

function ScrollTo(pos, speed) {
    $("html, body").animate({
        scrollTop: pos
    }, speed)
}
jQuery.fn.ShowButtonProgress = function (options) {
    options = $.extend({
        text: "Loading...",
        autosize: true
    }, options);
    return this.each(function () {
        var $button = $(this);
        if ($button.data("in-progress")) {
            $button.prev().remove();
            $button.show()
        }
        var $progressContainer = $('<div class="button-progress-container">'),
            $progressButton = $("<div>");
        $progressContainer.append($progressButton);
        $progressButton.attr("class", $button.attr("class"));
        $progressButton.addClass("show-progress");
        $progressButton.attr("style", $button.attr("style"));
        $progressButton.text(options.text || "Loading...");
        var positionStyles = {
            "margin-bottom": 0,
            "margin-left": 0,
            "margin-right": 0,
            "margin-top": 0,
            "bottom": 0,
            "left": 0,
            "right": 0,
            "top": 0,
            "float": "none",
            "position": "relative",
            "transform": "none"
        };
        $button.hide();
        for (var rule in positionStyles) {
            $progressContainer.css(rule, $button.css(rule));
            $progressButton.css(rule, positionStyles[rule])
        }
        $button.show();
        var buttonWidth = $button.outerWidth(),
            buttonDisplay = $button.css("display");
        if (options.autosize) $progressContainer.height($button.outerHeight());
        $progressContainer.insertBefore($button).show();
        $button.hide();
        $progressContainer.css("display", "inline-block");
        if (options.autosize) {
            buttonWidth = Math.max(buttonWidth, $progressContainer.outerWidth());
            $progressContainer.css("width", buttonWidth + "px").css("display", buttonDisplay)
        }
        $button.data("in-progress", true)
    })
};
jQuery.fn.HideButtonProgress = function () {
    return this.each(function () {
        var $button = $(this);
        if (!$button.data("in-progress")) return;
        $button.prev().remove();
        $button.show();
        $button.data("in-progress", false)
    })
};
jQuery.QueryString = function (paramsArray) {
    var data = {};
    for (var i = 0; i < paramsArray.length; i++) {
        var pieces = paramsArray[i].split("=");
        data[pieces[0]] = pieces.length > 1 ? decodeURIComponent(pieces[1].replace(/\+/g, " ")) : true
    }
    return data
}(window.location.search.substring(1).split("&"));
ko.isObservableArray = ko.isObservableArray || function (obj) {
    return ko.isObservable(obj) && !(obj.destroyAll === undefined)
};
ko.bindingHelpers = ko.bindingHelpers || {};
ko.bindingHelpers.extendAllBindings = function (allBindings, newBindings) {
    var bindings = function () {
        return $.extend({}, allBindings(), newBindings)
    };
    bindings.get = function (name) {
        var data = bindings();
        return name in data ? data[name] : null
    };
    return bindings
};
ko.bindingHandlers.buttonProgress = {
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        if (ko.unwrap(valueAccessor())) {
            var options = $.extend({}, allBindings.get("buttonProgressOptions"));
            options.text = allBindings.get("buttonProgressText") || options.text;
            $(element).ShowButtonProgress(options)
        } else $(element).HideButtonProgress()
    }
};
ko.bindingHandlers.masked = function () {
    var EvaluateMask = function (element, allBindings) {
        var mask = {
            format: "",
            options: {}
        };
        var preset = allBindings().maskedPreset;
        switch (preset) {
            case "phone":
                mask.format = "(000) 000-0000";
                break;
            case "phone-display":
                mask.format = "000.000.0000";
                break;
            case "zipcode":
                mask.format = "00000";
                break;
            case "date":
                mask.format = "00/00/0000";
                mask.options = {
                    placeholder: "MM/DD/YYYY"
                };
                break;
            case "card-number":
                mask.format = "0000000000000000";
                break;
            case "card-expire":
                mask.format = "00/00";
                break;
            case "card-security":
                mask.format =
                    "0000";
                break;
            case "bank-routing-number":
                mask.format = "Z00000000";
                mask.options = {
                    translation: {
                        "Z": {
                            pattern: /[012346789]/
                        }
                    }
                };
                break
        }
        mask.format = allBindings().maskedFormat || mask.format;
        $.extend(mask.options, allBindings().maskedOptions);
        return mask
    };
    var DetectElementType = function (element) {
        return $(element).is("input, textarea") ? "value" : "text"
    };
    return {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var elementType = DetectElementType(element);
            var mask = EvaluateMask(element, allBindings);
            if (elementType == "value") {
                var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
                    valueUpdate: "afterkeydown"
                });
                ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings, viewModel, bindingContext);
                $(element).mask(mask.format, mask.options)
            } else {
                var $element = $(element).text(valueAccessor()());
                $element.mask(mask.format, mask.options)
            }
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var elementType = DetectElementType(element);
            var mask = EvaluateMask(element,
                allBindings);
            if (elementType == "value") {
                var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
                    valueUpdate: "afterkeydown"
                });
                ko.bindingHandlers.value.update(element, valueAccessor, newAllBindings, viewModel, bindingContext);
                $(element).mask(mask.format, mask.options);
                valueAccessor()($(element).val())
            } else {
                var masked = $(element).masked(valueAccessor()());
                $(element).text(masked)
            }
        }
    }
}();
ko.bindingHandlers.submitForm = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).on("keydown", function (e) {
            var submitFormOn = allBindings.get("submitFormOn");
            if (e.keyCode === 13 && (!submitFormOn || typeof submitFormOn == "function" && submitFormOn())) {
                var params = [viewModel, e];
                var fn = valueAccessor();
                if (typeof fn == "function") fn.apply(bindingContext, params)
            }
        })
    }
};

function CallDWR(path, model, callback) {
    path = CallDWR._CleanPathString(path);
    var blocked = CallDWR.OnBeforeCallbacks.some(function (onBeforeCallback) {
        return onBeforeCallback(path, model) === false
    });
    if (blocked) return;
    var HandleCallbackData = function (data) {
        if (typeof callback == "function") callback(data);
        CallDWR.OnAfterCallbacks.forEach(function (onAfterCallback) {
            onAfterCallback(path, model, data)
        })
    };
    $.ajax({
        type: "POST",
        url: "/funnel/dwr/jsonp/" + path,
        data: model ? {
            jsonData: JSON.stringify(model)
        } : {},
        dataType: "json",
        timeout: 2 *
            60 * 1E4,
        complete: function (data, textStatus) {
            var callbackData;
            try {
                var escapedText = JSON.parse(data.responseText);
                if (escapedText.error) callbackData = {
                    error: escapedText.error.message,
                    status: textStatus
                };
                else callbackData = {
                    model: JSON.parse(escapedText.reply)
                }
            } catch (e) {
                callbackData = {
                    error: "An error has occurred."
                };
                console.log("Error from server request: " + e.message)
            }
            HandleCallbackData(callbackData)
        }
    })
}
CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];
CallDWR._CleanPathString = function (path) {
    return path.replace(/^\/*|\/*$/g, "")
};

function OnBeforeCallDWR(callback) {
    CallDWR.OnBeforeCallbacks.push(callback)
}

function OnAfterCallDWR(callback) {
    CallDWR.OnAfterCallbacks.push(callback)
}

function GetInterestArea() {
    var interestArea = "PEST CONTROL";
    if (GetURLParameter("interestArea") || caniuse.sessionStorage) {
        var newArea = GetURLParameter("interestArea") || sessionStorage.getItem("interestArea");
        if (newArea) interestArea = newArea.toUpperCase()
    }
    return interestArea
}

function IsFreeInspectionInterest(interestArea) {
    switch (interestArea) {
        case "PEST CONTROL":
        case "ANTS":
        case "CLOTHES MOTHS":
        case "CRICKETS":
        case "FLEAS":
        case "SCORPIONS":
        case "SILVERFISH":
        case "SPIDERS":
        case "TICKS":
        case "PAPER WASPS":
        case "MOSQUITOES":
            return false;
            break;
        case "ARMADILLOS":
        case "ATTIC INSULATION":
        case "BED BUG":
        case "CRAWL SPACE":
        case "MICE":
        case "RACCOONS":
        case "RODENTS":
        case "SQUIRRELS":
        case "TERMITES":
        case "WILDLIFE":
        case "OTHER SERVICES":
            return true;
            break
    }
}

function CommonViewModel() {
    var self = this;
    self.model = ko.observable();
    self.modelLoaded = ko.observable(false);
    self.error = ko.observable();
    self.submitting = ko.observable(false);
    self.ResolveScope = function (scope) {
        if (scope === undefined) scope = self.model;
        if (ko.isObservable(scope)) scope = scope();
        return scope
    };
    self.ResolveFieldName = function (field, scope) {
        var valueAccessor = self.ResolveScope(scope);
        if (!valueAccessor) return null;
        var pieces = field.split(".");
        for (var i = 0; i < pieces.length; i++) {
            if (!valueAccessor.hasOwnProperty(pieces[i])) return null;
            valueAccessor = ko.unwrap(valueAccessor[pieces[i]])
        }
        return valueAccessor
    };
    self.RemoveAllMessages = function (scope) {
        scope = self.ResolveScope(scope);
        if (!scope) return;
        if (ko.isObservableArray(scope.errorMessages)) scope.errorMessages.removeAll();
        if (ko.isObservableArray(scope.fieldValidationErrors)) scope.fieldValidationErrors.removeAll();
        if (ko.isObservableArray(scope.informationalMessages)) scope.informationalMessages.removeAll()
    };
    self.HasFieldError = function (field, scope) {
        return self.GetFieldError(field, scope) !=
            ""
    };
    self.GetFieldError = function (field, scope) {
        scope = self.ResolveScope(scope);
        if (!field || !scope) return "";
        var fieldError = scope.fieldValidationErrors().find(function (fieldError) {
            return ko.unwrap(fieldError.field) == field
        });
        return fieldError ? ko.unwrap(fieldError.errorMessage) : ""
    };
    self.AddFieldError = function (field, message, scope) {
        scope = self.ResolveScope(scope);
        if (field && message && scope && ko.isObservableArray(scope.fieldValidationErrors)) scope.fieldValidationErrors.push(ko.mapping.fromJS({
            field: field,
            errorMessage: message
        }))
    };
    self.RemoveFieldError = function (field, scope) {
        scope = self.ResolveScope(scope);
        if (scope && ko.isObservableArray(scope.fieldValidationErrors)) scope.fieldValidationErrors.remove(function (fieldError) {
            return ko.unwrap(fieldError.field) == field
        })
    };
    self.Validator = function () {
        if (this == self) return new self.Validator;
        var validator = this;
        validator.fields = {};
        validator.rules = {
            required: function (value, overrideMessage) {
                return {
                    hasError: !value,
                    message: overrideMessage || "This field is required."
                }
            },
            validEmail: function (value,
                overrideMessage) {
                return {
                    hasError: !IsValidEmailAddress(value),
                    message: overrideMessage || "A valid email address is required."
                }
            },
            validRoutingNumber: function (value, overrideMessage) {
                return {
                    hasError: !IsValidRoutingNumber(value),
                    message: overrideMessage || "A valid routing number is required."
                }
            }
        };
        validator.add = function (field, rule, options) {
            if (typeof rule != "function") {
                if (!validator.rules.hasOwnProperty(rule) || typeof validator.rules[rule] != "function") throw "Invalid rule provided.";
                rule = validator.rules[rule]
            }
            options =
                $.extend({
                    message: ""
                }, options);
            if (typeof options.valueAccessor != "function") options.valueAccessor = function () {
                return self.ResolveFieldName(field, options.scope)
            };
            validator.fields[field] = {
                options: options,
                validate: function () {
                    var value = options.valueAccessor();
                    var result = rule(value, options.message);
                    if (result.hasError) self.AddFieldError(field, result.message, options.scope);
                    else self.RemoveFieldError(field, options.scope);
                    return !result.hasError
                }
            };
            return validator
        };
        validator.test = function (field) {
            if (!validator.fields.hasOwnProperty(field)) throw "Invalid field provided.";
            return validator.fields[field].validate()
        };
        validator.onEvent = function (field) {
            return function () {
                if (validator.fields.hasOwnProperty(field)) validator.fields[field].validate()
            }
        }
    };
    self.AddressValidator = function (options) {
        if (this == self) return new self.AddressValidator(options);
        var validator = this;
        validator.isChoosing = ko.observable(false);
        validator.isValidating = ko.observable(false);
        validator.address = ko.observable();
        validator.addresses = ko.observableArray();
        validator.selectedAddress = ko.observable();
        validator.lastValidatedAddress =
            null;
        validator.cache = {};
        validator.ziptasticFail = ko.observable(false);
        var onKeepAddress;
        var onUseSelectedAddress;
        validator.ValidateAddress = function (address, options) {
            if (!options) throw "Options are required.";
            if (typeof options.onKeepAddress != "function") throw "onKeepAddress callback is required.";
            if (typeof options.onUseSelectedAddress != "function") throw "onUseSelectedAddress callback is required.";
            if (typeof options.onValidate != "function") throw "onValidate callback is required.";
            onKeepAddress = options.onKeepAddress;
            onUseSelectedAddress = options.onUseSelectedAddress;
            validator.isValidating(true);
            var json = {
                address1: StringToLower(address.address1),
                address2: StringToLower(address.address2),
                city: StringToLower(address.city),
                state: StringToLower(address.state),
                zipcode: StringToLower(address.zipCode)
            };
            var key = JSON.stringify(json);
            var HandleResult = function (addresses) {
                validator.isValidating(false);
                var hasCorrections = false;
                if (addresses && addresses.length) {
                    validator.address(address);
                    validator.addresses(addresses);
                    validator.selectedAddress(addresses[0]);
                    validator.isChoosing(true);
                    hasCorrections = true
                }
                validator.lastValidatedAddress = $.extend({}, address);
                options.onValidate(hasCorrections)
            };
            if (validator.cache.hasOwnProperty(key)) HandleResult(validator.cache[key].slice());
            else CallDWR("MyAccountTMXUIUtils/validateAddress", {
                address: json
            }, function (data) {
                var addresses;
                if (data.model && data.model.alternativeAddresses && data.model.alternativeAddresses.length) {
                    addresses = data.model.alternativeAddresses = data.model.alternativeAddresses.filter(function (alternativeAddress) {
                        return alternativeAddress &&
                            alternativeAddress.address1 && alternativeAddress.address1 != "null null" && alternativeAddress.city && alternativeAddress.city != "null" && alternativeAddress.state && alternativeAddress.state != "null" && alternativeAddress.zipcode && alternativeAddress.zipcode != "null"
                    });
                    addresses = addresses.map(function (address) {
                        return {
                            address1: address.address1,
                            address2: address.address2,
                            city: address.city,
                            state: address.state,
                            zipCode: address.zipcode
                        }
                    });
                    validator.cache[key] = addresses.slice()
                }
                HandleResult(addresses)
            })
        };
        validator.Reset =
            function () {
                validator.isChoosing(false);
                validator.isValidating(false);
                validator.addresses.removeAll();
                validator.selectedAddress(null);
                validator.cache = {}
            };
        validator.KeepAddress = function () {
            validator.isChoosing(false);
            if (typeof onKeepAddress == "function") {
                onKeepAddress();
                onKeepAddress = undefined
            }
        };
        validator.UseSelectedAddress = function () {
            if (!validator.selectedAddress()) throw "No address is selected.";
            var address = validator.selectedAddress();
            validator.isChoosing(false);
            validator.lastValidatedAddress = address;
            if (typeof onUseSelectedAddress == "function") {
                onUseSelectedAddress();
                onUseSelectedAddress = undefined
            }
        };
        validator.Cancel = function () {
            validator.isChoosing(false)
        };
        validator.IsLastValidatedAddress = function (address) {
            return address && validator.lastValidatedAddress && StringToLower(address.address1) == StringToLower(validator.lastValidatedAddress.address1) && StringToLower(address.address2) == StringToLower(validator.lastValidatedAddress.address2) && StringToLower(address.city) == StringToLower(validator.lastValidatedAddress.city) &&
                StringToLower(address.state) == StringToLower(validator.lastValidatedAddress.state) && StringToLower(address.zipCode) == StringToLower(validator.lastValidatedAddress.zipCode)
        }
    }
}

function FunnelViewModel() {
    var self = this;
    CommonViewModel.call(self);
    var STEP_ADDRESS = self.STEP_ADDRESS = "step_address";
    var STEP_PRODUCTS = self.STEP_PRODUCTS = "step_products";
    var STEP_SCHEDULE = self.STEP_SCHEDULE = "step_schedule";
    var STEP_PAYMENT = self.STEP_PAYMENT = "step_payment";
    var STEP_CONFIRMATION = self.STEP_CONFIRMATION = "step_confirmation";
    var stepTextMap = {};
    stepTextMap[STEP_ADDRESS] = "Address";
    stepTextMap[STEP_PRODUCTS] = "Pricing";
    stepTextMap[STEP_SCHEDULE] = "Schedule";
    stepTextMap[STEP_PAYMENT] = "Pay";
    stepTextMap[STEP_CONFIRMATION] = "Confirmation";
    var defaultStepOrder = [STEP_ADDRESS, STEP_PRODUCTS, STEP_SCHEDULE, STEP_PAYMENT, STEP_CONFIRMATION];
    var isShowStep = {};
    defaultStepOrder.forEach(function (step) {
        isShowStep[step] = ko.observable(true)
    });
    self.IsShowStep = function (step) {
        return step in isShowStep ? isShowStep[step]() : true
    };
    if (caniuse.sessionStorage) {
        var isShowStepJSON = sessionStorage.getItem("isShowStep");
        if (isShowStepJSON) {
            var json, parsed = false;
            try {
                json = JSON.parse(isShowStepJSON);
                parsed = true
            } catch (e) {
                console.log("An invalid JSON string was set in isShowStep sessionStorage.")
            }
            if (json) defaultStepOrder.forEach(function (step) {
                isShowStep[step](!!json[step])
            });
            else if (parsed) console.log("A non-object was set in isShowStep sessionStorage.")
        }
    }
    var stepOrder = self.stepOrder = ko.computed(function () {
        return defaultStepOrder.filter(self.IsShowStep)
    });
    var currentStepObservable = ko.observable(defaultStepOrder[0]);
    var stepHistory = [];
    var stepCache = {};
    defaultStepOrder.forEach(function (step) {
        stepCache[step] = ko.observable(null)
    });
    self.submittingStep = ko.observable();
    if (caniuse.sessionStorage) {
        var stepCacheJSON = sessionStorage.getItem("stepCache");
        if (stepCacheJSON) {
            var json,
                parsed = false;
            try {
                json = JSON.parse(stepCacheJSON);
                parsed = true
            } catch (e$0) {
                console.log("An invalid JSON string was set in stepCache sessionStorage.")
            }
            if (json) defaultStepOrder.forEach(function (step) {
                if (json[step]) stepCache[step](json[step])
            });
            else if (parsed) console.log("A non-object was set in stepCache sessionStorage.")
        }
    }

    function ClearStepCache(options) {
        options = $.extend({
            after: "",
            until: ""
        }, options);
        var startIndex = options.after && defaultStepOrder.indexOf(options.after) + 1 || 0;
        var stopIndex = options.until ?
            defaultStepOrder.indexOf(options.until) : -1;
        if (stopIndex == -1) stopIndex = defaultStepOrder.length;
        defaultStepOrder.slice(startIndex, stopIndex).forEach(function (step) {
            stepCache[step](null)
        })
    }
    self.currentStep = ko.computed(function () {
        return currentStepObservable()
    });
    var currentStepIndex = ko.computed(function () {
        return defaultStepOrder.indexOf(self.currentStep())
    });
    self.IsAffiliatePath = ko.observable(false);
    self.IsFreeInspectionPath = ko.computed(function () {
        if (!self.modelLoaded()) return false;
        return IsFreeInspectionInterest(self.model().interestArea())
    });
    self.squareFootageOptions = ko.observableArray([{
        optionText: "Up to 3999",
        value: 3999
    }, {
        optionText: "4,000-5,999",
        value: 5999
    }, {
        optionText: "6,000-7,999",
        value: 7999
    }, {
        optionText: "Over 8,000",
        value: 8E3
    }]);
    self.noDatesChosen = ko.observable(false);
    self.scheduleSubmitLegalText = ko.computed(function () {
        var noDateSelectedText = "By clicking above, you agree to have a representative from your local branch contact you to schedule an appointment",
            dateSelectedText = "By clicking above, you understand that you are scheduling an appointment";
        return self.noDatesChosen() ? noDateSelectedText : dateSelectedText
    });
    self.billingAddressSameAsServiceAddress = ko.observable(true);
    self.cardExpirationMonths = [];
    self.cardExpirationYears = [];
    for (var i = 1; i <= 12; i++) self.cardExpirationMonths.push({
        label: GetMonthName(i),
        value: i
    });
    for (var i = 0, y = (new Date).getFullYear(); i < 10; i++) self.cardExpirationYears.push(y + i);
    self.validator = new self.Validator;
    self.validator.add("customer.serviceProperty.address1", "required");
    self.validator.add("customer.serviceProperty.zipCode",
        "required");
    self.validator.add("customer.contact.email", "validEmail");
    self.validator.add("customer.contact.phone", "required");
    self.validator.add("customer.firstName", "required");
    self.validator.add("customer.lastName", "required");
    self.validator.add("payment.cardNumber", "required");
    self.validator.add("payment.cardExpire", "required");
    self.validator.add("payment.bankAccountNumber", "required");
    self.validator.add("payment.bankRoutingNumber", "validRoutingNumber");
    self.validator.add("customer.billingProperty.address1",
        "required");
    self.validator.add("customer.billingProperty.city", "required");
    self.validator.add("customer.billingProperty.state", "required");
    self.validator.add("customer.billingProperty.zipCode", "required");
    self.addressValidator = new self.AddressValidator;

    function GetStepURL(step) {
        var url = "/buyonline/",
            page = "";
        switch (step) {
            case STEP_ADDRESS:
                page = "address";
                break;
            case STEP_PRODUCTS:
                page = "pricing";
                break;
            case STEP_SCHEDULE:
                page = "schedule";
                break;
            case STEP_PAYMENT:
                page = "payment";
                break;
            case STEP_CONFIRMATION:
                page =
                    "confirmation";
                break
        }
        return url + page + window.location.search + window.location.hash
    }

    function GetStepName(step) {
        var url = GetStepURL(step);
        return url.substr(url.lastIndexOf("/") + 1).charAt(0).toUpperCase() + url.substr(url.lastIndexOf("/") + 1).slice(1)
    }
    self.OnHistoryPop = function () {
        if (stepHistory.length) {
            var step = stepHistory.pop();
            if (step in stepCache && stepCache[step]()) UpdateModel(stepCache[step](), true);
            else {
                var availableSteps = defaultStepOrder.filter(function (step) {
                    return stepCache[step]()
                });
                if (!availableSteps.length) {
                    window.location =
                        GetStepURL(defaultStepOrder[0]);
                    window.location.reload();
                    return
                }
                UpdateModel(stepCache[availableSteps.pop()](), true)
            }
            if (g_isHistorySupported) history.pushState(stepHistory, "", GetStepURL(self.currentStep()))
        } else if (g_isHistorySupported);
    };

    function UpdateSessionStorage(options) {
        options = $.extend({
            model: GetModelJSON(),
            stepCache: stepCache,
            isShowStep: isShowStep
        }, options);
        if (caniuse.sessionStorage) {
            sessionStorage.setItem("funnelModel", JSON.stringify(ko.mapping.toJS(options.model)));
            sessionStorage.setItem("stepCache",
                JSON.stringify(ko.mapping.toJS(options.stepCache)));
            sessionStorage.setItem("isShowStep", JSON.stringify(ko.mapping.toJS(options.isShowStep)))
        }
    }
    var GA_CATEGORY_FUNNEL = "TMX Funnel";

    function FireFunnelGAEvent(category, action, label, value) {
        category = typeof category == "string" && category || GA_CATEGORY_FUNNEL;
        FireGAEvent(category, action, label, value);
        if (self.model() && self.model().tiered()) category += ": Tiered";
        else category += ": Standard";
        FireGAEvent(category, action, label, value)
    }

    function FireAffiliateGAEvent(affiliate,
        label) {
        if (!affiliate.id || !affiliate.name) console.log("[FireAffiliateGAEvent] Affiliate name and id are required!");
        var action = "[" + affiliate.id + "] " + affiliate.name;
        FireGAEvent("Affiliate Tracking", action, label, "")
    }

    function FireFreeInspectionConversion() {
        var label = "";
        switch (self.model().interestArea()) {
            case "ATTIC INSULATION":
                label = "Attic Insulation";
                break;
            case "BED BUGS":
                label = "Bed Bugs";
                break;
            case "CRAWL SPACE":
                label = "Crawl Space";
                break;
            case "RODENTS":
            case "WILDLIFE":
                label = "Wildlife";
                break;
            case "TERMITES":
                label =
                    "Termite";
                break
        }
        FireFunnelGAEvent("Inspection Schedule", "Form Submit Success", label, "");
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Free Inspection", "Success: " + label, "")
    }

    function GetModelJSON() {
        var json = ko.mapping.toJS(self.model) || {};
        if (json) json.currentStep = self.currentStep();
        return json
    }

    function UpdateModel(model, ignoreHistory) {
        var passthroughDataJSON = JSON.parse(sessionStorage.getItem("passthroughDataJSON"));
        model = $.extend(GetModelJSON(), model);
        if (model.redirectRequest && model.redirectURL) {
            window.location =
                model.redirectURL;
            return
        }
        if (!model.currentStep) model.currentStep = defaultStepOrder[0];
        var isChangingSteps = model.currentStep != self.currentStep();
        if (g_isHistorySupported && !ignoreHistory) {
            if (isChangingSteps || !stepHistory.length) stepHistory.push(self.currentStep());
            stepCache[model.currentStep]($.extend(true, {}, model));
            history.replaceState(stepHistory, "", GetStepURL(model.currentStep))
        }
        if (model.customer && !model.customer.serviceProperty && model.customer.property) model.customer.serviceProperty = model.customer.property;
        if (!model.availableProducts) model.availableProducts = [];
        if (!self.IsAffiliatePath() && !IsObjectEmpty(model.commercialAffiliate)) self.IsAffiliatePath(true);
        switch (model.currentStep) {
            case STEP_ADDRESS:
                model.interestArea = GetInterestArea();
                model.showUpsell = false;
                if (IsFreeInspectionInterest(model.interestArea)) {
                    isShowStep[STEP_PRODUCTS](false);
                    isShowStep[STEP_PAYMENT](false)
                }
                try {
                    if (caniuse.sessionStorage)
                        if (passthroughDataJSON) {
                            model.customer.serviceProperty.address1 = passthroughDataJSON.address1;
                            model.customer.serviceProperty.address2 =
                                passthroughDataJSON.address2;
                            model.customer.serviceProperty.city = passthroughDataJSON.city;
                            model.customer.serviceProperty.state = passthroughDataJSON.state;
                            model.customer.serviceProperty.zipCode = passthroughDataJSON.zipCode;
                            model.customer.firstName = passthroughDataJSON.firstName;
                            model.customer.lastName = passthroughDataJSON.lastName;
                            model.customer.contact.email = passthroughDataJSON.email;
                            model.customer.contact.phone = passthroughDataJSON.phone;
                            model.entrySource.creative = passthroughDataJSON.creative;
                            model.entrySource.partnerCID =
                                passthroughDataJSON.partnerCID;
                            model.entrySource.source = passthroughDataJSON.source;
                            model.entrySource.campaign = passthroughDataJSON.campaign;
                            model.salesAgent = passthroughDataJSON.salesAgent;
                            model.entrySource.offerCode = passthroughDataJSON.offerCode;
                            model.entrySource.mkwid = passthroughDataJSON.mkwid;
                            model.entrySource.gclid = passthroughDataJSON.gclid;
                            model.entrySource.keyword = passthroughDataJSON.keyword;
                            model.entrySource.adType = passthroughDataJSON.adType;
                            model.entrySource.adName = passthroughDataJSON.adName;
                            model.entrySource.content = passthroughDataJSON.content
                        }
                } catch (e$1) {
                    console.log(e$1)
                }
                break;
            case STEP_PRODUCTS:
                if (model.productItemMap && !model.availableProducts.length) {
                    for (var productId in model.productItemMap) {
                        var product = model.productItemMap[productId];
                        if (!product.templateCode || !product.productCode) continue;
                        product.isDisabled = false;
                        product.isALaCarte = self.IsALaCarteProduct(product, model.tiered, model.interestArea);
                        product.isTiered = self.IsTieredProduct(product);
                        product.productCost = parseFloat(product.productCost) ||
                            0;
                        product.discount = parseFloat(product.discount) || 0;
                        product.taxes = parseFloat(product.taxes) || 0;
                        product.startUpCostTotal = parseFloat(product.startUpCostTotal) || product.productCost - product.discount + product.taxes;
                        product.frequencyCost = parseFloat(product.frequencyCost) || 0;
                        if (product.discount > 0) {
                            product.startUpCostTotalWithoutDiscount = product.productCost;
                            if (product.taxes) {
                                var taxRate = product.taxes / (product.productCost - product.discount);
                                product.startUpCostTotalWithoutDiscount += Math.ceil(product.productCost *
                                    taxRate * 100) / 100
                            }
                        } else product.startUpCostTotalWithoutDiscount = product.startUpCostTotal;
                        if (!product.paymentFrequency) product.paymentFrequency = "";
                        if (!product.frequencyCost) product.frequencyCost = 0;
                        product.paymentFrequencyDisplay = self.GetPaymentFrequencyDisplay(product.paymentFrequency);
                        model.availableProducts.push(product)
                    }
                    if (model.availableProducts.length) {
                        var targetPestItem = model.availableProducts.find(function (product) {
                            return product.productCode === model.targetPest
                        });
                        if (targetPestItem) {
                            model.availableProducts =
                                model.availableProducts.filter(function (product) {
                                    return product.productCode !== model.targetPest
                                });
                            model.availableProducts.unshift(targetPestItem)
                        }
                        model.productIdCart = [];
                        model.availableProducts.forEach(function (product) {
                            if (product.itemInCart) model.productIdCart.push(product.productId)
                        })
                    }
                }
                model.customer.billingProperty = model.customer.serviceProperty;
                break;
            case STEP_SCHEDULE:
                var foundSelectedDate = false;
                model.availableDates = [];
                if (model.serviceSchedule && model.serviceSchedule.length)
                    for (var i = 0; i < model.serviceSchedule.length; i++) {
                        var dateObj = {
                                dateString: "",
                                availableTimes: []
                            },
                            loopSchedule = model.serviceSchedule[i];
                        dateObj.dateString = loopSchedule.date;
                        for (var t = 0; t < loopSchedule.serviceTimeSlots.length; t++) dateObj.availableTimes.push(loopSchedule.serviceTimeSlots[t].schFrmTime);
                        model.availableDates.push(dateObj)
                    } else self.noDatesChosen(true);
                model.availableTimes = [];
                if (self.noDatesChosen()) {
                    model.selectedDate = "NONE";
                    model.timeSlotSelectedKey = "NONE";
                    model.selectedTime = "";
                    foundSelectedDate = true
                }
                if (model.availableDates && model.availableDates.length) model.availableDates.forEach(function (availableDate) {
                    if (!foundSelectedDate &&
                        availableDate.dateString == model.selectedDate) {
                        foundSelectedDate = true;
                        if (!availableDate.availableTimes || !availableDate.availableTimes.length || availableDate.availableTimes.indexOf(model.selectedTime) == -1) model.selectedTime = ""
                    }
                    if (availableDate.availableTimes && availableDate.availableTimes.length) availableDate.availableTimes.forEach(function (time) {
                        if (model.availableTimes.indexOf(time) == -1) model.availableTimes.push(time)
                    })
                });
                model.availableDates.sort(function (a, b) {
                    return new Date(a.dateString) - new Date(b.dateString)
                });
                model.availableTimes.sort(function (a, b) {
                    return parseInt(a.substr(0, 2)) - parseInt(b.substr(0, 2))
                });
                if (!foundSelectedDate) {
                    model.selectedDate = "";
                    model.selectedTime = ""
                }
                break;
            case STEP_PAYMENT:
                if (model.openSlotSelected) {
                    model.selectedDate = model.openSlotSelected.schDate;
                    model.selectedTime = model.openSlotSelected.schFrmTime
                } else {
                    model.selectedDate = "NONE";
                    model.selectedTime = ""
                }
                if (!model.payment.paymentMethod) {
                    model.payment.paymentMethod = "card";
                    model.payment.cardType = ""
                }
                break;
            case STEP_CONFIRMATION:
                if (model.openSlotSelected) {
                    model.selectedDate =
                        model.openSlotSelected.schDate;
                    model.selectedTime = model.openSlotSelected.schFrmTime
                } else {
                    model.selectedDate = "NONE";
                    model.selectedTime = ""
                }
                break
        }
        var productContentMap = {};
        model.availableProducts.forEach(function (product) {
            if (product.content) productContentMap[product.productId] = product.content;
            product.content = null;
            product.contentFailed = false
        });
        currentStepObservable("");
        self.model(ko.mapping.fromJS(model));
        self.modelLoaded(true);
        currentStepObservable(model.currentStep);
        switch (model.currentStep) {
            case STEP_ADDRESS:
                if (_ENABLE_PROPERTY_INFORMATION_TOGGLE) $(".sqft-block").hide();
                else $(".sqft-block").show();
                break;
            case STEP_PRODUCTS:
                break;
            case STEP_SCHEDULE:
                self.model().selectedDate.subscribe(function (value) {
                    self.model().selectedTime("");
                    if (value === "NONE") self.model().timeSlotSelectedKey(value)
                });
                self.model().selectedTime.subscribe(function (value) {
                    var dateMatch = ko.utils.arrayFirst(self.model().serviceSchedule(), function (item) {
                        return self.model().selectedDate() === item.date()
                    });
                    if (dateMatch) {
                        var timeMatch = ko.utils.arrayFirst(dateMatch.serviceTimeSlots(), function (item) {
                            return self.model().selectedTime() ===
                                item.schFrmTime()
                        });
                        if (timeMatch) self.model().timeSlotSelectedKey(timeMatch.schSlotKey())
                    }
                });
                self.noDatesChosen.subscribe(function (value) {
                    if (value === true) {
                        self.model().selectedDate("NONE");
                        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Links and Buttons", "Checkbox: None of these dates works", "")
                    } else self.model().selectedDate("")
                });
                break;
            case STEP_PAYMENT:
                self.model().payment.cardNumber.subscribe(function (value) {
                    var cardType = GetCreditCardType(value);
                    if (cardType) self.model().payment.cardType(cardType.value)
                });
                if (self.model().payment.cardNumber()) self.model().payment.cardNumber.valueHasMutated();
                self.billingAddressSameAsServiceAddress.subscribe(function (value) {
                    if (value === false) {
                        var billingZip = self.model().customer.billingProperty.zipCode();
                        self.model().customer.billingProperty.zip9Code(billingZip);
                        self.model().customer.billingProperty.lotSize(null);
                        self.model().customer.billingProperty.squareFootage(null);
                        self.model().customer.billingProperty.addressVerified(false)
                    }
                });
                break;
            case STEP_CONFIRMATION:
                if (caniuse.sessionStorage) sessionStorage.clear();
                break
        }
        self.model().availableProducts().forEach(function (product) {
            if (product.productId() in productContentMap) product.content(productContentMap[product.productId()]);
            else self.GetProductContentFromEndeca(product.productCode(), product.templateCode(), function (json) {
                if (!json || !json.content || json.content.error) product.contentFailed(true);
                else {
                    var unmappedProduct = ko.mapping.toJS(product);
                    for (var c in json.content) {
                        if (!json.content.hasOwnProperty(c)) continue;
                        if (IsObject(json.content[c]))
                            for (var item in json.content[c]) {
                                if (!json.content[c].hasOwnProperty(item)) continue;
                                json.content[c][item] = self.ReplacePricingTemplateKeys(json.content[c][item], unmappedProduct)
                            } else json.content[c] = self.ReplacePricingTemplateKeys(json.content[c], unmappedProduct)
                    }
                    product.content(json.content);
                    if (caniuse.sessionStorage && self.currentStep() !== STEP_CONFIRMATION) {
                        for (var step in stepCache) {
                            var json = stepCache[step]();
                            if (json && json.availableProducts) json.availableProducts.some(function (jsonProduct) {
                                if (jsonProduct.productId == product.productId()) {
                                    jsonProduct.content = product.content();
                                    return true
                                }
                            })
                        }
                        UpdateSessionStorage()
                    }
                }
            })
        });
        if (self.currentStep() !== STEP_CONFIRMATION && caniuse.sessionStorage) UpdateSessionStorage();
        if (self.model().actionList && self.model().actionList() && self.model().actionList().length)
            for (var a = 0; a < self.model().actionList().length; a++) {
                var action = self.model().actionList()[a];
                switch (action) {
                    case "INVALID_EMAIL":
                        new CommonModal({
                            title: "Email address verification error",
                            body: "<p>Sorry, we were unable to verify your email address <strong>" + self.model().customer.contact.email() + "</strong>. If this is the correct email address, please continue as is to resubmit, otherwise you may edit your email address and try again.</p>",
                            confirm: true,
                            btnCancelText: "Edit email",
                            btnConfirmText: "Continue as is"
                        }, function () {
                            self.model().customer.contact.emailVerified(true);
                            self.SubmitStep()
                        });
                        break;
                    case "INVALID_ADDRESS":
                        var addressline2, fullAddress, invalidType;
                        if (!self.model().customer.serviceProperty.addressVerified()) {
                            addressline2 = self.model().customer.serviceProperty.address2() != null ? self.model().customer.serviceProperty.address2() : "";
                            fullAddress = self.model().customer.serviceProperty.address1() + " " + addressline2 + " " + self.model().customer.serviceProperty.zipCode();
                            invalidType = "service"
                        } else if (!self.model().customer.billingProperty.addressVerified()) {
                            addressline2 = self.model().customer.billingProperty.address2() != null ? self.model().customer.billingProperty.address2() : "";
                            fullAddress = self.model().customer.billingProperty.address1() + " " + addressline2 + " " + self.model().customer.billingProperty.zipCode();
                            invalidType = "billing"
                        }
                        new CommonModal({
                            title: "Address verification error",
                            body: "<p>Sorry, we were unable to verify your " + invalidType + " address <strong>" + fullAddress +
                                "</strong>. If this is the correct address, please continue as is to resubmit, otherwise you may edit your address and try again.</p>",
                            confirm: true,
                            btnCancelText: "Edit address",
                            btnConfirmText: "Continue as is"
                        }, function () {
                            if (invalidType === "service") self.model().customer.serviceProperty.addressVerified(true);
                            else if (invalidType === "billing") self.model().customer.billingProperty.addressVerified(true);
                            self.SubmitStep()
                        });
                        self.model().actionList().pop();
                        break;
                    case "INVALID_PHONE":
                        break;
                    case "DISABLE_SUBMIT_ORDER":
                        if (self.model().currentStep() ==
                            STEP_PAYMENT) $(".cart .card-footer").hide();
                        else if (self.model().currentStep() == STEP_SCHEDULE) $("#submit-schedule-step").hide();
                        break
                }
            }
        if (isChangingSteps) {
            ScrollTo(0, "fast");
            try {
                utag.view({
                    ga_page_including_hash: window.location.pathname + window.location.search + window.location.hash
                })
            } catch (e$2) {
                console.log(e$2)
            }
        }
        if (passthroughDataJSON && model.currentStep === STEP_ADDRESS) {
            self.SubmitStep();
            sessionStorage.removeItem("passthroughDataJSON")
        }
    }
    self.HasAddressUnit = ko.computed(function (property) {
        if (self.modelLoaded() &&
            self.model()) {
            var _property = ko.unwrap(property);
            if (_property && ko.unwrap(_property.address2)) return true
        }
        return false
    });
    self.HasAvailableProducts = ko.computed(function () {
        return self.modelLoaded() && self.model() && self.model().availableProducts && self.model().availableProducts().length
    });
    self.IsMosquitoInSeason = ko.computed(function () {
        var inSeason = false;
        if (self.HasAvailableProducts() && self.model().interestArea() === "MOSQUITOES")
            for (var p = 0; p < self.model().availableProducts().length; p++) {
                var product = ko.mapping.toJS(self.model().availableProducts()[p]);
                if (product.productCode === "MOSQ") inSeason = true
            } else inSeason = true;
        return inSeason
    });
    self.ProductsHaveContent = ko.computed(function () {
        return self.HasAvailableProducts() && self.model().availableProducts().every(function (product) {
            return product.content()
        })
    });
    self.ProductsHaveFailedContent = ko.computed(function () {
        return self.HasAvailableProducts() && self.model().availableProducts().some(function (product) {
            return product.contentFailed()
        })
    });
    self.ProductHasDiscount = ko.computed(function () {
        if (!self.HasAvailableProducts()) return false;
        for (var p = 0; p < self.model().availableProducts().length; p++) {
            var product = ko.mapping.toJS(self.model().availableProducts()[p]);
            if (product.discount > 0) return true
        }
        return false
    });
    self.ProductHasRibbon = function (product) {
        if (!product) return false;
        var unmappedProduct = ko.mapping.toJS(product);
        return unmappedProduct && unmappedProduct.content.tiered.hasOwnProperty("ribbon")
    };
    self.IsALaCarteProduct = function (product, tiered, interestArea) {
        tiered = tiered || false;
        interestArea = interestArea || self.model().interestArea();
        if (!product) return false;
        if (interestArea === "MOSQUITOES" && product.productId === "10003") return false;
        if (product.productId === "10005" || product.productId === "10006" || product.productId === "10007") return false;
        if (product.productId === "10001") return false;
        return tiered
    };
    self.IsTieredProduct = function (product) {
        if (!product) return false;
        return product.productId === "10005" || product.productId === "10006" || product.productId === "10007"
    };
    self.GetPaymentFrequencyDisplay = function (paymentFrequencyCode, capitalized) {
        if (capitalized ===
            undefined) capitalized = true;
        switch (paymentFrequencyCode) {
            case "QTR":
                var paymentFrequencyDisplay = "quarterly";
                break;
            case "MTHLY":
                var paymentFrequencyDisplay = "monthly";
                break;
            case "BIMO":
                var paymentFrequencyDisplay = "bi-monthly";
                break;
            case "ONCE":
                var paymentFrequencyDisplay = "one-time";
                break;
            default:
                var paymentFrequencyDisplay = paymentFrequencyCode
        }
        if (capitalized) return paymentFrequencyDisplay.charAt(0).toUpperCase() + paymentFrequencyDisplay.slice(1);
        else return paymentFrequencyDisplay
    };
    self.GetPaymentFrequencyPerValue =
        function (paymentFrequencyCode) {
            switch (paymentFrequencyCode) {
                case "QTR":
                    var paymentFrequencyPerValue = "quarter";
                    break;
                case "MTHLY":
                    var paymentFrequencyPerValue = "month";
                    break;
                case "BIMO":
                    var paymentFrequencyPerValue = "two months";
                    break;
                default:
                    var paymentFrequencyPerValue = paymentFrequencyCode
            }
            return paymentFrequencyPerValue
        };
    self.GetProductContentFromEndeca = function (pc, tc, callback) {
        if (!pc || !tc || !callback) {
            console.log("[GetProductContentFromEndeca] Product code, Template code, and callback are required.");
            callback({});
            return
        }
        var data = {
            productCode: pc,
            templateCode: tc,
            folder: "Funnel"
        };
        GetEndecaContent(data, callback)
    };
    self.ReplacePricingTemplateKeys = function (content, product) {
        if (!content || typeof content !== "string" || !product) return;
        var templateKeys = {};
        templateKeys.initialPriceTag = "$" + FormatNumber(product.productCost - product.discount, 0);
        templateKeys.followUpPriceTag = "$" + FormatNumber(product.frequencyCost, 0);
        for (var templateKey in templateKeys) {
            var templateKeyStr = "{{" + templateKey + "}}";
            var templateReplacement =
                templateKeys[templateKey];
            content = content.replaceAll(templateKeyStr, templateReplacement)
        }
        return content
    };
    self.CartIsShowing = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return false;
        var ret = false;
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                break;
            case STEP_PRODUCTS:
                ret = self.IsFreeInspectionPath() ? self.ProductsHaveContent() && self.model().interestArea() === "TERMITES" : self.ProductsHaveContent();
                break;
            case STEP_SCHEDULE:
                ret = self.ProductsHaveContent() && self.HasProductsInCartWithPrice();
                break;
            case STEP_PAYMENT:
                ret = self.ProductsHaveContent();
                break;
            case STEP_CONFIRMATION:
                break
        }
        return ret
    });
    self.GetCartCTAText = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return "";
        switch (self.currentStep()) {
            case STEP_ADDRESS:
            case STEP_PRODUCTS:
            case STEP_SCHEDULE:
            case STEP_CONFIRMATION:
                var ret = "Next";
                break;
            case STEP_PAYMENT:
                var ret = "Submit";
                break
        }
        return ret
    });
    self.GetSubmittingCartCTAText = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return "";
        switch (self.currentStep()) {
            case STEP_PRODUCTS:
                var ret =
                    "Loading scheduling...";
                break;
            case STEP_SCHEDULE:
                var ret = self.submittingStep() == STEP_PRODUCTS ? "Loading scheduling..." : "Loading payment...";
                break;
            case STEP_PAYMENT:
                var ret = "Purchasing...";
                break;
            case STEP_ADDRESS:
            case STEP_CONFIRMATION:
                var ret = "";
                break
        }
        return ret
    });
    self.CanRemoveItemsFromCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return false;
        switch (self.currentStep()) {
            case STEP_ADDRESS:
            case STEP_SCHEDULE:
            case STEP_PAYMENT:
            case STEP_CONFIRMATION:
                var ret = false;
                break;
            case STEP_PRODUCTS:
                var ret =
                    self.submittingStep() !== STEP_PRODUCTS;
                break
        }
        return ret
    });
    self.HandleAddRemoveProductFromCart = function (item) {
        if (!item) return;
        var product = ko.mapping.toJS(item);
        if (self.IsProductInCart(product)) self.RemoveProductFromCart(product);
        else if (!self.ProductTriggeredCartRule(product)) self.AddProductToCart(product);
        ClearStepCache({
            after: self.currentStep()
        })
    };
    self.AddProductToCart = function (product) {
        if (!product && !product.productId) return;
        self.model().productIdCart.push(product.productId);
        if (self.IsFreeInspectionPath()) isShowStep[STEP_PAYMENT](true)
    };
    self.RemoveProductFromCart = function (product) {
        if (!product && !product.productId) return;
        self.model().productIdCart.remove(product.productId);
        if (self.IsFreeInspectionPath()) isShowStep[STEP_PAYMENT](false);
        self.HandleEnableProductsAfterRemove(product.productId)
    };
    self.IsTieredProductInCart = function () {
        return self.model().productIdCart().some(function (productId) {
            return productId === "10005" || productId === "10006" || productId === "10007"
        })
    };
    self.IsALaCarteProductInCart = function () {
        return self.productsInCart().some(function (product) {
            return product.isALaCarte()
        })
    };
    self.IsOnlyOnetimesInCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model() || !self.model().productItemMap) return false;
        return !self.model().productIdCart().some(function (productId) {
            return self.model().productItemMap[productId] && self.model().productItemMap[productId].paymentFrequency() !== "ONCE"
        })
    });
    self.IsPaidItemInCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model() || !self.model().productItemMap) return false;
        return self.model().productIdCart().some(function (productId) {
            return productId !==
                "10001"
        })
    });
    var _PRODUCT_RULESET = {
        10001: {
            "_name": "Free Termite Inspection",
            "disabled": [],
            "swap": ["10004", "10006", "10007"],
            "bundles": []
        },
        10002: {
            "_name": "Genpest products (quarterly)",
            "disabled": [],
            "swap": ["10005"],
            "bundles": []
        },
        10003: {
            "_name": "Mosquito (ATSB / Quick Guard)",
            "disabled": [],
            "swap": ["10009"],
            "bundles": [{
                    "productsInCart": ["10004", "10005"],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                },
                {
                    "productsInCart": ["10006"],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
                }]
        },
        10004: {
            "_name": "Termite one time",
            "disabled": [],
            "swap": ["10001"],
            "bundles": [{
                    "productsInCart": ["10003", "10005"],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                }, {
                    "productsInCart": ["10005"],
                    "productToUpgradeTo": "10006",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                },
                {
                    "productsInCart": ["10008"],
                    "productToUpgradeTo": "10006",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                }]
        },
        10005: {
            "_name": "Silver plan",
            "disabled": [],
            "swap": ["10002", "10006", "10007", "10008"],
            "bundles": [{
                "productsInCart": ["10003", "10004"],
                "productToUpgradeTo": "10007",
                "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
            }, {
                "productsInCart": ["10004"],
                "productToUpgradeTo": "10006",
                "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
            }]
        },
        10006: {
            "_name": "Gold plan",
            "disabled": [{
                "productToDisable": "10002",
                "message": false
            }, {
                "productToDisable": "10004",
                "message": true
            }, {
                "productToDisable": "10008",
                "message": false
            }],
            "swap": ["10001", "10002", "10004", "10005", "10007", "10008"],
            "bundles": [{
                "productsInCart": ["10003"],
                "productToUpgradeTo": "10007",
                "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
            }]
        },
        10007: {
            "_name": "Platinum plan",
            "disabled": [{
                "productToDisable": "10002",
                "message": false
            }, {
                "productToDisable": "10003",
                "message": true
            }, {
                "productToDisable": "10004",
                "message": true
            }, {
                "productToDisable": "10008",
                "message": false
            }],
            "swap": ["10001", "10002", "10003", "10004", "10005", "10006", "10008"],
            "bundles": []
        },
        10008: {
            "_name": "Genpest one time",
            "disabled": [],
            "swap": ["10005"],
            "bundles": []
        },
        10009: {
            "_name": "Mosquito one time",
            "disabled": [],
            "swap": ["10003"],
            "bundles": []
        }
    };
    if (typeof _GLOBAL_UI_SETTINGS != "undefined" &&
        _GLOBAL_UI_SETTINGS && _GLOBAL_UI_SETTINGS.productRuleSet) _PRODUCT_RULESET = _GLOBAL_UI_SETTINGS.productRuleSet;
    self.ProductTriggeredCartRule = function (productToAdd) {
        var productsInCart = ko.mapping.toJS(self.model().productIdCart()),
            rules = _PRODUCT_RULESET[productToAdd.productId];
        for (var b = 0; b < rules.bundles.length; b++) {
            var bundle = rules.bundles[b];
            if (ArrayContainsArray(productsInCart, bundle.productsInCart)) {
                var templateKeys = {};
                templateKeys.bundleSavings = function GetBundleSavings() {
                    var cartItemsCost = parseFloat(productToAdd.productCost) ||
                        0;
                    var bundleProductCost = 0;
                    bundle.productsInCart.forEach(function (productId) {
                        var product = self.model().availableProducts().find(function (product) {
                            return product.productId() == productId
                        });
                        if (product) cartItemsCost += parseFloat(product.productCost()) || 0
                    });
                    var bundledProduct = self.model().availableProducts().find(function (product) {
                        return bundle.productToUpgradeTo == product.productId()
                    });
                    if (bundledProduct) bundleProductCost = parseFloat(bundledProduct.productCost()) || 0;
                    return (cartItemsCost - bundleProductCost).toFixed(2)
                }();
                var bundleMessage = bundle.message;
                for (var templateKey in templateKeys) {
                    var templateKeyStr = "{{" + templateKey + "}}";
                    var templateReplacement = templateKeys[templateKey];
                    bundleMessage = bundleMessage.replaceAll(templateKeyStr, templateReplacement)
                }
                new CommonModal({
                        title: "Bundle Upgrade",
                        body: bundleMessage,
                        confirm: true,
                        btnCancelText: "No thanks",
                        btnConfirmText: "Upgrade"
                    }, function () {
                        var item = self.model().availableProducts().filter(function (item) {
                            return item.productId() === bundle.productToUpgradeTo
                        });
                        self.HandleAddRemoveProductFromCart(item[0])
                    },
                    function () {
                        self.HandleRuleSwapAndDisable(rules);
                        self.AddProductToCart(productToAdd)
                    });
                return true
            }
        }
        self.HandleRuleSwapAndDisable(rules);
        return false
    };
    self.HandleRuleSwapAndDisable = function (rules) {
        var productsInCart = ko.mapping.toJS(self.model().productIdCart());
        rules.swap.forEach(function (productId) {
            var itemId = productsInCart.filter(function (itemId) {
                return itemId === productId
            });
            if (itemId.length) {
                var productToRemove = self.productsInCart().filter(function (product) {
                    return product.productId() === itemId[0]
                });
                self.HandleAddRemoveProductFromCart(productToRemove[0])
            }
        });
        rules.disabled.forEach(function (product) {
            var item = self.model().availableProducts().filter(function (item) {
                return item.productId() === product.productToDisable
            });
            if (item.length) item[0].isDisabled(true)
        })
    };
    self.HandleEnableProductsAfterRemove = function (productId) {
        var disabledProducts = _PRODUCT_RULESET[productId].disabled;
        disabledProducts.forEach(function (itemId) {
            var product = self.model().availableProducts().filter(function (p) {
                return p.productId() ===
                    itemId.productToDisable
            });
            if (product.length) product[0].isDisabled(false)
        })
    };
    self.IsProductInCart = function (item) {
        var product = ko.mapping.toJS(item),
            model = ko.mapping.toJS(self.model);
        return model.productIdCart.indexOf(product.productId) > -1
    };
    self.productsInCart = ko.computed(function () {
        if (!self.modelLoaded()) return [];
        return self.model().availableProducts().filter(function (product) {
            return self.model().productIdCart().indexOf(product.productId()) > -1
        })
    });
    self.HasProductsInCartWithPrice = ko.computed(function () {
        return self.productsInCart().some(function (product) {
            return product.startUpCostTotal()
        })
    });
    self.cartTotals = ko.computed(function () {
        if (!self.modelLoaded()) return {};
        var totals = {
                subtotal: 0,
                discount: 0,
                tax: 0,
                due: 0
            },
            products = self.productsInCart();
        products.forEach(function (product) {
            totals.subtotal += product.productCost();
            totals.discount += product.discount();
            totals.tax += product.taxes();
            if (product.discount() > 0) totals.due += product.startUpCostTotal();
            else totals.due += product.startUpCostTotalWithoutDiscount()
        });
        return totals
    });
    self.FormatDateDisplayMonth = function (dateString) {
        var date = new Date(dateString);
        return GetMonthNameShort(date.getUTCMonth() + 1)
    };
    self.FormatDateDisplayDay = function (dateString) {
        var date = new Date(dateString);
        return date.getUTCDate()
    };
    self.IsTimeAvailable = function (time) {
        if (!self.modelLoaded() || !self.model().selectedDate()) return false;
        var selectedDate = self.model().availableDates().find(function (availableDate) {
            return availableDate.dateString() == self.model().selectedDate()
        });
        return !selectedDate ? false : selectedDate.availableTimes().indexOf(time) >= 0
    };
    self.FormatTimeSlot = function (startTime) {
        startTime =
            parseInt(startTime.substr(0, 2));
        var endTime = startTime + 2;
        var startSuffix = startTime > 12 ? "PM" : "AM";
        var endSuffix = endTime > 11 ? "PM" : "AM";
        if (startTime > 12) startTime -= 12;
        if (endTime > 12) endTime -= 12;
        return startTime + startSuffix + " - " + endTime + endSuffix
    };
    self.HasScheduleItemSelected = function () {
        if (self.modelLoaded() && self.model()) {
            var selectedKey = ko.unwrap(self.model().timeSlotSelectedKey);
            if (selectedKey) return true
        }
        return false
    };
    self.GetStepText = function (step) {
        return stepTextMap[step] || ""
    };
    self.IsStepComplete = function (step) {
        if (!self.modelLoaded()) return false;
        var stepIndex = defaultStepOrder.indexOf(step);
        if (stepIndex == -1 || stepIndex == currentStepIndex()) return false;
        if (stepIndex < currentStepIndex()) return true;
        return stepCache[step]() !== null
    };
    self.IsStepDisabled = function (step) {
        if (!self.modelLoaded()) return false;
        if (currentStepObservable() == STEP_CONFIRMATION) return true;
        if (!self.IsStepComplete(step)) return true;
        return false
    };
    self.CanGoToStep = function (step) {
        return !self.submitting() && !self.IsStepDisabled(step)
    };
    self.GoToStep = function (step) {
        if (!self.CanGoToStep(step)) return;
        if (step == STEP_ADDRESS) stepCache[step]().customer.serviceProperty.addressVerified = false;
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Click Step " + GetStepName(step), "From Step " + GetStepName(self.currentStep()), "");
        if (step in stepCache && stepCache[step]() !== null) UpdateModel(stepCache[step]())
    };
    self.StepBack = function () {
        if (!self.modelLoaded() || self.submitting() || currentStepIndex() < 1) return;
        self.GoToStep(stepOrder()[currentStepIndex() - 1])
    };
    self.StepCanBeSubmitted = ko.computed(function () {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                return self.modelLoaded();
            case STEP_PRODUCTS:
                break;
            case STEP_SCHEDULE:
                return self.HasScheduleItemSelected();
            case STEP_PAYMENT:
                if (self.model().actionList && self.model().actionList() && self.model().actionList().length > 0 && self.model().actionList()[0] == "DISABLE_SUBMIT_ORDER") return false;
                break
        }
        return true
    });
    self.IsValidStep = function () {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                var result = true;
                result = self.validator.test("customer.serviceProperty.address1") && result;
                result = self.validator.test("customer.serviceProperty.zipCode") &&
                    result;
                if (self.addressValidator.ziptasticFail()) {
                    result = self.validator.test("customer.serviceProperty.city") && result;
                    result = self.validator.test("customer.serviceProperty.state") && result
                }
                result = self.validator.test("customer.contact.email") && result;
                result = self.validator.test("customer.contact.phone") && result;
                if (!result) {
                    self.PostError("One or more form items are invalid. Please check the form for errors.");
                    return false
                }
                return true;
                break;
            case STEP_PRODUCTS:
                break;
            case STEP_SCHEDULE:
                var result = true,
                    errorMsg =
                    "";
                if (!self.HasProductsInCartWithPrice())
                    if (!self.validator.test("customer.firstName") || !self.validator.test("customer.lastName")) {
                        result = false && result;
                        errorMsg = "Please fill out the home owner's first and last name."
                    } if (!(self.model().selectedDate() && self.model().selectedTime() || self.model().selectedDate().toUpperCase() === "NONE")) {
                    result = false && result;
                    errorMsg = "Please select a date and time to schedule your appointment."
                }
                if (!result) {
                    self.PostError(errorMsg);
                    return false
                }
                return true;
                break;
            case STEP_PAYMENT:
                var result =
                    true;
                result = self.validator.test("customer.firstName") && result;
                result = self.validator.test("customer.lastName") && result;
                if (!self.billingAddressSameAsServiceAddress()) {
                    result = self.validator.test("customer.billingProperty.address1") && result;
                    result = self.validator.test("customer.billingProperty.city") && result;
                    result = self.validator.test("customer.billingProperty.state") && result;
                    result = self.validator.test("customer.billingProperty.zipCode") && result
                }
                switch (self.model().payment.paymentMethod()) {
                    case "card":
                        result =
                            self.validator.test("payment.cardNumber") && result;
                        result = self.validator.test("payment.cardExpire") && result;
                        if (result) self.model().payment.cardNumber(self.model().payment.cardNumber().replace(/\s/g, ""));
                        break;
                    case "bank":
                        result = self.validator.test("payment.bankAccountNumber") && result;
                        result = self.validator.test("payment.bankRoutingNumber") && result;
                        break;
                    default:
                        return false
                }
                result = self.validator.test("customer.contact.phone") && result;
                result = self.validator.test("customer.contact.email") && result;
                if (!result) {
                    self.PostError("One or more form items are invalid. Please check the form for errors.");
                    return false
                }
                return true;
                break
        }
        return true
    };
    self.PostError = function (errorMessage, messageColor, errorCode) {
        if (!errorMessage) return;
        var msg = {
            "errorMessage": errorMessage || "An error has occurred.",
            "messageColor": messageColor || null,
            "errorCode": errorCode || "GENERAL_ERROR"
        };
        self.model().errorMessages.push(msg);
        ScrollTo($(".error-msg.bg-danger.text-danger").offset().top - 100, 500)
    };
    self.CheckStepAddressValidation = function (json, submitFunction) {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                var address = json.customer.serviceProperty;
                if (self.addressValidator.IsLastValidatedAddress(address)) {
                    submitFunction();
                    return
                }
                self.addressValidator.isValidating(true);
                GetCityAndStateFromZip(address.zipCode, function (data) {
                    if (data.error) {
                        if (!self.addressValidator.ziptasticFail()) {
                            self.submitting(false);
                            self.addressValidator.ziptasticFail(true);
                            self.validator.add("customer.serviceProperty.city", "required");
                            self.validator.add("customer.serviceProperty.state", "required");
                            $(".city-state-block").show();
                            self.PostError("There was a problem resolving your ZIP. Please enter your city and state.");
                            FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Address Validation", "Error: There was a problem resolving your ZIP. Please enter your city and state.", "");
                            return
                        }
                        address.city = self.model().customer.serviceProperty.city();
                        address.state = self.model().customer.serviceProperty.state()
                    } else {
                        self.model().customer.serviceProperty.city(data.model.city);
                        self.model().customer.serviceProperty.state(data.model.state);
                        address.city = data.model.city;
                        address.state = data.model.state;
                        FireFunnelGAEvent(GA_CATEGORY_FUNNEL,
                            "Submit Address Validation", "Submit Success", "")
                    }
                    self.addressValidator.ziptasticFail(false);
                    self.addressValidator.ValidateAddress(address, {
                        onKeepAddress: function () {
                            SubmitStep()
                        },
                        onUseSelectedAddress: function (selectedAddress) {
                            self.model().customer.serviceProperty.address1(selectedAddress.address1);
                            self.model().customer.serviceProperty.address2(selectedAddress.address2);
                            self.model().customer.serviceProperty.city(selectedAddress.city);
                            self.model().customer.serviceProperty.state(selectedAddress.state);
                            self.model().customer.serviceProperty.zipCode(selectedAddress.zipCode);
                            SubmitStep()
                        },
                        onValidate: function (hasCorrections) {
                            if (hasCorrections) self.submitting(false);
                            else SubmitStep()
                        }
                    })
                });
                break;
            case STEP_PRODUCTS:
                submitFunction();
                break;
            case STEP_SCHEDULE:
                submitFunction();
                break;
            case STEP_PAYMENT:
                submitFunction();
                break
        }
    };

    function SubmitStep() {
        self.RemoveAllMessages();
        if (!self.IsValidStep()) return;
        self.submittingStep(self.currentStep());
        self.submitting(true);
        var json = GetModelJSON();
        self.CheckStepAddressValidation(json,
            function () {
                var dwr = "";
                json.liveChat = JSON.parse(sessionStorage.getItem("LiveChatState")) || {
                    visitorId: null,
                    hasChatOccurred: false
                };
                switch (self.currentStep()) {
                    case STEP_ADDRESS:
                        dwr = "TMXPurchaseFunnelUIUtils/getProductsAndPricing";
                        break;
                    case STEP_PRODUCTS:
                        dwr = "TMXPurchaseFunnelUIUtils/getScheduleAvailability";
                        break;
                    case STEP_SCHEDULE:
                        dwr = "TMXPurchaseFunnelUIUtils/submitSchedule";
                        break;
                    case STEP_PAYMENT:
                        dwr = "TMXPurchaseFunnelUIUtils/submitOrder";
                        break
                }
                if (!dwr) throw "Unrecognized funnel step.";
                if (currentStepObservable() ==
                    STEP_PRODUCTS) {
                    currentStepObservable(STEP_SCHEDULE);
                    ScrollTo(0, "fast")
                }
                CallDWR(dwr, json, function (data) {
                    var errorMessage = data.error || data.model && data.model.errorMessages && data.model.errorMessages.length && data.model.errorMessages[0].errorMessage;
                    if (errorMessage) {
                        switch (self.currentStep()) {
                            case STEP_PRODUCTS:
                                if (data.model && data.model.actionList && data.model.actionList.length && data.model.actionList[0] === "INVALID_ADDRESS") {
                                    self.PostError(errorMessage);
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Address Validation",
                                        "Error: " + errorMessage, "")
                                } else {
                                    currentStepObservable(json.currentStep);
                                    self.PostError(errorMessage);
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Retrieve Pricing", "Error: Failed to retrieve pricing.", "")
                                }
                                break;
                            case STEP_SCHEDULE:
                                if (!data.textStatus || data.textStatus.toLowerCase() !== "timeout") {
                                    self.PostError(errorMessage);
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Schedule Step", "Error: " + errorMessage, "")
                                } else {
                                    currentStepObservable(json.currentStep);
                                    json.currentStep = STEP_SCHEDULE;
                                    self.noDatesChosen(true);
                                    ClearStepCache({
                                        after: json.currentStep
                                    });
                                    stepCache[json.currentStep](json);
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Retrieve Schedule", "Error: Failed to retrieve schedule.", "");
                                    UpdateModel(json)
                                }
                                break;
                            case STEP_PAYMENT:
                                self.PostError(errorMessage);
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Order", "Error: " + errorMessage, "");
                                break;
                            case STEP_ADDRESS:
                            case STEP_CONFIRMATION:
                            default:
                                self.PostError(errorMessage);
                                break
                        }
                        if (data.model && data.model.actionList && data.model.actionList.length) UpdateModel(data.model)
                    } else {
                        switch (self.currentStep()) {
                            case STEP_ADDRESS:
                                break;
                            case STEP_PRODUCTS:
                                break;
                            case STEP_SCHEDULE:
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Retrieve Schedule", "Success", "");
                                break;
                            case STEP_PAYMENT:
                                break;
                            default:
                                console.log("-- default success?");
                                break
                        }
                        switch (json.currentStep) {
                            case STEP_ADDRESS:
                                if (data.model.currentStep == STEP_SCHEDULE) {
                                    isShowStep[STEP_PRODUCTS](false);
                                    isShowStep[STEP_PAYMENT](false)
                                } else {
                                    isShowStep[STEP_PRODUCTS](true);
                                    if (self.IsFreeInspectionPath()) isShowStep[STEP_PAYMENT](false);
                                    else isShowStep[STEP_PAYMENT](true)
                                }
                                break;
                            case STEP_SCHEDULE:
                                if (data.model.currentStep ==
                                    STEP_CONFIRMATION) isShowStep[STEP_PAYMENT](false);
                                else isShowStep[STEP_PAYMENT](true);
                                break
                        }
                        currentStepObservable(json.currentStep);
                        if (json.currentStep != data.model.currentStep) {
                            ClearStepCache({
                                after: data.model.currentStep
                            });
                            if (data.model.currentStep == STEP_CONFIRMATION) {
                                var gaConversion = data.model && data.model.sendInfoToGoogleAnalytics ? data.model.sendInfoToGoogleAnalytics : "";
                                if (gaConversion) try {
                                    utag.view(data.model.sendInfoToGoogleAnalytics)
                                } catch (e$3) {
                                    console.log(e$3)
                                }
                                var productCodeString = "",
                                    productsInCart =
                                    ko.mapping.toJS(self.productsInCart());
                                var freeInspOnly = false;
                                var gaValueTotal = 0;
                                for (var i = 0; i < productsInCart.length; i++) {
                                    freeInspOnly = productsInCart.length == 1 && productsInCart[i].productId == "10001";
                                    var gaValue = parseFloat(productsInCart[i].productCost);
                                    gaValue = gaValue ? gaValue : 0;
                                    gaValueTotal += gaValue;
                                    if (data.model.showUpsell) {
                                        var upsellLabel = freeInspOnly ? "FREEINSPONLY" : productsInCart[i].productCode;
                                        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "UPSELL", upsellLabel, gaValue)
                                    }
                                    if (data.model.tiered) FireFunnelGAEvent(GA_CATEGORY_FUNNEL,
                                        "TIERED", productsInCart[i].productCode, gaValue);
                                    if (!productCodeString) productCodeString = productsInCart[i].productCode + ": " + productsInCart[i].templateCode;
                                    else productCodeString += " / " + productsInCart[i].productCode + ": " + productsInCart[i].templateCode
                                }
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Submit Order", "Success: " + productCodeString, gaValueTotal);
                                if (self.IsAffiliatePath()) {
                                    var affiliate = ko.unwrap(self.model().commercialAffiliate);
                                    FireAffiliateGAEvent({
                                        id: affiliate.id(),
                                        name: affiliate.name()
                                    }, "Conversions")
                                }
                                var freeInspectionConversion =
                                    data.model && data.model.productIdCart.indexOf("10001") > -1;
                                if (self.IsFreeInspectionPath() && freeInspectionConversion) FireFreeInspectionConversion()
                            }
                        }
                        stepCache[json.currentStep](json);
                        UpdateModel(data.model)
                    }
                    self.submitting(false)
                })
            })
    }
    self.SubmitStep = function () {
        if (!self.modelLoaded() || self.submitting()) return;
        $("#funnel-wrapper :focus").blur();
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Links and Buttons", "Button: " + GetStepName(self.currentStep()) + " Step Submit", "");
        SubmitStep()
    };
    self.LoadModel = function () {
        CallDWR("TMXPurchaseFunnelUIUtils/getPurchaseModel" +
            window.location.search, null,
            function (data) {
                var optionalAbandonMsg = "";
                if (window.location.search.indexOf("abandon=true") > -1) optionalAbandonMsg = " On Abandon";
                if (data.error) {
                    self.error(data.error);
                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Funnel Loaded" + optionalAbandonMsg, "Error: " + data.error, "")
                } else {
                    if (g_isHistorySupported) history.pushState(stepHistory, "", GetStepURL(data.model.currentStep || defaultStepOrder[0]));
                    if (window.location.search.indexOf("abandon=true") > -1) {
                        var tmpmodel = Object.create(data.model);
                        tmpmodel.currentStep = STEP_ADDRESS;
                        stepCache[defaultStepOrder[0]]($.extend(true, {}, tmpmodel))
                    }
                    stepCache[data.model.currentStep || defaultStepOrder[0]]($.extend(true, {}, data.model));
                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Funnel Loaded" + optionalAbandonMsg, "Success", "");
                    if (!IsObjectEmpty(data.model.commercialAffiliate)) FireAffiliateGAEvent(data.model.commercialAffiliate, "Passed Through");
                    UpdateModel(data.model, true)
                }
            })
    };
    self.UpdateModel = function (model) {
        UpdateModel(model)
    };
    self.PurchaseStep = function () {
        self.submittingStep(self.currentStep());
        self.submitting(true);
        var json = GetModelJSON();
        CallDWR("TMXPurchaseFunnelUIUtils/getProductsAndPricing", json, function (data) {
            if (data.error) self.PostError("An error has occurred. Please try again.");
            else UpdateModel(data.model);
            self.submitting(false)
        })
    }
}
$(function () {
    var vm = new FunnelViewModel;
    window.onpopstate = function (event) {
        if (event.state) vm.OnHistoryPop();
        else {
            console.log("no event state");
            if (g_isHistorySupported);
        }
    };
    window.vm = vm;
    ko.applyBindings(vm, $("#funnel-wrapper").get(0));
    var loadedModel = false;
    if (caniuse.sessionStorage) {
        var funnelModelJSON = sessionStorage.getItem("funnelModel");
        if (funnelModelJSON) {
            var funnelModel;
            try {
                funnelModel = JSON.parse(funnelModelJSON)
            } catch (e) {
                console.log(e)
            }
            if (funnelModel) {
                vm.UpdateModel(funnelModel);
                loadedModel = true
            }
        }
    }
    if (!loadedModel) vm.LoadModel();
    $(".logo-container a").on("click", function () {
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Links and Buttons", "Link: Home Logo", "")
    });
    $(".phone-link a").on("click", function () {
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, "Links and Buttons", "Link: Phone Number", "")
    });
    $(document).on("click", ".see-more-details", function (e) {
        e.preventDefault();
        var $this = $(this);
        var showMoreDetailsHtml = '<i class="fa fa-chevron-circle-down"></i> Details',
            hideDetailsHtml = '<i class="fa fa-chevron-circle-up"></i> Hide details',
            $productContainer =
            $this.parents(".product-container"),
            $productDetails = $this.parents(".product-container").find(".product-details");
        if ($productContainer.find(".card").hasClass("tiered")) {
            $productContainer = $productContainer.parents("#tiered-products").find(".product-container");
            $productDetails = $productContainer.find(".product-details");
            $this = $productContainer.find(".see-more-details")
        }
        if ($productContainer.hasClass("open")) {
            $this.html(showMoreDetailsHtml);
            $productContainer.removeClass("open").addClass("closed");
            ScrollTo($productContainer.offset().top -
                120, "fast")
        } else {
            $this.html(hideDetailsHtml);
            $productContainer.removeClass("closed").addClass("open")
        }
        $productDetails.slideToggle("fast")
    });
    $(document).on("click", ".expand-details", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $i = $this.find("i.fa"),
            $subDetails = $this.parents("tr.line-item").next("tr.sub-details");
        if ($i.hasClass("fa-plus-circle")) {
            $i.removeClass("fa-plus-circle");
            $i.addClass("fa-minus-circle");
            $subDetails.show()
        } else {
            $i.removeClass("fa-minus-circle");
            $i.addClass("fa-plus-circle");
            $subDetails.hide()
        }
    });
    $(document).on("click", ".trigger-popup-content", function (e) {
        e.preventDefault();
        var $this = $(this);
        var title = $this.parent("li").find(".popup-title").html() || "",
            content = $this.find(".popup-content").html() || "";
        new CommonModal({
            title: title,
            body: content,
            confirm: false,
            btnCancelText: "Close",
            btnConfirmText: ""
        })
    })
});
