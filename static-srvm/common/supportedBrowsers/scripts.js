(function (window) {
    {
        var other = "-";
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = "" + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;
        if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
            browser = "Microsoft Internet Explorer";
            version = nAgt.substring(verOffset + 5)
        } else if (nAgt.indexOf("Trident/") != -1) {
            browser = "Microsoft Internet Explorer";
            version = nAgt.substring(nAgt.indexOf("rv:") + 3)
        } else {
            browser = other;
            version = other
        }
        if ((ix = version.indexOf(";")) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(" ")) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(")")) != -1) version = version.substring(0, ix);
        majorVersion = parseInt("" + version, 10);
        if (isNaN(majorVersion)) {
            version = "" + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10)
        }
        var os = other;
        var clientStrings = [{
                s: "Windows 10",
                r: /(Windows 10.0|Windows NT 10.0)/
            }, {
                s: "Windows 8.1",
                r: /(Windows 8.1|Windows NT 6.3)/
            }, {
                s: "Windows 8",
                r: /(Windows 8|Windows NT 6.2)/
            }, {
                s: "Windows 7",
                r: /(Windows 7|Windows NT 6.1)/
            }, {
                s: "Windows Vista",
                r: /Windows NT 6.0/
            }, {
                s: "Windows Server 2003",
                r: /Windows NT 5.2/
            }, {
                s: "Windows XP",
                r: /(Windows NT 5.1|Windows XP)/
            }, {
                s: "Windows 2000",
                r: /(Windows NT 5.0|Windows 2000)/
            }, {
                s: "Windows ME",
                r: /(Win 9x 4.90|Windows ME)/
            }, {
                s: "Windows 98",
                r: /(Windows 98|Win98)/
            }, {
                s: "Windows 95",
                r: /(Windows 95|Win95|Windows_95)/
            }, {
                s: "Windows NT 4.0",
                r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
            }, {
                s: "Windows CE",
                r: /Windows CE/
            },
            {
                s: "Windows 3.11",
                r: /Win16/
            }];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break
            }
        }
        var osVersion = other;
        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = "Windows"
        }
    }
    window.jscd = {
        browser: browser,
        browserVersion: version,
        browserMajorVersion: majorVersion,
        os: os,
        osVersion: osVersion
    }
})(this);
(function () {
    var readyFired = false;

    function ready() {
        if (!readyFired) {
            readyFired = true;
            handleUnsupportedBrowsers(false, false)
        }
    }

    function readyStateChange() {
        if (document.readyState === "complete") ready()
    }
    if (document.readyState === "complete") setTimeout(ready, 1);
    else if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", ready, false);
        window.addEventListener("load", ready, false)
    } else {
        document.attachEvent("onreadystatechange", readyStateChange);
        window.attachEvent("onload", ready)
    }
})();

function handleUnsupportedBrowsers(disableFormFields, forceShow) {
    disableFormFields = disableFormFields || false;
    forceShow = disableFormFields || forceShow || false;
    var banner = $("#unsupportedBrowserBanner:hidden");
    var cookie = getCookie("unsupportedBrowser");
    var unsupportedBrowser;
    if (cookie === null) {
        unsupportedBrowser = isUnsupportedBrowser();
        document.cookie = "unsupportedBrowser" + "=" + unsupportedBrowser
    } else unsupportedBrowser = cookie;
    if (unsupportedBrowser && (forceShow || cookie === null)) {
        if (disableFormFields) $("input").attr("disabled",
            true);
        banner.slideDown()
    }
    return unsupportedBrowser
}

function isUnsupportedBrowser() {
    var ieVersion = parseInt(jscd.browserVersion);
    var osVersion = jscd.osVersion;
    if (jscd.browser == "-" || jscd.os == "-") return false;
    else if (ieVersion < 9) return true;
    else if (ieVersion == 9 && osVersion == "Vista" || ieVersion == 10 && osVersion == "8" || ieVersion == 11) return false;
    return true
}

function getCookie(key) {
    var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
    return keyValue ? keyValue[2] : null
};