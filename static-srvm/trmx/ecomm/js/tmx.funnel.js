/*
 * Browser feature detection
 *
 * If more features are added, consider creating an object rather than several global variables.
 **********************************************************************************************/
var g_isHistorySupported = window.history && typeof history.pushState == 'function' && typeof history.replaceState == 'function' && typeof history.back == 'function';

var currentUrl = "http://test.terminix.com";

/*
 * Misc. functions
 **********************************************************************************************/
function FormatNumber(value, numDecimals, useCommas) {
    var decimals = '';

    if (numDecimals > 0) {
        for (var i = 0; i < numDecimals; i++)
            decimals += '0';

        var decimalFactor = Math.pow(10, numDecimals);
        decimals += (Math.floor(Math.round(value * decimalFactor)) % decimalFactor);

        decimals = '.' + decimals.substr(decimals.length - numDecimals);
    }

    var whole = '' + Math.abs(((value >= 0) ? Math.floor(value) : Math.ceil(value)) || 0);

    if (useCommas === true) {
        whole = whole.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return whole + decimals;
}

function GetLastFourDigits(value) {
    return value.substr(value.length - 4);
}

function GetMonthName(month) {
    var months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return month >= 0 && month < months.length ? months[month] : '';
}

function GetMonthNameShort(month) {
    return GetMonthName(month).substr(0, 3);
}

function StringToLower(str) {
    return (str ? '' + str : '').toLowerCase();
}

function StringToUpper(str) {
    return (str ? '' + str : '').toUpperCase();
}

function GetAddressLine(address) {
    var address1 = ko.unwrap(address.address1),
        address2 = ko.unwrap(address.address2);

    return address2 ? (address1 + ' ' + address2) : address1;
}

function GetCityAndStateFromZip(zip, callback) {
    if (_ENABLE_SMARTY_ZIP_TOGGLE == true) {
        if (typeof callback == 'function') {
            SmartyStreetsGetCityAndState(zip, callback);
        }
    } else {
        if (typeof callback == 'function') {
            ZiptasticGetCityAndState(zip, callback);
        }
    }
}

function ZiptasticGetCityAndState(zip, callback) {
    $.ajax({
        type: 'GET',
        url: 'https://zip.getziptastic.com/v2/US/' + zip,
        dataType: 'json'
    }).done(function (data) {
        callback({
            model: {
                city: data.city,
                state: data.state_short
            }
        });
    }).fail(function () {
        callback({
            error: "An error has occurred."
        });
    });
}

function SmartyStreetsGetCityAndState(zip, callback) {
    $.ajax({
        type: 'GET',
        url: currentUrl + '/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=' + zip,
        dataType: 'json'
    }).done(function (result) {
        if (result.data.validResponse) {
            callback({
                model: {
                    city: result.data.city,
                    state: result.data.state
                }
            });
        } else {
            callback({
                error: "Not a valid response was received. Asking Customer for their city and state."
            });
        }
    }).fail(function () {
        callback({
            error: "An error has occurred while calling getCityAndStateFromZip."
        });
    });
}

function ScrollTo(pos, speed) {
    $('html, body').animate({
        scrollTop: pos
    }, speed);
}

/*
 * jQuery utilities
 **********************************************************************************************/
jQuery.fn.ShowButtonProgress = function (options) {
    options = $.extend({
        text: 'Loading...',
        autosize: true
    }, options);

    return this.each(function () {
        var $button = $(this);

        if ($button.data('in-progress')) {
            // Remove progress so it can redo everything
            $button.prev().remove();
            $button.show();
        }

        // Create container and button
        var $progressContainer = $('<div class="button-progress-container">'),
            $progressButton = $('<div>');

        $progressContainer.append($progressButton);

        // Copy over styles to button
        $progressButton.attr('class', $button.attr('class'));
        $progressButton.addClass('show-progress');
        $progressButton.attr('style', $button.attr('style'));
        $progressButton.text(options.text || 'Loading...');

        // Copy position styles to container and remove any from the button
        var positionStyles = {
            'margin-bottom': 0,
            'margin-left': 0,
            'margin-right': 0,
            'margin-top': 0,
            'bottom': 0,
            'left': 0,
            'right': 0,
            'top': 0,
            'float': 'none',
            'position': 'relative',
            'transform': 'none'
        };

        $button.hide(); // Need to hide to be able to access "auto" margins in FireFox

        for (var rule in positionStyles) {
            $progressContainer.css(rule, $button.css(rule));
            $progressButton.css(rule, positionStyles[rule]);
        }

        $button.show(); // Show again because we need width/height sizes from below

        // Grab size of real button so container can match
        var buttonWidth = $button.outerWidth(),
            buttonDisplay = $button.css('display');

        if (options.autosize) {
            $progressContainer.height($button.outerHeight());
        }

        // Show the container and hide the original button
        $progressContainer.insertBefore($button).show();
        $button.hide();

        // Size the container properly
        $progressContainer.css('display', 'inline-block');

        if (options.autosize) {
            buttonWidth = Math.max(buttonWidth, $progressContainer.outerWidth());

            $progressContainer.css('width', buttonWidth + 'px').css('display', buttonDisplay);
        }

        // Set that the button has progress
        $button.data('in-progress', true);
    });
};

jQuery.fn.HideButtonProgress = function () {
    return this.each(function () {
        var $button = $(this);

        if (!$button.data('in-progress')) return;

        $button.prev().remove();
        $button.show();

        $button.data('in-progress', false);
    });
};

jQuery.QueryString = (function (paramsArray) {
    var data = {};
    for (var i = 0; i < paramsArray.length; i++) {
        var pieces = paramsArray[i].split('=');
        data[pieces[0]] = (pieces.length > 1) ? decodeURIComponent(pieces[1].replace(/\+/g, ' ')) : true;
    }
    return data;
})(window.location.search.substring(1).split('&'));


/*
 * Knockout custom bindings
 **********************************************************************************************/
ko.isObservableArray = ko.isObservableArray || function (obj) {
    return ko.isObservable(obj) && !(obj.destroyAll === undefined);
};

ko.bindingHelpers = ko.bindingHelpers || {};

ko.bindingHelpers.extendAllBindings = function (allBindings, newBindings) {
    var bindings = function () {
        return $.extend({}, allBindings(), newBindings);
    };

    bindings.get = function (name) {
        var data = bindings();

        return (name in data) ? data[name] : null;
    };

    return bindings;
};

ko.bindingHandlers.buttonProgress = {
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        if (ko.unwrap(valueAccessor())) {
            var options = $.extend({}, allBindings.get('buttonProgressOptions'));
            options.text = allBindings.get('buttonProgressText') || options.text;

            $(element).ShowButtonProgress(options);
        } else {
            $(element).HideButtonProgress();
        }
    }
};

ko.bindingHandlers.masked = (function () {
    var EvaluateMask = function (element, allBindings) {
        var mask = {
            format: '',
            options: {}
        };

        var preset = allBindings().maskedPreset;

        switch (preset) {
            case 'phone':
                mask.format = '(000) 000-0000';
                break;
            case 'phone-display':
                mask.format = '000.000.0000';
                break;
            case 'zipcode':
                mask.format = '00000';
                break;
            case 'date':
                mask.format = '00/00/0000';
                mask.options = {
                    placeholder: 'MM/DD/YYYY'
                };
                break;
            case 'card-number':
                mask.format = '0000000000000000';
                break;
            case 'card-expire':
                mask.format = '00/00';
                break;
            case 'card-security':
                mask.format = '0000';
                break;
            case 'bank-routing-number':
                mask.format = 'Z00000000';
                mask.options = {
                    translation: {
                        'Z': {
                            pattern: /[012346789]/
                        }
                    }
                };
                break;
        }

        mask.format = allBindings().maskedFormat || mask.format;
        $.extend(mask.options, allBindings().maskedOptions);

        return mask;
    };

    var DetectElementType = function (element) {
        return $(element).is('input, textarea') ? 'value' : 'text';
    };

    return {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // Apply the right default handler
            var elementType = DetectElementType(element);
            var mask = EvaluateMask(element, allBindings);

            if (elementType == 'value') {
                var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
                    valueUpdate: 'afterkeydown'
                });

                ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings, viewModel, bindingContext);

                // Apply the mask
                $(element).mask(mask.format, mask.options);
            } else {
                var $element = $(element).text(valueAccessor()());

                $element.mask(mask.format, mask.options);
            }
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // Apply the right default handler
            var elementType = DetectElementType(element);
            var mask = EvaluateMask(element, allBindings);

            if (elementType == 'value') {
                var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
                    valueUpdate: 'afterkeydown'
                });

                ko.bindingHandlers.value.update(element, valueAccessor, newAllBindings, viewModel, bindingContext);

                // Apply the mask
                $(element).mask(mask.format, mask.options);

                valueAccessor()($(element).val());
            } else {
                var masked = $(element).masked(valueAccessor()());

                $(element).text(masked);
            }
        }
    };
})();

ko.bindingHandlers.submitForm = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).on('keydown', function (e) {
            var submitFormOn = allBindings.get('submitFormOn');

            // Check if user hit ENTER to submit
            if (e.keyCode === 13 && (!submitFormOn || typeof submitFormOn == 'function' && submitFormOn())) {
                // Create fake params to send
                var params = [viewModel, e];

                // Call the function
                var fn = valueAccessor();

                if (typeof fn == 'function') {
                    fn.apply(bindingContext, params);
                }
            }
        });
    }
};


/*
 * DWR functions
 *****************************************************************/
function CallDWR(path, model, callback) {
    //path = CallDWR._CleanPathString(path);
    var basePath = 'http://test.terminix.com/funnel/dwr/jsonp/';
    // var basePath = currentUrl + '/funnel/dwr/jsonp/';
    var blocked = CallDWR.OnBeforeCallbacks.some(function (onBeforeCallback) {
        return onBeforeCallback(path, model) === false;
    });

    if (blocked) return;

    var HandleCallbackData = function (data) {
        if (typeof callback == 'function') {
            callback(data);
        }

        CallDWR.OnAfterCallbacks.forEach(function (onAfterCallback) {
            onAfterCallback(path, model, data);
        });
    };

    $.ajax({
        type: 'POST',
        url: basePath + path,
        data: model ? {
            jsonData: JSON.stringify(model)
        } : {},
        dataType: 'json',
        timeout: 2 * 60 * 10000,
        complete: function (data, textStatus) {
            var callbackData;

            try {
                var escapedText = JSON.parse(data.responseText);

                if (escapedText.error) {
                    callbackData = {
                        error: escapedText.error.message,
                        status: textStatus // for timeout check
                    };

                } else {
                    callbackData = {
                        model: JSON.parse(escapedText.reply)
                    };
                }
            } catch (e) {
                callbackData = {
                    error: "An error has occurred."
                };
                console.log("Error from server request: " + e.message);
            }

            HandleCallbackData(callbackData);
        }
    });
}

CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];

CallDWR._CleanPathString = function (path) {
    return path.replace(/^\/*|\/*$/g, '');
};

function OnBeforeCallDWR(callback) {
    CallDWR.OnBeforeCallbacks.push(callback);
}

function OnAfterCallDWR(callback) {
    CallDWR.OnAfterCallbacks.push(callback);
}

function GetInterestArea() {
    // Default to pest control
    var interestArea = 'PEST CONTROL';
    //    var interestArea = 'MOSQUITOES';

    // Update the interest area if one exists in the session
    if (GetURLParameter("interestArea") || caniuse.sessionStorage) {
        var newArea = GetURLParameter("interestArea") || sessionStorage.getItem("interestArea");

        if (newArea) {
            interestArea = newArea.toUpperCase();
        }
    }

    return interestArea;
}

function IsFreeInspectionInterest(interestArea) {
    switch (interestArea) {
        case 'PEST CONTROL':
        case 'ANTS':
        case 'CLOTHES MOTHS':
        case 'CRICKETS':
        case 'FLEAS':
        case 'SCORPIONS':
        case 'SILVERFISH':
        case 'SPIDERS':
        case 'TICKS':
        case 'PAPER WASPS':
        case 'MOSQUITOES':
            return false;
            break;

        case 'ARMADILLOS':
        case 'ATTIC INSULATION':
        case 'BED BUG':
        case 'CRAWL SPACE':
        case 'MICE':
        case 'RACCOONS':
        case 'RODENTS':
        case 'SQUIRRELS':
        case 'TERMITES':
        case 'WILDLIFE':
        case 'OTHER SERVICES':
            return true;
            break;
    }
}



/*
 * Common View Model functions
 *****************************************************************/
function CommonViewModel() {
    var self = this;

    // ViewModel variables
    // =========================

    self.model = ko.observable();
    self.modelLoaded = ko.observable(false);
    self.error = ko.observable();

    self.submitting = ko.observable(false);
    self.customerId = ko.observable();
    self.opaqueId = ko.observable();
    var refreshToken = ko.observable();
    self.isShowIFrame = ko.observable(false);
    self.testData = ko.observable("Test data");
    self.walletJsonData = ko.observable();

    // Common functions
    // =========================

    self.ResolveScope = function (scope) {
        if (scope === undefined) scope = self.model;
        if (ko.isObservable(scope)) scope = scope();

        return scope;
    };

    self.ResolveFieldName = function (field, scope) {
        var valueAccessor = self.ResolveScope(scope);
        if (!valueAccessor) return null;

        var pieces = field.split('.');

        for (var i = 0; i < pieces.length; i++) {
            if (!valueAccessor.hasOwnProperty(pieces[i]))
                return null;

            valueAccessor = ko.unwrap(valueAccessor[pieces[i]]);
        }

        return valueAccessor;
    };


    // Error functions
    // =========================

    self.RemoveAllMessages = function (scope) {
        scope = self.ResolveScope(scope);
        if (!scope) return;

        if (ko.isObservableArray(scope.errorMessages)) scope.errorMessages.removeAll();
        if (ko.isObservableArray(scope.fieldValidationErrors)) scope.fieldValidationErrors.removeAll();
        if (ko.isObservableArray(scope.informationalMessages)) scope.informationalMessages.removeAll();
    };

    self.HasFieldError = function (field, scope) {
        return self.GetFieldError(field, scope) != '';
    };

    self.GetFieldError = function (field, scope) {
        scope = self.ResolveScope(scope);

        if (!field || !scope) return '';

        var fieldError = scope.fieldValidationErrors().find(function (fieldError) {
            return ko.unwrap(fieldError.field) == field;
        });

        return fieldError ? ko.unwrap(fieldError.errorMessage) : '';
    };

    self.AddFieldError = function (field, message, scope) {
        scope = self.ResolveScope(scope);

        if (field && message && scope && ko.isObservableArray(scope.fieldValidationErrors)) {
            scope.fieldValidationErrors.push(ko.mapping.fromJS({
                field: field,
                errorMessage: message
            }));
        }
    };

    self.RemoveFieldError = function (field, scope) {
        scope = self.ResolveScope(scope);

        if (scope && ko.isObservableArray(scope.fieldValidationErrors)) {
            scope.fieldValidationErrors.remove(function (fieldError) {
                return ko.unwrap(fieldError.field) == field;
            });
        }
    };


    // Validator class
    // =========================

    self.Validator = function () {
        // Require a constructor call
        if (this == self) {
            return new self.Validator();
        }

        var validator = this;

        // Fields object of added fields
        validator.fields = {};

        // Available validation rules
        validator.rules = {
            required: function (value, overrideMessage) {
                return {
                    hasError: !value,
                    message: overrideMessage || "This field is required."
                };
            },
            validEmail: function (value, overrideMessage) {
                return {
                    hasError: !IsValidEmailAddress(value),
                    message: overrideMessage || "A valid email address is required."
                };
            },
            validRoutingNumber: function (value, overrideMessage) {
                return {
                    hasError: !IsValidRoutingNumber(value),
                    message: overrideMessage || "A valid routing number is required."
                };
            }
        };

        // Adds a field to the validator object to be tested
        validator.add = function (field, rule, options) {
            if (typeof rule != 'function') {
                if (!validator.rules.hasOwnProperty(rule) || typeof validator.rules[rule] != 'function')
                    throw "Invalid rule provided.";

                rule = validator.rules[rule];
            }

            // Other options:
            // - valueAccessor = function to return the value to validator
            // - scope = the scope of this field for error messages and base of path to field
            options = $.extend({
                message: "",
            }, options);

            if (typeof options.valueAccessor != 'function') {
                options.valueAccessor = function () {
                    return self.ResolveFieldName(field, options.scope);
                };
            }

            validator.fields[field] = {
                options: options,
                validate: function () {
                    var value = options.valueAccessor();
                    var result = rule(value, options.message);

                    if (result.hasError) {
                        self.AddFieldError(field, result.message, options.scope);
                    } else {
                        self.RemoveFieldError(field, options.scope);
                    }

                    return !result.hasError;
                }
            };

            return validator;
        };

        // Validates a given field
        validator.test = function (field) {
            if (!validator.fields.hasOwnProperty(field)) {
                throw "Invalid field provided.";
            }

            return validator.fields[field].validate();
        };

        // Used in knockout bindings for events
        validator.onEvent = function (field) {
            return function () {
                if (validator.fields.hasOwnProperty(field)) {
                    validator.fields[field].validate();
                }
            }
        };
    };


    // Address validator class
    // =========================

    self.AddressValidator = function (options) {
        // Require a constructor call
        if (this == self) {
            return new self.AddressValidator(options);
        }

        var validator = this;

        // status variables
        validator.isChoosing = ko.observable(false);
        validator.isValidating = ko.observable(false);

        // correction variables
        validator.address = ko.observable();
        validator.addresses = ko.observableArray();
        validator.selectedAddress = ko.observable();
        validator.lastValidatedAddress = null;
        validator.cache = {};

        validator.ziptasticFail = ko.observable(false);

        // used from validator.ValidateAddress to store the callbacks and use later
        var onKeepAddress;
        var onUseSelectedAddress;

        // validates an address object
        // Required options
        // options.onKeepAddress = function() {};
        // options.onUseSelectedAddress = function(selectedAddress) {};
        // options.onValidate = function(hasCorrections) {};
        validator.ValidateAddress = function (address, options) {
            if (!options) throw "Options are required.";
            if (typeof options.onKeepAddress != 'function') throw "onKeepAddress callback is required.";
            if (typeof options.onUseSelectedAddress != 'function') throw "onUseSelectedAddress callback is required.";
            if (typeof options.onValidate != 'function') throw "onValidate callback is required.";

            onKeepAddress = options.onKeepAddress;
            onUseSelectedAddress = options.onUseSelectedAddress;

            validator.isValidating(true);

            var json = {
                address1: StringToLower(address.address1),
                address2: StringToLower(address.address2),
                city: StringToLower(address.city),
                state: StringToLower(address.state),
                zipcode: StringToLower(address.zipCode)
            };

            var key = JSON.stringify(json);

            var HandleResult = function (addresses) {
                validator.isValidating(false);

                var hasCorrections = false;

                if (addresses && addresses.length) {
                    validator.address(address);
                    validator.addresses(addresses);
                    validator.selectedAddress(addresses[0]);

                    validator.isChoosing(true);

                    hasCorrections = true;
                }

                validator.lastValidatedAddress = $.extend({}, address);

                options.onValidate(hasCorrections);
            };

            if (validator.cache.hasOwnProperty(key)) {
                HandleResult(validator.cache[key].slice());
            } else {
                CallDWR('MyAccountTMXUIUtils/validateAddress', {
                    address: json
                }, function (data) {
                    var addresses;

                    if (data.model && data.model.alternativeAddresses && data.model.alternativeAddresses.length) {
                        // HACK: Remove alternativeAddresses results that contain null address fields
                        addresses = data.model.alternativeAddresses = data.model.alternativeAddresses.filter(function (alternativeAddress) {
                            return alternativeAddress &&
                                alternativeAddress.address1 && alternativeAddress.address1 != 'null null' &&
                                alternativeAddress.city && alternativeAddress.city != 'null' &&
                                alternativeAddress.state && alternativeAddress.state != 'null' &&
                                alternativeAddress.zipcode && alternativeAddress.zipcode != 'null';
                        });
                        // HACK: Change 'zipcode' to 'zipCode'
                        addresses = addresses.map(function (address) {
                            return {
                                address1: address.address1,
                                address2: address.address2,
                                city: address.city,
                                state: address.state,
                                zipCode: address.zipcode
                            };
                        });

                        validator.cache[key] = addresses.slice();
                    }

                    HandleResult(addresses);
                });
            }
        };

        // resets all data
        validator.Reset = function () {
            validator.isChoosing(false);
            validator.isValidating(false);
            validator.addresses.removeAll();
            validator.selectedAddress(null);
            validator.cache = {};
        };

        // ignores any corrections and uses what was input before
        validator.KeepAddress = function () {
            validator.isChoosing(false);

            if (typeof onKeepAddress == 'function') {
                onKeepAddress();
                onKeepAddress = undefined;
            }
        };

        // uses the selected correction address
        validator.UseSelectedAddress = function () {
            if (!validator.selectedAddress()) throw "No address is selected.";

            var address = validator.selectedAddress();

            validator.isChoosing(false);
            validator.lastValidatedAddress = address; // set the selected as last validated so we don't revalidate

            if (typeof onUseSelectedAddress == 'function') {
                onUseSelectedAddress();
                onUseSelectedAddress = undefined;
            }
        };

        // closes the selections area
        validator.Cancel = function () {
            validator.isChoosing(false);
        };

        // checks if this address has been validated
        validator.IsLastValidatedAddress = function (address) {
            return address && validator.lastValidatedAddress &&
                StringToLower(address.address1) == StringToLower(validator.lastValidatedAddress.address1) &&
                StringToLower(address.address2) == StringToLower(validator.lastValidatedAddress.address2) &&
                StringToLower(address.city) == StringToLower(validator.lastValidatedAddress.city) &&
                StringToLower(address.state) == StringToLower(validator.lastValidatedAddress.state) &&
                StringToLower(address.zipCode) == StringToLower(validator.lastValidatedAddress.zipCode);
        };
    };
}


/*
 * Funnel View Model
 *****************************************************************/
function FunnelViewModel() {
    var self = this;

    CommonViewModel.call(self);

    // ViewModel variables
    // =========================

    var STEP_ADDRESS = self.STEP_ADDRESS = 'step_address';
    var STEP_PRODUCTS = self.STEP_PRODUCTS = 'step_products';
    var STEP_SCHEDULE = self.STEP_SCHEDULE = 'step_schedule';
    var STEP_PAYMENT = self.STEP_PAYMENT = 'step_payment';
    var STEP_CONFIRMATION = self.STEP_CONFIRMATION = 'step_confirmation';

    var stepTextMap = {};

    stepTextMap[STEP_ADDRESS] = 'Address';
    stepTextMap[STEP_PRODUCTS] = 'Pricing';
    stepTextMap[STEP_SCHEDULE] = 'Schedule';
    stepTextMap[STEP_PAYMENT] = 'Pay';
    stepTextMap[STEP_CONFIRMATION] = 'Confirmation';

    var defaultStepOrder = [STEP_ADDRESS, STEP_PRODUCTS, STEP_SCHEDULE, STEP_PAYMENT, STEP_CONFIRMATION];

    var isShowStep = {};

    self.outOfSeasonMessage = ko.observable();

    defaultStepOrder.forEach(function (step) {
        isShowStep[step] = ko.observable(true);
    });

    self.IsShowStep = function (step) {
        return step in isShowStep ? isShowStep[step]() : true;
    };

    if (caniuse.sessionStorage) {
        var isShowStepJSON = sessionStorage.getItem("isShowStep");
        if (isShowStepJSON) {
            var json, parsed = false;

            try {
                json = JSON.parse(isShowStepJSON);
                parsed = true;
            } catch (e) {
                console.log("An invalid JSON string was set in isShowStep sessionStorage.");
            }

            if (json) {
                defaultStepOrder.forEach(function (step) {
                    isShowStep[step](!!json[step]);
                });
            } else if (parsed) {
                console.log("A non-object was set in isShowStep sessionStorage.");
            }
        }
    }

    var stepOrder = self.stepOrder = ko.computed(function () {
        return defaultStepOrder.filter(self.IsShowStep);
    });

    var currentStepObservable = ko.observable(defaultStepOrder[0]);
    var stepHistory = [];
    var stepCache = {};

    defaultStepOrder.forEach(function (step) {
        stepCache[step] = ko.observable(null);
    });

    self.submittingStep = ko.observable();

    if (caniuse.sessionStorage) {
        var stepCacheJSON = sessionStorage.getItem("stepCache");
        if (stepCacheJSON) {
            var json, parsed = false;

            try {
                json = JSON.parse(stepCacheJSON);
                parsed = true;
            } catch (e) {
                console.log("An invalid JSON string was set in stepCache sessionStorage.");
            }

            if (json) {
                defaultStepOrder.forEach(function (step) {
                    if (json[step]) {
                        stepCache[step](json[step]);
                    }
                });
            } else if (parsed) {
                console.log("A non-object was set in stepCache sessionStorage.");
            }
        }
    }

    /**
     * Clears step cache, within a range if params are provided
     *
     * @param options {Object} map of options to step cache clearing
     *                         - after: name of step -- cache will be cleared for all steps after this step
     *                         - until: name of step -- cache will be cleared up to this step, but not including this step
     *                         If both after and until are provided, then only steps between them will have their cache cleared
     */
    function ClearStepCache(options) {
        options = $.extend({
            after: '',
            until: ''
        }, options);

        var startIndex = options.after && (defaultStepOrder.indexOf(options.after) + 1) || 0;
        var stopIndex = options.until ? defaultStepOrder.indexOf(options.until) : -1;

        if (stopIndex == -1) {
            stopIndex = defaultStepOrder.length;
        }

        defaultStepOrder.slice(startIndex, stopIndex).forEach(function (step) {
            stepCache[step](null);
        });
    }

    // Wrapper so it cannot be changed outside of this view model
    self.currentStep = ko.computed(function () {
        return currentStepObservable();
    });

    var currentStepIndex = ko.computed(function () {
        return defaultStepOrder.indexOf(self.currentStep());
    });


    // Set the default affiliate path flag
    self.IsAffiliatePath = ko.observable(false);


    // Set the free inspection flag
    self.IsFreeInspectionPath = ko.computed(function () {
        if (!self.modelLoaded()) return false;

        return IsFreeInspectionInterest(self.model().interestArea())
    });


    self.squareFootageOptions = ko.observableArray([
        {
            optionText: 'Up to 3999',
            value: 3999
        },
        {
            optionText: '4,000-5,999',
            value: 5999
        },
        {
            optionText: '6,000-7,999',
            value: 7999
        },
        {
            optionText: 'Over 8,000',
            value: 8000
        }
    ]);

    self.noDatesChosen = ko.observable(false);
    self.scheduleSubmitLegalText = ko.computed(function () {
        var noDateSelectedText = "By clicking above, you agree to have a representative from your local branch contact you to schedule an appointment",
            dateSelectedText = "By clicking above, you understand that you are scheduling an appointment";

        return self.noDatesChosen() ? noDateSelectedText : dateSelectedText;
    });

    self.billingAddressSameAsServiceAddress = ko.observable(true);

    self.cardExpirationMonths = [];
    self.cardExpirationYears = [];

    for (var i = 1; i <= 12; i++) {
        self.cardExpirationMonths.push({
            label: GetMonthName(i),
            value: i
        });
    }

    for (var i = 0, y = (new Date()).getFullYear(); i < 10; i++) {
        self.cardExpirationYears.push(y + i);
    }


    // Field validation

    self.validator = new self.Validator();

    self.validator.add('customer.serviceProperty.address1', 'required');
    self.validator.add('customer.serviceProperty.zipCode', 'required');
    self.validator.add('customer.contact.email', 'validEmail');
    self.validator.add('customer.contact.phone', 'required');

    self.validator.add('customer.firstName', 'required');
    self.validator.add('customer.lastName', 'required');
    self.validator.add('payment.cardNumber', 'required');
    self.validator.add('payment.cardExpire', 'required');
    self.validator.add('payment.bankAccountNumber', 'required');
    self.validator.add('payment.bankRoutingNumber', 'validRoutingNumber');
    self.validator.add('customer.billingProperty.address1', 'required');
    self.validator.add('customer.billingProperty.city', 'required');
    self.validator.add('customer.billingProperty.state', 'required');
    self.validator.add('customer.billingProperty.zipCode', 'required');


    // Address validation

    self.addressValidator = new self.AddressValidator();


    // Model functions
    // =========================

    function GetStepURL(step) {
        //var url = "/test/funnel/",
        var url = "/buyonline/",
            page = "";

        switch (step) {
            case STEP_ADDRESS:
                page = 'address';
                break;
            case STEP_PRODUCTS:
                page = 'pricing';
                break;
            case STEP_SCHEDULE:
                page = 'schedule';
                break;
            case STEP_PAYMENT:
                page = 'payment';
                break;
            case STEP_CONFIRMATION:
                page = 'confirmation';
                break;
        }

        // Keep track of query params and hashes
        return url + page + window.location.search + window.location.hash;
    }

    function GetStepName(step) {
        var url = GetStepURL(step);
        return url.substr(url.lastIndexOf('/') + 1).charAt(0).toUpperCase() + url.substr(url.lastIndexOf('/') + 1).slice(1);
    }

    self.OnHistoryPop = function () {
        // Check if there is history
        if (stepHistory.length) {
            var step = stepHistory.pop();

            // Check if user can go to this step
            if (step in stepCache && stepCache[step]()) {
                // Show this step to the user
                UpdateModel(stepCache[step](), true);
            } else {
                // Find all available steps
                var availableSteps = defaultStepOrder.filter(function (step) {
                    return stepCache[step]();
                });

                //console.log('availableSteps', availableSteps);

                // If there are no available steps, then something broke so just reload the funnel
                if (!availableSteps.length) {
                    window.location = GetStepURL(defaultStepOrder[0]);
                    window.location.reload();
                    return;
                }

                // Move user to highest available step
                UpdateModel(stepCache[availableSteps.pop()](), true);
            }

            // Set the new state
            if (g_isHistorySupported) {
                history.pushState(stepHistory, "", GetStepURL(self.currentStep()));
            }
        } else {
            //console.log('no step history length');
            // If there is not history, then they are at the very beginning
            // Take them beck to where they came from
            if (g_isHistorySupported) {
                // Remove the popstate event so we don't infinite loop calling history.back()
                //window.onpopstate = function(){};
                //history.back();
            }
        }
    };

    /**
     * Updates the session storage with the latest funnel model and step cache
     *
     * @param options {Object} map of options of what to store in session storage
     *                         - model: object to store for "funnelModel" -- defaults to GetModelJSON()
     *                         - stepCache: object to store for "stepCache" -- defaults to stepCache variable
     *                         - isShowStep: object to store for "isShowStep" -- defaults to isShowStep variable
     *
     *                         objects will be ran through ko.mapping.toJS() to resolve any observables before
     *                         being ran through JSON.stringify()
     */
    function UpdateSessionStorage(options) {
        options = $.extend({
            model: GetModelJSON(),
            stepCache: stepCache,
            isShowStep: isShowStep
        }, options);

        if (caniuse.sessionStorage) {
            sessionStorage.setItem("funnelModel", JSON.stringify(ko.mapping.toJS(options.model)));
            sessionStorage.setItem("stepCache", JSON.stringify(ko.mapping.toJS(options.stepCache)));
            sessionStorage.setItem("isShowStep", JSON.stringify(ko.mapping.toJS(options.isShowStep)));
        }
    }

    var GA_CATEGORY_FUNNEL = "TMX Funnel";

    function FireFunnelGAEvent(category, action, label, value) {
        category = typeof category == 'string' && category || GA_CATEGORY_FUNNEL;

        FireGAEvent(category, action, label, value);

        if (self.model() && self.model().tiered()) {
            category += ": Tiered";
        } else {
            category += ": Standard";
        }

        FireGAEvent(category, action, label, value);
    }


    function FireAffiliateGAEvent(affiliate, label) {
        if (!affiliate.id || !affiliate.name) {
            console.log('[FireAffiliateGAEvent] Affiliate name and id are required!');
        }

        var action = '[' + affiliate.id + '] ' + affiliate.name;

        FireGAEvent('Affiliate Tracking', action, label, '');
    }


    function FireFreeInspectionConversion() {
        var label = '';

        switch (self.model().interestArea()) {
            case 'ATTIC INSULATION':
                label = 'Attic Insulation';
                break;

            case 'BED BUGS':
                label = 'Bed Bugs';
                break;

            case 'CRAWL SPACE':
                label = 'Crawl Space';
                break;

            case 'RODENTS':
            case 'WILDLIFE':
                label = 'Wildlife';
                break;

            case 'TERMITES':
                label = 'Termite';
                break;
        }

        FireFunnelGAEvent('Inspection Schedule', 'Form Submit Success', label, '');
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Free Inspection', 'Success: ' + label, '');
    }


    function GetModelJSON() {
        // Get the latest model data
        var json = ko.mapping.toJS(self.model) || {};

        // Set current step in case it was tampered on the model
        if (json) {
            json.currentStep = self.currentStep();
        }

        return json;
    }


    function UpdateModel(model, ignoreHistory) {

        // Extend from the current model to the new model, so we retain products and settings
        model = $.extend(GetModelJSON(), model);


        // Check if there is a redirect
        if (model.redirectRequest && model.redirectURL) {
            window.location = model.redirectURL;
            return;
        }


        // If no step is set, start at step 1
        if (!model.currentStep) {
            model.currentStep = defaultStepOrder[0];
        }


        var isChangingSteps = model.currentStep != self.currentStep();


        // Check if step changed and we should update in history
        if (g_isHistorySupported && !ignoreHistory) {
            // Update the history trail
            if (isChangingSteps || !stepHistory.length) {
                stepHistory.push(self.currentStep());
            }

            stepCache[model.currentStep]($.extend(true, {}, model));

            history.replaceState(stepHistory, "", GetStepURL(model.currentStep));
        }


        // Backwards compatibility for property on customer changing to serviceProperty
        if (model.customer && !model.customer.serviceProperty && model.customer.property) {
            model.customer.serviceProperty = model.customer.property;
        }


        // Initialize available products array
        if (!model.availableProducts) {
            model.availableProducts = [];
        }


        // Set the commercial affiliate if necessary
        if (!self.IsAffiliatePath() && !IsObjectEmpty(model.commercialAffiliate)) {
            self.IsAffiliatePath(true);
        }


        // Do any step-specific stuff for the model before updating the model on the page
        switch (model.currentStep) {
            case STEP_ADDRESS:
                model.interestArea = GetInterestArea();
                model.showUpsell = false;

                // Check for Free Inspection path
                if (IsFreeInspectionInterest(model.interestArea)) {
                    isShowStep[STEP_PRODUCTS](false);
                    isShowStep[STEP_PAYMENT](false);
                }
                break;


            case STEP_PRODUCTS:
                // Create the available products list from the returned product map
                if (model.productItemMap && !model.availableProducts.length) {

                    for (var productId in model.productItemMap) {
                        var product = model.productItemMap[productId];

                        if (!product.templateCode || !product.productCode) {
                            continue;
                        }

                        product.isDisabled = false;
                        product.isALaCarte = self.IsALaCarteProduct(product, model.tiered, model.interestArea);
                        product.isTiered = self.IsTieredProduct(product);

                        // Parse all values into floats
                        product.productCost = parseFloat(product.productCost) || 0.0;
                        product.discount = parseFloat(product.discount) || 0.0;
                        product.taxes = parseFloat(product.taxes) || 0.0;
                        product.startUpCostTotal = parseFloat(product.startUpCostTotal) || product.productCost - product.discount + product.taxes;
                        product.frequencyCost = parseFloat(product.frequencyCost) || 0.0;

                        // Add product total without discounts for visual
                        if (product.discount > 0) {
                            product.startUpCostTotalWithoutDiscount = product.productCost;

                            if (product.taxes) {
                                var taxRate = product.taxes / (product.productCost - product.discount);

                                // round the thousandth's decimal place up and truncate to only 2 decimals
                                product.startUpCostTotalWithoutDiscount += Math.ceil(product.productCost * taxRate * 100) / 100;
                            }
                        } else {
                            product.startUpCostTotalWithoutDiscount = product.startUpCostTotal;
                        }

                        // Populate frequency fields if they don't exist (backwards compatibility)
                        if (!product.paymentFrequency) product.paymentFrequency = '';
                        if (!product.frequencyCost) product.frequencyCost = 0;

                        // Use this if the payment frequency value is not user-friendly to map to a new user-friendly string
                        product.paymentFrequencyDisplay = self.GetPaymentFrequencyDisplay(product.paymentFrequency);

                        // Add product as available product
                        model.availableProducts.push(product);
                    }



                    // Only continue if available products were found in the map
                    if (model.availableProducts.length) {

                        // Grab the target pest product object
                        var targetPestItem = model.availableProducts.find(function (product) {
                            return product.productCode === model.targetPest;
                        });

                        // Move target pest item to top of available products array
                        if (targetPestItem) {
                            model.availableProducts = model.availableProducts.filter(function (product) {
                                return product.productCode !== model.targetPest;
                            });

                            model.availableProducts.unshift(targetPestItem);
                        }


                        // Place items in cart that need to be by default (only occurs on product step as a session model will already have the items set)
                        model.productIdCart = [];
                        model.availableProducts.forEach(function (product) {
                            if (product.itemInCart) {
                                model.productIdCart.push(product.productId);
                            }


                            // TODO: add error logic for if an item failed to add to cart upon submitting product step
                        });
                    }
                }


                // Set the billing address as service address
                model.customer.billingProperty = model.customer.serviceProperty;
                break;


            case STEP_SCHEDULE:
                var foundSelectedDate = false;

                model.availableDates = [];

                if (model.serviceSchedule && model.serviceSchedule.length) {
                    for (var i = 0; i < model.serviceSchedule.length; i++) {
                        var dateObj = {
                                dateString: '',
                                availableTimes: []
                            },
                            loopSchedule = model.serviceSchedule[i];

                        dateObj.dateString = loopSchedule.date;

                        for (var t = 0; t < loopSchedule.serviceTimeSlots.length; t++) {
                            dateObj.availableTimes.push(loopSchedule.serviceTimeSlots[t].schFrmTime);
                        }

                        model.availableDates.push(dateObj);
                    }
                } else {
                    self.noDatesChosen(true);
                }

                model.availableTimes = [];

                if (self.noDatesChosen()) {
                    model.selectedDate = 'NONE';
                    model.timeSlotSelectedKey = 'NONE';
                    model.selectedTime = '';
                    foundSelectedDate = true;
                }

                if (model.availableDates && model.availableDates.length) {
                    model.availableDates.forEach(function (availableDate) {
                        if (!foundSelectedDate && availableDate.dateString == model.selectedDate) {
                            foundSelectedDate = true;

                            if (!availableDate.availableTimes || !availableDate.availableTimes.length || availableDate.availableTimes.indexOf(model.selectedTime) == -1) {
                                model.selectedTime = '';
                            }
                        }

                        if (availableDate.availableTimes && availableDate.availableTimes.length) {
                            availableDate.availableTimes.forEach(function (time) {
                                if (model.availableTimes.indexOf(time) == -1) {
                                    model.availableTimes.push(time);
                                }
                            });
                        }
                    });
                }

                // Sort the dates/times
                model.availableDates.sort(function (a, b) {
                    return new Date(a.dateString) - new Date(b.dateString);
                });

                model.availableTimes.sort(function (a, b) {
                    return parseInt(a.substr(0, 2)) - parseInt(b.substr(0, 2));
                });


                if (!foundSelectedDate) {
                    model.selectedDate = '';
                    model.selectedTime = '';
                }
                break;


            case STEP_PAYMENT:
                if (model.openSlotSelected) {
                    model.selectedDate = model.openSlotSelected.schDate;
                    model.selectedTime = model.openSlotSelected.schFrmTime;
                } else {
                    model.selectedDate = 'NONE';
                    model.selectedTime = '';
                }

                if (!model.payment.paymentMethod) {
                    model.payment.paymentMethod = 'card';
                    model.payment.cardType = '';
                }
                break;


            case STEP_CONFIRMATION:
                if (model.openSlotSelected) {
                    model.selectedDate = model.openSlotSelected.schDate;
                    model.selectedTime = model.openSlotSelected.schFrmTime;
                } else {
                    model.selectedDate = 'NONE';
                    model.selectedTime = '';
                }
                break;
        }


        // Remember product contents because we want to keep them as an observable
        // But they are objects, so ko.mapping.fromJS() won't make them an observable
        var productContentMap = {};

        model.availableProducts.forEach(function (product) {
            // Get the content for the product from endeca
            if (product.content) {
                productContentMap[product.productId] = product.content;
            }

            // Set to null so it becomes an observable that can be updated later
            product.content = null;
            product.contentFailed = false;
        });


        // Unset the current step so no bindings are using the model while we update it
        currentStepObservable('');


        // Update the model object on the page
        self.model(ko.mapping.fromJS(model));
        self.modelLoaded(true);

        // Update any specific observables from the model
        currentStepObservable(model.currentStep);


        // Do any step-specific stuff for the model after the page has been update with new step
        switch (model.currentStep) {
            case STEP_ADDRESS:
                // Determine if we need to show the sqft dropdown to the user in Step-1
                if (_ENABLE_PROPERTY_INFORMATION_TOGGLE) {
                    $('.sqft-block').hide();
                } else {
                    $('.sqft-block').show();
                }
                break;

            case STEP_PRODUCTS:
                break;
            case STEP_SCHEDULE:
                // Reset selected time whenever date changes; update the timeSlotSelectedKey if NONE is selected
                self.model().selectedDate.subscribe(function (value) {
                    self.model().selectedTime('');

                    if (value === 'NONE') {
                        self.model().timeSlotSelectedKey(value);
                    }
                });

                // Reset selected time whenever date changes; update the timeSlotSelectedKey with the appropriate slot key
                self.model().selectedTime.subscribe(function (value) {
                    // Match the date object
                    var dateMatch = ko.utils.arrayFirst(self.model().serviceSchedule(), function (item) {
                        return self.model().selectedDate() === item.date();
                    });


                    if (dateMatch) {
                        // Match the time
                        var timeMatch = ko.utils.arrayFirst(dateMatch.serviceTimeSlots(), function (item) {
                            return self.model().selectedTime() === item.schFrmTime();
                        });

                        // Set the slot key
                        if (timeMatch) {
                            self.model().timeSlotSelectedKey(timeMatch.schSlotKey());
                        }
                    }
                });

                // Also update selectedDate if noDatesChosen flag is NONE
                self.noDatesChosen.subscribe(function (value) {
                    if (value === true) {
                        self.model().selectedDate('NONE');
                        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Links and Buttons', 'Checkbox: None of these dates works', '');
                    } else {
                        self.model().selectedDate('');
                    }
                });
                break;

            case STEP_PAYMENT:
                self.model().payment.cardNumber.subscribe(function (value) {
                    var cardType = GetCreditCardType(value);

                    if (cardType) self.model().payment.cardType(cardType.value);
                });

                if (self.model().payment.cardNumber()) {
                    self.model().payment.cardNumber.valueHasMutated();
                }
                // Check if same as billing has been unchecked
                self.billingAddressSameAsServiceAddress.subscribe(function (value) {
                    if (value === false) {
                        var billingZip = self.model().customer.billingProperty.zipCode();
                        self.model().customer.billingProperty.zip9Code(billingZip);
                        self.model().customer.billingProperty.lotSize(null);
                        self.model().customer.billingProperty.squareFootage(null);
                        self.model().customer.billingProperty.addressVerified(false);
                    }
                });

                break;

            case STEP_CONFIRMATION:
                // Clear out the session storage
                if (caniuse.sessionStorage) {
                    sessionStorage.clear();
                }
                break;
        }


        // Pull endeca content for products if we need to
        self.model().availableProducts().forEach(function (product) {
            if (product.productId() in productContentMap) {
                product.content(productContentMap[product.productId()]);
            } else {
                self.GetProductContentFromEndeca(product.productCode(), product.templateCode(), function (json) {

                    // Loop through all content and replace any template strings with values
                    if (!json || !json.content || json.content.error) {

                        product.contentFailed(true);

                    } else {

                        var unmappedProduct = ko.mapping.toJS(product);

                        for (var c in json.content) {
                            if (!json.content.hasOwnProperty(c)) {
                                continue;
                            }

                            if (IsObject(json.content[c])) {
                                for (var item in json.content[c]) {
                                    if (!json.content[c].hasOwnProperty(item)) {
                                        continue;
                                    }

                                    json.content[c][item] = self.ReplacePricingTemplateKeys(json.content[c][item], unmappedProduct);
                                }
                            } else {
                                json.content[c] = self.ReplacePricingTemplateKeys(json.content[c], unmappedProduct);
                            }
                        }


                        // Put the content on the product
                        product.content(json.content);


                        // Update the session storage with the new model
                        if (caniuse.sessionStorage && self.currentStep() !== STEP_CONFIRMATION) {
                            for (var step in stepCache) {
                                var json = stepCache[step]();

                                if (json && json.availableProducts) {
                                    json.availableProducts.some(function (jsonProduct) {
                                        if (jsonProduct.productId == product.productId()) {
                                            jsonProduct.content = product.content();
                                            return true;
                                        }
                                    });
                                }
                            }

                            UpdateSessionStorage();
                        }
                    }
                });
            }
        });


        // Save the model and cache to session if possible
        if (self.currentStep() !== STEP_CONFIRMATION && caniuse.sessionStorage) {
            UpdateSessionStorage();
        }


        // Run actions
        if (self.model().actionList && self.model().actionList() && self.model().actionList().length) {
            // TODO: create and implement an actions controller
            for (var a = 0; a < self.model().actionList().length; a++) {
                var action = self.model().actionList()[a];

                switch (action) {
                    case 'INVALID_EMAIL':
                        new CommonModal({
                            title: 'Email address verification error',
                            body: '<p>Sorry, we were unable to verify your email address <strong>' + self.model().customer.contact.email() + '</strong>. If this is the correct email address, please continue as is to resubmit, otherwise you may edit your email address and try again.</p>',
                            confirm: true,
                            btnCancelText: 'Edit email',
                            btnConfirmText: 'Continue as is'
                        }, function () {
                            self.model().customer.contact.emailVerified(true);
                            self.SubmitStep();
                        });
                        break;

                    case 'INVALID_ADDRESS':
                        var addressline2, fullAddress, invalidType;

                        if (!self.model().customer.serviceProperty.addressVerified()) {
                            addressline2 = self.model().customer.serviceProperty.address2() != null ? self.model().customer.serviceProperty.address2() : '';
                            fullAddress = self.model().customer.serviceProperty.address1() + ' ' + addressline2 + ' ' + self.model().customer.serviceProperty.zipCode();
                            invalidType = 'service';

                        } else if (!self.model().customer.billingProperty.addressVerified()) {
                            addressline2 = self.model().customer.billingProperty.address2() != null ? self.model().customer.billingProperty.address2() : '';
                            fullAddress = self.model().customer.billingProperty.address1() + ' ' + addressline2 + ' ' + self.model().customer.billingProperty.zipCode();
                            invalidType = 'billing';
                        }

                        new CommonModal({
                            title: 'Address verification error',
                            body: '<p>Sorry, we were unable to verify your ' + invalidType + ' address <strong>' +
                                fullAddress + '</strong>. If this is the correct address, please continue as is to resubmit, otherwise you may edit your address and try again.</p>',
                            confirm: true,
                            btnCancelText: 'Edit address',
                            btnConfirmText: 'Continue as is'

                        }, function () {
                            if (invalidType === 'service') {
                                self.model().customer.serviceProperty.addressVerified(true);
                            } else if (invalidType === 'billing') {
                                self.model().customer.billingProperty.addressVerified(true);
                            }

                            self.SubmitStep();
                        });

                        self.model().actionList().pop();
                        break;

                    case 'INVALID_PHONE':
                        break;
                    case 'DISABLE_SUBMIT_ORDER':
                        if (self.model().currentStep() == STEP_PAYMENT) {
                            $('.cart .card-footer').hide();
                        } else if (self.model().currentStep() == STEP_SCHEDULE) {
                            $('#submit-schedule-step').hide();
                        }
                        break;
                }
            }
        }


        if (isChangingSteps) {
            // Scroll to top of the page to show this step
            ScrollTo(0, 'fast');

            // Fire page view
            try {
                utag.view({
                    ga_page_including_hash: window.location.pathname + window.location.search + window.location.hash
                });
            } catch (e) {
                console.log(e);
            }

        }
    }


    // Address functions
    // =========================
    self.HasAddressUnit = ko.computed(function (property) {
        if (self.modelLoaded() && self.model()) {
            var _property = ko.unwrap(property);
            if (_property && ko.unwrap(_property.address2)) {
                return true;
            }
        }

        return false;
    });


    // Product functions
    // =========================

    /**
     * Checks if available products exist
     *
     * @return {Boolean}
     */
    self.HasAvailableProducts = ko.computed(function () {
        return self.modelLoaded() && self.model() && self.model().availableProducts && self.model().availableProducts().length;
    });


    self.IsMosquitoInSeason = ko.computed(function () {
        var inSeason = false;
        if (self.HasAvailableProducts() && self.model().interestArea() === "MOSQUITOES")
            for (var p = 0; p < self.model().availableProducts().length; p++) {
                var product = ko.mapping.toJS(self.model().availableProducts()[p]);
                if (product.productCode === "MOSQ") inSeason = true
            } else inSeason = true;
        return inSeason
    });

    /**
     * Mosquito Season Check. Checks if Mosquito is available for mosquito customer. 
     *
     * @return {Boolean}
     */
    self.ShowMosquitoOutOfSeason = ko.computed(function () {
        var showMessage = true;
        var cartHasMosqOneTime = false;
        if (self.HasAvailableProducts() && self.model().interestArea() === 'MOSQUITOES' &&
            self.model().tiered() === false) {
            // Check to see if any of the products are Mosquito Products
            for (var p = 0; p < self.model().availableProducts().length; p++) {
                var product = ko.mapping.toJS(self.model().availableProducts()[p]);

                if (product.productCode === 'MOSQ') {
                    showMessage = false;
                    self.outOfSeasonMessage = '';
                }

                if (product.productCode === 'MOSQ_ONETIME') {
                    cartHasMosqOneTime = true;
                }
            }

        } else {
            showMessage = false;
            self.outOfSeasonMessage = '';
        }
        // Determine what the out of season message should be
        if (showMessage) {
            if (cartHasMosqOneTime) {
                self.outOfSeasonMessage = 'Unfortunately, the monthly mosquito product is not available for purchase, because we are outside the purchase window. However, we do have a one-time Mosquito service available for purchase. Please check back during Mosquito season, or call 1.877.837.6464. Please see below for general pest control (not including mosquito).';
            } else {
                self.outOfSeasonMessage = 'Unfortunately, the monthly mosquito product is not available for purchase, because we are outside the purchase window. Please check back during Mosquito season, or call 1.877.837.6464. Please see below for general pest control (not including mosquito).'
            }
        }
        return showMessage;
    });





    /**
     * Checks if the products have content
     *
     * @return {Boolean}
     */
    self.ProductsHaveContent = ko.computed(function () {
        return self.HasAvailableProducts() && self.model().availableProducts().every(function (product) {
            return product.content();
        });
    });


    /**
     * Checks if the products failed to load content
     *
     * @return {Boolean}
     */
    self.ProductsHaveFailedContent = ko.computed(function () {
        return self.HasAvailableProducts() && self.model().availableProducts().some(function (product) {
            return product.contentFailed();
        });
    });


    /**
     * Checks if any of the products have a discount
     *
     * @return {Boolean}
     */
    self.ProductHasDiscount = ko.computed(function () {
        // If no products return false
        if (!self.HasAvailableProducts()) return false;

        // Check if each product has a discount
        for (var p = 0; p < self.model().availableProducts().length; p++) {
            var product = ko.mapping.toJS(self.model().availableProducts()[p]);

            if (product.discount > 0) {
                return true;
            }
        }

        // None of the products have a discount
        return false;
    });


    /**
     * Checks if the passed product has a ribbon to display
     *
     * @param {Object} product The product to check
     * @return {Boolean}
     */
    self.ProductHasRibbon = function (product) {
        if (!product) return false;

        var unmappedProduct = ko.mapping.toJS(product);

        return unmappedProduct && unmappedProduct.content.tiered.hasOwnProperty('ribbon');
    };


    /**
     * Checks if the passed product should be a la carte
     *
     * @param {Object} product The product to check
     * @param {Boolean} tiered If the path is tiered or not
     * @return {Boolean}
     */
    self.IsALaCarteProduct = function (product, tiered, interestArea) {
        tiered = tiered || false;
        interestArea = interestArea || self.model().interestArea();
        if (!product) return false;


        // If mosquito interest area and product is mosquito, fail
        if (interestArea === 'MOSQUITOES' && product.productId === '10003') {
            return false;
        }


        // If tiered product, fail
        if (product.productId === '10005' || product.productId === '10006' || product.productId === '10007') {
            return false;
        }


        // If free inspection, fail
        if (product.productId === '10001') {
            return false;
        }

        return tiered;
    };


    /**
     * Checks if the passed product is tiered
     *
     * @param {Object} product The product to check
     * @return {Boolean}
     */
    self.IsTieredProduct = function (product) {
        if (!product) return false;

        return product.productId === '10005' || product.productId === '10006' || product.productId === '10007';
    };

    /**
     * Returns product payment frequency display value for payment frequency code.
     *
     * @param {String} paymentFrequencyCode
     * @return {String}
     */
    self.GetPaymentFrequencyDisplay = function (paymentFrequencyCode, capitalized) {
        if (capitalized === undefined) capitalized = true;
        switch (paymentFrequencyCode) {
            case 'QTR':
                var paymentFrequencyDisplay = 'quarterly';
                break;
            case 'MTHLY':
                var paymentFrequencyDisplay = 'monthly';
                break;
            case 'BIMO':
                var paymentFrequencyDisplay = 'bi-monthly';
                break;
            case 'ONCE':
                var paymentFrequencyDisplay = 'one-time';
                break;
            default:
                var paymentFrequencyDisplay = paymentFrequencyCode;
        }
        if (capitalized) {
            return paymentFrequencyDisplay.charAt(0).toUpperCase() + paymentFrequencyDisplay.slice(1);
        } else {
            return paymentFrequencyDisplay;
        }

    };


    /**
     * Returns product payment frequency per value (e.g. "month" for "per month") for payment frequency code.
     *
     * @param {String} paymentFrequencyCode
     * @return {String}
     */
    self.GetPaymentFrequencyPerValue = function (paymentFrequencyCode) {
        switch (paymentFrequencyCode) {
            case 'QTR':
                var paymentFrequencyPerValue = 'quarter';
                break;
            case 'MTHLY':
                var paymentFrequencyPerValue = 'month';
                break;
            case 'BIMO':
                var paymentFrequencyPerValue = 'two months';
                break;
            default:
                var paymentFrequencyPerValue = paymentFrequencyCode;
        }
        return paymentFrequencyPerValue;
    };


    /**
     * Retrieves the content from endeca for a product
     *
     * @param {String} pc The product code of the product
     * @param {String} tc The template code of the product
     * @param {Function} callback The function to call when the items have returned
     */
    self.GetProductContentFromEndeca = function (pc, tc, callback) {
        if (!pc || !tc || !callback) {
            console.log('[GetProductContentFromEndeca] Product code, Template code, and callback are required.');
            callback({});
            return;
        }

        var data = {
            productCode: pc,
            templateCode: tc,
            folder: 'Funnel'
        };

        GetEndecaContent(data, callback);
    };


    /**
     * Replaces the template keys inside a string to prices
     *
     * @param {String} content The string to replace the template keys of
     * @param {Object} product The product to use for pricing
     * @return {String} The content with template keys replaced
     */
    self.ReplacePricingTemplateKeys = function (content, product) {
        if (!content || typeof content !== 'string' || !product) {
            return;
        }


        // Define which template keys we support for the content
        var templateKeys = {};

        // Initial price tag (original price - discounted price)
        templateKeys.initialPriceTag = '$' + FormatNumber(product.productCost - product.discount, 0);

        // Follow-up price tag for service
        templateKeys.followUpPriceTag = '$' + FormatNumber(product.frequencyCost, 0);

        // Replace any template strings in the content
        for (var templateKey in templateKeys) {
            var templateKeyStr = "{{" + templateKey + "}}";
            var templateReplacement = templateKeys[templateKey];

            content = content.replaceAll(templateKeyStr, templateReplacement);
        }

        return content;
    };


    // Cart functions
    // =========================

    // Returns whether to show to the cart or not
    //  - more possible logic to be added later
    self.CartIsShowing = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return false;

        var ret = false;

        switch (self.currentStep()) {
            case STEP_ADDRESS:
                break;

            case STEP_PRODUCTS:
                ret = self.IsFreeInspectionPath() ? self.ProductsHaveContent() && self.model().interestArea() === 'TERMITES' : self.ProductsHaveContent();
                break;

            case STEP_SCHEDULE:
                ret = self.ProductsHaveContent() && self.HasProductsInCartWithPrice();
                break;

            case STEP_PAYMENT:
                ret = self.ProductsHaveContent();
                break;

            case STEP_CONFIRMATION:
                break;
        }

        return ret;
    });


    // Returns the text for the primary CTA in the cart
    self.GetCartCTAText = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return '';

        switch (self.currentStep()) {
            case STEP_ADDRESS:
            case STEP_PRODUCTS:
            case STEP_SCHEDULE:
            case STEP_CONFIRMATION:
                var ret = 'Next';
                break;

            case STEP_PAYMENT:
                var ret = 'Submit';
                break;
        }

        return ret;
    });


    // Returns the text for the primary CTA in the cart when submitting
    self.GetSubmittingCartCTAText = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return '';

        switch (self.currentStep()) {
            case STEP_PRODUCTS:
                var ret = 'Loading scheduling...';
                break;

            case STEP_SCHEDULE:
                var ret = self.submittingStep() == STEP_PRODUCTS ? 'Loading scheduling...' : 'Loading payment...';
                break;

            case STEP_PAYMENT:
                var ret = 'Purchasing...';
                break;

            case STEP_ADDRESS:
            case STEP_CONFIRMATION:
                var ret = '';
                break;
        }

        return ret;
    });


    // Returns whether a user can remove items from the cart
    self.CanRemoveItemsFromCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model()) return false;

        // This is initial setup for more logic later (ex: up-sell sections)
        switch (self.currentStep()) {
            case STEP_ADDRESS:
            case STEP_SCHEDULE:
            case STEP_PAYMENT:
            case STEP_CONFIRMATION:
                var ret = false;
                break;

            case STEP_PRODUCTS:
                var ret = self.submittingStep() !== STEP_PRODUCTS;
                break;
        }

        return ret;
    });


    /**
     * Handles the setup to add/remove items to/from the cart
     *
     * @param item The product to add or remove
     */
    self.HandleAddRemoveProductFromCart = function (item) {
        if (!item) return;


        var product = ko.mapping.toJS(item);

        // If product is in cart, remove it
        if (self.IsProductInCart(product)) {
            self.RemoveProductFromCart(product);

            // If product is not in cart add it
        } else {

            // Check the cart for product combinations and rules
            if (!self.ProductTriggeredCartRule(product)) {

                // If there isn't a rule trigger, add the item to cart
                self.AddProductToCart(product);
            }
        }


        // Sets all future steps to null
        // This is if the user went from schedule/pay back to products, and removed/added an item, they need to pull schedule again
        ClearStepCache({
            after: self.currentStep()
        });
    };


    /**
     * Adds a product to the cart
     *
     * @param product The product to add to the cart
     */
    self.AddProductToCart = function (product) {
        if (!product && !product.productId) return;

        self.model().productIdCart.push(product.productId);

        if (self.IsFreeInspectionPath()) {
            isShowStep[STEP_PAYMENT](true);
        }
    };


    /**
     * Removes a product from the cart
     *
     * @param product The product to remove from the cart
     */
    self.RemoveProductFromCart = function (product) {
        if (!product && !product.productId) return;

        self.model().productIdCart.remove(product.productId);

        if (self.IsFreeInspectionPath()) {
            isShowStep[STEP_PAYMENT](false);
        }

        self.HandleEnableProductsAfterRemove(product.productId);
    };

    /**
     * Checks if tiered product is in cart
     * @return {Boolean} true if tiered product in cart.
     */
    self.IsTieredProductInCart = function () {
        return self.model().productIdCart().some(function (productId) {
            return productId === '10005' || productId === '10006' || productId === '10007';
        });
    };

    /**
     * Checks if alacarte product is in cart
     * @return {Boolean} true if alacarte product in cart.
     */
    self.IsALaCarteProductInCart = function () {
        return self.productsInCart().some(function (product) {
            return product.isALaCarte();
        });
    };

    /**
     * Checks if only one-time products in cart.
     * @return {Boolean} true if only one-time products are in cart.
     */
    self.IsOnlyOnetimesInCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model() || !self.model().productItemMap) return false;
        return !self.model().productIdCart().some(function (productId) {
            return self.model().productItemMap[productId] && self.model().productItemMap[productId].paymentFrequency() !== 'ONCE';
        });
    });

    /**
     * Checks if any paid item exist in cart.
     * @return {Boolean} true if any paid item is in cart.
     */
    self.IsPaidItemInCart = ko.computed(function () {
        if (!self.modelLoaded() || !self.model() || !self.model().productItemMap) return false;
        return self.model().productIdCart().some(function (productId) {
            return productId !== '10001';
        });
    });

    // Default to a rule set if one does not exist in the global ui settings object
    var _PRODUCT_RULESET = {
        "10001": {
            "_name": "Free Termite Inspection",
            "disabled": [],
            "swap": [
                "10004",
                "10006",
                "10007"
            ],
            "bundles": []
        },
        "10002": {
            "_name": "Genpest products (quarterly)",
            "disabled": [],
            "swap": [
                "10005"
            ],
            "bundles": []
        },
        "10003": {
            "_name": "Mosquito (ATSB / Quick Guard)",
            "disabled": [],
            "swap": [
                "10009"
            ],
            "bundles": [
                {
                    "productsInCart": [
                        "10004",
                        "10005"
                    ],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                },
                {
                    "productsInCart": [
                        "10006"
                    ],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
                }
            ]
        },
        "10004": {
            "_name": "Termite one time",
            "disabled": [],
            "swap": [
                "10001"
            ],
            "bundles": [
                {
                    "productsInCart": [
                        "10003",
                        "10005"
                    ],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                },
                {
                    "productsInCart": [
                        "10005"
                    ],
                    "productToUpgradeTo": "10006",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                },
                {
                    "productsInCart": [
                        "10008"
                    ],
                    "productToUpgradeTo": "10006",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                }
            ]
        },
        "10005": {
            "_name": "Silver plan",
            "disabled": [],
            "swap": [
                "10002",
                "10006",
                "10007",
                "10008"
            ],
            "bundles": [
                {
                    "productsInCart": [
                        "10003",
                        "10004"
                    ],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                },
                {
                    "productsInCart": [
                        "10004"
                    ],
                    "productToUpgradeTo": "10006",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                }
            ]
        },
        "10006": {
            "_name": "Gold plan",
            "disabled": [
                {
                    "productToDisable": "10002",
                    "message": false
                },
                {
                    "productToDisable": "10004",
                    "message": true
                },
                {
                    "productToDisable": "10008",
                    "message": false
                }
            ],
            "swap": [
                "10001",
                "10002",
                "10004",
                "10005",
                "10007",
                "10008"
            ],
            "bundles": [
                {
                    "productsInCart": [
                        "10003"
                    ],
                    "productToUpgradeTo": "10007",
                    "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
                }
            ]
        },
        "10007": {
            "_name": "Platinum plan",
            "disabled": [
                {
                    "productToDisable": "10002",
                    "message": false
                },
                {
                    "productToDisable": "10003",
                    "message": true
                },
                {
                    "productToDisable": "10004",
                    "message": true
                },
                {
                    "productToDisable": "10008",
                    "message": false
                }
            ],
            "swap": [
                "10001",
                "10002",
                "10003",
                "10004",
                "10005",
                "10006",
                "10008"
            ],
            "bundles": []
        },
        "10008": {
            "_name": "Genpest one time",
            "disabled": [],
            "swap": [
                "10005"
            ],
            "bundles": []
        },
        "10009": {
            "_name": "Mosquito one time",
            "disabled": [],
            "swap": [
                "10003"
            ],
            "bundles": []
        }
    };


    if (typeof _GLOBAL_UI_SETTINGS != 'undefined' && _GLOBAL_UI_SETTINGS && _GLOBAL_UI_SETTINGS.productRuleSet) {
        _PRODUCT_RULESET = _GLOBAL_UI_SETTINGS.productRuleSet;
    }



    /**
     * Checks for a cart rule and returns if one is triggered or not
     *
     * @param {Object} productToAdd The product being added to cart by the user
     * @return {Boolean}
     **/
    self.ProductTriggeredCartRule = function (productToAdd) {
        var productsInCart = ko.mapping.toJS(self.model().productIdCart()), // Array of product ids
            rules = _PRODUCT_RULESET[productToAdd.productId];


        // Check for bundle upgrades
        for (var b = 0; b < rules.bundles.length; b++) {
            var bundle = rules.bundles[b];

            if (ArrayContainsArray(productsInCart, bundle.productsInCart)) {
                // Define which template keys we support for the content
                var templateKeys = {};

                // Bundle savings amount
                // This is equal to the difference of the cost all of the products in the bundle, minus the cost of the bundled product
                templateKeys.bundleSavings = (function GetBundleSavings() {
                    var cartItemsCost = parseFloat(productToAdd.productCost) || 0;
                    var bundleProductCost = 0;

                    bundle.productsInCart.forEach(function (productId) {
                        var product = self.model().availableProducts().find(function (product) {
                            return product.productId() == productId;
                        });

                        if (product) {
                            cartItemsCost += parseFloat(product.productCost()) || 0;
                        }
                    });

                    var bundledProduct = self.model().availableProducts().find(function (product) {
                        return bundle.productToUpgradeTo == product.productId();
                    });

                    if (bundledProduct) {
                        bundleProductCost = parseFloat(bundledProduct.productCost()) || 0;
                    }

                    return (cartItemsCost - bundleProductCost).toFixed(2);
                })();

                // Replace any template strings in the content
                var bundleMessage = bundle.message;

                for (var templateKey in templateKeys) {
                    var templateKeyStr = "{{" + templateKey + "}}";
                    var templateReplacement = templateKeys[templateKey];

                    bundleMessage = bundleMessage.replaceAll(templateKeyStr, templateReplacement);
                }

                new CommonModal({
                    title: 'Bundle Upgrade',
                    body: bundleMessage,
                    confirm: true,
                    btnCancelText: 'No thanks',
                    btnConfirmText: 'Upgrade'


                    // Confirm callback
                }, function () {
                    var item = self.model().availableProducts().filter(function (item) {
                        return item.productId() === bundle.productToUpgradeTo;
                    });

                    self.HandleAddRemoveProductFromCart(item[0]);


                    // Cancel callback
                }, function () {
                    self.HandleRuleSwapAndDisable(rules);
                    self.AddProductToCart(productToAdd);
                });

                return true;
            }
        }

        self.HandleRuleSwapAndDisable(rules);
        return false;
    };



    /**
     * Handles the swap and disable rules for a product
     *
     * @param {Object} rules The rule set to follow
     **/
    self.HandleRuleSwapAndDisable = function (rules) {
        var productsInCart = ko.mapping.toJS(self.model().productIdCart()); // Array of product ids


        // Check for items to swap out of the cart
        rules.swap.forEach(function (productId) {
            var itemId = productsInCart.filter(function (itemId) {
                return itemId === productId;
            });

            if (itemId.length) {
                var productToRemove = self.productsInCart().filter(function (product) {
                    return product.productId() === itemId[0];
                });

                self.HandleAddRemoveProductFromCart(productToRemove[0]);
            }
        });


        // Check for items to disable
        rules.disabled.forEach(function (product) {
            var item = self.model().availableProducts().filter(function (item) {
                return item.productId() === product.productToDisable;
            });

            if (item.length) {
                item[0].isDisabled(true);
            }
        });
    };



    /**
     * Enables products based on disabled rule set
     *
     * @param {object} productId The product id of the rule set
     */
    self.HandleEnableProductsAfterRemove = function (productId) {
        var disabledProducts = _PRODUCT_RULESET[productId].disabled;

        disabledProducts.forEach(function (itemId) {
            var product = self.model().availableProducts().filter(function (p) {
                return p.productId() === itemId.productToDisable;
            });

            if (product.length) {
                product[0].isDisabled(false);
            }
        });
    };





    /**
     * Checks if a product is in the cart
     *
     * @param {object} item The product to check
     * @returns {boolean}
     */
    self.IsProductInCart = function (item) {
        var product = ko.mapping.toJS(item),
            model = ko.mapping.toJS(self.model);

        return model.productIdCart.indexOf(product.productId) > -1;
    };


    // Array of products in the cart
    self.productsInCart = ko.computed(function () {
        if (!self.modelLoaded()) return [];

        return self.model().availableProducts().filter(function (product) {
            return self.model().productIdCart().indexOf(product.productId()) > -1;
        });
    });


    // Whether there is a product in the cart with a price
    self.HasProductsInCartWithPrice = ko.computed(function () {
        return self.productsInCart().some(function (product) {
            return product.startUpCostTotal();
        });
    });


    // The totals for the cart
    self.cartTotals = ko.computed(function () {
        if (!self.modelLoaded()) return {};

        var totals = {
                subtotal: 0,
                discount: 0,
                tax: 0,
                due: 0
            },
            products = self.productsInCart();

        products.forEach(function (product) {
            totals.subtotal += product.productCost();
            totals.discount += product.discount();
            totals.tax += product.taxes();

            if (product.discount() > 0) {
                totals.due += product.startUpCostTotal();
            } else {
                totals.due += product.startUpCostTotalWithoutDiscount();
            }
        });

        return totals;
    });



    // Scheduling functions
    // =========================

    self.FormatDateDisplayMonth = function (dateString) {
        var date = new Date(dateString);

        return GetMonthNameShort(date.getUTCMonth() + 1);
    };

    self.FormatDateDisplayDay = function (dateString) {
        var date = new Date(dateString);

        return date.getUTCDate();
    };

    self.IsTimeAvailable = function (time) {
        if (!self.modelLoaded() || !self.model().selectedDate())
            return false;

        var selectedDate = self.model().availableDates().find(function (availableDate) {
            return availableDate.dateString() == self.model().selectedDate();
        });

        return !selectedDate ? false : selectedDate.availableTimes().indexOf(time) >= 0;
    };

    self.FormatTimeSlot = function (startTime) {
        startTime = parseInt(startTime.substr(0, 2));
        var endTime = startTime + 2;
        var startSuffix = startTime > 12 ? 'PM' : 'AM';
        var endSuffix = endTime > 11 ? 'PM' : 'AM';

        if (startTime > 12) startTime -= 12;
        if (endTime > 12) endTime -= 12;

        return startTime + startSuffix + ' - ' + endTime + endSuffix;
    };

    self.HasScheduleItemSelected = function () {
        if (self.modelLoaded() && self.model()) {
            var selectedKey = ko.unwrap(self.model().timeSlotSelectedKey);
            if (selectedKey) {
                return true;
            }
        }

        return false;
    };


    // Step functions
    // =========================

    self.GetStepText = function (step) {
        return stepTextMap[step] || '';
    };

    self.IsStepComplete = function (step) {
        if (!self.modelLoaded())
            return false;

        // If unrecognized or current step, then don't show as complete
        var stepIndex = defaultStepOrder.indexOf(step);
        if (stepIndex == -1 || stepIndex == currentStepIndex())
            return false;

        // If step is a previous step, then allow it
        if (stepIndex < currentStepIndex())
            return true;

        // Step must be a future step
        // Check if step is in history
        return stepCache[step]() !== null;
    };

    self.IsStepDisabled = function (step) {
        if (!self.modelLoaded())
            return false;

        // If on confirmation step, then disable all other steps
        if (currentStepObservable() == STEP_CONFIRMATION)
            return true;

        // If this step is not already complete, then disable it
        if (!self.IsStepComplete(step))
            return true;

        return false;
    };

    self.CanGoToStep = function (step) {
        return !self.submitting() && !self.IsStepDisabled(step);
    };

    self.GoToStep = function (step) {
        if (!self.CanGoToStep(step))
            return;

        if (step == STEP_ADDRESS) {
            stepCache[step]().customer.serviceProperty.addressVerified = false;
        }

        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Click Step ' + GetStepName(step), 'From Step ' + GetStepName(self.currentStep()), '');

        // This should always be populated since self.IsStepComplete checks for this
        // But just in case, don't leave opportunity for errors
        if (step in stepCache && stepCache[step]() !== null) {
            UpdateModel(stepCache[step]());
        }
    };

    self.StepBack = function () {
        if (!self.modelLoaded() || self.submitting() || currentStepIndex() < 1)
            return;

        self.GoToStep(stepOrder()[currentStepIndex() - 1]);
    };

    self.StepCanBeSubmitted = ko.computed(function () {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                return self.modelLoaded();


            case STEP_PRODUCTS:
                //return self.HasPrimaryProduct();
                break;


            case STEP_SCHEDULE:
                return self.HasScheduleItemSelected();


            case STEP_PAYMENT:
                if (self.model().actionList && self.model().actionList() &&
                    self.model().actionList().length > 0 &&
                    self.model().actionList()[0] == 'DISABLE_SUBMIT_ORDER') {
                    return false;
                }
                break;
        }

        return true;
    });

    self.IsValidStep = function () {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                var result = true;
                result = self.validator.test('customer.serviceProperty.address1') && result;
                result = self.validator.test('customer.serviceProperty.zipCode') && result;

                if (self.addressValidator.ziptasticFail()) {
                    result = self.validator.test('customer.serviceProperty.city') && result;
                    result = self.validator.test('customer.serviceProperty.state') && result;
                }

                result = self.validator.test('customer.contact.email') && result;
                result = self.validator.test('customer.contact.phone') && result;

                if (!result) {
                    self.PostError("One or more form items are invalid. Please check the form for errors.");
                    return false;
                }

                return true;
                break;


            case STEP_PRODUCTS:
                break;


            case STEP_SCHEDULE:
                var result = true,
                    errorMsg = '';

                // Fail if we're on free inspection path and the first and last name aren't populated
                if (!self.HasProductsInCartWithPrice()) {
                    if (!self.validator.test('customer.firstName') || !self.validator.test('customer.lastName')) {
                        result = false && result;
                        errorMsg = "Please fill out the home owner's first and last name.";
                    }
                }


                // Fail if there isn't a selectedDate and selectedTime, or selectedDate isn't set to NONE
                if (!((self.model().selectedDate() && self.model().selectedTime()) || (self.model().selectedDate().toUpperCase() === 'NONE'))) {
                    result = false && result;
                    errorMsg = 'Please select a date and time to schedule your appointment.';
                }


                if (!result) {
                    self.PostError(errorMsg);
                    return false;
                }

                return true;
                break;


            case STEP_PAYMENT:
                // var result = true;

                // result = self.validator.test('customer.firstName') && result;
                // result = self.validator.test('customer.lastName') && result;

                // if (!self.billingAddressSameAsServiceAddress()) {
                //     result = self.validator.test('customer.billingProperty.address1') && result;
                //     result = self.validator.test('customer.billingProperty.city') && result;
                //     result = self.validator.test('customer.billingProperty.state') && result;
                //     result = self.validator.test('customer.billingProperty.zipCode') && result;
                // }

                // switch (self.model().payment.paymentMethod()) {
                //     case 'card':
                //         result = self.validator.test('payment.cardNumber') && result;
                //         result = self.validator.test('payment.cardExpire') && result;

                //         // Remove spaces from CC number
                //         if (result) {
                //             self.model().payment.cardNumber(self.model().payment.cardNumber().replace(/\s/g, ''));
                //         }
                //         break;

                //     case 'bank':
                //         result = self.validator.test('payment.bankAccountNumber') && result;
                //         result = self.validator.test('payment.bankRoutingNumber') && result;
                //         break;

                //     default:
                //         return false;
                // }

                // result = self.validator.test('customer.contact.phone') && result;
                // result = self.validator.test('customer.contact.email') && result;

                // if (!result) {
                //     self.PostError("One or more form items are invalid. Please check the form for errors.");
                //     return false;
                // }

                return true;
                break;
        }

        return true;
    };

    self.PostError = function (errorMessage, messageColor, errorCode) {
        if (!errorMessage) return;

        var msg = {
            "errorMessage": errorMessage || 'An error has occurred.',
            "messageColor": messageColor || null,
            "errorCode": errorCode || 'GENERAL_ERROR'
        };

        self.model().errorMessages.push(msg);

        ScrollTo($('.error-msg.bg-danger.text-danger').offset().top - 100, 500);
    };

    self.CheckStepAddressValidation = function (json, submitFunction) {
        switch (self.currentStep()) {
            case STEP_ADDRESS:
                var address = json.customer.serviceProperty;

                if (self.addressValidator.IsLastValidatedAddress(address)) {
                    submitFunction();
                    return;
                }

                self.addressValidator.isValidating(true);

                GetCityAndStateFromZip(address.zipCode, function (data) {
                    if (data.error) {
                        // If ziptastic hasn't already failed once
                        if (!self.addressValidator.ziptasticFail()) {
                            self.submitting(false);
                            self.addressValidator.ziptasticFail(true);

                            self.validator.add('customer.serviceProperty.city', 'required');
                            self.validator.add('customer.serviceProperty.state', 'required');

                            $('.city-state-block').show();

                            self.PostError("There was a problem resolving your ZIP. Please enter your city and state.");
                            FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Address Validation', 'Error: There was a problem resolving your ZIP. Please enter your city and state.', '');
                            return;
                        }

                        // Save user entered city and state
                        address.city = self.model().customer.serviceProperty.city();
                        address.state = self.model().customer.serviceProperty.state();
                    } else {
                        // Populate the address on the model
                        self.model().customer.serviceProperty.city(data.model.city);
                        self.model().customer.serviceProperty.state(data.model.state);

                        // Populate the address being sent to validate
                        address.city = data.model.city;
                        address.state = data.model.state;

                        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Address Validation', 'Submit Success', '');
                    }

                    self.addressValidator.ziptasticFail(false);

                    self.addressValidator.ValidateAddress(address, {
                        onKeepAddress: function () {
                            SubmitStep();
                        },
                        onUseSelectedAddress: function (selectedAddress) {
                            self.model().customer.serviceProperty.address1(selectedAddress.address1);
                            self.model().customer.serviceProperty.address2(selectedAddress.address2);
                            self.model().customer.serviceProperty.city(selectedAddress.city);
                            self.model().customer.serviceProperty.state(selectedAddress.state);
                            self.model().customer.serviceProperty.zipCode(selectedAddress.zipCode);

                            SubmitStep();
                        },
                        onValidate: function (hasCorrections) {
                            if (hasCorrections) {
                                self.submitting(false);
                            } else {
                                SubmitStep();
                            }
                        }
                    });
                });
                break;
            case STEP_PRODUCTS:
                submitFunction();
                break;
            case STEP_SCHEDULE:
                submitFunction();
                break;
            case STEP_PAYMENT:
                submitFunction();
                break;
        }
    };

    
   function SubmitWallet() {
            window.frames.wallet.postMessage({
                action: 'submit'
            }, 'https://test.checkout.servicemaster.com');
    }
    
    function SubmitStep() {
        self.RemoveAllMessages();

        if (!self.IsValidStep())
            return;

        self.submittingStep(self.currentStep());
        self.submitting(true);

        var json = GetModelJSON();

        self.CheckStepAddressValidation(json, function () {
            var dwr = '';
            json.liveChat = JSON.parse(sessionStorage.getItem('LiveChatState')) || {
                visitorId: null,
                hasChatOccurred: false
            };

            switch (self.currentStep()) {
                case STEP_ADDRESS:
                    dwr = 'TMXPurchaseFunnelUIUtils/getProductsAndPricing';
                    break;
                case STEP_PRODUCTS:
                    dwr = 'TMXPurchaseFunnelUIUtils/getScheduleAvailability';
                    break;
                case STEP_SCHEDULE:
                    dwr = 'TMXPurchaseFunnelUIUtils/submitSchedule';
                    break;
                case STEP_PAYMENT:
                    debugger;
                    json.payment.paymentMethodType = self.walletJsonData().data.paymentMethodType;
                    json.payment.paymentMethodId = self.walletJsonData().data.paymentMethodId;
                    json.payment.firstName = self.walletJsonData().data.firstName;
                    json.payment.lastName = self.walletJsonData().data.lastName;
                    json.payment.city =  self.walletJsonData().data.city;
                    json.payment.errorCode =  self.walletJsonData().data.errorCode;
                    json.payment.state =  self.walletJsonData().data.state;
                    json.payment.streetAddress =  self.walletJsonData().streetAddress;
                    json.payment.zipCode = self.walletJsonData().zipCode;
                    dwr = 'TMXPurchaseFunnelUIUtils/submitOrder';
                    break;
            }

            if (!dwr) throw "Unrecognized funnel step.";

            // Check if we should show loading on the next step
            if (currentStepObservable() == STEP_PRODUCTS) {
                // Go to next step and show loading there
                currentStepObservable(STEP_SCHEDULE);

                // Scroll to top of the page to show this step
                ScrollTo(0, 'fast');
            }

            CallDWR(dwr, json, function (data) {
                var errorMessage = data.error || data.model && data.model.errorMessages && data.model.errorMessages.length && data.model.errorMessages[0].errorMessage;

                if (errorMessage) {
                    switch (self.currentStep()) {
                        // Step 2 errors: Failed to retrieve pricing
                        case STEP_PRODUCTS:
                            // TODO: add non 8k logic
                            if (data.model && data.model.actionList && data.model.actionList.length && data.model.actionList[0] === "INVALID_ADDRESS") {
                                self.PostError(errorMessage);
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Address Validation', 'Error: ' + errorMessage, '');
                            } else {
                                currentStepObservable(json.currentStep);
                                self.PostError(errorMessage);
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Retrieve Pricing', 'Error: Failed to retrieve pricing.', '');
                            }
                            break;


                            // Step 3 errors: This occurs whenever scheduler times out or returns no dates
                        case STEP_SCHEDULE:

                            // If we didn't get a timeout, then an error was thrown
                            if (!data.textStatus || data.textStatus.toLowerCase() !== 'timeout') {
                                self.PostError(errorMessage);
                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Schedule Step', 'Error: ' + errorMessage, '');

                                // Timeout occurred when trying to retrieve scheduling information
                            } else {
                                currentStepObservable(json.currentStep);

                                // Set step 3 default date to NONE
                                json.currentStep = STEP_SCHEDULE;
                                self.noDatesChosen(true);

                                // Forget any cached model that is after this step
                                ClearStepCache({
                                    after: json.currentStep
                                });

                                stepCache[json.currentStep](json);

                                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Retrieve Schedule', 'Error: Failed to retrieve schedule.', '');
                                UpdateModel(json);
                            }
                            break;


                        case STEP_PAYMENT:
                            self.PostError(errorMessage);
                            FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Order', 'Error: ' + errorMessage, '');
                            break;


                        case STEP_ADDRESS:
                        case STEP_CONFIRMATION:

                        default:
                            //console.log(errorMessage);
                            self.PostError(errorMessage);
                            break;
                    }


                    if (data.model && data.model.actionList && data.model.actionList.length) {
                        UpdateModel(data.model);
                    }

                } else {
                    switch (self.currentStep()) {
                        case STEP_ADDRESS:
                            break;

                        case STEP_PRODUCTS:
                            //FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Retrieve Pricing', 'Success: ' + data.model.primaryProduct.productCode + ' ' + data.model.primaryProduct.templateCode, '');
                            break;

                        case STEP_SCHEDULE:
                            // This is for firing that we retrieved schedule
                            FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Retrieve Schedule', 'Success', '');
                            break;

                        case STEP_PAYMENT:
                            break;


                        default:
                            console.log('-- default success?');
                            break;
                    }


                    // Update whether the products and payment steps should be shown
                    switch (json.currentStep) {
                        case STEP_ADDRESS:
                            // If jumped from address to schedule, then no pricing step
                            // Also, there would be no payment step
                            if (data.model.currentStep == STEP_SCHEDULE) {
                                isShowStep[STEP_PRODUCTS](false);
                                isShowStep[STEP_PAYMENT](false);
                            } else {
                                isShowStep[STEP_PRODUCTS](true);

                                if (self.IsFreeInspectionPath()) {
                                    isShowStep[STEP_PAYMENT](false);
                                } else {
                                    isShowStep[STEP_PAYMENT](true);
                                }
                            }
                            break;

                        case STEP_SCHEDULE:
                            // If jumped from schedule to confirmation, then no payment step
                            if (data.model.currentStep == STEP_CONFIRMATION) {
                                isShowStep[STEP_PAYMENT](false);
                            } else {
                                self.setIdentityAndOpaqueId();
                                isShowStep[STEP_PAYMENT](true);
                            }
                            break;
                    }


                    // Go back to old step so we can detect step changes and such
                    currentStepObservable(json.currentStep);

                    // Check if user changed steps
                    if (json.currentStep != data.model.currentStep) {
                        // Forget any cached model that is after this step
                        ClearStepCache({
                            after: data.model.currentStep
                        });

                        // Check for confirmation step for conversions
                        if (data.model.currentStep == STEP_CONFIRMATION) {

                            // Get information for GA Conversion
                            var gaConversion = data.model && data.model.sendInfoToGoogleAnalytics ? data.model.sendInfoToGoogleAnalytics : '';

                            // Send GA Conversion
                            if (gaConversion) {
                                try {
                                    utag.view(data.model.sendInfoToGoogleAnalytics);
                                } catch (e) {
                                    console.log(e);
                                }
                            }

                            // Fire Submit Order Event(s)
                            var productCodeString = '',
                                productsInCart = ko.mapping.toJS(self.productsInCart());
                            var freeInspOnly = false;
                            var gaValueTotal = 0;

                            for (var i = 0; i < productsInCart.length; i++) {
                                freeInspOnly = productsInCart.length == 1 && productsInCart[i].productId == '10001';
                                var gaValue = parseFloat(productsInCart[i].productCost);
                                gaValue = gaValue ? gaValue : 0;
                                gaValueTotal += gaValue;
                                if (data.model.showUpsell) {
                                    var upsellLabel = freeInspOnly ? 'FREEINSPONLY' : productsInCart[i].productCode;
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'UPSELL', upsellLabel, gaValue);
                                }
                                if (data.model.tiered) {
                                    FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'TIERED', productsInCart[i].productCode, gaValue);
                                }
                                if (!productCodeString) {
                                    productCodeString = productsInCart[i].productCode + ': ' + productsInCart[i].templateCode;
                                } else {
                                    productCodeString += ' / ' + productsInCart[i].productCode + ': ' + productsInCart[i].templateCode;
                                }
                            }

                            FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Submit Order', 'Success: ' + productCodeString, gaValueTotal);

                            // Check for Affiliate conversion
                            if (self.IsAffiliatePath()) {
                                var affiliate = ko.unwrap(self.model().commercialAffiliate);

                                FireAffiliateGAEvent({
                                    id: affiliate.id(),
                                    name: affiliate.name()
                                }, 'Conversions');
                            }


                            // Check for Free Inspection conversion
                            var freeInspectionConversion = data.model && data.model.productIdCart.indexOf('10001') > -1;

                            if (self.IsFreeInspectionPath() && freeInspectionConversion) {
                                FireFreeInspectionConversion();
                            }
                        }
                    }

                    stepCache[json.currentStep](json);

                    UpdateModel(data.model);
                }

                self.submitting(false);
            });
        });
    }

    self.SubmitStep = function () {
        if (!self.modelLoaded() || self.submitting())
            return;

        $('#funnel-wrapper :focus').blur();

        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Links and Buttons', 'Button: ' + GetStepName(self.currentStep()) + ' Step Submit', '');
        SubmitStep();
    };
    
    self.SubmitWallet = function () {
        if (!self.modelLoaded() || self.submitting())
            return;

        $('#funnel-wrapper :focus').blur();

        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Links and Buttons', 'Button: ' + GetStepName(self.currentStep()) + ' Step Submit', '');
        SubmitWallet();
    };



    // Initialization
    // =========================



    self.LoadModel = function () {
        CallDWR('TMXPurchaseFunnelUIUtils/getPurchaseModel' + window.location.search, null, function (data) {
            var optionalAbandonMsg = '';
            if (window.location.search.indexOf("abandon=true") > -1) {
                optionalAbandonMsg = ' On Abandon';
            }
            if (data.error) {
                self.error(data.error);
                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Funnel Loaded' + optionalAbandonMsg, 'Error: ' + data.error, '');
            } else {
                // Replace the history state on load with this default state
                if (g_isHistorySupported) {
                    //                    history.pushState(stepHistory, "", GetStepURL(data.model.currentStep || defaultStepOrder[0]));
                }

                if (window.location.search.indexOf("abandon=true") > -1) {
                    var tmpmodel = Object.create(data.model);
                    tmpmodel.currentStep = STEP_ADDRESS;
                    stepCache[defaultStepOrder[0]]($.extend(true, {}, tmpmodel));
                }
                stepCache[data.model.currentStep || defaultStepOrder[0]]($.extend(true, {}, data.model));

                FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Funnel Loaded' + optionalAbandonMsg, 'Success', '');

                // Fire affiliate event
                if (!IsObjectEmpty(data.model.commercialAffiliate)) {
                    FireAffiliateGAEvent(data.model.commercialAffiliate, 'Passed Through');
                }

                UpdateModel(data.model, true);

            }
        });
    };

    self.UpdateModel = function (model) {
        UpdateModel(model);
    };

    function setCustomerId(customerId) {
        //        debugger;
        self.customerId(customerId);
    };

    function setOpaqueId(opaqueId) {
        //        debugger;
//        self.opaqueId(opaqueId);
        self.isShowIFrame(true);
    };


    self.setIdentityAndOpaqueId = function () {
        debugger;
    //    self.customerId("b7515870-a44c-4494-929f-c81e98254668");
    //    self.opaqueId("95d0bfe3-72ca-4417-8735-3f4570ddda89");
        self.getIdentityAndOpaqueId(function (data) {
            if (data != "Error") {
                self.customerId(data.identityId);
                self.opaqueId(data.opaqueId);
            } else {
                console.log("Error In getting identity Id");
            }
        });
    };

    self.getIdentityAndOpaqueId = function (callbackfn) {
        $.ajax({
            type: 'POST',
            url: "http://storefront.local/api/cxa/TmxFulfillment/GetPaymentDetails",
            success: function (data) {
                try {
                    callbackfn(JSON.parse(data));
                } catch (e) {
                    console.log("Error from server request: " + e.message);
                    callbackfn("Error")
                }
            },
            error: function (error) {
                console.log("Error from server request: " + error.message);
                callbackfn("Error");
            }
        });
    };


    //    function getCustomerOpaqueId() {
    ////        debugger;
    //        $.ajax({
    //            type: 'POST',
    //            url: "https://test.servsmartapi.com/V4/Authorizations/AssociationId",
    //            data:  JSON.stringify({"Data":{
    //                "UserName": "murali",
    //                "IPAddress": "120.0.0.1",
    //                "ApplicationName": "tmxecom",
    //                "RefreshToken":"lZypRjcHBUf0gau_3_q915oalXTJnAXcT9q0dTboZbMAAQAAaKYQ8z7ksRl3VSjtv8KQIGdUVKuEoCQJ-Phh1QUzrF1OqgHu9G_Uf_CY_6NtQgpuDnUrLymsJBOP4n_2M_pvsIYSY7As6vUFZrXDkApyHC_lQIzqUlGly81SyDKTWoiKe3JIii0ewnZrO-f5IWqylg5Ba4zP9wmw6qbh1CTLLNBMXWhY4P1ur2b8_jd948ef4_zhkAO3WsQuvozV4QIjYU0EeLcb9PF8_Yfp85Tuo8NPNhWGk6hcQAxDJKuKESjDRVovMZZP91Fs-dGA_OG7kH0UD56lWtrSS9HXgvK0ngbo6cpK9108A_0C3B3iUo873f9kb52oFqlj2EYFQx1ZDGANAABMUgFUsyoeqZ57RwT9lZqczIDgsHarhZ04sNa-5Xt3AqXzVPSP5c-oxLtD943jRlKN6nO2CwkAqmhvBIRhCBKc6p0KANArZ25iGPqih44ROGbhvNFdBgpByWncU8HlQBYJil5zUMpzJPgT6T3X4rnsP9Ol_2AEw1UiPlQVhTVRZEVyGqOMre9oUhMT0354s5uUiAH5ouqXt26V5BVFE_y4ZvXsIriyIgd25mUaQ5mD2exX4cgeyAnTbdMVD4je8g4cVtd3udFTyI5ssFPRSKePygvueGjkaIIvcoZ1KMCthGbRCZ8EehnuV5pOUhrEPwdsUmzmyLJ8ZLcR6DZwMEc4r0uW4DLoGOH-p3gfT-GUcC4wiLQXHXZ3WdkE-j_b9f2rBdoPLsm8Y6WqzQ4r0ysUOvl_4mDFz1_Zm_vMbhTXosPtLvDBpYZv5ifAm76EzJGoWmpj5SLhmqglTtO7yeAZ8z7jgV_WiDuZrL0dCH3aOpY8RhZ-1SuCz00wX0xidWm3VV1Yxq1N81USkwJeLKumLCJBxY0gitTuREWPjXJ8rzSZYbRtWkwoQqpSHXoBF88_IVMxs3NumLra_iUkoqJ6C75gBTUQH3J_IVICDPBSnc4-qTwOI6XYqIuStA4oxpzvqQAYu_FxtxBiCwHp9INS2nskMVjmIkwIncLDl03tN74d5p5nWH45fsRzaRC2tRGnL8jBsAFKolJLePx82ALJ18ekVMN_DR0NOoDVL6MYnTgze1tPQA2Z6dXhjpvB8KpKOqmiJZF1kFDLnUrC2aoCksJcWd8ub4_2pwM9fdjzclmIJV4z5IbXCo07affQGHPEluFetdlgIWujKaSrwxTQt8de1JMCCYGp5EbZlEzq4EgjaF_vUbATng5HVyB_y_vNj4GBZ6OHvgBJGdC53Vf9y584H4qpm24loXs8QmHqOex5DuBckCLFBanWa2fmwY4mdA8hAiLGn5Fxk1t9eOmBaoJJ_lOq_y5BxZAiMzjgvxPFKtr4BvoLe31shHESGkvur3Cvx9YjxFliB2RHP-Mx1agoJNhIFOpSMlVWl_D81DQn84TsilAsuHNNnuJbkak0gyJRAPnFuj2mCeVGWfkhOcj6IQBKXtozDfCQI4IkfmZc3c4Kd4shPZf6Lnwlkgi31XAHPrBvPkWlcj7WiuOZyfuy44Pvw4KtcbQ7J1ee7YkIU4jhv_C1Hojox0AlSKPsx3FyIWc_3WYgtnqX8C2CsSpYjgM6X4OQOMKuDN0zabMRsL2AW4DX473Q06tc9iU2-uHAq-unUc2xjNxmlJCZ_6AxU656dyiLM4CUwu3IsM7ImzCyTzDPq9EaL8xd5IV6cB4cNxKh175SlFy5Q7PWBVvph2BezvMmhmgHKAq0_7N16SUrrsDmeD89FA70mtCOIPqOaT4fqfVfXgBnmtj2hFjOuRsNhq5-mgGSvzykoNFQ4Ktq4z-d6KDpZVMw031V-rPM6AkBftzNZblgrIsHzjsVIAxofy222g493RM9H2nk5RhKBoJMg4qDjQIWnYRjJ7m4W5ACR6LXHDQnkEuCfXN_ZN3Rv025wc6TFLGfi_eMKvREAObRjLxmfV-KYsqXCeqKoF0BYwhc2ss-D1c5q9ssXvMoNlX7WvWfxQgYs9ORI0NhhnCMbR-mcKTseMcL5qlxlM2H2-xuucO5lAfNEs-nJp8x-3zoymCppF7UMbHTnhDJGAMxcyoBh18NwYfqTfMd57f3II8p3tnDi6LgSPX_8Ivs9H2bhemWFDNYsqqykwmDAxniYMIiM2_YFANKahuXybDaEMms_YmtDOxRXAYy_mfxsvgJ7EVHhc4ogRCBi5kFcRRNC4pUKr1MWrjIHZIMhAkFVCa6i_hNEgoxK0D8LgekkJXAF5i6OnOhSfM0kHxLG7qHikibubxMlPi610fhd4mYm6g7xY5LAVNA7l6FMu-Xro5KxWMqhgx_gdGWfDvfP6rr8lJF4gUQ6SLKCqKEevQSve2AF3_a5NBw-IMHxpFqWyVffptB8yQID5XGl8Si6-S6rCuwvkNwRWeaZlPeNzVHMy5m7ilJOUaZitVv8siXlSxB9v3IKIhk21m656R4efTwFenPSxAWO-oXIUylXIw82yuXmhNXFAX79sqp9WX-GJmsOuw8k_hRT487YitIdXlRfQ8pOdvYu8p5hn6pZhb_HCqCcHMA7BasYtfqNLPbJvMkwwKqppyI2toe7BG3RWhnfr8NuS06LqToC3JnszaOB6_H_ObHYjHGEwSB-3ltEESg3tyrnPh1LfKUdE2S0rP3pecpehRSqmQr3bbtzfnnMBd1lYh96Ov0WXUDrBHoMeO9sH8Ong4So2HX9_I7RjaYchnUz3jamx6NptdiRJVYWV2v5UaUOrK4ZPnVscw71sIirgAdwj-hxQDXeq2qK5CINaH-OeKPjV_ECMJAjax-kyYldoANsg1kPZ-b6q8FSsZGNj9jvbLwnaP5Oq82rFMCCJ496IIH5tcOZ7IxDjoVOyhLxb9QfE3TS-nX5-rOPrSGZBVfUKdX3uERQy_9QuMiGkNVLodb1n_OccPmApMtnElid5i5dQNWI75dB26Wju1Pa63XKea2kRil9iagWW5b7Pr_vRLR2dSHW2CiDpT206imIc-1oPgntjH-7-wHI5LeQCRixl9bzRkQ6A6xmVdu22zeWZ6A_T5CIjIfdPcPAidFYfVyUT50HNAejcpMBmQwOZjjGx0XpnLDQt5HRijSaf2pNSNyJc1pkLJxoH1V-WOvMU-iejNSpqJbwzjtwgwGnMMBKZ-WdFj71L-bk2FWzRxCAcdNghlVmeFFFLbfIwB4tevtebX5IehI3DduTunsAh6AYTK4FxeKTQckT8cqPQQf1k4LaXihNfMEN_cu9Bl9gSusmsEeoKYBQAho07AGClwX2MaDC-0rWklO_Kdr4VFJcBs7tYHmEu_2Dftu-FSv83ECfy6G6UFXja9AxdRh3vPo1v2nqs6RYqFPhQ-eD5uGo4ImNppZoBDJ6VmQM5Z7NViDYM6ZkuHMUz3t5AtmDETVpc18yrrRHSda30RLedF4LdWnqZWcLSLkvzj--o5Xx1jm_C4H3qeW1VTE3diBaTEFy920lIUhIJAbyRYbs9kiilywXLc75_O7cbdhLeWaS2m8IUGwzCyDcfWxVh49ni9d6qoGF9ZAXScdb9xx52QYKrWO3AxVS3hmGDnVY6xgKy3PIz4ujHDeA8ctZ73WJJ9HNCmslr0SNg0SbTjjmPh0Y5DKfS9PA9mCqGkuj6gy7wE-EbDPdSik3MM1GRyBnSOC4Br30BiaB_LICAB5TUBgWOPTzenvPXP4XDjPubUNWpndOKA4enFUPndJ2zfBHrObAKu77Qlb98c95xKkr9ehn9p7exlYG_FEP6T05gbQ-ZE6iSQeYCxE81VnqMe3gWOKphQijk0WrjMwlhNBqT85cAprIUUY2chq_gngnmw_gegzOsGAT4Wk77hny7kiraEmjlVsyypcDXPlIx6gXYw37Nx3fnXTOlN8b1XCcVpZlrvAJ1V1CsfJIQ4OAGNAiCDYb4dpLc_hNNgqQPQHXRNEjdAeNOnXyPAE6UYfi-g_5yqcnD971huozrWlxuer_hjq2t4mS7QAe7JUlWuZzN6bLGOUG-xI8M_nChN3NZ3vwK6mBSmkNHU5g739CR8IGOtRiA72QRZ25OHntm2jrJLCysQlz9-OsrfjbOWAVvJu8oikf2B-wZ86cBvJqmEEZPVQ5lWDAgC-UVbvHisdglTMedAh4MYhbOigaIDC9MCmP-YkR8ewE-BF83rWZgmx5Pd5A4MRilTYib8VIX0ea6elDgUzwmx7gQODxG1GJIleNeIjqEQRil0xRiRg-EUIMP0DVkQKbHuUBOO017nV9n_XLkK1ZV9WMyrxD-7zsArqc6ZQ_DdTXbHTQ6STI73CK5ky5izW9MoZN9m-EDongWWb1_e8i6iHhgSU6cTuukBWwI6DugSEfRLomihrl8cmqSgvpYXMNW4p723ps2Vn3E4-80q65wziOGJhbP5ba2kR6-bf9Gk-Gs3YTOIZoTOxCCCVO4irRofe9GP2dHlWfv3Fn_YVh1yzFCxNaonjSOy0fHNYNR62BKcXvuUayHA0kHST53VxPY8kqhXARUB5eayLjIoilhHq3xoAeO0v6SUxLVIGgCdO9jgrRFjDVqtpRgSRr5SvY9aF3SfYnLwBA93qmFzy_8GI28qaxXL2cCfqj4GbIXA4LcdIkPrhEZqS9m3gh8r4bGMFMMHpEmC8Uqu9ZF-VWtPo2DBWCIJvdd8ASRqcbNKrorjvdQjAAcSGoE540Hw4Ue0IlZyL1CxUsBvsNxFcJzV4pnYKSv7rPPqqJ5kJ69O7s7OUrqAf8kXHOProg5c3Xc_9R9cbrrO3uvqa8lCGSYB--3-61uFykuCmfgloE07nDsXyO2hn-eUzysOxlzkyaSlzgYu-UcgWsocJ6sCfehz68PdKs_u3dk8n_F_SrYz9NYcSQaIxsE-LWJ6zBUpvBQzPIoRNWIsCb4X3slpvN_URyJIoTN9VXuESnKhX2-qN.GJdGhZivud_WkOAFdGTQMxAvGvuxHSugIHELx3PiPjevOeD3q271vPzEuQEsLvpPpqbkZmQ_KX2XC4eqR5YSzcE70N3WhiVXSlFncqO45BwGCVT0eaUoBbjApxZTziFvQUmWZgUJqEiFBgEBtk04OA_k1fG-K7NSo3nAsEu4RPR2I-OtwHfw210Z_3qJM95Q0HuIRvLLa0aWVtzlZ9uITYOdqjo18z2BUGuMmNpJaKttSmhjmIJ-1i6PHxkL82tL8JoUI_bVLyJMUaVNkfab6R3_Vhq0VraZRgZC3AXFHpQf3edq0MU4strlfdp2oBe2IblUAwVKt2QnzlgqKtC1yA",
    //                "CreationDate": "2019-02-06T10:50:15.188Z"
    //              }
    //            }),
    //            contentType: "application/json",
    //            // timeout: 2 * 60 * 10000,
    //            app_key: "fyJ05YgNHezggJZcpqrz5rjeGLHVfgPu",
    //            authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6InFDUnZ6VUFFZjlCQ3AwaENLdE55TFlPWDVRYyJ9.eyJhdWQiOiJodHRwOi8vbWVtMGJzY3dlYjAxZDo2MC8iLCJpc3MiOiJodHRwOi8vYWRmcy5zZXJ2aWNlbWFzdGVyLmNvbS9hZGZzL3NlcnZpY2VzL3RydXN0IiwiaWF0IjoxNDYyMjk0OTI0LCJleHAiOjE0NjIzNDg5MjQsIndpbmFjY291bnRuYW1lIjoicmlzYXd5ZXIiLCJncm91cCI6WyJEb21haW4gVXNlcnMiLCJJVCBEZXZlbG9wZXJzIiwiQklaIFZQTiBDZXJ0IiwiSVQgQXNzb2NpYXRlcyBMaXN0aW5nIiwiSVRBSFNUZWFtIiwiU2hhcmVmaWxlIFVzZXJzIiwiWGVuTW9iaWxlLVNlcnZpY2VNYXN0ZXItQlNDLUlUIl0sImF1dGhfdGltZSI6IjIwMTYtMDUtMDNUMTc6MDI6MDQuNzk3WiIsImF1dGhtZXRob2QiOiJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZFByb3RlY3RlZFRyYW5zcG9ydCIsInZlciI6IjEuMCIsImFwcGlkIjoiYWdlbnQtZGVza3RvcCJ9.K4EiULi7MRvPW4gHc9QVNUXFNWc7aL9lr0wMrZUZKem52qdxyQarC2B2YOWv3IeoGGvtuzKaDQbAbzKywlflYBAamV2qWvUeewCaKatEH8lEt5YzZe5elUahzAfKcLCGRouSvxWONQKcqrtuKcA5_TbH-_dfsNGK37OdfdWHMDCSqusO2JZeX2IlHKDFPVoOY0yAhv0bsveeTNpuGYuPWzfa1A4hiWEM7VVQ2E8-qXcaKQMFtF81p3dEH2jvp3xgUYIElkDf2bPgy3MYadwhfhs3c5IFNRmErxqVamEYG54Dab7J8c2LYgoQguNekDZxzYFm9K5AvtDcXt9O9vJZ3Q",
    //            success: function (data, self) {
    //                try {
    //                    setOpaqueId(data.Data.Id)
    //                } catch (e) {
    //               debugger;
    //                    console.log("Error from server request: " + e.message);
    //                }
    //            },
    //            error: function(error){
    //                console.log("Error from server request: " + error.message);
    //            }
    //        });
    //    };

    //    self.LoadIFrameData = function(){
    //        debugger;
    //        $.ajax({
    //            type: 'GET',
    //            url: "https://test.servsmartapi.com/V5/Identities/?fLegacySystemIdentifier=10011077&fType=Customer",
    //            // data: model ? {
    //            //     jsonData: JSON.stringify(model)
    //            // } : {},
    //            // dataType: 'json',
    //            // timeout: 2 * 60 * 10000,
    //            app_key: "fyJ05YgNHezggJZcpqrz5rjeGLHVfgPu",
    //            authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6InFDUnZ6VUFFZjlCQ3AwaENLdE55TFlPWDVRYyJ9.eyJhdWQiOiJodHRwOi8vbWVtMGJzY3dlYjAxZDo2MC8iLCJpc3MiOiJodHRwOi8vYWRmcy5zZXJ2aWNlbWFzdGVyLmNvbS9hZGZzL3NlcnZpY2VzL3RydXN0IiwiaWF0IjoxNDYyMjk0OTI0LCJleHAiOjE0NjIzNDg5MjQsIndpbmFjY291bnRuYW1lIjoicmlzYXd5ZXIiLCJncm91cCI6WyJEb21haW4gVXNlcnMiLCJJVCBEZXZlbG9wZXJzIiwiQklaIFZQTiBDZXJ0IiwiSVQgQXNzb2NpYXRlcyBMaXN0aW5nIiwiSVRBSFNUZWFtIiwiU2hhcmVmaWxlIFVzZXJzIiwiWGVuTW9iaWxlLVNlcnZpY2VNYXN0ZXItQlNDLUlUIl0sImF1dGhfdGltZSI6IjIwMTYtMDUtMDNUMTc6MDI6MDQuNzk3WiIsImF1dGhtZXRob2QiOiJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZFByb3RlY3RlZFRyYW5zcG9ydCIsInZlciI6IjEuMCIsImFwcGlkIjoiYWdlbnQtZGVza3RvcCJ9.K4EiULi7MRvPW4gHc9QVNUXFNWc7aL9lr0wMrZUZKem52qdxyQarC2B2YOWv3IeoGGvtuzKaDQbAbzKywlflYBAamV2qWvUeewCaKatEH8lEt5YzZe5elUahzAfKcLCGRouSvxWONQKcqrtuKcA5_TbH-_dfsNGK37OdfdWHMDCSqusO2JZeX2IlHKDFPVoOY0yAhv0bsveeTNpuGYuPWzfa1A4hiWEM7VVQ2E8-qXcaKQMFtF81p3dEH2jvp3xgUYIElkDf2bPgy3MYadwhfhs3c5IFNRmErxqVamEYG54Dab7J8c2LYgoQguNekDZxzYFm9K5AvtDcXt9O9vJZ3Q",
    //            success: function (data, self) {
    //                try {
    //                    debugger;
    //                   setCustomerId(data.Data[0].IdentityID);
    //                   // console.log("Successfully loaded CustomerId = "+self.customerId());
    //                    getCustomerOpaqueId();
    //                } catch (e) {
    //               
    //                    console.log("Error from server request: " + e.message);
    //                }
    //            },
    //            error: function(error){
    //                console.log("Error from server request: " + error.message);
    //            }
    //        });
    //    };


    //    self.LoadEnableFlags = function(toggleparam){
    //        debugger;
    //        $.ajax({
    //            type: 'GET',
    //            url: "http://10.32.21.11/api/cxa/TerminixCatalog/GetKOData/{toggleparam}",
    //            success: function (data, self) {
    //                try {
    //                    return(data);
    //                    
    //                } catch (e) {
    //               
    //                    console.log("Error from server request: " + e.message);
    //                }
    //            },
    //            error: function(error){
    //                console.log("Error from server request: " + error.message);
    //            }
    //        });
    //    };


    /////////////////
    // Testing Methods
    //////////////////
    self.PurchaseStep = function () {
        self.submittingStep(self.currentStep());
        self.submitting(true);

        var json = GetModelJSON();

        CallDWR('TMXPurchaseFunnelUIUtils/getProductsAndPricing', json, function (data) {
            if (data.error) {
                self.PostError("An error has occurred. Please try again.");
            } else {
                UpdateModel(data.model);
            }

            self.submitting(false);
        });
    };
}

$(function () {
    var vm = new FunnelViewModel();

    // Update the model when window history changes (back)
    window.onpopstate = function (event) {
        // If event.state is not null, that means we have set history item before
        if (event.state) {
            vm.OnHistoryPop();
        } else {
            console.log('no event state');
            // Since no event state exists, take user back to where they came from
            if (g_isHistorySupported) {
                //history.back();
            }
        }
    };

    // Expose globally for debugging
    window.vm = vm;

    ko.applyBindings(vm, $('#funnel-wrapper').get(0));

    var loadedModel = false;


    // Check if there's an already existing session
    if (caniuse.sessionStorage) {
        var funnelModelJSON = sessionStorage.getItem("funnelModel");
        if (funnelModelJSON) {
            var funnelModel;

            try {
                funnelModel = JSON.parse(funnelModelJSON);
            } catch (e) {

            }

            // Load the model if it was found
            if (funnelModel) {
                vm.UpdateModel(funnelModel);
                loadedModel = true;
            }
        }
    }

    if (!loadedModel) {
        // Load the first step
        //debugger;
        vm.LoadModel();
    }
    //    vm.LoadIFrameData();
    //vm.setIdentityAndOpaqueId();
    //    vm.LoadEnableFlags('_BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE');
    //    vm.LoadEnableFlags('_ENABLE_PROPERTY_INFORMATION_TOGGLE');
    //    vm.LoadEnableFlags('_ENABLE_SMARTY_ZIP_TOGGLE');
    $('.logo-container a').on('click', function () {
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Links and Buttons', 'Link: Home Logo', '');
    });

    $('.phone-link a').on('click', function () {
        FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Links and Buttons', 'Link: Phone Number', '');
    });

    // Show product details on product step
    $(document).on('click', '.see-more-details', function (e) {
        e.preventDefault();

        var $this = $(this);
        var showMoreDetailsHtml = '<i class="fa fa-chevron-circle-down"></i> Details',
            hideDetailsHtml = '<i class="fa fa-chevron-circle-up"></i> Hide details',
            $productContainer = $this.parents('.product-container'),
            $productDetails = $this.parents('.product-container').find('.product-details');


        // If it's a tiered item, we need to select all the tiered product containers
        if ($productContainer.find('.card').hasClass('tiered')) {
            $productContainer = $productContainer.parents('#tiered-products').find('.product-container');
            $productDetails = $productContainer.find('.product-details');
            $this = $productContainer.find('.see-more-details');
        }


        // Change the text for the toggle
        if ($productContainer.hasClass('open')) {
            $this.html(showMoreDetailsHtml);
            $productContainer.removeClass('open').addClass('closed');

            // Scroll back to the top of the card
            ScrollTo($productContainer.offset().top - 120, 'fast');
        } else {
            $this.html(hideDetailsHtml);
            $productContainer.removeClass('closed').addClass('open');
        }

        // Toggle the product details section
        $productDetails.slideToggle('fast');
    });

    $(document).on('click', '.expand-details', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $i = $this.find('i.fa'),
            $subDetails = $this.parents('tr.line-item').next('tr.sub-details');

        if ($i.hasClass('fa-plus-circle')) {
            $i.removeClass('fa-plus-circle');
            $i.addClass('fa-minus-circle');
            $subDetails.show();
        } else {
            $i.removeClass('fa-minus-circle');
            $i.addClass('fa-plus-circle');
            $subDetails.hide();
        }
    });

    // Popup content when needed
    $(document).on('click', '.trigger-popup-content', function (e) {


        e.preventDefault();
        var $this = $(this);
        var title = $this.parent('li').find('.popup-title').html() || '',
            content = $this.find('.popup-content').html() || '';

        new CommonModal({
            title: title,
            body: content,
            confirm: false,
            btnCancelText: 'Close',
            btnConfirmText: ''
        });

    });
});
