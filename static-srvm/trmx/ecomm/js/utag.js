//tealium universal tag - utag.loader ut4.0.201809211413, Copyright 2018 Tealium.com Inc. All Rights Reserved.
if (typeof utag_err == 'undefined') var utag_err = [];
window._tealium_old_error = window._tealium_old_error || window.onerror || function () {};
window.onerror = function (m, u, l) {
    if (typeof u !== 'undefined' && u.indexOf('/utag.') > 0 && utag_err.length < 5) utag_err.push({
        e: m,
        s: u,
        l: l,
        t: 'js'
    });
    window._tealium_old_error(m, u, l)
};
var utag_condload = false;
try {
    (function () {
        function ul(src, a, b) {
            a = document;
            b = a.createElement('script');
            b.language = 'javascript';
            b.type = 'text/javascript';
            b.src = src;
            a.getElementsByTagName('head')[0].appendChild(b)
        };
        if (("" + document.cookie).match("utag_env_servicemaster_trmx=(\/\/tags\.tiqcdn\.com\/utag\/servicemaster\/[^\S;]*)")) {
            if (RegExp.$1.indexOf("/prod/") === -1) {
                var s = RegExp.$1;
                while (s.indexOf("%") != -1) {
                    s = decodeURIComponent(s);
                }
                s = s.replace(/\.\./g, "");
                ul(s);
                utag_condload = true;
                __tealium_default_path = 'https://tags.tiqcdn.com/utag/servicemaster/trmx/prod/';
            }
        }
    })();
} catch (e) {};
try {
    function doesArrayContain(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }

    function stringEndsWith(string, suffix) {
        string = string.toLowerCase();
        suffix = suffix.toLowerCase();
        return string.indexOf(suffix, string.length - suffix.length) !== -1;
    }

    function checkStringContains(string, stringContains) {
        string = string.toLowerCase();
        stringContains = stringContains.toLowerCase();
        return string.indexOf(stringContains) !== -1;
    }

    function getServiceMarketValue(formId) {
        var fieldValue = $(formId + " input[type='radio'][name='Brand']:checked").val();
        var serviceMarket = "";
        if (fieldValue === "TM") {
            serviceMarket = "residential";
        } else if (fieldValue === "TM-C") {
            serviceMarket = "commercial";
        }
        return serviceMarket;
    }

    function triggerConversionTagsFromLeadGenSubmission() {
        leadgenSubmissionTags = document.body.appendChild(document.createElement("div"));
        leadgenSubmissionTags.setAttribute("id", "leadgenConversionTags");
        leadgenSubmissionTags.style.position = "absolute";
        leadgenSubmissionTags.style.top = "0";
        leadgenSubmissionTags.style.left = "0";
        leadgenSubmissionTags.style.width = "1px";
        leadgenSubmissionTags.style.height = "1px";
        leadgenSubmissionTags.style.display = "none";
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        var facebook_pixel = "<img height='1' width='1' style='display:none' src='https://www.facebook.com/tr?id=1605499789689578&ev=PageView&noscript=1'/>",
            facebook_pixel_2 = "<img height='1' width='1' style='display:none' src='https://www.facebook.com/tr?id=427677574102359&ev=Lead&noscript=1'/>",
            twitter_pixel = "<img height='1' width='1' style='display:none;' alt='' src='//t.co/i/adsct?txn_id=l69ar&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0'/>",
            advertising_pixel = "<img src='https://secure.ace-tag.advertising.com/action/type=123678/bins=1/rich=0/mnum=1516/logs=0/site=695501/betr=teminix_leadgenconv=[+]30day[720]' width='1' height='1' border='0'>",
            turn_pixel = "<img border='0' src='https://r.turn.com/r/beacon?b2=4GX0FMjKEmWUl19DVdZazcYdedfpOhu-XY6GkztHUwiNjHhiTUh86Jy09fJ1r6kNV7jGzFXaV6TkIcnz-U92ow&cid='>",
            facebook_conversion = "<img height='1' width='1' alt='' style='display:none' src='https://www.facebook.com/tr?ev=6020328001587&cd[value]=0.00&cd[currency]=USD&noscript=1'/>",
            doubleclick_conversion = "<img src='https://ad.doubleclick.net/ddm/activity/src=4782537;type=conf076;cat=pest587;ord=1;num=" + a + "?' width='1' height='1' alt='' />",
            turn_pixel_2 = "<img border='0' src='https://r.turn.com/r/beacon?b2=SEG7yCx-6HngHbwfNGR0xpmM1ueMo-wTciTP56fegiCNjHhiTUh86Jy09fJ1r6kNQlz5hIxXmeuXTApZ6bWcgg&cid='>",
            dmn_doubleclick_tag = "<iframe src='https://5288050.fls.doubleclick.net/activityi;src=5288050;type=dmnag00;cat=termi00;qty=1;cost=[Revenue];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=[OrderID]?' width='1' height='1' frameborder='0' style='display:none'></iframe>",
            dmn_twitter_pixel = '<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nu486&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />',
            dmn_twitter_pixel_2 = '<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nu486&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />',
            dmn_pinterest_pixel = '<img height="1" width="1" alt="" src="https://ct.pinterest.com/?tid=KWPcH3ZiEep"/>',
            conversant_form_complete = '<img src="https://secure.fastclick.net/w/roitrack.cgi?aid=1000050789" width=1 height=1 border=0>';
        leadgenSubmissionTags.innerHTML = facebook_pixel + facebook_pixel_2 + twitter_pixel + advertising_pixel + turn_pixel + facebook_conversion + doubleclick_conversion + turn_pixel_2 + generate_rocketfuel_tag("8", "883", "20660541") + dmn_doubleclick_tag + dmn_pinterest_pixel + dmn_twitter_pixel + dmn_twitter_pixel_2 + conversant_form_complete;
        if (document.location.pathname.indexOf("/commercial") !== -1) {
            commercial_leadgen_script = document.createElement('script');
            commercial_leadgen_script.src = 'https://i.simpli.fi/dpx.js?cid=25637&campaign_id=0&m=1&conversion=0';
            leadgenSubmissionTags.appendChild(commercial_leadgen_script);
        }
        window.google_trackConversion({
            google_conversion_id: 946213092,
            google_conversion_language: "en",
            google_conversion_format: "3",
            google_conversion_color: "ffffff",
            google_conversion_label: "hY9ECLXVsFwQ5KGYwwM",
            google_remarketing_only: false
        });
        window.google_trackConversion({
            google_conversion_id: 971429503,
            google_conversion_language: "en",
            google_conversion_format: "3",
            google_conversion_color: "ffffff",
            google_conversion_label: "A9erCKHt9mIQ_6ybzwM",
            google_remarketing_only: false
        });

        function generate_rocketfuel_tag(rfVersion, rfPublisher, rfConversionID) {
            rfiPub = {
                ver: rfVersion,
                publisher: rfPublisher,
                conversionID: rfConversionID
            };
            rfiCustomParams = {};
            var B = document,
                C = function (c) {
                    return encodeURIComponent(c).replace(/[!'()~]/g, escape).replace(/\*/g, "%2A");
                },
                V = function () {
                    return new Date().getTime();
                },
                G = (function () {
                    var s = !1;
                    try {
                        var p = B.location.protocol;
                        s = p && p == "https:";
                    } catch (e) {}
                    return s;
                })(),
                H = G ? "https://" : "http://",
                a = "";
            if (typeof (rfiCustomParams) === "object") {
                for (var k in rfiCustomParams) {
                    a += "&" + C(k) + "=" + C(rfiCustomParams[k]);
                }
            }
            var rocketfuel_container = "<ifr" + "ame src=\"" + H + "p.rfihub.com/ca.html" + "?rb=" + rfiPub.publisher + "&ca=" + rfiPub.conversionID + "&ra=" + (V() % 1000000000) + "" + Math.floor(Math.random() * 10000) + a + "\"" + " border=0 frameborder=0 vspace=0 hspace=0 scrolling='no' marginheight='0'" + " marginwidth='0' style='display:none;padding:0;margin:0' width='0'" + " height='0'></iframe>";
            return rocketfuel_container;
        }
    }

    function getUrlVars() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        var path = "path=/";
        document.cookie = cname + "=" + cvalue + "; " + expires + "; " + path;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function cleanUdo() {
        utag_data.page_name = cleanString(utag_data.page_name);
        utag_data.page_section = cleanString(utag_data.page_section);
        utag_data.search_term = cleanString(utag_data.search_term);
        utag_data.customer_id = cleanString(utag_data.customer_id);
        utag_data.customer_first_name = cleanString(utag_data.customer_first_name);
        utag_data.customer_last_name = cleanString(utag_data.customer_last_name);
        utag_data.customer_email = cleanString(utag_data.customer_email);
        utag_data.customer_company = cleanString(utag_data.customer_company);
        utag_data.customer_address = cleanString(utag_data.customer_address);
        utag_data.customer_city = cleanString(utag_data.customer_city);
        utag_data.customer_state = cleanString(utag_data.customer_state);
        utag_data.customer_zip = cleanString(utag_data.customer_zip);
        utag_data.customer_concern = cleanString(utag_data.customer_concern);
        utag_data.customer_property_option = cleanString(utag_data.customer_property_option);
        utag_data.customer_type = cleanString(utag_data.customer_type);
        utag_data.customer_lead_call_num = cleanString(utag_data.customer_lead_call_num);
        utag_data.customer_lead_sms = cleanString(utag_data.customer_lead_sms);
        utag_data.customer_appointment_available = cleanString(utag_data.customer_appointment_available);
        utag_data.customer_appointment_date = cleanString(utag_data.customer_appointment_date);
        utag_data.customer_appointment_time = cleanString(utag_data.customer_appointment_time);
        utag_data.customer_appointment_callinnum = cleanString(utag_data.customer_appointment_callinnum);
        utag_data.customer_appointment_sms = cleanString(utag_data.customer_appointment_sms);
        utag_data.customer_appointment_usermessage = cleanString(utag_data.customer_appointment_usermessage);
        utag_data.customer_purchase_step = cleanString(utag_data.customer_purchase_step);
        utag_data.imp_ids = cleanArray(utag_data.imp_ids);
        utag_data.imp_names = cleanArray(utag_data.imp_names);
        utag_data.imp_lists = cleanArray(utag_data.imp_lists);
        utag_data.imp_categories = cleanArray(utag_data.imp_categories);
        utag_data.imp_variants = cleanArray(utag_data.imp_variants);
        utag_data.imp_positions = cleanNumArray(utag_data.imp_positions);
        utag_data.imp_prices = cleanNumArray(utag_data.imp_prices);
        utag_data.promo_ids = cleanArray(utag_data.promo_ids);
        utag_data.promo_names = cleanArray(utag_data.promo_names);
        utag_data.promo_creatives = cleanArray(utag_data.promo_creatives);
        utag_data.promo_positions = cleanNumArray(utag_data.promo_positions);
        utag_data.product_ids = cleanArray(utag_data.product_ids);
        utag_data.product_names = cleanArray(utag_data.product_names);
        utag_data.product_categories = cleanArray(utag_data.product_categories);
        utag_data.product_variants = cleanArray(utag_data.product_variants);
        utag_data.product_prices = cleanNumArray(utag_data.product_prices);
        utag_data.product_discounts = cleanArray(utag_data.product_discounts);
        utag_data.product_positions = cleanNumArray(utag_data.product_positions);
        utag_data.order_id = cleanString(utag_data.order_id);
        utag_data.order_total = cleanNumString(utag_data.order_total);
        utag_data.order_subtotal = cleanNumString(utag_data.order_subtotal);
        utag_data.order_shipping_amt = cleanNumString(utag_data.order_shipping_amt);
        utag_data.order_tax_amt = cleanNumString(utag_data.order_tax_amt);
        utag_data.order_currency_code = cleanString(utag_data.order_currency_code);
        utag_data.order_promo_code = cleanString(utag_data.order_promo_code);
        utag_data.order_discount = cleanNumString(utag_data.order_discount);
        utag_data.order_affiliation = cleanString(utag_data.order_affiliation);
        utag_data.order_type = cleanString(utag_data.order_type);
        utag_data.order_payment_method = cleanString(utag_data.order_payment_method);
        utag_data.ga_ecom_action_type = cleanString(utag_data.ga_ecom_action_type);
        utag_data.ga_action_option = cleanString(utag_data.ga_action_option);
        utag_data.ga_event_category = cleanString(utag_data.ga_event_category);
        utag_data.ga_event_action = cleanString(utag_data.ga_event_action);
        utag_data.ga_event_label = cleanString(utag_data.ga_event_label);
        utag_data.ga_event_value = cleanString(utag_data.ga_event_value);
        utag_data.cd01_Title01 = cleanString(utag_data.cd01_Title01);
        utag_data.cd02_Title02 = cleanString(utag_data.cd01_Title02);
        utag_data.cd03_Title03 = cleanString(utag_data.cd01_Title03);
        utag_data.cd04_Title04 = cleanString(utag_data.cd01_Title04);
        utag_data.cd05_Title05 = cleanString(utag_data.cd01_Title05);
        utag_data.cd06_Title06 = cleanString(utag_data.cd01_Title06);
        utag_data.cd07_Title07 = cleanString(utag_data.cd01_Title07);
        utag_data.cd08_Title08 = cleanString(utag_data.cd01_Title08);
        utag_data.cd09_Title09 = cleanString(utag_data.cd01_Title09);
        utag_data.cd10_Title10 = cleanString(utag_data.cd01_Title10);
    }

    function cleanArray(udoVar) {
        var cleanedArray = [],
            array = [];
        if (($.type(udoVar) === 'array' && udoVar.length === 1 && udoVar[0] === '') || typeof udoVar === 'undefined') {
            return [];
        }
        if ($.type(udoVar) === 'string') {
            array = udoVar.split(',');
        } else {
            array = udoVar;
        }
        for (var i = 0, l = array.length; i < l; ++i) {
            cleanedArray[i] = $.trim(array[i]);
        }
        return cleanedArray;
    }

    function cleanString(udoVar) {
        if (udoVar === '' || typeof udoVar === 'undefined') {
            return '';
        }
        var cleanedString = [],
            string = [];
        if ($.type(udoVar) === 'array') {
            string = udoVar.join(',');
        } else {
            string = udoVar;
        }
        cleanedString = string.trim();
        return cleanedString;
    }

    function cleanNumArray(udoVar) {
        var cleanedNumArray = [],
            numArray = [];
        if (($.type(udoVar) === 'array' && udoVar.length === 1 && udoVar[0] === '') || typeof udoVar === 'undefined') {
            return [];
        }
        if ($.type(udoVar) === 'string') {
            numArray = udoVar.split(',');
        } else {
            numArray = udoVar;
        }
        for (var i = 0, l = numArray.length; i < l; ++i) {
            cleanedNumArray[i] = cleanNumString(numArray[i]);
        }
        return cleanedNumArray;
    }

    function cleanNumString(udoVar) {
        if (typeof udoVar === 'undefined') {
            return '';
        } else {
            var cleanNum = udoVar.replace(/[^0-9\.]/g, '');
            return cleanNum;
        }
    }
} catch (e) {};
if (!utag_condload) {
    try {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://www.googleadservices.com/pagead/conversion_async.js';
        head.appendChild(script);
    } catch (e) {}
};
if (!utag_condload) {
    try {
        if (document.location.pathname.indexOf('/free-inspection/step-three/') !== -1) {
            utag_data['order_id'] = "";
        }
    } catch (e) {}
};
if (!utag_condload) {
    try {
        try {
            function getJSONP(url, success) {
                var ud = '_' + +new Date,
                    script = document.createElement('script'),
                    head = document.getElementsByTagName('head')[0] || document.documentElement;
                window[ud] = function (data) {
                    head.removeChild(script);
                    success && success(data);
                };
                script.src = url.replace('callback=?', 'callback=' + ud);
                head.appendChild(script);
            }

            function neustar_response(seg) {
                neustarSeg = seg.segment;
                crmSeg = seg.seg2;
            }
            getJSONP('https://aa.agkn.com/adscores/g.json?sid=9212292208&page=' + window.location.origin + window.location.pathname, function (data) {});
        } catch (e) {
            utag.DB(e)
        }
    } catch (e) {}
};
if (!utag_condload) {
    try {
        try {
            var path = window.location.pathname;
            if (/buyonline\/step\-two/.test(path)) {
                sonar_lead = {
                    transaction: 'Pest_Lead'
                };
            }
            if (/buyonline\/step\-four/.test(path)) {
                sonar_basket = {
                    products: [{
                        identifier: '111111',
                        amount: '11',
                        currency: 'USD',
                        quantity: '1'
                    }],
                    transaction: 'Pest_PurchaseConf',
                    amount: '1111',
                    currency: 'USD'
                };
            }
            if (/free\-inspection\/step\-two/.test(path)) {
                sonar_lead = {
                    transaction: 'Termite_Lead'
                };
            }
            if (/free\-inspection\/step\-three/.test(path)) {
                sonar_basket = {
                    products: [{
                        identifier: '111111',
                        amount: '11',
                        currency: 'USD',
                        quantity: '1'
                    }],
                    transaction: 'TermiteConf',
                    amount: '1111',
                    currency: 'USD'
                };
            }
        } catch (e) {
            utag.DB(e)
        }
    } catch (e) {}
};
if (!utag_condload) {
    try {
        try {
            debugger;
            if (document.URL.match(/buyonline\/address|buyonline\/pricing|buyonline\/schedule|buyonline\/payment|buyonline\/confirmation/)) {
                window.utag_cfg_ovrd = window.utag_cfg_ovrd || {};
                window.utag_cfg_ovrd.noview = true;
            }
        } catch (e) {
            utag.DB(e)
        }
    } catch (e) {}
};
if (typeof utag == "undefined" && !utag_condload) {
    var utag = {
        id: "servicemaster.trmx",
        o: {},
        sender: {},
        send: {},
        rpt: {
            ts: {
                a: new Date()
            }
        },
        dbi: [],
        loader: {
            q: [],
            lc: 0,
            f: {},
            p: 0,
            ol: 0,
            wq: [],
            lq: [],
            bq: {},
            bk: {},
            rf: 0,
            ri: 0,
            rp: 0,
            rq: [],
            ready_q: [],
            sendq: {
                "pending": 0
            },
            run_ready_q: function () {
                for (var i = 0; i < utag.loader.ready_q.length; i++) {
                    utag.DB("READY_Q:" + i);
                    try {
                        utag.loader.ready_q[i]()
                    } catch (e) {
                        utag.DB(e)
                    };
                }
            },
            lh: function (a, b, c) {
                a = "" + location.hostname;
                b = a.split(".");
                c = (/\.co\.|\.com\.|\.org\.|\.edu\.|\.net\.|\.asn\./.test(a)) ? 3 : 2;
                return b.splice(b.length - c, c).join(".");
            },
            WQ: function (a, b, c, d, g) {
                utag.DB('WQ:' + utag.loader.wq.length);
                try {
                    if (utag.udoname && utag.udoname.indexOf(".") < 0) {
                        utag.ut.merge(utag.data, window[utag.udoname], 0);
                    }
                    utag.handler.RE('view', utag.data, "bwq");
                    if (utag.cfg.load_rules_at_wait) {
                        utag.handler.LR();
                    }
                } catch (e) {
                    utag.DB(e)
                };
                d = 0;
                g = [];
                for (a = 0; a < utag.loader.wq.length; a++) {
                    b = utag.loader.wq[a];
                    b.load = utag.loader.cfg[b.id].load;
                    if (b.load == 4) {
                        this.f[b.id] = 0;
                        utag.loader.LOAD(b.id)
                    } else if (b.load > 0) {
                        g.push(b);
                        d++;
                    } else {
                        this.f[b.id] = 1;
                    }
                }
                for (a = 0; a < g.length; a++) {
                    utag.loader.AS(g[a]);
                }
                if (d == 0) {
                    utag.loader.END();
                }
            },
            AS: function (a, b, c, d) {
                utag.send[a.id] = a;
                if (typeof a.src == 'undefined') {
                    a.src = utag.cfg.path + ((typeof a.name != 'undefined') ? a.name : 'utag.' + a.id + '.js')
                }
                a.src += (a.src.indexOf('?') > 0 ? '&' : '?') + 'utv=' + (a.v ? a.v : utag.cfg.v);
                utag.rpt['l_' + a.id] = a.src;
                b = document;
                this.f[a.id] = 0;
                if (a.load == 2) {
                    utag.DB("Attach sync: " + a.src);
                    a.uid = a.id;
                    b.write('<script id="utag_' + a.id + '" src="' + a.src + '"></scr' + 'ipt>')
                    if (typeof a.cb != 'undefined') a.cb();
                } else if (a.load == 1 || a.load == 3) {
                    if (b.createElement) {
                        c = 'utag_servicemaster.trmx_' + a.id;
                        if (!b.getElementById(c)) {
                            d = {
                                src: a.src,
                                id: c,
                                uid: a.id,
                                loc: a.loc
                            }
                            if (a.load == 3) {
                                d.type = "iframe"
                            };
                            if (typeof a.cb != 'undefined') d.cb = a.cb;
                            utag.ut.loader(d);
                        }
                    }
                }
            },
            GV: function (a, b, c) {
                b = {};
                for (c in a) {
                    if (a.hasOwnProperty(c) && typeof a[c] != "function") b[c] = a[c];
                }
                return b
            },
            OU: function (a, b, c, d, f) {
                try {
                    if (typeof utag.data['cp.OPTOUTMULTI'] != 'undefined') {
                        c = utag.loader.cfg;
                        a = utag.ut.decode(utag.data['cp.OPTOUTMULTI']).split('|');
                        for (d = 0; d < a.length; d++) {
                            b = a[d].split(':');
                            if (b[1] * 1 !== 0) {
                                if (b[0].indexOf('c') == 0) {
                                    for (f in utag.loader.GV(c)) {
                                        if (c[f].tcat == b[0].substring(1)) c[f].load = 0
                                    }
                                } else if (b[0] * 1 == 0) {
                                    utag.cfg.nocookie = true
                                } else {
                                    for (f in utag.loader.GV(c)) {
                                        if (c[f].tid == b[0]) c[f].load = 0
                                    }
                                }
                            }
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            },
            RDdom: function (o) {
                var d = document || {},
                    l = location || {};
                o["dom.referrer"] = eval("document." + "referrer");
                o["dom.title"] = "" + d.title;
                o["dom.domain"] = "" + l.hostname;
                o["dom.query_string"] = ("" + l.search).substring(1);
                o["dom.hash"] = ("" + l.hash).substring(1);
                o["dom.url"] = "" + d.URL;
                o["dom.pathname"] = "" + l.pathname;
                o["dom.viewport_height"] = window.innerHeight || (d.documentElement ? d.documentElement.clientHeight : 960);
                o["dom.viewport_width"] = window.innerWidth || (d.documentElement ? d.documentElement.clientWidth : 960);
            },
            RDcp: function (o, b, c, d) {
                b = b || utag.loader.RC();
                for (d in b) {
                    if (d.match(/utag_(.*)/)) {
                        for (c in utag.loader.GV(b[d])) {
                            o["cp.utag_" + RegExp.$1 + "_" + c] = b[d][c];
                        }
                    }
                }
                for (c in utag.loader.GV((utag.cl && !utag.cl['_all_']) ? utag.cl : b)) {
                    if (c.indexOf("utag_") < 0 && typeof b[c] != "undefined") o["cp." + c] = b[c];
                }
            },
            RDqp: function (o, a, b, c) {
                a = location.search + (location.hash + '').replace("#", "&");
                if (utag.cfg.lowerqp) {
                    a = a.toLowerCase()
                };
                if (a.length > 1) {
                    b = a.substring(1).split('&');
                    for (a = 0; a < b.length; a++) {
                        c = b[a].split("=");
                        if (c.length > 1) {
                            o["qp." + c[0]] = utag.ut.decode(c[1])
                        }
                    }
                }
            },
            RDmeta: function (o, a, b, h) {
                a = document.getElementsByTagName("meta");
                for (b = 0; b < a.length; b++) {
                    try {
                        h = a[b].name || a[b].getAttribute("property") || "";
                    } catch (e) {
                        h = "";
                        utag.DB(e)
                    };
                    if (utag.cfg.lowermeta) {
                        h = h.toLowerCase()
                    };
                    if (h != "") {
                        o["meta." + h] = a[b].content
                    }
                }
            },
            RDva: function (o, a, b) {
                a = "";
                try {
                    a = localStorage.getItem("tealium_va");
                    if (!a || a == "{}") return;
                    b = utag.ut.flatten({
                        va: JSON.parse(a)
                    });
                    utag.ut.merge(o, b, 1);
                } catch (e) {
                    utag.DB("localStorage not supported");
                }
            },
            RDut: function (o, a) {
                o["ut.domain"] = utag.cfg.domain;
                o["ut.version"] = utag.cfg.v;
                o["ut.event"] = a || "view";
                try {
                    o["ut.account"] = utag.cfg.utid.split("/")[0];
                    o["ut.profile"] = utag.cfg.utid.split("/")[1];
                    o["ut.env"] = utag.cfg.path.split("/")[6];
                } catch (e) {
                    utag.DB(e)
                }
            },
            RD: function (o, a, b, c, d) {
                utag.DB("utag.loader.RD");
                if (typeof o["_t_session_id"] != "undefined") {
                    return
                };
                a = (new Date()).getTime();
                b = utag.loader.RC();
                c = a + parseInt(utag.cfg.session_timeout);
                d = a;
                if (!b.utag_main) {
                    b.utag_main = {};
                } else if (b.utag_main.ses_id && typeof b.utag_main._st != "undefined" && parseInt(b.utag_main._st) < a) {
                    delete b.utag_main.ses_id;
                }
                if (!b.utag_main.v_id) {
                    b.utag_main.v_id = utag.ut.vi(a);
                }
                if (!b.utag_main.ses_id) {
                    b.utag_main.ses_id = d + '';
                    b.utag_main._ss = b.utag_main._pn = 1;
                    b.utag_main._sn = 1 + parseInt(b.utag_main._sn || 0);
                } else {
                    d = b.utag_main.ses_id;
                    b.utag_main._ss = 0;
                    b.utag_main._pn = 1 + parseInt(b.utag_main._pn);
                    b.utag_main._sn = parseInt(b.utag_main._sn);
                }
                if (isNaN(b.utag_main._sn) || b.utag_main._sn < 1) {
                    b.utag_main._sn = b.utag_main._pn = 1
                }
                b.utag_main._st = c + '';
                utag.loader.SC("utag_main", {
                    "v_id": b.utag_main.v_id,
                    "_sn": b.utag_main._sn,
                    "_ss": b.utag_main._ss,
                    "_pn": b.utag_main._pn + ";exp-session",
                    "_st": c,
                    "ses_id": d + ";exp-session"
                });
                o["_t_visitor_id"] = b.utag_main.v_id;
                o["_t_session_id"] = d;
                this.RDqp(o);
                this.RDmeta(o);
                this.RDcp(o, b);
                this.RDdom(o);
                this.RDva(o);
                this.RDut(o);
            },
            RC: function (a, x, b, c, d, e, f, g, h, i, j, k, l, m, n, o, v, ck, cv, r, s, t) {
                o = {};
                b = ("" + document.cookie != "") ? (document.cookie).split("; ") : [];
                r = /^(.*?)=(.*)$/;
                s = /^(.*);exp-(.*)$/;
                t = (new Date()).getTime();
                for (c = 0; c < b.length; c++) {
                    if (b[c].match(r)) {
                        ck = RegExp.$1;
                        cv = RegExp.$2;
                    }
                    e = utag.ut.decode(cv);
                    if (typeof ck != "undefined") {
                        if (ck.indexOf("ulog") == 0 || ck.indexOf("utag_") == 0) {
                            e = e.split("$");
                            g = [];
                            j = {};
                            for (f = 0; f < e.length; f++) {
                                try {
                                    g = e[f].split(":");
                                    if (g.length > 2) {
                                        g[1] = g.slice(1).join(":");
                                    }
                                    v = "";
                                    if (("" + g[1]).indexOf("~") == 0) {
                                        h = g[1].substring(1).split("|");
                                        for (i = 0; i < h.length; i++) h[i] = utag.ut.decode(h[i]);
                                        v = h
                                    } else v = utag.ut.decode(g[1]);
                                    j[g[0]] = v;
                                } catch (er) {
                                    utag.DB(er)
                                };
                            }
                            o[ck] = {};
                            for (f in utag.loader.GV(j)) {
                                if (j[f] instanceof Array) {
                                    n = [];
                                    for (m = 0; m < j[f].length; m++) {
                                        if (j[f][m].match(s)) {
                                            k = (RegExp.$2 == "session") ? (typeof j._st != "undefined" ? j._st : t - 1) : parseInt(RegExp.$2);
                                            if (k > t) n[m] = (x == 0) ? j[f][m] : RegExp.$1;
                                        }
                                    }
                                    j[f] = n.join("|");
                                } else {
                                    j[f] = "" + j[f];
                                    if (j[f].match(s)) {
                                        k = (RegExp.$2 == "session") ? (typeof j._st != "undefined" ? j._st : t - 1) : parseInt(RegExp.$2);
                                        j[f] = (k < t) ? null : (x == 0 ? j[f] : RegExp.$1);
                                    }
                                }
                                if (j[f]) o[ck][f] = j[f];
                            }
                        } else if (utag.cl[ck] || utag.cl['_all_']) {
                            o[ck] = e
                        }
                    }
                }
                return (a) ? (o[a] ? o[a] : {}) : o;
            },
            SC: function (a, b, c, d, e, f, g, h, i, j, k, x, v) {
                if (!a) return 0;
                if (a == "utag_main" && utag.cfg.nocookie) return 0;
                v = "";
                var date = new Date();
                var exp = new Date();
                exp.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
                x = exp.toGMTString();
                if (c && c == "da") {
                    x = "Thu, 31 Dec 2009 00:00:00 GMT";
                } else if (a.indexOf("utag_") != 0 && a.indexOf("ulog") != 0) {
                    if (typeof b != "object") {
                        v = b
                    }
                } else {
                    d = utag.loader.RC(a, 0);
                    for (e in utag.loader.GV(b)) {
                        f = "" + b[e];
                        if (f.match(/^(.*);exp-(\d+)(\w)$/)) {
                            g = date.getTime() + parseInt(RegExp.$2) * ((RegExp.$3 == "h") ? 3600000 : 86400000);
                            if (RegExp.$3 == "u") g = parseInt(RegExp.$2);
                            f = RegExp.$1 + ";exp-" + g;
                        }
                        if (c == "i") {
                            if (d[e] == null) d[e] = f;
                        } else if (c == "d") delete d[e];
                        else if (c == "a") d[e] = (d[e] != null) ? (f - 0) + (d[e] - 0) : f;
                        else if (c == "ap" || c == "au") {
                            if (d[e] == null) d[e] = f;
                            else {
                                if (d[e].indexOf("|") > 0) {
                                    d[e] = d[e].split("|")
                                }
                                g = (d[e] instanceof Array) ? d[e] : [d[e]];
                                g.push(f);
                                if (c == "au") {
                                    h = {};
                                    k = {};
                                    for (i = 0; i < g.length; i++) {
                                        if (g[i].match(/^(.*);exp-(.*)$/)) {
                                            j = RegExp.$1;
                                        }
                                        if (typeof k[j] == "undefined") {
                                            k[j] = 1;
                                            h[g[i]] = 1;
                                        }
                                    }
                                    g = [];
                                    for (i in utag.loader.GV(h)) {
                                        g.push(i);
                                    }
                                }
                                d[e] = g
                            }
                        } else d[e] = f;
                    }
                    h = new Array();
                    for (g in utag.loader.GV(d)) {
                        if (d[g] instanceof Array) {
                            for (c = 0; c < d[g].length; c++) {
                                d[g][c] = encodeURIComponent(d[g][c])
                            }
                            h.push(g + ":~" + d[g].join("|"))
                        } else h.push(g + ":" + encodeURIComponent(d[g]))
                    };
                    if (h.length == 0) {
                        h.push("");
                        x = ""
                    }
                    v = (h.join("$"));
                }
                document.cookie = a + "=" + v + ";path=/;domain=" + utag.cfg.domain + ";expires=" + x;
                return 1
            },
            LOAD: function (a, b, c, d) {
                if (!utag.loader.cfg) {
                    return
                }
                if (this.ol == 0) {
                    if (utag.loader.cfg[a].block && utag.loader.cfg[a].cbf) {
                        this.f[a] = 1;
                        delete utag.loader.bq[a];
                    }
                    for (b in utag.loader.GV(utag.loader.bq)) {
                        if (utag.loader.cfg[a].load == 4 && utag.loader.cfg[a].wait == 0) {
                            utag.loader.bk[a] = 1;
                            utag.DB("blocked: " + a);
                        }
                        utag.DB("blocking: " + b);
                        return;
                    }
                    utag.loader.INIT();
                    return;
                }
                utag.DB('utag.loader.LOAD:' + a);
                if (this.f[a] == 0) {
                    this.f[a] = 1;
                    if (utag.cfg.noview != true) {
                        if (utag.loader.cfg[a].send) {
                            utag.DB("SENDING: " + a);
                            try {
                                if (utag.loader.sendq.pending > 0 && utag.loader.sendq[a]) {
                                    utag.DB("utag.loader.LOAD:sendq: " + a);
                                    while (d = utag.loader.sendq[a].shift()) {
                                        utag.DB(d);
                                        utag.sender[a].send(d.event, utag.handler.C(d.data));
                                        utag.loader.sendq.pending--;
                                    }
                                } else {
                                    utag.sender[a].send('view', utag.handler.C(utag.data));
                                }
                                utag.rpt['s_' + a] = 0;
                            } catch (e) {
                                utag.DB(e);
                                utag.rpt['s_' + a] = 1;
                            }
                        }
                    }
                    if (utag.loader.rf == 0) return;
                    for (b in utag.loader.GV(this.f)) {
                        if (this.f[b] == 0 || this.f[b] == 2) return
                    }
                    utag.loader.END();
                }
            },
            EV: function (a, b, c, d) {
                if (b == "ready") {
                    if (!utag.data) {
                        try {
                            utag.cl = {
                                '_all_': 1
                            };
                            utag.loader.initdata();
                            utag.loader.RD(utag.data);
                        } catch (e) {
                            utag.DB(e)
                        };
                    }
                    if ((document.attachEvent || utag.cfg.dom_complete) ? document.readyState === "complete" : document.readyState !== "loading") setTimeout(c, 1);
                    else {
                        utag.loader.ready_q.push(c);
                        var RH;
                        if (utag.loader.ready_q.length <= 1) {
                            if (document.addEventListener) {
                                RH = function () {
                                    document.removeEventListener("DOMContentLoaded", RH, false);
                                    utag.loader.run_ready_q()
                                };
                                if (!utag.cfg.dom_complete) document.addEventListener("DOMContentLoaded", RH, false);
                                window.addEventListener("load", utag.loader.run_ready_q, false);
                            } else if (document.attachEvent) {
                                RH = function () {
                                    if (document.readyState === "complete") {
                                        document.detachEvent("onreadystatechange", RH);
                                        utag.loader.run_ready_q()
                                    }
                                };
                                document.attachEvent("onreadystatechange", RH);
                                window.attachEvent("onload", utag.loader.run_ready_q);
                            }
                        }
                    }
                } else {
                    if (a.addEventListener) {
                        a.addEventListener(b, c, false)
                    } else if (a.attachEvent) {
                        a.attachEvent(((d == 1) ? "" : "on") + b, c)
                    }
                }
            },
            END: function (b, c, d, e, v, w) {
                if (this.ended) {
                    return
                };
                this.ended = 1;
                utag.DB("loader.END");
                b = utag.data;
                if (utag.handler.base && utag.handler.base != '*') {
                    e = utag.handler.base.split(",");
                    for (d = 0; d < e.length; d++) {
                        if (typeof b[e[d]] != "undefined") utag.handler.df[e[d]] = b[e[d]]
                    }
                } else if (utag.handler.base == '*') {
                    utag.ut.merge(utag.handler.df, b, 1);
                }
                utag.rpt['r_0'] = "t";
                for (var r in utag.loader.GV(utag.cond)) {
                    utag.rpt['r_' + r] = (utag.cond[r]) ? "t" : "f";
                }
                utag.rpt.ts['s'] = new Date();
                (function (a, b, c, l) {
                    if (typeof utag_err != 'undefined' && utag_err.length > 0) {
                        a = 'https://uconnect.tealiumiq.com/ulog/_error?utid=' + utag.cfg.utid;
                        l = utag_err.length > 5 ? 5 : utag_err.length;
                        for (b = 0; b < l; b++) {
                            c = utag_err[b];
                            a += '&e' + b + '=' + encodeURIComponent(c.t + '::' + c.l + '::' + c.s + '::' + c.e);
                        }
                        utag.dbi.push((new Image()).src = a);
                    }
                })();
                v = ".tiqcdn.com";
                w = utag.cfg.path.indexOf(v);
                if (w > 0 && b["cp.utag_main__ss"] == 1) utag.ut.loader({
                    src: utag.cfg.path.substring(0, w) + v + "/utag/tiqapp/utag.v.js?a=" + utag.cfg.utid + (utag.cfg.nocookie ? "&nocookie=1" : "&cb=" + (new Date).getTime()),
                    id: "tiqapp"
                })
                utag.handler.RE('view', b, "end");
                utag.handler.INIT();
            }
        },
        DB: function (a, b) {
            if (utag.cfg.utagdb === false) {
                return;
            } else if (typeof utag.cfg.utagdb == "undefined") {
                utag.db_log = [];
                b = document.cookie + '';
                utag.cfg.utagdb = ((b.indexOf('utagdb=true') >= 0) ? true : false);
            }
            if (utag.cfg.utagdb === true) {
                var t;
                if (utag.ut.typeOf(a) == "object") {
                    t = utag.handler.C(a)
                } else {
                    t = a
                }
                utag.db_log.push(t);
                try {
                    console.log(t)
                } catch (e) {}
            }
        },
        RP: function (a, b, c) {
            if (typeof a != 'undefined' && typeof a.src != 'undefined' && a.src != '') {
                b = [];
                for (c in utag.loader.GV(a)) {
                    if (c != 'src') b.push(c + '=' + escape(a[c]))
                }
                this.dbi.push((new Image()).src = a.src + '?utv=' + utag.cfg.v + '&utid=' + utag.cfg.utid + '&' + (b.join('&')))
            }
        },
        view: function (a, c, d) {
            return this.track({
                event: 'view',
                data: a,
                cfg: {
                    cb: c,
                    uids: d
                }
            })
        },
        link: function (a, c) {
            return this.track({
                event: 'link',
                data: a,
                cfg: {
                    cb: c
                }
            })
        },
        track: function (a, b, c, d) {
            if (typeof a == "string") a = {
                event: a,
                data: b,
                cfg: {
                    cb: c
                }
            };
            for (d in utag.loader.GV(utag.o)) {
                try {
                    utag.o[d].handler.trigger(a.event || "view", a.data || a, a.cfg)
                } catch (e) {
                    utag.DB(e)
                };
            }
            if (a.cfg && a.cfg.cb) try {
                a.cfg.cb()
            } catch (e) {
                utag.DB(e)
            };
            return true
        },
        handler: {
            base: "",
            df: {},
            o: {},
            send: {},
            iflag: 0,
            INIT: function (a, b, c) {
                utag.DB('utag.handler.INIT');
                if (utag.initcatch) {
                    utag.initcatch = 0;
                    return
                }
                this.iflag = 1;
                a = utag.loader.q.length;
                if (a > 0) {
                    for (b = 0; b < a; b++) {
                        c = utag.loader.q[b];
                        utag.handler.trigger(c.a, c.b)
                    }
                }
            },
            test: function () {
                return 1
            },
            LR: function () {
                utag.DB("Load Rules");
                for (var d in utag.loader.GV(utag.cond)) {
                    utag.cond[d] = false;
                }
                utag.DB(utag.data);
                utag.loader.loadrules();
                utag.DB(utag.cond);
                utag.loader.initcfg();
                utag.loader.OU();
            },
            RE: function (a, b, c, d, e, f, g) {
                if (c && !this.cfg_extend) {
                    return 0;
                }
                utag.DB('All Tags EXTENSIONS');
                utag.DB(b);
                if (typeof this.extend != "undefined") {
                    g = 0;
                    for (d = 0; d < this.extend.length; d++) {
                        try {
                            e = 0;
                            if (typeof this.cfg_extend != "undefined") {
                                f = this.cfg_extend[d];
                                if (typeof f.count == "undefined") f.count = 0;
                                if (f[a] == 0 || (f.once == 1 && f.count > 0) || (typeof c != "undefined" && f[c] == 0)) {
                                    e = 1
                                } else {
                                    if (typeof c != "undefined" && f[c] == 1) {
                                        g = 1
                                    };
                                    f.count++
                                }
                            }
                            if (e != 1) {
                                this.extend[d](a, b);
                                utag.rpt['ex_' + d] = 0
                            }
                        } catch (er) {
                            utag.rpt['ex_' + d] = 1;
                            utag.ut.error({
                                e: er.message,
                                s: utag.cfg.path + 'utag.js',
                                l: d,
                                t: 'ge'
                            });
                        }
                    }
                    utag.DB(b);
                    return g;
                }
            },
            trigger: function (a, b, c, d, e, f) {
                utag.DB('trigger:' + a);
                b = b || {};
                utag.DB(b);
                if (!this.iflag) {
                    for (d in utag.loader.f) {
                        if (!(utag.loader.f[d] === 1)) utag.DB('Tag ' + d + ' did not LOAD')
                    }
                    utag.loader.q.push({
                        a: a,
                        b: b
                    });
                    return;
                }
                utag.cfg.noview = false;
                utag.ut.merge(b, this.df, 0);
                utag.loader.RDqp(b);
                utag.loader.RDcp(b);
                utag.loader.RDdom(b);
                utag.loader.RDmeta(b);
                utag.loader.RDva(b);
                utag.loader.RDut(b, a);

                function sendTag(a, b, d) {
                    try {
                        if (typeof utag.sender[d] != "undefined") {
                            utag.DB("SENDING: " + d);
                            utag.sender[d].send(a, utag.handler.C(b));
                            utag.rpt['s_' + d] = 0;
                        } else if (utag.loader.cfg[d].load != 2 && utag.loader.cfg[d].s2s != 1) {
                            utag.loader.sendq[d] = utag.loader.sendq[d] || [];
                            utag.loader.sendq[d].push({
                                "event": a,
                                "data": b
                            });
                            utag.loader.sendq.pending++;
                            utag.loader.AS({
                                id: d,
                                load: 1
                            });
                        }
                    } catch (e) {
                        utag.DB(e)
                    }
                }
                if (c && c.uids) {
                    this.RE(a, b);
                    for (f = 0; f < c.uids.length; f++) {
                        d = c.uids[f];
                        sendTag(a, b, d);
                    }
                } else if (utag.cfg.load_rules_ajax) {
                    this.RE(a, b, "blr");
                    utag.ut.merge(utag.data, b, 1);
                    this.LR();
                    this.RE(a, b);
                    for (f = 0; f < utag.loader.cfgsort.length; f++) {
                        d = utag.loader.cfgsort[f];
                        if (utag.loader.cfg[d].load && utag.loader.cfg[d].send) {
                            sendTag(a, b, d);
                        }
                    }
                } else {
                    this.RE(a, b);
                    for (d in utag.loader.GV(utag.sender)) {
                        sendTag(a, b, d);
                    }
                }
                utag.loader.SC("utag_main", {
                    "_st": ((new Date()).getTime() + parseInt(utag.cfg.session_timeout))
                });
            },
            C: function (a, b, c) {
                b = {};
                for (c in utag.loader.GV(a)) {
                    if (a[c] instanceof Array) {
                        b[c] = a[c].slice(0)
                    } else {
                        b[c] = a[c]
                    }
                }
                return b
            }
        },
        ut: {
            pad: function (a, b, c, d) {
                a = "" + ((a - 0).toString(16));
                d = '';
                if (b > a.length) {
                    for (c = 0; c < (b - a.length); c++) {
                        d += '0'
                    }
                }
                return "" + d + a
            },
            vi: function (t, a, b) {
                if (!utag.v_id) {
                    a = this.pad(t, 12);
                    b = "" + Math.random();
                    a += this.pad(b.substring(2, b.length), 16);
                    try {
                        a += this.pad((navigator.plugins.length ? navigator.plugins.length : 0), 2);
                        a += this.pad(navigator.userAgent.length, 3);
                        a += this.pad(document.URL.length, 4);
                        a += this.pad(navigator.appVersion.length, 3);
                        a += this.pad(screen.width + screen.height + parseInt((screen.colorDepth) ? screen.colorDepth : screen.pixelDepth), 5)
                    } catch (e) {
                        utag.DB(e);
                        a += "12345"
                    };
                    utag.v_id = a;
                }
                return utag.v_id
            },
            isEmptyObject: function (o, a) {
                for (a in o) {
                    return false;
                }
                return true;
            },
            typeOf: function (e) {
                return ({}).toString.call(e).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
            },
            flatten: function (o) {
                var a = {};

                function r(c, p) {
                    if (Object(c) !== c || c instanceof Array) {
                        a[p] = c;
                    } else {
                        if (utag.ut.isEmptyObject(c)) {} else {
                            for (var d in c) {
                                r(c[d], p ? p + "." + d : d);
                            }
                        }
                    }
                }
                r(o, "");
                return a;
            },
            merge: function (a, b, c, d) {
                if (c) {
                    for (d in utag.loader.GV(b)) {
                        a[d] = b[d]
                    }
                } else {
                    for (d in utag.loader.GV(b)) {
                        if (typeof a[d] == "undefined") a[d] = b[d]
                    }
                }
            },
            decode: function (a, b) {
                b = "";
                try {
                    b = decodeURIComponent(a)
                } catch (e) {
                    utag.DB(e)
                };
                if (b == "") {
                    b = unescape(a)
                };
                return b
            },
            error: function (a, b, c) {
                if (typeof utag_err != "undefined") {
                    utag_err.push(a)
                }
                c = "";
                for (b in a) {
                    c += b + ":" + a[b] + " , "
                };
                utag.DB(c)
            },
            loader: function (o, a, b, c, l) {
                a = document;
                if (o.type == "iframe") {
                    b = a.createElement("iframe");
                    o.attrs = o.attrs || {
                        "height": "1",
                        "width": "1",
                        "style": "display:none"
                    };
                    for (l in utag.loader.GV(o.attrs)) {
                        b.setAttribute(l, o.attrs[l])
                    }
                    b.setAttribute("src", o.src);
                } else if (o.type == "img") {
                    utag.DB("Attach img: " + o.src);
                    b = new Image();
                    b.src = o.src;
                    return;
                } else {
                    b = a.createElement("script");
                    b.language = "javascript";
                    b.type = "text/javascript";
                    b.async = 1;
                    b.charset = "utf-8";
                    for (l in utag.loader.GV(o.attrs)) {
                        b[l] = o.attrs[l]
                    }
                    b.src = o.src;
                }
                if (o.id) {
                    b.id = o.id
                };
                if (typeof o.cb == "function") {
                    if (b.addEventListener) {
                        b.addEventListener("load", function () {
                            o.cb()
                        }, false);
                    } else {
                        b.onreadystatechange = function () {
                            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                                this.onreadystatechange = null;
                                o.cb()
                            }
                        };
                    }
                }
                l = o.loc || "head";
                c = a.getElementsByTagName(l)[0];
                if (c) {
                    utag.DB("Attach to " + l + ": " + o.src);
                    if (l == "script") {
                        c.parentNode.insertBefore(b, c);
                    } else {
                        c.appendChild(b)
                    }
                }
            }
        }
    };
    utag.o['servicemaster.trmx'] = utag;
    utag.cfg = {
        v: "ut4.36.201809211413",
        load_rules_ajax: true,
        load_rules_at_wait: false,
        lowerqp: false,
        session_timeout: 1800000,
        readywait: 0,
        noload: 0,
        domain: utag.loader.lh(),
        path: "https://tags.tiqcdn.com/utag/servicemaster/trmx/prod/",
        utid: "servicemaster/trmx/201809211413"
    };
    try {
        var _gaq = _gaq || [];
        var pageTracker = pageTracker || {
            _trackEvent: function (c, d, e, f, g) {
                g = {
                    ga_eventCat: c,
                    ga_eventAction: d,
                    ga_eventLabel: e,
                    ga_eventValue: f
                };
                utag.link(g, null, [4]);
            },
            _trackPageview: function (c) {
                _gaq.push(['_trackPageview', c ? c : null]);
            }
        }
    } catch (e) {};
    try {
        var _gaq = _gaq || [];
        var pageTracker = pageTracker || {
            _trackEvent: function (c, d, e, f, g) {
                g = {
                    ga_eventCat: c,
                    ga_eventAction: d,
                    ga_eventLabel: e,
                    ga_eventValue: f
                };
                utag.link(g, null, [5]);
            },
            _trackPageview: function (c) {
                _gaq.push(['_trackPageview', c ? c : null]);
            }
        }
    } catch (e) {};
    utag.cond = {
        19: 0,
        20: 0,
        24: 0,
        25: 0,
        31: 0,
        32: 0,
        35: 0,
        39: 0,
        43: 0,
        44: 0,
        4: 0,
        52: 0,
        53: 0,
        54: 0,
        56: 0,
        57: 0,
        58: 0,
        59: 0,
        60: 0,
        61: 0,
        62: 0,
        66: 0
    };
    utag.loader.initdata = function () {
        try {
            utag.data = (typeof utag_data != 'undefined') ? utag_data : {};
            utag.udoname = 'utag_data';
        } catch (e) {
            utag.data = {};
            utag.DB('idf:' + e);
        }
    };
    utag.loader.loadrules = function (_pd, _pc) {
        var d = _pd || utag.data;
        var c = _pc || utag.cond;
        for (var l in utag.loader.GV(c)) {
            switch (l) {
                case '19':
                    try {
                        c[19] |= (d['dom.pathname'].toString().toLowerCase().indexOf('thank-you'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '20':
                    try {
                        c[20] |= (d['dom.pathname'].toString().toLowerCase().indexOf('buyonline/step-two'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('buyonline/step-2'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '24':
                    try {
                        c[24] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-two'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection/step-two'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-2'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '25':
                    try {
                        c[25] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection/step-three'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '31':
                    try {
                        c[31] |= (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/buyonline/pricing'.toLowerCase()) < 0) || (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/buyonline/confirmation'.toLowerCase()) < 0) || (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/buyonline/step-two'.toLowerCase()) < 0) || (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/buyonline/step-four'.toLowerCase()) < 0) || (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/free-inspection/step-two'.toLowerCase()) < 0) || (d['dom.domain'].toString().indexOf('terminix.com') > -1 && d['dom.url'].toString().toLowerCase().indexOf('/free-inspection/step-three'.toLowerCase()) < 0)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '32':
                    try {
                        c[32] |= (d['dom.url'].toString().indexOf('/buyonline/step-four') > -1) || (d['dom.url'].toString().indexOf('/buyonline/step-two') > -1) || (d['dom.url'].toString().indexOf('/free-inspection/step-two') > -1) || (d['dom.url'].toString().indexOf('/free-inspection/step-three') > -1) || (d['dom.url'].toString().indexOf('/buyonline/pricing') > -1) || (d['dom.url'].toString().indexOf('/buyonline/confirmation') > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '35':
                    try {
                        c[35] |= (d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase() && /^\/my-account/i.test(d['dom.pathname']))
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '39':
                    try {
                        c[39] |= (d['dom.pathname'].toString().indexOf('buyonline') > -1) || (d['dom.pathname'].toString().indexOf('free-inspection') > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '4':
                    try {
                        c[4] |= (d['dom.domain'].toString().toLowerCase() == 'prod2.terminix.com'.toLowerCase()) || (d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase())
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '43':
                    try {
                        c[43] |= (d['dom.domain'].toString().toLowerCase().indexOf('www.terminix.com'.toLowerCase()) > -1 && d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection/step-three'.toLowerCase()) < 0) || (d['dom.domain'].toString().toLowerCase().indexOf('www.terminix.com'.toLowerCase()) > -1 && d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-four'.toLowerCase()) < 0)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '44':
                    try {
                        c[44] |= (d['dom.pathname'].toString().toLowerCase() == '/'.toLowerCase()) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/ants'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/cockroaches'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/mosquitoes'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/rodents'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/spiders'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/wildlife'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control/other-pests'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/termite-control'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/termite-control/treatment'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/termite-control/protection'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/bed-bug-control'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/additional-pest-solutions'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/additional-pest-solutions/crawl-space-services'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/additional-pest-solutions/attic-insulation'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/commercial'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/about'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/blog'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-one'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/my-account'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '52':
                    try {
                        c[52] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/pest-control'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-one'.toLowerCase()) > -1) || (d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-two'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '53':
                    try {
                        c[53] |= (d['dom.url'].toString().toLowerCase().indexOf('https://www.terminix.com/free-inspection/'.toLowerCase()) > -1 && d['dom.url'].toString().toLowerCase().indexOf('two'.toLowerCase()) < 0 && d['dom.url'].toString().toLowerCase().indexOf('three'.toLowerCase()) < 0) || (d['dom.url'].toString().toLowerCase().indexOf('https://www.terminix.com/termite-control'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '54':
                    try {
                        c[54] |= (d['dom.url'].toString().toLowerCase().indexOf('free-inspection/step-three'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '56':
                    try {
                        c[56] |= (d['dom.url'].toString().toLowerCase().indexOf('www.terminix.com'.toLowerCase()) > -1 && d['dom.pathname'].toString().toLowerCase().indexOf('free-inspection/step-two'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '57':
                    try {
                        c[57] |= (d['dom.url'].toString().toLowerCase().indexOf('www.terminix.com/buyonline/'.toLowerCase()) > -1 && d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase())
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '58':
                    try {
                        c[58] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection/step-two'.toLowerCase()) > -1 && d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase())
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '59':
                    try {
                        c[59] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection'.toLowerCase()) > -1 && d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase())
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '60':
                    try {
                        c[60] |= (d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase() && d['dom.pathname'].toString().toLowerCase().indexOf('/buyonline/step-two'.toLowerCase()) > -1)
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '61':
                    try {
                        c[61] |= (d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase() && /^\/buyonline\/step-four/i.test(d['dom.pathname']))
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '62':
                    try {
                        c[62] |= (d['dom.pathname'].toString().toLowerCase().indexOf('/free-inspection/step-three'.toLowerCase()) > -1 && d['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase())
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
                case '66':
                    try {
                        c[66] |= (/buyonline\/confirmation.*/.test(d['dom.pathname'])) || (/buyonline\/step-4.*/.test(d['dom.pathname']))
                    } catch (e) {
                        utag.DB(e)
                    };
                    break;
            }
        }
    };
    utag.pre = function () {
        utag.loader.initdata();
        try {
            utag.loader.RD(utag.data)
        } catch (e) {
            utag.DB(e)
        };
        utag.loader.loadrules();
    };
    utag.loader.GET = function () {
        utag.cl = {
            '_all_': 1
        };
        utag.pre();
        utag.handler.extend = [function (a, b) {
            cleanUdo();
        }, function (a, b, c, d) {
            b._ccity = '';
            b._ccountry = '';
            b._ccurrency = (typeof b['order_currency_code'] != 'undefined') ? b['order_currency_code'] : '';
            b._ccustid = '';
            b._corder = (typeof b['order_id'] != 'undefined') ? b['order_id'] : '';
            b._cpromo = (typeof b['order_promo_code'] != 'undefined') ? b['order_promo_code'] : '';
            b._cship = (typeof b['order_shipping_amt'] != 'undefined') ? b['order_shipping_amt'] : '';
            b._cstate = '';
            b._cstore = (typeof b['order_agent'] != 'undefined') ? b['order_agent'] : 'web';
            b._csubtotal = (typeof b['order_subtotal'] != 'undefined') ? b['order_subtotal'] : '';
            b._ctax = (typeof b['order_tax_amt'] != 'undefined') ? b['order_tax_amt'] : '';
            b._ctotal = (typeof b['order_total'] != 'undefined') ? b['order_total'] : '';
            b._ctype = '';
            b._czip = '';
            b._cprod = (typeof b['product_ids'] != 'undefined' && b['product_ids'].length > 0) ? b['product_ids'] : [];
            b._cprodname = (typeof b['product_names'] != 'undefined' && b['product_names'].length > 0) ? b['product_names'] : [];
            b._cbrand = [];
            b._ccat = (typeof b['product_categories'] != 'undefined' && b['product_categories'].length > 0) ? b['product_categories'] : [];
            b._ccat2 = [];
            b._cquan = [];
            b._cprice = (typeof b['product_prices'] != 'undefined' && b['product_prices'].length > 0) ? b['product_prices'] : [];
            b._csku = (typeof b['product_ids'] != 'undefined' && b['product_ids'].length > 0) ? b['product_ids'] : [];
            b._cpdisc = [];
            if (b._cprod.length == 0) {
                b._cprod = b._csku.slice()
            };
            if (b._cprodname.length == 0) {
                b._cprodname = b._csku.slice()
            };

            function tf(a) {
                if (a == '' || isNaN(parseFloat(a))) {
                    return a
                } else {
                    return (parseFloat(a)).toFixed(2)
                }
            };
            b._ctotal = tf(b._ctotal);
            b._csubtotal = tf(b._csubtotal);
            b._ctax = tf(b._ctax);
            b._cship = tf(b._cship);
            for (c = 0; c < b._cprice.length; c++) {
                b._cprice[c] = tf(b._cprice[c])
            };
            for (c = 0; c < b._cpdisc.length; c++) {
                b._cpdisc[c] = tf(b._cpdisc[c])
            };
        }, function (a, b) {
            try {
                if (1) {
                    try {
                        b['tealiumEnv'] = utag.data['ut.env'];
                    } catch (e) {}
                }
            } catch (e) {
                utag.DB(e)
            }
        }, function (a, b, c, d, e, f, g) {
            d = b['tealiumEnv'];
            if (typeof d == 'undefined') return;
            c = [{
                'dev': 'UA-2425893-27'
            }, {
                'qa': 'UA-2425893-29'
            }, {
                'prod': 'UA-2425893-8'
            }];
            var m = false;
            for (e = 0; e < c.length; e++) {
                for (f in c[e]) {
                    if (d == f) {
                        b['ga_rollup_account_id'] = c[e][f];
                        m = true
                    };
                };
                if (m) break
            };
            if (!m) b['ga_rollup_account_id'] = 'UA-2425893-28';
        }, function (a, b, c, d, e, f, g) {
            d = b['tealiumEnv'];
            if (typeof d == 'undefined') return;
            c = [{
                'dev': 'UA-2425893-28'
            }, {
                'qa': 'UA-2425893-30'
            }, {
                'prod': 'UA-2425893-9'
            }];
            var m = false;
            for (e = 0; e < c.length; e++) {
                for (f in c[e]) {
                    if (d == f) {
                        b['ga_main_account_id'] = c[e][f];
                        m = true
                    };
                };
                if (m) break
            };
            if (!m) b['ga_main_account_id'] = 'UA-2425893-28';
        }, function (a, b) {
            try {
                if ((b['dom.pathname'].toString().indexOf('/my-account/pages/home.jsp') > -1 && typeof b['ga_page_including_hash'] == 'undefined')) {
                    try {
                        b['ga_page_including_hash'] = window.location.pathname + window.location
                    } catch (e) {}
                }
            } catch (e) {
                utag.DB(e)
            }
        }, function (a, b) {
            var thisPath = window.location.pathname;
            var ecomcheck = thisPath.indexOf("/buyonline/step-four");
            var signupcheck = thisPath.indexOf("/free-inspection/step-three");
            if (ecomcheck == 0) {
                utag_data.marin_conversion_type = 'Ecomm';
                utag_data.marin_order_id = utag_data.order_id;
                utag_data.marin_price = utag_data.order_grand_total;
            } else if (signupcheck == 0) {
                utag_data.marin_conversion_type = 'ScheduleAppt';
                utag_data.marin_order_id = '';
                utag_data.marin_price = '';
            } else {
                utag_data.marin_conversion_type = 'lgform';
                utag_data.marin_price = '';
                utag_data.marin_price = '';
            }
        }, function (a, b) {
            try {
                if (1) {
                    try {
                        b['hotjar_user_id'] = hj.pageVisit.property.get('userId').split("-").shift();
                    } catch (e) {}
                }
            } catch (e) {
                utag.DB(e)
            }
        }];
        utag.handler.cfg_extend = [{
            "alr": 0,
            "bwq": 0,
            "id": "122",
            "blr": 1,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "1",
            "blr": 0,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "62",
            "blr": 0,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "63",
            "blr": 0,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "210",
            "blr": 0,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "145",
            "blr": 0,
            "end": 0
        }, {
            "alr": 0,
            "bwq": 0,
            "id": "191",
            "blr": 1,
            "end": 0
        }, {
            "alr": 1,
            "bwq": 0,
            "id": "226",
            "blr": 0,
            "end": 0
        }];
        utag.loader.initcfg = function () {
            utag.loader.cfg = {
                "15": {
                    load: 1,
                    send: 1,
                    v: 201806081719,
                    wait: 1,
                    tid: 7110
                },
                "16": {
                    load: 1,
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 7110
                },
                "41": {
                    load: utag.cond[4],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 18016
                },
                "67": {
                    load: utag.cond[66],
                    send: 1,
                    v: 201809201519,
                    wait: 1,
                    tid: 20011
                },
                "131": {
                    load: utag.cond[66],
                    send: 1,
                    v: 201809201519,
                    wait: 1,
                    tid: 18016
                },
                "133": {
                    load: utag.cond[20],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 18016
                },
                "160": {
                    load: utag.cond[54],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 18016
                },
                "177": {
                    load: utag.cond[19],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20067
                },
                "181": {
                    load: utag.cond[25],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20011
                },
                "195": {
                    load: utag.cond[31],
                    send: 1,
                    v: 201807031501,
                    wait: 1,
                    tid: 13002
                },
                "196": {
                    load: utag.cond[32],
                    send: 1,
                    v: 201807031501,
                    wait: 1,
                    tid: 13002
                },
                "197": {
                    load: utag.cond[24],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 7117
                },
                "202": {
                    load: 1,
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 8009
                },
                "205": {
                    load: 1,
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20010
                },
                "215": {
                    load: utag.cond[39],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 7129
                },
                "218": {
                    load: utag.cond[66],
                    send: 1,
                    v: 201809201519,
                    wait: 1,
                    tid: 20011
                },
                "219": {
                    load: utag.cond[25],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20011
                },
                "220": {
                    load: utag.cond[44],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20067
                },
                "229": {
                    load: utag.cond[66],
                    send: 1,
                    v: 201809201519,
                    wait: 1,
                    tid: 6026
                },
                "241": {
                    load: utag.cond[43],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 6026
                },
                "230": {
                    load: utag.cond[25],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 6026
                },
                "242": {
                    load: utag.cond[25],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 20067
                },
                "251": {
                    load: utag.cond[25],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 7117
                },
                "256": {
                    load: utag.cond[52],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 18016
                },
                "259": {
                    load: utag.cond[53],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 18016
                },
                "262": {
                    load: utag.cond[24],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 7117
                },
                "263": {
                    load: utag.cond[20],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 20011
                },
                "264": {
                    load: utag.cond[56],
                    send: 1,
                    v: 201803122102,
                    wait: 1,
                    tid: 4001
                },
                "271": {
                    load: utag.cond[4],
                    send: 1,
                    v: 201803131540,
                    wait: 1,
                    tid: 20067
                },
                "272": {
                    load: utag.cond[57],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "274": {
                    load: utag.cond[60],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "276": {
                    load: utag.cond[61],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "273": {
                    load: utag.cond[59],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "275": {
                    load: utag.cond[58],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "277": {
                    load: utag.cond[62],
                    send: 1,
                    v: 201803081736,
                    wait: 1,
                    tid: 20067
                },
                "284": {
                    load: utag.cond[35],
                    send: 1,
                    v: 201805221445,
                    wait: 1,
                    tid: 20067
                }
            };
            utag.loader.cfgsort = ["15", "16", "41", "67", "131", "133", "160", "177", "181", "195", "196", "197", "202", "205", "215", "218", "219", "220", "229", "241", "230", "242", "251", "256", "259", "262", "263", "264", "271", "272", "274", "276", "273", "275", "277", "284"];
        }
        utag.loader.initcfg();
    }
    if (typeof utag_cfg_ovrd != 'undefined') {
        for (var i in utag.loader.GV(utag_cfg_ovrd)) utag.cfg[i] = utag_cfg_ovrd[i]
    };
    utag.loader.PINIT = function (a, b, c) {
        utag.DB("Pre-INIT");
        if (utag.cfg.noload) {
            return;
        }
        try {
            this.GET();
            if (utag.handler.RE('view', utag.data, "blr")) {
                utag.handler.LR();
            }
        } catch (e) {
            utag.DB(e)
        };
        a = this.cfg;
        c = 0;
        for (b in this.GV(a)) {
            if (a[b].block == 1 || (a[b].load > 0 && (typeof a[b].src != 'undefined' && a[b].src != ''))) {
                a[b].block = 1;
                c = 1;
                this.bq[b] = 1;
            }
        }
        if (c == 1) {
            for (b in this.GV(a)) {
                if (a[b].block) {
                    a[b].id = b;
                    if (a[b].load == 4) a[b].load = 1;
                    a[b].cb = function () {
                        var d = this.uid;
                        utag.loader.cfg[d].cbf = 1;
                        utag.loader.LOAD(d)
                    };
                    this.AS(a[b]);
                }
            }
        }
        if (c == 0) this.INIT();
    };
    utag.loader.INIT = function (a, b, c, d, e) {
        utag.DB('utag.loader.INIT');
        if (this.ol == 1) return -1;
        else this.ol = 1;
        utag.handler.RE('view', utag.data);
        utag.rpt.ts['i'] = new Date();
        d = this.cfgsort;
        for (a = 0; a < d.length; a++) {
            e = d[a];
            b = this.cfg[e];
            b.id = e;
            if (b.block != 1 && b.s2s != 1) {
                if (utag.loader.bk[b.id] || (utag.cfg.readywait && b.load == 4)) {
                    this.f[b.id] = 0;
                    utag.loader.LOAD(b.id)
                } else if (b.wait == 1 && utag.loader.rf == 0 && !(b.load == 4 && utag.cfg.noview)) {
                    utag.DB('utag.loader.INIT: waiting ' + b.id);
                    this.wq.push(b)
                    this.f[b.id] = 2;
                } else if (b.load > 0) {
                    utag.DB('utag.loader.INIT: loading ' + b.id);
                    this.lq.push(b);
                    this.AS(b);
                }
            }
        }
        if (this.wq.length > 0) utag.loader.EV('', 'ready', function (a) {
            if (utag.loader.rf == 0) {
                utag.DB('READY:utag.loader.wq');
                utag.loader.rf = 1;
                utag.loader.WQ();
            }
        });
        else if (this.lq.length > 0) utag.loader.rf = 1;
        else if (this.lq.length == 0) utag.loader.END();
        return 1
    };
    utag.loader.EV('', 'ready', function (a) {
        if (utag.loader.efr != 1) {
            utag.loader.efr = 1;
            try {
                function followLink(hrefLink, target) {
                    if (typeof target != 'undefined' && target) {
                        window.open(hrefLink, target);
                    } else {
                        window.location.href = hrefLink;
                    }
                }
                var internalURLs = ['terminixcommercial.com', 'terminix.com'
];
                jQuery('a').click(function (event) {
                    event.preventDefault();
                    var ref = jQuery(this).attr('href');
                    if (ref && ref != null && ref.indexOf("http") != -1) {
                        var internalLink = 0;
                        for (i = 0; i < internalURLs.length; i++) {
                            if (ref.indexOf(internalURLs[i]) !== -1) {
                                internalLink = 1;
                            }
                        }
                        if (internalLink === 0) {
                            utag.link({
                                ga_event_category: "External Links",
                                ga_event_action: "Click",
                                ga_event_label: ref
                            });
                        }
                    }
                    if (ref && ref != null) {
                        followLink(ref, jQuery(this).attr('target'));
                    }
                    return;
                });
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (typeof utag.runonce == 'undefined') utag.runonce = {};
                utag.jdh = function (h, i, j, k) {
                    h = utag.jdhc.length;
                    if (h == 0) window.clearInterval(utag.jdhi);
                    else {
                        for (i = 0; i < h; i++) {
                            j = utag.jdhc[i];
                            k = jQuery(j.i).is(":visible") ? 1 : 0;
                            if (k != j.s) {
                                if (j.e == (j.s = k)) jQuery(j.i).trigger(j.e ? "afterShow" : "afterHide")
                            }
                        }
                    }
                };
                utag.jdhi = window.setInterval(utag.jdh, 250);
                utag.jdhc = [];
                if (1) {
                    if (typeof utag.runonce[69] == 'undefined') {
                        utag.runonce[69] = 1;
                        jQuery(document.body).on('click', '#utilBar.navbar-right.navbar-nav.nav li a:eq(0)', function (e) {
                            utag.link({
                                "ga_event_category": 'Login',
                                "ga_event_action": 'Logged in',
                                "ga_event_label": 'Header'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[70] == 'undefined') {
                        utag.runonce[70] = 1;
                        jQuery(document.body).on('click', '#log-in-button.pointed-button.btn', function (e) {
                            utag.link({
                                "ga_event_category": 'Login',
                                "ga_event_action": 'Logged in',
                                "ga_event_label": utag.data['dom.url']
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[71] == 'undefined') {
                        utag.runonce[71] = 1;
                        jQuery(document.body).on('click', '#start-here-button.pointed-button.btn', function (e) {
                            utag.link({
                                "ga_event_category": 'Login',
                                "ga_event_action": 'Logged In',
                                "ga_event_label": utag.data['dom.url']
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                $('#state-list #go-button.pointed-button.btn').click(function (event) {
                    var zip = $('#find-a-location-home').val();
                    if (zip != null && zip.length > 0) {
                        zip = zip.substring(0, 3);
                    }
                    utag.link({
                        ga_event_category: 'Find a Location',
                        ga_event_action: 'Zip Code',
                        ga_event_label: zip
                    });
                    return;
                });
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[78] == 'undefined') {
                        utag.runonce[78] = 1;
                        jQuery(document.body).on('click', '#page-wrap.container div.container div.row div.col-sm-3 ul.states-holder li.state a', function (e) {
                            utag.link({
                                "ga_event_category": 'Find a Location',
                                "ga_event_action": 'Clicked State',
                                "ga_event_label": $(this).text()
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[79] == 'undefined') {
                        utag.runonce[79] = 1;
                        jQuery(document.body).on('click', 'form#contact-form #submit-button.pointed-button.btn', function (e) {
                            utag.link({
                                "ga_event_category": 'Customer Service',
                                "ga_event_action": 'Clicked Submit'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[80] == 'undefined') {
                        utag.runonce[80] = 1;
                        jQuery(document.body).on('click', 'a[href="tel:18778376464"]', function (e) {
                            utag.link({
                                "ga_event_category": 'Phone',
                                "ga_event_action": 'Tap to Call',
                                "ga_event_label": document.location.pathname
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[86] == 'undefined') {
                        utag.runonce[86] = 1;
                        utag.jdhc.push({
                            i: 'div#banner.error-page',
                            e: 1
                        });
                        jQuery(document.body).on('afterShow', 'div#banner.error-page', function (e) {
                            if (jQuery('div#banner.error-page').is(':visible')) {
                                utag.link({
                                    "ga_event_category": 'Error Pages',
                                    "ga_event_action": '404',
                                    "ga_event_label": window.location.pathname
                                })
                            }
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[81] == 'undefined') {
                        utag.runonce[81] = 1;
                        jQuery(document.body).on('click', 'a#request-a-call', function (e) {
                            utag.link({
                                "ga_event_category": 'Free Estimate',
                                "ga_event_action": 'Mobile Form Opened'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[82] == 'undefined') {
                        utag.runonce[82] = 1;
                        jQuery(document.body).on('click', 'button#page-1', function (e) {
                            setTimeout(function () {
                                if ($('#free-estimate-form .red').length === 0) {
                                    var label = "unknown selection";
                                    if ($('#free-estimate-form input[name=Brand]:checked').val() == 'TM') {
                                        label = 'residential';
                                    } else if ($('#free-estimate-form input[name=Brand]:checked').val() == 'TM-C') {
                                        label = 'commercial';
                                    }
                                    utag.link({
                                        ga_event_category: 'Free Estimate',
                                        ga_event_action: 'Clicked Get Started',
                                        ga_event_label: label
                                    });
                                }
                            }, 200);
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                window.procEvent = function procEvent(name, type, a, b) {
                    name = typeof name !== 'undefined' ? name : '';
                    type = typeof type !== 'undefined' ? type : '';
                    a = typeof a !== 'undefined' ? a : '';
                    b = typeof b !== 'undefined' ? b : '';
                    cleanUdo();
                    if (name === "") {
                        return false;
                    }
                    switch (name) {
                        case "Free Estimate Submit":
                            var serviceMarket = getCookie("service_market");
                            if (window.location.pathname == "/request-quote/") {
                                var serviceMarket = "Generic request quote";
                                console.log(serviceMarket);
                            } else if (typeof serviceMarket === 'undefined' || serviceMarket === '') {
                                serviceMarket = 'none'
                            }
                            if (window.location.pathname == "/customer-support/") {
                                var serviceMarket = "Contact Us";
                                console.log(serviceMarket);
                            } else if (typeof serviceMarket === 'undefined' || serviceMarket === '') {
                                serviceMarket = 'none'
                            }
                            utag.link({
                                ga_event_category: "Free Estimate",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: serviceMarket
                            });
                            triggerConversionTagsFromLeadGenSubmission();
                            break;
                        case "Free Estimate Trans Error":
                            utag.link({
                                ga_event_category: "Free Estimate",
                                ga_event_action: "Form Error",
                                ga_event_label: "Transmission Error"
                            });
                            break;
                        case "Free Estimate User Error":
                        case "Free Estimate Error":
                            var errors = "";
                            $(formID + " .red").each(function () {
                                if ($(this).is('input, a:not(.drop-text), button ')) {
                                    errors += $(this).attr('id') + ", ";
                                }
                            });
                            errors = errors.substr(0, errors.length - 2);
                            utag.link({
                                ga_event_category: "Free Estimate",
                                ga_event_action: "Form Error",
                                ga_event_label: "User Error: " + errors
                            });
                            break;
                        case "Free Estimate Cancel":
                            utag.link({
                                ga_event_category: "Free Estimate",
                                ga_event_action: "Clicked Cancel"
                            });
                            break;
                        case "ecommerce":
                            switch (type) {
                                case 'add to cart success':
                                    utag.link({
                                        product_ids: utag_data.product_ids,
                                        product_names: utag_data.product_names,
                                        product_categories: utag_data.product_categories,
                                        product_variants: utag_data.product_variants,
                                        product_prices: utag_data.product_prices,
                                        product_discounts: utag_data.product_discounts,
                                        ga_action_type: 'add'
                                    });
                                    break;
                                case 'add to cart error':
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type
                                    });
                                    break;
                                case 'address form error':
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type,
                                        ga_event_label: 'concern: ' + $('#productSelect').find('option:selected').text()
                                    });
                                    break;
                                case 'address form success':
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type,
                                        ga_event_label: 'concern: ' + $('#productSelect').find('option:selected').text() + ' / redirected to ' + a
                                    });
                                    break;
                                case 'submit billing info error':
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type,
                                        ga_event_label: utag_data.product_categories + ' / ' + utag_data.product_variants + ' / ' + utag_data.product_prices
                                    });
                                    break;
                                case 'submit billing info success':
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type,
                                        ga_event_label: utag_data.product_names + ' / ' + utag_data.product_categories + ' / ' + utag_data.product_variants,
                                        product_ids: utag_data.product_ids,
                                        product_names: utag_data.product_names,
                                        product_categories: utag_data.product_categories,
                                        product_variants: utag_data.product_variants,
                                        product_prices: utag_data.product_prices,
                                        product_discounts: utag_data.product_discounts,
                                        customer_purchase_step: '1',
                                        ga_action_option: utag_data.order_payment_method,
                                        ga_action_type: 'checkout'
                                    });
                                    break;
                                case 'orderConfirmation':
                                    utag_data.ga_action_type = "";
                                    break;
                                default:
                                    utag.link({
                                        ga_event_category: name,
                                        ga_event_action: type,
                                        ga_event_label: a
                                    });
                                    break;
                            }
                        default:
                            utag.link({
                                ga_event_category: name,
                                ga_event_action: type,
                                ga_event_label: a
                            });
                            break;
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[104] == 'undefined') {
                        utag.runonce[104] = 1;
                        jQuery(document.body).on('click', '#banner > div.banner-copy-holder.right.white.vcb-was-modified-insert > a[href="https://www.terminix.com/buyonline/address.jsp"]', function (e) {
                            utag.link({
                                "ga_event_category": 'ecommerce',
                                "ga_event_action": 'Click Get Started Button'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                function stringEndsWith(string, suffix) {
                    string = string.toLowerCase();
                    suffix = suffix.toLowerCase();
                    return string.indexOf(suffix, string.length - suffix.length) !== -1;
                }

                function checkStringContains(string, stringContains) {
                    string = string.toLowerCase();
                    stringContains = stringContains.toLowerCase();
                    return string.indexOf(stringContains) !== -1;
                }
                var pathName = location.pathname;
                if (stringEndsWith(pathName, 'html') || stringEndsWith(pathName, 'htm')) {
                    pathName = pathName.substring(0, pathName.lastIndexOf("/"));
                }
                if (stringEndsWith(pathName, '/')) {
                    pathName = pathName.substring(0, pathName.length - 1);
                }
                if (stringEndsWith(location.pathname, 'free-inspection/step-three') === true || stringEndsWith(location.pathname, '/buyonline/leadgenconfirm.jsp') === true) {
                    triggerConversionTagsFromLeadGenSubmission();
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                var requestId = getUrlVars()['_requestid'];
                if (requestId !== undefined && checkStringContains(location.href, "thank-you") === true) {
                    var cookieValue = getCookie('_requestid');
                    if (cookieValue === "" || requestId !== cookieValue) {
                        setCookie('_requestid', requestId, 1);
                        var serviceMarket = getCookie("service_market");
                        utag.link({
                            ga_event_category: "Free Estimate",
                            ga_event_action: "Form Submit Success",
                            ga_event_label: serviceMarket
                        });
                        triggerConversionTagsFromLeadGenSubmission();
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[130] == 'undefined') {
                        utag.runonce[130] = 1;
                        jQuery(document.body).on('click', '#free-estimate-form input[type="radio"][name="Brand"]', function (e) {
                            var serviceMarket = getServiceMarketValue("#free-estimate-form");
                            setCookie('service_market', serviceMarket, 1);
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                var requestId = getUrlVars()['_requestid'];
                if (requestId !== undefined && checkStringContains(location.href, "try-again") === true) {
                    var cookieValue = getCookie('_requestid');
                    if (cookieValue === "" || requestId !== cookieValue) {
                        setCookie('_requestid', requestId, 1);
                        procEvent("Free Estimate Trans Error");
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (checkStringContains(location.pathname, "buyonline/address.jsp") === true) {
                    var checkForThankyou = setInterval(function () {
                        if ($('#thank-you-modal').is(':visible')) {
                            utag.link({
                                ga_event_category: 'Free Estimate',
                                ga_event_action: 'Form Submit Success',
                                ga_event_label: 'residential'
                            });
                            triggerConversionTagsFromLeadGenSubmission();
                            clearInterval(checkForThankyou);
                        }
                    }, 200);
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                (function () {
                    if (location.pathname.match(/\/my-account/)) {
                        $('div#page-content').on('click', '.steps .step-login .register-text a', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Register Now Clicked',
                                ga_event_label: 'Register Now Clicked on Login Page'
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', 'div.step-header-subtext a', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Please Re-Register Clicked',
                                ga_event_label: 'Please Re-Register Clicked on Login Page'
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', 'a[href="/my-account/pages/forgotPassword.jsp"]', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Reset Password',
                                ga_event_label: 'Forgot Password Clicked on Login'
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/register\.jsp/)) {
                        $('div#page-content').on('click', '.steps .step-header .step-header-collapse', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Registration Step Heading Clicked',
                                ga_event_label: $(this).siblings('.step-header-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .lookup-type-switcher label', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Lookup Type Selected',
                                ga_event_label: $(this).find('.label-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .form-radio-accordion label', function () {
                            var lookupType = $('.steps .step-lookup .lookup-type-switcher :checked + .label-text').text().trim();
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: lookupType + ' Lookup Field Selected',
                                ga_event_label: $(this).find('.label-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .form-buttons .btn:not(.btn-inactive)', function () {
                            var lookupType = $('.steps .step-lookup .lookup-type-switcher :checked + .label-text').text().trim();
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Customer Lookup Next Step Clicked',
                                ga_event_label: lookupType + ' Look Up'
                            };
                            var addressLookup = false;
                            var lookupOption = $('.steps .step-lookup .form-radio-accordion :checked + .label-text').text().trim();
                            link_data.ga_event_label += ' By ' + lookupOption;
                            if (lookupType == 'Residential') {
                                addressLookup = lookupOption == 'Property Address';
                            } else {
                                addressLookup = lookupOption == 'Billing Address';
                            }
                            utag.link(link_data);
                            var interval = setInterval(function () {
                                if (!$('.steps .step-lookup .step-contents:visible').length) {
                                    clearInterval(interval);
                                    return;
                                }
                                var $errors = $('.steps .step-lookup .step-contents .errors .error-box');
                                if ($errors.length) {
                                    clearInterval(interval);
                                    var errorMessage = $errors.text();
                                    var link_data = {
                                        ga_event_category: 'My Account Registration',
                                        ga_event_action: 'Registration Error: Lookup',
                                        ga_event_label: errorMessage
                                    };
                                    utag.link(link_data);
                                    if (addressLookup) {
                                        link_data = {
                                            ga_event_category: 'My Account Registration',
                                            ga_event_action: 'Registration Error: Address lookup',
                                            ga_event_label: errorMessage
                                        };
                                        utag.link(link_data);
                                    }
                                }
                            }, 100);
                        });
                        $('div#page-content').on('click', '.steps .step-properties .form-buttons .btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Confirm Properties Clicked',
                                ga_event_label: 'Confirm Properties Clicked',
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-personal .form-buttons .btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Confirm Personal Information Clicked',
                                ga_event_label: 'Confirm Information Clicked',
                            };
                            utag.link(link_data);
                        });
                        var TIMER_MESSAGES_STEP_ACCOUNT = null;
                        $('div#page-content').on('click', '.steps + .form-buttons .btn:not(.cancel, .btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Setup My Account Clicked',
                                ga_event_label: 'Setup My Account Clicked',
                            };
                            utag.link(link_data);
                            clearInterval(TIMER_MESSAGES_STEP_ACCOUNT);
                            TIMER_MESSAGES_STEP_ACCOUNT = setInterval(function () {
                                var $messages = $('.steps .step-account .step-contents .informationals .informational-box');
                                if ($messages.length) {
                                    var messageText = $messages.text();
                                    if (messageText.indexOf("Logging you in") >= 0) {
                                        var link_data = {
                                            ga_event_category: 'My Account Registration',
                                            ga_event_action: 'Setup My Account Clicked',
                                            ga_event_label: 'User Found Logged In'
                                        };
                                        utag.link(link_data);
                                    }
                                    clearInterval(TIMER_MESSAGES_STEP_ACCOUNT);
                                }
                            }, 100);
                        });
                        $('div#page-content').on('click', '.steps + .form-buttons .btn.cancel', function () {
                            var link_data = {
                                ga_event_category: 'My Account Registration',
                                ga_event_action: 'Cancel Clicked',
                                ga_event_label: 'Cancel Clicked',
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/forgotPassword\.jsp/)) {
                        $('div#page-content').on('click', '.form-buttons .btn', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Reset Password',
                                ga_event_label: 'Submit Clicked to Send Email'
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/forgotPasswordConfirmation\.jsp/)) {
                        $('div#page-content').on('click', '.form-buttons .btn', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Reset Password',
                                ga_event_label: 'Return to Login Clicked after Email Sent'
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/resetPassword\.jsp/)) {
                        $('div#page-content').on('click', '.form-buttons .btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Reset Password',
                                ga_event_label: 'Change Password Clicked'
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/home\.jsp/) || location.pathname.match(/\/my-account/) && $('#myaccount-header').length > 0) {
                        var page_with_hash = function () {
                            return window.location.pathname + window.location.search + window.location.hash;
                        };
                        $(window).on('hashchange', function () {
                            utag.view({
                                ga_page_including_hash: page_with_hash()
                            });
                        })
                        $('#page-content').on('click', '#myaccount-nav > ul > li > a', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Header Icon Clicked',
                                ga_event_label: $(this).text().trim(),
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#myaccount-header .add-account a', function () {
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Add a Property Clicked',
                                ga_event_label: 'Add a Property on My Account Pages Clicked',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-contact-info-large .form-buttons .btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Update Profile Info',
                                ga_event_label: 'Save Changes Clicked to update Contact Profile',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-contact-info-small a:not(.btn):eq(0)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Update Profile Info',
                                ga_event_label: 'Edit Contact Info Clicked in Sidebar',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-contact-info-small .form-buttons .btn', function () {
                            if ($(this).closest('.qas-validator').length) {
                                return;
                            }
                            if ($(this).text().trim() == "Cancel") {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Update Profile Info',
                                    ga_event_label: 'Cancel to remove contact info changes Clicked in Sidebar',
                                    ga_page_including_hash: page_with_hash()
                                };
                                utag.link(link_data);
                            } else {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Update Profile Info',
                                    ga_event_label: 'Save contact info button Clicked in Sidebar',
                                    ga_page_including_hash: page_with_hash()
                                };
                                utag.link(link_data);
                            }
                        });
                        $('#page-content').on('click', '#tab-contents .module-preferences .form-buttons .btn:not(.btn-inactive)', function () {
                            var vm, newModel, oldModel;
                            if (typeof propertiesList != 'undefined') {
                                vm = propertiesList.currentProperty().preferences();
                                newModel = vm.toJSON();
                                oldModel = ko.mapping.toJS(vm.originalModel());
                            } else {
                                vm = $('.module.module-preferences').data('vm');
                                newModel = ko.mapping.toJS(vm.model);
                                oldModel = ko.mapping.toJS(vm.originalModel);
                            }
                            var updatingPreferences = false;
                            var updatingContactInfo = false;
                            if (newModel.serviceNotificationPreferenceEmail != oldModel.serviceNotificationPreferenceEmail || newModel.serviceNotificationPreferenceCall != oldModel.serviceNotificationPreferenceCall || newModel.serviceNotificationPreferenceText != oldModel.serviceNotificationPreferenceText) {
                                updatingPreferences = true;
                            }
                            if (newModel.serviceNotificationEmail != oldModel.serviceNotificationEmail || ('' + newModel.serviceNotificationPhoneNumber).replace(/\D/g, '') != ('' + oldModel.serviceNotificationPhoneNumber).replace(/\D/g, '')) {
                                updatingContactInfo = true;
                            }
                            $('#alerts .alert').each(function () {
                                if ($(this).data('module') == 'preferences') {
                                    $(this).addClass('ignore-submit-checks');
                                }
                            });
                            var interval = setInterval(function () {
                                if (vm.submitting()) return;
                                clearInterval(interval);
                                var $alerts = $('#alerts .alert:not(.ignore-submit-checks)').filter(function () {
                                    return $(this).data('module') == 'preferences';
                                });
                                if ($('.module.module-preferences .error').filter('input, select, textarea').length) {} else if ($alerts.filter('.red').length) {
                                    if (updatingPreferences || updatingContactInfo) {
                                        utag.link({
                                            ga_event_category: 'My Account',
                                            ga_event_action: 'Notifications Preferences',
                                            ga_event_label: 'Notifications preference error occurred',
                                            ga_page_including_hash: page_with_hash()
                                        });
                                    }
                                } else {
                                    if (updatingPreferences) {
                                        utag.link({
                                            ga_event_category: 'My Account',
                                            ga_event_action: 'Notifications Preferences',
                                            ga_event_label: 'Modified notification preferences',
                                            ga_page_including_hash: page_with_hash()
                                        });
                                    }
                                    if (updatingContactInfo) {
                                        utag.link({
                                            ga_event_category: 'My Account',
                                            ga_event_action: 'Notifications Preferences',
                                            ga_event_label: 'Notification preferences contact updated',
                                            ga_page_including_hash: page_with_hash()
                                        });
                                    }
                                }
                            }, 100);
                        });
                        $('#page-content').on('click', '#tab-contents .module-change-password .form-buttons .btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Reset Password',
                                ga_event_label: 'Reset Password Save Changes Clicked while logged in',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-due-payments .pay-total-balance a', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Pay Now - Past Due',
                                ga_event_label: 'Pay Total Balance Clicked',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-due-payments table input', function () {
                            var $row = $(this).closest('tr');
                            if ($row.hasClass('mobile-amount')) {
                                $row = $row.prev();
                            }
                            if (!$row.is('.summary.renewal')) {
                                return;
                            }
                            var link_data = {
                                ga_event_category: 'My Account Termite Renewal',
                                ga_event_action: 'Payment Due',
                                ga_event_label: 'Payment Amount Clicked',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-due-payments .payment-summary .payment-method .radio-option', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Pay Now - Past Due',
                                ga_event_label: 'Enter Payment Info Clicked',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('change', '#tab-contents .module-due-payments .payment-method .form-select select', function () {
                            var selectedOptionIndex = $(this).find('option:selected').index();
                            if (selectedOptionIndex > 0) {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Pay Now - Past Due',
                                    ga_event_label: 'Enter Payment Info Clicked',
                                    ga_page_including_hash: page_with_hash()
                                };
                                utag.link(link_data);
                                var hasRenewal = $('.module-due-payments table:visible').find('tr.summary.renewal').filter(function () {
                                    var $row = $(this);
                                    var $input = $row.find('input');
                                    var $balance = $row.find('.col-balance');
                                    if (!$input.length && $row.next().hasClass('.mobile-amount')) {
                                        $input = $row.next().find('input');
                                    }
                                    var $spans = $balance.children('span');
                                    var amountDue = $spans.last().text();
                                    var amountEntered = parseFloat($input.val()).toFixed(2);
                                    if (amountDue == amountEntered) {
                                        return true;
                                    }
                                }).length > 0;
                                if (hasRenewal) {
                                    utag.link({
                                        ga_event_category: 'My Account Termite Renewal',
                                        ga_event_action: 'Payment Due',
                                        ga_event_label: 'Payment Method Selected',
                                        ga_page_including_hash: page_with_hash()
                                    });
                                }
                            }
                        });
                        $('#page-content').on('click', '#tab-contents .module-due-payments .payment-method-information .form-buttons .btn:not(.btn-inactive)', function () {
                            if ($(this).text().trim() == 'Cancel') {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Pay Now - Past Due',
                                    ga_event_label: 'Cancel Clicked',
                                    ga_page_including_hash: page_with_hash()
                                };
                                utag.link(link_data);
                            } else {
                                var $paymentMethodInformation = $(this).closest('.payment-method-saved, .payment-method-bank, .payment-method-card');
                                if ($paymentMethodInformation.find('.form-checkbox.form-checkbox-help input:checkbox').prop('checked')) {
                                    var link_data = {
                                        ga_event_category: "My Account",
                                        ga_event_action: "Pay Now - Past Due",
                                        ga_event_label: "Add Sales Agreement to EasyPay Checked",
                                        ga_page_including_hash: page_with_hash()
                                    };
                                    utag.link(link_data);
                                    var link_data = {
                                        ga_event_category: 'My Account',
                                        ga_event_action: 'Easy Pay',
                                        ga_event_label: 'Clicked to enroll in EasyPay in payment flow',
                                        ga_page_including_hash: page_with_hash()
                                    };
                                    utag.link(link_data);
                                } else if ($paymentMethodInformation.find('.form-checkbox input:checkbox').prop('checked')) {
                                    var link_data = {
                                        ga_event_category: "My Account",
                                        ga_event_action: "Pay Now - Past Due",
                                        ga_event_label: "Save Payment Method Selected",
                                        ga_page_including_hash: page_with_hash()
                                    };
                                    utag.link(link_data);
                                }
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Pay Now - Past Due',
                                    ga_event_label: 'Pay Now to Submit Payment Clicked',
                                    ga_page_including_hash: page_with_hash()
                                };
                                utag.link(link_data);
                                var hasRenewal = $('.module-due-payments table:visible').find('tr.summary.renewal').filter(function () {
                                    var $row = $(this);
                                    var $input = $row.find('input');
                                    var $balance = $row.find('.col-balance');
                                    if (!$input.length && $row.next().hasClass('.mobile-amount')) {
                                        $input = $row.next().find('input');
                                    }
                                    var $spans = $balance.children('span');
                                    var amountDue = $spans.last().text();
                                    var amountEntered = parseFloat($input.val()).toFixed(2);
                                    if (amountDue == amountEntered) {
                                        return true;
                                    }
                                }).length > 0;
                                if (hasRenewal) {
                                    utag.link({
                                        ga_event_category: 'My Account Termite Renewal',
                                        ga_event_action: 'Payment Due',
                                        ga_event_label: 'Pay Now Clicked',
                                        ga_page_including_hash: page_with_hash()
                                    });
                                }
                            }
                        });
                        $('#page-content').on('click', '#tab-contents .module-stored-payments .form-radio.form-radio-large', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Payment Methods',
                                ga_event_label: 'Add a Payment Method: ' + $(this).text().trim(),
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-stored-payments .add-payment-method-buttons .btn', function () {
                            var type = $(this).text().toLowerCase().indexOf("bank") >= 0 ? "Bank Account" : "Debit/Credit";
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Payment Methods',
                                ga_event_label: 'Add a Payment Method: ' + type,
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-stored-payments .payment-method-form .form-buttons a.btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Payment Methods',
                                ga_event_label: 'Add a Payment Method: ' + $(this).text().trim(),
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-stored-payments .stored-payment-methods tr.summary td a', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Payment Methods',
                                ga_event_label: 'Edit Payment Method: ' + $(this).text().trim(),
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-stored-payments .stored-payment-methods .payment-method-form-edit a.btn:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Payment Methods',
                                ga_event_label: 'Edit Payment Method: ' + $(this).text().trim(),
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-service-history .detail .download-document.ga-has-documents a', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Service Documents',
                                ga_event_label: 'Download service document',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-service-history .summary .collapse', function () {
                            var $detail = $(this).closest('.summary').next('.detail');
                            if (!$detail.is(':visible'))
                                return;
                            if ($detail.find('.download-document.ga-has-documents, .download-document.ga-no-documents').length)
                                return;
                            if ($detail.hasClass('ga-loaded-documents-already'))
                                return;
                            CheckForDocumentsToLoad($detail);
                        });
                        $('#page-content').on('click', '#tab-contents .module-service-history .detail .download-document.ga-error a.retry', function () {
                            CheckForDocumentsToLoad($(this).closest('.detail'));
                        });

                        function CheckForDocumentsToLoad($detail) {
                            $detail.addClass('ga-loaded-documents-already');
                            var interval = setInterval(function () {
                                var $document = $detail.find('.download-document');
                                if ($document.hasClass('ga-loading'))
                                    return;
                                if ($document.hasClass('ga-error')) {
                                    var link_data = {
                                        ga_event_category: 'My Account',
                                        ga_event_action: 'Service Documents',
                                        ga_event_label: 'Error finding documents',
                                        ga_page_including_hash: page_with_hash()
                                    };
                                    utag.link(link_data);
                                } else if ($document.hasClass('ga-no-documents')) {
                                    var link_data = {
                                        ga_event_category: 'My Account',
                                        ga_event_action: 'Service Documents',
                                        ga_event_label: 'No document found response',
                                        ga_page_including_hash: page_with_hash()
                                    };
                                    utag.link(link_data);
                                }
                                clearInterval(interval);
                            }, 100);
                        }
                        $('#page-content').on('click', '#tab-contents .module-moving a[href^="/buyonline"]', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Schedule your free inspection" to go to termite sales funnel',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-moving .btn-white-bordered', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Pay Balance" on the dashboard module',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '#tab-contents .module-moving .btn-orange:not(.btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Search" on the dashboard module',
                                ga_page_including_hash: page_with_hash()
                            };
                            utag.link(link_data);
                        });
                    }
                    if (location.pathname.match(/\/my-account\/pages\/addProperty\.jsp/)) {
                        $('div#page-content').on('click', '.steps .step-header .step-header-collapse', function () {
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Add Property Step Heading Clicked',
                                ga_event_label: $(this).siblings('.step-header-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .lookup-type-switcher label', function () {
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Lookup Type Selected',
                                ga_event_label: $(this).find('.label-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .form-radio-accordion label', function () {
                            var lookupType = $('.steps .step-lookup .lookup-type-switcher :checked + .label-text').text().trim();
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: lookupType + ' Lookup Field Selected',
                                ga_event_label: $(this).find('.label-text').text().trim()
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps .step-lookup .form-buttons .btn:not(.btn-inactive)', function () {
                            var lookupType = $('.steps .step-lookup .lookup-type-switcher :checked + .label-text').text().trim();
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Customer Lookup Next Step Clicked',
                                ga_event_label: lookupType + ' Look Up'
                            };
                            var addressLookup = false;
                            if (lookupType == 'Residential') {
                                var lookupResOption = $('.steps .step-lookup .form-radio-accordion :checked + .label-text').text().trim();
                                link_data.ga_event_label += ' By ' + lookupResOption;
                                addressLookup = lookupResOption == 'Property Address';
                            } else {
                                var lookupComOption = $('.steps .step-lookup .form-radio-accordion :checked + .label-text').text().trim();
                                link_data.ga_event_label += ' By ' + lookupComOption;
                                addressLookup = lookupComOption == 'Billing Address';
                            }
                            utag.link(link_data);
                            if (addressLookup) {
                                var interval = setInterval(function () {
                                    if (!$('.steps .step-lookup .step-contents:visible').length) {
                                        clearInterval(interval);
                                        return;
                                    }
                                    var $errors = $('.steps .step-lookup .step-contents .errors .error-box');
                                    if ($errors.length) {
                                        clearInterval(interval);
                                        var link_data = {
                                            ga_event_category: 'My Account Add Property',
                                            ga_event_action: 'Add Property Error: Address lookup',
                                            ga_event_label: $errors.text()
                                        };
                                        utag.link(link_data);
                                    }
                                }, 100);
                            }
                        });
                        $('div#page-content').on('click', '.steps + .form-buttons .btn:not(.cancel, .btn-inactive)', function () {
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Add Property Clicked',
                                ga_event_label: 'Final Add Property Clicked to submit',
                            };
                            utag.link(link_data);
                        });
                        $('div#page-content').on('click', '.steps + .form-buttons .btn.cancel', function () {
                            var link_data = {
                                ga_event_category: 'My Account Add Property',
                                ga_event_action: 'Cancel Clicked',
                                ga_event_label: 'Cancel Clicked',
                            };
                            utag.link(link_data);
                        });
                    }
                    if (/^\/my-account/.test(window.location.pathname) && $('#site-header .welcome').length > 0) {
                        var errorMessageToFind = "We are experiencing a technical issue. Check later to ensure your payment went through.";
                        var arbitraryClassNameToAdd = "ga-tracked-for-vague-error-message";
                        var secondsToWaitBeforeCheckingAgain = 1;

                        function CheckForVagueErrorMessage() {
                            if (window.location.hash == '#payments') {
                                var $errors = $('#tab-contents .tab-payments .module-due-payments .errors .error-box:not(.' + arbitraryClassNameToAdd + ')');
                                if ($errors.length) {
                                    var foundVagueErrorMessage = false;
                                    $errors.each(function () {
                                        var errorMessage = $(this).addClass(arbitraryClassNameToAdd).text();
                                        if (errorMessage == errorMessageToFind) {
                                            foundVagueErrorMessage = true;
                                        }
                                    });
                                    if (foundVagueErrorMessage) {
                                        var link_data = {
                                            ga_event_category: 'My Account',
                                            ga_event_action: 'Pay Now - Past Due',
                                            ga_event_label: 'Error to check again later',
                                            ga_page_including_hash: window.location.pathname + window.location.search + window.location.hash
                                        };
                                        utag.link(link_data);
                                    }
                                }
                            }
                            setTimeout(CheckForVagueErrorMessage, secondsToWaitBeforeCheckingAgain * 1000);
                        }
                        CheckForVagueErrorMessage();
                    }
                    if (location.pathname.match(/\/my-account\/pages\/moving\.jsp/)) {
                        $('#page-content').on('click', 'a[href^="/buyonline"]', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Schedule your free inspection" to go to termite sales funnel'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-address .form-buttons > a.btn-orange', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Save Transfer Information" on New Address'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-schedule .form-buttons > a.btn-orange', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Submit" on Schedule Appointment'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-my-services .form-buttons > a.btn-white-bordered', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Keep Existing Service" to keep existing appointment at current residence before move date'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-my-services .form-buttons > a.btn-orange', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "Reschedule at New Address" to reschedule existing appointment to new house after move date'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-schedule-pest .form-buttons > a.btn-orange', function () {
                            if ($('.step-schedule-pest .table-calendar').length) {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Clicked "Submit" on Best Fit for Pest page'
                                };
                                utag.link(link_data);
                            } else {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Clicked "Submit" on Pest CFR screen'
                                };
                                utag.link(link_data);
                            }
                        });
                        $('#page-content').on('click', '.step-schedule-pest .form-buttons > a.btn-orange', function () {
                            if ($('.step-schedule-pest .table-calendar').length) {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Clicked "Submit" on Best Fit for Pest page'
                                };
                                utag.link(link_data);
                            }
                        });
                        $('#page-content').on('click', '.step-schedule-pest .months-wrapper + .form-buttons > a.btn-white-bordered', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "None of these dates works for me" on Best Fit for Pest page'
                            };
                            utag.link(link_data);
                        });
                        $('#page-content').on('click', '.step-schedule-termite .form-buttons > a.btn-orange', function () {
                            if ($('.step-schedule-termite [name=optOutFreeTermiteInspection]:checked').length) {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Checked box to indicate I do not want a FREE Termite Inspection'
                                };
                                utag.link(link_data);
                            } else if ($('.step-schedule-pest .table-calendar').length) {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Clicked "Submit" on Best Fit for Termite page'
                                };
                                utag.link(link_data);
                            } else {
                                var link_data = {
                                    ga_event_category: 'My Account',
                                    ga_event_action: 'Moving Soon',
                                    ga_event_label: 'Clicked "Submit" on Termite CFR screen'
                                };
                                utag.link(link_data);
                            }
                        });
                        $('#page-content').on('click', '.step-schedule-termite .months-wrapper + .form-buttons > a.btn-white-bordered', function () {
                            var link_data = {
                                ga_event_category: 'My Account',
                                ga_event_action: 'Moving Soon',
                                ga_event_label: 'Clicked "None of these dates works for me" on Best Fit for Termite page'
                            };
                            utag.link(link_data);
                        });
                    }
                })();
            } catch (e) {
                utag.DB(e)
            };
            try {
                (function () {
                    if (window.location.pathname.match(/^\/my-account(\/.*)?$/i)) {
                        var leadingWhitespace = (function (d) {
                            d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
                            return d.firstChild.nodeType === 3;
                        })(document.createElement('div'));
                        if (!leadingWhitespace) {
                            window.location.assign("http://www.terminix.com/browser-upgrade-notification");
                        }
                    }
                })();
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[154] == 'undefined') {
                        utag.runonce[154] = 1;
                        jQuery(document.body).on('click', 'a.scanmaster-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Commercial: My Services',
                                "ga_event_label": '"Access ScanMaster" Link clicked on dashboard'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[155] == 'undefined') {
                        utag.runonce[155] = 1;
                        jQuery(document.body).on('click', 'p.download-ticket a', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Service History',
                                "ga_event_label": 'Download Full Ticket clicked'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[157] == 'undefined') {
                        utag.runonce[157] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services a.collapse', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked to expand a scheduled service'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[158] == 'undefined') {
                        utag.runonce[158] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services table tr.summary .btn.reschedule', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Reschedule Service"'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[159] == 'undefined') {
                        utag.runonce[159] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .detail-view .form-buttons .btn:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Add Note" for technician'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[160] == 'undefined') {
                        utag.runonce[160] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .schedule-appointment a:contains("None of these dates work")', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "None of these dates work for me" on reschedule'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[161] == 'undefined') {
                        utag.runonce[161] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .schedule-appointment .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Submit" to submit rescheduled appointment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[162] == 'undefined') {
                        utag.runonce[162] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .schedule-appointment .form-buttons .btn.btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Cancel" to keep scheduled appointment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[163] == 'undefined') {
                        utag.runonce[163] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .schedule-contact .form-buttons .btn.btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Cancel" on preferred reschedule form'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[164] == 'undefined') {
                        utag.runonce[164] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services .schedule-contact .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked "Confirm" to request rescheduled appointment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[165] == 'undefined') {
                        utag.runonce[165] = 1;
                        jQuery(document.body).on('click', '.module-my-services tr.summary .reschedule-button .btn', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Schedule Follow Up"'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[166] == 'undefined') {
                        utag.runonce[166] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-appointment a:contains("None of these dates work")', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "None of these dates work for me" on follow up'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[167] == 'undefined') {
                        utag.runonce[167] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-appointment .form-buttons .btn.btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Cancel" for no follow up'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[168] == 'undefined') {
                        utag.runonce[168] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-appointment .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Confirm" to select follow up appointment '
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[169] == 'undefined') {
                        utag.runonce[169] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-contact .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Submit" to select preferred follow up'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[170] == 'undefined') {
                        utag.runonce[170] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-contact .form-buttons .btn.btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Cancel" on preferred follow up'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[171] == 'undefined') {
                        utag.runonce[171] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-details .form-buttons .btn.btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Cancel" on follow up service details'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[172] == 'undefined') {
                        utag.runonce[172] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-details .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "Confirm" on follow up service details'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[173] == 'undefined') {
                        utag.runonce[173] = 1;
                        jQuery(document.body).on('click', '.module-my-services .schedule-details .selected-date-display a.edit', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'My Services',
                                "ga_event_label": 'Clicked "edit" to change scheduled follow up'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[175] == 'undefined') {
                        utag.runonce[175] = 1;
                        jQuery(document.body).on('click', 'a:contains("REFERRAL PROGRAM")', function (e) {
                            utag.link({
                                "ga_event_category": 'Footer Actions',
                                "ga_event_action": 'Link Clicks',
                                "ga_event_label": 'Referral Program'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.url'].toString().indexOf('my-account') > -1) {
                    if (typeof utag.runonce[176] == 'undefined') {
                        utag.runonce[176] = 1;
                        jQuery(document.body).on('click', 'a:contains("Refer A Friend")', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Clicks',
                                "ga_event_label": 'Refer A Friend link'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                var thisPath = window.location.pathname;

                function setFunnelStartCookie(cname, cvalue) {
                    document.cookie = cname + "=" + cvalue + ";path=/";
                }

                function getfstartCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }

                function checkfstartCookieInspections() {
                    var fstartcookie = getfstartCookie("fstart");
                    switch (fstartcookie) {
                        case "/bed-bug-control/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Bed Bugs"
                            });
                            break;
                        case "/additional-pest-solutions/attic-insulation/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Attic Insulation"
                            });
                            break;
                        case "/additional-pest-solutions/crawl-space-services/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Crawl Space"
                            });
                            break;
                        case "/termite-control/treatment/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Termite"
                            });
                            break;
                        case "/termite-control/protection/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Termite"
                            });
                            break;
                        case "/pest-control/wildlife/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Wildlife"
                            });
                            break;
                        case "/pest-control/rodents/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Wildlife"
                            });
                            break;
                        case "/termite-control/":
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Termite"
                            });
                            break;
                        default:
                            utag.link({
                                ga_event_category: "Inspection Schedule",
                                ga_event_action: "Form Submit Success",
                                ga_event_label: "Termite"
                            });
                    }
                }

                function checkfstartCookiePests() {
                    var fstartcookie = getfstartCookie("fstart");
                    switch (fstartcookie) {
                        case "/pest-control/ants/":
                            utag.link({
                                PurchaseTypePCPM: "PC Ants"
                            });
                            break;
                        case "/pest-control/cockroaches/":
                            utag.link({
                                PurchaseTypePCPM: "PC Cockroaches"
                            });
                            break;
                        case "/pest-control/spiders/":
                            utag.link({
                                PurchaseTypePCPM: "PC Spiders"
                            });
                            break;
                        case "/pest-control/":
                            utag.link({
                                PurchaseTypePCPM: "PC Homepage"
                            });
                            break;
                        case "/pest-control/mosquitoes/":
                            utag.link({
                                PurchaseTypePCPM: "PM Mosquito"
                            });
                            break;
                        case "/":
                            utag.link({
                                PurchaseTypePCPM: "PC TMX.com Homepage"
                            });
                            break;
                        default:
                            utag.link({
                                PurchaseTypePCPM: "PC Default"
                            });
                            break;
                    }
                }

                function setInspTypeDimension() {
                    var fstartcookie = getfstartCookie("fstart");
                    switch (fstartcookie) {
                        case "/bed-bug-control/":
                            utag.link({
                                inspection_type: "Bed Bugs",
                            });
                            break;
                        case "/additional-pest-solutions/attic-insulation/":
                            utag.link({
                                inspection_type: "Attic Insulation"
                            });
                            break;
                        case "/additional-pest-solutions/crawl-space-services/":
                            utag.link({
                                inspection_type: "Crawl Space"
                            });
                            break;
                        case "/termite-control/treatment/":
                            utag.link({
                                inspection_type: "Termite Control Treatment"
                            });
                            break;
                        case "/termite-control/protection/":
                            utag.link({
                                inspection_type: "Termite Control Protection"
                            });
                            break;
                        case "/pest-control/wildlife/":
                            utag.link({
                                inspection_type: "Wildlife"
                            });
                            break;
                        case "/pest-control/rodents/":
                            utag.link({
                                inspection_type: "Wildlife"
                            });
                            break;
                        case "/termite-control/":
                            utag.link({
                                inspection_type: "Termite Control Generic"
                            });
                            break;
                        default:
                            utag.link({
                                inspection_type: "Termite Control Generic"
                            });
                    }
                }
                jQuery.fn.exists = function () {
                    return jQuery(this).length > 0;
                }
                termInspBtnDeskMenu = $('body > header.header-container.desktop > div.header-right-container > div.bottom-bar-container > div > ul > li.termite-control > div > div > div > a > span');
                if (termInspBtnDeskMenu.exists() && (termInspBtnDeskMenu.text() == "Free Inspection")) {
                    termInspBtnDeskMenu.click(function () {
                        setFunnelStartCookie("fstart", "/termite-control/")
                    });
                }
                if ($("a[href='/free-inspection/'][class^='tmx\-secondary\-txt']").exists()) {
                    $("a[href='/free-inspection/'][class^='tmx\-secondary\-txt']").click(function () {
                        setFunnelStartCookie("fstart", "/termite-control/")
                    });
                }
                $(".btn.pull-right").click(function () {
                    setFunnelStartCookie("fstart", thisPath)
                });
                $(".btn.btn-cta.rm-text-decor").click(function () {
                    setFunnelStartCookie("fstart", thisPath)
                });
                if (/buyonline/.test(window.location.pathname) != true && /pest\-control/.test(window.location.pathname) != true) {
                    $("a[href='/buyonline/step-one/']").click(function () {
                        var fstartcookie = getfstartCookie("fstart");
                        if (fstartcookie.length > 0) {} else {
                            setFunnelStartCookie("fstart", thisPath)
                        }
                    });
                }
                if (/buyonline\/step\-one/.test(window.location.pathname) == true) {
                    $('#document').ready(function () {
                        checkfstartCookiePests();
                    });
                }
                var pathcheckInspections = thisPath.indexOf("/free-inspection/step-three/");
                if (pathcheckInspections == 0) {
                    checkfstartCookieInspections();
                }
                if (/\/free-inspection$|\/free-inspection\/$|\/free-inspection\?/.test(window.location.pathname) == true) {
                    setInspTypeDimension();
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[188] == 'undefined') {
                        utag.runonce[188] = 1;
                        jQuery(document.body).on('click', '.module-easy-pay .sales-agreements-table tr.summary a', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Easy Pay',
                                "ga_event_label": 'Clicked ' + $(this).text().trim() + ' EasyPay'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[189] == 'undefined') {
                        utag.runonce[189] = 1;
                        jQuery(document.body).on('click', '.module-easy-pay .sales-agreements-table .easy-pay-form-edit .form-radio.form-radio-large', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Easy Pay',
                                "ga_event_label": 'Selected ' + $(this).text().replace(/\d/g, '').trim() + ' as payment method'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[279] == 'undefined') {
                        utag.runonce[279] = 1;
                        jQuery(document.body).on('change', '.module-easy-pay .sales-agreements-table .easy-pay-form-edit select:eq(0)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Easy Pay',
                                "ga_event_label": 'Selected ' + $(this).find('option:selected').text().replace(/\d/g, '').trim() + ' as payment method'
                            })
                        });
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[190] == 'undefined') {
                        utag.runonce[190] = 1;
                        jQuery(document.body).on('mousedown', '.module-easy-pay .sales-agreements-table .form-buttons a.btn:not(.btn-inactive), .module-easy-pay .enroll-form .form-buttons a.btn:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Easy Pay',
                                "ga_event_label": 'Clicked ' + $(this).text().trim() + ' on EasyPay'
                            })
                        });
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[280] == 'undefined') {
                        utag.runonce[280] = 1;
                        jQuery(document.body).on('click', 'div.module.module-yia-pay div.enroll-form div.form-checkbox', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Year in Advance Payments',
                                "ga_event_label": 'Selected sales agreement to enroll in Year in Advance pay'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[192] == 'undefined') {
                        utag.runonce[192] = 1;
                        jQuery(document.body).on('click', '.module-yia-pay table tr.summary .form-checkbox', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Year in Advance Payments',
                                "ga_event_label": 'Selected sales agreement to enroll in Year in Advance pay'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[281] == 'undefined') {
                        utag.runonce[281] = 1;
                        jQuery(document.body).on('click', 'div.module.module-yia-pay div.form-buttons-right .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Year in Advance Payments',
                                "ga_event_label": 'Clicked ' + $(this).text().trim() + ' to complete Year in Advance payment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[193] == 'undefined') {
                        utag.runonce[193] = 1;
                        jQuery(document.body).on('click', '.module-yia-pay .form-buttons .btn.btn-orange:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Year in Advance Payments',
                                "ga_event_label": 'Clicked ' + $(this).text().trim() + ' to complete Year in Advance payment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                var trackerName = 'TerminixCom',
                    containerIDs = ['contact-form'],
                    containerClasses = ['modal-submit-form'],
                    container = null,
                    i = 0;
                if (typeof (ga) !== 'undefined') {
                    var tracker = ga.getByName(trackerName);
                    if (tracker && tracker !== null) {
                        utag.data.GAClientID = tracker.get('clientId');
                    }
                } else {
                    var GACookieID = getCookie('_ga');
                    if (GACookieID != undefined && GACookieID != '') {
                        utag_data['GAClientID'] = GACookieID.replace("GA1.2.", "");
                    }
                }
                while (container === null && i <= containerIDs.length) {
                    var container = document.getElementById(containerIDs[i]);
                    i++;
                }
                if (container === null) {
                    i = 0;
                    while (container === null && i <= containerClasses.length) {
                        var container = document.getElementsByClassName(containerClasses[i]);
                        i++;
                    }
                }
                if (container != null) {
                    var input = document.createElement("input");
                    input.type = "hidden";
                    input.name = "GAClientID";
                    input.value = utag.data.GAClientID;
                    container[0].appendChild(input);
                }

                function getCookie(name) {
                    var value = "; " + document.cookie;
                    var parts = value.split("; " + name + "=");
                    if (parts.length == 2) return parts.pop().split(";").shift();
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[198] == 'undefined') {
                        utag.runonce[198] = 1;
                        utag.jdhc.push({
                            i: '.module-easy-pay',
                            e: 1
                        });
                        jQuery(document.body).on('afterShow', '.module-easy-pay', function (e) {
                            if (jQuery('.module-easy-pay').is(':visible')) {
                                utag.link({
                                    "ga_event_category": 'My Account',
                                    "ga_event_action": 'Easy Pay',
                                    "ga_event_label": 'Easy Pay available'
                                })
                            }
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[204] == 'undefined') {
                        utag.runonce[204] = 1;
                        jQuery(document.body).on('click', '#buy_online_mx_dt', function (e) {
                            utag.link({
                                "ga_event_category": 'Nav Bar',
                                "ga_event_action": 'Link Clicks',
                                "ga_event_label": 'Buy Online (DT)'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[205] == 'undefined') {
                        utag.runonce[205] = 1;
                        jQuery(document.body).on('click', '#buy_online_mx_mb', function (e) {
                            utag.link({
                                "ga_event_category": 'Nav Bar',
                                "ga_event_action": 'Link Clicks',
                                "ga_event_label": 'Buy Online (MB)'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                var thisPath = window.location.pathname;

                function getphoneCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }

                function editPhoneDiv(phonecookie) {
                    $("body > header > div > div.wrap.header-top > span").text(phonecookie);
                    $("body > header > div > div.orange-box.white.visible-custom.sm-pad > a > u").html("<u>TAP TO CALL " + phonecookie + " </u>");
                    $("body > header > div > div.orange-box.white.visible-custom.sm-pad > a > u").attr("href", "tel:" + phonecookie + "");
                    $("#clickToCallBox > a").html("<u class=\"white\">Tap to Call</u><br>" + phonecookie + "");
                    $("a.white.text-center").attr("href", "tel:" + phonecookie + "");
                }

                function checkphoneCookie() {
                    var phonecookie = getphoneCookie("phoneswitch");
                    if (phonecookie.length == "14") {
                        editPhoneDiv(phonecookie);
                    }
                }
                var pathcheck = thisPath.indexOf("/free-inspection/step-two/");
                if (pathcheck == 0) {
                    checkphoneCookie();
                };
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[207] == 'undefined') {
                        utag.runonce[207] = 1;
                        jQuery('#page-content').on('click', 'div.stored-payment-methods div.payment-method-form-remove div.form-buttons-right a.btn', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Payment Methods',
                                "ga_event_label": 'Remove Payment Method:  ' + $(this).text().trim()
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[282] == 'undefined') {
                        utag.runonce[282] = 1;
                        jQuery('#page-content').on('click', '.module-stored-payments .stored-payment-methods .payment-method-form-remove .form-buttons .btn', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Payment Methods',
                                "ga_event_label": 'Remove Payment Method:  ' + $(this).text().trim()
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                $($(".fs-sm.md-m-left")[0]).click(function () {
                    utag.link({
                        ga_event_category: "Header Links",
                        ga_event_action: "MyAcct Login Click Desktop",
                        ga_event_label: "page: " + window.location.pathname
                    });
                    console.log("event_run");
                });
                $(".pull-left.sm-font.bold").click(function () {
                    utag.link({
                        ga_event_category: "Header Links",
                        ga_event_action: "MyAcct Login Click Mobile",
                        ga_event_label: "page: " + window.location.pathname
                    });
                    console.log("event_run");
                });
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[213] == 'undefined') {
                        utag.runonce[213] = 1;
                        jQuery(document.body).on('click', '.module-scheduled-services table tr.summary .btn[data-bind*="ConfirmTentativeAppointment"]', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'Scheduled Services',
                                "ga_event_label": 'Clicked to confirm'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (typeof jQuery !== 'undefined') {
                            jQuery('a').each(function () {
                                var href = jQuery(this).attr('href');
                                if (href && href.match(/^mailto\:/i)) {
                                    jQuery(this).click(function () {
                                        utag.link({
                                            ga_event_category: 'Email Links',
                                            ga_event_action: 'Click',
                                            ga_event_label: 'Email Link Clicked'
                                        });
                                    });
                                }
                            });
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (typeof jQuery !== 'undefined') {
                            var filetypes = /\.(zip|gzip|exe|pdf|doc*|xls*|ppt*|mp3|mp4|mov)$/i;
                            jQuery('a').each(function () {
                                var href = jQuery(this).attr('href');
                                if (href && href.match(filetypes)) {
                                    jQuery(this).click(function () {
                                        var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                                        var filePath = href;
                                        utag.link({
                                            ga_event_category: 'Document Download',
                                            ga_event_action: 'Click-' + extension,
                                            ga_event_label: filePath
                                        });
                                        if (jQuery(this).attr('target') != undefined && jQuery(this).attr('target').toLowerCase() != '_blank') {
                                            setTimeout(function () {
                                                location.href = baseHref + href;
                                            }, 200);
                                            return false;
                                        }
                                    });
                                }
                            });
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.url'].toString().indexOf('buyonline') > -1) {
                        _uzactfeed = window._uzactfeed || [];
                        _uzactfeed.push(['_setID', '1E80ACC10405E71180CC0050569444FB']);
                        _uzactfeed.push(['_setSID', '1D80ACC10405E71180CC0050569444FB']);
                        _uzactfeed.push(['_start']);
                        (function () {
                            var uz = document.createElement('script');
                            uz.type = 'text/javascript';
                            uz.async = true;
                            uz.charset = 'utf-8';
                            uz.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn5.userzoom.com/feedback/js/uz_feed_us.js?cuid=D2EE77AC6BCDE41180C90050569444FB';
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(uz, s);
                        })();
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[218] == 'undefined') {
                        utag.runonce[218] = 1;
                        jQuery(document.body).on('click', '#document html body.white-box.no-pad div.clearfix.main div.unpad.row div.unpad.flex-desktop.col-xs-12.pest-control-image.home-image div.no-marg-m.white-back-transp.col-md-offset-6.col-sm-offset-5.col-sm-7.col-xs-12 div.lg-pad-bot.lg-m-top.col-md-offset-3.col-md-6.col-xs-offset-2.col-xs-8 a button.pull-right.btn', function (e) {
                            utag.link({
                                "ga_event_action": 'Buy Now ',
                                "ga_event_category": 'PestCtrl AB Test',
                                "ga_event_label": 'Buy Now top btn click'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (location.pathname.indexOf("/pest-control") == 0) {
                            $($(".btn")[3]).on("click", function () {
                                utag.link({
                                    ga_event_category: 'Button',
                                    ga_event_action: 'Pest Control',
                                    ga_event_label: 'See Pricing'
                                });
                            })
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (1) {
                    if (typeof utag.runonce[225] == 'undefined') {
                        utag.runonce[225] = 1;
                        jQuery('div#article').on('click', 'button.btn', function (e) {
                            utag.link({
                                "ga_event_category": 'Pest Article Page CTA',
                                "ga_event_action": 'Call to Action Clicked',
                                "ga_event_label": $(this).text().trim()
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        $(document).ready(function () {
                            $(".mainnav ul li a, .mainnav li a").on("click", function () {
                                try {
                                    var link_data = {
                                        ga_event_category: 'TMX Ecom',
                                        ga_event_action: 'NewNav:' + $(this).text(),
                                        ga_event_label: 'Link Clicked in Header on ' + window.location.pathname
                                    };
                                    utag.link(link_data)
                                } catch (e) {
                                    console.log(e)
                                }
                            })
                        });
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[231] == 'undefined') {
                        utag.runonce[231] = 1;
                        jQuery(document.body).on('click', '#page-content .form-container .schedule-appointment .a[data-bind*="SetNoRescheduleDay"]', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'DispatchMe Scheduling',
                                "ga_event_label": 'Clicked "None of these dates work for me" on reschedule'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[232] == 'undefined') {
                        utag.runonce[232] = 1;
                        jQuery(document.body).on('click', '#page-content .form-container .schedule-appointment .form-buttons .btn:not(.btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'DispatchMe Scheduling',
                                "ga_event_label": 'Clicked "Submit" to submit rescheduled appointment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[233] == 'undefined') {
                        utag.runonce[233] = 1;
                        jQuery(document.body).on('click', '#page-content .form-container .schedule-contact .form-buttons .btn-link', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'DispatchMe Scheduling',
                                "ga_event_label": 'Clicked "Cancel" on preferred reschedule form'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                if (utag.data['dom.pathname'].toString().indexOf('/my-account') > -1) {
                    if (typeof utag.runonce[234] == 'undefined') {
                        utag.runonce[234] = 1;
                        jQuery(document.body).on('click', '#page-content .form-container .schedule-contact .form-buttons .btn:not(.btn-link, .btn-inactive)', function (e) {
                            utag.link({
                                "ga_event_category": 'My Account',
                                "ga_event_action": 'DispatchMe Scheduling',
                                "ga_event_label": 'Clicked "Confirm" to request rescheduled appointment'
                            })
                        })
                    }
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (location.pathname.indexOf("/blog") > -1) {
                            $('.btn').on("click", function () {
                                utag.link({
                                    ga_event_category: 'TMX Blog CTA',
                                    ga_event_action: 'Click',
                                    ga_event_label: '' + $(this).text()
                                })
                                console.log("this is a test");
                            })
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        ahsCust = false;
                        tmxCust = false;
                        svcLinePC = false;
                        svcLinePM = false;
                        svcLineTC = false;
                        dim32 = "";
                        for (i = 0; i < crmSeg.length; i++) {
                            if (crmSeg[i] == "1") {
                                ahsCust = true;
                            }
                            if (crmSeg[i] == "2") {
                                tmxCust = true;
                            }
                            if (crmSeg[i] == "3") {
                                svcLinePC = true;
                            }
                            if (crmSeg[i] == "4") {
                                svcLinePM = true;
                            }
                            if (crmSeg[i] == "5") {
                                svcLineTC = true;
                            }
                        }
                        if (ahsCust == true && tmxCust == true) {
                            dim32 = "both";
                        } else {
                            if (ahsCust == true) {
                                dim32 = "ahs";
                            }
                            if (tmxCust == true) {
                                dim32 = "tmx";
                            }
                        }
                        if (dim32.length > 1) {
                            utag.link({
                                neustarCRMSegCurrentCust: dim32
                            });
                        }
                        if (svcLinePC == true) {
                            utag.link({
                                neustarCRMSegPC: "PC"
                            });
                        }
                        if (svcLinePM == true) {
                            utag.link({
                                neustarCRMSegPM: "PM"
                            });
                        }
                        if (svcLineTC == true) {
                            utag.link({
                                neustarCRMSegTC: "TC"
                            });
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (/\/pest\-control\/$|\/pest\-control\/\?|\/pest\-control$|\/pest\-control\?|\/pest\-control\/ants\/$|\/pest\-control\/ants\/\?|\/pest\-control\/ants$|\/pest\-control\/ants\?|\/pest\-control\/cockroaches\/$|\/pest\-control\/cockroaches\/\?|\/pest\-control\/cockroaches$|\/pest\-control\/cockroaches\?|\/pest\-control\/spiders\/$|\/pest\-control\/spiders\/\?|\/pest\-control\/spiders$|\/pest\-control\/spiders\?/.test(window.location.pathname) == true) {
                            $('body > div.main.clearfix > div.row.wrap > div.col-xs-12.col-md-12.grey-box.text-center.lg-pad > a').on("click", function () {
                                utag.link({
                                    ga_event_category: 'Pest Control Homepage',
                                    ga_event_action: 'Button',
                                    ga_event_label: 'Save 50 Content Box'
                                });
                            });
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        var path = window.location.pathname;
                        if (window.location.hostname == "www.terminix.com") {
                            if (path.includes("/bed-bug-control")) {
                                var isMobile = false;
                                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;
                                $('body > div.main.clearfix > div.row.clearfix.unpad > div > div.col-xs-12.col-sm-7.col-sm-offset-5.col-md-offset-6.white-back-transp.no-marg-m > div').html("CALL FOR YOUR<br><span style=\"color: #0C8F43\">FREE INSPECTION!<\/span>\r\n  \r\n  <a class=\"\" href=\"tel:18009982647\"><button class=\"btn pull-right\">1.800.998.2647<\/button><\/a><br>");
                                $('body > div.main.clearfix > div.row.clearfix.unpad > div > div.col-xs-12.col-sm-7.col-sm-offset-5.col-md-offset-6.white-back-transp.no-marg-m > div').css({
                                    'padding-bottom': '20px',
                                    'text-align': 'center',
                                    'line-height': '24px',
                                    'font-weight': '600'
                                });
                                $('body > div.main.clearfix > div.wrap.row > div.col-xs-12.md-pad.grey-box > div.col-md-4.col-xs-12').remove();
                                $('body > div.main.clearfix > div.wrap.row > div.col-xs-12.md-pad.grey-box > div').css('width', '100%');
                                if (isMobile == false) {
                                    $('body > div.main.clearfix > div.row.clearfix.unpad > div > div.col-xs-12.col-sm-7.col-sm-offset-5.col-md-offset-6.white-back-transp.no-marg-m > div > a').css({
                                        'pointer-events': 'none',
                                        'cursor': 'default'
                                    });
                                }
                                var telNum = "1.800.998.2647";
                                $topLeftTFN = $('body > header.header-container.desktop > div.header-right-container > div.top-bar-container > div.phone-number-container.left > span');
                                $botRightTFN = $('body > footer > div.help-bar > div > a');
                                $footerTFN = $('body > footer > div.container > div.row.footer-section > div:nth-child(4) > div > a');
                                $topLeftTFN.text(telNum);
                                $botRightTFN.text(telNum);
                                $botRightTFN.attr('href', 'tel:18009982647');
                                $footerTFN.text(telNum);
                                $footerTFN.attr('href', 'tel:18009982647');
                                $('body > header.header-container.mobile.open > div.bar-container > div.phone-link > a').attr('href', 'tel:18009982647');
                                $('body > header.header-container.mobile.open > div.bar-container > div.phone-link > a > span').text(telNum);
                                $('body > header.header-container.mobile > div.bar-container > div.phone-link > a').attr('href', 'tel:18009982647');
                                $('body > header.header-container.mobile > div.bar-container > div.phone-link > a > span').text(telNum);
                                $('body > header.header-container.desktop > div.header-right-container > div.top-bar-container > div.secondary-links-container.right').html("<a href=\"\/exterminators\/\"><i class=\"material-icons tiny\">place<\/i> Find A Location<\/a>");
                                $('body > header.header-container.mobile.open > div.nav-container > div').html("<a href=\"\/my-account\/\"><i class=\"material-icons tiny\">account_circle<\/i> Login<\/a>");
                                ga('terminixRollUp.set', 'dimension26', 'Bed Bugs Call Only Test');
                                $('body > div.main.clearfix > div.row.clearfix.unpad > div > div.col-xs-12.col-sm-7.col-sm-offset-5.col-md-offset-6.white-back-transp.no-marg-m > div > a').on("click", function () {
                                    utag.link({
                                        ga_event_category: 'Bed Bugs Page',
                                        ga_event_action: 'CTA Button',
                                        ga_event_label: 'Click To Call'
                                    });
                                })
                                $('body > header.header-container.mobile > div.bar-container > div.nav-icon > a').on("click", function () {
                                    $('body > header.header-container.mobile.open > div.nav-container > div').html("<a href=\"\/my-account\/\"><i class=\"material-icons tiny\">account_circle<\/i> Login<\/a>");
                                })
                            }
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.pathname'].toString().indexOf('pest-control/mosquitoes') > -1) {
                        if (/\/pest\-control\/mosquitoes($|\/$|\?|\/\?)/.test(window.location.pathname) == true && window.location.host == "www.terminix.com") {
                            $('#solution > section:nth-child(2) > div > ul > li:nth-child(3) > div > p').text("Treatment contains a zone of protection that lasts weeks.");
                            $('#threat > section:nth-child(1) > p').text("You step outside and you're under attack. That's frustrating enough. But mosquitoes can also carry disease. That's why you should get professional help to stop them.");
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if ((utag.data['dom.domain'].toString().indexOf('www.terminix.com') > -1 && utag.data['dom.pathname'].toString().indexOf('/blog') > -1)) {
                        $('#document').ready(function () {
                            jQuery.fn.exists = function () {
                                return jQuery(this).length > 0;
                            }
                            var currentPath = window.location.pathname;
                            var deskMenuBtns = $('a[class="action-button"][href="/buyonline/step-one/"]');
                            if (deskMenuBtns.exists()) {
                                for (var i = 0; i < deskMenuBtns.length; i++) {
                                    $('a[class="action-button"][href="/buyonline/step-one/"]').eq(i).find("span").text("GET A QUOTE");
                                }
                            }
                            var mobMenuText = $('a[href="/buyonline/step-one/"][class="tmx-secondary-txt"]');
                            if (mobMenuText.exists()) {
                                for (var i = 0; i < mobMenuText.length; i++) {
                                    $('a[href="/buyonline/step-one/"][class="tmx-secondary-txt"]').eq(i).text("GET A QUOTE");
                                }
                            }
                        });
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.url'].toString().toLowerCase().indexOf('www.terminix.com/buyonline/step-three'.toLowerCase()) > -1) {
                        $('#serviceScheduler > div > div.scheduler-container > p').on("click", function () {
                            utag.link({
                                ga_event_category: 'TMX Ecom',
                                ga_event_action: 'Pest Scheduler Click',
                                ga_event_label: 'None Of These Times Work'
                            });
                        });
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.domain'].toString().toLowerCase() == 'www.terminix.com'.toLowerCase()) {
                        (function () {
                            var s = document.createElement('script');
                            var x = document.getElementsByTagName('script')[0];
                            s.type = 'text/javascript';
                            s.async = true;
                            s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +
                                'us-sonar.sociomantic.com/js/2010-07-01/adpan/terminix-us';
                            x.parentNode.insertBefore(s, x);
                        })();
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.url'].toString().toLowerCase().indexOf('www.terminix.com/buyonline'.toLowerCase()) > -1) {
                        function getParameterByName(name, url) {
                            if (!url) url = window.location.href;
                            name = name.replace(/[\[\]]/g, "\\$&");
                            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                                results = regex.exec(url);
                            if (!results) return null;
                            if (!results[2]) return '';
                            return decodeURIComponent(results[2].replace(/\+/g, " "));
                        }
                        var pestSelected = getParameterByName("pest");
                        if (window.location.pathname.includes("/buyonline/step-one") && pestSelected == "mosquitoes") {
                            setTimeout(function () {
                                $('body > header > nav > div.white > div > span').text("SAVE $50 ON MOSQUITO CONTROL");
                                jQuery("span:contains('PEST PROBLEM?')").text("MOSQUITO PROBLEM? ");
                                $("img[alt='terminix-truck']").attr("src", "/static-srvm/trmx/ecomm/img/legacy/funnel/MOSQ_QuickGuardGraphic.jpg");
                                $("div[class='extra-plan-information card hide-on-med-and-down']").toggle();
                            }, 200);
                        }
                        if (window.location.pathname.includes("/buyonline/step-two")) {
                            var mosqCard = $("button[name='paymentTypeMOSQ']").parent().parent();
                            var mosqCardText = $(mosqCard).find("div[class='col m6 s9 lg-m-bottom']").children().text();
                            if (mosqCardText.includes("Mosquito Control")) {
                                $(mosqCard).append("<div class=\"col s12 lg-m-bottom\"><div class=\"col s12 md-font no-pad\"><p style=\"font-weight: bold\">Additional service fees may be required for properties with a yard size larger than 1 acre.<\/p><\/div><\/div>");
                            }
                            setTimeout(function () {
                                $('#ppFAQ').toggle();
                            }, 1000);
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (utag.data['dom.url'].toString().toLowerCase().indexOf('www.terminix.com/termite-control/'.toLowerCase()) > -1) {
                        if ($('#solution > div.row.padding-bottom > div').length > 0) {
                            $('#solution > div.row.padding-bottom > div').append("<div class=\"padding-full\">\r\n                                        <p>Ultimate Protection Guarantee not available in portions of Georgia, Florida, Mississippi, Arkansas, and Alabama. For details on availability in these states call 1.800.597.6464<\/p>\r\n                                    <\/div>")
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
            try {
                try {
                    if (1) {
                        if (document.URL.match(/buyonline\/address/)) {
                            if (GetURLParameter('affiliate') === 'FULLCUP') {
                                (function () {
                                    var mt = document.createElement('script');
                                    mt.type = 'text/javascript';
                                    mt.async = true;
                                    mt.src = '//elite.postclickmarketing.com/Outside/liveball.js';
                                    var fscr = document.getElementsByTagName('script')[0];
                                    console.log('prepending liveball script');
                                    fscr.parentNode.insertBefore(mt, fscr);
                                })();
                                var overridePhoneNumberFromParam = function () {
                                    var tfn = GetURLParameter('TFN');
                                    if (!tfn) tfn = GetURLParameter('tfn');
                                    if (tfn && tfn.match(/^1\d{10}$/)) {
                                        var ftfn = "1." + tfn.substring(1, 4) + "." + tfn.substring(4, 7) + "." + tfn.substring(7, 11);
                                        $('.phone-number-container span').text(ftfn);
                                        $('.phone-link a').attr({
                                            "href": "tel:" + tfn
                                        }).text(ftfn);
                                    }
                                };
                            }
                            var marinTrackPage = function () {
                                console.log('marinTrackPage');
                                utag.view({}, null, [195]);
                            };
                            var marinTrackConversion = function () {
                                console.log('marinTrackConversion');
                                if (!vm.utd) return;
                                utag.view({
                                    order_currency_code: 'USD',
                                    marin_conversion_type: 'Ecomm',
                                    marin_order_id: vm.utd.order_id,
                                    product_categories: vm.utd.product_categories,
                                    product_prices: vm.utd.product_prices,
                                    product_ids: vm.utd.product_ids
                                }, null, [196]);
                            };
                            var marinTrackLeadGeneration = function () {
                                console.log('marinTrackLeadGeneration');
                                utag.view({
                                    marin_conversion_type: 'lgform',
                                    marin_order_id: '',
                                    marin_price: ''
                                }, null, [196]);
                            };
                            setTimeout(function () {
                                if (GetURLParameter('affiliate') === 'FULLCUP') {
                                    overridePhoneNumberFromParam();
                                }
                                vm.stepAddressTrackingFired = false;
                                vm.stepProductsTrackingFired = false;
                                vm.stepScheduleTrackingFired = false;
                                vm.stepPaymentTrackingFired = false;
                                vm.stepConfirmationTrackingFired = false;
                                vm.Track = ko.computed(function () {
                                    if (vm.currentStep().toLowerCase() === 'step_address' && !vm.stepAddressTrackingFired) {
                                        marinTrackPage();
                                        vm.stepAddressTrackingFired = false;
                                    } else if (vm.currentStep().toLowerCase() === 'step_products' && !vm.stepProductsTrackingFired) {
                                        marinTrackPage();
                                        marinTrackLeadGeneration();
                                        if (GetURLParameter('affiliate') === 'FULLCUP') {
                                            console.log('liveballRecognize / liveballConvert');
                                            liveballUseJSON(true);
                                            liveballRecognize("elite.postclickmarketing.com");
                                            liveballConvert();
                                        }
                                        vm.stepProductsTrackingFired = true;
                                    } else if (vm.currentStep().toLowerCase() === 'step_schedule' && !vm.stepScheduleTrackingFired) {
                                        marinTrackPage();
                                        vm.stepScheduleTrackingFired = true;
                                    } else if (vm.currentStep().toLowerCase() === 'step_payment' && !vm.stepPaymentTrackingFired) {
                                        marinTrackPage();
                                        vm.stepPaymentTrackingFired = true;
                                    } else if (vm.currentStep().toLowerCase() === 'step_confirmation' && !vm.stepConfirmationTrackingFired) {
                                        marinTrackPage();
                                        vm.utd = ko.toJS(vm).model.sendInfoToGoogleAnalytics;
                                        marinTrackConversion();
                                        vm.stepConfirmationTrackingFired = true;
                                    }
                                    return true;
                                });
                            }, 3000);
                        }
                    }
                } catch (e) {
                    utag.DB(e)
                }
            } catch (e) {
                utag.DB(e)
            };
        }
    })
    if (utag.cfg.readywait || utag.cfg.waittimer) {
        utag.loader.EV('', 'ready', function (a) {
            if (utag.loader.rf == 0) {
                utag.loader.rf = 1;
                utag.cfg.readywait = 1;
                utag.DB('READY:utag.cfg.readywait');
                setTimeout(function () {
                    utag.loader.PINIT()
                }, utag.cfg.waittimer || 1);
            }
        })
    } else {
        utag.loader.PINIT()
    }
}
