var QAS_EMAIL_CONFIG = {
    SUBMIT_PATH: "https://www.terminix.com/includes/email_atg_proxy.jsp",
    AFTER_VERIFY: function () {
        CompleteOrderSubmit()
    },
    OPEN_MODAL: function () {
        if (_MATERIALIZE) $("#qasEmailModal").modal("open");
        else $("#qasEmailModal").modal("show")
    },
    CLOSE_MODAL: function () {
        if (_MATERIALIZE) $("#qasEmailModal").modal("close");
        else $("#qasEmailModal").modal("hide")
    },
    ADD_HIDDEN: function (elem) {
        if (_MATERIALIZE) elem.addClass("hide");
        else elem.addClass("hidden")
    },
    REMOVE_HIDDEN: function (elem) {
        if (_MATERIALIZE) elem.removeClass("hide");
        else elem.removeClass("hidden")
    },
    EMAIL_FIELD: "email"
};
var QAS_EMAIL_CERTAINTYLEVELS = {
    "verified": {
        "qasEmailPrompt": "We were able to verify your email address."
    },
    "undeliverable": {
        "qasEmailPrompt": "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
    },
    "unreachable": {
        "qasEmailPrompt": "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
    },
    "illegitimate": {
        "qasEmailPrompt": "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
    },
    "disposable": {
        "qasEmailPrompt": "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
    },
    "unknown": {
        "qasEmailPrompt": "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
    }
};

function QAS_EMAIL_VALIDATE() {
    QAS_EMAIL_CONFIG.OPEN_MODAL();
    QAS_EMAIL_CONFIG.ADD_HIDDEN($("#qasEmailValidationBody"));
    QAS_EMAIL_CONFIG.ADD_HIDDEN($(".modal-footer"));
    QAS_EMAIL_CONFIG.REMOVE_HIDDEN($("#qasEmailProcessingBody"));
    QAS_EMAIL_SEND()
}

function QAS_EMAIL_SEND() {
    var qasEmail = $("#" + QAS_EMAIL_CONFIG.EMAIL_FIELD).val();
    var data = "?qasemail=" + qasEmail;
    var path = QAS_EMAIL_CONFIG.SUBMIT_PATH + data;
    var xhr = new XMLHttpRequest;
    xhr.open("GET", path, true);
    xhr.timeout = 4E4;
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var response = null;
            try {
                response = JSON.parse(xhr.responseText)
            } catch (error) {
                console.log(error)
            }
            QAS_EMAIL_RESPONSE(response)
        }
    }
}

function QAS_EMAIL_RESPONSE(response) {
    var certainty = null;
    if (response == null || typeof response.certainty === "undefined") console.error("No or incorrect response");
    else certainty = response.certainty;
    if (response == null || certainty == null || certainty == "verified") QAS_EMAIL_PUSH(response);
    else QAS_EMAIL_MODAL(response)
}

function QAS_EMAIL_PUSH(response) {
    QAS_EMAIL_CONFIG.CLOSE_MODAL();
    QAS_EMAIL_CONFIG.AFTER_VERIFY()
}

function QAS_EMAIL_MODAL(response) {
    for (var key in QAS_EMAIL_CERTAINTYLEVELS[response.certainty]) {
        if (QAS_EMAIL_CERTAINTYLEVELS[response.certainty][key] !== "NONE") $("#" + key).text(QAS_EMAIL_CERTAINTYLEVELS[response.certainty][key]);
        else $("#" + key).text("");
        $("#qasEmailUserEnteredLabel").text("Continue with: " + $("#" + QAS_EMAIL_CONFIG.EMAIL_FIELD).val());
        $("#qasEmailModifyCheckLabel").text("Update to: ")
    }
    QAS_EMAIL_CONFIG.ADD_HIDDEN($("#qasEmailProcessingBody"));
    QAS_EMAIL_CONFIG.REMOVE_HIDDEN($("#qasEmailValidationBody"));
    QAS_EMAIL_CONFIG.REMOVE_HIDDEN($(".modal-footer"))
}

function QAS_EMAIL_REVALIDATE() {
    var $emailInput = $("#qasEmailModifyInput");
    if (document.getElementById("qasEmailUserEnteredCheck").checked) {
        $emailInput.prop("required", false);
        QAS_EMAIL_CONFIG.CLOSE_MODAL();
        QAS_EMAIL_PUSH()
    } else {
        $emailInput.prop("required", true);
        if (!$emailInput[0].checkValidity()) {
            $emailInput.addClass("invalid");
            return false
        }
        document.getElementById(QAS_EMAIL_CONFIG.EMAIL_FIELD).value = $emailInput.val();
        $emailInput.prop("required", false);
        $emailInput.val("");
        if (_MATERIALIZE) Materialize.updateTextFields();
        $("#qasEmailUserEnteredCheck").trigger("click");
        QAS_EMAIL_VALIDATE()
    }
}
$(document).ready(function () {
    $("input[name='emailChoice']").change(function () {
        if ($("#qasEmailModifyCheck").is(":checked")) $("#qasEmailModifyInput").prop("disabled", false);
        else $("#qasEmailModifyInput").prop("disabled", true)
    })
});
