jQuery.extend(jQuery.expr[":"], {
    invalid: function (elem, index, match) {
        var invalids = document.querySelectorAll(":invalid"),
            result = false,
            len = invalids.length;
        if (len)
            for (var i = 0; i < len; i++)
                if (elem === invalids[i]) {
                    result = true;
                    break
                } return result
    }
});

function checkURLParam(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split("&");
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split("=");
        if (sParameterName[0] == sParam) return sParameterName[1]
    }
    return false
}
$(document).ready(function () {
    if (checkURLParam("fromReferral") && $("#referralInput").length) {
        $("#referralInput > input").val(checkURLParam("fromReferral"));
        $("#referralInput").removeClass("hide")
    }
    if (_AUTO_ADDING_PEST_CONTROL_FEATURE_TOGGLE)
        if ($("#MOSQ").text() === "" && $("#GENPEST").text() === "") $("#addItemGENPEST").trigger("click");
    if (_MATERIALIZE) $(".slider").slider({
        full_width: true,
        height: 200,
        interval: 7E3,
        indicators: false
    });
    $("#passwordMyAcccount").focus(function () {
        $(".passwordValidation").removeClass("hidden")
    });
    $("#passwordMyAcccount").keyup(function () {
        if (!$(".passwordValidation").hasClass("hidden")) validationPassword()
    });
    $(".buyToggle").click(function (e) {
        e.stopPropagation();
        $(this).toggleClass("disabled")
    });
    heightNormalization("", ".sameHeight");
    displayMosqSeason($("#templateCode").text());
    if ($("#totalAmountWithTax").text() != "") $("#mobileTotal").text($("#totalAmountWithTax").text());
    changeTargetPest();
    $('input[name="paymentType"]:first').click();
    $('[data-toggle="tooltip"]').tooltip();
    $("#viewCoveredPests").click(function () {
        $("#CoveredPests").toggleClass("hidden")
    });
    $("#sendConfirmation").click(function () {
        OpenModal("sendEmailConfirmation-modal");
        setTimeout(function () {
            CloseModal("sendEmailConfirmation-modal")
        }, 5E3)
    });
    $("#phoneIns, #phoneTwin").mask("(999)999-9999");
    $("#stepOneButton").on("click", function () {
        $(this).removeClass("disabled")
    });
    $("#addressForm").submit(function (e) {
        if ($("#over8000Modal").hasClass("in") || $("#over8000Modal").hasClass("open")) {
            CloseModal("over8000Modal");
            OpenModal("processing-modal");
            if (typeof utag_data !== "undefined") {
                utag_data.customer_email =
                    $("#email").val();
                utag_data.customer_address = $("#address1").val() + " " + $("#address2").val();
                utag_data.customer_zip = $("#zipCode").val();
                utag_data.customer_property_option = $("#homeSize_hidden").val();
                utag_data.customer_type = "offline"
            }
            procEvent("ecommerce", "address form success", "leadgen", "OVER 8000 SQ FT");
            return true
        }
        if ($("#sqftSize option:selected").val() === "8000") {
            OpenModal("over8000Modal");
            $("#firstName").attr("required", true);
            $("#lastName").attr("required", true);
            $("#phone").attr("required", true);
            $("#consent").attr("required", true);
            $("#firstName").removeClass("invalid");
            $("#lastName").removeClass("invalid");
            $("#phone").removeClass("invalid");
            $("#consent").removeClass("invalid");
            if ($("#abPhone")) {
                $("#phone").val($("#abPhone").val());
                Materialize.updateTextFields()
            }
            return false
        } else OpenModal("processing-modal")
    });
    $("#reqInspection").click(function (e) {
        if (isValid($("#firstName")) && isValid($("#lastName")) && isValid($("#phone")) && isValid($("#consent"))) $("#addressForm").submit()
    });
    $("#billingAddressCheckbox").change(function () {
        if (_MATERIALIZE) $("#billingAddress").toggleClass("hide");
        else $("#billingAddress").toggleClass("hidden")
    });
    $("#emailMyQuote").submit(function (e) {
        e.preventDefault();
        if ($("#emailTo").val() == "" || $("emailTo").hasClass("invalid"));
        else submitAjaxForm("emailMyQuote", showResponseModal)
    });
    $("#existingCustomer").change(function () {
        if ($(this).is(":checked")) OpenModal("custLookupModal");
        else CloseModal("custLookupModal")
    });
    if (checkURLParam("cNum") && $("#existingCustomer").length) {
        $("#existingCustomer").prop("checked", true);
        $("#customerNumber").val(checkURLParam("cNum"));
        $("#phoneTwin").val(checkURLParam("cTel")).trigger("change");
        getCustomerDetails()
    } else $("#existingCustomer").prop("checked", false)
});

function showResponseModal(data) {
    var status = data.status;
    if (status === "OK") OpenModal("purchaseThanks");
    else if (status === "EMPTY");
    else OpenModal("purchaseSorry")
}

function changeTargetPest() {
    if (window.location.href.indexOf("mosquitoes") > -1) $("#productId_hidden").val("10003X10002")
}

function isValid(field) {
    if (field.attr("id") == "consent")
        if (_MATERIALIZE)
            if (field.is(":invalid")) $(".consentText").addClass("red-text");
            else $(".consentText").removeClass("red-text");
    else if (field.is(":invalid")) $(".consentText").addClass("red");
    else $(".consentText").removeClass("red");
    if (field.is(":invalid") && field.prop("required")) {
        field.addClass("invalid");
        field.removeClass("hidden");
        if (field.is("select") && _MATERIALIZE) field.parent(".select-wrapper").children(".select-dropdown").addClass("invalid");
        return false
    }
    if (!ValidateCardType(field)) return false;
    if (field.attr("id") == "bankRoutingNumber" && field.prop("required")) {
        var number = field.val();
        if (number.length !== 9 || number[0] == "5" || !$.isNumeric(number)) return false;
        var checksumTotal = 7 * (parseInt(number.charAt(0), 10) + parseInt(number.charAt(3), 10) + parseInt(number.charAt(6), 10)) + 3 * (parseInt(number.charAt(1), 10) + parseInt(number.charAt(4), 10) + parseInt(number.charAt(7), 10)) + 9 * (parseInt(number.charAt(2), 10) + parseInt(number.charAt(5), 10) + parseInt(number.charAt(8),
            10));
        var checksumMod = checksumTotal % 10;
        if (checksumMod !== 0) {
            field.addClass("invalid");
            return false
        } else return true
    }
    if ((field.attr("id") == "expMonth" || field.attr("id") == "expYear") && field.prop("required")) return ValidateExpDate();
    return true
}

function ValidateCardType(field) {
    if (field.attr("id") === "ccNumber" && field.prop("required")) {
        var $ccType = $("#ccType");
        var creditCardNumber = field.val(),
            creditCardType = $ccType.val();
        if (!ValidateCreditCardNumber(creditCardNumber, creditCardType)) {
            field.addClass("invalid");
            return false
        }
    }
    return true
}

function ValidateExpDate() {
    var $month = $("#expMonth"),
        $year = $("#expYear"),
        monthVal = $("#expMonth option:selected").val(),
        yearVal = $("#expYear option:selected").val(),
        d = (new Date).getMonth() + 2,
        y = (new Date).getFullYear();
    if (!monthVal || !yearVal || yearVal == y && monthVal < d) {
        $month.addClass("invalid");
        $month.parent(".select-wrapper").children(".select-dropdown").addClass("invalid");
        $year.addClass("invalid");
        $year.parent(".select-wrapper").children(".select-dropdown").addClass("invalid");
        return false
    }
    $month.removeClass("invalid");
    $month.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid");
    $year.removeClass("invalid");
    $year.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid");
    return true
}
$(document).on("blur change", "#expYear, #expMonth", function () {
    if ($("#expYear option:selected").val() && $("#expMonth option:selected").val()) ValidateExpDate()
});
$(document).on("change", "#ccType", function (e) {
    var $ccType = $(this),
        $ccNumber = $("#ccNumber");
    var cardType = GetCreditCardType($ccNumber.val());
    if (cardType && IsObject(cardType))
        if (cardType.value.toLowerCase() === $ccType.val().toLowerCase()) {
            $ccType.removeClass("invalid");
            $ccType.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid");
            return
        } $ccType.addClass("invalid");
    $ccType.parent(".select-wrapper").children(".select-dropdown").addClass("invalid")
});
$(document).on("keyup", "#ccNumber", function (e) {
    var $ccNumber = $(this),
        $ccType = $("#ccType");
    if ($ccNumber.val()) {
        var cardType = GetCreditCardType($ccNumber.val());
        if (cardType && IsObject(cardType)) $ccType.val(cardType.value);
        else $ccType.val("");
        if (_MATERIALIZE) $ccType.material_select()
    }
});

function submitAjaxForm(formId, responseHandler) {
    var options = {
        success: function (data) {
            if (formId === "addToCartForm");
            else if (formId === "removeFromCartFormByRefId" || formId === "removeFromCartForm");
            responseHandler(data)
        },
        error: function (data) {
            if (formId === "addToCartForm");
            else if (formId === "removeFromCartFormByRefId" || formId === "removeFromCartForm");
            responseHandler(data)
        },
        dataType: "json"
    };
    var form = $("#" + formId);
    form.ajaxSubmit(options);
    return false
}

function sendConfirmation() {
    submitAjaxForm("emailForm", doNothing);
    return false
}

function checkAvailableDates(date, slotArray) {
    var availableDates = new Array(0);
    for (var i = 0; i < slotArray.length; i++) {
        var availDate = slotArray[i].schDate;
        if (availableDates.indexOf(availDate) == -1) {
            if (availDate.charAt(0) == 0) availDate = availDate.substring(1, availDate.length);
            availableDates.push(availDate.replace("/0", "/"))
        }
    }
    dmy = date.getMonth() + 1 + "/" + date.getDate() + "/" + String(date.getFullYear()).substring(2, 4);
    if ($.inArray(dmy, availableDates) > -1) return [true, "selectable", "Available"];
    else return [false, "", "unAvailable"]
}

function checkOpenTimeSlots(selectedDate, slotArray, id) {
    var timeSlots;
    var dateString = selectedDate.substring(0, 6) + selectedDate.substring(8, 12);
    if (dateString.charAt(0) == 0) dateString = dateString.substring(1, dateString.length);
    timeSlots = new Array(0);
    for (var i = 0; i < slotArray.length; i++)
        if (slotArray[i].schDate == dateString) {
            var time = timeScanner(slotArray[i].schToTime.substring(0, 5));
            if (timeSlots.push(slotArray[i].schSlotKey + "XX" + timeScanner(slotArray[i].schFrmTime.substring(0, 5)) + " - " + timeScanner(slotArray[i].schToTime.substring(0,
                    5))) == -1) timeSlots.push(slotArray[i].schSlotKey + "XX" + timeScanner(slotArray[i].schFrmTime.substring(0, 5)) + " - " + timeScanner(slotArray[i].schToTime.substring(0, 5)))
        } timeSlots.sort(SortByTime);
    updateSelectedDate(id, selectedDate);
    buildTimeSlots(timeSlots, id)
}

function SortByTime(a, b) {
    var atime = a.split("XX")[1].substr(0, 7);
    var btime = b.split("XX")[1].substr(0, 7);
    if (atime.indexOf("M") == -1) atime = atime + "M";
    if (btime.indexOf("M") == -1) btime = btime + "M";
    return new Date("1970/01/01 " + atime) - new Date("1970/01/01 " + btime)
}

function timeScanner(slotTime) {
    var ampm = "AM";
    var hh = parseInt(slotTime.substring(0, 2));
    var mm = slotTime.substring(2, 5);
    if (hh > 11) ampm = "PM";
    hh = hourCalc(hh);
    slotTime = hh + mm + " " + ampm;
    return slotTime
}

function hourCalc(hh) {
    if (hh > 11) {
        hh = parseInt(hh) - 12;
        if (hh == 0) hh = 12
    }
    return hh
}

function updateSelectedDate(id, itemSelected) {
    $("#" + id + " .alternateDate").empty();
    $("#" + id + " .alternateTime").empty();
    $("#" + id + " .alternateDate").append(itemSelected)
}

function updateSelectedTime(itemSelected, id) {
    $("#" + id + " .alternateTime").empty();
    $("#" + id + " .alternateTime").append(itemSelected);
    if (itemSelected == "None of the dates work for me") $("#" + id + " .alternateDate").empty();
    if (_MATERIALIZE) $("#" + id + " .alternateDateTime").removeClass("hide");
    else $("#" + id + " .alternateDateTime").removeClass("hidden")
}

function buildTimeSlots(timeSlots, id) {
    $("#" + id + "CalendarSlots").empty();
    timeSlotDisplayed = new Array(0);
    $("#scheduleServiceButton").addClass("disabled");
    for (var i = 0; i < timeSlots.length; i++)
        if ($.inArray(timeSlots[i].split("XX")[1], timeSlotDisplayed) < 0)
            if (_MATERIALIZE) {
                $("#" + id + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + "timeslotradio' class='customslot hidden' id='timeslottrigger' value='" + timeSlots[i].split("XX")[0] + "' onClick='updateSelectedTime(\"" + timeSlots[i].split("XX")[1] +
                    '","' + id + "\")'><h5 class='materialRadioFix sbold'>" + timeSlots[i].split("XX")[1] + " </h5></label></div>");
                timeSlotDisplayed.push(timeSlots[i].split("XX")[1])
            } else {
                $("#" + id + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + "timeslotradio' class='customslot' id='timeslottrigger' value='" + timeSlots[i].split("XX")[0] + "' onClick='updateSelectedTime(\"" + timeSlots[i].split("XX")[1] + '","' + id + "\")'><h5 class=' sbold'>" + timeSlots[i].split("XX")[1] + " </h5></label></div>");
                timeSlotDisplayed.push(timeSlots[i].split("XX")[1])
            } if (_MATERIALIZE) $("#" +
        id + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + "timeslotradio' class='customslot' id='timeslotnoappointment' onClick='updateSelectedTime(\"None of the dates work for me\",\"" + id + "\")' value=''><h5 class='materialRadioFix  sbold'> None of the dates work for me </h5></label></div>");
    else $("#" + id + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + "timeslotradio' class='customslot' id='timeslotnoappointment' onClick='updateSelectedTime(\"None of the dates work for me\",\"" +
        id + "\")' value=''><h5 class=' sbold'> None of the dates work for me </h5></label></div>");
    $("#" + id + "timeslotnoappointment").click(function () {});
    $("#" + id + "selectedCalendarDateSlots input:radio").click(function () {
        $("#" + id + "scheduleServiceButton").removeClass("disabled")
    })
}

function getSlotsForAppointment(productId, appointmentType, id) {
    var ajaxTime = (new Date).getTime();
    $.ajax({
        type: "POST",
        url: "/buyonline/common/openSlotsMapJSON.jsp?productId=" + productId + "&appointmentType=" + appointmentType,
        dataType: "json",
        success: function (data) {
            var totalTime = (new Date).getTime() - ajaxTime;
            procEvent("appointmentSlotRetrievalStep2", "Step2 Appointment Retrieval Success", "loadTime:" + totalTime)
        },
        error: function (data) {}
    })
}

function getSlotsForCalendar(productId, appointmentType, id) {
    var ajaxTime = (new Date).getTime();
    $.ajax({
        type: "POST",
        url: "/buyonline/common/openSlotsMapJSON.jsp?productId=" + productId + "&appointmentType=" + appointmentType,
        dataType: "json",
        success: function (data) {
            var slotArray;
            var availableDates;
            var availableDateTimes;
            slotArray = new Array(0);
            availableDates = new Array(0);
            availableDateTimes = new Array(0);
            for (var i = 0; i < data.jsonOpenSlotMap.length; i++) {
                var availDate = data.jsonOpenSlotMap[i].schDate;
                if (availDate.charAt(0) ==
                    0) availDate = availDate.substring(1, availDate.length);
                data.jsonOpenSlotMap[i].schDate = availDate;
                var dateTime = data.jsonOpenSlotMap[i].schSlotKey.slice(-14);
                availableDateTimes.push(dateTime);
                if (slotArray.indexOf(dateTime) == -1) slotArray.push(data.jsonOpenSlotMap[i]);
                if (availableDates.indexOf(availDate) == -1) availableDates.push(availDate.replace("/0", "/"))
            }
            createCalendar(id, slotArray);
            var totalTime = (new Date).getTime() - ajaxTime;
            procEvent("appointmentSlotRetrievalStep3", "Step3 Appointment Retrieval Success",
                "loadTime:" + totalTime)
        },
        error: function (data) {
            createCalendar(id, null)
        }
    })
}

function createCalendar(id, slotArray) {
    $(document).trigger("SetNewSchedulerDates", [slotArray]);
    var hideElement = "hidden";
    if (_MATERIALIZE) hideElement = "hide";
    $("#" + id + " .processing").addClass(hideElement);
    $("#" + id + " .defaultslot").prop("checked", true);
    $("#" + id + " .default-time").click(function () {
        $("#" + id + " .timeslothidden").val($("#" + id + " > .default-time").val())
    });
    $("#" + id + " .choose-different").click(function () {
        $("#" + id + " .defaultslot").prop("checked", false)
    });
    $("#" + id + "Modal" + " #cancelButton").click(function () {
        $("#" +
            id + " .defaultslot").prop("checked", true);
        $("#" + id + " .choose-different").prop("checked", false);
        $("#" + id + " .alternateDateTime").addClass(hideElement)
    });
    if (slotArray != null && slotArray.length > 0) {
        $("#" + id + " .slotSelect").removeClass(hideElement);
        var firstDate;
        firstDate = slotArray[0];
        if (firstDate) {
            $("#" + id + " .defaultSlot").append(firstDate.schDate + " " + timeScanner(firstDate.schFrmTime) + " to " + " " + timeScanner(firstDate.schToTime));
            $("#" + id + " .default-time").val(firstDate.schSlotKey)
        }
        if (_MATERIALIZE) $("#" + id +
            "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + 'timeslotradio\' onClick=\'updateSelectedTime("None of the dates work for me","' + id + "\")' class='customslot' id='timeslotnoappointment' value=''><h5 class='materialRadioFix sbold'> None of the dates work for me.</h5></label></div>");
        else $("#" + id + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + id + 'timeslotradio\' onClick=\'updateSelectedTime("None of the dates work for me","' + id + "\")' class='customslot' id='timeslotnoappointment' value=''><h5 class=' sbold'> None of the dates work for me.</h5></label></div>");
        $(".defaultslot").click(function () {
            $("#" + id + " .customslot").prop("checked", false);
            $("#" + id + " .choose-different").prop("checked", false);
            $("#" + id + " .alternateDateTime").addClass(hideElement)
        });
        var $datepicker = $("#" + id + "Calendar");
        if ($datepicker.length) $datepicker.datepicker({
            beforeShowDay: function (date) {
                return checkAvailableDates(date, slotArray)
            },
            nextText: "",
            prevText: "",
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            onSelect: function (date) {
                checkOpenTimeSlots(date, slotArray, id)
            }
        });
        selectDefaultTimeSlot(id);
        $(".ui-state-active").removeClass("ui-state-active")
    } else {
        $("#" + id + " .noAppointments").removeClass(hideElement);
        $("#" + id + " .noDateDisplay").show();
        $("#requestInspec").removeClass(hideElement);
        $(".requestInspection").removeClass("submitting").removeAttr("disabled").text("Request Inspection")
    }
    $("#" + id + "Calendar .selectable").first().click()
}

function selectDefaultTimeSlot(id) {
    $("#" + id + " > .defaultSlot").click();
    $("#" + id + " > .choose-different").prop("checked", false)
}

function handleSubmitOrder() {
    var validated = true;
    $("#paymentGroupForm" + " :input").each(function () {
        if (!isValid($(this))) {
            validated = false;
            $("#cartCheckout").removeAttr("disabled")
        }
    });
    if (validated)
        if (_QAS_FEATURE_TOGGLE) QAS_EMAIL_VALIDATE();
        else CompleteOrderSubmit();
    else;
}

function CompleteOrderSubmit() {
    if (typeof utag_data !== "undefined") utag_data.ga_action = $("#ccType").val();
    if (!($(".serviceScheduler").length > 0)) {
        if ($("#serviceScheduler").length > 0) $("#serviceScheduler .timeslothidden").val($("input:radio[name=serviceSchedulertimeslotradio]:checked").val());
        if ($("#salesScheduler").length > 0) $("#salesScheduler .timeslothidden").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val())
    }
    OpenModal("processing-modal");
    addPaymentGroup()
}

function handleFreeInspection() {
    var $requestInspection = $(".requestInspection"),
        $submitError = $(".submitError");
    if ($requestInspection.length) {
        $submitError.hide();
        if (!$(".timeslothidden").val()) {
            $submitError.show();
            return false
        }
        $requestInspection.addClass("submitting").attr("disabled", true).text("Processing...")
    } else {
        $("#requestInspec").addClass("disabled");
        if ($("#salesScheduler").length > 0) $("#salesScheduler .timeslothidden").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val())
    }
    QAS_EMAIL_CONFIG.AFTER_VERIFY =
        function () {
            OpenModal("processing-modal");
            submitAjaxForm("paymentGroupForm", submitInspectionShippingForm)
        };
    QAS_EMAIL_VALIDATE()
}

function submitInspectionShippingForm() {
    submitAjaxForm("shippingGroupForm", submitInspectionForm)
}

function submitInspectionForm() {
    $("#submitSchedule").submit()
}

function handleGetFreeQuote(pesttype) {
    var validated = true;
    $("#firstName").removeAttr("required");
    $("#lastName").removeAttr("required");
    $("#phone").removeAttr("required");
    $("#consent").removeAttr("required");
    $("#addressForm :input").each(function () {
        if (!isValid($(this))) validated = false
    });
    $("#city_hidden, #state_hidden").each(function () {
        if (!isValid($(this))) $(".hiddenInputs").removeClass("hidden")
    });
    if (validated) $("#addressForm").submit();
    else;
}

function processAddressValidationResult(data) {
    if (data.code == "suggestion") OpenModal("addressSelectionModal");
    else if (data.code == "error") OpenModal("addressSelectionModal")
}

function setAddressValidationForm() {
    $("#address1Validation_hidden").val($("#address1").val());
    $("#cityValidation_hidden").val($("#city_hidden").val());
    $("#stateValidation_hidden").val($("#state_hidden").val());
    $("#postalCodeValidation_hidden").val($("#zipCode").val())
}

function addShippingGroup() {
    return submitAjaxForm("shippingGroupForm", addShippingGroupResponse)
}

function addShippingGroupResponse(data) {
    $("#submitOrder").submit()
}

function addPaymentGroup() {
    return submitAjaxForm("paymentGroupForm", addPaymentGroupResponse)
}

function addPaymentGroupResponse(data) {
    if (data && data.code == "error") CloseModal("processing-modal");
    else addShippingGroup()
}

function linkToUrl(url) {
    window.location.href = url
}

function populateProductCodeAndPestType(productCodeAndPestType) {
    $("#productCode_hidden, #targetPest_hidden").val(productCodeAndPestType)
}

function planSelect(elem, sku, product, form, prodcode) {
    $("#itemSkuIdHid").val(sku);
    $("#itemProdIdHid").val(product);
    $("#heading" + prodcode).addClass("productSelected");
    $("#cartTotals").addClass("hidden");
    $("#cartTotals").addClass("hide");
    $("#cartHolder").addClass("hidden");
    $("#cartHolder").addClass("hide");
    $("#cartProcessing").removeClass("hidden");
    $("#cartProcessing").removeClass("hide");
    submitAjaxForm(form, refreshCartAddition);
    return false
}

function refreshCartAddition(data) {
    if (data.code == "success") {
        if (_MATERIALIZE) var cart = "https://www.terminix.com/includes/materialcart2-select.jsp";
        else var cart = "https://www.terminix.com/includes/cart.jsp";
        $("#cartHolder").load(cart, function () {
            $("#cartProcessing").addClass("hidden");
            $("#cartProcessing").addClass("hide");
            $("#cartHolder").removeClass("hidden");
            $("#cartHolder").removeClass("hide");
            $(".cartRemovebutton").click(function (e) {
                e.stopPropagation();
                var product = $(this).attr("name");
                $("#heading" + product + " > .buytoggle.removeButton").toggleClass("disabled")
            });
            if ($("#totalAmountWithTax").text() != "") $("#mobileTotal").text($("#totalAmountWithTax").text());
            else $("#mobileTotal").text("$0.00");
            productInCart($(".product"));
            refreshButtons();
            if (_MATERIALIZE) {
                $(".modal-trigger").modal();
                $("#modalMonthlyPay").modal()
            }
        })
    } else submitAjaxForm("addToCartForm", refreshCartFinal)
}

function refreshCartFinal(data) {
    if (_MATERIALIZE) var cart = "https://www.terminix.com/includes/materialcart2-select.jsp";
    else var cart = "https://www.terminix.com/includes/cart.jsp";
    $("#cartHolder").load(cart, function () {
        $("#cartProcessing").addClass("hidden");
        $("#cartProcessing").addClass("hide");
        $("#cartHolder").removeClass("hidden");
        $("#cartHolder").removeClass("hide");
        if ($("#totalAmountWithTax").text() != "") $("#mobileTotal").text($("#totalAmountWithTax").text());
        else $("#mobileTotal").text("$0.00");
        productInCart($(".product"));
        refreshButtons();
        if (_MATERIALIZE) {
            $(".modal-trigger").modal();
            $("#modalMonthlyPay").modal()
        }
    })
}

function refreshButtons() {
    $(".buyToggle.disabled").each(function () {
        $(this).removeClass("disabled");
        $(this).addClass("hidden");
        $(this).siblings(".buyToggle").removeClass("hidden")
    })
}

function doNothing(data) {}

function cartCheckoutSubmit() {
    $("#cartCheckout").attr("disabled", "true");
    if ($(".serviceScheduler").length > 0) {
        var slotkey = $(".timeslothidden").val();
        if (!slotkey) {
            $("#cartCheckout").removeAttr("disabled");
            return
        }
    }
    if ($("#serviceScheduler").length > 0) $("#finalSlotSelected").val($("input:radio[name=serviceSchedulertimeslotradio]:checked").val());
    if ($("#salesScheduler").length > 0) $("#finalSlotSelected").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val());
    if ($("#hidden_form1").length) {
        if (typeof utag_data !==
            "undefined") {
            var position = $("#easyPay_hidden").val() == "true" ? 0 : 1;
            utag_data.product_ids = [utag_data.imp_ids[position]];
            utag_data.product_names = [utag_data.imp_names[position]];
            utag_data.product_categories = [utag_data.imp_categories[position]];
            utag_data.product_variants = [utag_data.imp_variants[position]];
            utag_data.product_prices = [utag_data.imp_prices[position]]
        }
        OpenModal("processing-modal");
        $("#hidden_form1").submit()
    } else if ($("#submitOrder").length) handleSubmitOrder()
}

function updateAddress() {
    if ($("#homeSize_hidden").val().length <= 0) $(".propertySelect option").each(function () {
        if (this.selected) $("#homeSize_hidden").val($(this).val())
    });
    if (typeof utag_data !== "undefined") {
        utag_data.customer_address = $("#address1").val() + " " + $("#address2").val();
        utag_data.customer_zip = $("#zipCode").val();
        utag_data.customer_property_option = $("#homeSize_hidden").val();
        utag_data.customer_type = "online"
    }
    procEvent("ecommerce", "address change success");
    $("#editModal").modal("hide");
    OpenModal("processing-modal");
    submitAjaxForm("updateAddressForm", doNothing);
    return true
}

function removeItemFromCartByRefId(productSku, productCode) {
    $("#removalCommerceItemRefIdHid").val(productSku);
    $("#cartHolder").addClass("hidden");
    $("#cartHolder").addClass("hide");
    $("#cartProcessing").removeClass("hidden");
    $("#cartProcessing").removeClass("hide");
    $("#cartTotals").addClass("hidden");
    $("#cartTotals").addClass("hide");
    $("#heading" + productCode).removeClass("productSelected");
    submitAjaxForm("removeFromCartFormByRefId", refreshCartFinal)
}

function removeItemFromCart(commerceId, productSku, productCode) {
    $("#addItem" + productCode).toggleClass("hide");
    $("#maddItem" + productCode).toggleClass("hide");
    $("#removeItem" + productCode).toggleClass("hide");
    $("#mremoveItem" + productCode).toggleClass("hide");
    $("#removeFromCartForm > #commerceItemIdHid").val(commerceId);
    $("input[value='" + productSku + "']").prop("checked", false);
    $("#heading" + productCode).removeClass("productSelected");
    $("#cartHolder").addClass("hidden");
    $("#cartHolder").addClass("hide");
    $("#cartAccordion").addClass("hidden");
    $("#cartTotals").addClass("hidden");
    $("#cartProcessing").removeClass("hidden");
    $("#cartProcessing").removeClass("hide");
    submitAjaxForm("removeFromCartForm", refreshCartFinal)
}

function productInCart(products) {
    if (products.length > 0) $("#sendQuoteButton").removeClass("disabled");
    else $("#sendQuoteButton").addClass("disabled");
    for (var i = 0; i < products.length; i++) {
        var addButton = "addItem" + $(products[i]).attr("id");
        var removeButton = "removeItem" + $(products[i]).attr("id");
        if ($("#cartHolder").children().has($(products[i]).attr("id"))) {
            $("#" + addButton + ", #m" + addButton).addClass("hide");
            $("#" + removeButton + ", #m" + removeButton).removeClass("hide")
        } else {
            $("#" + removeButton + ", #m" + removeButton).addClass("hide");
            $("#" + addButton + ", #m" + addButton).removeClass("hide")
        }
    }
}

function displayMosqSeason(tempCode) {
    var mosqSeason = {
        "PC192": "Jan|Dec|12",
        "PC193": "Mar|Oct|8",
        "PC194": "April|Sep|6",
        "PC195": "April|Oct|7",
        "PC196": "April|Nov|8",
        "PC197": "May|Sep|5",
        "PC198": "May|Oct|6",
        "PC307": "Jan|Dec|12",
        "PC310": "Mar|Oct|8",
        "PC308": "April|Oct|7",
        "PC314": "May|Oct|6"
    };
    for (var code in mosqSeason)
        if (code == tempCode) {
            var seasonInfo = mosqSeason[code].split("|");
            $("#mosqSeason").text("Your Mosquito season Starts in " + seasonInfo[0] + " and Ends in " + seasonInfo[1] + ".");
            $("#mosqSeasonBack").text("For this fee, you will receive monthly treatments from " +
                seasonInfo[0] + " to " + seasonInfo[1] + ", the length of mosquito season for your area.");
            $("#mosqPay").text(seasonInfo[2] + " payments in total")
        }
}

function validationPassword() {
    var password = $("#passwordMyAcccount");
    var regexPatterns = [new RegExp("[a-zA-Z0-9]{7}"), new RegExp("[A-Z]"), new RegExp("[a-z]"), new RegExp("[0-9]")];
    var checkMarks = [$("#passLength"), $("#passUpper"), $("#passLower"), $("#passNumber")];
    var i;
    for (i = 0; i < regexPatterns.length; i++)
        if (password.val().match(regexPatterns[i])) {
            checkMarks[i].removeClass("gray");
            checkMarks[i].addClass("green")
        } else checkMarks[i].addClass("gray")
}

function turnButtonsOn() {
    $("button").each(function () {
        $(this).attr("disabled", false);
        $(this).removeClass("disabled")
    })
}

function changePaymentType(from, to) {
    if (_MATERIALIZE) {
        $("." + to).removeClass("hide");
        $("." + from).addClass("hide")
    } else {
        $("." + to).removeClass("hidden");
        $("." + from).addClass("hidden")
    }
    $("." + from + " input, ." + from + " select").removeAttr("required");
    $("." + to + " input, ." + to + " select").prop("required", true)
}
$(window).on("load", function () {
    turnButtonsOn();
    productInCart($(".product"));
    $(window).trigger("scroll");
    var errored = false;
    var errorMessage = "";
    if ($("#error").length > 0) {
        errorMessage = $("#error").find("li").text();
        errored = true
    }
    if (window.location.href.indexOf("buyonline/step-one") > -1) {
        if (errored) procEvent("ecommerce", "address form error", errorMessage);
        if ($("#Consent").length > 0) {
            $("#stepOneButton").addClass("disabled");
            $("#Consent").change(function () {
                if (this.checked) $("#stepOneButton").removeClass("disabled");
                else $("#stepOneButton").addClass("disabled")
            })
        }
    }
    if ($("#serviceScheduler").length > 0) getSlotsForCalendar(undefined, undefined, "serviceScheduler");
    if ($("#salesScheduler").length > 0) getSlotsForCalendar("10001", "sales", "salesScheduler");
    if ($("#preSchedule").length > 0) getSlotsForAppointment(undefined, undefined, "");
    if (window.location.href.indexOf("buyonline/step-two") > -1 || window.location.href.indexOf("buyonline/step-2") > -1)
        if (errored) procEvent("ecommerce", "add to cart error", errorMessage);
        else procEvent("ecommerce",
            "address form success", "online purchase");
    if (window.location.href.indexOf("buyonline/step-three") > -1 || window.location.href.indexOf("buyonline/step-3") > -1)
        if (errored) procEvent("ecommerce", "submit billing info error", errorMessage);
        else procEvent("ecommerce", "add to cart success");
    if (window.location.pathname.replace(/\//g, "") == "free-inspection")
        if (errored) procEvent("Termite Inspection Funnel", "Enter Address Page", errorMessage);
    if (window.location.href.indexOf("free-inspection/step-two") > -1) {
        submitAjaxForm("addToCartForm",
            doNothing);
        if (errored) procEvent("Termite Inspection Funnel", "Schedule Appointment Page", errorMessage);
        else procEvent("Termite Inspection Funnel", "Enter Address Page", "address form completed")
    }
    if (window.location.href.indexOf("free-inspection/step-three") > -1) procEvent("Termite Inspection Funnel", "Confirmation Page", utag_data.order_contract_ids[0]);
    if (window.location.href.indexOf("buyonline/step-four") > -1 || window.location.href.indexOf("buyonline/step-4") > -1) {
        procEvent("ecommerce", "submit billing info success");
        procEvent("Pest Funnel", "Pest Test", utag_data.customer_id)
    }
});
$("#addItemGENPEST, #removeItemGENPEST").click(function () {
    $("#addItemGENPEST, #removeItemGENPEST").toggleClass("hidden");
    $("#addItemGENPEST, #removeItemGENPEST").toggleClass("hide")
});
$("#removeItemMOSQ, #addItemMOSQ").click(function () {
    $("#addItemMOSQ, #removeItemMOSQ").toggleClass("hidden");
    $("#addItemMOSQ, #removeItemMOSQ").toggleClass("hide")
});
$("#maddItemGENPEST, #mremoveItemGENPEST").click(function () {
    $("#maddItemGENPEST, #mremoveItemGENPEST").toggleClass("hidden");
    $("#maddItemGENPEST, #mremoveItemGENPEST").toggleClass("hide")
});
$("#mremoveItemMOSQ, #maddItemMOSQ").click(function () {
    $("#maddItemMOSQ, #mremoveItemMOSQ").toggleClass("hidden");
    $("#maddItemMOSQ, #mremoveItemMOSQ").toggleClass("hide")
});

function iniTesting() {
    $(".ini-hidden").removeClass("ini-hidden");
    $(".ini").addClass("ini-hidden")
}
$(window).scroll(function () {
    if ($(window).width() < 540) {
        var main = $("#stickyHeader").outerHeight(true);
        if ($(window).scrollTop() >= main + $("#stickyHeader").scrollTop()) $("#stickyHeader").css({
            "position": "fixed",
            "z-index": "100",
            "top": "0px",
            "width": "inherit",
            "background-color": "white"
        });
        else $("#stickyHeader").css({
            "position": "inherit",
            "z-index": "1",
            "width": "initial"
        })
    }
});
$(document).ready(function () {
    if ($("#confirm").length) initSession()
});
var sessionPingInt = 6E4;
var sessionExpirationMinutes = 30;
var sessionTimeoutVar = 9E5;
var sessionIntervalID;
var sessionLastActivity;

function initSession() {
    sessionLastActivity = new Date;
    sessionSetInterval();
    $(document).keypress(function (e) {
        sessionKeyPress(e)
    })
}

function resetAll() {
    if (typeof sessionIntervalID !== "undefined") clearInterval(sessionIntervalID);
    if (typeof countDownInterval !== "undefined") clearInterval(countDownInterval);
    sessionLastActivity = new Date;
    sessionSetInterval();
    CloseModal("confirm")
}

function continueSession() {
    var pingUrl = "https://www.terminix.com/includes/ping.jsp";
    $.post(pingUrl);
    resetAll();
    sessionSetInterval()
}

function sessionSetInterval() {
    sessionIntervalID = setInterval("sesionInterval()", 18E5 - 6E4);
    sessionInterval = setInterval("sesionIntervalEnd()", 18E5)
}

function sessionKeyPress(e) {
    resetAll()
}

function sessionExp() {
    CloseModal("confirm");
    $("form :button").each(function () {
        $(this).attr("disabled", true)
    });
    window.location.href = "/buyonline/step-one"
}

function sesionIntervalEnd() {
    var now = new Date;
    var diff = now - sessionLastActivity;
    var diffMin = diff / 1E3 / 60;
    if (diffMin >= sessionExpirationMinutes) {
        procEvent("SessionConfirmation", "Session Expired", "From " + window.location.pathname);
        sessionExp()
    }
}

function sesionInterval() {
    countDown();
    OpenModal("confirm");
    procEvent("SessionConfirmation", "Session dieing in 1 min", "From " + window.location.pathname)
}
$(".yes").click(function () {
    continueSession()
});

function countDown() {
    var countDownTime = 1 * 60;
    var tickDuration = 1;
    countDownInterval = setInterval(function () {
        countDownTime = countDownTime - tickDuration;
        $("#countDownTime").html(countDownTime);
        if (countDownTime <= 0) {
            clearInterval(countDownInterval);
            $("#countDownTime").html("");
            return
        }
    }, 1E3)
}
document.addEventListener("DOMContentLoaded", function (event) {
    var ini = document.getElementById("easyPaySelected");
    if (!document.getElementsByClassName("productSelected").length && ini !== null) ini.click()
});

function getCustomerDetails() {
    var input = {
        "customerNumber": $("#customerNumber").val(),
        "phone": $("#phoneTwin").val()
    };
    var postObj = addProcessor("CustLookupProcessor", input);
    CloseModal("custLookupModal");
    OpenModal("processing-modal");
    $.ajax({
        type: "POST",
        url: "/admin/dwr/jsonp/TmxGenericProcessor/startProcess?jsonData=" + JSON.stringify(postObj),
        dataType: "json",
        success: function (data) {
            var json = JSON.parse(data.reply).PROCESSORARRAY[0].PROCESSOROUTPUT;
            if (json.type != undefined && json.type == "BSN") {
                window.location =
                    "/request-commercial-quote/";
                return
            }
            CloseModal("processing-modal");
            if (json.address1 != undefined) {
                $("#currentAddress").attr("data-address", JSON.stringify(json));
                var addStr = json.address1 + " " + json.city + ", " + json.state + " " + json.zip;
                $("label[for='currentAddress']").text(addStr);
                OpenModal("custDetailsModal")
            } else {
                OpenModal("custDetailsFailModal");
                clearCustomerInfo()
            }
        },
        error: function (data) {
            console.log(data)
        }
    })
}

function pushCustomerDetails() {
    CloseModal("custDetailsModal");
    if ($("#currentAddress").is(":checked")) {
        var json = JSON.parse($("#currentAddress").attr("data-address"));
        $("#address1").val(json.address1).focus();
        $("#city_hidden").val(json.city);
        $("#state_hidden").val(json.state);
        $("#zipCode").val(json.zip).focus()
    }
}

function clearCustomerInfo() {
    $("#customerNumber").val("");
    $("#phoneTwin").val("").trigger("change");
    $("#existingCustomer").attr("checked", false)
}

function addProcessor(name, input, obj) {
    if (obj == null || obj == undefined) obj = {};
    if (obj.PROCESSORARRAY == undefined) obj.PROCESSORARRAY = [];
    var obj2 = {};
    obj2.PROCESSOR = name;
    obj2.PROCESSORINPUT = input;
    obj.PROCESSORARRAY.push(obj2);
    return obj
}
$(".twinInput").change(function () {
    var tId = $(this).data("twin");
    $("#" + tId).val($(this).val())
});
