function ApplyMasks()
{
	$('[name="zipcode"]').mask('00000');
	$('.mask-phone').mask('(000) 000-0000');
	$('[name="contractNumber"]').mask('0#');
}

ko.bindingHandlers.masked = (function()
{
	var EvaluateMask = function(element, allBindings)
	{
		var mask = {
			format: '',
			options: {}
		};
		
		var preset = allBindings().maskedPreset;
		
		switch (preset)
		{
		case 'phone':
			mask.format = '(000) 000-0000';
			break;
		case 'phone-display':
			mask.format = '000.000.0000';
			break;
		case 'zipcode':
			mask.format = '00000';
			break;
		case 'date':
			mask.format = '00/00/0000';
			mask.options = { placeholder: 'MM/DD/YYYY' };
			break;
		case 'card-number':
			mask.format = '0000000000000000';
			break;
		case 'card-security':
			mask.format = '0000';
			break;
		case 'alpha':
			mask.format = 'a';
			mask.options = {
			translation: { 
				'a': {
					pattern: /[A-Za-z]+/,
					recursive: true
				}
			}
			};
			break;
		case 'nodigit':
			mask.format = 'a';
			mask.options = {
			translation: { 
				'a': {
					pattern: /^\D*$/,
					recursive: true
				}
			}
			};
			break;
		}
		
		mask.format = allBindings().maskedFormat || mask.format;
		$.extend(mask.options, allBindings().maskedOptions);
		
		return mask;
	};
	
	var DetectElementType = function(element)
	{
		return $(element).is('input, textarea') ? 'value' : 'text';
	};
	
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext) 
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var mask = EvaluateMask(element, allBindings);
			
			if (elementType == 'value')
			{
				var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
				
				ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings, viewModel, bindingContext);
				
				// Apply the mask
				$(element).mask(mask.format, mask.options);
			}
			else
			{
				var $element = $(element).text(valueAccessor()());
				
				$element.mask(mask.format, mask.options);
			}
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext) 
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var mask = EvaluateMask(element, allBindings);
			
			if (elementType == 'value')
			{
				var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
				
				ko.bindingHandlers.value.update(element, valueAccessor, newAllBindings, viewModel, bindingContext);
				
				// Apply the mask
				$(element).mask(mask.format, mask.options);

				valueAccessor()($(element).val());
			}
			else
			{
				var masked = $(element).masked(valueAccessor()());
				
				$(element).text(masked);
			}
		}
	};
})();

ko.bindingHandlers.numeric = (function()
{
	var EvaluateMask = function(element, allBindings)
	{
		var options = $.extend({
			decimal: '.',
			negative: false,
			decimalPlaces: 2
		}, allBindings().numericOptions);
		
		// Apply the mask
		$(element).removeNumeric().numeric(options);
	};
	
	var DetectElementType = function(element)
	{
		return $(element).is('input, textarea') ? 'value' : 'text';
	};
	 
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext) 
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
			
			ko.bindingHandlers[elementType].init(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			
			// Apply the mask
			EvaluateMask(element, allBindings);
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext) 
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
			
			ko.bindingHandlers[elementType].update(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			
			// Apply the mask
			EvaluateMask(element, allBindings);
			
			// Update the value if it is an input type
			if (elementType == 'value')
			{
				valueAccessor()($(element).val());
			}
		}
	};
})();