$(document).ready(function() {

	$('.animPlaceholder').each(function() {
		var parentHeight = $(this).parent().height();
		$(this).height(parentHeight);
	});
	try {
		$('[data-toggle="tooltip"]').tooltip();
	} catch (err) {
	}
	heightNormalization('#quoteCarousel', '.item');
	heightNormalization('.normalizedCarousel', '.item');

	document.oncontextmenu = function(e) {
		e = e || window.event;
		if (/^img$/i.test((e.target || e.srcElement).nodeName))
			return false;
	};

	if ((_LESS_THAN_IE9) && (location.pathname.indexOf('browser-upgrade-notification') < 0))
	{
		window.location.assign("/browser-upgrade-notification");
	}

	// set variable to the pest based on the page in order to lead user to click
	// point form
	// (/request-quote)
	if ($("#pestChoice").length) {
		$("#pestChoice").val($("#pest").text());
	}

	if ($("#pest").text() != "") {
		$("#pestDetails").html(function() {
			return $(this).html().replace(new RegExp("Pests", "ig"), $("#pest").text().charAt(0) + $("#pest").text().substr(1).toLowerCase());
		});
	}

	function validateFields(fields) {
		valid = true;
		fields.each(function() {
			if (!validateField($(this))) {
				valid = false;
			}
		});
		if (valid === false) {
			procEvent(fields.first().closest('form').attr('id') + " User Error");
		}
		return valid;
	}
	function validateField(field) {
		if (!field.prop('disabled')) {
			if (field.prop('pattern') && !field.val().match(field.prop('pattern'))) {
				if (field.prop('required')) {
					field.addClass('invalid');
					return false;
				} else if (field.val() != "") {
					field.addClass('invalid');
					return false;
				}
			} else if (field.attr('type') === 'radio') {
				if (!$("input[name=" + field.attr('name') + "]:checked").val()) {
					field.addClass('invalid');
					return false;
				}
			} else if (field.attr('type') === 'checkbox') {
				if (!field.prop("checked")) {
					field.addClass('invalid');
					return false;
				}
			} else if (field.is('select')) {
				if ($('#' + field.attr('id') + " option:selected").prop('disabled')) {
					field.addClass('invalid');
					return false;
				}
			} else if (field.is('textarea') && !field.val()) {
				field.addClass('invalid');

				return false;
			} else {
				field.removeClass('invalid');
			}
		}
		return true;
	}
	function resetForm($form) {
		/*$form.find('input:text, input[type="tel"],input[type="email"], input:password, input:file, select, textarea').val('');
		$form.find('input:radio, input:checkbox').prop('checked', false).prop('selected', false);*/
		$form[0].reset();
	}

	$('#page-1').click(function() {
		if (validateFields($('#firstName,#lastName,#phone,#zipCode,input[name="Brand"]'))) {
			$(this).closest('form').toggleClass("trigger");
		}
	});

	$("#phone,.phone,#phone-contact").keypress(function() {
		if ($(this).val().length == 3 || $(this).val().length == 7) {
			$(this).val($(this).val() + '-');
		}
	});

	$('input, .radio-holder label, select, textarea').focus(function() {
		$(this).removeClass('invalid');
		$("#" + $(this).attr('for')).siblings().removeClass('invalid');
	});
	$('input,textarea').blur(function() {
		validateField($(this));
	});

	$('.radio-holder label').click(function() {
		$('#InterestArea').removeProp('disabled');
		$("#" + $(this).attr('for')).prop("checked", true);
		$('#InterestArea option[value=""]').attr("selected", true);
		$('#InterestArea option, #company').hide();
		$('#InterestArea option, #company').prop('disabled', true);
		$("." + $(this).attr('for')).show();
		$("." + $(this).attr('for')).removeProp('disabled');
	});

	$('.submit-button').click(function(e) {
		if (validateFields($('#' + $(this).closest('form').attr('id') + ' :input'))) {
			$('#' + $(this).closest('form').attr('id')).submit();
		}
	});
	$('.form-cancel').click(function() {
		$(this).closest('form').removeClass("trigger");
		procEvent($(this).closest('form').attr('id') + " Cancel");
		resetForm($('#' + $(this).closest('form').attr('id')));
	});
	$('#landingId').val(window.location);
	$('#free-estimate-form, #free-estimate-form-mod,#contact-form,.modal-submit-form').submit(function() {
		var form = $(this);
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		$('#processing-modal').modal('show');
		$.ajax({
			url : formURL,
			type : "POST",
			data : postData,
			success : function(data, responsecode, XHR) {
				form.removeClass('trigger');
				resetForm(form);
				if (data.leadApiResponse != undefined) {
					ga('create', utag.sender[15].data.account[0], 'auto');
					ga('send', 'event', '_setLeadId', 'sucessfullySet', {
						'dimension4' : data.leadApiResponse.leadApiId
					});
				}
				$('#processing-modal').modal('hide');
				$('#thank-you-modal').modal('show');
				procEvent("Free Estimate Submit");
			},
			error : function(data, responsecode, XHR) {
				$('#processing-modal').modal('hide');
				$('#try-again-modal').modal('show');
				procEvent("Free Estimate Trans Error");
			}
		})
		return false;
	});
	$('#LandingID').val(window.location);
	(function myLoop(i) {
		setTimeout(function() {
			var consentText = $('#consentText').text();
			if (consentText != '' && consentText != null) {
				$('.consentText').empty();
				$('.consentText').text(consentText);
			} else if (--i) {
				myLoop(i);
			}
		}, 500)
	})(10);

	function processZipData(data) {
		$("#city").val(data.city);
		$('#State option[value="' + data.state + '"]').prop('selected', true);
		$('#State option[value="' + data.state_short + '"]').prop('selected', true);
	}
	;
	$('.buy-button').click(function(e) {
		var rect = e.target.getBoundingClientRect();
		if (typeof utag_data !== 'undefined') {
			utag_data.ga_event_label = rect.top + ", " + rect.left;
		}
		procEvent('ecommerce', 'click buy button');
	});

	$('.arrow-up, .clickover-left, .clickover-right').click(function() {
		$(".arrow-up").toggleClass('rotate');
		$('.sticky-footer').toggleClass("sf-shrunk");
		$('.fs-info').toggleClass("hidden");
	});
	$('.zipCodeSearch').click(function(e) {
		window.location.href = '/exterminators/search?q=' + $('#find-a-location-home').val();
	});
});

$(window).on('load', function()
{
	/*
	$('.AB_EVENT').each(function() {
		ga('create', utag.sender[15].data.account[0], 'auto');
		ga('send', 'event', '_setABTest', 'sucessfullySet', {
			'dimension2' : $(this).attr('id')
		});
	});
	*/
	$('.animPlaceholder').each(function() {
		var parentHeight = $(this).parent().height();
		$(this).height(parentHeight);
	});
});

// re-populate InterestArea field when it is emptied due to reload of page
$("#pestSelect").change(function() {
	$("#InterestArea").val($("#pestSelect").val());
});

$("#hoverCustomerNumb, #questionCustomernumber").hover(function() {
	$("#questionCustomernumber").show();
}, function() {
	$("#questionCustomernumber").hide();
})

function heightNormalization(parentId, child) {
	var items = $(parentId + ' ' + child), heights = [], tallest;

	if (items.length) {
		function normalizeHeights() {
			tallest = 0, heights.length = 0; // reset vars
			items.each(function() {
				$(this).css('min-height', '0'); // reset min-height
			});
			items.each(function() { // add heights to array
				heights.push($(this).innerHeight());
			});
			tallest = Math.max.apply(null, heights); // cache largest value
			items.each(function() {
				$(this).css('min-height', tallest + 'px');
			});
		}
		normalizeHeights();
		
		$(window).on('load', function()
		{
			normalizeHeights();
		});
		
		$(window).on('resize', function() {
			normalizeHeights(); // run it again
		});
		$(window).on('orientationchange', function() {
			normalizeHeights(); // run it again
		});
	}
}

function submitReferral() {
	$("#referalForm").ajaxForm({
		datatype : 'json',
		success : function(data) {
			if (data.error) {
				/*
				 * $("#referral-error").empty();
				 * $("#referral-error").append(data.error);
				 */
				$("#processing-modal").modal('hide');
				$("#referral-sorry-modal").modal('show');
			} else {
				$("#referralCode, #referral-error").empty();
				$("#referralCode").append(data.referralCode);
				$("#processing-modal").modal('hide');
				$("#referral-thank-you-modal").modal('show');
			}
		},
		error : function(data) {
			$("#processing-modal").modal('hide');
			$("#referral-sorry-modal").modal('show');
		}
	});
	if ($("#referalForm")[0].checkValidity()) {
		$("#processing-modal").modal('show');
		$("#referalForm").submit();
	}

}
