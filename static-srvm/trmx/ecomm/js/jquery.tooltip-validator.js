function TooltipValidator($container, options)
{
	var api = this;
	
	api.options = $.extend(
	{
		headerText: '', // Text above the validation requirements
		showCapsLock: false, // Shows a warning about Caps Lock being on
		allowEmptyRules: false, // Allows the rules returned from onValidate to be empty
		onValidate: function(val) {}, // return object of rules
		onAfterValidate: function(valid) {} // when validation is done, calls whether it is valid or not
		
	}, options);
	
	if (!$.isFunction(api.options.onValidate))
	{
		api.options.onValidate = function() { return false; };
	}
	
	if (!$.isFunction(api.options.onAfterValidate))
	{
		api.options.onAfterValidate = $.noop;
	}
	
	api.$container = $container;
	
	if (api.options.containerClass)
	{
		api.$container.addClass(api.options.containerClass);
	}
	
	api.$tooltip = $container.find('.field-validate-tooltip');
	
	if (!api.$tooltip.length)
	{
		api.$tooltip = $('<div class="field-validate-tooltip"></div>').appendTo(api.$container);
	}
	
	api.isCapsLockOn = false;
	
	api.GetValue = function()
	{
		var $input = api.$container.find('input:focus');
		
		if (!$input.length)
		{
			$input = api.$container.find('input:not(.masked)');
			
			if (!$input.length)
			{
				$input = api.$container.find('input');
				
				if (!$input.length)
				{
					return '';
				}
			}
		}
		
		if ($input.hasClass('masked'))
		{
			var $before = $input.prev();
			
			if ($before.length)
			{
				return $before.val();
			}
		}
		
		return $input.val();
	};
	
	api.UpdateValidation = function(value)
	{
		var rules = api.options.onValidate.call(api, value);
		var $content = $();
		var valid = true;
		
		if (typeof rules == 'object')
		{
			if (rules.items && rules.items.length)
			{
				var $items = $('<ul class="field-validate-items"></ul>');
				
				rules.items.forEach(function(item)
				{
					var textOrHtml = item.html ? 'html' : 'text';
					var content = (textOrHtml in item) ? item[textOrHtml] : '';
					
					$items.append($('<li></li>').addClass(item.passed ? 'valid' : 'invalid')[textOrHtml](content));
					
					if (!item.passed)
					{
						valid = false;
					}
				});
				
				$content = $content.add($items);
			}
			
			if (rules.description)
			{
				$content = $content.add($('<p class="field-validate-desc"></p>').html(rules.description));
			}
		}
		
		if (!$content.length && !api.options.allowEmptyRules)
		{
			valid = false;
			$content = $('<p class="field-validate-desc field-validate-error">Invalid validation rules response.</p>');
		}
		
		if (api.options.headerText)
		{
			$content = $('<p class="field-validate-header"></p>').html(api.options.headerText).add($content);
		}
		
		if (api.isCapsLockOn && api.options.showCapsLock)
		{
			$content = $content.add($('<p class="field-validate-desc field-validate-warning"></p>').text('Your Caps Lock is on.'));
		}
		
		api.$tooltip.empty().append($content);
		
		api.options.onAfterValidate(valid);
	};
	
	api.$container.off('.tooltipValidator');
	
	api.$container.on('blur.tooltipValidator', 'input', function(e)
	{
		// Delay hiding the tooltip so it doesn't interfere with events that make the input blur
		setTimeout(function() { api.$tooltip.hide(); }, 100);
	});
	
	api.$container.on('focus.tooltipValidator', 'input', function(e)
	{
		// Reset caps lock state to false in case they changed it while the input was not in focus
		api.isCapsLockOn = false;
		
		api.UpdateValidation(api.GetValue());
		
		// Only show the tooltip if there's something to show
		if (api.$tooltip.children().length)
		{
			api.$tooltip.show();
		}
	});
	
	api.$container.on(['keyup', 'change', 'input', 'paste', 'drop', 'keydown', ''].join('.tooltipValidator '), 'input', function(e)
	{
		// Update caps lock value if this is a keyboard event
		if (e && e.originalEvent && e.originalEvent instanceof KeyboardEvent)
		{
			api.isCapsLockOn = e.originalEvent.getModifierState && e.originalEvent.getModifierState('CapsLock');
		}
		
		api.UpdateValidation(api.GetValue());
		
		// If the input updated while focused, then update the tooltip visibility based on contents
		if (api.$container.find('input:focus').length)
		{
			api.$tooltip.toggle(api.$tooltip.children().length > 0);
		}
	});
	
	if (api.$container.find('input:focus').length)
	{
		api.UpdateValidation(api.GetValue());
		
		// Only show the tooltip if there's something to show
		if (api.$tooltip.children().length)
		{
			api.$tooltip.show();
		}
	}
	else
	{
		api.$tooltip.hide();
	}
	
	api.$container.data('TooltipValidator', api);
}

$.fn.TooltipValidator = function(options)
{
	return this.each(function()
	{
		var $el = $(this);
		
		if (!$el.is('.field-validate-container'))
		{
			$el = $el.closest('.field-validate-container');
		}
		
		new TooltipValidator($el, options);
	});
};

TooltipValidator.presets = {};

TooltipValidator.presets.password = {
	containerClass: "field-validate-preset-password",
	headerText: "Passwords must contain:",
	showCapsLock: true,
	onValidate: function(value)
	{
		var rules = { items: [] };
		
		rules.items.push({
			passed: value.length >= 8,
			html: "at least 8 characters"
		});
		
		rules.items.push({
			passed: /[a-z]/.test(value),
			html: "at least 1 lowercase character"
		});
		
		rules.items.push({
			passed: /[A-Z]/.test(value),
			html: "at least 1 uppercase character"
		});
		
		rules.items.push({
			passed: /[0-9]/.test(value),
			html: "at least 1 number"
		});
		
		return rules;
	}
};

TooltipValidator.presets.capsLock = {
	containerClass: "field-validate-preset-capsLock",
	allowEmptyRules: true,
	showCapsLock: true
};