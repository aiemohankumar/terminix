/*
 * Browser feature detection
 * 
 * If more features are added, consider creating an object rather than several global variables.
 **********************************************************************************************/
var g_isHistorySupported =
  window.history &&
  typeof history.pushState == "function" &&
  typeof history.replaceState == "function" &&
  typeof history.back == "function";

function FormatNumber(value, numDecimals, useCommas) {
  var decimals = "";

  if (numDecimals > 0) {
    for (var i = 0; i < numDecimals; i++) decimals += "0";

    var decimalFactor = Math.pow(10, numDecimals);
    decimals += Math.floor(Math.round(value * decimalFactor)) % decimalFactor;

    decimals = "." + decimals.substr(decimals.length - numDecimals);
  }

  var whole =
    "" + Math.abs((value >= 0 ? Math.floor(value) : Math.ceil(value)) || 0);

  if (useCommas === true) {
    whole = whole.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return whole + decimals;
}

function GetLastFourDigits(value) {
  return value.substr(value.length - 4);
}

function GetMonthName(month) {
  var months = [
    "",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  return month >= 0 && month < months.length ? months[month] : "";
}

function GetMonthNameShort(month) {
  return GetMonthName(month).substr(0, 3);
}

function StringToLower(str) {
  return (str ? "" + str : "").toLowerCase();
}

function StringToUpper(str) {
  return (str ? "" + str : "").toUpperCase();
}

/*
 * Button Progress Custom Binding
 **********************************************************************************************/
ko.isObservableArray =
  ko.isObservableArray ||
  function(obj) {
    return ko.isObservable(obj) && !(obj.destroyAll === undefined);
  };

ko.bindingHelpers = ko.bindingHelpers || {};

ko.bindingHelpers.extendAllBindings = function(allBindings, newBindings) {
  var bindings = function() {
    return $.extend({}, allBindings(), newBindings);
  };

  bindings.get = function(name) {
    var data = bindings();

    return name in data ? data[name] : null;
  };

  return bindings;
};

ko.bindingHandlers.buttonProgress = (function() {
  return {
    init: function(
      element,
      valueAccessor,
      allBindings,
      viewModel,
      bindingContext
    ) {
      //
    },
    update: function(
      element,
      valueAccessor,
      allBindings,
      viewModel,
      bindingContext
    ) {
      if (ko.unwrap(valueAccessor())) {
        var options = $.extend({}, allBindings.get("buttonProgressOptions"));
        options.text = allBindings.get("buttonProgressText") || options.text;

        $(element).ShowButtonProgress(options);
      } else {
        $(element).HideButtonProgress();
      }
    }
  };
})();

ko.bindingHandlers.masked = (function() {
  var EvaluateMask = function(element, allBindings) {
    var mask = {
      format: "",
      options: {}
    };

    var preset = allBindings().maskedPreset;

    switch (preset) {
      case "phone":
        mask.format = "(000) 000-0000";
        break;
      case "phone-display":
        mask.format = "000.000.0000";
        break;
      case "zipcode":
        mask.format = "00000";
        break;
      case "date":
        mask.format = "00/00/0000";
        mask.options = { placeholder: "MM/DD/YYYY" };
        break;
      case "card-number":
        mask.format = "0000000000000000";
        break;
      case "card-expire":
        mask.format = "00/00";
        break;
      case "card-security":
        mask.format = "0000";
        break;
      case "bank-routing-number":
        mask.format = "Z00000000";
        mask.options = { translation: { Z: { pattern: /[012346789]/ } } };
        break;
    }

    mask.format = allBindings().maskedFormat || mask.format;
    $.extend(mask.options, allBindings().maskedOptions);

    return mask;
  };

  var DetectElementType = function(element) {
    return $(element).is("input, textarea") ? "value" : "text";
  };

  return {
    init: function(
      element,
      valueAccessor,
      allBindings,
      viewModel,
      bindingContext
    ) {
      // Apply the right default handler
      var elementType = DetectElementType(element);
      var mask = EvaluateMask(element, allBindings);

      if (elementType == "value") {
        var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
          valueUpdate: "afterkeydown"
        });

        ko.bindingHandlers.value.init(
          element,
          valueAccessor,
          newAllBindings,
          viewModel,
          bindingContext
        );

        // Apply the mask
        $(element).mask(mask.format, mask.options);
      } else {
        var $element = $(element).text(valueAccessor()());

        $element.mask(mask.format, mask.options);
      }
    },
    update: function(
      element,
      valueAccessor,
      allBindings,
      viewModel,
      bindingContext
    ) {
      // Apply the right default handler
      var elementType = DetectElementType(element);
      var mask = EvaluateMask(element, allBindings);

      if (elementType == "value") {
        var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, {
          valueUpdate: "afterkeydown"
        });

        ko.bindingHandlers.value.update(
          element,
          valueAccessor,
          newAllBindings,
          viewModel,
          bindingContext
        );

        // Apply the mask
        $(element).mask(mask.format, mask.options);

        valueAccessor()($(element).val());
      } else {
        var masked = $(element).masked(valueAccessor()());

        $(element).text(masked);
      }
    }
  };
})();

// ko.bindingHandlers.masked = {
//     init: function(element, valueAccessor, allBindingsAccessor) {
//         var mask = allBindingsAccessor().mask || {};
//         $(element).mask(mask);
//         ko.utils.registerEventHandler(element, 'focusout', function() {
//             var observable = valueAccessor();
//             observable($(element).val());
//         });
//     },
//     update: function (element, valueAccessor) {
//         var value = ko.utils.unwrapObservable(valueAccessor());
//         $(element).val(value);
//     }
// };

    function GetCityAndStateFromZip(zip, callback) {
        var _ENABLE_SMARTY_ZIP_TOGGLE = true;
         if (_ENABLE_SMARTY_ZIP_TOGGLE == true) {
             if (typeof callback == 'function') {
                 SmartyStreetsGetCityAndState(zip, callback);
             }
         } else {
             if (typeof callback == 'function') {
                 ZiptasticGetCityAndState(zip, callback);
             }
         }
     }
     
     function SmartyStreetsGetCityAndState(zip, callback) {
         $.ajax({
             type: 'GET',
             // url: '/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=' + zip,
             url:  'https://www.terminix.com/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=' + zip,
             dataType: 'json'
         }).done(function(result) {
             if (result.data.validResponse == 'true') {
                 callback({ model: { city: result.data.city, state: result.data.state } });
             } else {
                 callback({ error: "Not a valid response was received. Asking Customer for their city and state." });
             }
         }).fail(function() {
             callback({ error: "An error has occurred while calling getCityAndStateFromZip." });
         });
     }
 
     function ZiptasticGetCityAndState(zip, callback) {
         $.ajax({
             type: 'GET',
             url: 'https://zip.getziptastic.com/v2/US/' + zip,
             dataType: 'json'
         }).done(function(data) {
             callback({ model: { city: data.city, state: data.state_short } });
         }).fail(function() {
             callback({ error: "An error has occurred." });
         });
     }

function ZiptasticGetCityAndState(zip, callback) {
  $.ajax({
    type: "GET",
    url: "https://zip.getziptastic.com/v2/US/" + zip,
    dataType: "json"
  })
    .done(function(data) {
      callback({ model: { city: data.city, state: data.state_short } });
    })
    .fail(function() {
      callback({ error: "An error has occurred." });
    });
}

/*
 * DWR functions
 *****************************************************************/
function CallDWR(path, model, callback) {
  //path = CallDWR._CleanPathString(path);
  var basePath = "https://www.terminix.com/funnel/dwr/jsonp/";

  var blocked = CallDWR.OnBeforeCallbacks.some(function(onBeforeCallback) {
    return onBeforeCallback(path, model) === false;
  });

  if (blocked) return;

  var HandleCallbackData = function(data) {
    if (typeof callback == "function") {
      callback(data);
    }

    CallDWR.OnAfterCallbacks.forEach(function(onAfterCallback) {
      onAfterCallback(path, model, data);
    });
  };
  // MyAccountTMXUIUtils/validateAddress
  $.ajax({
    type: "POST",
    url: basePath + path,
    data: model ? { jsonData: JSON.stringify(model) } : {},
    dataType: "json",
    timeout: 2 * 60 * 10000,
    complete: function(data, textStatus) {
      var callbackData;

      try {
        var escapedText = JSON.parse(data.responseText);

        if (escapedText.error) {
          callbackData = {
            error: escapedText.error.message,
            status: textStatus // for timeout check
          };
        } else {
          callbackData = { model: JSON.parse(escapedText.reply) };
        }
      } catch (e) {
        callbackData = { error: "An error has occurred." };
        console.log("Error from server request: " + e.message);
      }

      HandleCallbackData(callbackData);
    }
  });
}

CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];

CallDWR._CleanPathString = function(path) {
  return path.replace(/^\/*|\/*$/g, "");
};

function OnBeforeCallDWR(callback) {
  CallDWR.OnBeforeCallbacks.push(callback);
}

function OnAfterCallDWR(callback) {
  CallDWR.OnAfterCallbacks.push(callback);
}

function GetInterestArea() {
  // Default to pest control
  var interestArea = "PEST CONTROL";

  // Update the interest area if one exists in the session
  if (GetURLParameter("interestArea") || caniuse.sessionStorage) {
    var newArea =
      GetURLParameter("interestArea") || sessionStorage.getItem("interestArea");

    if (newArea) {
      interestArea = newArea.toUpperCase();
    }
  }

  return interestArea;
}

function IsFreeInspectionInterest(interestArea) {
  switch (interestArea) {
    case "PEST CONTROL":
    case "ANTS":
    case "CLOTHES MOTHS":
    case "CRICKETS":
    case "FLEAS":
    case "SCORPIONS":
    case "SILVERFISH":
    case "SPIDERS":
    case "TICKS":
    case "PAPER WASPS":
    case "MOSQUITOES":
      return false;
      break;

    case "ARMADILLOS":
    case "ATTIC INSULATION":
    case "BED BUG":
    case "CRAWL SPACE":
    case "MICE":
    case "RACCOONS":
    case "RODENTS":
    case "SQUIRRELS":
    case "TERMITES":
    case "WILDLIFE":
    case "OTHER SERVICES":
      return true;
      break;
  }
}

function ScrollTo(pos, speed) {
  $("html, body").animate({ scrollTop: pos }, speed);
}

/*
 * jQuery utilities
 **********************************************************************************************/
jQuery.fn.ShowButtonProgress = function(options) {
  options = $.extend(
    {
      text: "Loading...",
      autosize: true
    },
    options
  );

  return this.each(function() {
    var $button = $(this);

    if ($button.data("in-progress")) {
      // Remove progress so it can redo everything
      $button.prev().remove();
      $button.show();
    }

    // Create container and button
    var $progressContainer = $('<div class="button-progress-container">'),
      $progressButton = $("<div>");

    $progressContainer.append($progressButton);

    // Copy over styles to button
    $progressButton.attr("class", $button.attr("class"));
    $progressButton.addClass("show-progress");
    $progressButton.attr("style", $button.attr("style"));
    $progressButton.text(options.text || "Loading...");

    // Copy position styles to container and remove any from the button
    var positionStyles = {
      "margin-bottom": 0,
      "margin-left": 0,
      "margin-right": 0,
      "margin-top": 0,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      float: "none",
      position: "relative",
      transform: "none"
    };

    $button.hide(); // Need to hide to be able to access "auto" margins in FireFox

    for (var rule in positionStyles) {
      $progressContainer.css(rule, $button.css(rule));
      $progressButton.css(rule, positionStyles[rule]);
    }

    $button.show(); // Show again because we need width/height sizes from below

    // Grab size of real button so container can match
    var buttonWidth = $button.outerWidth(),
      buttonDisplay = $button.css("display");

    if (options.autosize) {
      $progressContainer.height($button.outerHeight());
    }

    // Show the container and hide the original button
    $progressContainer.insertBefore($button).show();
    $button.hide();

    // Size the container properly
    $progressContainer.css("display", "inline-block");

    if (options.autosize) {
      buttonWidth = Math.max(buttonWidth, $progressContainer.outerWidth());

      $progressContainer
        .css("width", buttonWidth + "px")
        .css("display", buttonDisplay);
    }

    // Set that the button has progress
    $button.data("in-progress", true);
  });
};

jQuery.fn.HideButtonProgress = function() {
  return this.each(function() {
    var $button = $(this);

    if (!$button.data("in-progress")) return;

    $button.prev().remove();
    $button.show();

    $button.data("in-progress", false);
  });
};

ko.extenders.required = function(target, overrideMessage) {
  //add some sub-observables to our observable
  target.isInitialized = ko.observable(false);
  target.hasError = ko.observable();
  target.validationMessage = ko.observable();

  //define a function to do validation
  function validate(newValue) {
    target.isInitialized(true);
    target.hasError(newValue ? false : true);
    target.validationMessage(
      newValue ? "" : overrideMessage || "This field is required"
    );
  }
  //initial validation
  //validate(target());
  //validate whenever the value changes
  target.subscribe(validate);
  //return the original observable
  return target;
};

ko.extenders.email = function(target, overrideMessage) {
  target.isInitialized = ko.observable(false);
  target.hasError = ko.observable();
  target.validationMessage = ko.observable();
  function validate(value) {
    target.isInitialized(true);
    target.hasError(!IsValidEmailAddress(value));
    target.validationMessage(
      IsValidEmailAddress(value)
        ? ""
        : overrideMessage || "Invalid email address"
    );
  }
  target.subscribe(validate);
  return target;
};

var tmx = tmx || {}

tmx.funnel = function(){
    // this.customer = funnelModel.customer;
    
    // this.customer = {
    //     serviceProperty : {
    //         address1 : ko.observable().extend({ required:"" }),
    //         address2 : ko.observable(),
    //         city : ko.observable().extend({ required: ""}),
    //         state : ko.observable().extend({ required: ""}),
    //         zipCode : ko.observable().extend({required: ""}),
    //         squareFootage: ko.observable().extend({ required: ""}),
    //         addressVerified : ko.observable(false) // optionally set from frontend if user pressed "continue as is" on invalid address modal
    //     },
    //     contact : {
    //         email : ko.observable().extend({email: ""}),
    //         phone : ko.observable().extend({ required: ""}),
    //     }
    // },

    this.errorMessages = ko.observableArray([]),
    this.errorCount = 0;
    this.submittingStep = ko.observable(),
    this.submitting = ko.observable(false),
    this.isValidating = ko.observable(false),
    this.ziptasticFail = ko.observable(false),
    this.model = ko.observable();
   // this.model(funnelModel);

    
    // Common functions
    // =========================

    this.ResolveScope = function(scope) {
        if (scope === undefined) scope = self.model;
        if (ko.isObservable(scope)) scope = scope();

        return scope;
    };

    return valueAccessor;
  };

  // Error functions
  // =========================

  this.RemoveAllMessages = function(scope) {
    scope = self.ResolveScope(scope);
    if (!scope) return;

    if (ko.isObservableArray(scope.errorMessages))
      scope.errorMessages.removeAll();
    if (ko.isObservableArray(scope.fieldValidationErrors))
      scope.fieldValidationErrors.removeAll();
    if (ko.isObservableArray(scope.informationalMessages))
      scope.informationalMessages.removeAll();
  };

  this.HasFieldError = function(field, scope) {
    return self.GetFieldError(field, scope) != "";
  };

  this.GetFieldError = function(field, scope) {
    scope = self.ResolveScope(scope);

    if (!field || !scope) return "";

    var fieldError = scope.fieldValidationErrors().find(function(fieldError) {
      return ko.unwrap(fieldError.field) == field;
    });

    return fieldError ? ko.unwrap(fieldError.errorMessage) : "";
  };

  this.AddFieldError = function(field, message, scope) {
    scope = self.ResolveScope(scope);

    if (
      field &&
      message &&
      scope &&
      ko.isObservableArray(scope.fieldValidationErrors)
    ) {
      scope.fieldValidationErrors.push(
        ko.mapping.fromJS({
          field: field,
          errorMessage: message
        })
      );
    }
  };

  this.RemoveFieldError = function(field, scope) {
    scope = self.ResolveScope(scope);

    if (scope && ko.isObservableArray(scope.fieldValidationErrors)) {
      scope.fieldValidationErrors.remove(function(fieldError) {
        return ko.unwrap(fieldError.field) == field;
      });
    }
  };

  // Validator class
  // =========================

  (this.Validator = function() {
    // Require a constructor call
    if (this == self) {
      return new self.Validator();
    }

    var validator = this;

    // Fields object of added fields
    validator.fields = {};

    // Available validation rules
    validator.rules = {
      required: function(value, overrideMessage) {
        return {
          hasError: !value,
          message: overrideMessage || "This field is required."
        };
      },
      validEmail: function(value, overrideMessage) {
        return {
          hasError: !IsValidEmailAddress(value),
          message: overrideMessage || "A valid email address is required."
        };
      },
      validRoutingNumber: function(value, overrideMessage) {
        return {
          hasError: !IsValidRoutingNumber(value),
          message: overrideMessage || "A valid routing number is required."
        };
      }
    };

    // Adds a field to the validator object to be tested
    validator.add = function(field, rule, options) {
      if (typeof rule != "function") {
        if (
          !validator.rules.hasOwnProperty(rule) ||
          typeof validator.rules[rule] != "function"
        )
          throw "Invalid rule provided.";

        rule = validator.rules[rule];
      }

      // Other options:
      // - valueAccessor = function to return the value to validator
      // - scope = the scope of this field for error messages and base of path to field
      options = $.extend(
        {
          message: ""
        },
        options
      );

      if (typeof options.valueAccessor != "function") {
        options.valueAccessor = function() {
          return self.ResolveFieldName(field, options.scope);
        };
      }

      validator.fields[field] = {
        options: options,
        validate: function() {
          var value = options.valueAccessor();
          var result = rule(value, options.message);

          if (result.hasError) {
            self.AddFieldError(field, result.message, options.scope);
          } else {
            self.RemoveFieldError(field, options.scope);
          }

          return !result.hasError;
        }
      };

      return validator;
    };

    // Validates a given field
    validator.test = function(field) {
      if (!validator.fields.hasOwnProperty(field)) {
        throw "Invalid field provided.";
      }

      return validator.fields[field].validate();
    };

    // Used in knockout bindings for events
    validator.onEvent = function(field) {
      return function() {
        if (validator.fields.hasOwnProperty(field)) {
          validator.fields[field].validate();
        }
      };
    };
  }),
    (this.validator = new this.Validator());

  this.validator.add("customer.serviceProperty.address1", "required");
  this.validator.add("customer.serviceProperty.zipCode", "required");
  this.validator.add("customer.contact.email", "validEmail");
  this.validator.add("customer.contact.phone", "required");

  this.validator.add("customer.firstName", "required");
  this.validator.add("customer.lastName", "required");
  this.validator.add("payment.cardNumber", "required");
  this.validator.add("payment.cardExpire", "required");
  this.validator.add("payment.bankAccountNumber", "required");
  this.validator.add("payment.bankRoutingNumber", "validRoutingNumber");
  this.validator.add("customer.billingProperty.address1", "required");
  this.validator.add("customer.billingProperty.city", "required");
  this.validator.add("customer.billingProperty.state", "required");
  this.validator.add("customer.billingProperty.zipCode", "required");

  this.modelLoaded = ko.observable(false);
  this.error = ko.observable();

  (this.STEP_ADDRESS = "step_address"),
    (this.STEP_PRODUCTS = "step_products"),
    (this.STEP_SCHEDULE = "step_schedule"),
    (this.STEP_PAYMENT = "step_payment"),
    (this.STEP_CONFIRMATION = "step_confirmation"),
    (this.defaultStepOrder = [
      STEP_ADDRESS,
      STEP_PRODUCTS,
      STEP_SCHEDULE,
      STEP_PAYMENT,
      STEP_CONFIRMATION
    ]);
  this.isShowStep = {};
  this.currentStepObservable = ko.observable(defaultStepOrder[0]);
  this.stepHistory = [];
  this.stepCache = {};

  defaultStepOrder.forEach(function(step) {
    isShowStep[step] = ko.observable(true);
  });

  if (caniuse.sessionStorage) {
    var stepCacheJSON = sessionStorage.getItem("stepCache");
    if (stepCacheJSON) {
      var json,
        parsed = false;

      try {
        json = JSON.parse(stepCacheJSON);
        parsed = true;
      } catch (e) {
        console.log(
          "An invalid JSON string was set in stepCache sessionStorage."
        );
      }

      if (json) {
        defaultStepOrder.forEach(function(step) {
          if (json[step]) {
            stepCache[step](json[step]);
          }
        });
      } else if (parsed) {
        console.log("A non-object was set in stepCache sessionStorage.");
      }
    }
  }

  (this.IsShowStep = function(step) {
    return step in isShowStep ? isShowStep[step]() : true;
  }),
    (this.init = function() {
      debugger;
      // Please Change the calling function for address,pricing and schedule pages
      // These will be removed in the further update. For the current working using it.

      LoadModel();
      // getProductPricingData();
      //getScheduleData();

      if (!(window.sessionStorage.getItem("funnelData") === null)) {
        //  ko.mapping.fromJSON(window.sessionStorage.getItem("funnelData"),{},customer);
      }
      if (!(window.sessionStorage.getItem("currentStep") === null)) {
        // currentStep(window.sessionStorage.getItem("currentStep"));
        // funnelModel.currentStep(window.sessionStorage.getItem("currentStep"));
      }
      //funnelModel.currentStep(window.sessionStorage.getItem("currentStep"));
      // currentStep(funnelModel.currentStep());

      //funnelModel.currentStep(STEP_PRODUCTS)
      //currentStep(STEP_SCHEDULE);

      // if(currentStep() == STEP_PRODUCTS)
      //  getProductPricingData();

      //if(currentStep() == STEP_SCHEDULE)
      //   getScheduleData();
    }),
    /**
     * Clears step cache, within a range if params are provided
     *
     * @param options {Object} map of options to step cache clearing
     *                         - after: name of step -- cache will be cleared for all steps after this step
     *                         - until: name of step -- cache will be cleared up to this step, but not including this step
     *                         If both after and until are provided, then only steps between them will have their cache cleared
     */
    (this.ClearStepCache = function(options) {
      options = $.extend(
        {
          after: "",
          until: ""
        },
        options
      );

      var startIndex =
        (options.after && defaultStepOrder.indexOf(options.after) + 1) || 0;
      var stopIndex = options.until
        ? defaultStepOrder.indexOf(options.until)
        : -1;

      if (stopIndex == -1) {
        stopIndex = defaultStepOrder.length;
      }

      // defaultStepOrder.slice(startIndex, stopIndex).forEach(function(step) {
      //     stepCache[step](null);
      // });
    });

  // // Wrapper so it cannot be changed outside of this view model
  // this.currentStep = ko.computed(function() {
  //     return currentStepObservable();
  // });

  var currentStepIndex = ko.computed(function() {
    return defaultStepOrder.indexOf(self.currentStep());
  });
  // Set the default affiliate path flag
  this.IsAffiliatePath = ko.observable(false);

  // Set the free inspection flag
  this.IsFreeInspectionPath = ko.computed(function() {
    if (!self.modelLoaded()) return false;

    return IsFreeInspectionInterest(self.model().interestArea());
  });

  this.squareFootageOptions = ko.observableArray([
    {
      optionText: "Up to 3999",
      value: 3999
    },
    {
      optionText: "4,000-5,999",
      value: 5999
    },
    {
      optionText: "6,000-7,999",
      value: 7999
    },
    {
      optionText: "Over 8,000",
      value: 8000
    }
  ]);

  this.noDatesChosen = ko.observable(false);

  self.scheduleSubmitLegalText = ko.computed(function() {
    var noDateSelectedText =
        "By clicking above, you agree to have a representative from your local branch contact you to schedule an appointment",
      dateSelectedText =
        "By clicking above, you understand that you are scheduling an appointment";

    return self.noDatesChosen() ? noDateSelectedText : dateSelectedText;
  });

  self.billingAddressSameAsServiceAddress = ko.observable(true);

  self.cardExpirationMonths = [];
  self.cardExpirationYears = [];

  (this.validateField = function(value, hasError, fieldId) {
    if (value == undefined || value == "" || hasError) {
      ko.utils.triggerEvent(document.getElementById(fieldId), "change");
      return false;
    } else return true;
  }),
    (this.AddressValidator = function(options) {
      // Require a constructor call
      if (this == self) {
        return new self.AddressValidator(options);
      }

      var validator = this;

      // status variables
      validator.isChoosing = ko.observable(false);
      validator.isValidating = ko.observable(false);

      // correction variables
      validator.address = ko.observable();
      validator.addresses = ko.observableArray();
      validator.selectedAddress = ko.observable();
      validator.lastValidatedAddress = null;
      validator.cache = {};

      validator.ziptasticFail = ko.observable(false);

      // used from validator.ValidateAddress to store the callbacks and use later
      var onKeepAddress;
      var onUseSelectedAddress;

      // validates an address object
      // Required options
      // options.onKeepAddress = function() {};
      // options.onUseSelectedAddress = function(selectedAddress) {};
      // options.onValidate = function(hasCorrections) {};
      validator.ValidateAddress = function(address, options) {
        if (!options) throw "Options are required.";
        if (typeof options.onKeepAddress != "function")
          throw "onKeepAddress callback is required.";
        if (typeof options.onUseSelectedAddress != "function")
          throw "onUseSelectedAddress callback is required.";
        if (typeof options.onValidate != "function")
          throw "onValidate callback is required.";

        onKeepAddress = options.onKeepAddress;
        onUseSelectedAddress = options.onUseSelectedAddress;

        validator.isValidating(true);

        var json = {
          address1: StringToLower(address.address1),
          address2: StringToLower(address.address2),
          city: StringToLower(address.city),
          state: StringToLower(address.state),
          zipcode: StringToLower(address.zipCode)
        };

        var key = JSON.stringify(json);

        var HandleResult = function(addresses) {
          validator.isValidating(false);

          var hasCorrections = false;

          if (addresses && addresses.length) {
            validator.address(address);
            validator.addresses(addresses);
            validator.selectedAddress(addresses[0]);

    this.validator = new this.Validator();

    this.validator.add('customer.serviceProperty.address1', 'required');
    this.validator.add('customer.serviceProperty.zipCode', 'required');
    this.validator.add('customer.contact.email', 'validEmail');
    this.validator.add('customer.contact.phone', 'required');

    this.validator.add('customer.firstName', 'required');
    this.validator.add('customer.lastName', 'required');
    this.validator.add('payment.cardNumber', 'required');
    this.validator.add('payment.cardExpire', 'required');
    this.validator.add('payment.bankAccountNumber', 'required');
    this.validator.add('payment.bankRoutingNumber', 'validRoutingNumber');
    this.validator.add('customer.billingProperty.address1', 'required');
    this.validator.add('customer.billingProperty.city', 'required');
    this.validator.add('customer.billingProperty.state', 'required');
    this.validator.add('customer.billingProperty.zipCode', 'required');

    this.modelLoaded = ko.observable(false);
    this.error = ko.observable();
    
    var STEP_ADDRESS = self.STEP_ADDRESS = 'step_address';
    var STEP_PRODUCTS = self.STEP_PRODUCTS = 'step_products';
    var STEP_SCHEDULE = self.STEP_SCHEDULE = 'step_schedule';
    var STEP_PAYMENT = self.STEP_PAYMENT = 'step_payment';
    var STEP_CONFIRMATION = self.STEP_CONFIRMATION = 'step_confirmation';

    var stepTextMap = {};

    stepTextMap[STEP_ADDRESS] = 'Address';
    stepTextMap[STEP_PRODUCTS] = 'Pricing';
    stepTextMap[STEP_SCHEDULE] = 'Schedule';
    stepTextMap[STEP_PAYMENT] = 'Pay';
    stepTextMap[STEP_CONFIRMATION] = 'Confirmation';

    var defaultStepOrder = [STEP_ADDRESS, STEP_PRODUCTS, STEP_SCHEDULE, STEP_PAYMENT, STEP_CONFIRMATION];

    var isShowStep = {};

    defaultStepOrder.forEach(function(step) {
        isShowStep[step] = ko.observable(true);
    });

    self.IsShowStep = function(step) {
        return step in isShowStep ? isShowStep[step]() : true;
    };

    if (caniuse.sessionStorage) {
        var isShowStepJSON = sessionStorage.getItem("isShowStep");
        if (isShowStepJSON) {
            var json, parsed = false;

            try {
                json = JSON.parse(isShowStepJSON);
                parsed = true;
            }
            catch (e) {
                console.log("An invalid JSON string was set in isShowStep sessionStorage.");
            }

            if (json) {
                defaultStepOrder.forEach(function(step) {
                    isShowStep[step](!!json[step]);
                });
            } else if (parsed) {
                console.log("A non-object was set in isShowStep sessionStorage.");
            }
        }
    }

    var stepOrder = self.stepOrder = ko.computed(function() {
        return defaultStepOrder.filter(self.IsShowStep);
    });

    this.currentStepObservable = ko.observable(defaultStepOrder[0]);
    this.stepHistory = [];
    this.stepCache = {};
   
    // defaultStepOrder.forEach(function(step) {
    //     isShowStep[step] = ko.observable(true);
    // });

    defaultStepOrder.forEach(function(step) {
        stepCache[step] = ko.observable(null);
    });

            hasCorrections = true;
          }

          validator.lastValidatedAddress = $.extend({}, address);

          options.onValidate(hasCorrections);
        };

        if (validator.cache.hasOwnProperty(key)) {
          HandleResult(validator.cache[key].slice());
        } else {
          CallDWR(
            "MyAccountTMXUIUtils/validateAddress",
            { address: json },
            function(data) {
              var addresses;

              if (
                data.model &&
                data.model.alternativeAddresses &&
                data.model.alternativeAddresses.length
              ) {
                // HACK: Remove alternativeAddresses results that contain null address fields
                addresses = data.model.alternativeAddresses = data.model.alternativeAddresses.filter(
                  function(alternativeAddress) {
                    return (
                      alternativeAddress &&
                      alternativeAddress.address1 &&
                      alternativeAddress.address1 != "null null" &&
                      alternativeAddress.city &&
                      alternativeAddress.city != "null" &&
                      alternativeAddress.state &&
                      alternativeAddress.state != "null" &&
                      alternativeAddress.zipcode &&
                      alternativeAddress.zipcode != "null"
                    );
                  }
                );
                // HACK: Change 'zipcode' to 'zipCode'
                addresses = addresses.map(function(address) {
                  return {
                    address1: address.address1,
                    address2: address.address2,
                    city: address.city,
                    state: address.state,
                    zipCode: address.zipcode
                  };
                });

                validator.cache[key] = addresses.slice();
              }

              HandleResult(addresses);
            }
          );
        }
      };

      // resets all data
      validator.Reset = function() {
        validator.isChoosing(false);
        validator.isValidating(false);
        validator.addresses.removeAll();
        validator.selectedAddress(null);
        validator.cache = {};
      };

    this.init = function(){
        debugger;
       // Please Change the calling function for address,pricing and schedule pages
       // These will be removed in the further update. For the current working using it.
      
       LoadModel();
       getProductPricingData();
       getScheduleData();

        validator.isChoosing(false);
        validator.lastValidatedAddress = address; // set the selected as last validated so we don't revalidate

        if (typeof onUseSelectedAddress == "function") {
          onUseSelectedAddress();
          onUseSelectedAddress = undefined;
        }
      };

      // closes the selections area
      validator.Cancel = function() {
        validator.isChoosing(false);
      };

      // checks if this address has been validated
      validator.IsLastValidatedAddress = function(address) {
        return (
          address &&
          validator.lastValidatedAddress &&
          StringToLower(address.address1) ==
            StringToLower(validator.lastValidatedAddress.address1) &&
          StringToLower(address.address2) ==
            StringToLower(validator.lastValidatedAddress.address2) &&
          StringToLower(address.city) ==
            StringToLower(validator.lastValidatedAddress.city) &&
          StringToLower(address.state) ==
            StringToLower(validator.lastValidatedAddress.state) &&
          StringToLower(address.zipCode) ==
            StringToLower(validator.lastValidatedAddress.zipCode)
        );
      };
    }),
    // Address validation

    (this.addressValidator = new this.AddressValidator());

  self.CheckStepAddressValidation = function(json, submitFunction) {
    switch (self.currentStep()) {
      case STEP_ADDRESS:
        var address = json.customer.serviceProperty;

        if (self.addressValidator.IsLastValidatedAddress(address)) {
          submitFunction();
          return;
        }

        self.addressValidator.isValidating(true);

        GetCityAndStateFromZip(address.zipCode, function(data) {
          if (data.error) {
            // If ziptastic hasn't already failed once
            if (!self.addressValidator.ziptasticFail()) {
              self.submitting(false);
              self.addressValidator.ziptasticFail(true);

              self.validator.add("customer.serviceProperty.city", "required");
              self.validator.add("customer.serviceProperty.state", "required");

              $(".city-state-block").show();

              self.PostError(
                "There was a problem resolving your ZIP. Please enter your city and state."
              );
              FireFunnelGAEvent(
                GA_CATEGORY_FUNNEL,
                "Submit Address Validation",
                "Error: There was a problem resolving your ZIP. Please enter your city and state.",
                ""
              );
              return;
            }

            // Save user entered city and state
            address.city = self.model().customer.serviceProperty.city();
            address.state = self.model().customer.serviceProperty.state();
          } else {
            // Populate the address on the model
            self.model().customer.serviceProperty.city(data.model.city);
            self.model().customer.serviceProperty.state(data.model.state);

            // Populate the address being sent to validate
            address.city = data.model.city;
            address.state = data.model.state;

            FireFunnelGAEvent(
              GA_CATEGORY_FUNNEL,
              "Submit Address Validation",
              "Submit Success",
              ""
            );
          }

          self.addressValidator.ziptasticFail(false);

          self.addressValidator.ValidateAddress(address, {
            onKeepAddress: function() {
              SubmitStep();
            },
            onUseSelectedAddress: function(selectedAddress) {
              self
                .model()
                .customer.serviceProperty.address1(selectedAddress.address1);
              self
                .model()
                .customer.serviceProperty.address2(selectedAddress.address2);
              self.model().customer.serviceProperty.city(selectedAddress.city);
              self
                .model()
                .customer.serviceProperty.state(selectedAddress.state);
              self
                .model()
                .customer.serviceProperty.zipCode(selectedAddress.zipCode);

              SubmitStep();
            },
            onValidate: function(hasCorrections) {
              if (hasCorrections) {
                self.submitting(false);
              } else {
                SubmitStep();
              }
            }
          });
        });
        break;
      case STEP_PRODUCTS:
        submitFunction();
        break;
      case STEP_SCHEDULE:
        submitFunction();
        break;
      case STEP_PAYMENT:
        submitFunction();
        break;
    }
  };

  // Model functions
  // =========================

  function GetStepURL(step) {
    //var url = "/test/funnel/",
    var url = "/buyonline/",
      page = "";

    switch (step) {
      case STEP_ADDRESS:
        page = "address";
        break;
      case STEP_PRODUCTS:
        page = "pricing";
        break;
      case STEP_SCHEDULE:
        page = "schedule";
        break;
      case STEP_PAYMENT:
        page = "payment";
        break;
      case STEP_CONFIRMATION:
        page = "confirmation";
        break;
    }
    debugger;

    return url + page + window.location.search + window.location.hash;
    // Keep track of query params and hashes
    // return "file:///D:/Terminix/terminix/buyonline/address.html";
  }

  function GetStepName(step) {
    var url = GetStepURL(step);
    return (
      url
        .substr(url.lastIndexOf("/") + 1)
        .charAt(0)
        .toUpperCase() + url.substr(url.lastIndexOf("/") + 1).slice(1)
    );
  }

  this.OnHistoryPop = function() {
    // Check if there is history
    if (stepHistory.length) {
      var step = stepHistory.pop();

      // Check if user can go to this step
      if (step in stepCache && stepCache[step]()) {
        // Show this step to the user
        UpdateModel(stepCache[step](), true);
      } else {
        // Find all available steps
        var availableSteps = defaultStepOrder.filter(function(step) {
          return stepCache[step]();
        });

        //console.log('availableSteps', availableSteps);

        // If there are no available steps, then something broke so just reload the funnel
        if (!availableSteps.length) {
          window.location = GetStepURL(defaultStepOrder[0]);
          window.location.reload();
          return;
        }

        // Move user to highest available step
        UpdateModel(stepCache[availableSteps.pop()](), true);
      }

      // Set the new state
      if (g_isHistorySupported) {
        history.pushState(stepHistory, "", GetStepURL(self.currentStep()));
      }
    } else {
      //console.log('no step history length');
      // If there is not history, then they are at the very beginning
      // Take them beck to where they came from
      if (g_isHistorySupported) {
        // Remove the popstate event so we don't infinite loop calling history.back()
        //window.onpopstate = function(){};
        //history.back();
      }
    }
  };

    // Wrapper so it cannot be changed outside of this view model
    this.currentStep = ko.computed(function() {
        return currentStepObservable();
    });

    var currentStepIndex = ko.computed(function() {
        return defaultStepOrder.indexOf(self.currentStep());
    });
        // Set the default affiliate path flag
    this.IsAffiliatePath = ko.observable(false);

    if (caniuse.sessionStorage) {
      sessionStorage.setItem(
        "funnelModel",
        JSON.stringify(ko.mapping.toJS(options.model))
      );
      sessionStorage.setItem(
        "stepCache",
        JSON.stringify(ko.mapping.toJS(options.stepCache))
      );
      sessionStorage.setItem(
        "isShowStep",
        JSON.stringify(ko.mapping.toJS(options.isShowStep))
      );
    }
  };

  var GA_CATEGORY_FUNNEL = "TMX Funnel";

  this.FireFunnelGAEvent = function(category, action, label, value) {
    category = (typeof category == "string" && category) || GA_CATEGORY_FUNNEL;

    FireGAEvent(category, action, label, value);

    if (self.model() && self.model().tiered()) {
      category += ": Tiered";
    } else {
      category += ": Standard";
    }

    FireGAEvent(category, action, label, value);
  };

  this.FireAffiliateGAEvent = function(affiliate, label) {
    if (!affiliate.id || !affiliate.name) {
      console.log("[FireAffiliateGAEvent] Affiliate name and id are required!");
    }

    var action = "[" + affiliate.id + "] " + affiliate.name;

    FireGAEvent("Affiliate Tracking", action, label, "");
  };

  this.FireFreeInspectionConversion = function() {
    var label = "";

    switch (self.model().interestArea()) {
      case "ATTIC INSULATION":
        label = "Attic Insulation";
        break;

      case "BED BUGS":
        label = "Bed Bugs";
        break;

      case "CRAWL SPACE":
        label = "Crawl Space";
        break;

      case "RODENTS":
      case "WILDLIFE":
        label = "Wildlife";
        break;

      case "TERMITES":
        label = "Termite";
        break;
    }

    FireFunnelGAEvent("Inspection Schedule", "Form Submit Success", label, "");
    FireFunnelGAEvent(
      GA_CATEGORY_FUNNEL,
      "Submit Free Inspection",
      "Success: " + label,
      ""
    );
  };

  this.GetModelJSON = function(model) {
    // Get the latest model data
    var json = ko.mapping.toJS(self.model) || {};

    // Set current step in case it was tampered on the model
    if (json) {
      json.currentStep = self.currentStep();
    }

    return json;
  };

  this.UpdateModel = function(model, ignoreHistory) {
    // Extend from the current model to the new model, so we retain products and settings
    //  model = $.extend(GetModelJSON(model), model);
    model = $.extend(ko.mapping.toJS(model), model);

    // Check if there is a redirect
    if (model.redirectRequest && model.redirectURL) {
      window.location = model.redirectURL;
      return;
    }

    // If no step is set, start at step 1
    if (!model.currentStep) {
      model.currentStep = defaultStepOrder[0];
    }

    self.currentStep(model.currentStep);
    // var isChangingSteps = model.currentStep != self.currentStep();

    // // Check if step changed and we should update in history
    // if (g_isHistorySupported && !ignoreHistory) {
    //     // Update the history trail
    //     if (isChangingSteps || !stepHistory.length) {
    //         stepHistory.push(self.currentStep());
    //     }

    //     stepCache[model.currentStep]($.extend(true, {}, model));

    //     history.replaceState(stepHistory, "", GetStepURL(model.currentStep));
    // }

    // Backwards compatibility for property on customer changing to serviceProperty
    if (
      model.customer &&
      !model.customer.serviceProperty &&
      model.customer.property
    ) {
      model.customer.serviceProperty = model.customer.property;
    }

    // Initialize available products array
    if (!model.availableProducts) {
      model.availableProducts = [];
    }

    // Set the commercial affiliate if necessary
    // if (!self.IsAffiliatePath() && !IsObjectEmpty(model.commercialAffiliate)) {
    //     self.IsAffiliatePath(true);
    // }

    // Do any step-specific stuff for the model before updating the model on the page
    switch (model.currentStep) {
      case STEP_ADDRESS:
        model.interestArea = GetInterestArea();
        model.showUpsell = false;

        // Check for Free Inspection path
        if (IsFreeInspectionInterest(model.interestArea)) {
          isShowStep[STEP_PRODUCTS](false);
          isShowStep[STEP_PAYMENT](false);
        }
        break;

      case STEP_PRODUCTS:
        // Create the available products list from the returned product map
        if (model.productItemMap && !model.availableProducts.length) {
          for (var productId in model.productItemMap) {
            var product = model.productItemMap[productId];

            if (!product.templateCode || !product.productCode) {
              continue;
            }

            product.isDisabled = false;
            product.isALaCarte = self.IsALaCarteProduct(
              product,
              model.tiered,
              model.interestArea
            );
            product.isTiered = self.IsTieredProduct(product);

            // Parse all values into floats
            product.productCost = parseFloat(product.productCost) || 0.0;
            product.discount = parseFloat(product.discount) || 0.0;
            product.taxes = parseFloat(product.taxes) || 0.0;
            product.startUpCostTotal =
              parseFloat(product.startUpCostTotal) ||
              product.productCost - product.discount + product.taxes;
            product.frequencyCost = parseFloat(product.frequencyCost) || 0.0;

            // Add product total without discounts for visual
            if (product.discount > 0) {
              product.startUpCostTotalWithoutDiscount = product.productCost;

              if (product.taxes) {
                var taxRate =
                  product.taxes / (product.productCost - product.discount);

                // round the thousandth's decimal place up and truncate to only 2 decimals
                product.startUpCostTotalWithoutDiscount +=
                  Math.ceil(product.productCost * taxRate * 100) / 100;
              }
            } else {
              product.startUpCostTotalWithoutDiscount =
                product.startUpCostTotal;
            }

            // Populate frequency fields if they don't exist (backwards compatibility)
            if (!product.paymentFrequency) product.paymentFrequency = "";
            if (!product.frequencyCost) product.frequencyCost = 0;

            // Use this if the payment frequency value is not user-friendly to map to a new user-friendly string
            product.paymentFrequencyDisplay = self.GetPaymentFrequencyDisplay(
              product.paymentFrequency
            );

            // Add product as available product
            model.availableProducts.push(product);
          }

          // Only continue if available products were found in the map
          if (model.availableProducts.length) {
            // Grab the target pest product object
            var targetPestItem = model.availableProducts.find(function(
              product
            ) {
              return product.productCode === model.targetPest;
            });

            // Move target pest item to top of available products array
            if (targetPestItem) {
              model.availableProducts = model.availableProducts.filter(function(
                product
              ) {
                return product.productCode !== model.targetPest;
              });

              model.availableProducts.unshift(targetPestItem);
            }

            // Place items in cart that need to be by default (only occurs on product step as a session model will already have the items set)
            model.productIdCart = [];
            model.availableProducts.forEach(function(product) {
              if (product.itemInCart) {
                model.productIdCart.push(product.productId);
              }

              // TODO: add error logic for if an item failed to add to cart upon submitting product step
            });
          }
        }

        // Set the billing address as service address
        model.customer.billingProperty = model.customer.serviceProperty;
        break;

      case STEP_SCHEDULE:
        var foundSelectedDate = false;

        model.availableDates = [];

        if (model.serviceSchedule && model.serviceSchedule.length) {
          for (var i = 0; i < model.serviceSchedule.length; i++) {
            var dateObj = { dateString: "", availableTimes: [] },
              loopSchedule = model.serviceSchedule[i];

            dateObj.dateString = loopSchedule.date;

            for (var t = 0; t < loopSchedule.serviceTimeSlots.length; t++) {
              dateObj.availableTimes.push(
                loopSchedule.serviceTimeSlots[t].schFrmTime
              );
            }

            model.availableDates.push(dateObj);
          }
        } else {
          self.noDatesChosen(true);
        }

        model.availableTimes = [];

        if (self.noDatesChosen()) {
          model.selectedDate = "NONE";
          model.timeSlotSelectedKey = "NONE";
          model.selectedTime = "";
          foundSelectedDate = true;
        }

        if (model.availableDates && model.availableDates.length) {
          model.availableDates.forEach(function(availableDate) {
            if (
              !foundSelectedDate &&
              availableDate.dateString == model.selectedDate
            ) {
              foundSelectedDate = true;

              if (
                !availableDate.availableTimes ||
                !availableDate.availableTimes.length ||
                availableDate.availableTimes.indexOf(model.selectedTime) == -1
              ) {
                model.selectedTime = "";
              }
            }

            if (
              availableDate.availableTimes &&
              availableDate.availableTimes.length
            ) {
              availableDate.availableTimes.forEach(function(time) {
                if (model.availableTimes.indexOf(time) == -1) {
                  model.availableTimes.push(time);
                }
              });
            }
          });
        }

        // Sort the dates/times
        model.availableDates.sort(function(a, b) {
          return new Date(a.dateString) - new Date(b.dateString);
        });

        model.availableTimes.sort(function(a, b) {
          return parseInt(a.substr(0, 2)) - parseInt(b.substr(0, 2));
        });

        if (!foundSelectedDate) {
          model.selectedDate = "";
          model.selectedTime = "";
        }
        return url + page + window.location.search + window.location.hash;
        // Keep track of query params and hashes
       // return "file:///D:/Terminix/terminix/buyonline/address.html"; 
    }

      case STEP_CONFIRMATION:
        if (model.openSlotSelected) {
          model.selectedDate = model.openSlotSelected.schDate;
          model.selectedTime = model.openSlotSelected.schFrmTime;
        } else {
          model.selectedDate = "NONE";
          model.selectedTime = "";
        }
        break;
    }

    // Remember product contents because we want to keep them as an observable
    // But they are objects, so ko.mapping.fromJS() won't make them an observable
    var productContentMap = {};

    model.availableProducts.forEach(function(product) {
      // Get the content for the product from endeca
      if (product.content) {
        productContentMap[product.productId] = product.content;
      }

      // Set to null so it becomes an observable that can be updated later
      product.content = null;
      product.contentFailed = false;
    });

    // Unset the current step so no bindings are using the model while we update it
    currentStepObservable("");

    // Update the model object on the page
    self.model(ko.mapping.fromJS(model));
    self.modelLoaded(true);

    // Update any specific observables from the model
    currentStepObservable(model.currentStep);

    // Do any step-specific stuff for the model after the page has been update with new step
    switch (model.currentStep) {
      case STEP_ADDRESS:
        // Determine if we need to show the sqft dropdown to the user in Step-1
        // if (_ENABLE_PROPERTY_INFORMATION_TOGGLE) {
        //     $('.sqft-block').hide();
        // }
        // else {
        //     $('.sqft-block').show();
        // }
        break;

      case STEP_PRODUCTS:
        break;
      case STEP_SCHEDULE:
        // Reset selected time whenever date changes; update the timeSlotSelectedKey if NONE is selected
        self.model().selectedDate.subscribe(function(value) {
          self.model().selectedTime("");

          if (value === "NONE") {
            self.model().timeSlotSelectedKey(value);
          }
        });

        // Reset selected time whenever date changes; update the timeSlotSelectedKey with the appropriate slot key
        self.model().selectedTime.subscribe(function(value) {
          // Match the date object
          var dateMatch = ko.utils.arrayFirst(
            self.model().serviceSchedule(),
            function(item) {
              return self.model().selectedDate() === item.date();
            }
          );

          if (dateMatch) {
            // Match the time
            var timeMatch = ko.utils.arrayFirst(
              dateMatch.serviceTimeSlots(),
              function(item) {
                return self.model().selectedTime() === item.schFrmTime();
              }
            );

            // Set the slot key
            if (timeMatch) {
              self.model().timeSlotSelectedKey(timeMatch.schSlotKey());
            }
          }
        });

        // Also update selectedDate if noDatesChosen flag is NONE
        self.noDatesChosen.subscribe(function(value) {
          if (value === true) {
            self.model().selectedDate("NONE");
            FireFunnelGAEvent(
              GA_CATEGORY_FUNNEL,
              "Links and Buttons",
              "Checkbox: None of these dates works",
              ""
            );
          } else {
            self.model().selectedDate("");
          }
        });
        break;

      case STEP_PAYMENT:
        self.model().payment.cardNumber.subscribe(function(value) {
          var cardType = GetCreditCardType(value);

          if (cardType) self.model().payment.cardType(cardType.value);
        });

        if (self.model().payment.cardNumber()) {
          self.model().payment.cardNumber.valueHasMutated();
        }
        // Check if same as billing has been unchecked
        self.billingAddressSameAsServiceAddress.subscribe(function(value) {
          if (value === false) {
            var billingZip = self.model().customer.billingProperty.zipCode();
            self.model().customer.billingProperty.zip9Code(billingZip);
            self.model().customer.billingProperty.lotSize(null);
            self.model().customer.billingProperty.squareFootage(null);
            self.model().customer.billingProperty.addressVerified(false);
          }
        });

        break;

      case STEP_CONFIRMATION:
        // Clear out the session storage
        if (caniuse.sessionStorage) {
          sessionStorage.clear();
        }
        break;
    }

    // Pull endeca content for products if we need to
    self
      .model()
      .availableProducts()
      .forEach(function(product) {
        if (product.productId() in productContentMap) {
          product.content(productContentMap[product.productId()]);
        } else {
          self.GetProductContentFromEndeca(
            product.productCode(),
            product.templateCode(),
            function(json) {
              // Loop through all content and replace any template strings with values
              if (!json || !json.content || json.content.error) {
                product.contentFailed(true);
              } else {
                var unmappedProduct = ko.mapping.toJS(product);

                for (var c in json.content) {
                  if (!json.content.hasOwnProperty(c)) {
                    continue;
                  }

                  if (IsObject(json.content[c])) {
                    for (var item in json.content[c]) {
                      if (!json.content[c].hasOwnProperty(item)) {
                        continue;
                      }

                      json.content[c][item] = self.ReplacePricingTemplateKeys(
                        json.content[c][item],
                        unmappedProduct
                      );
                    }
                  } else {
                    json.content[c] = self.ReplacePricingTemplateKeys(
                      json.content[c],
                      unmappedProduct
                    );
                  }
                }

                // Put the content on the product
                product.content(json.content);

                // Update the session storage with the new model
                if (
                  caniuse.sessionStorage &&
                  self.currentStep() !== STEP_CONFIRMATION
                ) {
                  for (var step in stepCache) {
                    var json = stepCache[step]();

                    if (json && json.availableProducts) {
                      json.availableProducts.some(function(jsonProduct) {
                        if (jsonProduct.productId == product.productId()) {
                          jsonProduct.content = product.content();
                          return true;
                        }
                      });
                    }
                  }

                  UpdateSessionStorage();
                }
              }
            }
          );
        }
      });

    // Save the model and cache to session if possible
    if (self.currentStep() !== STEP_CONFIRMATION && caniuse.sessionStorage) {
      UpdateSessionStorage();
    }

    // Run actions
    if (
      self.model().actionList &&
      self.model().actionList() &&
      self.model().actionList().length
    ) {
      // TODO: create and implement an actions controller
      for (var a = 0; a < self.model().actionList().length; a++) {
        var action = self.model().actionList()[a];

        switch (action) {
          case "INVALID_EMAIL":
            new CommonModal(
              {
                title: "Email address verification error",
                body:
                  "<p>Sorry, we were unable to verify your email address <strong>" +
                  self.model().customer.contact.email() +
                  "</strong>. If this is the correct email address, please continue as is to resubmit, otherwise you may edit your email address and try again.</p>",
                confirm: true,
                btnCancelText: "Edit email",
                btnConfirmText: "Continue as is"
              },
              function() {
                self.model().customer.contact.emailVerified(true);
                self.SubmitStep();
              }
            );
            break;

          case "INVALID_ADDRESS":
            var addressline2, fullAddress, invalidType;

            if (!self.model().customer.serviceProperty.addressVerified()) {
              addressline2 =
                self.model().customer.serviceProperty.address2() != null
                  ? self.model().customer.serviceProperty.address2()
                  : "";
              fullAddress =
                self.model().customer.serviceProperty.address1() +
                " " +
                addressline2 +
                " " +
                self.model().customer.serviceProperty.zipCode();
              invalidType = "service";
            } else if (
              !self.model().customer.billingProperty.addressVerified()
            ) {
              addressline2 =
                self.model().customer.billingProperty.address2() != null
                  ? self.model().customer.billingProperty.address2()
                  : "";
              fullAddress =
                self.model().customer.billingProperty.address1() +
                " " +
                addressline2 +
                " " +
                self.model().customer.billingProperty.zipCode();
              invalidType = "billing";
            }

            new CommonModal(
              {
                title: "Address verification error",
                body:
                  "<p>Sorry, we were unable to verify your " +
                  invalidType +
                  " address <strong>" +
                  fullAddress +
                  "</strong>. If this is the correct address, please continue as is to resubmit, otherwise you may edit your address and try again.</p>",
                confirm: true,
                btnCancelText: "Edit address",
                btnConfirmText: "Continue as is"
              },
              function() {
                if (invalidType === "service") {
                  self.model().customer.serviceProperty.addressVerified(true);
                } else if (invalidType === "billing") {
                  self.model().customer.billingProperty.addressVerified(true);
                }

                self.SubmitStep();
              }
            );

            self
              .model()
              .actionList()
              .pop();
            break;

          case "INVALID_PHONE":
            break;
          case "DISABLE_SUBMIT_ORDER":
            if (self.model().currentStep() == STEP_PAYMENT) {
              $(".cart .card-footer").hide();
            } else if (self.model().currentStep() == STEP_SCHEDULE) {
              $("#submit-schedule-step").hide();
            }
            break;
        }
      }
    }

    // if (isChangingSteps) {
    //     // Scroll to top of the page to show this step
    //     ScrollTo(0, 'fast');

    //     // Fire page view
    //     try {
    //         utag.view({
    //             ga_page_including_hash: window.location.pathname + window.location.search + window.location.hash
    //         });
    //     } catch(e) {
    //         console.log(e);
    //     }

    // }
  };

  // Address functions
  // =========================
  this.HasAddressUnit = ko.computed(function(property) {
    if (self.modelLoaded() && self.model()) {
      var _property = ko.unwrap(property);
      if (_property && ko.unwrap(_property.address2)) {
        return true;
      }
    }

    return false;
  });

  // Product functions
  // =========================

  /**
   * Checks if available products exist
   *
   * @return {Boolean}
   */
  this.HasAvailableProducts = ko.computed(function() {
    return (
      self.modelLoaded() &&
      self.model() &&
      self.model().availableProducts &&
      self.model().availableProducts().length
    );
  });

  /**
   * Checks if the products have content
   *
   * @return {Boolean}
   */
  this.ProductsHaveContent = ko.computed(function() {
    return (
      self.HasAvailableProducts() &&
      self
        .model()
        .availableProducts()
        .every(function(product) {
          return product.content();
        })
    );
  });

  /**
   * Checks if the products failed to load content
   *
   * @return {Boolean}
   */
  this.ProductsHaveFailedContent = ko.computed(function() {
    return (
      self.HasAvailableProducts() &&
      self
        .model()
        .availableProducts()
        .some(function(product) {
          return product.contentFailed();
        })
    );
  });

        var isChangingSteps = model.currentStep != self.currentStep();

        // Check if step changed and we should update in history
        if (g_isHistorySupported && !ignoreHistory) {
            // Update the history trail
            if (isChangingSteps || !stepHistory.length) {
                stepHistory.push(self.currentStep());
            }

            stepCache[model.currentStep]($.extend(true, {}, model));

            history.replaceState(stepHistory, "", GetStepURL(model.currentStep));
        }

    return (
      unmappedProduct && unmappedProduct.content.tiered.hasOwnProperty("ribbon")
    );
  };

  /**
   * Checks if the passed product should be a la carte
   *
   * @param {Object} product The product to check
   * @param {Boolean} tiered If the path is tiered or not
   * @return {Boolean}
   */
  this.IsALaCarteProduct = function(product, tiered, interestArea) {
    tiered = tiered || false;
    interestArea = interestArea || self.model().interestArea();
    if (!product) return false;

    // If mosquito interest area and product is mosquito, fail
    if (interestArea === "MOSQUITOES" && product.productId === "10003") {
      return false;
    }

    // If tiered product, fail
    if (
      product.productId === "10005" ||
      product.productId === "10006" ||
      product.productId === "10007"
    ) {
      return false;
    }

    // If free inspection, fail
    if (product.productId === "10001") {
      return false;
    }

    return tiered;
  };

  /**
   * Checks if the passed product is tiered
   *
   * @param {Object} product The product to check
   * @return {Boolean}
   */
  this.IsTieredProduct = function(product) {
    if (!product) return false;

    return (
      product.productId === "10005" ||
      product.productId === "10006" ||
      product.productId === "10007"
    );
  };

  /**
   * Returns product payment frequency display value for payment frequency code.
   *
   * @param {String} paymentFrequencyCode
   * @return {String}
   */
  (this.GetPaymentFrequencyDisplay = function(
    paymentFrequencyCode,
    capitalized
  ) {
    if (capitalized === undefined) capitalized = true;
    switch (paymentFrequencyCode) {
      case "QTR":
        var paymentFrequencyDisplay = "quarterly";
        break;
      case "MTHLY":
        var paymentFrequencyDisplay = "monthly";
        break;
      case "BIMO":
        var paymentFrequencyDisplay = "bi-monthly";
        break;
      case "ONCE":
        var paymentFrequencyDisplay = "one-time";
        break;
      default:
        var paymentFrequencyDisplay = paymentFrequencyCode;
    }
    if (capitalized) {
      return (
        paymentFrequencyDisplay.charAt(0).toUpperCase() +
        paymentFrequencyDisplay.slice(1)
      );
    } else {
      return paymentFrequencyDisplay;
    }
  }),
    /**
     * Returns product payment frequency per value (e.g. "month" for "per month") for payment frequency code.
     *
     * @param {String} paymentFrequencyCode
     * @return {String}
     */
    (this.GetPaymentFrequencyPerValue = function(paymentFrequencyCode) {
      switch (paymentFrequencyCode) {
        case "QTR":
          var paymentFrequencyPerValue = "quarter";
          break;
        case "MTHLY":
          var paymentFrequencyPerValue = "month";
          break;
        case "BIMO":
          var paymentFrequencyPerValue = "two months";
          break;
        default:
          var paymentFrequencyPerValue = paymentFrequencyCode;
      }
      return paymentFrequencyPerValue;
    });

  /**
     * Retrieves the content from endeca for a product
     *
    //  * @param {String} pc The product code of the product
    //  * @param {String} tc The template code of the product
    //  * @param {Function} callback The function to call when the items have returned
     */
  (this.GetProductContentFromEndeca = function(pc, tc, callback) {
    if (!pc || !tc || !callback) {
      console.log(
        "[GetProductContentFromEndeca] Product code, Template code, and callback are required."
      );
      callback({});
      return;
    }

    var data = {
      productCode: pc,
      templateCode: tc,
      folder: "Funnel"
    };

    GetEndecaContent(data, callback);
  }),
    /**
     * Replaces the template keys inside a string to prices
     *
     * @param {String} content The string to replace the template keys of
     * @param {Object} product The product to use for pricing
     * @return {String} The content with template keys replaced
     */
    (this.ReplacePricingTemplateKeys = function(content, product) {
      if (!content || typeof content !== "string" || !product) {
        return;
      }

      // Define which template keys we support for the content
      var templateKeys = {};

      // Initial price tag (original price - discounted price)
      templateKeys.initialPriceTag =
        "$" + FormatNumber(product.productCost - product.discount, 0);

      // Follow-up price tag for service
      templateKeys.followUpPriceTag =
        "$" + FormatNumber(product.frequencyCost, 0);

      // Replace any template strings in the content
      for (var templateKey in templateKeys) {
        var templateKeyStr = "{{" + templateKey + "}}";
        var templateReplacement = templateKeys[templateKey];

        content = content.replaceAll(templateKeyStr, templateReplacement);
      }

      return content;
    });

  // Cart functions
  // =========================

  // Returns whether to show to the cart or not
  //  - more possible logic to be added later
  this.CartIsShowing = ko.computed(function() {
    if (!self.modelLoaded() || !self.model()) return false;

    var ret = false;

    switch (self.currentStep()) {
      case STEP_ADDRESS:
        break;

      case STEP_PRODUCTS:
        ret = self.IsFreeInspectionPath()
          ? self.ProductsHaveContent() &&
            self.model().interestArea() === "TERMITES"
          : self.ProductsHaveContent();
        break;

      case STEP_SCHEDULE:
        ret = self.ProductsHaveContent() && self.HasProductsInCartWithPrice();
        break;

      case STEP_PAYMENT:
        ret = self.ProductsHaveContent();
        break;

      case STEP_CONFIRMATION:
        break;
    }

    return ret;
  });

  // Returns the text for the primary CTA in the cart
  this.GetCartCTAText = ko.computed(function() {
    if (!self.modelLoaded() || !self.model()) return "";

    switch (self.currentStep()) {
      case STEP_ADDRESS:
      case STEP_PRODUCTS:
      case STEP_SCHEDULE:
      case STEP_CONFIRMATION:
        var ret = "Next";
        break;

      case STEP_PAYMENT:
        var ret = "Submit";
        break;
    }

    return ret;
  });

  // Returns the text for the primary CTA in the cart when submitting
  this.GetSubmittingCartCTAText = ko.computed(function() {
    if (!self.modelLoaded() || !self.model()) return "";

    switch (self.currentStep()) {
      case STEP_PRODUCTS:
        var ret = "Loading scheduling...";
        break;

      case STEP_SCHEDULE:
        var ret =
          self.submittingStep() == STEP_PRODUCTS
            ? "Loading scheduling..."
            : "Loading payment...";
        break;

      case STEP_PAYMENT:
        var ret = "Purchasing...";
        break;

      case STEP_ADDRESS:
      case STEP_CONFIRMATION:
        var ret = "";
        break;
    }

    return ret;
  });

  // Returns whether a user can remove items from the cart
  this.CanRemoveItemsFromCart = ko.computed(function() {
    if (!self.modelLoaded() || !self.model()) return false;

    // This is initial setup for more logic later (ex: up-sell sections)
    switch (self.currentStep()) {
      case STEP_ADDRESS:
      case STEP_SCHEDULE:
      case STEP_PAYMENT:
      case STEP_CONFIRMATION:
        var ret = false;
        break;

      case STEP_PRODUCTS:
        var ret = self.submittingStep() !== STEP_PRODUCTS;
        break;
    }

    return ret;
  });

  /**
   * Handles the setup to add/remove items to/from the cart
   *
   * @param item The product to add or remove
   */
  this.HandleAddRemoveProductFromCart = function(item) {
    if (!item) return;

    var product = ko.mapping.toJS(item);

    // If product is in cart, remove it
    if (self.IsProductInCart(product)) {
      self.RemoveProductFromCart(product);

      // If product is not in cart add it
    } else {
      // Check the cart for product combinations and rules
      if (!self.ProductTriggeredCartRule(product)) {
        // If there isn't a rule trigger, add the item to cart
        self.AddProductToCart(product);
      }
    }

    // Sets all future steps to null
    // This is if the user went from schedule/pay back to products, and removed/added an item, they need to pull schedule again
    ClearStepCache({ after: self.currentStep() });
  };

  /**
   * Adds a product to the cart
   *
   * @param product The product to add to the cart
   */
  this.AddProductToCart = function(product) {
    if (!product && !product.productId) return;

    self.model().productIdCart.push(product.productId);

    if (self.IsFreeInspectionPath()) {
      isShowStep[STEP_PAYMENT](true);
    }
  };

  /**
   * Removes a product from the cart
   *
   * @param product The product to remove from the cart
   */
  this.RemoveProductFromCart = function(product) {
    if (!product && !product.productId) return;

    self.model().productIdCart.remove(product.productId);

    if (self.IsFreeInspectionPath()) {
      isShowStep[STEP_PAYMENT](false);
    }

    self.HandleEnableProductsAfterRemove(product.productId);
  };

  /**
   * Checks if tiered product is in cart
   * @return {Boolean} true if tiered product in cart.
   */
  this.IsTieredProductInCart = function() {
    return self
      .model()
      .productIdCart()
      .some(function(productId) {
        return (
          productId === "10005" ||
          productId === "10006" ||
          productId === "10007"
        );
      });
  };

  /**
   * Checks if alacarte product is in cart
   * @return {Boolean} true if alacarte product in cart.
   */
  this.IsALaCarteProductInCart = function() {
    return self.productsInCart().some(function(product) {
      return product.isALaCarte();
    });
  };

  /**
   * Checks if only one-time products in cart.
   * @return {Boolean} true if only one-time products are in cart.
   */
  this.IsOnlyOnetimesInCart = ko.computed(function() {
    if (!self.modelLoaded() || !self.model() || !self.model().productItemMap)
      return false;
    return !self
      .model()
      .productIdCart()
      .some(function(productId) {
        return (
          self.model().productItemMap[productId] &&
          self.model().productItemMap[productId].paymentFrequency() !== "ONCE"
        );
      });
  });

  /**
   * Checks if any paid item exist in cart.
   * @return {Boolean} true if any paid item is in cart.
   */
  this.IsPaidItemInCart = ko.computed(function() {
    if (!self.modelLoaded() || !self.model() || !self.model().productItemMap)
      return false;
    return self
      .model()
      .productIdCart()
      .some(function(productId) {
        return productId !== "10001";
      });
  });

  // Default to a rule set if one does not exist in the global ui settings object
  var _PRODUCT_RULESET = {
    "10001": {
      _name: "Free Termite Inspection",
      disabled: [],
      swap: ["10004", "10006", "10007"],
      bundles: []
    },
    "10002": {
      _name: "Genpest products (quarterly)",
      disabled: [],
      swap: ["10005"],
      bundles: []
    },
    "10003": {
      _name: "Mosquito (ATSB / Quick Guard)",
      disabled: [],
      swap: ["10009"],
      bundles: [
        {
          productsInCart: ["10004", "10005"],
          productToUpgradeTo: "10007",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
        },
        {
          productsInCart: ["10006"],
          productToUpgradeTo: "10007",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
        }
      ]
    },
    "10004": {
      _name: "Termite one time",
      disabled: [],
      swap: ["10001"],
      bundles: [
        {
          productsInCart: ["10003", "10005"],
          productToUpgradeTo: "10007",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
        },
        {
          productsInCart: ["10005"],
          productToUpgradeTo: "10006",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
        },
        {
          productsInCart: ["10008"],
          productToUpgradeTo: "10006",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
        }
      ]
    },
    "10005": {
      _name: "Silver plan",
      disabled: [],
      swap: ["10002", "10006", "10007", "10008"],
      bundles: [
        {
          productsInCart: ["10003", "10004"],
          productToUpgradeTo: "10007",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
        },
        {
          productsInCart: ["10004"],
          productToUpgradeTo: "10006",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
        }
      ]
    },
    "10006": {
      _name: "Gold plan",
      disabled: [
        {
          productToDisable: "10002",
          message: false
        },
        {
          productToDisable: "10004",
          message: true
        },
        {
          productToDisable: "10008",
          message: false
        }
      ],
      swap: ["10001", "10002", "10004", "10005", "10007", "10008"],
      bundles: [
        {
          productsInCart: ["10003"],
          productToUpgradeTo: "10007",
          message:
            "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
        }
      ]
    },
    "10007": {
      _name: "Platinum plan",
      disabled: [
        {
          productToDisable: "10002",
          message: false
        },
        {
          productToDisable: "10003",
          message: true
        },
        {
          productToDisable: "10004",
          message: true
        },
        {
          productToDisable: "10008",
          message: false
        }
      ],
      swap: ["10001", "10002", "10003", "10004", "10005", "10006", "10008"],
      bundles: []
    },
    "10008": {
      _name: "Genpest one time",
      disabled: [],
      swap: ["10005"],
      bundles: []
    },
    "10009": {
      _name: "Mosquito one time",
      disabled: [],
      swap: ["10003"],
      bundles: []
    }
  };

  // if (typeof _GLOBAL_UI_SETTINGS != 'undefined' && _GLOBAL_UI_SETTINGS && _GLOBAL_UI_SETTINGS.productRuleSet) {
  //     _PRODUCT_RULESET = _GLOBAL_UI_SETTINGS.productRuleSet;
  // }

  /**
   * Checks for a cart rule and returns if one is triggered or not
   *
   * @param {Object} productToAdd The product being added to cart by the user
   * @return {Boolean}
   **/
  self.ProductTriggeredCartRule = function(productToAdd) {
    var productsInCart = ko.mapping.toJS(self.model().productIdCart()), // Array of product ids
      rules = _PRODUCT_RULESET[productToAdd.productId];

    // Check for bundle upgrades
    for (var b = 0; b < rules.bundles.length; b++) {
      var bundle = rules.bundles[b];

      if (ArrayContainsArray(productsInCart, bundle.productsInCart)) {
        // Define which template keys we support for the content
        var templateKeys = {};

        // Bundle savings amount
        // This is equal to the difference of the cost all of the products in the bundle, minus the cost of the bundled product
        templateKeys.bundleSavings = (function GetBundleSavings() {
          var cartItemsCost = parseFloat(productToAdd.productCost) || 0;
          var bundleProductCost = 0;

          bundle.productsInCart.forEach(function(productId) {
            var product = self
              .model()
              .availableProducts()
              .find(function(product) {
                return product.productId() == productId;
              });

            if (product) {
              cartItemsCost += parseFloat(product.productCost()) || 0;
            }
          });

          var bundledProduct = self
            .model()
            .availableProducts()
            .find(function(product) {
              return bundle.productToUpgradeTo == product.productId();
            });

          if (bundledProduct) {
            bundleProductCost = parseFloat(bundledProduct.productCost()) || 0;
          }

          return (cartItemsCost - bundleProductCost).toFixed(2);
        })();

        // Replace any template strings in the content
        var bundleMessage = bundle.message;

        for (var templateKey in templateKeys) {
          var templateKeyStr = "{{" + templateKey + "}}";
          var templateReplacement = templateKeys[templateKey];

          bundleMessage = bundleMessage.replaceAll(
            templateKeyStr,
            templateReplacement
          );
        }

        new CommonModal(
          {
            title: "Bundle Upgrade",
            body: bundleMessage,
            confirm: true,
            btnCancelText: "No thanks",
            btnConfirmText: "Upgrade"

            // Confirm callback
          },
          function() {
            var item = self
              .model()
              .availableProducts()
              .filter(function(item) {
                return item.productId() === bundle.productToUpgradeTo;
              });

            self.HandleAddRemoveProductFromCart(item[0]);

            // Cancel callback
          },
          function() {
            self.HandleRuleSwapAndDisable(rules);
            self.AddProductToCart(productToAdd);
          }
        );

        return true;
      }
    }

    self.HandleRuleSwapAndDisable(rules);
    return false;
  };

  /**
   * Handles the swap and disable rules for a product
   *
   * @param {Object} rules The rule set to follow
   **/
  this.HandleRuleSwapAndDisable = function(rules) {
    var productsInCart = ko.mapping.toJS(self.model().productIdCart()); // Array of product ids

    // Check for items to swap out of the cart
    rules.swap.forEach(function(productId) {
      var itemId = productsInCart.filter(function(itemId) {
        return itemId === productId;
      });

      if (itemId.length) {
        var productToRemove = self.productsInCart().filter(function(product) {
          return product.productId() === itemId[0];
        });

        self.HandleAddRemoveProductFromCart(productToRemove[0]);
      }
    });

    // Check for items to disable
    rules.disabled.forEach(function(product) {
      var item = self
        .model()
        .availableProducts()
        .filter(function(item) {
          return item.productId() === product.productToDisable;
        });

      if (item.length) {
        item[0].isDisabled(true);
      }
    });
  };

  /**
   * Enables products based on disabled rule set
   *
   * @param {object} productId The product id of the rule set
   */
  this.HandleEnableProductsAfterRemove = function(productId) {
    var disabledProducts = _PRODUCT_RULESET[productId].disabled;

    disabledProducts.forEach(function(itemId) {
      var product = self
        .model()
        .availableProducts()
        .filter(function(p) {
          return p.productId() === itemId.productToDisable;
        });

      if (product.length) {
        product[0].isDisabled(false);
      }
    });
  };

  /**
   * Checks if a product is in the cart
   *
   * @param {object} item The product to check
   * @returns {boolean}
   */
  this.IsProductInCart = function(item) {
    var product = ko.mapping.toJS(item),
      model = ko.mapping.toJS(self.model);

    return model.productIdCart.indexOf(product.productId) > -1;
  };

  // Array of products in the cart
  this.productsInCart = ko.computed(function() {
    if (!self.modelLoaded()) return [];

    return self
      .model()
      .availableProducts()
      .filter(function(product) {
        return (
          self
            .model()
            .productIdCart()
            .indexOf(product.productId()) > -1
        );
      });
  });

  // Whether there is a product in the cart with a price
  this.HasProductsInCartWithPrice = ko.computed(function() {
    return self.productsInCart().some(function(product) {
      return product.startUpCostTotal();
    });
  });

  // The totals for the cart
  this.cartTotals = ko.computed(function() {
    if (!self.modelLoaded()) return {};

    var totals = {
        subtotal: 0,
        discount: 0,
        tax: 0,
        due: 0
      },
      products = self.productsInCart();

    products.forEach(function(product) {
      totals.subtotal += product.productCost();
      totals.discount += product.discount();
      totals.tax += product.taxes();

      if (product.discount() > 0) {
        totals.due += product.startUpCostTotal();
      } else {
        totals.due += product.startUpCostTotalWithoutDiscount();
      }
    });

    return totals;
  });

  // Scheduling functions
  // =========================

  this.FormatDateDisplayMonth = function(dateString) {
    var date = new Date(dateString);

    return GetMonthNameShort(date.getUTCMonth() + 1);
  };

  this.FormatDateDisplayDay = function(dateString) {
    var date = new Date(dateString);

    return date.getUTCDate();
  };

  this.IsTimeAvailable = function(time) {
    if (!self.modelLoaded() || !self.model().selectedDate()) return false;

    var selectedDate = self
      .model()
      .availableDates()
      .find(function(availableDate) {
        return availableDate.dateString() == self.model().selectedDate();
      });

    return !selectedDate
      ? false
      : selectedDate.availableTimes().indexOf(time) >= 0;
  };

  this.FormatTimeSlot = function(startTime) {
    startTime = parseInt(startTime.substr(0, 2));
    var endTime = startTime + 2;
    var startSuffix = startTime > 12 ? "PM" : "AM";
    var endSuffix = endTime > 11 ? "PM" : "AM";

    if (startTime > 12) startTime -= 12;
    if (endTime > 12) endTime -= 12;

    return startTime + startSuffix + " - " + endTime + endSuffix;
  };

  this.HasScheduleItemSelected = function() {
    if (self.modelLoaded() && self.model()) {
      var selectedKey = ko.unwrap(self.model().timeSlotSelectedKey);
      if (selectedKey) {
        return true;
      }
    }

    return false;
  };

  // Step functions
  // =========================

  this.GetStepText = function(step) {
    return stepTextMap[step] || "";
  };

  this.IsStepComplete = function(step) {
    if (!self.modelLoaded()) return false;

    // If unrecognized or current step, then don't show as complete
    var stepIndex = defaultStepOrder.indexOf(step);
    if (stepIndex == -1 || stepIndex == currentStepIndex()) return false;

    // If step is a previous step, then allow it
    if (stepIndex < currentStepIndex()) return true;

    // Step must be a future step
    // Check if step is in history
    return stepCache[step]() !== null;
  };

  this.IsStepDisabled = function(step) {
    if (!self.modelLoaded()) return false;

    // If on confirmation step, then disable all other steps
    if (currentStepObservable() == STEP_CONFIRMATION) return true;

    // If this step is not already complete, then disable it
    if (!self.IsStepComplete(step)) return true;

    return false;
  };

  this.CanGoToStep = function(step) {
    return !self.submitting() && !self.IsStepDisabled(step);
  };

  this.GoToStep = function(step) {
    if (!self.CanGoToStep(step)) return;

    if (step == STEP_ADDRESS) {
      stepCache[step]().customer.serviceProperty.addressVerified = false;
    }

    FireFunnelGAEvent(
      GA_CATEGORY_FUNNEL,
      "Click Step " + GetStepName(step),
      "From Step " + GetStepName(self.currentStep()),
      ""
    );

    // This should always be populated since self.IsStepComplete checks for this
    // But just in case, don't leave opportunity for errors
    if (step in stepCache && stepCache[step]() !== null) {
      UpdateModel(stepCache[step]());
    }
  };

  this.StepBack = function() {
    if (!self.modelLoaded() || self.submitting() || currentStepIndex() < 1)
      return;

    self.GoToStep(stepOrder()[currentStepIndex() - 1]);
  };

  this.StepCanBeSubmitted = ko.computed(function() {
    switch (self.currentStep()) {
      case STEP_ADDRESS:
        return self.modelLoaded();

      case STEP_PRODUCTS:
        //return self.HasPrimaryProduct();
        break;

      case STEP_SCHEDULE:
        return self.HasScheduleItemSelected();

      case STEP_PAYMENT:
        if (
          self.model().actionList &&
          self.model().actionList() &&
          self.model().actionList().length > 0 &&
          self.model().actionList()[0] == "DISABLE_SUBMIT_ORDER"
        ) {
          return false;
        }
        break;
    }

    return true;
  });

  this.IsValidStep = function() {
    switch (self.currentStep()) {
      case STEP_ADDRESS:
        var result = true;
        result =
          self.validator.test("customer.serviceProperty.address1") && result;
        result =
          self.validator.test("customer.serviceProperty.zipCode") && result;

        if (self.addressValidator.ziptasticFail()) {
          result =
            self.validator.test("customer.serviceProperty.city") && result;
          result =
            self.validator.test("customer.serviceProperty.state") && result;
        }

        result = self.validator.test("customer.contact.email") && result;
        result = self.validator.test("customer.contact.phone") && result;

        if (!result) {
          self.PostError(
            "One or more form items are invalid. Please check the form for errors."
          );
          return false;
        }

        return true;
        break;

      case STEP_PRODUCTS:
        break;

      case STEP_SCHEDULE:
        var result = true,
          errorMsg = "";

        // Fail if we're on free inspection path and the first and last name aren't populated
        if (!self.HasProductsInCartWithPrice()) {
          if (
            !self.validator.test("customer.firstName") ||
            !self.validator.test("customer.lastName")
          ) {
            result = false && result;
            errorMsg = "Please fill out the home owner's first and last name.";
          }
        }

        // Fail if there isn't a selectedDate and selectedTime, or selectedDate isn't set to NONE
        if (
          !(
            (self.model().selectedDate() && self.model().selectedTime()) ||
            self
              .model()
              .selectedDate()
              .toUpperCase() === "NONE"
          )
        ) {
          result = false && result;
          errorMsg =
            "Please select a date and time to schedule your appointment.";
        }

        if (!result) {
          self.PostError(errorMsg);
          return false;
        }

        return true;
        break;

      case STEP_PAYMENT:
        var result = true;

        result = self.validator.test("customer.firstName") && result;
        result = self.validator.test("customer.lastName") && result;

        if (!self.billingAddressSameAsServiceAddress()) {
          result =
            self.validator.test("customer.billingProperty.address1") && result;
          result =
            self.validator.test("customer.billingProperty.city") && result;
          result =
            self.validator.test("customer.billingProperty.state") && result;
          result =
            self.validator.test("customer.billingProperty.zipCode") && result;
        }

        switch (self.model().payment.paymentMethod()) {
          case "card":
            result = self.validator.test("payment.cardNumber") && result;
            result = self.validator.test("payment.cardExpire") && result;

            // Remove spaces from CC number
            if (result) {
              self.model().payment.cardNumber(
                self
                  .model()
                  .payment.cardNumber()
                  .replace(/\s/g, "")
              );
            }
            break;

          case "bank":
            result = self.validator.test("payment.bankAccountNumber") && result;
            result = self.validator.test("payment.bankRoutingNumber") && result;
            break;

          default:
            return false;
        }

        result = self.validator.test("customer.contact.phone") && result;
        result = self.validator.test("customer.contact.email") && result;

        if (!result) {
          self.PostError(
            "One or more form items are invalid. Please check the form for errors."
          );
          return false;
        }

        return true;
        break;
    }

    return true;
  };

  this.PostError = function(errorMessage, messageColor, errorCode) {
    if (!errorMessage) return;

    var msg = {
      errorMessage: errorMessage || "An error has occurred.",
      messageColor: messageColor || null,
      errorCode: errorCode || "GENERAL_ERROR"
    };

    self.model().errorMessages.push(msg);
    ScrollTo($(".error-msg.bg-danger.text-danger").offset().top - 100, 500);
  };

  this.SubmitStep = function() {
    self.RemoveAllMessages();

    if (!self.IsValidStep()) return;

    self.submittingStep(self.currentStep());
    self.submitting(true);

    var json = GetModelJSON();

    self.CheckStepAddressValidation(json, function() {
      var dwr = "";
      json.liveChat = JSON.parse(sessionStorage.getItem("LiveChatState")) || {
        visitorId: null,
        hasChatOccurred: false
      };

      switch (self.currentStep()) {
        case STEP_ADDRESS:
          dwr = "TMXPurchaseFunnelUIUtils/getProductsAndPricing";
          break;
        case STEP_PRODUCTS:
          dwr = "TMXPurchaseFunnelUIUtils/getScheduleAvailability";
          break;
        case STEP_SCHEDULE:
          dwr = "TMXPurchaseFunnelUIUtils/submitSchedule";
          break;
        case STEP_PAYMENT:
          dwr = "TMXPurchaseFunnelUIUtils/submitOrder";
          break;
      }

      if (!dwr) throw "Unrecognized funnel step.";

      // Check if we should show loading on the next step
      if (currentStepObservable() == STEP_PRODUCTS) {
        // Go to next step and show loading there
        currentStepObservable(STEP_SCHEDULE);

        // Scroll to top of the page to show this step
        ScrollTo(0, "fast");
      }

      CallDWR(dwr, json, function(data) {
        var errorMessage =
          data.error ||
          (data.model &&
            data.model.errorMessages &&
            data.model.errorMessages.length &&
            data.model.errorMessages[0].errorMessage);

        if (errorMessage) {
          switch (self.currentStep()) {
            // Step 2 errors: Failed to retrieve pricing
            case STEP_PRODUCTS:
              // TODO: add non 8k logic
              if (
                data.model &&
                data.model.actionList &&
                data.model.actionList.length &&
                data.model.actionList[0] === "INVALID_ADDRESS"
              ) {
                self.PostError(errorMessage);
                FireFunnelGAEvent(
                  GA_CATEGORY_FUNNEL,
                  "Submit Address Validation",
                  "Error: " + errorMessage,
                  ""
                );
              } else {
                currentStepObservable(json.currentStep);
                self.PostError(errorMessage);
                FireFunnelGAEvent(
                  GA_CATEGORY_FUNNEL,
                  "Retrieve Pricing",
                  "Error: Failed to retrieve pricing.",
                  ""
                );
              }
              break;

            // Step 3 errors: This occurs whenever scheduler times out or returns no dates
            case STEP_SCHEDULE:
              // If we didn't get a timeout, then an error was thrown
              if (
                !data.textStatus ||
                data.textStatus.toLowerCase() !== "timeout"
              ) {
                self.PostError(errorMessage);
                FireFunnelGAEvent(
                  GA_CATEGORY_FUNNEL,
                  "Submit Schedule Step",
                  "Error: " + errorMessage,
                  ""
                );

                // Timeout occurred when trying to retrieve scheduling information
              } else {
                currentStepObservable(json.currentStep);

                // Set step 3 default date to NONE
                json.currentStep = STEP_SCHEDULE;
                self.noDatesChosen(true);

                // Forget any cached model that is after this step
                ClearStepCache({ after: json.currentStep });

                stepCache[json.currentStep](json);

                FireFunnelGAEvent(
                  GA_CATEGORY_FUNNEL,
                  "Retrieve Schedule",
                  "Error: Failed to retrieve schedule.",
                  ""
                );
                UpdateModel(json);
              }
              break;

            case STEP_PAYMENT:
              self.PostError(errorMessage);
              FireFunnelGAEvent(
                GA_CATEGORY_FUNNEL,
                "Submit Order",
                "Error: " + errorMessage,
                ""
              );
              break;

            case STEP_ADDRESS:
            case STEP_CONFIRMATION:

            default:
              //console.log(errorMessage);
              self.PostError(errorMessage);
              break;
          }

          if (
            data.model &&
            data.model.actionList &&
            data.model.actionList.length
          ) {
            UpdateModel(data.model);
          }
        } else {
          switch (self.currentStep()) {
            case STEP_ADDRESS:
              break;

            case STEP_PRODUCTS:
              //FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Retrieve Pricing', 'Success: ' + data.model.primaryProduct.productCode + ' ' + data.model.primaryProduct.templateCode, '');
              break;

            case STEP_SCHEDULE:
              // This is for firing that we retrieved schedule
              FireFunnelGAEvent(
                GA_CATEGORY_FUNNEL,
                "Retrieve Schedule",
                "Success",
                ""
              );
              break;

            case STEP_PAYMENT:
              break;

            default:
              console.log("-- default success?");
              break;
          }

          // Update whether the products and payment steps should be shown
          switch (json.currentStep) {
            case STEP_ADDRESS:
              // If jumped from address to schedule, then no pricing step
              // Also, there would be no payment step
              if (data.model.currentStep == STEP_SCHEDULE) {
                isShowStep[STEP_PRODUCTS](false);
                isShowStep[STEP_PAYMENT](false);
              } else {
                isShowStep[STEP_PRODUCTS](true);

                if (self.IsFreeInspectionPath()) {
                  isShowStep[STEP_PAYMENT](false);
                } else {
                  isShowStep[STEP_PAYMENT](true);
                }
              }
              break;

            case STEP_SCHEDULE:
              // If jumped from schedule to confirmation, then no payment step
              if (data.model.currentStep == STEP_CONFIRMATION) {
                isShowStep[STEP_PAYMENT](false);
              } else {
                isShowStep[STEP_PAYMENT](true);
              }
              break;
          }

          // Go back to old step so we can detect step changes and such
          currentStepObservable(json.currentStep);

          // Check if user changed steps
          if (json.currentStep != data.model.currentStep) {
            // Forget any cached model that is after this step
            ClearStepCache({ after: data.model.currentStep });

            // Check for confirmation step for conversions
            if (data.model.currentStep == STEP_CONFIRMATION) {
              // Get information for GA Conversion
              var gaConversion =
                data.model && data.model.sendInfoToGoogleAnalytics
                  ? data.model.sendInfoToGoogleAnalytics
                  : "";

              // Send GA Conversion
              if (gaConversion) {
                try {
                  utag.link(data.model.sendInfoToGoogleAnalytics);
                } catch (e) {
                  console.log(e);
                }
              }

              // Fire Submit Order Event(s)
              var productCodeString = "",
                productsInCart = ko.mapping.toJS(self.productsInCart());
              var freeInspOnly = false;
              var gaValueTotal = 0;

              for (var i = 0; i < productsInCart.length; i++) {
                freeInspOnly =
                  productsInCart.length == 1 &&
                  productsInCart[i].productId == "10001";
                var gaValue = parseFloat(productsInCart[i].productCost);
                gaValue = gaValue ? gaValue : 0;
                gaValueTotal += gaValue;
                if (data.model.showUpsell) {
                  var upsellLabel = freeInspOnly
                    ? "FREEINSPONLY"
                    : productsInCart[i].productCode;
                  FireFunnelGAEvent(
                    GA_CATEGORY_FUNNEL,
                    "UPSELL",
                    upsellLabel,
                    gaValue
                  );
                }
                if (data.model.tiered) {
                  FireFunnelGAEvent(
                    GA_CATEGORY_FUNNEL,
                    "TIERED",
                    productsInCart[i].productCode,
                    gaValue
                  );
                }
                if (!productCodeString) {
                  productCodeString =
                    productsInCart[i].productCode +
                    ": " +
                    productsInCart[i].templateCode;
                } else {
                  productCodeString +=
                    " / " +
                    productsInCart[i].productCode +
                    ": " +
                    productsInCart[i].templateCode;
                }
              }

              FireFunnelGAEvent(
                GA_CATEGORY_FUNNEL,
                "Submit Order",
                "Success: " + productCodeString,
                gaValueTotal
              );

              // Check for Affiliate conversion
              if (self.IsAffiliatePath()) {
                var affiliate = ko.unwrap(self.model().commercialAffiliate);

                FireAffiliateGAEvent(
                  { id: affiliate.id(), name: affiliate.name() },
                  "Conversions"
                );
              }

              // Check for Free Inspection conversion
              var freeInspectionConversion =
                data.model && data.model.productIdCart.indexOf("10001") > -1;

              if (self.IsFreeInspectionPath() && freeInspectionConversion) {
                FireFreeInspectionConversion();
              }
            }
          }

          // stepCache[json.currentStep](json);

          UpdateModel(data.model);
        }

        self.submitting(false);
      });
    });
  };

  // this.SubmitStep = function() {
  //     errorMessages.removeAll();
  //     debugger;
  //     if (!self.IsValidStep()){
  //         errorMessages.push("One or more form items are invalid. Please check the form for errors");
  //         ScrollTo(0, 'fast');
  //         submitting(false);
  //         return;
  //     }

  //     isValidating(true);
  //     this.submittingStep(currentStep());
  //     this.submitting(true);

  //     this.CheckStepAddressValidation(function(){
  //         window.sessionStorage.setItem("funnelData",JSON.stringify(ko.mapping.toJS(customer)));
  //         window.sessionStorage.setItem("currentStep",STEP_PRODUCTS);
  //         submitting(false);

  //         var dwr = '';

  //         switch (self.currentStep()) {
  //             case STEP_ADDRESS: dwr = 'TMXPurchaseFunnelUIUtils/getProductsAndPricing'; break;
  //             case STEP_PRODUCTS: dwr = 'TMXPurchaseFunnelUIUtils/getScheduleAvailability'; break;
  //             case STEP_SCHEDULE: dwr = 'TMXPurchaseFunnelUIUtils/submitSchedule'; break;
  //             case STEP_PAYMENT: dwr = 'TMXPurchaseFunnelUIUtils/submitOrder'; break;
  //         }
  //         // if (!dwr) throw "Unrecognized funnel step.";

  //         // CallDWR(dwr, json, function(data) {
  //         //     var errorMessage = data.error || data.model && data.model.errorMessages && data.model.errorMessages.length && data.model.errorMessages[0].errorMessage;
  //         window.location.href = "../buyonline/pricing.html";

  //         // });
  //     });
  // },

  // Initialization
  // =========================

  this.LoadModel = function() {
    CallDWR(
      "TMXPurchaseFunnelUIUtils/getPurchaseModel" + window.location.search,
      null,
      function(data) {
        var optionalAbandonMsg = "";
        if (window.location.search.indexOf("abandon=true") > -1) {
          optionalAbandonMsg = " On Abandon";
        }
        if (data.error) {
          self.error(data.error);
          FireFunnelGAEvent(
            GA_CATEGORY_FUNNEL,
            "Funnel Loaded" + optionalAbandonMsg,
            "Error: " + data.error,
            ""
          );
        } else {
          // Replace the history state on load with this default state
          if (g_isHistorySupported) {
            //history.pushState(stepHistory, "", GetStepURL(data.model.currentStep || defaultStepOrder[0]));
          }

          if (window.location.search.indexOf("abandon=true") > -1) {
            var tmpmodel = Object.create(data.model);
            tmpmodel.currentStep = STEP_ADDRESS;
            //   stepCache[defaultStepOrder[0]]($.extend(true, {}, tmpmodel));
          }
          // stepCache[data.model.currentStep || defaultStepOrder[0]]($.extend(true, {}, data.model));

          //  FireFunnelGAEvent(GA_CATEGORY_FUNNEL, 'Funnel Loaded' + optionalAbandonMsg, 'Success', '');

          // Fire affiliate event
          // if (!IsObjectEmpty(data.model.commercialAffiliate)) {
          //     FireAffiliateGAEvent(data.model.commercialAffiliate, 'Passed Through');
          // }

          UpdateModel(data.model, true);
        }
      }
    );
  };

  // self.UpdateModel = function(model) {
  //     UpdateModel(model);
  // };

  // Address validator class
  // =========================

  (this.availableProducts = ko.observableArray([]).extend({ deferred: true })),
    (this.productDatas = ko.observableArray([]).extend({ deferred: true })),
    (this.availableDates = ko.observableArray([]).extend({ deferred: true })),
    (this.availableTimes = ko.observableArray([]).extend({ deferred: true })),
    // this.selectedDate = ko.observable(),

    (this.contents = ko.observableArray([]).extend({ deferred: true })),
    (this.getProductPricingData = function() {
      debugger;

      var json = ko.mapping.toJS(funnelModel) || {};
      var dwr = "TMXPurchaseFunnelUIUtils/getProductsAndPricing";
      CallDWR(dwr, json, function(data) {
        UpdateModel(data.model, true);
        availableProducts(model().availableProducts());
      });
    });

  this.getScheduleData = function() {
    debugger;
    submittingStep(STEP_SCHEDULE);
    var json = ko.mapping.toJS(funnelModel) || {};
    var dwr = "TMXPurchaseFunnelUIUtils/getScheduleAvailability";
    CallDWR(dwr, json, function(data) {
      UpdateModel(data.model, true);
      availableDates(model().availableDates());
      availableTimes(model().availableTimes());
    });
  };

  this.ValidateAddress = function(address, callbackFn) {
    var json = {
      address1: StringToLower(address.address1()),
      address2: StringToLower(address.address2()),
      city: StringToLower(address.city()),
      state: StringToLower(address.state()),
      zipcode: StringToLower(address.zipCode())
    };

    CallDWR("MyAccountTMXUIUtils/validateAddress", { address: json }, function(
      data
    ) {
      var addresses;

      if (
        data.model &&
        data.model.alternativeAddresses &&
        data.model.alternativeAddresses.length
      ) {
        // HACK: Remove alternativeAddresses results that contain null address fields
        addresses = data.model.alternativeAddresses = data.model.alternativeAddresses.filter(
          function(alternativeAddress) {
            return (
              alternativeAddress &&
              alternativeAddress.address1 &&
              alternativeAddress.address1 != "null null" &&
              alternativeAddress.city &&
              alternativeAddress.city != "null" &&
              alternativeAddress.state &&
              alternativeAddress.state != "null" &&
              alternativeAddress.zipcode &&
              alternativeAddress.zipcode != "null"
            );
          }
        );
        // HACK: Change 'zipcode' to 'zipCode'
        addresses = addresses.map(function(address) {
          return {
            address1: address.address1,
            address2: address.address2,
            city: address.city,
            state: address.state,
            zipCode: address.zipcode
          };
        });
      }

    return {
        init: init,
        errorMessages: errorMessages,
        SubmitStep: SubmitStep,
        availableProducts: availableProducts,
        productDatas:productDatas,
        contents:contents,
        ProductsHaveContent:ProductsHaveContent,
        IsProductInCart:IsProductInCart,
        productsInCart:productsInCart,
        CanRemoveItemsFromCart:CanRemoveItemsFromCart,
        cartTotals:cartTotals,
        IsOnlyOnetimesInCart:IsOnlyOnetimesInCart,
        HandleAddRemoveProductFromCart: HandleAddRemoveProductFromCart,
        ProductsHaveFailedContent: ProductsHaveFailedContent,
        StepCanBeSubmitted: StepCanBeSubmitted,
        GetCartCTAText : GetCartCTAText,
        GetSubmittingCartCTAText: GetSubmittingCartCTAText,
        submitting: submitting,
        availableDates: availableDates,
        availableTimes: availableTimes,
        FormatDateDisplayMonth :FormatDateDisplayMonth,
        FormatDateDisplayDay : FormatDateDisplayDay,
        FormatTimeSlot: FormatTimeSlot,
        IsTimeAvailable: IsTimeAvailable,
        IsPaidItemInCart: IsPaidItemInCart,
        HasProductsInCartWithPrice :HasProductsInCartWithPrice,
        noDatesChosen: noDatesChosen,
        IsFreeInspectionPath : IsFreeInspectionPath,
        HasFieldError: HasFieldError,
        GetFieldError: GetFieldError,
        validator: validator,
        addressValidator: addressValidator,
        squareFootageOptions:squareFootageOptions,
        submittingStep: submittingStep,
        stepOrder: stepOrder,
        IsStepDisabled: IsStepDisabled,
        currentStep: currentStep,
        GoToStep: GoToStep,
        GetStepText: GetStepText
    }
}();

ko.applyBindings(tmx.funnel,$('#funnel-wrapper').get(0));
