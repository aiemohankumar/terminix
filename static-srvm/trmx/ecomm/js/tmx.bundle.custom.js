function IsObject(e) {
    return "[object Object]" == Object.prototype.toString.call(e)
}

function IsArray(e) {
    return "[object Array]" == Object.prototype.toString.call(e)
}

function IsObjectEmpty(e) {
    for (var t in e) return !1;
    return !0
}

function GetWindowWidth() {
    return window.innerWidth
}

function ScrollTo(e, t) {
    $("html, body").animate({
        scrollTop: e
    }, t)
}

function ArrayContainsArray(t, e) {
    return !!e.length && e.every(function (e) {
        return -1 < t.indexOf(e)
    })
}

function IsStorageAvailable(e) {
    try {
        var t = window[e],
            a = "__storage_test__";
        return t.setItem(a, a), t.removeItem(a), !0
    } catch (e) {
        return e instanceof DOMException && (22 === e.code || 1014 === e.code || "QuotaExceededError" === e.name || "NS_ERROR_DOM_QUOTA_REACHED" === e.name) && 0 !== t.length
    }
}
var caniuse = function (e) {
    return e.history = window.history && "function" == typeof history.pushState && "function" == typeof history.replaceState && "function" == typeof history.back, e.sessionStorage = IsStorageAvailable("sessionStorage"), e.localStorage = IsStorageAvailable("localStorage"), e
}({});

function GetCookie(e) {
    if (!e) return console.log("[GetCookie] Name is required to get the value of a cookie."), null;
    e += "=";
    for (var t = document.cookie.split(";"), a = 0; a < t.length; a++) {
        for (var i = t[a];
            " " == i.charAt(0);) i = i.substring(1, i.length);
        if (0 == i.indexOf(e)) return i.substring(e.length, i.length)
    }
    return null
}

function SetCookie(e, t, a) {
    if (!e || !t) return console.log("[SetCookie] Name and value are both required to set a cookie."), !1;
    if (a) {
        var i = new Date;
        i.setTime(i.getTime() + 24 * a * 60 * 60 * 1e3);
        var n = "; expires=" + i.toUTCString()
    } else n = "";
    return document.cookie = e + "=" + t + n + "; path=/", !0
}

function OpenModal(e) {
    "use strict";
    !e || "string" != typeof e || -1 < e.indexOf("#") || (_MATERIALIZE ? $("#" + e).modal("open") : $("#" + e).modal("show"))
}

function CloseModal(e) {
    "use strict";
    !e || "string" != typeof e || -1 < e.indexOf("#") || (_MATERIALIZE ? $("#" + e).modal("close") : $("#" + e).modal("hide"))
}

function is_iOS() {
    "use strict";
    for (var e = ["iPad Simulator", "iPhone Simulator", "iPod Simulator", "iPad", "iPhone", "iPod"]; e.length;)
        if (navigator.platform === e.pop()) return !0;
    return !1
}

function GetURLParameter(e) {
    "use strict";
    if (!e) throw "[GetURLParameter] param is required to pull a parameter.";
    var t, a, i = null,
        n = decodeURIComponent(window.location.search.substring(1)).split("&");
    for (a = 0; a < n.length; a++)
        if ((t = n[a].split("="))[0] === e) {
            i = void 0 === t[1] ? null : t[1];
            break
        } return i
}

function GetTargetPest() {
    "use strict";
    var e = null,
        t = GetURLParameter("targetPest");
    return t ? e = t : caniuse.sessionStorage && sessionStorage.getItem("targetPest") && (e = sessionStorage.getItem("targetPest").toUpperCase()), e
}

function IsValidEmailAddress(e) {
    "use strict";
    return /^\w+([\.%#$&*+_-]\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(e = e || "")
}

function IsValidRoutingNumber(e) {
    "use strict";
    return !(!e || 9 !== e.length || !$.isNumeric(e)) && (3 * (parseInt(e.charAt(0), 10) + parseInt(e.charAt(3), 10) + parseInt(e.charAt(6), 10)) + 7 * (parseInt(e.charAt(1), 10) + parseInt(e.charAt(4), 10) + parseInt(e.charAt(7), 10)) + 1 * (parseInt(e.charAt(2), 10) + parseInt(e.charAt(5), 10) + parseInt(e.charAt(8), 10))) % 10 == 0
}

function GetGlobalUIPattern(e) {
    var t = "";
    return e && (t = _GLOBAL_UI_SETTINGS ? _GLOBAL_UI_SETTINGS.inputRegexPattern[e.toLowerCase()] : ""), t
}

function FireGAEvent(e, t, a, i) {
    if (a = a || "", i = i || "", e)
        if (t) {
            console.log("[FireGAEvent] " + e + " :: " + t + " :: " + a + " :: " + i);
            try {
                var n = {
                    ga_event_category: e,
                    ga_event_action: t,
                    ga_event_label: a,
                    ga_event_value: i
                };
                utag.link(n)
            } catch (e) {
                console.log(e)
            }
        } else console.log("[FireGAEvent] Event Action is required to fire a GA event.");
    else console.log("[FireGAEvent] Event Category is required to fire a GA event.")
}

function GetEndecaContent(i, t) {
    var n = function (e) {
        t(e)
    };
    if (!i) throw "[GetEndecaContent] Data is required.";
    if (!i.productCode || !i.templateCode || !i.folder) return console.log("[GetEndecaContent] Product Code, Template Code, and folder are all required in data."), void n({});
    var e = "/includes/common/getEndecaContent.jsp?productCode=" + i.productCode + "&templateCode=" + i.templateCode + "&folder=" + i.folder;
    $.ajax({
        type: "GET",
        url: e,
        timeout: 12e5,
        complete: function (e) {
            var t;
            try {
                t = JSON.parse(e.responseText)
            } catch (e) {
                t = {
                    error: "An error has occurred."
                }, console.log("Error from server request: " + e.message)
            }
            var a = {
                templateCode: i.templateCode,
                content: t
            };
            n(a)
        }
    })
}

function CommonModal(e, t, a) {
    var i = e.title || "",
        n = e.body || "",
        o = e.confirm || !1,
        r = e.btnCancelText || "Cancel",
        s = e.btnConfirmText || "Ok",
        l = $("#commonModal"),
        d = {},
        c = l.find(".cta.primary"),
        u = l.find(".cta.secondary, button.close");
    if (!l) throw "[CommonModal] No common modal id found on the page.";
    l.find(".modal-title").text(i), l.find(".modal-body").html(n), c.text(s), l.find(".cta.secondary").text(r), d.show = !0, o ? (d.backdrop = "static", c.show()) : c.hide(), l.on("show.bs.modal", function (e) {
        if (o) {
            if (!t) throw "[CommonModal] Callback method required for confirmation modal.";
            c.off().on("click", function (e) {
                return t()
            }), u.off().on("click", function (e) {
                if ("function" == typeof a) return a()
            })
        }
    }), l.on("hide.bs.modal", function (e) {
        null
    }), l.modal(d)
}

function ThrowToast(e) {}
var _CreditCardTypes = [{
    name: "Visa",
    regex: /^4/,
    value: "visa"
}, {
    name: "Master Card",
    regex: /^(5[1-5]|2(2(2[1-9][0-9]{2}|[3-9])|[3-6]|7([01]|20[0-9]{2})))/,
    value: "masterCard"
}, {
    name: "American Express",
    regex: /^3[47]/,
    value: "americanExpress"
}, {
    name: "Discover",
    regex: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5])|64[4-9]|65)/,
    value: "discover"
}];

function GetCreditCardType(e) {
    "use strict";
    if (!e || "string" != typeof e) return null;
    for (var t = 0; t < _CreditCardTypes.length; t++)
        if (_CreditCardTypes[t].regex.test(e)) {
            var a = _CreditCardTypes[t];
            return {
                name: a.name,
                value: a.value
            }
        } return null
}

function ValidateCreditCardNumber(e, t) {
    "use strict";
    if (!e || "string" != typeof e) throw "[ValidateCreditCardNumber] String parameter cardNumber is required.";
    if (!t || "string" != typeof t) throw "[ValidateCreditCardNumber] String parameter cardType is required.";
    for (var a = 0; a < _CreditCardTypes.length; a++)
        if (_CreditCardTypes[a].value.toLowerCase() === t.toLowerCase() && _CreditCardTypes[a].regex.test(e)) return !0;
    return !1
}
RegExp.escape = function (e) {
    return e.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
}, String.prototype.replaceAll = function (e, t) {
    return this.replace(new RegExp(RegExp.escape(e), "g"), t)
};
var _SCHEDULE_SLOTS = [];

function checkURLParam(e) {
    for (var t = window.location.search.substring(1).split("&"), a = 0; a < t.length; a++) {
        var i = t[a].split("=");
        if (i[0] == e) return i[1]
    }
    return !1
}

function showResponseModal(e) {
    var t = e.status;
    "OK" === t ? OpenModal("purchaseThanks") : "EMPTY" === t || OpenModal("purchaseSorry")
}

function changeTargetPest() {
    -1 < window.location.href.indexOf("mosquitoes") && $("#productId_hidden").val("10003X10002")
}

function isValid(e) {
    if ("consent" == e.attr("id") && (_MATERIALIZE ? e.is(":invalid") ? $(".consentText").addClass("red-text") : $(".consentText").removeClass("red-text") : e.is(":invalid") ? $(".consentText").addClass("red") : $(".consentText").removeClass("red")), e.is(":invalid") && e.prop("required")) return e.addClass("invalid"), e.removeClass("hidden"), e.is("select") && _MATERIALIZE && e.parent(".select-wrapper").children(".select-dropdown").addClass("invalid"), !1;
    if (!ValidateCardType(e)) return !1;
    if ("bankRoutingNumber" == e.attr("id") && e.prop("required")) {
        var t = e.val();
        return !(9 !== t.length || "5" == t[0] || !$.isNumeric(t)) && (0 === (7 * (parseInt(t.charAt(0), 10) + parseInt(t.charAt(3), 10) + parseInt(t.charAt(6), 10)) + 3 * (parseInt(t.charAt(1), 10) + parseInt(t.charAt(4), 10) + parseInt(t.charAt(7), 10)) + 9 * (parseInt(t.charAt(2), 10) + parseInt(t.charAt(5), 10) + parseInt(t.charAt(8), 10))) % 10 || (e.addClass("invalid"), !1))
    }
    return "expMonth" != e.attr("id") && "expYear" != e.attr("id") || !e.prop("required") || ValidateExpDate()
}

function ValidateCardType(e) {
    if ("ccNumber" === e.attr("id") && e.prop("required")) {
        var t = $("#ccType");
        if (!ValidateCreditCardNumber(e.val(), t.val())) return e.addClass("invalid"), !1
    }
    return !0
}

function ValidateExpDate() {
    var e = $("#expMonth"),
        t = $("#expYear"),
        a = $("#expMonth option:selected").val(),
        i = $("#expYear option:selected").val(),
        n = (new Date).getMonth() + 2,
        o = (new Date).getFullYear();
    return !a || !i || i == o && a < n ? (e.addClass("invalid"), e.parent(".select-wrapper").children(".select-dropdown").addClass("invalid"), t.addClass("invalid"), t.parent(".select-wrapper").children(".select-dropdown").addClass("invalid"), !1) : (e.removeClass("invalid"), e.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid"), t.removeClass("invalid"), t.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid"), !0)
}

function submitAjaxForm(e, t) {
    var a = {
        success: function (e) {
            t(e)
        },
        error: function (e) {
            t(e)
        },
        dataType: "json"
    };
    return $("#" + e).ajaxSubmit(a), !1
}

function sendConfirmation() {
    return submitAjaxForm("emailForm", doNothing), !1
}

function checkAvailableDates(e, t) {
    for (var a = new Array(0), i = 0; i < t.length; i++) {
        var n = t[i].schDate; - 1 == a.indexOf(n) && (0 == n.charAt(0) && (n = n.substring(1, n.length)), a.push(n.replace("/0", "/")))
    }
    return dmy = e.getMonth() + 1 + "/" + e.getDate() + "/" + String(e.getFullYear()).substring(2, 4), -1 < $.inArray(dmy, a) ? [!0, "selectable", "Available"] : [!1, "", "unAvailable"]
}

function checkOpenTimeSlots(e, t, a) {
    var i, n = e.substring(0, 6) + e.substring(8, 12);
    0 == n.charAt(0) && (n = n.substring(1, n.length)), i = new Array(0);
    for (var o = 0; o < t.length; o++)
        if (t[o].schDate == n) {
            timeScanner(t[o].schToTime.substring(0, 5)); - 1 == i.push(t[o].schSlotKey + "XX" + timeScanner(t[o].schFrmTime.substring(0, 5)) + " - " + timeScanner(t[o].schToTime.substring(0, 5))) && i.push(t[o].schSlotKey + "XX" + timeScanner(t[o].schFrmTime.substring(0, 5)) + " - " + timeScanner(t[o].schToTime.substring(0, 5)))
        } i.sort(SortByTime), updateSelectedDate(a, e), buildTimeSlots(i, a)
}

function SortByTime(e, t) {
    var a = e.split("XX")[1].substr(0, 7),
        i = t.split("XX")[1].substr(0, 7);
    return -1 == a.indexOf("M") && (a += "M"), -1 == i.indexOf("M") && (i += "M"), new Date("1970/01/01 " + a) - new Date("1970/01/01 " + i)
}

function timeScanner(e) {
    var t = "AM",
        a = parseInt(e.substring(0, 2)),
        i = e.substring(2, 5);
    return 11 < a && (t = "PM"), e = (a = hourCalc(a)) + i + " " + t
}

function hourCalc(e) {
    return 11 < e && 0 == (e = parseInt(e) - 12) && (e = 12), e
}

function updateSelectedDate(e, t) {
    $("#" + e + " .alternateDate").empty(), $("#" + e + " .alternateTime").empty(), $("#" + e + " .alternateDate").append(t)
}

function updateSelectedTime(e, t) {
    $("#" + t + " .alternateTime").empty(), $("#" + t + " .alternateTime").append(e), "None of the dates work for me" == e && $("#" + t + " .alternateDate").empty(), _MATERIALIZE ? $("#" + t + " .alternateDateTime").removeClass("hide") : $("#" + t + " .alternateDateTime").removeClass("hidden")
}

function buildTimeSlots(e, t) {
    $("#" + t + "CalendarSlots").empty(), timeSlotDisplayed = new Array(0), $("#scheduleServiceButton").addClass("disabled");
    for (var a = 0; a < e.length; a++) $.inArray(e[a].split("XX")[1], timeSlotDisplayed) < 0 && (_MATERIALIZE ? $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + "timeslotradio' class='customslot hidden' id='timeslottrigger' value='" + e[a].split("XX")[0] + "' onClick='updateSelectedTime(\"" + e[a].split("XX")[1] + '","' + t + "\")'><h5 class='materialRadioFix sbold'>" + e[a].split("XX")[1] + " </h5></label></div>") : $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + "timeslotradio' class='customslot' id='timeslottrigger' value='" + e[a].split("XX")[0] + "' onClick='updateSelectedTime(\"" + e[a].split("XX")[1] + '","' + t + "\")'><h5 class=' sbold'>" + e[a].split("XX")[1] + " </h5></label></div>"), timeSlotDisplayed.push(e[a].split("XX")[1]));
    _MATERIALIZE ? $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + "timeslotradio' class='customslot' id='timeslotnoappointment' onClick='updateSelectedTime(\"None of the dates work for me\",\"" + t + "\")' value=''><h5 class='materialRadioFix  sbold'> None of the dates work for me </h5></label></div>") : $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + "timeslotradio' class='customslot' id='timeslotnoappointment' onClick='updateSelectedTime(\"None of the dates work for me\",\"" + t + "\")' value=''><h5 class=' sbold'> None of the dates work for me </h5></label></div>"), $("#" + t + "timeslotnoappointment").click(function () {}), $("#" + t + "selectedCalendarDateSlots input:radio").click(function () {
        $("#" + t + "scheduleServiceButton").removeClass("disabled")
    })
}

function getSlotsForAppointment(e, t, a) {
    var i = (new Date).getTime();
    $.ajax({
        type: "POST",
        url: "/buyonline/common/openSlotsMapJSON.jsp?productId=" + e + "&appointmentType=" + t,
        dataType: "json",
        success: function (e) {
            var t = (new Date).getTime() - i;
            procEvent("appointmentSlotRetrievalStep2", "Step2 Appointment Retrieval Success", "loadTime:" + t)
        },
        error: function (e) {}
    })
}

function getSlotsForCalendar(e, t, r) {
    var s = (new Date).getTime();
    $.ajax({
        type: "POST",
        url: "/buyonline/common/openSlotsMapJSON.jsp?productId=" + e + "&appointmentType=" + t,
        dataType: "json",
        success: function (e) {
            var t, a;
            t = new Array(0), a = new Array(0);
            for (var i = 0; i < e.jsonOpenSlotMap.length; i++) {
                var n = e.jsonOpenSlotMap[i].schDate; - 1 == a.indexOf(n) && (0 == n.charAt(0) && (n = n.substring(1, n.length)), e.jsonOpenSlotMap[i].schDate = n, t.push(e.jsonOpenSlotMap[i]), a.push(n.replace("/0", "/")))
            }
            createCalendar(r, t);
            var o = (new Date).getTime() - s;
            procEvent("appointmentSlotRetrievalStep3", "Step3 Appointment Retrieval Success", "loadTime:" + o)
        },
        error: function (e) {
            createCalendar(r, null)
        }
    })
}

function createCalendar(t, a) {
    $(document).trigger("SetNewSchedulerDates", [a]);
    var e = "hidden";
    if (_MATERIALIZE && (e = "hide"), $("#" + t + " .processing").addClass(e), $("#" + t + " .defaultslot").prop("checked", !0), $("#" + t + " .default-time").click(function () {
            $("#" + t + " .timeslothidden").val($("#" + t + " > .default-time").val())
        }), $("#" + t + " .choose-different").click(function () {
            $("#" + t + " .defaultslot").prop("checked", !1)
        }), $("#" + t + "Modal #cancelButton").click(function () {
            $("#" + t + " .defaultslot").prop("checked", !0), $("#" + t + " .choose-different").prop("checked", !1), $("#" + t + " .alternateDateTime").addClass(e)
        }), null != a && 0 < a.length) {
        var i;
        $("#" + t + " .slotSelect").removeClass(e), (i = a[0]) && ($("#" + t + " .defaultSlot").append(i.schDate + " " + timeScanner(i.schFrmTime) + " to  " + timeScanner(i.schToTime)), $("#" + t + " .default-time").val(i.schSlotKey)), _MATERIALIZE ? $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + 'timeslotradio\' onClick=\'updateSelectedTime("None of the dates work for me","' + t + "\")' class='customslot' id='timeslotnoappointment' value=''><h5 class='materialRadioFix sbold'> None of the dates work for me.</h5></label></div>") : $("#" + t + "CalendarSlots").append("<div class='radio'><label><input type='radio' name='" + t + 'timeslotradio\' onClick=\'updateSelectedTime("None of the dates work for me","' + t + "\")' class='customslot' id='timeslotnoappointment' value=''><h5 class=' sbold'> None of the dates work for me.</h5></label></div>"), $(".defaultslot").click(function () {
            $("#" + t + " .customslot").prop("checked", !1), $("#" + t + " .choose-different").prop("checked", !1), $("#" + t + " .alternateDateTime").addClass(e)
        });
        var n = $("#" + t + "Calendar");
        n.length && n.datepicker({
            beforeShowDay: function (e) {
                return checkAvailableDates(e, a)
            },
            nextText: "",
            prevText: "",
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            onSelect: function (e) {
                checkOpenTimeSlots(e, a, t)
            }
        }), selectDefaultTimeSlot(t), $(".ui-state-active").removeClass("ui-state-active")
    } else $("#" + t + " .noAppointments").removeClass(e), $("#" + t + " .noDateDisplay").show(), $("#requestInspec").removeClass(e), $(".requestInspection").removeClass("submitting").removeAttr("disabled").text("Request Inspection");
    $("#" + t + "Calendar .selectable").first().click()
}

function selectDefaultTimeSlot(e) {
    $("#" + e + " > .defaultSlot").click(), $("#" + e + " > .choose-different").prop("checked", !1)
}

function handleSubmitOrder() {
    var e = !0;
    $("#paymentGroupForm :input").each(function () {
        isValid($(this)) || (e = !1, $("#cartCheckout").removeAttr("disabled"))
    }), e && (_QAS_FEATURE_TOGGLE ? QAS_EMAIL_VALIDATE() : CompleteOrderSubmit())
}

function CompleteOrderSubmit() {
    "undefined" != typeof utag_data && (utag_data.ga_action = $("#ccType").val()), 0 < $(".serviceScheduler").length || (0 < $("#serviceScheduler").length && $("#serviceScheduler .timeslothidden").val($("input:radio[name=serviceSchedulertimeslotradio]:checked").val()), 0 < $("#salesScheduler").length && $("#salesScheduler .timeslothidden").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val())), OpenModal("processing-modal"), addPaymentGroup()
}

function handleFreeInspection() {
    var e = $(".requestInspection"),
        t = $(".submitError");
    if (e.length) {
        if (t.hide(), !$(".timeslothidden").val()) return t.show(), !1;
        e.addClass("submitting").attr("disabled", !0).text("Processing...")
    } else $("#requestInspec").addClass("disabled"), 0 < $("#salesScheduler").length && $("#salesScheduler .timeslothidden").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val());
    QAS_EMAIL_CONFIG.AFTER_VERIFY = function () {
        OpenModal("processing-modal"), submitAjaxForm("paymentGroupForm", submitInspectionShippingForm)
    }, QAS_EMAIL_VALIDATE()
}

function submitInspectionShippingForm() {
    submitAjaxForm("shippingGroupForm", submitInspectionForm)
}

function submitInspectionForm() {
    $("#submitSchedule").submit()
}

function handleGetFreeQuote(e) {
    var t = !0;
    $("#firstName").removeAttr("required"), $("#lastName").removeAttr("required"), $("#phone").removeAttr("required"), $("#consent").removeAttr("required"), $("#addressForm :input").each(function () {
        isValid($(this)) || (t = !1)
    }), $("#city_hidden, #state_hidden").each(function () {
        isValid($(this)) || $(".hiddenInputs").removeClass("hidden")
    }), t && $("#addressForm").submit()
}

function processAddressValidationResult(e) {
    "suggestion" == e.code ? OpenModal("addressSelectionModal") : "error" == e.code && OpenModal("addressSelectionModal")
}

function setAddressValidationForm() {
    $("#address1Validation_hidden").val($("#address1").val()), $("#cityValidation_hidden").val($("#city_hidden").val()), $("#stateValidation_hidden").val($("#state_hidden").val()), $("#postalCodeValidation_hidden").val($("#zipCode").val())
}

function addShippingGroup() {
    return submitAjaxForm("shippingGroupForm", addShippingGroupResponse)
}

function addShippingGroupResponse(e) {
    $("#submitOrder").submit()
}

function addPaymentGroup() {
    return submitAjaxForm("paymentGroupForm", addPaymentGroupResponse)
}

function addPaymentGroupResponse(e) {
    e && "error" == e.code ? CloseModal("processing-modal") : addShippingGroup()
}

function linkToUrl(e) {
    window.location.href = e
}

function populateProductCodeAndPestType(e) {
    $("#productCode_hidden, #targetPest_hidden").val(e)
}

function planSelect(e, t, a, i, n) {
    return $("#itemSkuIdHid").val(t), $("#itemProdIdHid").val(a), $("#heading" + n).addClass("productSelected"), $("#cartTotals").addClass("hidden"), $("#cartTotals").addClass("hide"), $("#cartHolder").addClass("hidden"), $("#cartHolder").addClass("hide"), $("#cartProcessing").removeClass("hidden"), $("#cartProcessing").removeClass("hide"), submitAjaxForm(i, refreshCartAddition), !1
}

function refreshCartAddition(e) {
    if ("success" == e.code) {
        if (_MATERIALIZE) var t = "/includes/materialcart2-select.jsp";
        else t = "/includes/cart.jsp";
        $("#cartHolder").load(t, function () {
            $("#cartProcessing").addClass("hidden"), $("#cartProcessing").addClass("hide"), $("#cartHolder").removeClass("hidden"), $("#cartHolder").removeClass("hide"), $(".cartRemovebutton").click(function (e) {
                e.stopPropagation();
                var t = $(this).attr("name");
                $("#heading" + t + " > .buytoggle.removeButton").toggleClass("disabled")
            }), "" != $("#totalAmountWithTax").text() ? $("#mobileTotal").text($("#totalAmountWithTax").text()) : $("#mobileTotal").text("$0.00"), productInCart($(".product")), refreshButtons(), _MATERIALIZE && ($(".modal-trigger").modal(), $("#modalMonthlyPay").modal())
        })
    } else submitAjaxForm("addToCartForm", refreshCartFinal)
}

function refreshCartFinal(e) {
    if (_MATERIALIZE) var t = "/includes/materialcart2-select.jsp";
    else t = "/includes/cart.jsp";
    $("#cartHolder").load(t, function () {
        $("#cartProcessing").addClass("hidden"), $("#cartProcessing").addClass("hide"), $("#cartHolder").removeClass("hidden"), $("#cartHolder").removeClass("hide"), "" != $("#totalAmountWithTax").text() ? $("#mobileTotal").text($("#totalAmountWithTax").text()) : $("#mobileTotal").text("$0.00"), productInCart($(".product")), refreshButtons(), _MATERIALIZE && ($(".modal-trigger").modal(), $("#modalMonthlyPay").modal())
    })
}

function refreshButtons() {
    $(".buyToggle.disabled").each(function () {
        $(this).removeClass("disabled"), $(this).addClass("hidden"), $(this).siblings(".buyToggle").removeClass("hidden")
    })
}

function doNothing(e) {}

function cartCheckoutSubmit() {
    if (($("#cartCheckout").attr("disabled", "true"), 0 < $(".serviceScheduler").length) && !$(".timeslothidden").val()) return void $("#cartCheckout").removeAttr("disabled");
    if (0 < $("#serviceScheduler").length && $("#finalSlotSelected").val($("input:radio[name=serviceSchedulertimeslotradio]:checked").val()), 0 < $("#salesScheduler").length && $("#finalSlotSelected").val($("input:radio[name=salesSchedulertimeslotradio]:checked").val()), $("#hidden_form1").length) {
        if ("undefined" != typeof utag_data) {
            var e = "true" == $("#easyPay_hidden").val() ? 0 : 1;
            utag_data.product_ids = [utag_data.imp_ids[e]], utag_data.product_names = [utag_data.imp_names[e]], utag_data.product_categories = [utag_data.imp_categories[e]], utag_data.product_variants = [utag_data.imp_variants[e]], utag_data.product_prices = [utag_data.imp_prices[e]]
        }
        OpenModal("processing-modal"), $("#hidden_form1").submit()
    } else $("#submitOrder").length && handleSubmitOrder()
}

function updateAddress() {
    return $("#homeSize_hidden").val().length <= 0 && $(".propertySelect option").each(function () {
        this.selected && $("#homeSize_hidden").val($(this).val())
    }), "undefined" != typeof utag_data && (utag_data.customer_address = $("#address1").val() + " " + $("#address2").val(), utag_data.customer_zip = $("#zipCode").val(), utag_data.customer_property_option = $("#homeSize_hidden").val(), utag_data.customer_type = "online"), procEvent("ecommerce", "address change success"), $("#editModal").modal("hide"), OpenModal("processing-modal"), submitAjaxForm("updateAddressForm", doNothing), !0
}

function removeItemFromCartByRefId(e, t) {
    $("#removalCommerceItemRefIdHid").val(e), $("#cartHolder").addClass("hidden"), $("#cartHolder").addClass("hide"), $("#cartProcessing").removeClass("hidden"), $("#cartProcessing").removeClass("hide"), $("#cartTotals").addClass("hidden"), $("#cartTotals").addClass("hide"), $("#heading" + t).removeClass("productSelected"), submitAjaxForm("removeFromCartFormByRefId", refreshCartFinal)
}

function removeItemFromCart(e, t, a) {
    $("#addItem" + a).toggleClass("hide"), $("#maddItem" + a).toggleClass("hide"), $("#removeItem" + a).toggleClass("hide"), $("#mremoveItem" + a).toggleClass("hide"), $("#removeFromCartForm > #commerceItemIdHid").val(e), $("input[value='" + t + "']").prop("checked", !1), $("#heading" + a).removeClass("productSelected"), $("#cartHolder").addClass("hidden"), $("#cartHolder").addClass("hide"), $("#cartAccordion").addClass("hidden"), $("#cartTotals").addClass("hidden"), $("#cartProcessing").removeClass("hidden"), $("#cartProcessing").removeClass("hide"), submitAjaxForm("removeFromCartForm", refreshCartFinal)
}

function productInCart(e) {
    0 < e.length ? $("#sendQuoteButton").removeClass("disabled") : $("#sendQuoteButton").addClass("disabled");
    for (var t = 0; t < e.length; t++) {
        var a = "addItem" + $(e[t]).attr("id"),
            i = "removeItem" + $(e[t]).attr("id");
        $("#cartHolder").children().has($(e[t]).attr("id")) ? ($("#" + a + ", #m" + a).addClass("hide"), $("#" + i + ", #m" + i).removeClass("hide")) : ($("#" + i + ", #m" + i).addClass("hide"), $("#" + a + ", #m" + a).removeClass("hide"))
    }
}

function displayMosqSeason(e) {
    var t = {
        PC192: "Jan|Dec|12",
        PC193: "Mar|Oct|8",
        PC194: "April|Sep|6",
        PC195: "April|Oct|7",
        PC196: "April|Nov|8",
        PC197: "May|Sep|5",
        PC198: "May|Oct|6",
        PC307: "Jan|Dec|12",
        PC310: "Mar|Oct|8",
        PC308: "April|Oct|7",
        PC314: "May|Oct|6"
    };
    for (var a in t)
        if (a == e) {
            var i = t[a].split("|");
            $("#mosqSeason").text("Your Mosquito season Starts in " + i[0] + " and Ends in " + i[1] + "."), $("#mosqSeasonBack").text("For this fee, you will receive monthly treatments from " + i[0] + " to " + i[1] + ", the length of mosquito season for your area."), $("#mosqPay").text(i[2] + " payments in total")
        }
}

function validationPassword() {
    var e, t = $("#passwordMyAcccount"),
        a = [new RegExp("[a-zA-Z0-9]{7}"), new RegExp("[A-Z]"), new RegExp("[a-z]"), new RegExp("[0-9]")],
        i = [$("#passLength"), $("#passUpper"), $("#passLower"), $("#passNumber")];
    for (e = 0; e < a.length; e++) t.val().match(a[e]) ? (i[e].removeClass("gray"), i[e].addClass("green")) : i[e].addClass("gray")
}

function turnButtonsOn() {
    $("button").each(function () {
        $(this).attr("disabled", !1), $(this).removeClass("disabled")
    })
}

function changePaymentType(e, t) {
    _MATERIALIZE ? ($("." + t).removeClass("hide"), $("." + e).addClass("hide")) : ($("." + t).removeClass("hidden"), $("." + e).addClass("hidden")), $("." + e + " input, ." + e + " select").removeAttr("required"), $("." + t + " input, ." + t + " select").prop("required", !0)
}

function iniTesting() {
    $(".ini-hidden").removeClass("ini-hidden"), $(".ini").addClass("ini-hidden")
}
$(document).ready(function () {
    window.nav = new function () {
        "use strict";
        var t = this,
            e = $("li.more"),
            a = $("li.shrink-1"),
            i = $(".subnav .shrink-1"),
            n = $("li.shrink-2"),
            o = $(".subnav .shrink-2"),
            r = $(".header-container.mobile"),
            s = $(".header-container.desktop");
        return t.states = [0, 1, 2, 3], t.currentState = t.states[2], t.SetNav = function () {
            var e = GetWindowWidth();
            t.currentState = e < 992 ? t.states[3] : e < 1380 ? t.states[2] : e < 1575 ? t.states[1] : t.states[0], t.UpdateNav()
        }, t.UpdateNav = function () {
            switch (t.currentState) {
                case 0:
                    r.hide(), s.show(), e.hide(), a.show(), i.hide(), n.show(), o.hide();
                    break;
                case 1:
                    r.hide(), s.show(), e.show(), a.hide(), i.show(), n.show(), o.hide();
                    break;
                case 3:
                    r.show(), s.hide();
                    break;
                case 2:
                default:
                    r.hide(), s.show(), e.show(), a.hide(), i.show(), n.hide(), o.show()
            }
        }, t
    }, nav.SetNav(), $(window).resize(nav.SetNav), $(".mobile .nav-icon").on("click", function () {
        $(".mobile .nav-icon a").parents(".header-container.mobile").toggleClass("open")
    });
    var a = $(".mobile .mainnav > li");
    a.on("click", function (e) {
        var t = $(this);
        t.hasClass("active") || a.removeClass("active"), t.toggleClass("active")
    }), $(".scroll-to-top").on("click", function (e) {
        ScrollTo(0, 500)
    });
    var e = "1.877.837.6464";
    if (_BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE) {
        var t = GetCookie("branch_phone_number");
        e = t || e
    }
    $(".phone-number-container > span").text(e), $(".call-phone > a").text(e).attr("href", "tel:" + e.replace(/\./g, "")), $(".phone-link > a").text(e).attr("href", "tel:" + e.replace(/\./g, "")), $(".more-less-toggle").on("click", function (e) {
        $(".more-less-content").collapse("toggle"), $(".more-less-toggle").text(function (e, t) {
            return "less" === t ? "more" : "less"
        })
    });
    var i = (new Date).getFullYear();
    $(".current-year").text(i);
    var n = document.createElement("script");
    if (n.type = "application/ld+json", n.text = JSON.stringify({
            "@context": "http://schema.org/",
            "@type": "WebPage",
            author: "https://www.terminix.com/",
            datePublished: (new Date).toISOString(),
            headline: document.querySelector(".jsonld-webpage-headline").text,
            publisher: "https://www.terminix.com/",
            description: document.querySelector(".jsonld-webpage-description").getAttribute("content")
        }), document.querySelector("head").appendChild(n), caniuse.sessionStorage) {
        var o = {
            "/additional-pest-solutions/attic-insulation/": "ATTIC INSULATION",
            "/bed-bug-control/": "BED BUGS",
            "/additional-pest-solutions/crawl-space-services/": "CRAWL SPACE",
            "/pest-control/": "PEST CONTROL",
            "/pest-control/ants/": "ANTS",
            "/pest-control/cockroaches/": "COCKROACHES",
            "/pest-control/crickets/": "CRICKETS",
            "/pest-control/fleas/": "FLEAS",
            "/pest-control/mosquitoes/": "MOSQUITOES",
            "/pest-control/mosquitoes/atsb/": "MOSQUITOES",
            "/pest-control/moths/": "CLOTHES MOTHS",
            "/pest-control/rodents/": "RODENTS",
            "/pest-control/scorpions/": "SCORPIONS",
            "/pest-control/silverfish/": "SILVERFISH",
            "/pest-control/spiders/": "SPIDERS",
            "/pest-control/ticks/": "TICKS",
            "/pest-control/wasps/": "PAPER WASPS",
            "/pest-control/wildlife/": "WILDLIFE",
            "/termite-control/": "TERMITES",
            "/additional-pest-solutions/": "OTHER SERVICES"
        };
        $("body").on("click", 'a[href^="/free-inspection"], a[href^="/buyonline"]', function (e) {
            var t = window.location.pathname;
            t.length && "/" != t.charAt(t.length - 1) && (t += "/"), (t = t.toLowerCase()) in o ? sessionStorage.setItem("interestArea", o[t]) : sessionStorage.removeItem("interestArea")
        })
    }
    $(document).on("ShowMobileChatLink", function (e) {
        $(".header-container.mobile .chat-link").show()
    }), $(document).on("ShowGenericChatLink", function (e) {
        $(".chat-link").show()
    }), $(document).on("HideGenericChatLink", function (e) {
        $(".chat-link").hide()
    }), $(".header-container .chat-link").hide(), $(document).on("click", ".chat-link", function (e) {
        e.preventDefault(), LC_API.open_chat_window()
    });
    var r = $(".scheduler-container"),
        s = $(".scheduler-select"),
        l = $(".time-slot"),
        d = $(".representative-text"),
        c = $(".loading-container"),
        u = $(".note-text"),
        m = $(".timeslothidden");

    function p() {
        var e = $("#scheduler-none"),
            t = $(".scheduler.dates > li > a.active"),
            a = $(".scheduler.times > li > a.active"),
            i = "";
        e.prop("checked") ? i = "None" : t.length && a.length && (i = a.data("slotkey")), m.val(i)
    }

    function h(e) {
        var t = e,
            a = 12 <= t ? "PM" : "AM";
        return (t = (t %= 12) || 12) + " " + a
    }
    $(document).on("SetNewSchedulerDates", function (e, t) {
        _SCHEDULE_SLOTS = [], t && (_SCHEDULE_SLOTS = function (e) {
            for (var t = new Array(0), a = 0; a < e.length; a++) {
                for (var i = null, n = e[a], o = !1, r = !1, s = -1, l = 0; l < t.length; l++)
                    if (n.schDate === t[l].dateString) {
                        i = t[l], o = !0;
                        for (var d = 0; d < i.times.length; d++)
                            if (n.schFrmTime === i.times[d].timeStart) {
                                r = !0, s = d;
                                break
                            } break
                    } null === i && (i = {
                    dateString: n.schDate,
                    times: []
                }), r ? parseInt(n.schDriveTime) < parseInt(i.times[s].data.schDriveTime) && (i.times[s].data = n) : i.times.push({
                    timeStart: n.schFrmTime,
                    timeStop: n.schToTime,
                    data: n
                }), o || t.push(i)
            }
            for (var c = 0; c < t.length; c++) t[c].times.sort(function (e, t) {
                var e = parseInt(e.timeStart.toString().substring(0, 2)),
                    t = parseInt(t.timeStart.toString().substring(0, 2));
                return e - t
            });
            return t
        }(t)), null != _SCHEDULE_SLOTS && 0 < _SCHEDULE_SLOTS.length ? (! function (e, t) {
            for (var a = $("ul.scheduler.dates"), i = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], n = 0; n < e.length; n++) {
                var o = new Date(e[n].dateString),
                    r = '<li><a href="#" class="schedule-date" data-date="' + e[n].dateString + '"><span class="month">' + i[o.getMonth()] + '</span><span class="date">' + o.getDate() + "</span></a></li>";
                a.append(r)
            }
            t()
        }(_SCHEDULE_SLOTS, function () {
            r.show(), u.show(), c.hide()
        }), $(document).trigger("SetDefaultDateAndTime")) : (r.hide(), d.show(), u.show(), c.hide(), $("#scheduler-none").trigger("click"))
    }), $("#scheduler-none").on("change", function (e) {
        $(this).prop("checked") ? (s.hide(), d.show()) : (s.show(), d.hide()), p()
    }), $(document).on("click", ".scheduler.dates > li > a", function (e) {
        e.preventDefault();
        var t = $(this),
            a = $(".scheduler.dates > li > a");
        t.hasClass("active") || (a.removeClass("active"), t.addClass("active"), function (e, t) {
            var a = $("ul.scheduler.times"),
                i = function (e) {
                    for (var t = null, a = 0; a < _SCHEDULE_SLOTS.length; a++) _SCHEDULE_SLOTS[a].dateString === e && (t = _SCHEDULE_SLOTS[a]);
                    return t
                }(e);
            a.empty();
            for (var n = 0; n < i.times.length; n++) {
                var o = h(i.times[n].timeStart.toString().substring(0, 2)),
                    r = h(i.times[n].timeStop.toString().substring(0, 2)),
                    s = '<li><a href="#" class="schedule-time" data-slotKey="' + i.times[n].data.schSlotKey + '"><span>' + o + " - " + r + "</span></a></li>";
                a.append(s)
            }
            t()
        }(t.data("date"), function () {
            l.show()
        })), p()
    }), $(document).on("click", ".scheduler.times > li > a", function (e) {
        e.preventDefault();
        var t = $(this),
            a = $(".scheduler.times > li > a");
        t.hasClass("active") || (a.removeClass("active"), t.addClass("active"), p())
    }), $(document).on("SetDefaultDateAndTime", function () {
        $(".scheduler.dates > li:first-child > a").trigger("click"), $(".scheduler.times > li:first-child > a").trigger("click")
    })
}), $(window).on("load", function () {
    $(".AB_EVENT").each(function () {
        ga("create", utag.sender[15].data.account[0], "auto"), ga("send", "event", "_setABTest", "sucessfullySet", {
            dimension2: $(this).attr("id")
        })
    })
}), jQuery.extend(jQuery.expr[":"], {
    invalid: function (e, t, a) {
        var i = document.querySelectorAll(":invalid"),
            n = !1,
            o = i.length;
        if (o)
            for (var r = 0; r < o; r++)
                if (e === i[r]) {
                    n = !0;
                    break
                } return n
    }
}), $(document).ready(function () {
    checkURLParam("fromReferral") && $("#referralInput").length && ($("#referralInput > input").val(checkURLParam("fromReferral")), $("#referralInput").removeClass("hide")), _AUTO_ADDING_PEST_CONTROL_FEATURE_TOGGLE && "" === $("#MOSQ").text() && "" === $("#GENPEST").text() && $("#addItemGENPEST").trigger("click"), _MATERIALIZE && $(".slider").slider({
        full_width: !0,
        height: 200,
        interval: 7e3,
        indicators: !1
    }), $("#passwordMyAcccount").focus(function () {
        $(".passwordValidation").removeClass("hidden")
    }), $("#passwordMyAcccount").keyup(function () {
        $(".passwordValidation").hasClass("hidden") || validationPassword()
    }), $(".buyToggle").click(function (e) {
        e.stopPropagation(), $(this).toggleClass("disabled")
    }), heightNormalization("", ".sameHeight"), displayMosqSeason($("#templateCode").text()), "" != $("#totalAmountWithTax").text() && $("#mobileTotal").text($("#totalAmountWithTax").text()), changeTargetPest(), $('input[name="paymentType"]:first').click(), $('[data-toggle="tooltip"]').tooltip(), $("#viewCoveredPests").click(function () {
        $("#CoveredPests").toggleClass("hidden")
    }), $("#sendConfirmation").click(function () {
        OpenModal("sendEmailConfirmation-modal"), setTimeout(function () {
            CloseModal("sendEmailConfirmation-modal")
        }, 5e3)
    }), $("#phoneIns, #phoneTwin").mask("(999)999-9999"), $("#stepOneButton").on("click", function () {
        $(this).removeClass("disabled")
    }), $("#addressForm").submit(function (e) {
        return $("#over8000Modal").hasClass("in") || $("#over8000Modal").hasClass("open") ? (CloseModal("over8000Modal"), OpenModal("processing-modal"), "undefined" != typeof utag_data && (utag_data.customer_email = $("#email").val(), utag_data.customer_address = $("#address1").val() + " " + $("#address2").val(), utag_data.customer_zip = $("#zipCode").val(), utag_data.customer_property_option = $("#homeSize_hidden").val(), utag_data.customer_type = "offline"), procEvent("ecommerce", "address form success", "leadgen", "OVER 8000 SQ FT"), !0) : "8000" === $("#sqftSize option:selected").val() ? (OpenModal("over8000Modal"), $("#firstName").attr("required", !0), $("#lastName").attr("required", !0), $("#phone").attr("required", !0), $("#consent").attr("required", !0), $("#firstName").removeClass("invalid"), $("#lastName").removeClass("invalid"), $("#phone").removeClass("invalid"), $("#consent").removeClass("invalid"), $("#abPhone") && ($("#phone").val($("#abPhone").val()), Materialize.updateTextFields()), !1) : void OpenModal("processing-modal")
    }), $("#reqInspection").click(function (e) {
        isValid($("#firstName")) && isValid($("#lastName")) && isValid($("#phone")) && isValid($("#consent")) && $("#addressForm").submit()
    }), $("#billingAddressCheckbox").change(function () {
        _MATERIALIZE ? $("#billingAddress").toggleClass("hide") : $("#billingAddress").toggleClass("hidden")
    }), $("#emailMyQuote").submit(function (e) {
        e.preventDefault(), "" == $("#emailTo").val() || $("emailTo").hasClass("invalid") || submitAjaxForm("emailMyQuote", showResponseModal)
    }), $("#existingCustomer").change(function () {
        $(this).is(":checked") ? OpenModal("custLookupModal") : CloseModal("custLookupModal")
    }), checkURLParam("cNum") && $("#existingCustomer").length ? ($("#existingCustomer").prop("checked", !0), $("#customerNumber").val(checkURLParam("cNum")), $("#phoneTwin").val(checkURLParam("cTel")).trigger("change"), getCustomerDetails()) : $("#existingCustomer").prop("checked", !1)
}), $(document).on("blur change", "#expYear, #expMonth", function () {
    $("#expYear option:selected").val() && $("#expMonth option:selected").val() && ValidateExpDate()
}), $(document).on("change", "#ccType", function (e) {
    var t = $(this),
        a = GetCreditCardType($("#ccNumber").val());
    if (a && IsObject(a) && a.value.toLowerCase() === t.val().toLowerCase()) return t.removeClass("invalid"), void t.parent(".select-wrapper").children(".select-dropdown").removeClass("invalid");
    t.addClass("invalid"), t.parent(".select-wrapper").children(".select-dropdown").addClass("invalid")
}), $(document).on("keyup", "#ccNumber", function (e) {
    var t = $(this),
        a = $("#ccType");
    if (t.val()) {
        var i = GetCreditCardType(t.val());
        i && IsObject(i) ? a.val(i.value) : a.val(""), _MATERIALIZE && a.material_select()
    }
}), $(window).on("load", function () {
    turnButtonsOn(), productInCart($(".product")), $(window).trigger("scroll");
    var e = !1,
        t = "";
    0 < $("#error").length && (t = $("#error").find("li").text(), e = !0), -1 < window.location.href.indexOf("buyonline/step-one") && (e && procEvent("ecommerce", "address form error", t), 0 < $("#Consent").length && ($("#stepOneButton").addClass("disabled"), $("#Consent").change(function () {
        this.checked ? $("#stepOneButton").removeClass("disabled") : $("#stepOneButton").addClass("disabled")
    }))), 0 < $("#serviceScheduler").length && getSlotsForCalendar(void 0, void 0, "serviceScheduler"), 0 < $("#salesScheduler").length && getSlotsForCalendar("10001", "sales", "salesScheduler"), 0 < $("#preSchedule").length && getSlotsForAppointment(void 0, void 0, ""), (-1 < window.location.href.indexOf("buyonline/step-two") || -1 < window.location.href.indexOf("buyonline/step-2")) && (e ? procEvent("ecommerce", "add to cart error", t) : procEvent("ecommerce", "address form success", "online purchase")), (-1 < window.location.href.indexOf("buyonline/step-three") || -1 < window.location.href.indexOf("buyonline/step-3")) && (e ? procEvent("ecommerce", "submit billing info error", t) : procEvent("ecommerce", "add to cart success")), "free-inspection" == window.location.pathname.replace(/\//g, "") && e && procEvent("Termite Inspection Funnel", "Enter Address Page", t), -1 < window.location.href.indexOf("free-inspection/step-two") && (submitAjaxForm("addToCartForm", doNothing), e ? procEvent("Termite Inspection Funnel", "Schedule Appointment Page", t) : procEvent("Termite Inspection Funnel", "Enter Address Page", "address form completed")), -1 < window.location.href.indexOf("free-inspection/step-three") && procEvent("Termite Inspection Funnel", "Confirmation Page", utag_data.order_contract_ids[0]), (-1 < window.location.href.indexOf("buyonline/step-four") || -1 < window.location.href.indexOf("buyonline/step-4")) && (procEvent("ecommerce", "submit billing info success"), procEvent("Pest Funnel", "Pest Test", utag_data.customer_id))
}), $("#addItemGENPEST, #removeItemGENPEST").click(function () {
    $("#addItemGENPEST, #removeItemGENPEST").toggleClass("hidden"), $("#addItemGENPEST, #removeItemGENPEST").toggleClass("hide")
}), $("#removeItemMOSQ, #addItemMOSQ").click(function () {
    $("#addItemMOSQ, #removeItemMOSQ").toggleClass("hidden"), $("#addItemMOSQ, #removeItemMOSQ").toggleClass("hide")
}), $("#maddItemGENPEST, #mremoveItemGENPEST").click(function () {
    $("#maddItemGENPEST, #mremoveItemGENPEST").toggleClass("hidden"), $("#maddItemGENPEST, #mremoveItemGENPEST").toggleClass("hide")
}), $("#mremoveItemMOSQ, #maddItemMOSQ").click(function () {
    $("#maddItemMOSQ, #mremoveItemMOSQ").toggleClass("hidden"), $("#maddItemMOSQ, #mremoveItemMOSQ").toggleClass("hide")
}), $(window).scroll(function () {
    if ($(window).width() < 540) {
        var e = $("#stickyHeader").outerHeight(!0);
        $(window).scrollTop() >= e + $("#stickyHeader").scrollTop() ? $("#stickyHeader").css({
            position: "fixed",
            "z-index": "100",
            top: "0px",
            width: "inherit",
            "background-color": "white"
        }) : $("#stickyHeader").css({
            position: "inherit",
            "z-index": "1",
            width: "initial"
        })
    }
}), $(document).ready(function () {
    $("#confirm").length && initSession()
});
var sessionIntervalID, sessionLastActivity, sessionPingInt = 6e4,
    sessionExpirationMinutes = 30,
    sessionTimeoutVar = 9e5;

function initSession() {
    sessionLastActivity = new Date, sessionSetInterval(), $(document).keypress(function (e) {
        sessionKeyPress(e)
    })
}

function resetAll() {
    void 0 !== sessionIntervalID && clearInterval(sessionIntervalID), "undefined" != typeof countDownInterval && clearInterval(countDownInterval), sessionLastActivity = new Date, sessionSetInterval(), CloseModal("confirm")
}

function continueSession() {
    $.post("/includes/ping.jsp"), resetAll(), sessionSetInterval()
}

function sessionSetInterval() {
    sessionIntervalID = setInterval("sesionInterval()", 174e4), sessionInterval = setInterval("sesionIntervalEnd()", 18e5)
}

function sessionKeyPress(e) {
    resetAll()
}

function sessionExp() {
    CloseModal("confirm"), $("form :button").each(function () {
        $(this).attr("disabled", !0)
    }), window.location.href = "/buyonline/step-one"
}

function sesionIntervalEnd() {
    var e = new Date;
    sessionExpirationMinutes <= (e - sessionLastActivity) / 1e3 / 60 && (procEvent("SessionConfirmation", "Session Expired", "From " + window.location.pathname), sessionExp())
}

function sesionInterval() {
    countDown(), OpenModal("confirm"), procEvent("SessionConfirmation", "Session dieing in 1 min", "From " + window.location.pathname)
}

function countDown() {
    var e = 60;
    countDownInterval = setInterval(function () {
        if (e -= 1, $("#countDownTime").html(e), e <= 0) return clearInterval(countDownInterval), void $("#countDownTime").html("")
    }, 1e3)
}

function getCustomerDetails() {
    var e = addProcessor("CustLookupProcessor", {
        customerNumber: $("#customerNumber").val(),
        phone: $("#phoneTwin").val()
    });
    CloseModal("custLookupModal"), OpenModal("processing-modal"), $.ajax({
        type: "POST",
        url: "/admin/dwr/jsonp/TmxGenericProcessor/startProcess?jsonData=" + JSON.stringify(e),
        dataType: "json",
        success: function (e) {
            var t = JSON.parse(e.reply).PROCESSORARRAY[0].PROCESSOROUTPUT;
            if (null == t.type || "BSN" != t.type)
                if (CloseModal("processing-modal"), null != t.address1) {
                    $("#currentAddress").attr("data-address", JSON.stringify(t));
                    var a = t.address1 + " " + t.city + ", " + t.state + " " + t.zip;
                    $("label[for='currentAddress']").text(a), OpenModal("custDetailsModal")
                } else OpenModal("custDetailsFailModal"), clearCustomerInfo();
            else window.location = "/request-commercial-quote/"
        },
        error: function (e) {
            console.log(e)
        }
    })
}

function pushCustomerDetails() {
    if (CloseModal("custDetailsModal"), $("#currentAddress").is(":checked")) {
        var e = JSON.parse($("#currentAddress").attr("data-address"));
        $("#address1").val(e.address1).focus(), $("#city_hidden").val(e.city), $("#state_hidden").val(e.state), $("#zipCode").val(e.zip).focus()
    }
}

function clearCustomerInfo() {
    $("#customerNumber").val(""), $("#phoneTwin").val("").trigger("change"), $("#existingCustomer").attr("checked", !1)
}

function addProcessor(e, t, a) {
    null != a && null != a || (a = {}), null == a.PROCESSORARRAY && (a.PROCESSORARRAY = []);
    var i = {};
    return i.PROCESSOR = e, i.PROCESSORINPUT = t, a.PROCESSORARRAY.push(i), a
}
$(".yes").click(function () {
    continueSession()
}), document.addEventListener("DOMContentLoaded", function (e) {
    var t = document.getElementById("easyPaySelected");
    document.getElementsByClassName("productSelected").length || null === t || t.click()
}), $(".twinInput").change(function () {
    var e = $(this).data("twin");
    $("#" + e).val($(this).val())
});
var QAS_Variables = {
        PROXY_PATH: "/includes/qas_atg_proxy.jsp",
        ADD_PROXY_PATH: "/includes/add_atg_proxy.jsp",
        TD_PROXY_PATH: "/includes/email_atg_proxy.jsp",
        QAS_LAYOUT: "ATG",
        PRE_ON_CLICK: null,
        POST_ON_CLICK: function () {
            handleGetFreeQuote()
        },
        BUTTON_ID: "",
        ADDRESS_FIELD_IDS: [["address1", "address2", "city_hidden", "state_hidden", "zipCode"]],
        modifyIncomingValueRules: {},
        COUNTRY_FIELD_IDS: ["false"],
        DATA_SETS: ["USA", "CAN"],
        ADD_DATA_SETS: ["BRA", "CHN", "HKG", "IND", "IDN", "ITA", "JPN", "KOR", "MEX", "RUS", "ESP", "UKR", "VNM"],
        DEFAULT_DATA: "USA",
        COUNTRY_MAP: [["US", "USA"], ["U.S.", "USA"], ["U.S.A.", "USA"], ["United States", "USA"], ["United States of America", "USA"], ["Canada", "CAN"], ["CA", "CAN"], ["China", "CHN"], ["Brazil", "BRA"], ["Hong Kong", "HKG"], ["India", "IND"], ["Indonesia", "IDN"], ["Italy", "ITA"], ["Japan", "JPN"], ["Korea", "KOR"], ["Mexico", "MEX"]],
        PHONE_VALIDATE_COUNTRY: ["AIA", "ATG", "BHS", "BRB", "BMU", "CAN", "CYM", "DMA", "DOM", "GRD", "JAM", "MSR", "KNA", "LCA", "TCA", "TTA", "USA", "VCT", "VGB", "VIR"],
        LVR: 7,
        EMAIL_FIELD_IDS: ["qas_filler"],
        EMAIL_ERR_FIELD_IDS: ["email_error"],
        PHONE_FIELD_IDS: ["qas_filler"],
        PHONE_ERR_FIELD_IDS: ["phone_error"],
        DISPLAY_CUSTOM_EMAIL_ERR: !1,
        DISPLAY_CUSTOM_PHONE_ERR: !1,
        EMAIL_VAL_LEVEL: "2",
        EMAIL_PHONE_NUM_SUBMITS: 2,
        ADDRESS_INTERACTION: !0,
        EMAIL_PHONE_INTERACTION: !0,
        EMAIL_PHONE_USEDIALOG: !0,
        DISPLAY_ERRORS: -1 < window.location.search.indexOf("debug=1"),
        TIMEOUT: 6e3,
        TIMEOUT_EMAILPHONE: 6e3,
        DISPLAY_LINES: 5
    },
    QAS_PROMPTS = {
        InteractionRequired: {
            header: "<b>We think that your address may be incorrect or incomplete.</b><br />To proceed, please choose one of the options below.",
            prompt: "We recommend:",
            button: "Use suggested address"
        },
        PremisesPartial: {
            header: "<b>Sorry, we think your apartment/suite/unit is missing or wrong</b><br />To proceed, please enter your apartment/suite/unit or use your address as entered",
            prompt: "Confirm your Apartment/Suite/Unit number:",
            button: "Confirm number",
            showPicklist: "Show all potential matches",
            invalidRange: "Secondary information not within valid range"
        },
        StreetPartial: {
            header: "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
            prompt: "Confirm your House/Building number:",
            button: "Confirm number",
            showPicklist: "Show all potential matches",
            invalidRange: "Primary information not within valid range"
        },
        DPVPartial: {
            header: "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
            prompt: "Confirm your House/Building number:",
            button: "Confirm number"
        },
        AptAppend: {
            header: "<b>Sorry, we think your apartment/suite/unit may be missing.</b><br />To proceed, please check and choose from one of the options below.",
            prompt: "Confirm Apt/Ste:",
            button: "Continue",
            noApt: "I do not have an apt or suite"
        },
        Multiple: {
            header: "<b>We found more than one match for your address.</b><br />To proceed, please choose one of the options below.",
            prompt: "Our suggested matches:"
        },
        None: {
            header: "<b>Sorry, we could not find a match for your address.</b><br />To proceed, please choose one of the options below."
        },
        RightSide: {
            prompt: "You Entered:",
            edit: "Edit",
            button: "Use Address As Entered",
            warning: "<b>*Your address may be undeliverable</b>"
        },
        ConfirmEmailPhone: {
            header: "<b>Sorry we could not confirm your e-mail address and phone number</b><br />To proceed, please confirm your e-mail address and phone number below.",
            headerPhone: "<b>Sorry we could not confirm your phone number</b><br />To proceed, please confirm your phone number below.",
            headerEmail: "<b>Sorry we could not confirm your e-mail address</b><br />To proceed, please confirm your e-mail address below.",
            promptEmail: "Confirm or edit your e-mail address",
            promptPhone: "Confirm or edit your phone number"
        },
        waitMessage: "Please wait, your details are being verified",
        title: "VERIFY YOUR ADDRESS DETAILS",
        emailphoneTitle: "Verify your contact details"
    },
    EMAIL_ERR_MESSAGES = {
        5: "Validation Timeout",
        10: "Syntax OK",
        20: "Syntax OK and domain valid according to the domain database",
        30: "Syntax OK and domain exists",
        40: "Syntax OK, domain exists, and domain can receive email",
        50: "Syntax OK, domain exists, and mailbox does not reject mail",
        100: "Email has a general syntax error",
        110: "Email has an invalid character",
        115: "Email domain syntax is invalid",
        120: "Email username syntax is invalid",
        125: "Username syntax is invalid for that domain",
        130: "Email is too long",
        135: "Incorrect parentheses, brackets, or quotes",
        140: "Email does not have a username",
        145: "Email does not have a domain",
        150: "Email does not have an @ sign",
        155: "Email has more than one @ sign",
        200: "Email has an invalid top-level-domain",
        205: "Email cannot have an IP address as domain",
        210: "Email address contains space or extra text",
        215: "Email has unquoted spaces",
        310: "Email domain is invalid",
        315: "Email domain IP address is not valid",
        325: "Email domain cannot receive email",
        400: "Email username is invalid or nonexistent",
        410: "Email mailbox is full",
        420: "Email is not accepted for this domain",
        500: "Email username is not permitted",
        505: "Emails domain is not permitted",
        510: "Email is suppressed and not permitted"
    },
    PHONE_ERR_MESSAGES = {
        5: "Validation Timeout",
        10: "Successfully Parsed and Standardized, Area Code and Exchange Match",
        100: "Area code contains invalid exchange digits",
        110: "Invalid area code and exchange",
        120: "Phone number has too few digits",
        130: "Phone number has too many digits",
        133: "Phone exchange and number not allowed",
        134: "Phone number exchange not allowed",
        135: "Phone number not allowed",
        140: "Extension greater than 5 digits",
        150: "Toll free number was entered",
        160: "900 numbers was entered"
    },
    QAS_TEMP_VARS = {
        NUM_EMAIL_PHONE_SUBMITS: 0,
        EMAIL_PHONE_POS: 0
    };

function Address() {
    var n, o, r, s = QAS_Variables.ADDRESS_FIELD_IDS,
        l = QAS_Variables.COUNTRY_FIELD_IDS,
        d = [],
        a = [],
        i = new Array(s.length),
        e = [],
        t = [],
        c = [],
        u = QAS_Variables.DATA_SETS,
        m = QAS_Variables.ADD_DATA_SETS,
        p = function (e, t) {
            for (var a = !1, i = 0, n = 0; i < e.length;) {
                if ("" !== e[i] && (a = !0), void 0 === e[i]) return !1;
                i++
            }
            if (a) {
                for (n = 0; n < u.length; n++)
                    if (t === u[n]) return !0;
                for (n = 0; n < m.length; n++)
                    if (t === m[n]) return !0
            }
            return !1
        };
    this.getSearchStrings = function () {
            return e
        }, this.getSearchCountries = function () {
            return t
        }, this.getOriginalAddresses = function () {
            return a
        }, this.storeCleanedAddress = function (e) {
            c.push(e)
        }, this.returnCleanAddresses = function () {
            ! function () {
                for (n = 0; n < s.length; n++)
                    if (void 0 !== c[i[n]])
                        for (o = 0; o < s[n].length; o++)
                            if (QAS_Variables.modifyIncomingValueRules[s[n][o]]) {
                                var e = decodeURIComponent(c[i[n]][o]);
                                $("#" + s[n][o]).val(QAS_Variables.modifyIncomingValueRules[s[n][o]](e))
                            } else $("#" + s[n][o]).val(decodeURIComponent(c[i[n]][o]))
            }()
        },
        function () {
            for (n = 0; n < s.length; n++) {
                var e = [];
                for (o = 0; o < s[n].length; o++) {
                    var t = $("#" + s[n][o]).val(),
                        a = encodeURIComponent(t);
                    void 0 === a ? QAS_Variables.DISPLAY_ERRORS && alert("ID '" + s[n][o] + "' is undefined") : a = a.replace(/^\s+|\s+$/g, ""), e.push(a)
                }
                var i = $("#" + l[n]).val();
                for ("" !== i && void 0 !== i || (i = QAS_Variables.DEFAULT_DATA), r = 0; r < QAS_Variables.COUNTRY_MAP.length; r++) i.toLowerCase() === QAS_Variables.COUNTRY_MAP[r][0].toString().toLowerCase() && (i = QAS_Variables.COUNTRY_MAP[r][1].toString());
                e.push(i), d.push(e)
            }
        }(),
        function () {
            var e = !0,
                t = 0;
            for (n = 0; n < d.length; n++) {
                for (i[n] = a.length, e = !0, t = 0; e && t < a.length;) d[n].toString().toLowerCase() === a[t].toString().toLowerCase() && (e = !1, i[n] = t), t++;
                e && a.push(d[n])
            }
        }(),
        function () {
            for (n = 0; n < a.length; n++) t.push(a[n].pop()), p(a[n], t[n]) ? e.push(a[n].join("|")) : e.push(!1)
        }()
}

function Clean(e, t, a) {
    var n, i, o = this,
        r = a,
        s = QAS_Variables.TIMEOUT,
        l = !1,
        d = !1,
        c = "",
        u = e,
        m = function (e) {
            for (i = 0; i < QAS_Variables.ADD_DATA_SETS.length; i++)
                if (e === QAS_Variables.ADD_DATA_SETS[i]) return QAS_Variables.ADD_PROXY_PATH;
            return QAS_Variables.PROXY_PATH
        }(t),
        p = function (e) {
            switch (o.country) {
                case "AUS":
                    e = e.replace(/\d{4}$/, "");
                    break;
                case "DEU":
                    e = e.replace(/\d{5}-\d{5}$/, "");
                    break;
                case "DNK":
                    e = e.replace(/\s\d{4}\s/, " ");
                    break;
                case "FRA":
                    e = e.replace(/\s\d{5}\s/, " ");
                    break;
                case "GBR":
                    e = e.replace(/\w{1,2}\d{1,2}\w?\s\d\w{2}$/, "");
                    break;
                case "LUX":
                    e = e.replace(/\s\d{4}\s/, "");
                    break;
                case "NLD":
                    e = e.replace(/\s\d{4}\s\w{2}\s/, " ");
                    break;
                case "NZL":
                    e = e.replace(/\d{4}$/, "");
                    break;
                case "SGP":
                    e = e.replace(/\d{6}$/, "");
                    break;
                case "USA":
                    e = e.replace(/-\d{4}$/, "")
            }
            return e
        },
        h = function () {
            o.result.push($(this).text())
        },
        S = function () {
            var e = $(this).find("partialtext").text(),
                t = $(this).find("addresstext").text(),
                a = $(this).find("postcode").text(),
                i = $(this).find("moniker").text(),
                n = $(this).find("fulladdress").text();
            o.result.push({
                partialText: e,
                addressText: t,
                postCode: a,
                moniker: i,
                fulladdress: n
            })
        },
        A = function (e) {
            o.verifylevel = $(e).find("verifylevel").text(), o.dpv = $(e).find("dpvstatus").text(), o.error = $(e).find("error").text(), "" !== o.error && QAS_Variables.DISPLAY_ERRORS ? r(e, o.error, "Error") : (l && "PremisesPartial" === o.verifylevel ? l = !1 : d && "StreetPartial" === o.verifylevel ? d = !1 : (o.result = [], d = l = !1, o.missingsubprem = !1, "Verified" === o.verifylevel || "VerifiedStreet" === o.verifylevel || "VerifiedPlace" === o.verifylevel || "InteractionRequired" === o.verifylevel ? ($(e).find("line").each(h), o.missingsubprem = $(e).find("missingsubprem").text()) : ($(e).find("picklistitem").each(S), "PremisesPartial" !== o.verifylevel && "StreetPartial" !== o.verifylevel || null === (c = function () {
                var e;
                for (e = 0; e < o.result.length; e++)
                    if ("false" === o.result[e].fulladdress.toString().toLowerCase()) return o.result[e].partialText;
                return null
            }()) && (o.verifylevel = "Multiple"))), n())
        },
        _ = function (e) {
            $.ajax({
                type: "POST",
                url: m,
                async: !0,
                data: e,
                dataType: "xml",
                success: A,
                timeout: s,
                error: r
            })
        },
        f = function (e, t) {
            var a = {
                action: "search",
                addlayout: QAS_Variables.QAS_LAYOUT,
                country: t,
                searchstring: e
            };
            _(a)
        };
    this.result = [], this.verifylevel = "", this.dpv = "", this.error = "", this.missingsubprem = !1, this.country = t, this.search = function (e) {
        n = e, f(u, o.country)
    }, this.searchPremisesPartial = function (e, t) {
        n = t, l = !0;
        var a = p(decodeURIComponent(c)),
            i = encodeURIComponent(a.replace(/,/, " # " + e + ","));
        f(i, o.country)
    }, this.searchStreetPartial = function (e, t) {
        n = t, d = !0;
        var a = p(decodeURIComponent(c)),
            i = encodeURIComponent(e + " " + a);
        f(i, o.country)
    }, this.searchDPVPartial = function (e, t) {
        n = t;
        var a = o.result.join("|");
        a = encodeURIComponent(a.replace(/\|?\d+\w*\s/, "|" + e + " ")), f(a, o.country)
    }, this.formatAddress = function (e, t) {
        var a, i;
        n = t, a = e, i = {
            action: "GetFormattedAddress",
            addlayout: QAS_Variables.QAS_LAYOUT,
            moniker: a
        }, _(i)
    }, this.refineAddress = function (e, t) {
        var a, i;
        n = t, a = e, i = {
            action: "refine",
            addlayout: QAS_Variables.QAS_LAYOUT,
            moniker: a,
            refinetext: ""
        }, _(i)
    }
}

function Business(e, t, a, i) {
    var n = this,
        o = e,
        r = t,
        s = a,
        l = i,
        d = "",
        c = 0,
        u = function (e) {
            return !r.result[e] || -1 !== r.result.join("|").search(/\|?\d+\s*-\s*\d+/)
        };
    this.noInteraction = function () {
        "Verified" === r.verifylevel || "VerifiedStreet" === n.verifylevel || "VerifiedPlace" === n.verifylevel || "InteractionRequired" === r.verifylevel ? o() : n.useOriginal()
    }, this.processResult = function () {
        switch (c++, r.verifylevel) {
            case "Verified":
            case "VerifiedStreet":
                "USA" === r.country ? "DPVNotConfirmed" === t.dpv ? (l.setDPVPartial(s, QAS_PROMPTS.DPVPartial, n.refineDPV, n.useOriginal), l.display()) : "DPVConfirmedMissingSec" === t.dpv ? (l.setInterReq(r.result, s, QAS_PROMPTS.InteractionRequired, n.acceptInter, n.useOriginal), l.display()) : o() : "CAN" === r.country ? u(QAS_Variables.LVR - 1) ? o() : (l.setAptAppend(s, QAS_PROMPTS.AptAppend, n.appendApt, o, n.useOriginal), l.display()) : o();
                break;
            case "VerifiedPlace":
            case "InteractionRequired":
                "CAN" !== r.country || u(QAS_Variables.LVR - 1) ? 1 < c ? o() : (l.setInterReq(r.result, s, QAS_PROMPTS.InteractionRequired, n.acceptInter, n.useOriginal), l.display()) : (l.setAptAppend(s, QAS_PROMPTS.AptAppend, n.appendApt, o, n.useOriginal), l.display());
                break;
            case "PremisesPartial":
                l.setPartial(r.result, s, QAS_PROMPTS.PremisesPartial, n.refineApt, n.acceptMoniker, n.useOriginal), l.display(), "PremisesPartial" === d && alert(QAS_PROMPTS.PremisesPartial.invalidRange), d = "PremisesPartial";
                break;
            case "StreetPartial":
                l.setPartial(r.result, s, QAS_PROMPTS.StreetPartial, n.refineBuild, n.acceptMoniker, n.useOriginal), l.display(), "StreetPartial" === d && alert(QAS_PROMPTS.StreetPartial.invalidRange), d = "StreetPartial";
                break;
            case "Multiple":
                l.setMultiple(r.result, s, QAS_PROMPTS.Multiple, n.acceptMoniker, n.refineMult, n.useOriginal), l.display();
                break;
            case "None":
                l.setNone(s, QAS_PROMPTS.None, n.useOriginal), l.display()
        }
    }, this.acceptInter = function () {
        o()
    }, this.acceptMoniker = function (e) {
        r.formatAddress(e, o)
    }, this.refineApt = function () {
        var e = $("#QAS_RefineText").val();
        r.searchPremisesPartial(e, n.processResult)
    }, this.refineBuild = function () {
        var e = $("#QAS_RefineText").val();
        r.searchStreetPartial(e, n.processResult)
    }, this.refineDPV = function () {
        var e = $("#QAS_RefineText").val();
        r.searchDPVPartial(e, n.processResult)
    }, this.appendApt = function () {
        for (var e = $("#QAS_RefineText").val(), t = 0, a = !1; !a && t < r.result.length;) - 1 !== decodeURIComponent(r.result[t]).search(/^\d+\s/) && (a = !0, r.result[t] = e + "-" + r.result[t]), t++;
        o()
    }, this.refineMult = function (e) {
        r.refineAddress(e, n.processResult)
    }, this.useOriginal = function () {
        r.result = s, o()
    }
}

function Interface(e) {
    var r, s, i = e,
        l = "",
        d = function (e) {
            var t, a = "";
            for (t = 0; t < s.length; t++) a += "<tr><td>" + decodeURIComponent(s[t]) + "</td></tr>";
            $(".QAS_RightDetails").html("<div class='QAS_RightSidePrompt'><div class='QAS_RightSidePromptText'>" + QAS_PROMPTS.RightSide.prompt + "<span class='QAS_EditLink'>[<a href='#' id='QAS_Edit'>" + QAS_PROMPTS.RightSide.edit + "</a>]</span></div></div><table>" + a + "</table><input type='button' id='QAS_AcceptOriginal' value='" + QAS_PROMPTS.RightSide.button + "' />"), $("#QAS_AcceptOriginal").button(), $("#QAS_AcceptOriginal").click(function () {
                $("#QAS_Dialog").dialog("close"), e()
            }), $("#QAS_Edit").click(function () {
                $("#QAS_Dialog").dialog("close"), i()
            })
        };
    this.waitOpen = function () {
        $("#QAS_Wait").dialog("open"), $(".ui-dialog-titlebar-close").css("display", "none"), $(".ui-dialog-content").hide()
    }, this.waitClose = function () {
        $("#QAS_Wait").dialog("close")
    }, this.display = function () {
        window.scroll(0, 0), $("#QAS_Dialog").dialog("open"), $(".ui-dialog-titlebar-close").css("display", "none"), $("#QAS_RefineBtn").blur(), $(".QAS_Header").focus()
    }, this.setInterReq = function (e, t, a, i, n) {
        s = t, a;
        var o, r = "";
        for (d(n), o = 0; o < QAS_Variables.DISPLAY_LINES; o++) r += "<tr><td>" + decodeURIComponent(e[o]) + "</td></tr>";
        $(".QAS_Header").html(a.header), $(".QAS_PromptText").html(a.prompt), $(".QAS_PromptData").html("<table>" + r + "</table>"), $(".QAS_Input").html("<input type='button' id='QAS_RefineBtn' value='" + a.button + "' />"), $(".QAS_MultPick").html(""), $(".QAS_ShowPick").html(""), $(".QAS_Pick").html(""), $(".QAS_MultPick").hide(), $("#QAS_RefineBtn").button(), $("#QAS_RefineBtn").click(function () {
            $("#QAS_Dialog").dialog("close"), i()
        })
    }, this.setPartial = function (e, t, a, i, n, o) {
        r = e, s = t, a,
            function () {
                var e;
                for (l = "", e = 0; e < r.length; e++) "true" === r[e].fulladdress.toString().toLowerCase() ? l += "<tr><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].postCode) + "</a></td></tr>" : l += "<tr><td NOWRAP>" + decodeURIComponent(r[e].addressText) + "</td><td NOWRAP>" + decodeURIComponent(r[e].postCode) + "</td></tr>"
            }(), d(o), $(".QAS_Header").html(a.header), $(".QAS_PromptText").html(a.prompt), $(".QAS_PromptData").html(""), $(".QAS_Input").html("<input type='text' id='QAS_RefineText' /><input type='button' id='QAS_RefineBtn' value='" + a.button + "' />"), $(".QAS_MultPick").html(""), $(".QAS_ShowPick").html("<a href='#'>" + a.showPicklist + "</a>"), $(".QAS_Pick").html("<table>" + l + "</table>"), $(".QAS_MultPick").hide(), $("#QAS_RefineBtn").button(), $("#QAS_RefineBtn").click(function () {
                "" === $("#QAS_RefineText").val() ? alert("No value entered") : ($("#QAS_Dialog").dialog("close"), i())
            }), $(".QAS_StepIn").click(function () {
                $("#QAS_Dialog").dialog("close");
                var e = $(this).attr("moniker");
                n(e)
            })
    }, this.setDPVPartial = function (e, t, a, i) {
        s = e, t, d(i), $(".QAS_Header").html(t.header), $(".QAS_PromptText").html(t.prompt), $(".QAS_PromptData").html(""), $(".QAS_Input").html("<input type='text' id='QAS_RefineText' /><input type='button' id='QAS_RefineBtn' value='" + t.button + "' />"), $(".QAS_MultPick").html(""), $(".QAS_MultPick").hide(), $("#QAS_RefineBtn").button(), $("#QAS_RefineBtn").click(function () {
            "" === $("#QAS_RefineText").val() ? alert("No value entered") : ($("#QAS_Dialog").dialog("close"), a())
        })
    }, this.setAptAppend = function (e, t, a, i, n) {
        s = e, t, d(n), $(".QAS_Header").html(t.header), $(".QAS_PromptText").html(t.prompt), $(".QAS_PromptData").html(""), $(".QAS_Input").html("<input type='text' id='QAS_RefineText' /><input type='button' id='QAS_RefineBtn' value='" + t.button + "' /><br /><input type='button' id='QAS_NoApt' value='" + t.noApt + "' />"), $(".QAS_MultPick").html(""), $(".QAS_MultPick").hide(), $("#QAS_RefineBtn").button(), $("#QAS_NoApt").button(), $("#QAS_RefineBtn").click(function () {
            "" === $("#QAS_RefineText").val() ? alert("No value entered") : ($("#QAS_Dialog").dialog("close"), a())
        }), $("#QAS_NoApt").click(function () {
            $("#QAS_Dialog").dialog("close"), i()
        })
    }, this.setMultiple = function (e, t, a, i, n, o) {
        r = e, s = t, a,
            function () {
                var e;
                for (l = "", e = 0; e < r.length; e++) "true" === r[e].fulladdress.toString().toLowerCase() ? l += "<tr><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_StepIn' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].postCode) + "</a></td></tr>" : l += "<tr><td NOWRAP><a href='#' class='QAS_Refine' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].addressText) + "</a></td><td NOWRAP><a href='#' class='QAS_Refine' moniker='" + r[e].moniker + "'>" + decodeURIComponent(r[e].postCode) + "</a></td></tr>"
            }(), d(o), $(".QAS_Header").html(a.header), $(".QAS_PromptText").html(a.prompt), $(".QAS_PromptData").html(""), $(".QAS_Input").html(""), $(".QAS_MultPick").html("<table>" + l + "</table>"), $(".QAS_ShowPick").html(""), $(".QAS_Pick").html(""), $(".QAS_MultPick").show(), $(".QAS_StepIn").click(function () {
                $("#QAS_Dialog").dialog("close");
                var e = $(this).attr("moniker");
                i(e)
            }), $(".QAS_Refine").click(function () {
                $("#QAS_Dialog").dialog("close");
                var e = $(this).attr("moniker");
                n(e)
            })
    }, this.setNone = function (e, t, a) {
        s = e, t, d(a), $(".QAS_Header").html(t.header), $(".QAS_Prompt").remove(), $(".QAS_Input").remove(), $(".QAS_MultPick").html(""), $(".QAS_ShowPick").remove(), $(".QAS_Pick").remove(), $(".QAS_RightDetails").css("float", "left"), $(".QAS_MultPick").hide()
    }, $("#QAS_Dialog").remove(), $("#QAS_Wait").remove(), $(document.body).append("<div id='QAS_Dialog' title='" + QAS_PROMPTS.title + "'>  <div class='QAS_Header ui-state-highlight'></div>  <div class='QAS_LeftDetails'>  <div class='QAS_Prompt'>    <div class='QAS_PromptText'></div>    <div class='QAS_PromptData'></div>         <div class='QAS_Input'></div>  </div>  <div class='QAS_Picklist'>    <div class='QAS_MultPick'></div>    <div class='QAS_ShowPick'></div>    <div class='QAS_Pick'></div>  </div>  </div>  <div class='QAS_RightDetails'></div></div><div id='QAS_Wait' title = '" + QAS_PROMPTS.waitMessage + "'></div>"), $("#QAS_Dialog").dialog({
        modal: !0,
        width: 850,
        autoOpen: !1,
        closeOnEscape: !1,
        resizable: !1,
        draggable: !1
    }), $("#QAS_Wait").dialog({
        modal: !0,
        height: 100,
        width: 200,
        autoOpen: !1,
        closeOnEscape: !1,
        resizable: !1,
        draggable: !1
    }), $(".QAS_ShowPick").click(function () {
        $(".QAS_Pick").slideToggle("slow")
    }), $(window).resize(function () {
        $("#QAS_Dialog").dialog("option", "position", "center")
    })
}

function EmailPhoneInterface(e) {
    var u;
    this.waitOpen = function () {
        $("#QAS_Wait").dialog("open"), $(".ui-dialog-titlebar-close").css("display", "none"), $(".ui-dialog-content").hide()
    }, this.waitClose = function () {
        $("#QAS_Wait").dialog("close")
    }, this.display = function () {
        window.scroll(0, 0), $("#QAS_Dialog").dialog("open"), $(".ui-dialog-titlebar-close").css("display", "none"), $("#QAS_RefineBtn").blur(), $(".QAS_Header").focus()
    }, this.displayResult = function (e, t) {
        var a, i, n, o = 0,
            r = $("#QAS_Dialog").children(),
            s = r.length,
            l = 0,
            d = 0,
            c = !1;
        for (u = e, n = 0; n < s; n++) i = r[n], "QAS_EmailPrompt" === $(i).attr("class") ? l = n : "QAS_PhonePrompt" === $(i).attr("class") && (d = n);
        if ($(".QAS_Header").html(""), $(".QAS_EmailPromptText").html(""), $(".QAS_EmailErrText").html(""), $(".QAS_EmailInput").html(""), $(".QAS_EmailPromptData").html(""), $(".QAS_PhonePromptText").html(""), $(".QAS_PhoneErrText").html(""), $(".QAS_PhoneInput").html(""), $(".QAS_EmailSuggPrompt").html(""), void 0 !== u.email && null !== u.email && !1 === u.email.ok) {
            if (c = !0, $(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.headerEmail), $(".QAS_EmailPromptText").html(QAS_PROMPTS.ConfirmEmailPhone.promptEmail), !0 === QAS_Variables.DISPLAY_CUSTOM_EMAIL_ERR && $(".QAS_EmailErrText").html(EMAIL_ERR_MESSAGES[u.email.status_code]), $(".QAS_EmailInput").html("<input type='text' id='QAS_EmailRefineText' value='" + u.email.address + "' />"), void 0 !== u.email.corrections && null !== u.email.corrections && 0 < u.email.corrections.length) {
                for ($(".QAS_EmailSuggPrompt").html("Suggestions:"), a = "<table id='QAS_EmailSuggestions'><tbody>", n = 0; n < u.email.corrections.length; n++) a += "<tr><td>" + u.email.corrections[n] + "</td></tr>";
                a += "</tbody></table>", $(".QAS_EmailPromptData").html(a)
            }
            d < l && (i = r[l], r[l] = r[d], r[d] = i), o++
        }
        void 0 !== u.phone && null !== u.phone && !1 === u.phone.ok && (!0, $(".QAS_PhonePromptText").html(QAS_PROMPTS.ConfirmEmailPhone.promptPhone), !0 === QAS_Variables.DISPLAY_CUSTOM_PHONE_ERR && $(".QAS_PhoneErrText").html(PHONE_ERR_MESSAGES[e.phone.status_code]), $(".QAS_PhoneInput").html("<input type='text' id='QAS_PhoneRefineText' value='" + $("input#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val() + "' />"), 1 === o ? ($(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.header), $(".QAS_PhonePrompt").css("float", "right")) : ($(".QAS_Header").html(QAS_PROMPTS.ConfirmEmailPhone.headerPhone), $(".QAS_PhonePrompt").css("float", "left"), l < d && (i = r[l], r[l] = r[d], r[d] = i)), o++), $(r).remove(), $("#QAS_Dialog").append($(r)), !0 === c && $("tbody td").click(function (e) {
            $("input#QAS_EmailRefineText").val($(this).text())
        }), 1 === o ? ($("#QAS_Dialog").dialog({
            width: 550
        }), $(".QAS_EmailInput").focus()) : $("#QAS_Dialog").dialog({
            width: 800
        }), 0 < o && ($(".QAS_EmailPhoneContinue").html("<input type='button' id='QAS_TDContinue' value='Continue' />"), $("#QAS_TDContinue").button(), $("#QAS_TDContinue").click(function () {
            var e = "action=validate";
            void 0 !== u.phone && null !== u.phone && !1 === u.phone.ok && ($("#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val($("#QAS_PhoneRefineText").val()), e = e + "&phone=" + $("#QAS_PhoneRefineText").val()), void 0 !== u.email && null !== u.email && !1 === u.email.ok && ($("#" + QAS_Variables.EMAIL_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val($("#QAS_EmailRefineText").val()), e = (e = e + "&email=" + $("#QAS_EmailRefineText").val()) + "&emaillev=" + QAS_Variables.EMAIL_VAL_LEVEL), $("#QAS_Dialog").dialog("close"), t(e)
        }))
    };
    $("#QAS_Wait").remove(), $("#QAS_Dialog").remove(), $(document.body).append("<div id='QAS_Dialog' title='" + QAS_PROMPTS.emailphoneTitle + "'>  <div class='QAS_Header ui-state-highlight'></div>  <div class='QAS_EmailPrompt'>    <div class='QAS_EmailPromptText'></div>    <div class='QAS_EmailErrText'></div>    <div class='QAS_EmailInput'></div>    <div class='QAS_EmailSuggPrompt'></div>    <div class='QAS_EmailPromptData'></div>  </div>  <div class='QAS_PhonePrompt'>    <div class='QAS_PhonePromptText'></div>    <div class='QAS_PhoneErrText'></div>    <div class='QAS_PhoneInput'></div>  </div>  <div class='QAS_EmailPhoneContinue'></div></div><div id='QAS_Wait' title='" + QAS_PROMPTS.waitMessage + "'></div>"), $("#QAS_Wait").dialog({
        modal: !0,
        height: 100,
        width: 200,
        autoOpen: !1,
        closeOnEscape: !1,
        resizable: !1,
        draggable: !1
    }), $("#QAS_Dialog").dialog({
        modal: !0,
        width: 800,
        autoOpen: !1,
        closeOnEscape: !1,
        resizable: !1,
        draggable: !1
    }), $(window).resize(function () {
        $("#QAS_Dialog").dialog("option", "position", "center")
    })
}

function EmailPhoneValidation(e, t, a) {
    this.records = [];
    var n, i, o = e,
        r = t,
        s = a,
        l = !1,
        d = function (e, t, a) {
            i.waitClose(), QAS_Variables.DISPLAY_ERRORS && alert(t + "\n Error with AJAX call. Check to make sure the service is configured and running correctly.")
        },
        c = function (e) {
            $.ajax({
                type: "POST",
                url: QAS_Variables.TD_PROXY_PATH,
                async: !0,
                data: n,
                dataType: "json",
                success: e,
                timeout: QAS_Variables.TIMEOUT_EMAILPHONE,
                error: d,
                cache: !1
            })
        },
        u = function () {
            $("select").css("visibility", ""), i.waitClose(), null != s ? a(o, r) : (null != o && o(), "" !== r && ($("#" + r).attr("onclick", ""), $("#" + r).parent("form").attr("onsubmit", ""), $("#" + r).click()))
        },
        m = function (e) {
            QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS < QAS_Variables.EMAIL_PHONE_NUM_SUBMITS || 0 === QAS_Variables.EMAIL_PHONE_NUM_SUBMITS ? (n = e, i.waitOpen(), QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS++, c(p)) : (QAS_TEMP_VARS.EMAIL_PHONE_POS++, S())
        },
        p = function (e) {
            var t = !1;
            i.waitClose(), void 0 !== e || null !== e ? (void 0 !== e.email && !1 === e.email.ok && (i.displayResult(e, m), i.display(), t = !0), void 0 !== e.phone && !1 === t && !1 === e.phone.ok && (i.displayResult(e, m), i.display(), t = !0)) : void 0 !== e.error && (t = !0, QAS_Variables.DISPLAY_ERRORS && alert(e.error + "\n Error with AJAX call. Check to make sure the service is configured and running correctly.")), !1 === t && (QAS_TEMP_VARS.EMAIL_PHONE_POS++, S())
        },
        h = function (e) {
            void 0 !== e.email && null !== e.email && !1 === e.email.ok && ($("label#" + QAS_Variables.EMAIL_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).show(), !0 === QAS_Variables.DISPLAY_CUSTOM_EMAIL_ERR && (message = EMAIL_ERR_MESSAGES[e.email.status_code], void 0 !== message && null !== message && $("label#" + QAS_Variables.EMAIL_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).text(message))), void 0 !== e.phone && null !== e.phone && !1 === e.phone.ok && ($("label#" + QAS_Variables.PHONE_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).show(), !0 === QAS_Variables.DISPLAY_CUSTOM_PHONE_ERR && (message = PHONE_ERR_MESSAGES[e.phone.status_code], void 0 !== message && null !== message && $("label#" + QAS_Variables.PHONE_ERR_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).text(message))), QAS_TEMP_VARS.EMAIL_PHONE_POS++, S()
        },
        S = function () {
            var e;
            if (e = !(QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS = 0) === QAS_Variables.EMAIL_PHONE_USEDIALOG ? p : h, !1 === function () {
                    var e, t, a = !1,
                        i = !1;
                    if (QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.EMAIL_FIELD_IDS.length && QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.PHONE_FIELD_IDS.length) return !1;
                    if (n = (n = "action=validate") + "&emaillev=" + QAS_Variables.EMAIL_VAL_LEVEL, "" !== (e = encodeURIComponent($("input#" + QAS_Variables.EMAIL_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val())) && (i = !0, n = n + "&email=" + e), "ALL" === QAS_Variables.PHONE_VALIDATE_COUNTRY[0] || QAS_Variables.COUNTRY_FIELD_IDS.length < QAS_TEMP_VARS.EMAIL_PHONE_POS) a = !0;
                    else
                        for (e = $("#" + QAS_Variables.COUNTRY_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val(), t = 0; t < QAS_Variables.PHONE_VALIDATE_COUNTRY.length; t++)
                            if (e === QAS_Variables.PHONE_VALIDATE_COUNTRY[t]) {
                                a = !0;
                                break
                            } return !0 === a && "" !== (e = encodeURIComponent($("input#" + QAS_Variables.PHONE_FIELD_IDS[QAS_TEMP_VARS.EMAIL_PHONE_POS]).val())) && (i = !0, n = n + "&phone=" + e), i
                }()) {
                if (QAS_TEMP_VARS.EMAIL_PHONE_POS++, QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.EMAIL_FIELD_IDS.length && QAS_TEMP_VARS.EMAIL_PHONE_POS >= QAS_Variables.PHONE_FIELD_IDS.length) return void u();
                S()
            } else QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS++, c(e)
        };
    this.process = function () {
            !1 === l ? u() : ($("select").css("visibility", "hidden"), i.waitOpen(), S())
        }, i = new EmailPhoneInterface(null), QAS_TEMP_VARS.NUM_EMAIL_PHONE_SUBMITS = 0, QAS_TEMP_VARS.EMAIL_PHONE_POS = 0,
        function () {
            var e;
            for (e = 0; e < QAS_Variables.EMAIL_FIELD_IDS.length; e++)
                if ("" !== $("input#" + QAS_Variables.EMAIL_FIELD_IDS[e]).val()) {
                    l = !0;
                    break
                } for (e = 0; !1 === l && e < QAS_Variables.PHONE_FIELD_IDS.length; e++)
                if ("" !== $("input#" + QAS_Variables.PHONE_FIELD_IDS[e]).val()) {
                    l = !0;
                    break
                }
        }()
}

function Main(e, t) {
    var i, n, o = this,
        a = e,
        r = t,
        s = new Address,
        l = s.getSearchStrings(),
        d = s.getSearchCountries(),
        c = s.getOriginalAddresses(),
        u = 0;
    this.process = function () {
        i = new Interface(o.returnEarly), n = new Clean(l[u], d[u], o.ajaxError), l[u] ? (i.waitOpen(), n.search(o.process2)) : (n.result = c[u], o.next())
    }, this.process2 = function () {
        i.waitClose();
        var e = new Business(o.next, n, c[u], i);
        QAS_Variables.ADDRESS_INTERACTION ? e.processResult() : e.noInteraction()
    }, this.next = function () {
        n.result.push(n.verifylevel), s.storeCleanedAddress(n.result), ++u < l.length ? o.process() : o.finish()
    }, this.finish = function () {
        s.returnCleanAddresses(), null !== a && a(), "" !== r && ($("#" + r).attr("onclick", ""), $("#" + r).parent("form").attr("onsubmit", ""), $("#" + r).click())
    }, this.returnEarly = function () {
        $("select").css("visibility", ""), s.returnCleanAddresses()
    }, this.ajaxError = function (e, t, a) {
        "timeout" === t ? t = n.verifylevel = "Timeout" : n.verifylevel = "Error", i.waitClose(), QAS_Variables.DISPLAY_ERRORS && alert(t + "\n Error with AJAX call. Check to make sure the service is configured and running correctly."), n.result = c[u], o.next()
    }
}

function QAS_Verify() {
    var e = QAS_Variables.PRE_ON_CLICK,
        t = QAS_Variables.POST_ON_CLICK,
        a = QAS_Variables.BUTTON_ID;
    return $(".error").hide(), null === e ? new EmailPhoneValidation(t, a, new Main(t, a).process).process() : e() && new EmailPhoneValidation(t, a, new Main(t, a).process).process(), !1
}

function QAS_Verify_Address() {
    var e = QAS_Variables.PRE_ON_CLICK,
        t = QAS_Variables.POST_ON_CLICK,
        a = QAS_Variables.BUTTON_ID;
    return null === e ? new Main(t, a).process() : e() && new Main(t, a).process(), !1
}

function QAS_Verify_EmailPhone() {
    var e = QAS_Variables.PRE_ON_CLICK,
        t = QAS_Variables.POST_ON_CLICK,
        a = QAS_Variables.BUTTON_ID;
    return $(".error").hide(), null === e ? new EmailPhoneValidation(t, a).process() : e() && new EmailPhoneValidation(t, a).process(), !1
}
window.QAS_Verify = QAS_Verify, window.QAS_Verify_Address = QAS_Verify_Address, window.QAS_Verify_EmailPhone = QAS_Verify_EmailPhone;
var LOCAPI_CONFIG = {
        SUBMIT_PATH: "/includes/endpointLocApi.jsp",
        PROP_SUBMIT_PATH: "/includes/endpointLocApiProp.jsp",
        AFTER_VERIFY: function () {
            document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD) ? LOCAPI_PROP_DATA() : handleGetFreeQuote()
        },
        PROP_AFTER_VERIFY: function (e) {
            handleGetFreeQuote()
        },
        PROP_PROCESS_RESPONSE: function (e) {
            var t = [{
                    min: 0,
                    max: 3999,
                    value: "3999"
                }, {
                    min: 4e3,
                    max: 5999,
                    value: "5999"
                }, {
                    min: 6e3,
                    max: 7999,
                    value: "7999"
                }, {
                    min: 8e3,
                    max: 1e16,
                    value: "8000"
                }],
                a = [{
                    min: 0,
                    max: .5,
                    value: "0.5"
                }, {
                    min: .501,
                    max: 1,
                    value: "1.0"
                }, {
                    min: 1.001,
                    max: 1.5,
                    value: "1.5"
                }, {
                    min: 1.501,
                    max: 2,
                    value: "2.0"
                }, {
                    min: 2.001,
                    max: 2.5,
                    value: "2.5"
                }, {
                    min: 2.501,
                    max: 3,
                    value: "3.0"
                }, {
                    min: 3.001,
                    max: 3.5,
                    value: "3.5"
                }, {
                    min: 3.501,
                    max: 4,
                    value: "4.0"
                }, {
                    min: 4.001,
                    max: 4.5,
                    value: "4.5"
                }, {
                    min: 4.501,
                    max: 5,
                    value: "5.0"
                }, {
                    min: 5.001,
                    max: 5.5,
                    value: "5.5"
                }, {
                    min: 5.501,
                    max: 6,
                    value: "6.0"
                }, {
                    min: 6.001,
                    max: 6.5,
                    value: "6.5"
                }, {
                    min: 6.501,
                    max: 7,
                    value: "7.0"
                }, {
                    min: 7.001,
                    max: 7.5,
                    value: "7.5"
                }, {
                    min: 7.501,
                    max: 8,
                    value: "8.0"
                }, {
                    min: 8.001,
                    max: 8.5,
                    value: "8.5"
                }, {
                    min: 8.501,
                    max: 9,
                    value: "9.0"
                }, {
                    min: 9.001,
                    max: 1e16,
                    value: "9.5"
                }];
            if (e && e.svmSquareFootage && "" !== e.svmSquareFootage) {
                for (var i = 0; i < t.length; i++)
                    if (e.svmSquareFootage < t[i].max && e.svmSquareFootage >= t[i].min) {
                        $("#sqftSize").val(t[i].value);
                        break
                    }
            } else $("#sqftSize").val(t[0].value);
            if (e && e.svmLotSize && "" !== e.svmLotSize) {
                for (i = 0; i < a.length; i++)
                    if (e.svmLotSize < a[i].max && e.svmLotSize >= a[i].min) {
                        $("#lotSize").val(a[i].value);
                        break
                    }
            } else $("#lotSize").val(a[0].value);
            handleGetFreeQuote()
        },
        OPEN_MODAL: function () {
            _MATERIALIZE ? $("#locApiModal").modal("open") : $("#locApiModal").modal("show")
        },
        CLOSE_MODAL: function () {
            _MATERIALIZE ? $("#locApiModal").modal("close") : $("#locApiModal").modal("hide")
        },
        REMOVE_HIDDEN: function (e) {
            _MATERIALIZE ? e.classList.remove("hide") : e.classList.remove("hidden")
        },
        ADD_HIDDEN: function (e) {
            _MATERIALIZE ? e.classList.add("hide") : e.classList.add("hidden")
        },
        ADDRESS_ID_FIELD: "addressId_hidden",
        ADDRESS_FIELD: "address1",
        SUITE_APT_FIELD: "address2",
        CITY_FIELD: "city_hidden",
        STATE_FIELD: "state_hidden",
        POSTALCODE_FIELD: "zipCode",
        BRAND: "TMX.COM",
        POST_VERIFY_FIELD_MODS: {
            POSTALCODE_FIELD: function (e) {
                return e.replace(/-\d{4}/, "")
            }
        },
        SHOW_CORRECTIONS: !0,
        PROCESSING_BODY_ID: "locApiModalProcessingBody",
        EDIT_BODY_ID: "locApiModalModBody",
        MODIFY_ID: "locApiModify",
        USE_AS_ENTERED_ID: "locApiUseAsEntered",
        MAX_SUGGESTIONS: 3,
        MATCH_TAG: "p",
        PURE_JS_MODAL: !0
    },
    LOCAPI_VERIFYLEVELS = {
        InteractionRequired: {
            locWarning: "<b>We think that your address may be incorrect or incomplete.</b><br />To proceed, please choose one of the options below.",
            locPrompt: "We recommend:",
            locSubmit: "Use suggested address",
            inputBind: "NONE"
        },
        PremisesPartial: {
            locWarning: "<b>Sorry, we think your apartment/suite/unit is missing or wrong</b><br />To proceed, please enter your apartment/suite/unit or use your address as entered",
            locPrompt: "Confirm your Apartment/Suite/Unit number:",
            locSubmit: "Confirm number",
            inputBind: "SUITE_APT_FIELD"
        },
        StreetPartial: {
            locWarning: "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
            locPrompt: "Confirm your House/Building number:",
            locSubmit: "Confirm number",
            inputBind: "ADDRESS_FIELD"
        },
        DPVPartial: {
            locWarning: "<b>Sorry, we do not recognize your house or building number.</b><br />To proceed, please check and choose from one of the options below.",
            locPrompt: "Confirm your House/Building number:",
            locSubmit: "Confirm number",
            inputBind: "ADDRESS_FIELD"
        },
        AptAppend: {
            locWarning: "<b>Sorry, we think your apartment/suite/unit may be missing.</b><br />To proceed, please check and choose from one of the options below.",
            locPrompt: "Confirm Apt/Ste:",
            locSubmit: "Continue",
            inputBind: "SUITE_APT_FIELD"
        },
        Multiple: {
            locWarning: "<b>We found more than one match for your address.</b><br />To proceed, please choose one of the options below.",
            locPrompt: "Our suggested matches:",
            locSubmit: "NONE",
            inputBind: "NONE"
        },
        None: {
            locWarning: "<b>Sorry, we could not find a match for your address.</b><br />To proceed, please choose one of the options below.",
            inputBind: "NONE",
            locSubmit: "Edit Address"
        },
        UseAsEntered: {
            locWarning: "You Entered:",
            locSubmit: "Use Address As Entered",
            locPrompt: "Edit"
        },
        processing: "Please wait, your details are being verified"
    };

function LOCAPI_VERIFY() {
    LOCAPI_PROCESSING_MODAL();
    return VERIFY_REPONSE(LOCAPI_MARSHALL_DATA()), !1
}

function LOCAPI_PROCESSING_MODAL() {
    LOCAPI_CONFIG.PURE_JS_MODAL && LOCAPI_CONFIG.OPEN_MODAL(), document.querySelectorAll("#" + LOCAPI_CONFIG.PROCESSING_BODY_ID + " .processing")[0].innerHTML = LOCAPI_VERIFYLEVELS.processing, LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById(LOCAPI_CONFIG.EDIT_BODY_ID)), LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById(LOCAPI_CONFIG.PROCESSING_BODY_ID)), LOCAPI_CONFIG.PURE_JS_MODAL || LOCAPI_CONFIG.OPEN_MODAL()
}

function LOCAPI_MARSHALL_DATA() {
    var e = "";
    e = LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD ? document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value : document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value + " " + document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value;
    var t = document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value,
        a = document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value,
        i = document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value,
        n = "?brand=" + LOCAPI_CONFIG.BRAND;
    return e && (n += "&addressLine=" + e), t && (n += "&city=" + t), a && (n += "&state=" + a), i && (n += "&postalCode=" + i), n += "&country=USA"
}

function VERIFY_REPONSE(e) {
    var t = new XMLHttpRequest;
    t.open("GET", LOCAPI_CONFIG.SUBMIT_PATH + e, !0), t.timeout = 4e4, t.send(JSON.stringify(e)), t.onreadystatechange = function () {
        if (4 === t.readyState) {
            var e = null;
            try {
                e = JSON.parse(t.responseText)
            } catch (e) {}
            LOCAPI_PROCESS_RESPONSE(e)
        }
    }
}

function LOCAPI_PROCESS_RESPONSE(e) {
    var t = "Unresponsive";
    if (null != e && void 0 !== e.data && void 0 !== e.data[0] && (t = e.data[0].verifyLevel), null != e && e.hasOwnProperty("errors") && e.errors.length)
        for (i = 0; i < e.errors.length; i++) {
            if ("Severe" == e.errors[i].severity) {
                t = "PremisesPartial";
                break
            }
            "Critical" == e.errors[i].severity && (t = "None")
        }
    null == t || "Verified" == t || "Unresponsive" == t || "Unverified" == t || "InteractionRequired" == t && !LOCAPI_CONFIG.SHOW_CORRECTIONS ? LOCAPI_PUSH_RESPONSE(e) : LOCAPI_GENERATE_MODALS(e, t)
}

function LOCAPI_PUSH_RESPONSE(e) {
    if (null != e && void 0 !== e.data && void 0 !== e.data[0] && null != e.data[0].verifyLevel) LOCAPI_PUSH_DATA(e.data[0]);
    LOCAPI_CONFIG.AFTER_VERIFY(e), LOCAPI_CONFIG.CLOSE_MODAL()
}

function LOCAPI_PUSH_DATA(e) {
    for (var t in LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD ? document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value = e.address1 : (document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value = e.address1, e.address2, document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value = e.address2), document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value = e.city, document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value = e.state, document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value = e.postalCode, document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD) && (document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD).value = e.ID), LOCAPI_CONFIG.POST_VERIFY_FIELD_MODS)
        if (document.getElementById(LOCAPI_CONFIG[t])) {
            var a = document.getElementById(LOCAPI_CONFIG[t]).value;
            document.getElementById(LOCAPI_CONFIG[t]).value = LOCAPI_CONFIG.POST_VERIFY_FIELD_MODS[t](a)
        } return !0
}

function LOCAPI_GENERATE_MODALS(e, t) {
    var a = t || "None",
        n = "";
    for (var o in LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD ? n += document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value : n += document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value + " " + document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value, n += " <br/> " + document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value, n += ", " + document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value, n += " " + document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value, document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " .locUserEntered")[0].innerHTML = n, LOCAPI_VERIFYLEVELS.UseAsEntered) document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " ." + o)[0] && (document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " ." + o)[0].innerHTML = LOCAPI_VERIFYLEVELS.UseAsEntered[o]);
    for (var o in LOCAPI_VERIFYLEVELS[a]) document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + o)[0] && ("NONE" != LOCAPI_VERIFYLEVELS[a][o] ? (document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + o)[0].innerHTML = LOCAPI_VERIFYLEVELS[a][o], LOCAPI_CONFIG.REMOVE_HIDDEN(document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + o)[0])) : (document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + o)[0].innerHTML = "", LOCAPI_CONFIG.ADD_HIDDEN(document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + o)[0]))), "inputBind" == o && "NONE" != LOCAPI_VERIFYLEVELS[a][o] ? (document.getElementById("locApiInput").setAttribute("data-locbind", LOCAPI_VERIFYLEVELS[a][o]), document.getElementById("locApiInput").value = document.getElementById(LOCAPI_CONFIG[LOCAPI_VERIFYLEVELS[a][o]]).value, LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locApiInput"))) : (document.getElementById("locApiInput").setAttribute("data-locbind", ""), LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locApiInput")));
    if (LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locSuggested")), document.getElementById("locSuggested").setAttribute("data-locbind", ""), document.getElementById("locSuggested").innerHTML = "", LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locMatchList")), document.getElementById("locMatchList").setAttribute("data-locbind", ""), document.getElementById("locMatchList").innerHTML = "", "InteractionRequired" == a) {
        var r = "";
        r += e.data[0].address1, "" != e.data[0].address2 && (r += " " + e.data[0].address2), r += " <br/> " + e.data[0].city, r += ", " + e.data[0].state, r += " " + e.data[0].postalCode, document.getElementById("locSuggested").setAttribute("data-locbind", JSON.stringify(e.data[0])), document.getElementById("locSuggested").innerHTML = r, LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locSuggested"))
    }
    if ("Multiple" == a) {
        for (i = 0; i < e.data.length; i++)
            if (i < LOCAPI_CONFIG.MAX_SUGGESTIONS) {
                document.getElementById("locMatchList").innerHTML += "<" + LOCAPI_CONFIG.MATCH_TAG + "><a href='#' onclick='LOCAPI_USE_THIS(this)'>" + e.data[i].fullAddress + "</a></" + LOCAPI_CONFIG.MATCH_TAG + ">"
            } LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locMatchList"))
    }
    LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById(LOCAPI_CONFIG.PROCESSING_BODY_ID)), LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById(LOCAPI_CONFIG.EDIT_BODY_ID))
}

function LOCAPI_REVERIFY() {
    var e = !1;
    if ("" != document.getElementById("locApiInput").getAttribute("data-locbind") && (document.getElementById(LOCAPI_CONFIG[document.getElementById("locApiInput").getAttribute("data-locbind")]).value = document.getElementById("locApiInput").value, e = !0), "" != document.getElementById("locSuggested").getAttribute("data-locbind")) {
        var t = document.getElementById("locSuggested").getAttribute("data-locbind");
        LOCAPI_PUSH_DATA(JSON.parse(t));
        e = !0
    }
    e ? LOCAPI_VERIFY() : LOCAPI_CONFIG.CLOSE_MODAL()
}

function LOCAPI_USE_THIS(e) {
    var t = e.innerHTML,
        a = /((.)+(, (.)+)([A-Z]{2} )((\d){5}-(\d){4}))/g.exec(t),
        i = {};
    i.postalCode = a[6].trim(), i.state = a[5].trim(), i.city = a[3].substring(2).trim(), i.address1 = a[0].split(a[3])[0], i.address2 = "";
    LOCAPI_PUSH_DATA(i);
    LOCAPI_VERIFY()
}

function LOCAPI_BYPASS() {
    LOCAPI_CONFIG.CLOSE_MODAL(), LOCAPI_CONFIG.AFTER_VERIFY("")
}

function isPropertyInformationEnabled() {
    var e = !0;
    return null !== document.getElementById("propertyInformationOff_hidden") && (e = !1), e
}

function LOCAPI_PROP_DATA() {
    var e = isPropertyInformationEnabled();
    if (console.log(e), document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD) && e) {
        var t = document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD).value;
        if (t.replace("0", "").replace("-", "").trim()) {
            var a = "?brand=" + LOCAPI_CONFIG.BRAND;
            a += "&addressId=" + t;
            var i = new XMLHttpRequest;
            i.open("GET", LOCAPI_CONFIG.PROP_SUBMIT_PATH + a, !0), i.timeout = 4e4, i.send(JSON.stringify(a)), i.onreadystatechange = function () {
                if (4 === i.readyState) {
                    var e = null;
                    try {
                        e = JSON.parse(i.responseText)
                    } catch (e) {}
                    LOCAPI_CONFIG.PROP_PROCESS_RESPONSE(e)
                }
            }
        } else LOCAPI_CONFIG.PROP_AFTER_VERIFY()
    } else LOCAPI_CONFIG.PROP_AFTER_VERIFY()
}

function heightNormalization(e, t) {
    var a, i = $(e + " " + t),
        n = [];
    if (i.length) {
        function o() {
            0,
            n.length = 0,
            i.each(function () {
                $(this).css("min-height", "0")
            }),
            i.each(function () {
                n.push($(this).innerHeight())
            }),
            a = Math.max.apply(null, n),
            i.each(function () {
                $(this).css("min-height", a + "px")
            })
        }
        o(), $(window).on("load", function () {
            o()
        }), $(window).on("resize", function () {
            o()
        }), $(window).on("orientationchange", function () {
            o()
        })
    }
}

function submitReferral() {
    $("#referalForm").ajaxForm({
        datatype: "json",
        success: function (e) {
            e.error ? ($("#processing-modal").modal("hide"), $("#referral-sorry-modal").modal("show")) : ($("#referralCode, #referral-error").empty(), $("#referralCode").append(e.referralCode), $("#processing-modal").modal("hide"), $("#referral-thank-you-modal").modal("show"))
        },
        error: function (e) {
            $("#processing-modal").modal("hide"), $("#referral-sorry-modal").modal("show")
        }
    }), $("#referalForm")[0].checkValidity() && ($("#processing-modal").modal("show"), $("#referalForm").submit())
}

function populateCityAndState(e) {
    _ENABLE_CITY_STATE_TOGGLE ? showCityAndState() : isNaN(e) || 5 != e.length || (_ENABLE_SMARTY_ZIP_TOGGLE ? smartyStreetsGetCityAndState(e) : ziptasticGetCityAndState(e))
}

function ziptasticGetCityAndState(e) {
    var t = $.getJSON("https://zip.getziptastic.com/v2/US/" + e);
    t.done(function (e) {
        $("#city_hidden, .city_hidden").val(e.city), $("#state_hidden, .state_hidden").val(e.state_short)
    }), t.fail(function () {
        showCityAndState()
    })
}

function smartyStreetsGetCityAndState(e) {
    var t = $.getJSON("/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=" + e);
    t.done(function (e) {
        "true" == e.data.validResponse ? ($("#city_hidden, .city_hidden").val(e.data.city), $("#state_hidden, .state_hidden").val(e.data.state)) : showCityAndState()
    }), t.fail(function () {
        showCityAndState()
    })
}

function showCityAndState() {
    $("#city_hidden[required], .city_hidden[required]").parents(".hiddenInputs").removeClass("hidden hide"), $("#state_hidden[required], .state_hidden[required]").parents(".hiddenInputs").removeClass("hidden hide"), $(".city-state-block").show(), $(".city-state-block #city_hidden").focus()
}

function removeLightBox(e) {
    e.addClass("hidden"), e.addClass("dismissed")
}

function showLightBox(e) {
    e.hasClass("dismissed") || (e.removeClass("hide"), e.removeClass("hidden"))
}

function scrollToLegalFooter() {
    $("html, body").animate({
        scrollTop: $(document).height()
    }, "slow")
}
$(document).ready(function () {
    $("#Consent").click(function () {
        $("phoneconsent").val($(this).val())
    }), $("#phoneNumber").mask("999-999-9999"), $("#LandingID").val(window.location)
}), $(document).ready(function () {
    $(".animPlaceholder").each(function () {
        var e = $(this).parent().height();
        $(this).height(e)
    });
    try {
        $('[data-toggle="tooltip"]').tooltip()
    } catch (e) {}

    function t(e) {
        return valid = !0, e.each(function () {
            a($(this)) || (valid = !1)
        }), !1 === valid && procEvent(e.first().closest("form").attr("id") + " User Error"), valid
    }

    function a(e) {
        if (!e.prop("disabled"))
            if (e.prop("pattern") && !e.val().match(e.prop("pattern"))) {
                if (e.prop("required")) return e.addClass("invalid"), !1;
                if ("" != e.val()) return e.addClass("invalid"), !1
            } else if ("radio" === e.attr("type")) {
            if (!$("input[name=" + e.attr("name") + "]:checked").val()) return e.addClass("invalid"), !1
        } else if ("checkbox" === e.attr("type")) {
            if (!e.prop("checked")) return e.addClass("invalid"), !1
        } else if (e.is("select")) {
            if ($("#" + e.attr("id") + " option:selected").prop("disabled")) return e.addClass("invalid"), !1
        } else {
            if (e.is("textarea") && !e.val()) return e.addClass("invalid"), !1;
            e.removeClass("invalid")
        }
        return !0
    }

    function n(e) {
        e[0].reset()
    }
    heightNormalization("#quoteCarousel", ".item"), heightNormalization(".normalizedCarousel", ".item"), document.oncontextmenu = function (e) {
            if (e = e || window.event, /^img$/i.test((e.target || e.srcElement).nodeName)) return !1
        }, _LESS_THAN_IE9 && location.pathname.indexOf("browser-upgrade-notification") < 0 && window.location.assign("/browser-upgrade-notification"), $("#pestChoice").length && $("#pestChoice").val($("#pest").text()), "" != $("#pest").text() && $("#pestDetails").html(function () {
            return $(this).html().replace(new RegExp("Pests", "ig"), $("#pest").text().charAt(0) + $("#pest").text().substr(1).toLowerCase())
        }), $("#page-1").click(function () {
            t($('#firstName,#lastName,#phone,#zipCode,input[name="Brand"]')) && $(this).closest("form").toggleClass("trigger")
        }), $("#phone,.phone,#phone-contact").keypress(function () {
            3 != $(this).val().length && 7 != $(this).val().length || $(this).val($(this).val() + "-")
        }), $("input, .radio-holder label, select, textarea").focus(function () {
            $(this).removeClass("invalid"), $("#" + $(this).attr("for")).siblings().removeClass("invalid")
        }), $("input,textarea").blur(function () {
            a($(this))
        }), $(".radio-holder label").click(function () {
            $("#InterestArea").removeProp("disabled"), $("#" + $(this).attr("for")).prop("checked", !0), $('#InterestArea option[value=""]').attr("selected", !0), $("#InterestArea option, #company").hide(), $("#InterestArea option, #company").prop("disabled", !0), $("." + $(this).attr("for")).show(), $("." + $(this).attr("for")).removeProp("disabled")
        }), $(".submit-button").click(function (e) {
            t($("#" + $(this).closest("form").attr("id") + " :input")) && $("#" + $(this).closest("form").attr("id")).submit()
        }), $(".form-cancel").click(function () {
            $(this).closest("form").removeClass("trigger"), procEvent($(this).closest("form").attr("id") + " Cancel"), n($("#" + $(this).closest("form").attr("id")))
        }), $("#landingId").val(window.location), $("#free-estimate-form, #free-estimate-form-mod,#contact-form,.modal-submit-form").submit(function () {
            var i = $(this),
                e = $(this).serializeArray(),
                t = $(this).attr("action");
            return $("#processing-modal").modal("show"), $.ajax({
                url: t,
                type: "POST",
                data: e,
                success: function (e, t, a) {
                    i.removeClass("trigger"), n(i), null != e.leadApiResponse && (ga("create", utag.sender[15].data.account[0], "auto"), ga("send", "event", "_setLeadId", "sucessfullySet", {
                        dimension4: e.leadApiResponse.leadApiId
                    })), $("#processing-modal").modal("hide"), $("#thank-you-modal").modal("show"), procEvent("Free Estimate Submit")
                },
                error: function (e, t, a) {
                    $("#processing-modal").modal("hide"), $("#try-again-modal").modal("show"), procEvent("Free Estimate Trans Error")
                }
            }), !1
        }), $("#LandingID").val(window.location),
        function t(a) {
            setTimeout(function () {
                var e = $("#consentText").text();
                "" != e && null != e ? ($(".consentText").empty(), $(".consentText").text(e)) : --a && t(a)
            }, 500)
        }(10), $(".buy-button").click(function (e) {
            var t = e.target.getBoundingClientRect();
            "undefined" != typeof utag_data && (utag_data.ga_event_label = t.top + ", " + t.left), procEvent("ecommerce", "click buy button")
        }), $(".arrow-up, .clickover-left, .clickover-right").click(function () {
            $(".arrow-up").toggleClass("rotate"), $(".sticky-footer").toggleClass("sf-shrunk"), $(".fs-info").toggleClass("hidden")
        }), $(".zipCodeSearch").click(function (e) {
            window.location.href = "/exterminators/search?q=" + $("#find-a-location-home").val()
        })
}), $(window).on("load", function () {
    $(".animPlaceholder").each(function () {
        var e = $(this).parent().height();
        $(this).height(e)
    })
}), $("#pestSelect").change(function () {
    $("#InterestArea").val($("#pestSelect").val())
}), $("#hoverCustomerNumb, #questionCustomernumber").hover(function () {
    $("#questionCustomernumber").show()
}, function () {
    $("#questionCustomernumber").hide()
}), $("#menu-toggle, .menu-toggle").click(function (e) {
    e.preventDefault(), $("#wrapper").toggleClass("toggled")
}), 0 < $("#user-city").text().length && $(".add-city").append(" IN " + $("#user-city").text().toUpperCase() + ", " + $("#user-state").text().toUpperCase()), $(".expand-icon").click(function (e) {
    e.preventDefault();
    var t = $(this).offset().left + $(this).outerWidth();
    if (e.clientX > t - 80 || $(this).hasClass("collapsed")) return $("#" + $(this).attr("data-target")).collapse("toggle"), !1;
    e.stopPropagation(), location.href = $(this).attr("href")
}), $(".main , #page-wrap").click(function (e) {
    $("#wrapper").hasClass("toggled") && (e.preventDefault(), $("#wrapper").toggleClass("toggled"))
}), $(".print").click(function () {
    window.print()
}), $("ul.highlight a").click(function () {
    $(".highlighted").removeClass("highlighted"), $(this).addClass("highlighted")
}), $("a").mousedown(function (e) {
    return 2 == e.which && $(this).attr("target", "_blank"), !0
}), $("#collapse-nav > li > a").each(function () {
    var e = $(this).attr("href").split("/")[1]; - 1 < window.location.href.indexOf(e) && ($(this).addClass("highlighted-nav-bar"), $("#" + e).addClass("orange"))
}), $("#pest-submit-button").click(function () {
    "" == $("#pest-submit-button #zipCode").val() ? procEvent("Home Page Info Form", $("#pest-submit-button #pestSelect").val(), "Zip Code Empty") : "" == $("#pest-submit-button #pestSelect").val() ? procEvent("Home Page Info Form", $("#pest-submit-button #zipCode").val(), "Pest Select Empty") : procEvent("Home Page Info Form", $("#pest-submit-button #pestSelect").val(), "No Error")
}), $(".pest-submit-button").click(function () {
    "" == $(".pest-submit-button #zipCode").val() ? procEvent("Stick Info Form", $(".pest-submit-button #pestSelect").val(), "Zip Code Empty") : "" == $(".pest-submit-button #pestSelect").val() ? procEvent("Stick Info Form", $(".pest-submit-button #zipCode").val(), "Pest Select Empty") : procEvent("Stick Info Form", $(".pest-submit-button #pestSelect").val(), "Submitted")
}), $(document).ready(function () {
    var t = new Date;
    if ($(document).click(function (e) {
            t = new Date
        }), setInterval(function () {
            1 < (new Date - t) / 1e3 / 60 && (0 == document.location.pathname.indexOf("/buyonline/") ? ($("#clickToCallBox").hide(), $("#clickToCallBoxMat").hide()) : $("#materialD").length ? showLightBox($("#clickToCallBoxMat")) : showLightBox($("#clickToCallBox")))
        }, 6e4), $(window).scroll(function () {
            var e = !0;
            975 <= $(document).width() ? 350 <= $(window).scrollTop() && (e = !1) : 500 <= $(document).width() ? 500 <= $(window).scrollTop() && (e = !1) : 600 <= $(window).scrollTop() && (e = !1), e || is_iOS() ? $("#stick-form").addClass("hidden") : $("#stick-form").removeClass("hidden")
        }), $("#zipCodeStick, .pestSelect").focus(function () {
            $(this).addClass("iphoneInputfix")
        }), $("#zipCodeStick, .pestSelect").blur(function () {
            $(this).removeClass("iphoneInputfix")
        }), $(window).trigger("scroll"), _FAB_FEATURE_TOGGLE) {
        function n() {
            $("#livechat-compact-container").hide()
        }
        $(document).on("InitialGreetingInitiated", function (e) {
            n();
            var t = $(".fixed-action-btn.click-to-toggle"),
                a = $("li.chat-bubble"),
                i = "Hello, how can we help you?";
            a.children("span.mobile-fab-tip").html(i), a.children("a").prop("data-tooltip", i), t.hasClass("active") || t.children("a").trigger("click.fabClickToggle")
        }), $(document).on("ShowFABChatButton", function (e) {
            $("li.chat-bubble").show()
        }), $(document).on("HideFABChatButton", function (e) {
            $("li.chat-bubble").hide()
        }), $(document).on("HideOfficialLiveChatBubble", function (e) {
            n()
        }), $(document).on("click", ".fixed-action-btn.click-to-toggle ul a", function (e) {
            $(".fixed-action-btn.click-to-toggle > a").trigger("click.fabClickToggle");
            var t = $(this).parent();
            t.hasClass("call-bubble") ? procEvent("FAB Trigger", "click to call") : t.hasClass("chat-bubble") && procEvent("FAB Trigger", "click to chat")
        }), $(document).on("click", "li.chat-bubble > a", function (e) {
            e.preventDefault(), parent.LC_API.open_chat_window({
                source: "minimized"
            })
        }), $(document).on("click", ".fixed-action-btn.click-to-toggle > a", function (e) {
            $(this).parent().hasClass("active") && procEvent("FAB Trigger", "FAB was opened")
        })
    }
}), $(window).on("load", function () {
    $(window).trigger("scroll")
}), $(document).ready(function () {
    $(window).scroll(function () {
        if (975 <= $(document).width()) {
            var e = $("footer").length ? $("footer")[0].getBoundingClientRect().height : "0",
                t = $(".main").outerHeight(!0) ? $(".main").outerHeight(!0) : $("#page-wrap").outerHeight(!0);
            $(window).scrollTop() + $(window).height() >= t ? ($(".sticky-footer").css({
                position: "absolute",
                bottom: e + "px"
            }), $(".sticky-footer").removeClass("sf-shrunk"), $(".fs-info").removeClass("hidden"), $(".arrow-up").addClass("rotate")) : ($(".sticky-footer").css({
                position: "fixed",
                bottom: 0
            }), $(".sticky-footer").hasClass("sf-shrunk") && $(".arrow-up").removeClass("rotate"))
        } else $(".sticky-footer").removeAttr("style"), $(".sticky-footer").removeClass("sf-shrunk")
    }), $(window).trigger("scroll")
}), $(window).on("load", function () {
    $(window).trigger("scroll")
});
var googleMap, googleGeocoder, QAS_EMAIL_CONFIG = {
        SUBMIT_PATH: "/includes/email_atg_proxy.jsp",
        AFTER_VERIFY: function () {
            CompleteOrderSubmit()
        },
        OPEN_MODAL: function () {
            _MATERIALIZE ? $("#qasEmailModal").modal("open") : $("#qasEmailModal").modal("show")
        },
        CLOSE_MODAL: function () {
            _MATERIALIZE ? $("#qasEmailModal").modal("close") : $("#qasEmailModal").modal("hide")
        },
        ADD_HIDDEN: function (e) {
            _MATERIALIZE ? e.addClass("hide") : e.addClass("hidden")
        },
        REMOVE_HIDDEN: function (e) {
            _MATERIALIZE ? e.removeClass("hide") : e.removeClass("hidden")
        },
        EMAIL_FIELD: "email"
    },
    QAS_EMAIL_CERTAINTYLEVELS = {
        verified: {
            qasEmailPrompt: "We were able to verify your email address."
        },
        undeliverable: {
            qasEmailPrompt: "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
        },
        unreachable: {
            qasEmailPrompt: "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
        },
        illegitimate: {
            qasEmailPrompt: "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
        },
        disposable: {
            qasEmailPrompt: "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
        },
        unknown: {
            qasEmailPrompt: "We were unable to verify your email address as valid. Please check your email for mistakes and update them, or choose to continue with the currently entered email."
        }
    };

function QAS_EMAIL_VALIDATE() {
    QAS_EMAIL_CONFIG.OPEN_MODAL(), QAS_EMAIL_CONFIG.ADD_HIDDEN($("#qasEmailValidationBody")), QAS_EMAIL_CONFIG.ADD_HIDDEN($(".modal-footer")), QAS_EMAIL_CONFIG.REMOVE_HIDDEN($("#qasEmailProcessingBody")), QAS_EMAIL_SEND()
}

function QAS_EMAIL_SEND() {
    var e = "?qasemail=" + $("#" + QAS_EMAIL_CONFIG.EMAIL_FIELD).val(),
        t = QAS_EMAIL_CONFIG.SUBMIT_PATH + e,
        a = new XMLHttpRequest;
    a.open("GET", t, !0), a.timeout = 4e4, a.send(JSON.stringify(e)), a.onreadystatechange = function () {
        if (4 === a.readyState) {
            var e = null;
            try {
                e = JSON.parse(a.responseText)
            } catch (e) {
                console.log(e)
            }
            QAS_EMAIL_RESPONSE(e)
        }
    }
}

function QAS_EMAIL_RESPONSE(e) {
    var t = null;
    null == e || void 0 === e.certainty ? console.error("No or incorrect response") : t = e.certainty, null == e || null == t || "verified" == t ? QAS_EMAIL_PUSH(e) : QAS_EMAIL_MODAL(e)
}

function QAS_EMAIL_PUSH(e) {
    QAS_EMAIL_CONFIG.CLOSE_MODAL(), QAS_EMAIL_CONFIG.AFTER_VERIFY()
}

function QAS_EMAIL_MODAL(e) {
    for (var t in QAS_EMAIL_CERTAINTYLEVELS[e.certainty]) "NONE" !== QAS_EMAIL_CERTAINTYLEVELS[e.certainty][t] ? $("#" + t).text(QAS_EMAIL_CERTAINTYLEVELS[e.certainty][t]) : $("#" + t).text(""), $("#qasEmailUserEnteredLabel").text("Continue with: " + $("#" + QAS_EMAIL_CONFIG.EMAIL_FIELD).val()), $("#qasEmailModifyCheckLabel").text("Update to: ");
    QAS_EMAIL_CONFIG.ADD_HIDDEN($("#qasEmailProcessingBody")), QAS_EMAIL_CONFIG.REMOVE_HIDDEN($("#qasEmailValidationBody")), QAS_EMAIL_CONFIG.REMOVE_HIDDEN($(".modal-footer"))
}

function QAS_EMAIL_REVALIDATE() {
    var e = $("#qasEmailModifyInput");
    if (document.getElementById("qasEmailUserEnteredCheck").checked) e.prop("required", !1), QAS_EMAIL_CONFIG.CLOSE_MODAL(), QAS_EMAIL_PUSH();
    else {
        if (e.prop("required", !0), !e[0].checkValidity()) return e.addClass("invalid"), !1;
        document.getElementById(QAS_EMAIL_CONFIG.EMAIL_FIELD).value = e.val(), e.prop("required", !1), e.val(""), _MATERIALIZE && Materialize.updateTextFields(), $("#qasEmailUserEnteredCheck").trigger("click"), QAS_EMAIL_VALIDATE()
    }
}

function initMap() {
    console.log("Loaded map"), googleGeocoder = new google.maps.Geocoder, googleMap = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: 35,
            lng: -90
        },
        zoom: 15,
        styles: [{
            featureType: "administrative",
            elementType: "labels.text.fill",
            stylers: [{
                color: "#444444"
            }, {
                lightness: "60"
            }]
        }, {
            featureType: "landscape",
            elementType: "all",
            stylers: [{
                color: "#f2f2f2"
            }]
        }, {
            featureType: "poi",
            elementType: "all",
            stylers: [{
                visibility: "off"
            }]
        }, {
            featureType: "road",
            elementType: "all",
            stylers: [{
                saturation: -100
            }, {
                lightness: 45
            }]
        }, {
            featureType: "road.highway",
            elementType: "all",
            stylers: [{
                visibility: "simplified"
            }]
        }, {
            featureType: "road.arterial",
            elementType: "labels.icon",
            stylers: [{
                visibility: "off"
            }]
        }, {
            featureType: "transit",
            elementType: "all",
            stylers: [{
                visibility: "off"
            }]
        }, {
            featureType: "water",
            elementType: "all",
            stylers: [{
                color: "#46bcec"
            }, {
                visibility: "on"
            }]
        }]
    })
}
$(document).ready(function () {
        $("input[name='emailChoice']").change(function () {
            $("#qasEmailModifyCheck").is(":checked") ? $("#qasEmailModifyInput").prop("disabled", !1) : $("#qasEmailModifyInput").prop("disabled", !0)
        })
    }),
    function () {
        var e = new n,
            t = "tmx_city",
            a = "tmx_state",
            i = "tmx_zipcode";

        function n(e, t, a, i) {
            this.zipCode = e, this.city = t, this.state = a, this.coordinates = i
        }
        null != localStorage.getItem(t) && "" != localStorage.getItem(t) && (e.city = localStorage.getItem(t)), null != localStorage.getItem(a) && "" != localStorage.getItem(a) && (e.state = localStorage.getItem(a)), null != localStorage.getItem(i) && "" != localStorage.getItem(i) && (e.zipCode = localStorage.getItem(i), $("#txtZipCodeLocation").val(e.zipCode), $("#cbRememberLocation").prop("checked", !0)), $(document).ready(function () {
            $("#txtZipCodeLocation").on("change paste keyup", function () {}), $("#formFindLocations").submit(function (e) {
                if (5 <= $("#txtZipCodeLocation").val().length) return e.preventDefault(), window.location = "https://www.terminix.com/exterminators/search?q=" + $("#txtZipCodeLocation").val(), !1;
                console.log("invalid")
            })
        })
    }();


//function goBack() {
//    history.go(-1);
//}

$(document).ready(function () {
    $('#content-back').click(function (e) {
        history.go(-1);
    });
});

var _LESS_THAN_IE9 = false;

$(document).ready(function () {
    // Masks for inputs
    $('.mask-zip-short').mask('00000');
    $('.mask-zip').mask('00000-0000');
    $('.mask-phone').mask('(000) 000-0000');
    $('.mask-phone-dash').mask('000-000-0000');
    $('.mask-numeric').mask('0#');

    // Regex patterns for inputs
    $('.patt-email').attr('pattern', function (i, o) {
        return GetGlobalUIPattern('email')
    });
    $('.patt-phone').attr('pattern', function (i, o) {
        return GetGlobalUIPattern('phone')
    });


    if (_HTML5_POLYFILL) {
        // Shim to fix html5 forms and srcset on unsupported browsers
        webshim.activeLang('en');
        webshims.polyfill('forms');
        webshims.polyfill('picture');
        webshims.cfg.no$Switch = true;
    }


    // currently utag_data.cvo_sid coming from telium
    var MAX_ATTEMPT = 100;
    var initCount = 0;
    var cvoSIdInterval = setInterval(function () {
        initCount++;
        if (initCount < MAX_ATTEMPT) {
            var cvoSId = utag_data['cp.cvo_sid1'];
            console.log("cvoSId=" + cvoSId);
            if (cvoSId) {
                var $cvoSId = $("#input_cvoSId");
                if ($cvoSId.length) {
                    $("#input_cvoSId").val(cvoSId);
                }
                clearInterval(cvoSIdInterval);
            }
        } else {
            clearInterval(cvoSIdInterval);
            console.log("Exceeded max attempt " + MAX_ATTEMPT + " times. Quit now.");
        }
    }, 500);

});
