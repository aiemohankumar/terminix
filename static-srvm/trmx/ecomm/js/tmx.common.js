
ko.components.register('funnel-nav', {
    template:  `<ul>
    <li data-bind="css: {current: funnelModel.currentStep() == 'step_address'}">
        <a data-bind="click:redirectToUrl.bind(this,'step_address')">
            <span class="step-number">1</span>
            <span class="step-name hidden-xs">Address</span>
        </a>
    </li>
    
    <li data-bind="css: {current: funnelModel.currentStep() == 'step_products'}">
        <a data-bind="click:redirectToUrl.bind(this,'step_products')">
            <span class="step-number">2</span>
            <span class="step-name hidden-xs">Pricing</span>
        </a>
    </li>
    
    <li  data-bind="css: {current: funnelModel.currentStep() == 'step_schedule'}">
        <a data-bind="click:redirectToUrl.bind(this,'step_schedule')">
            <span class="step-number">3</span>
            <span class="step-name hidden-xs">Schedule</span>
        </a>
    </li>
    
    <li data-bind="css: {current: funnelModel.currentStep() == 'step_payment'}">
        <a data-bind="click:redirectToUrl.bind(this,'step_payment')">
            <span class="step-number">4</span>
            <span class="step-name hidden-xs" >Pay</span>
        </a>
    </li>
    <li  class="disabled">
        <a data-bind="click:redirectToUrl.bind(this,'step_confirmation')" >
            <span class="step-number">5</span>
            <span class="step-name hidden-xs" >Confirmation</span>
        </a>
    </li>
    </ul>`
});


var STEP_ADDRESS = self.STEP_ADDRESS = 'step_address';
var STEP_PRODUCTS = self.STEP_PRODUCTS = 'step_products';
var STEP_SCHEDULE = self.STEP_SCHEDULE = 'step_schedule';
var STEP_PAYMENT = self.STEP_PAYMENT = 'step_payment';
var STEP_CONFIRMATION = self.STEP_CONFIRMATION = 'step_confirmation';
// disabled
var tmx = tmx || {}

tmx.funnel = function(){

    
    this.funnelModel = {
        currentStep : ko.observable("step_address"),
        customer : {
            loggedIn: ko.observable(true),
            profileId: ko.observable("660507377"),
            leadIdentifier: ko.observable(null),
            partyId: ko.observable(null),
            salesCallId: ko.observable(null),
            customerNumber: ko.observable(null),
            firstName: ko.observable(null),
            lastName: ko.observable(null),
            serviceProperty: {
                address1 : ko.observable("3169  James Martin Circle"),
                address2 : ko.observable(null),
                city : ko.observable("Westerville"),
                state : ko.observable("OH"),
                zipCode : ko.observable("43081"),
                zip9Code : ko.observable(null),
                lotSize : ko.observable("0.5"),
                squareFootage: ko.observable(),
                addressVerified : ko.observable(true)
            },
            billingProperty : {
                address1 : ko.observable(null),
                address2 : ko.observable(null),
                city : ko.observable(null),
                state : ko.observable(null),
                zipCode : ko.observable(null),
                zip9Code : ko.observable(null),
                lotSize : ko.observable(null),
                squareFootage: ko.observable(null),
                addressVerified : ko.observable(false)
            },
            contact : {
                email : ko.observable("jeo11@gmail.com"),
                phone : ko.observable("8904729112"),
                contactConsent : ko.observable(null),
                emailVerified : ko.observable(null)
            },
            errorMsgList : ko.observable(null)
        },
        payment : {
            paymentMethod : ko.observable(null),
            bankAccountNumber : ko.observable(null),
            bankRoutingNumber : ko.observable(null),
            cardNumber : ko.observable(null),
            cardType : ko.observable(null),
            cardExpire : ko.observable(null),
            cvv : ko.observable(null)
        },
        
        productItemMap: ko.observable(null),
        couponCode: ko.observable(null),
        targetPest: ko.observable("GENPEST"),
        startUpCostTotal: ko.observable(null),
        serviceable: ko.observable(true),
        over8kProcessed: ko.observable(false),
        franchisePhone: ko.observable(null),
        mPaymentToken: ko.observable(null),
        addressId: ko.observable("08e93ff4-9dcb-4697-bbb4-1ba6dafd9a0e"),
        productCode: ko.observable(null),
        salesAgent: ko.observable("TMX.COM"),
        serviceSchedule: ko.observable(null),
        openSlotSelected: ko.observable(null),
        timeSlotSelectedKey: ko.observable(null),
        productIdCart: ko.observableArray([]),
        apEffDateYYYYMMDD: ko.observable(null),
        apEasyPay: ko.observable(null),
        AssignmentChanged: ko.observable(false),
        entrySource: {
          source: ko.observable(""),
          campaign: ko.observable(""),
          offerCode: ko.observable(""),
          mkwid: ko.observable(""),
          gclid: ko.observable(""),
          keyword: ko.observable(""),
          adType: ko.observable(""),
          content: ko.observable("")
        },
        interestArea: ko.observable("PEST CONTROL"),
        taxRate: ko.observable(null),
        sendInfoToGoogleAnalytics: ko.observable(null),
        busUnit: ko.observable(null),
        assignedTerritory: ko.observable(null),
        assignedTerritoryEmployee: ko.observable(null),
        orderId: ko.observable(null),
        tiered: ko.observable(false),
        showUpsell: ko.observable(false),
        actionList: ko.observableArray([]),
        dueDateCFR: ko.observable(null),
        liveChat: {
          visitorId: ko.observable(null),
          hasChatOccurred: ko.observable(false)
        },
        commercialAffiliate: ko.observable(null),
        errorMessages: ko.observableArray([]),
        informationalMessages: ko.observableArray([]),
        fieldValidationErrors: ko.observableArray([]),
        redirectRequest: ko.observable(false),
        redirectURL: ko.observable(null),
        availableProducts: ko.observableArray([])
    }

    this.redirectToUrl = function(tab){
        var url = ""
        // funnelData.currentStep(tab);
        switch(tab){
            case "step_address":
                url = "../buyonline/address.html";
                break;
            case "step_products":
                url =  "../buyonline/pricing.html";
                break;
            case "step_schedule": 
                url = "../buyonline/schedule.html";
                break;
            case "step_payment": 
                url = "../buyonline/payment.html";
                break;
            case "step_confirmation": 
                url = "../buyonline/confirmation.html";
                break;
            default: 
                url = "#";
                break;
        }
        window.sessionStorage.setItem("currentStep",tab);

        window.location.href = url;
    },

    this.setModal = function(action){
        
        switch (action) {
            case 'INVALID_EMAIL':
                new CommonModal({
                    title: 'Email address verification error',
                    body: '<p>Sorry, we were unable to verify your email address <strong>' + customer.contact.email() + '</strong>. If this is the correct email address, please continue as is to resubmit, otherwise you may edit your email address and try again.</p>',
                    confirm: true,
                    btnCancelText: 'Edit email',
                    btnConfirmText: 'Continue as is',
                    type: 'InvalidEmail'
                }, function() {
                    customer.contact.emailVerified(true);
                    SubmitStep();
                });
                break;

            case 'INVALID_ADDRESS':
                var addressline2, fullAddress, invalidType;

                if (!customer.serviceProperty.addressVerified()) {
                    addressline2 = customer.serviceProperty.address2() != null ? customer.serviceProperty.address2() : '';
                    fullAddress = customer.serviceProperty.address1() + ' ' + addressline2 + ' ' + customer.serviceProperty.zipCode();
                    invalidType = 'service';

                } else if (!customer.billingProperty.addressVerified()) {
                    addressline2 = customer.billingProperty.address2() != null ? customer.billingProperty.address2() : '';
                    fullAddress = customer.billingProperty.address1() + ' ' + addressline2 + ' ' + customer.billingProperty.zipCode();
                    invalidType = 'billing';
                }

                new CommonModal({
                    title : 'Address verification error',
                    body : '<p>Sorry, we were unable to verify your ' + invalidType + ' address <strong>'
                    + fullAddress + '</strong>. If this is the correct address, please continue as is to resubmit, otherwise you may edit your address and try again.</p>',
                    confirm : true,
                    btnCancelText : 'Edit address',
                    btnConfirmText : 'Continue as is'

                }, function() {
                    if (invalidType === 'service') {
                        customer.serviceProperty.addressVerified(true);
                    } else if (invalidType === 'billing') {
                        customer.billingProperty.addressVerified(true);
                    }

                    submitStep();
                });

                //actionList().pop();
                break;

            case 'INVALID_PHONE':
                break;
            // case 'DISABLE_SUBMIT_ORDER':
            //     if (self.model().currentStep() == STEP_PAYMENT) {
            //         $('.cart .card-footer').hide();
            //     } else if (self.model().currentStep() == STEP_SCHEDULE) {
            //         $('#submit-schedule-step').hide();
            //     }
            //     break;
        }
    },

    this.addErrorMessage = function(message){
        tmx.funnel.errorMessages.add(message);
    }

    this.removeAllErrorMessage = function(message) {
        tmx.funnel.errorMessages.removeAll();
    }

    return {
        addErrorMessage: addErrorMessage,
        removeAllErrorMessage: removeAllErrorMessage,
        funnelModel: funnelModel,
        redirectToUrl: redirectToUrl,
        setModal: setModal
    }
}();