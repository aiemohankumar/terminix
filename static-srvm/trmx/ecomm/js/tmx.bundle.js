function IsObject(e) {
    return "[object Object]" == Object.prototype.toString.call(e)
}

function IsArray(e) {
    return "[object Array]" == Object.prototype.toString.call(e)
}

function IsObjectEmpty(e) {
    for (var t in e) return !1;
    return !0
}

function GetWindowWidth() {
    return window.innerWidth
}

function ScrollTo(e, t) {
    $("html, body").animate({
        scrollTop: e
    }, t)
}

function ArrayContainsArray(t, e) {
    return !!e.length && e.every(function (e) {
        return -1 < t.indexOf(e)
    })
}

function IsStorageAvailable(e) {
    try {
        var t = window[e],
            o = "__storage_test__";
        return t.setItem(o, o), t.removeItem(o), !0
    } catch (e$0) {
        return e$0 instanceof DOMException && (22 === e$0.code || 1014 === e$0.code || "QuotaExceededError" === e$0.name || "NS_ERROR_DOM_QUOTA_REACHED" === e$0.name) && 0 !== t.length
    }
}
//$(document).ready(function () {
//    var t = 0,
//        o = setInterval(function () {
//            if (++t < 100) {
//                var e = utag_data["cp.cvo_sid1"];
//                if (e) $("#input_cvoSId").length && $("#input_cvoSId").val(e), clearInterval(o)
//            } else clearInterval(o), console.log("Exceeded max attempt 100 times. Quit now.")
//        }, 500)
//});
var caniuse = function (e) {
    return e.history = window.history && "function" == typeof history.pushState && "function" == typeof history.replaceState && "function" == typeof history.back, e.sessionStorage = IsStorageAvailable("sessionStorage"), e.localStorage = IsStorageAvailable("localStorage"), e
}({});

function GetCookie(e) {
    if (!e) return console.log("[GetCookie] Name is required to get the value of a cookie."), null;
    e += "=";
    for (var t = document.cookie.split(";"), o = 0; o < t.length; o++) {
        for (var n = t[o];
            " " == n.charAt(0);) n = n.substring(1, n.length);
        if (0 == n.indexOf(e)) return n.substring(e.length, n.length)
    }
    return null
}

function SetCookie(e, t, o) {
    if (!e || !t) return console.log("[SetCookie] Name and value are both required to set a cookie."), !1;
    if (o) {
        var n = new Date;
        n.setTime(n.getTime() + 24 * o * 60 * 60 * 1E3);
        var r = "; expires=" + n.toUTCString()
    } else r = "";
    return document.cookie = e + "=" + t + r + "; path=/", !0
}

function OpenModal(e) {
    !e || "string" != typeof e || -1 < e.indexOf("#") || (_MATERIALIZE ? $("#" + e).modal("open") : $("#" + e).modal("show"))
}

function CloseModal(e) {
    !e || "string" != typeof e || -1 < e.indexOf("#") || (_MATERIALIZE ? $("#" + e).modal("close") : $("#" + e).modal("hide"))
}

function is_iOS() {
    for (var e = ["iPad Simulator", "iPhone Simulator", "iPod Simulator", "iPad", "iPhone", "iPod"]; e.length;)
        if (navigator.platform === e.pop()) return !0;
    return !1
}

function GetURLParameter(e) {
    if (!e) throw "[GetURLParameter] param is required to pull a parameter.";
    var t, o, n = null,
        r = decodeURIComponent(window.location.search.substring(1)).split("&");
    for (o = 0; o < r.length; o++)
        if ((t = r[o].split("="))[0] === e) {
            n = void 0 === t[1] ? null : t[1];
            break
        }
    return n
}

function GetTargetPest() {
    var e = null,
        t = GetURLParameter("targetPest");
    return t ? e = t : caniuse.sessionStorage && sessionStorage.getItem("targetPest") && (e = sessionStorage.getItem("targetPest").toUpperCase()), e
}

function IsValidEmailAddress(e) {
    return /^\w+([\.%#$&*+_-]\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(e = e || "")
}

function IsValidRoutingNumber(e) {
    return !(!e || 9 !== e.length || !$.isNumeric(e)) && (3 * (parseInt(e.charAt(0), 10) + parseInt(e.charAt(3), 10) + parseInt(e.charAt(6), 10)) + 7 * (parseInt(e.charAt(1), 10) + parseInt(e.charAt(4), 10) + parseInt(e.charAt(7), 10)) + 1 * (parseInt(e.charAt(2), 10) + parseInt(e.charAt(5), 10) + parseInt(e.charAt(8), 10))) % 10 == 0
}

function GetGlobalUIPattern(e) {
    var t = "";
    return e && (t = _GLOBAL_UI_SETTINGS ? _GLOBAL_UI_SETTINGS.inputRegexPattern[e.toLowerCase()] : ""), t
}

//function FireGAEvent(e, t, o, n) {
//    if (o = o || "", n = n || "", e)
//        if (t) {
//            console.log("[FireGAEvent] " + e + " :: " + t + " :: " + o);
//            try {
//                var r = {
//                    ga_event_category: e,
//                    ga_event_action: t,
//                    ga_event_label: o
//                };
//                utag.link(r)
//            } catch (e$1) {
//                console.log(e$1)
//            }
//        } else console.log("[FireGAEvent] Event Action is required to fire a GA event.");
//    else console.log("[FireGAEvent] Event Category is required to fire a GA event.")
//}

function GetEndecaContent(n, t) {
    var r = function (e) {
        t(e)
    };
    if (!n) throw "[GetEndecaContent] Data is required.";
    if (!n.productCode || !n.templateCode || !n.folder) return console.log("[GetEndecaContent] Product Code, Template Code, and folder are all required in data."), void r({});
    var e = "/includes/common/getEndecaContent.jsp?productCode=" + n.productCode + "&templateCode=" + n.templateCode + "&folder=" + n.folder;
    $.ajax({
        type: "GET",
        url: e,
        timeout: 12E5,
        complete: function (e) {
            var t;
            try {
                t = JSON.parse(e.responseText)
            } catch (e$2) {
                t = {
                    error: "An error has occurred."
                }, console.log("Error from server request: " + e$2.message)
            }
            var o = {
                templateCode: n.templateCode,
                content: t
            };
            r(o)
        }
    })
}

function CommonModal(e, t, o) {
    var n = e.title || "",
        r = e.body || "",
        a = e.confirm || !1,
        i = e.btnCancelText || "Cancel",
        s = e.btnConfirmText || "Ok",
        l = $("#commonModal"),
        c = {},
        d = l.find(".cta.primary"),
        u = l.find(".cta.secondary, button.close");
    if (!l) throw "[CommonModal] No common modal id found on the page.";
    l.find(".modal-title").text(n), l.find(".modal-body").html(r), d.text(s), l.find(".cta.secondary").text(i), c.show = !0, a ? (c.backdrop = "static", d.show()) : d.hide(), l.on("show.bs.modal", function (e) {
        if (a) {
            if (!t) throw "[CommonModal] Callback method required for confirmation modal.";
            d.off().on("click", function (e) {
                return t()
            }), u.off().on("click", function (e) {
                if ("function" == typeof o) return o()
            })
        }
    }), l.on("hide.bs.modal", function (e) {
        null
    }), l.modal(c)
}

function ThrowToast(e) {}
var _CreditCardTypes = [{
    name: "Visa",
    regex: /^4/,
    value: "visa"
}, {
    name: "Master Card",
    regex: /^(5[1-5]|2(2(2[1-9][0-9]{2}|[3-9])|[3-6]|7([01]|20[0-9]{2})))/,
    value: "masterCard"
}, {
    name: "American Express",
    regex: /^3[47]/,
    value: "americanExpress"
}, {
    name: "Discover",
    regex: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5])|64[4-9]|65)/,
    value: "discover"
}];

function GetCreditCardType(e) {
    if (!e || "string" != typeof e) return null;
    for (var t = 0; t < _CreditCardTypes.length; t++)
        if (_CreditCardTypes[t].regex.test(e)) {
            var o = _CreditCardTypes[t];
            return {
                name: o.name,
                value: o.value
            }
        }
    return null
}

function ValidateCreditCardNumber(e, t) {
    if (!e || "string" != typeof e) throw "[ValidateCreditCardNumber] String parameter cardNumber is required.";
    if (!t || "string" != typeof t) throw "[ValidateCreditCardNumber] String parameter cardType is required.";
    for (var o = 0; o < _CreditCardTypes.length; o++)
        if (_CreditCardTypes[o].value.toLowerCase() === t.toLowerCase() && _CreditCardTypes[o].regex.test(e)) return !0;
    return !1
}
RegExp.escape = function (e) {
    return e.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
}, String.prototype.replaceAll = function (e, t) {
    return this.replace(new RegExp(RegExp.escape(e), "g"), t)
};
var googleMap, googleGeocoder, _SCHEDULE_SLOTS, _BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE = [];

function initMap() {
    console.log("Loaded map"), googleGeocoder = new google.maps.Geocoder, googleMap = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: 35,
            lng: -90
        },
        zoom: 15,
        styles: [{
                featureType: "administrative",
                elementType: "labels.text.fill",
                stylers: [{
                    color: "#444444"
                }, {
                    lightness: "60"
                }]
            }, {
                featureType: "landscape",
                elementType: "all",
                stylers: [{
                    color: "#f2f2f2"
                }]
            }, {
                featureType: "poi",
                elementType: "all",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "road",
                elementType: "all",
                stylers: [{
                    saturation: -100
                }, {
                    lightness: 45
                }]
            },
            {
                featureType: "road.highway",
                elementType: "all",
                stylers: [{
                    visibility: "simplified"
                }]
            }, {
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "transit",
                elementType: "all",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "water",
                elementType: "all",
                stylers: [{
                    color: "#46bcec"
                }, {
                    visibility: "on"
                }]
            }]
    })
}
$(document).ready(function () {
        window.nav = new function () {
            var t = this,
                e = $("li.more"),
                o = $("li.shrink-1"),
                n = $(".subnav .shrink-1"),
                r = $("li.shrink-2"),
                a = $(".subnav .shrink-2"),
                i = $(".header-container.mobile"),
                s = $(".header-container.desktop");
            return t.states = [0, 1, 2, 3], t.currentState = t.states[2], t.SetNav = function () {
                var e = GetWindowWidth();
                t.currentState = e < 992 ? t.states[3] : e < 1380 ? t.states[2] : e < 1575 ? t.states[1] : t.states[0], t.UpdateNav()
            }, t.UpdateNav = function () {
                switch (t.currentState) {
                    case 0:
                        i.hide(), s.show(), e.hide(),
                            o.show(), n.hide(), r.show(), a.hide();
                        break;
                    case 1:
                        i.hide(), s.show(), e.show(), o.hide(), n.show(), r.show(), a.hide();
                        break;
                    case 3:
                        i.show(), s.hide();
                        break;
                    case 2:
                    default:
                        i.hide(), s.show(), e.show(), o.hide(), n.show(), r.hide(), a.show()
                }
            }, t
        }, nav.SetNav(), $(window).resize(nav.SetNav), $(".mobile .nav-icon").on("click", function () {
            $(".mobile .nav-icon a").parents(".header-container.mobile").toggleClass("open")
        });
        var o = $(".mobile .mainnav > li");
        o.on("click", function (e) {
            var t = $(this);
            t.hasClass("active") || o.removeClass("active"),
                t.toggleClass("active")
        }), $(".scroll-to-top").on("click", function (e) {
            ScrollTo(0, 500)
        });
        var e = "1.877.837.6464";
        if (_BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE) {
            var t = GetCookie("branch_phone_number");
            e = t || e
        }
        $(".phone-number-container > span").text(e), $(".call-phone > a").text(e).attr("href", "tel:" + e.replace(/\./g, "")), $(".phone-link > a").text(e).attr("href", "tel:" + e.replace(/\./g, "")), $(".more-less-toggle").on("click", function (e) {
            $(".more-less-content").collapse("toggle"), $(".more-less-toggle").text(function (e,
                t) {
                return "less" === t ? "more" : "less"
            })
        });
        var n = (new Date).getFullYear();
        $(".current-year").text(n);
        var r = document.createElement("script");
        if (r.type = "application/ld+json", r.text = JSON.stringify({
                "@context": "http://schema.org/",
                "@type": "WebPage",
                author: "https://www.terminix.com/",
                datePublished: (new Date).toISOString(),
                headline: document.querySelector(".jsonld-webpage-headline").text,
                publisher: "https://www.terminix.com/",
                description: document.querySelector(".jsonld-webpage-description").getAttribute("content")
            }),
            document.querySelector("head").appendChild(r), caniuse.sessionStorage) {
            var a = {
                "/additional-pest-solutions/attic-insulation/": "ATTIC INSULATION",
                "/bed-bug-control/": "BED BUGS",
                "/additional-pest-solutions/crawl-space-services/": "CRAWL SPACE",
                "/pest-control/": "PEST CONTROL",
                "/pest-control/ants/": "ANTS",
                "/pest-control/cockroaches/": "COCKROACHES",
                "/pest-control/crickets/": "CRICKETS",
                "/pest-control/fleas/": "FLEAS",
                "/pest-control/mosquitoes/": "MOSQUITOES",
                "/pest-control/mosquitoes/atsb/": "MOSQUITOES",
                "/pest-control/moths/": "CLOTHES MOTHS",
                "/pest-control/rodents/": "RODENTS",
                "/pest-control/scorpions/": "SCORPIONS",
                "/pest-control/silverfish/": "SILVERFISH",
                "/pest-control/spiders/": "SPIDERS",
                "/pest-control/ticks/": "TICKS",
                "/pest-control/wasps/": "PAPER WASPS",
                "/pest-control/wildlife/": "WILDLIFE",
                "/termite-control/": "TERMITES",
                "/additional-pest-solutions/": "OTHER SERVICES"
            };
            $("body").on("click", 'a[href^="/free-inspection"], a[href^="/buyonline"]', function (e) {
                var t = window.location.pathname;
                t.length && "/" != t.charAt(t.length - 1) && (t += "/"), (t = t.toLowerCase()) in
                    a ? sessionStorage.setItem("interestArea", a[t]) : sessionStorage.removeItem("interestArea")
            })
        }
        $(document).on("ShowMobileChatLink", function (e) {
            $(".header-container.mobile .chat-link").show()
        }), $(document).on("ShowGenericChatLink", function (e) {
            $(".chat-link").show()
        }), $(document).on("HideGenericChatLink", function (e) {
            $(".chat-link").hide()
        }), $(".header-container .chat-link").hide(), $(document).on("click", ".chat-link", function (e) {
            e.preventDefault(), LC_API.open_chat_window()
        });
        var i = $(".scheduler-container"),
            s = $(".scheduler-select"),
            l = $(".time-slot"),
            c = $(".representative-text"),
            d = $(".loading-container"),
            u = $(".note-text"),
            h = $(".timeslothidden");

        function p() {
            var e = $("#scheduler-none"),
                t = $(".scheduler.dates > li > a.active"),
                o = $(".scheduler.times > li > a.active"),
                n = "";
            e.prop("checked") ? n = "None" : t.length && o.length && (n = o.data("slotkey")), h.val(n)
        }

        function m(e) {
            var t = e,
                o = 12 <= t ? "PM" : "AM";
            return (t = (t %= 12) || 12) + " " + o
        }
        $(document).on("SetNewSchedulerDates", function (e, t) {
            _SCHEDULE_SLOTS = [], t && (_SCHEDULE_SLOTS = function (e) {
                for (var t =
                        new Array(0), o = 0; o < e.length; o++) {
                    for (var n = null, r = e[o], a = !1, i = !1, s = -1, l = 0; l < t.length; l++)
                        if (r.schDate === t[l].dateString) {
                            n = t[l], a = !0;
                            for (var c = 0; c < n.times.length; c++)
                                if (r.schFrmTime === n.times[c].timeStart) {
                                    i = !0, s = c;
                                    break
                                }
                            break
                        }
                    null === n && (n = {
                        dateString: r.schDate,
                        times: []
                    }), i ? parseInt(r.schDriveTime) < parseInt(n.times[s].data.schDriveTime) && (n.times[s].data = r) : n.times.push({
                        timeStart: r.schFrmTime,
                        timeStop: r.schToTime,
                        data: r
                    }), a || t.push(n)
                }
                for (var d = 0; d < t.length; d++) t[d].times.sort(function (e, t) {
                    var e =
                        parseInt(e.timeStart.toString().substring(0, 2)),
                        t = parseInt(t.timeStart.toString().substring(0, 2));
                    return e - t
                });
                return t
            }(t)), null != _SCHEDULE_SLOTS && 0 < _SCHEDULE_SLOTS.length ? (! function (e, t) {
                for (var o = $("ul.scheduler.dates"), n = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], r = 0; r < e.length; r++) {
                    var a = new Date(e[r].dateString),
                        i = '<li><a href="#" class="schedule-date" data-date="' + e[r].dateString + '"><span class="month">' + n[a.getMonth()] + '</span><span class="date">' + a.getDate() +
                        "</span></a></li>";
                    o.append(i)
                }
                t()
            }(_SCHEDULE_SLOTS, function () {
                i.show(), u.show(), d.hide()
            }), $(document).trigger("SetDefaultDateAndTime")) : (i.hide(), c.show(), u.show(), d.hide(), $("#scheduler-none").trigger("click"))
        }), $("#scheduler-none").on("change", function (e) {
            $(this).prop("checked") ? (s.hide(), c.show()) : (s.show(), c.hide()), p()
        }), $(document).on("click", ".scheduler.dates > li > a", function (e) {
            e.preventDefault();
            var t = $(this),
                o = $(".scheduler.dates > li > a");
            t.hasClass("active") || (o.removeClass("active"),
                t.addClass("active"),
                function (e, t) {
                    var o = $("ul.scheduler.times"),
                        n = function (e) {
                            for (var t = null, o = 0; o < _SCHEDULE_SLOTS.length; o++) _SCHEDULE_SLOTS[o].dateString === e && (t = _SCHEDULE_SLOTS[o]);
                            return t
                        }(e);
                    o.empty();
                    for (var r = 0; r < n.times.length; r++) {
                        var a = m(n.times[r].timeStart.toString().substring(0, 2)),
                            i = m(n.times[r].timeStop.toString().substring(0, 2)),
                            s = '<li><a href="#" class="schedule-time" data-slotKey="' + n.times[r].data.schSlotKey + '"><span>' + a + " - " + i + "</span></a></li>";
                        o.append(s)
                    }
                    t()
                }(t.data("date"),
                    function () {
                        l.show()
                    })), p()
        }), $(document).on("click", ".scheduler.times > li > a", function (e) {
            e.preventDefault();
            var t = $(this),
                o = $(".scheduler.times > li > a");
            t.hasClass("active") || (o.removeClass("active"), t.addClass("active"), p())
        }), $(document).on("SetDefaultDateAndTime", function () {
            $(".scheduler.dates > li:first-child > a").trigger("click"), $(".scheduler.times > li:first-child > a").trigger("click")
        })
    }), 
    
//    $(window).on("load", function () {
//        $(".AB_EVENT").each(function () {
//            ga("create", utag.sender[15].data.account[0],
//                "auto"), ga("send", "event", "_setABTest", "sucessfullySet", {
//                dimension2: $(this).attr("id")
//            })
//        })
//    }),
    function () {
        var e = new r,
            t = "tmx_city",
            o = "tmx_state",
            n = "tmx_zipcode";

        function r(e, t, o, n) {
            this.zipCode = e, this.city = t, this.state = o, this.coordinates = n
        }
        null != localStorage.getItem(t) && "" != localStorage.getItem(t) && (e.city = localStorage.getItem(t)), null != localStorage.getItem(o) && "" != localStorage.getItem(o) && (e.state = localStorage.getItem(o)), null != localStorage.getItem(n) && "" != localStorage.getItem(n) && (e.zipCode = localStorage.getItem(n),
            $("#txtZipCodeLocation").val(e.zipCode), $("#cbRememberLocation").prop("checked", !0)), $(document).ready(function () {
            $("#txtZipCodeLocation").on("change paste keyup", function () {}), $("#formFindLocations").submit(function (e) {
                if (5 <= $("#txtZipCodeLocation").val().length) return e.preventDefault(), window.location = "https://www.terminix.com/exterminators/search?q=" + $("#txtZipCodeLocation").val(), !1;
                console.log("invalid")
            })
        })
    }();


