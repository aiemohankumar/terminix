function LOCAPI_VERIFY() {
    var wait = LOCAPI_PROCESSING_MODAL();
    var data = LOCAPI_MARSHALL_DATA();
    VERIFY_REPONSE(data);
    return false
}

function LOCAPI_PROCESSING_MODAL() {
    if (LOCAPI_CONFIG.PURE_JS_MODAL) LOCAPI_CONFIG.OPEN_MODAL();
    document.querySelectorAll("#" + LOCAPI_CONFIG.PROCESSING_BODY_ID + " .processing")[0].innerHTML = LOCAPI_VERIFYLEVELS.processing;
    LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById(LOCAPI_CONFIG.EDIT_BODY_ID));
    LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById(LOCAPI_CONFIG.PROCESSING_BODY_ID));
    if (!LOCAPI_CONFIG.PURE_JS_MODAL) LOCAPI_CONFIG.OPEN_MODAL()
}

function LOCAPI_MARSHALL_DATA() {
    var addressLine = "";
    if (LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD) addressLine = document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value;
    else addressLine = document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value + " " + document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value;
    var city = document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value;
    var state = document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value;
    var postalCode = document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value;
    var returnable = "?brand=" + LOCAPI_CONFIG.BRAND;
    if (addressLine) returnable += "&addressLine=" + addressLine;
    if (city) returnable += "&city=" + city;
    if (state) returnable += "&state=" + state;
    if (postalCode) returnable += "&postalCode=" + postalCode;
    returnable += "&country=USA";
    return returnable
}

function VERIFY_REPONSE(data) {
    var xhr = new XMLHttpRequest;
    xhr.open("GET", LOCAPI_CONFIG.SUBMIT_PATH + data, true);
    xhr.timeout = 4E4;
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var response = null;
            try {
                response = JSON.parse(xhr.responseText)
            } catch (error) {}
            LOCAPI_PROCESS_RESPONSE(response)
        }
    }
}

function LOCAPI_PROCESS_RESPONSE(res) {
    var responseCode = "Unresponsive";
    if (res != null && typeof res.data !== "undefined" && typeof res.data[0] !== "undefined") responseCode = res.data[0].verifyLevel;
    if (res != null && res.hasOwnProperty("errors") && res.errors.length)
        for (i = 0; i < res.errors.length; i++) {
            if (res.errors[i].severity == "Severe") {
                responseCode = "PremisesPartial";
                break
            }
            if (res.errors[i].severity == "Critical") responseCode = "None"
        }
    if (responseCode == null || responseCode == "Verified" || responseCode == "Unresponsive" || responseCode ==
        "Unverified" || responseCode == "InteractionRequired" && !LOCAPI_CONFIG.SHOW_CORRECTIONS) LOCAPI_PUSH_RESPONSE(res);
    else LOCAPI_GENERATE_MODALS(res, responseCode)
}

function LOCAPI_PUSH_RESPONSE(res) {
    if (res != null && typeof res.data !== "undefined" && typeof res.data[0] !== "undefined")
        if (res.data[0].verifyLevel != null) var returned = LOCAPI_PUSH_DATA(res.data[0]);
    LOCAPI_CONFIG.AFTER_VERIFY(res);
    LOCAPI_CONFIG.CLOSE_MODAL()
}

function LOCAPI_PUSH_DATA(data) {
    var addressLine = "";
    if (LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD) document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value = data.address1;
    else {
        document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value = data.address1;
        if (data.address2);
        document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value = data.address2
    }
    document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value = data.city;
    document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value = data.state;
    document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value =
        data.postalCode;
    if (document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD)) document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD).value = data.ID;
    for (var key in LOCAPI_CONFIG.POST_VERIFY_FIELD_MODS)
        if (document.getElementById(LOCAPI_CONFIG[key])) {
            var propVal = document.getElementById(LOCAPI_CONFIG[key]).value;
            document.getElementById(LOCAPI_CONFIG[key]).value = LOCAPI_CONFIG.POST_VERIFY_FIELD_MODS[key](propVal)
        } return true
}

function LOCAPI_GENERATE_MODALS(res, resCode) {
    var responseCode = resCode ? resCode : "None";
    var entered = "";
    if (LOCAPI_CONFIG.ADDRESS_FIELD == LOCAPI_CONFIG.SUITE_APT_FIELD) entered += document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value;
    else entered += document.getElementById(LOCAPI_CONFIG.ADDRESS_FIELD).value + " " + document.getElementById(LOCAPI_CONFIG.SUITE_APT_FIELD).value;
    entered += " <br/> " + document.getElementById(LOCAPI_CONFIG.CITY_FIELD).value;
    entered += ", " + document.getElementById(LOCAPI_CONFIG.STATE_FIELD).value;
    entered += " " + document.getElementById(LOCAPI_CONFIG.POSTALCODE_FIELD).value;
    document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " .locUserEntered")[0].innerHTML = entered;
    for (var key in LOCAPI_VERIFYLEVELS.UseAsEntered)
        if (document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " ." + key)[0]) document.querySelectorAll("#" + LOCAPI_CONFIG.USE_AS_ENTERED_ID + " ." + key)[0].innerHTML = LOCAPI_VERIFYLEVELS.UseAsEntered[key];
    var inputRequired = false;
    for (var key in LOCAPI_VERIFYLEVELS[responseCode]) {
        if (document.querySelectorAll("#" +
                LOCAPI_CONFIG.MODIFY_ID + " ." + key)[0])
            if (LOCAPI_VERIFYLEVELS[responseCode][key] != "NONE") {
                document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + key)[0].innerHTML = LOCAPI_VERIFYLEVELS[responseCode][key];
                LOCAPI_CONFIG.REMOVE_HIDDEN(document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + key)[0])
            } else {
                document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + key)[0].innerHTML = "";
                LOCAPI_CONFIG.ADD_HIDDEN(document.querySelectorAll("#" + LOCAPI_CONFIG.MODIFY_ID + " ." + key)[0])
            } if (key == "inputBind" && LOCAPI_VERIFYLEVELS[responseCode][key] !=
            "NONE") {
            document.getElementById("locApiInput").setAttribute("data-locbind", LOCAPI_VERIFYLEVELS[responseCode][key]);
            document.getElementById("locApiInput").value = document.getElementById(LOCAPI_CONFIG[LOCAPI_VERIFYLEVELS[responseCode][key]]).value;
            LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locApiInput"))
        } else {
            document.getElementById("locApiInput").setAttribute("data-locbind", "");
            LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locApiInput"))
        }
    }
    LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locSuggested"));
    document.getElementById("locSuggested").setAttribute("data-locbind", "");
    document.getElementById("locSuggested").innerHTML = "";
    LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById("locMatchList"));
    document.getElementById("locMatchList").setAttribute("data-locbind", "");
    document.getElementById("locMatchList").innerHTML = "";
    if (responseCode == "InteractionRequired") {
        var suggested = "";
        suggested += res.data[0].address1;
        if (res.data[0].address2 != "") suggested += " " + res.data[0].address2;
        suggested += " <br/> " + res.data[0].city;
        suggested += ", " + res.data[0].state;
        suggested += " " + res.data[0].postalCode;
        document.getElementById("locSuggested").setAttribute("data-locbind", JSON.stringify(res.data[0]));
        document.getElementById("locSuggested").innerHTML = suggested;
        LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locSuggested"))
    }
    if (responseCode == "Multiple") {
        for (i = 0; i < res.data.length; i++)
            if (i < LOCAPI_CONFIG.MAX_SUGGESTIONS) {
                var dataInfo = "";
                document.getElementById("locMatchList").innerHTML += "<" + LOCAPI_CONFIG.MATCH_TAG + "><a href='#' onclick='LOCAPI_USE_THIS(this)'>" +
                    res.data[i].fullAddress + "</a></" + LOCAPI_CONFIG.MATCH_TAG + ">"
            } LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById("locMatchList"))
    }
    LOCAPI_CONFIG.ADD_HIDDEN(document.getElementById(LOCAPI_CONFIG.PROCESSING_BODY_ID));
    LOCAPI_CONFIG.REMOVE_HIDDEN(document.getElementById(LOCAPI_CONFIG.EDIT_BODY_ID))
}

function LOCAPI_REVERIFY() {
    var hasMods = false;
    if (document.getElementById("locApiInput").getAttribute("data-locbind") != "") {
        document.getElementById(LOCAPI_CONFIG[document.getElementById("locApiInput").getAttribute("data-locbind")]).value = document.getElementById("locApiInput").value;
        hasMods = true
    }
    if (document.getElementById("locSuggested").getAttribute("data-locbind") != "") {
        var boundJson = document.getElementById("locSuggested").getAttribute("data-locbind");
        var returned = LOCAPI_PUSH_DATA(JSON.parse(boundJson));
        hasMods = true
    }
    if (hasMods) LOCAPI_VERIFY();
    else LOCAPI_CONFIG.CLOSE_MODAL()
}

function LOCAPI_USE_THIS(elem) {
    var line = elem.innerHTML;
    var regexP = /((.)+(, (.)+)([A-Z]{2} )((\d){5}-(\d){4}))/g;
    var match = regexP.exec(line);
    var dataObj = {};
    dataObj.postalCode = match[6].trim();
    dataObj.state = match[5].trim();
    dataObj.city = match[3].substring(2).trim();
    dataObj.address1 = match[0].split(match[3])[0];
    dataObj.address2 = "";
    var returned = LOCAPI_PUSH_DATA(dataObj);
    LOCAPI_VERIFY()
}

function LOCAPI_BYPASS() {
    LOCAPI_CONFIG.CLOSE_MODAL();
    LOCAPI_CONFIG.AFTER_VERIFY("")
}

function isPropertyInformationEnabled() {
    var isEnabled = true;
    var addressId = document.getElementById("propertyInformationOff_hidden");
    if (addressId !== null) isEnabled = false;
    return isEnabled
}

function LOCAPI_PROP_DATA() {
    var isEnabled = isPropertyInformationEnabled();
    console.log(isEnabled);
    if (document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD) && isEnabled) {
        var id = document.getElementById(LOCAPI_CONFIG.ADDRESS_ID_FIELD).value;
        if (id.replace("0", "").replace("-", "").trim()) {
            var data = "?brand=" + LOCAPI_CONFIG.BRAND;
            data += "&addressId=" + id;
            var xhr = new XMLHttpRequest;
            xhr.open("GET", LOCAPI_CONFIG.PROP_SUBMIT_PATH + data, true);
            xhr.timeout = 4E4;
            xhr.send(JSON.stringify(data));
            xhr.onreadystatechange = function () {
                if (xhr.readyState ===
                    4) {
                    var response = null;
                    try {
                        response = JSON.parse(xhr.responseText)
                    } catch (error) {}
                    LOCAPI_CONFIG.PROP_PROCESS_RESPONSE(response)
                }
            }
        } else LOCAPI_CONFIG.PROP_AFTER_VERIFY()
    } else LOCAPI_CONFIG.PROP_AFTER_VERIFY()
};
