function ViewModel() {
    var self = this;

    self.model = ko.observable(null);
    self.modelLoaded = ko.observable(false);
    self.recaptchaToken = ko.observable();

    ViewModel_AddCommonFunctions(self);
    ViewModel_AddStepFunctions(self, {
        stepOrder: ['lookup', 'properties', 'personal', 'account'],
        OnChangeStep: function (step) {
            self.QAS.Reset();
        }
    });

    var checker = IsChatAvailableChecker(function (data) {
        if (LiveChat_IsEnabledFor('Register')) {
            self.isLiveChatEnabled(data.areAgentsAvailable || data.isVisitorEngaged);
        } else {
            checker.remove();
        }
    });

    self.QAS = {
        isChoosingAddress: ko.observable(false),
        isChoosingEmail: ko.observable(false),
        address: ko.observable(null),
        addresses: ko.observableArray(),
        emails: ko.observableArray(),
        selectedAddress: ko.observable(null),
        selectedEmail: ko.observable(null),
        lastValidatedAddress: null,
        lastValidatedEmail: null,
        isVerifying: ko.observable(false),

        Reset: function () {
            self.QAS.isChoosingAddress(false);
            self.QAS.isChoosingEmail(false);
            self.QAS.address(null);
            self.QAS.addresses.removeAll();
            self.QAS.emails.removeAll();
            self.QAS.selectedAddress(null);
            self.QAS.selectedEmail(null);
            self.QAS.lastValidatedAddress = null;
            self.QAS.lastValidatedEmail = null;
            self.QAS.isVerifying(false);
        },

        KeepAddress: function () {
            self.QAS.lastValidatedAddress = ko.mapping.toJS(self.model().address);

            self.Lookup();
        },

        KeepEmail: function (field, submit) {
            self.QAS.lastValidatedEmail = ko.unwrap(field);

            if (typeof submit == 'function') {
                submit();
            }
        },

        UseSelectedAddress: function () {
            if (self.QAS.selectedAddress()) {
                self.model().address.address1(self.QAS.selectedAddress().address1);
                self.model().address.address2(self.QAS.selectedAddress().address2);
                self.model().address.city(self.QAS.selectedAddress().city);
                self.model().address.state(self.QAS.selectedAddress().state);
                self.model().address.zipcode(self.QAS.selectedAddress().zipcode);

                self.QAS.lastValidatedAddress = self.QAS.selectedAddress();
            }

            self.Lookup();
        },

        UseSelectedEmail: function (field, submit) {
            if (self.QAS.selectedEmail() && ko.isObservable(field)) {
                field(self.QAS.selectedEmail());
            }

            self.QAS.lastValidatedEmail = self.QAS.selectedEmail();

            if (typeof submit == 'function') {
                submit();
            }
        },

        IsLastValidatedAddress: function () {
            if (!self.QAS.lastValidatedAddress)
                return false;

            var address = ko.mapping.toJS(self.model().address);

            return address.address1 == self.QAS.lastValidatedAddress.address1 &&
                address.address2 == self.QAS.lastValidatedAddress.address2 &&
                address.city == self.QAS.lastValidatedAddress.city &&
                address.state == self.QAS.lastValidatedAddress.state &&
                address.zipcode == self.QAS.lastValidatedAddress.zipcode;
        },

        IsLastValidatedEmail: function (field) {
            return ko.unwrap(field) == self.QAS.lastValidatedEmail;
        }
    };

    self.passwordIsValid = ko.observable(false);

    self.LoadData = function () {
        CallDWR('http://test.terminix.com/my-account/dwr/jsonp/MyAccountTMXRegistrationUIUtils/getRegisterModel', null, function (data) {
            if (data.error) {
                alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
                return;
            }

            var json = data.model;

            self.model(ko.mapping.fromJS(json));

            if (self.model().redirectURL()) {
                Redirect(self.model().redirectURL());
                return;
            }

			if (self.model().GAEventLabel) {
				utag.link({
					ga_event_category: "My Account",
					ga_event_action: "Register",
					ga_event_label: self.model().GAEventLabel()
				});
			}
			
			self.modelLoaded(true);
			
			self.UpdateAutoFocus();
		});
	};
	
	self.IsValidLookup = function()
	{
		if (self.model().lookupType() == 'residential')
		{
			if (!IsValidPhoneNumber(self.model().lookupPhoneNumber()))
				return false;
			
			if (self.model().lookupResOption() == 'address')
			{
				if (!self.model().address.address1()
				||  !self.model().address.city()
				||  !self.model().address.state()
				||  !self.model().address.zipcode())
					return false;
			}
			else if (self.model().lookupResOption() == 'customerId')
			{
				if (!self.model().customerId())
					return false;
			}
			else if (self.model().lookupResOption() == 'salesAgreementId')
			{
				if (!self.model().salesAgreementId())
					return false;
			}
			else
				return false;
		}
		else if (self.model().lookupType() == 'commercial')
		{
			if (!self.model().customerId())
				return false;
			
			if (self.model().lookupComOption() == 'address')
			{
				if (!self.model().address.address1()
				||  !self.model().address.city()
				||  !self.model().address.state()
				||  !self.model().address.zipcode())
					return false;
			}
			else if (self.model().lookupComOption() == 'invoiceId')
			{
				if (!self.model().invoiceId())
					return false;
			}
			else if (self.model().lookupComOption() == 'salesAgreementId')
			{
				if (!self.model().salesAgreementId())
					return false;
			}
			else
				return false;
		}
		else
			return false;
		
		return true;
	};
	
	self.Lookup = function(m, e)
	{
		if (!self.IsValidLookup() || self.submitting()) return;
		
		self.RemoveAllErrors();

        self.QAS.isChoosingAddress(false);

        var json = ko.mapping.toJS(self.model);

        json.isRegisterByProp = false;

        if (self.model().lookupType() == 'residential') {
            json.isRegisterByProp = self.model().lookupResOption() == 'address';
        } else if (self.model().lookupType() == 'commercial') {
            json.isRegisterByProp = self.model().lookupComOption() == 'address';
        }

        var SendLookupData = function () {
            self.submitting(true);

            CallDWR('MyAccountTMXRegistrationUIUtils/handleStepLookup', json, function (data) {
                self.submitting(false);

                if (data.error) {
                    self.model().errorMessages.removeAll();
                    self.model().errorMessages.push({
                        errorMessage: data.error
                    });
                    return;
                }

                json = data.model;

                self.model(ko.mapping.fromJS(json));

                if (self.model().redirectURL()) {
                    Redirect(self.model().redirectURL());
                    return;
                }

                self.UpdateAutoFocus();
            });
        };

        if (json.isRegisterByProp && !self.QAS.IsLastValidatedAddress()) {
            self.QAS.isVerifying(true);
            self.submitting(true);

            QAS_VerifyAddress(json.address, function (model, error) {
                self.submitting(false);
                self.QAS.isVerifying(false);

                if (!model || !model.alternativeAddresses || !model.alternativeAddresses.length) {
                    SendLookupData();
                } else {
                    self.QAS.address(json.address);
                    self.QAS.addresses(model.alternativeAddresses);
                    self.QAS.selectedAddress(model.alternativeAddresses[0]);

                    self.QAS.isChoosingAddress(true);
                }
            });
        } else {
            SendLookupData();
        }
    };

    self.GetAddressLine = function (address) {
        var address1 = ko.unwrap(address.address1),
            address2 = ko.unwrap(address.address2);

        return address2 ? (address1 + ' ' + address2) : address1;
    };

    self.HasPropertiesSelected = function () {
        return self.model().foundPartyList().filter(function (party) {
            return party.isUseParty();

        }).length > 0;
    };

    self.ConfirmProperties = function (m, e) {
        if (!self.HasPropertiesSelected() || self.submitting()) return;

        self.RemoveAllErrors();

        self.submitting(true);

        var json = ko.mapping.toJS(self.model);

        CallDWR('MyAccountTMXRegistrationUIUtils/handleStepProperties', json, function (data) {
            self.submitting(false);

            if (data.error) {
                self.model().errorMessages.removeAll();
                self.model().errorMessages.push({
                    errorMessage: data.error
                });
                return;
            }

            json = data.model;

            self.model(ko.mapping.fromJS(json));

            if (self.model().redirectURL()) {
                Redirect(self.model().redirectURL());
                return;
            }

            self.UpdateAutoFocus();
        });
    };

    self.IsValidPersonalInformation = function () {
        if (!self.model().firstName() ||
            !self.model().lastName()
            //TMA-115
            /*||  !IsValidPhoneNumber(self.model().phoneNumber())*/
            ||
            !IsValidEmail(self.model().email()))
            return false;

        return true;
    };

    self.ConfirmPersonalInformation = function (m, e) {
        if (!self.IsValidPersonalInformation() || self.submitting()) return;

        self.RemoveAllErrors();

        self.QAS.isChoosingEmail(false);

        var json = ko.mapping.toJS(self.model);

        var SendPersonalData = function () {
            self.submitting(true);

            CallDWR('MyAccountTMXRegistrationUIUtils/handleStepPersonal', json, function (data) {
                self.submitting(false);

                if (data.error) {
                    self.model().errorMessages.removeAll();
                    self.model().errorMessages.push({
                        errorMessage: data.error
                    });
                    return;
                }

                json = data.model;

                self.model(ko.mapping.fromJS(json));

                if (self.model().redirectURL()) {
                    Redirect(self.model().redirectURL());
                    return;
                }

                self.UpdateAutoFocus();
            });
        };

        if (self.QAS.IsLastValidatedEmail(json.email)) {
            SendPersonalData();
        } else {
            self.QAS.isVerifying(true);
            self.submitting(true);

            QAS_VerifyEmail(json.email, function (data) {
                self.submitting(false);
                self.QAS.isVerifying(false);

                if (data.model && data.model.corrections && data.model.corrections.length) {
                    self.QAS.emails(data.model.corrections);
                    self.QAS.selectedEmail(null);
                    self.QAS.isChoosingEmail(true);
                } else if (data.model && data.model.errorMessages && data.model.errorMessages.length) {
                    self.model().errorMessages(data.model.errorMessages);
                } else {
                    self.QAS.lastValidatedEmail = json.email;

                    SendPersonalData();
                }
            });
        }
    };

    self.IsValidAccount = function () {
        if (!IsValidEmail(self.model().login()) ||
            !self.model().password())
            return false;

        return true;
    };

    self.AllChecked = ko.computed({
        read: function () {
            if (self.model() && self.model().foundPartyList() != undefined) {
                var firstUnchecked = ko.utils.arrayFirst(self.model().foundPartyList(), function (item) {
                    return item.isUseParty() == false;
                });
            }
            return firstUnchecked == null;
        },
        write: function (value) {
            if (self.model() && self.model().foundPartyList()) {
                ko.utils.arrayForEach(self.model().foundPartyList(), function (item) {
                    item.isUseParty(value);
                });
            }
        }
    });

    self.SetupAccount = function (m, e) {
        if (!self.IsValidAccount() || self.submitting()) return;

        self.RemoveAllErrors();

        self.QAS.isChoosingEmail(false);

        var model = ko.mapping.toJS(self.model);

        var SendAccountData = function () {
            model.recaptchaToken = self.recaptchaToken();

            if ($("#recaptcha-btn").length && !model.recaptchaToken) {
                $("#recaptcha-btn").click();
                return false;
            }

            self.submitting(true);

            CallDWR('MyAccountTMXRegistrationUIUtils/handleStepAccount', model, function (data) {
                self.submitting(false);

                if (data.error) {
                    self.model().errorMessages.removeAll();
                    self.model().errorMessages.push({
                        errorMessage: data.error
                    });
                    return;
                }

                var json = data.model;

                // If recaptcha token was unset on backend, then remove it on front-end
                // Also clear out recaptcha response if there was one
                if (!json.recapchaToken && model.recaptchaToken) {
                    self.recaptchaToken('');

                    if (grecaptcha.getResponse()) {
                        grecaptcha.reset();
                    }
                }

                self.model(ko.mapping.fromJS(json));

                if (self.model().redirectURL()) {
                    setTimeout(function () {
                        Redirect(self.model().redirectURL());

                    }, 1500);
                    return;
                }

                self.UpdateAutoFocus();
            });
        };

        if (self.QAS.IsLastValidatedEmail(model.login)) {
            SendAccountData();
        } else {
            self.QAS.isVerifying(true);
            self.submitting(true);

            QAS_VerifyEmail(model.login, function (data) {
                self.submitting(false);
                self.QAS.isVerifying(false);

                if (data.model && data.model.corrections && data.model.corrections.length) {
                    self.QAS.emails(data.model.corrections);
                    self.QAS.selectedEmail(null);
                    self.QAS.isChoosingEmail(true);
                } else if (data.model && data.model.errorMessages && data.model.errorMessages.length) {
                    self.model().errorMessages(data.model.errorMessages);
                } else {
                    self.QAS.lastValidatedEmail = model.login;

                    SendAccountData();
                }
            });
        }
    };
}

function registrationRecaptchaCallback(data) {
    //vm.recaptchaToken(data);
    vm.SetupAccount();
}

$(function () {
    window.vm = new ViewModel();

    ko.applyBindings(vm);

    $('.on-knockout-loaded').show();

    vm.LoadData();
});
