/*
 * Module: Payments Small
 ***********************************************************************************************************/
TAB.Modules.Add('payments-small', function($module, options, onLoaded)
{
	if (g_HidePaymentsTab)
	{
		$module.detach();
		return onLoaded();
	}
	
	debugger;
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,

		// Remove self.LoadData = function(){} from the callback() param when setting LoadDataDWR
		LoadDataDWR: 'my-account-json/getPaymentDueDataSmall.json',

		callback: function(self)
		{
			debugger;
			self.ViewPaymentHistory = function(m, e)
			{
				TAB.Load('payments', { focusModule: 'payment-history' });
			};

			self.ViewPayments = function(m, e)
			{
				TAB.Load('payments', { focusModule: 'due-payments' });
			};
			// Override UpdateModel so we can add renewal discount alert
			var UpdateModel = self.UpdateModel;
			self.UpdateModel = function(json) {
				debugger;
				UpdateModel(json);
				var renewalDiscount = false;
				if (self.model()) {
					if (ko.isObservable(self.model().duePayments)) {
						self.model().duePayments().forEach(function(duePayment) {
							if(duePayment.renewalDiscountAmount() > 0){// && duePayment.amountDue() == duePayment.totalAmount()){
								renewalDiscount = true;
							}
						});
					}
					if(renewalDiscount){
						LookupMessage("renewalDiscount");
					}
				}
			}
		}
	
	});
	
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});