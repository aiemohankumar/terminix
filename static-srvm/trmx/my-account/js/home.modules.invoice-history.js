/*
 * Module: Invoice History
 ***********************************************************************************************************/
TAB.Modules.Add('invoice-history', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,

		//LoadDataDWR: 'MyAccountTMXPaymentUIUtils/getPaymentDueData',

		api:
		{
			setPaymentDueData: function(self, json)
			{
				self.UpdateModel(json);
				
				if (self.model().redirectURL())
				{
					Redirect(self.model().redirectURL());
					return;
				}
				
				self.modelLoaded(true);
				
				onLoaded();
			}
		},
		
		callback: function(self)
		{
			// Backwards compatibility for when LoadDataDWR is turned off
			self.LoadData = self.LoadData || function(){};
			
			var UpdateModel = self.UpdateModel;
			
			self.UpdateModel = function(json)
			{
				if (json.invoiceHistory && json.invoiceHistory.history && json.invoiceHistory.history.length > 0)
				{
					json.invoiceHistory.history.forEach(function(item)
					{
						item.documentId = item.documentId || null;
						item.downloadLink = item.downloadLink || ''; // Only use for testing purposes
						item.loading = false;
					});
				}
				
				UpdateModel(json);
			};
			
			self.defaultNumViewable = 10;
			self.numViewableIncrease = 10;
			self.numViewable = ko.observable(self.defaultNumViewable);
			
			self.GetViewableHistory = ko.computed(function()
			{
				return self.model() ? self.model().invoiceHistory.history().slice(0, self.numViewable()) : [];
			});
			
			self.CheckInvoiceLink = function(item, event)
			{
				// If it already has a link, then allow the link to work
				if (item.downloadLink())
					return true;
				
				// If already loading the link, do nothing
				if (item.loading())
					return false;
				
				// Load the link
				item.loading(true);
				
				var json = ko.mapping.toJS(item);
				
				CallDWR('MyAccountTMXPaymentUIUtils/getInvoiceDocumentLink', json, function(data)
				{
					if (data.error || (data.model && data.model.errorMessages.length > 0))
					{
						CreateAlert(
						{
							color: 'red',
							button: false,
							closeButton: true,
							message: "There was a problem retrieving the document link for your invoice. Please try again.",
							scrollToAlerts: true,
							$module: $module
						});
					}
					else if (data.model)
					{
						item.downloadLink(data.model.downloadLink);
						
						window.open(item.downloadLink(), '_blank');
					}
					
					item.loading(false);
				});
			};

			self.CanViewMore = function()
			{
				if (!self.model())
					return false;
				
				return self.numViewable() < self.model().invoiceHistory.history().length;
			};

			self.CanViewLess = function()
			{
				if (!self.model())
					return false;
				
				return self.numViewable() != self.defaultNumViewable;
			};

			self.ViewMore = function()
			{
				self.numViewable(Math.min(self.numViewable() + self.numViewableIncrease, self.model().invoiceHistory.history().length));
				
				return self;
			};

			self.ViewLess = function()
			{
				self.numViewable(self.defaultNumViewable);
				
				return self;
			};
			
			self.GoToMyAccount = function()
			{
				TAB.Load('my-account');
				
				return self;
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});