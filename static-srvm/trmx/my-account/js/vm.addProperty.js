function ViewModel()
{
	var self = this;
	
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);

	ViewModel_AddCommonFunctions(self);
	ViewModel_AddStepFunctions(self, {
		stepOrder: ['lookup', 'properties']
	});
	
	var checker = IsChatAvailableChecker(function(data) {
		if (LiveChat_IsEnabledFor('Dashboard')) {
			self.isLiveChatEnabled(data.areAgentsAvailable || data.isVisitorEngaged);
		} else {
			checker.remove();
		}
	});

	self.QAS = {
		isChoosing: ko.observable(false),
		address: ko.observable(null),
		addresses: ko.observableArray(),
		selectedAddress: ko.observable(null),
		lastValidatedAddress: null,
		
		KeepAddress: function()
		{
			self.QAS.lastValidatedAddress = ko.mapping.toJS(self.model().address);
			
			self.Lookup();
		},
		
		UseSelected: function()
		{
			if (self.QAS.selectedAddress())
			{
				self.model().address.address1(self.QAS.selectedAddress().address1);
				self.model().address.address2(self.QAS.selectedAddress().address2);
				self.model().address.city(self.QAS.selectedAddress().city);
				self.model().address.state(self.QAS.selectedAddress().state);
				self.model().address.zipcode(self.QAS.selectedAddress().zipcode);
				
				self.QAS.lastValidatedAddress = self.QAS.selectedAddress();
			}
			
			self.Lookup();
		},
		
		IsLastValidatedAddress: function()
		{
			if (!self.QAS.lastValidatedAddress)
				return false;
			
			var address = ko.mapping.toJS(self.model().address);
			
			return address.address1 == self.QAS.lastValidatedAddress.address1
			    && address.address2 == self.QAS.lastValidatedAddress.address2
			    && address.city     == self.QAS.lastValidatedAddress.city
			    && address.state    == self.QAS.lastValidatedAddress.state
			    && address.zipcode  == self.QAS.lastValidatedAddress.zipcode;
		}
	};
	
	self.LoadData = function()
	{
		CallDWR('my-account-json/getRegisterModel.json', null, function(data)
		{
			if (data.error)
			{
				alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
				return;
			}
			
			var json = data.model;

			self.model(ko.mapping.fromJS(json));
			
			if (self.model().redirectURL())
			{
				Redirect(self.model().redirectURL());
				return;
			}
			
			self.modelLoaded(true);
			
			self.UpdateAutoFocus();
		});
	};
	
	self.IsValidLookup = function()
	{
		if (self.model().lookupType() == 'residential')
		{
			if (!IsValidPhoneNumber(self.model().lookupPhoneNumber()))
				return false;
			
			if (self.model().lookupResOption() == 'address')
			{
				if (!self.model().address.address1()
				||  !self.model().address.city()
				||  !self.model().address.state()
				||  !self.model().address.zipcode())
					return false;
			}
			else if (self.model().lookupResOption() == 'customerId')
			{
				if (!self.model().customerId())
					return false;
			}
			else if (self.model().lookupResOption() == 'salesAgreementId')
			{
				if (!self.model().salesAgreementId())
					return false;
			}
			else
				return false;
		}
		else if (self.model().lookupType() == 'commercial')
		{
			if (!self.model().customerId())
				return false;
			
			if (self.model().lookupComOption() == 'address')
			{
				if (!self.model().address.address1()
				||  !self.model().address.city()
				||  !self.model().address.state()
				||  !self.model().address.zipcode())
					return false;
			}
			else if (self.model().lookupComOption() == 'invoiceId')
			{
				if (!self.model().invoiceId())
					return false;
			}
			else if (self.model().lookupComOption() == 'salesAgreementId')
			{
				if (!self.model().salesAgreementId())
					return false;
			}
			else
				return false;
		}
		else
			return false;
		
		return true;
	};
	
	self.Lookup = function(m, e)
	{
		if (!self.IsValidLookup() || self.submitting()) return;
		
		self.RemoveAllErrors();

		self.QAS.isChoosing(false);
		self.submitting(true);
		
		var json = ko.mapping.toJS(self.model);
		
		json.isRegisterByProp = false;
		
		if (self.model().lookupType() == 'residential')
		{
			json.isRegisterByProp = self.model().lookupResOption() == 'address';
		}
		else if (self.model().lookupType() == 'commercial')
		{
			json.isRegisterByProp = self.model().lookupComOption() == 'address';
		}
		
		if (json.isRegisterByProp && !self.QAS.IsLastValidatedAddress())
		{
			QAS_VerifyAddress(json.address, function(model, error)
			{
				self.submitting(false);
				
				if (!model || !model.alternativeAddresses || !model.alternativeAddresses.length)
				{
					self.QAS.KeepAddress();
				}
				else
				{
					self.QAS.address(json.address);
					self.QAS.addresses(model.alternativeAddresses);
					self.QAS.selectedAddress(model.alternativeAddresses[0]);
					
					self.QAS.isChoosing(true);
				}
			});
		}
		else
		{
			CallDWR('MyAccountTMXRegistrationUIUtils/handleStepLookup', json, function(data)
			{
				self.submitting(false);
				
				if (data.error)
				{
					self.model().errorMessages.removeAll();
					self.model().errorMessages.push({ errorMessage: data.error });
					return;
				}
				
				json = data.model;
	
				self.model(ko.mapping.fromJS(json));
				
				if (self.model().redirectURL())
				{
					Redirect(self.model().redirectURL());
					return;
				}
				
				self.UpdateAutoFocus();
			});
		}
	};
	
	self.GetAddressLine = function(address)
	{
		var address1 = ko.unwrap(address.address1),
			address2 = ko.unwrap(address.address2);
		
		return address2 ? (address1 + ' ' + address2) : address1;
	};
	
	self.HasPropertiesSelected = function()
	{
		return self.model().foundPartyList().filter(function(party)
		{
			return party.isUseParty();
			
		}).length > 0;
	};
	
	self.ConfirmProperties = function(m, e)
	{
		if (!self.HasPropertiesSelected() || self.submitting()) return;
		
		self.RemoveAllErrors();
		
		self.submitting(true);
		
		var json = ko.mapping.toJS(self.model);
		
		CallDWR('MyAccountTMXRegistrationUIUtils/addProperties', json, function(data)
		{
			self.submitting(false);
			
			if (data.error)
			{
				self.model().errorMessages.removeAll();
				self.model().errorMessages.push({ errorMessage: data.error });
				return;
			}
			
			json = data.model;
			
			self.model(ko.mapping.fromJS(json));
			
			if (self.model().redirectURL())
			{
				Redirect(self.model().redirectURL());
				return;
			}
			
			self.UpdateAutoFocus();
		});
	};
}

$(function()
{
	window.vm = new ViewModel();
	
	ko.applyBindings(vm);
	
	$('.on-knockout-loaded').show();
	
	vm.LoadData();
	
	ResetTimeout.initialize();
});