/*
 * Module: Easy Pay
 ***********************************************************************************************************/
TAB.Modules.Add('easy-pay', function($module, options, onLoaded) {
	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		LoadDataDWR : 'my-account-json/getEasyPayModel.json',

		api : {
			refreshStoredPaymentMethods : function(self, apiData) {
				var UpdateStoredPaymentMethods = function(storedPaymentMethods) {
					if (!self.model())
						return;

					// Find if there are any new stored payment methods to add

					var modelStoredPaymentMethods = self.model().storedPaymentMethods;

					if (typeof modelStoredPaymentMethods === 'function') {
						storedPaymentMethods.forEach(function(paymentMethod) {
							var existingPaymentMethod = ko.utils.arrayFirst(modelStoredPaymentMethods(), function(existingPaymentMethod) {
								return existingPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId;
							});

							if (!existingPaymentMethod) {
								paymentMethod.submitting = false;
								paymentMethod._original = $.extend(true, {}, paymentMethod);
								paymentMethod.type = ""
								
								modelStoredPaymentMethods.push(ko.mapping.fromJS(paymentMethod));
							}

						});
						
						ko.utils.arrayForEach(modelStoredPaymentMethods(), function(existingPaymentMethod) {
							if (existingPaymentMethod) {
								var isExistingPaymentMethod = ko.utils.arrayFirst(storedPaymentMethods, function(storedPaymentMethod) {
									return existingPaymentMethod.paymentMethodId() == storedPaymentMethod.paymentMethodId;
								});

								if (!isExistingPaymentMethod && existingPaymentMethod.paymentMethodId() != null) {
									modelStoredPaymentMethods.remove(existingPaymentMethod);
								}
							}
						});

					}
				};

				if (typeof apiData.storedPaymentMethods != 'undefined') {
					UpdateStoredPaymentMethods(apiData.storedPaymentMethods);
				} else {
					CallDWR('my-account-json/getStoredPaymentMethods.json', {}, function(data) {
						if (data.error) {
							// What do.
							return;
						}

						// Check if there is no array to work with
						if (!data.model.storedPaymentMethods || !IsArray(data.model.storedPaymentMethods)) {
							// What do.
							return;
						}

						UpdateStoredPaymentMethods(data.model.storedPaymentMethods);
					});
				}
			},

			updateEasyPaySalesAgreements : function(self, apiData) {
				self.ReloadModule();
			}
		},

		callback : function(self) {
			self.enrollPaymentMethod = ko.observable(null);

			self.sortedPaymentMethods = ko.computed(function() {
				if (!self.model())
					return [];

				var paymentMethods = self.model().storedPaymentMethods();

				return paymentMethods.slice(0).sort(function(a, b) {
					var typeA = a.type(), typeB = b.type();
					
					if (typeA == typeB)
						return ('' + a.description()).localeCompare('' + b.description());
					if (typeA == 'saved' || typeA == '')
						return -1;
					if (typeB == 'saved' || typeB == '')
						return 1;
					if (typeA == 'bank')
						return -1;
					return 1;
				});
			});

			self.UpdateModel = function(json) {
				// Add properties that aren't on the back-end
				json.submitting = false;
				
				json.salesAgreements.forEach(function(item) {
					item.submitting = false;
					item.modifyStep = '';
					// item.acceptedTerms = true;
					
					if (!item.paymentMethod) {
						item.paymentMethod = {
							type : '',
							paymentMethodId : '',
							description : ''
						};
					}

					item._original = $.extend(true, {}, item);
				});

				var model = ko.mapping.fromJS(json);
				
				model.salesAgreements().forEach(function(item) {
					item.selectedPaymentMethod = ko.observable(null);

					var storedPaymentMethods = json.storedPaymentMethods.slice().map(function(paymentMethod) {
						return $.extend({}, paymentMethod);
					});

					item.storedPaymentMethods = ko.mapping.fromJS(storedPaymentMethods);

					item.sortedPaymentMethods = ko.computed(function() {
						var paymentMethods = item.storedPaymentMethods();

						return paymentMethods.slice(0).sort(function(a, b) {
							var typeA = a.type(), typeB = b.type();

							if (typeA == typeB)
								return ('' + a.description()).localeCompare('' + b.description());
							if (typeA == 'saved' || typeA == '')
								return -1;
							if (typeB == 'saved' || typeB == '')
								return 1;
							if (typeA == 'bank')
								return -1;
							return 1;
						});
					});

					for (var i = 0; i < storedPaymentMethods.length; i++) {
						var paymentMethod = item.storedPaymentMethods()[i];

						if (paymentMethod.type() == item.paymentMethod.type() && item.paymentMethod.paymentMethodId() == paymentMethod.paymentMethodId()) {
							self.ApplyModelChanges(paymentMethod, ko.mapping.toJS(item.paymentMethod));

							item.selectedPaymentMethod(paymentMethod);
							break;
						}
					}
				});

				self.model(model);

				self.enrollPaymentMethod(null);

				if (json.enrollPaymentMethod) {
					var storedPaymentMethods = self.model().storedPaymentMethods();

					for (var i = 0; i < storedPaymentMethods.length; i++) {
						var paymentMethod = storedPaymentMethods[i];

						if (paymentMethod.type() == json.enrollPaymentMethod.type && json.enrollPaymentMethod.paymentMethodId == paymentMethod.paymentMethodId()) {
							self.ApplyModelChanges(paymentMethod, json.enrollPaymentMethod);

							self.enrollPaymentMethod(paymentMethod);
							break;
						}
					}
				}
			};

			self.HasEligibleSalesAgreements = function() {
				return self.model() && self.model().salesAgreements().length > 0;
			};

			self.HasActiveEasyPay = function() {
				return null !== ko.utils.arrayFirst(self.model().salesAgreements(), function(item) {
					return item.isActive();
				});
			};
			
			self.HasSalesAgreementsSelected = ko.computed(function() {
				return self.modelLoaded() && ko.utils.arrayFirst(self.model().salesAgreements(), function(item) {
					return item.isActive();
				});
			});
			
			self.HasSalesAgreementsSelected.subscribe(function(value) {
				if (self.modelLoaded() && !self.model().hasEnrolled() && !value) {
					self.CancelEnrollForm();
				}
			});

			self.IsValidEnrollment = function() {
				if (!self.HasSalesAgreementsSelected())
					return false;

				var paymentMethod = self.enrollPaymentMethod();

				if (!paymentMethod)
					return false;
				
				switch (paymentMethod.type()) {
				case 'card': {
					if (!paymentMethod.cardNumber() || !paymentMethod.cardExpireMonth() || !paymentMethod.cardExpireYear())
						return false;
					
					if (!paymentMethod.billingAddress1() || !paymentMethod.billingCity() || !paymentMethod.billingState() || !paymentMethod.billingZipcode())
						return false;
					break;
				}
				case 'bank': {
					if (!paymentMethod.bankRoutingNumber() || !paymentMethod.bankAccountNumber())
						return false;
					break;
				}
				}

				// if (!self.model().acceptedTerms()) return false;

				return true;
			};
			
			self.CancelEnrollForm = function() {
				if (!self.modelLoaded() || self.model().submitting()) return;
				
				var fieldsToRevert = ['cardNumber', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1', 'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'bankRoutingNumber', 'bankAccountNumber'];
				
				self.model().storedPaymentMethods().forEach(function(paymentMethod) {
					fieldsToRevert.forEach(function(field) {
						paymentMethod[field]('');
					});
				});
				
				self.enrollPaymentMethod(null);
			};

			self.SubmitEnrollForm = function() {
				if (!self.IsValidEnrollment() || self.model().submitting())
					return;

				self.RemoveAllErrors();

				self.model().submitting(true);

				var json = ko.mapping.toJS(self.model);

				json.enrollPaymentMethod = ko.mapping.toJS(self.enrollPaymentMethod);

				CallDWR('MyAccountTMXPaymentUIUtils/enrollEasyPay', json, function(data) {
					self.model().submitting(false);

					if (data.error) {
						self.model().errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.UpdateModel(data.model);

					// Check if it was successful
					if (self.model().informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(self.model().informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
						TAB.Modules.API('*', 'refreshStoredPaymentMethods', {
							storedPaymentMethods : data.model.storedPaymentMethods
						});

					}
				});
			};

			self.GetSortedSalesAgreements = function() {
				var salesAgreements = self.model().salesAgreements();

				return salesAgreements.filter(function(item) {
					return item.isActive();
				}).concat(salesAgreements.filter(function(item) {
					return !item.isActive();
				}));
			};
			
			var ScrollToItem = function(event) {
				var $summary = $(event.target).closest('tr.summary'),
					$detail = $summary.next();
				
				var offset = $summary.offset();
				offset.height = $summary.outerHeight() + $detail.outerHeight();
				offset.bottom = offset.top + offset.height;
				
				var windowTop = $(window).scrollTop();
				var windowHeight = $(window).height();
				var windowBottom = windowTop + windowHeight;
				
				if (offset.top > windowTop + (windowHeight * .2))
				{
					ScrollTo(offset.top, 800);
				}
				else if (offset.bottom < windowTop + (windowHeight * .8))
				{
					if (offset.height < windowHeight * .6)
					{
						ScrollTo(offset.top, 800);
					}
					else
					{
						ScrollTo(offset.bottom - (windowHeight * .8));
					}
				}
			};

			self.SetAddMode = function(item, event) {
				item.selectedPaymentMethod(null);
				
				item.modifyStep('add');
				
				ScrollToItem(event);
			};

			self.SetEditMode = function(item, event) {
				item.selectedPaymentMethod(item.storedPaymentMethods().find(function(storedPaymentMethod) {
					return storedPaymentMethod.type() == item.paymentMethod.type() && storedPaymentMethod.paymentMethodId() == item.paymentMethod.paymentMethodId();
				}));
				
				item.modifyStep('edit');
				
				ScrollToItem(event);
			};

			/*
			self.SetRemoveMode = function(item) {
				item.modifyStep('remove');
			};
			*/

			self.CancelAddForm = function(item) {
				item.modifyStep('');

				self.RemoveAllErrors(item);
				item.selectedPaymentMethod(null);

				self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

				var fieldsToRevert = ['cardNumber', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1', 'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'bankRoutingNumber', 'bankAccountNumber'];
				
				item.storedPaymentMethods().forEach(function(paymentMethod) {
					fieldsToRevert.forEach(function(field) {
						paymentMethod[field]('');
					});
				});
				
				// Scroll back to the table row
				var position = $module.find('.sales-agreements-table').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};

			self.CancelEditForm = function(item) {
				item.modifyStep('');

				self.RemoveAllErrors(item);
				self.RemoveAllErrors(item.selectedPaymentMethod);

				self.ApplyModelChanges(item, ko.mapping.toJS(item._original));
				
				item.selectedPaymentMethod(item.storedPaymentMethods().find(function(storedPaymentMethod) {
					return storedPaymentMethod.type() == item.paymentMethod.type() && storedPaymentMethod.paymentMethodId() == item.paymentMethod.paymentMethodId();
				}));

				var fieldsToRevert = ['cardNumber', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1', 'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'bankRoutingNumber', 'bankAccountNumber'];
				
				item.storedPaymentMethods().forEach(function(paymentMethod) {
					fieldsToRevert.forEach(function(field) {
						paymentMethod[field]('');
					});
				});
				
				// Scroll back to the table row
				var position = $module.find('.sales-agreements-table').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};

			/*
			self.CancelRemoveForm = function(item) {
				item.modifyStep('');

				self.RemoveAllErrors(item);

				// Scroll back to the table row
				var position = $module.find('.sales-agreements-table').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};
			*/

			self.GetAvailableCreditCardYears = function() {
				var years = [];

				for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
					years.push(i);
				}

				return years;
			};

			self.SetVisibleHelp = function(item, field) {
				item.visibleHelp(field);

				return self;
			};

			self.IsVisibleHelp = function(item, field) {
				return item.visibleHelp() == field;
			};

			self.IsValidEasyPay = function(item) {
				var paymentMethod = item.selectedPaymentMethod();
				if (!paymentMethod)
					return false;
				
				switch (paymentMethod.type()) {
				case 'card': {
					if (!paymentMethod.cardNumber() || !paymentMethod.cardExpireMonth() || !paymentMethod.cardExpireYear())
						return false;
					
					if (!paymentMethod.billingAddress1() || !paymentMethod.billingCity() || !paymentMethod.billingState() || !paymentMethod.billingZipcode())
						return false;
					break;
				}
				case 'bank': {
					if (!paymentMethod.bankRoutingNumber() || !paymentMethod.bankAccountNumber())
						return false;
					break;
				}
				}
				
				return true;
			};

			self.HasChangedEasyPay = function(item) {
				if (!item.selectedPaymentMethod())
					return false;

				return item.paymentMethod.type() != item.selectedPaymentMethod().type()
						|| item.paymentMethod.paymentMethodId() != item.selectedPaymentMethod().paymentMethodId();
			};

			self.SubmitAddForm = function(item) {
				if (!self.IsValidEasyPay(item) || item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				json.paymentMethod = json.selectedPaymentMethod;

				json.typeOfAutoPaySubmit = "add"; //used to differentiate between which success message to show
				
				CallDWR('MyAccountTMXPaymentUIUtils/editEasyPay', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
						TAB.Modules.API('*', 'refreshStoredPaymentMethods');

					}
				});
			};

			self.SubmitEditForm = function(item) {
				if (!self.IsValidEasyPay(item) || !self.HasChangedEasyPay(item) || item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				json.paymentMethod = json.selectedPaymentMethod;
				
				json.typeOfAutoPaySubmit = "edit"; //used to differentiate between which success message to show
				
				CallDWR('MyAccountTMXPaymentUIUtils/editEasyPay', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}
					
					if(data.model.errorMessages.length || data.model.fieldValidationErrors.length){
						data.model.paymentMethod = ko.mapping.toJS(item.paymentMethod);
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
						TAB.Modules.API('*', 'refreshStoredPaymentMethods');

					}
				});
			};

			/*
			self.SubmitRemoveForm = function(item) {
				if (item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				CallDWR('MyAccountTMXPaymentUIUtils/removeEasyPay', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
						TAB.Modules.API('*', 'refreshStoredPaymentMethods');

					}
				});
			};
			*/
		}
	});

	ko.applyBindings(vm, $module.get(0));

	vm.LoadData(onLoaded);
});