/*
 * Module: Preferences
 ***********************************************************************************************************/
TAB.Modules.Add('preferences', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,
		
		LoadDataDWR: 'my-account-json/getPreferences.json',
		
		OnAfterLoadData: function(self)
		{
			if (!self.model().isPreferencesEnabled())
			{
				$module.remove();
			}
		},
		
		callback: function(self)
		{
			self.scheduleDays = ['S', 'M', 'T', 'W', 'TH', 'F', 'SA'];
			self.scheduleTimes = ['AM', 'PM'];
			
			var scheduleDayTimePreferences = {};
			
			self.scheduleDays.forEach(function(dayKey)
			{
				scheduleDayTimePreferences[dayKey] = {};
				
				self.scheduleTimes.forEach(function(timeKey)
				{
					scheduleDayTimePreferences[dayKey][timeKey] = false;
				});
			});
			
			self.scheduleDayTimePreferences = ko.mapping.fromJS(scheduleDayTimePreferences);
			
			self.QAS = {
				isChoosingEmail: ko.observable(false),
				emails: ko.observableArray(),
				selectedEmail: ko.observable(null),
				lastValidatedEmail: null,
				isVerifying: ko.observable(false),
				
				KeepEmail: function()
				{
					self.QAS.lastValidatedEmail = self.model().serviceNotificationEmail();
					
					self.QAS.isChoosingEmail(false);
					
					self.Submit();
				},
				
				UseSelectedEmail: function()
				{
					if (self.QAS.selectedEmail())
					{
						self.model().serviceNotificationEmail(self.QAS.selectedEmail());
						
						self.QAS.lastValidatedEmail = self.QAS.selectedEmail();
					}
					
					self.QAS.isChoosingEmail(false);
					
					self.Submit();
				},
				
				IsLastValidatedEmail: function()
				{
					return self.QAS.lastValidatedEmail == self.model().serviceNotificationEmail();
				}
			};
			
			var UpdateModel = self.UpdateModel;
			
			self.UpdateModel = function(json)
			{
				// Update the day time preferences if it exists
				self.scheduleDays.forEach(function(dayKey)
				{
					self.scheduleTimes.forEach(function(timeKey)
					{
						self.scheduleDayTimePreferences[dayKey][timeKey](json && json.scheduleDayTimePreferences && json.scheduleDayTimePreferences[dayKey] && !!json.scheduleDayTimePreferences[dayKey][timeKey]);
					});
				});
				
				UpdateModel(json);
			};
			
			self.HasModelChanged = function()
			{
				if (!self.model() || !self.originalModel())
					return false;
				
				var originalModel = ko.mapping.toJS(self.originalModel);
				
				if (self.model().isServiceNotificationPreferencesEnabled())
				{
					var inputs = ['serviceNotificationPreferenceEmail', 'serviceNotificationPreferenceCall', 'serviceNotificationPreferenceText', 'serviceNotificationEmail'];
					
					for (var i = 0; i < inputs.length; i++)
					{
						if (self.model()[inputs[i]]() != originalModel[inputs[i]])
						{
							//console.log('Model is different on ' + inputs[i] + ':', self.model()[inputs[i]](), self.originalModel()[inputs[i]]());
							return true;
						}
					}
					
					// Phone is a special case because sometimes it compares the formatted phone vs unformatted phone
					var oldPhone = ('' + originalModel.serviceNotificationPhoneNumber).replace(/[^\d]/g, ''),
						newPhone = ('' + self.model().serviceNotificationPhoneNumber()).replace(/[^\d]/g, '');
					
					if (oldPhone != newPhone)
					{
						//console.log('Model is different on phone:', newPhone, oldPhone);
						return true;
					}
				}
				
				if (self.model().isSchedulingPreferencesEnabled())
				{
					var changedPreference = self.scheduleDays.some(function(dayKey)
					{
						return self.scheduleTimes.some(function(timeKey)
						{
							var originalPreference = originalModel.scheduleDayTimePreferences && originalModel.scheduleDayTimePreferences[dayKey] && !!originalModel.scheduleDayTimePreferences[dayKey][timeKey];
							
							return self.scheduleDayTimePreferences[dayKey][timeKey]() != originalPreference;
						});
					});
					
					if (changedPreference)
						return true;
					
					if (self.model().scheduleWeekOfMonth() != originalModel.scheduleWeekOfMonth)
						return true;
				}
				
				return false;
			};
			
			self.Submit = function()
			{
				if (!self.HasModelChanged() || self.submitting())
					return;
				
				self.RemoveAllErrors();
				
				var json = ko.mapping.toJS(self.model);
				
				// Copy over day time preferences object
				json.scheduleDayTimePreferences = ko.mapping.toJS(self.scheduleDayTimePreferences);

				self.QAS.isChoosingEmail(false);
				
				if (self.model().serviceNotificationPreferenceEmail() && !self.QAS.IsLastValidatedEmail())
				{
					self.QAS.isVerifying(true);
					self.submitting(true);
					
					QAS_VerifyEmail(self.model().serviceNotificationEmail(), function(data)
					{
						self.submitting(false);
						self.QAS.isVerifying(false);
						
						if (data.model && data.model.corrections && data.model.corrections.length)
						{
							self.QAS.emails(data.model.corrections);
							self.QAS.selectedEmail(null);
							self.QAS.isChoosingEmail(true);
						}
						else if (data.model && data.model.errorMessages && data.model.errorMessages.length)
						{
							self.model().errorMessages(data.model.errorMessages);
						}
						else
						{
							self.QAS.KeepEmail();
						}
					});
				}
				else
				{
					self.submitting(true);
					
					CallDWR('MyAccountTMXContactInfoUIUtils/setPreferences', json, function(data)
					{
						self.submitting(false);
						
						var hasError = false;
						var scrollToFieldErrors = false;
						
						if (data.error)
						{
							hasError = true;
						}
						else if (data.model)
						{
							if (data.model.errorMessages && data.model.errorMessages.length)
							{
								hasError = true;
								data.model.errorMessages = [];
							}
							else if (data.model.fieldValidationErrors && data.model.fieldValidationErrors.length)
							{
								scrollToFieldErrors = true;
							}
							
							self.UpdateModel(data.model);
						}
						
						if (hasError)
						{
							CreateAlert(
							{
								color: 'red',
								message: "Sorry, something went wrong. Please try again.",
								button: false,
								closeButton: true,
								scrollToAlerts: true,
								$module: $module
							});
						}
						
						if (scrollToFieldErrors)
						{
							var $errorInput = $module.find('.error');
							if ($errorInput.length)
							{
								ScrollTo(Math.max(0, $errorInput.offset().top - 20), 400);
							}
						}
						
						if (self.model().redirectURL())
						{
							setTimeout(function()
							{
								Redirect(self.model().redirectURL());
								
							}, 1500);
							return;
						}
						
						// Success message should be an alert
						if (self.model().informationalMessages().length)
						{
							var messages = ko.mapping.toJS(self.model().informationalMessages);
							
							self.model().informationalMessages.removeAll();
							
							messages.forEach(function(message, messageIndex)
							{
								CreateAlert(
								{
									color: 'green',
									message: message.infoMessage,
									messageIsHTML: true,
									button: false,
									closeButton: true,
									scrollToAlerts: messageIndex == 0,
									$module: $module
								});
							});
						}
					});
				}
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});