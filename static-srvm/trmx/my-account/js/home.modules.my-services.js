/*
 * Module: My Services
 ***********************************************************************************************************/
TAB.Modules.Add('my-services', function($module, options, onLoaded)
{
	// testing flags
	var TESTING_FLAG_CALENDAR = 1;
	var TESTING_FLAGS = 0; // Set to 0 to disable any testing
	
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,
		
		api: {
			setLiveChatEnabled: function(self, isLiveChatEnabled) {
				self.isLiveChatEnabled = isLiveChatEnabled;
			}
		},
		
		LoadDataDWR: 'my-account-json/getMyServicesData.json',
		
		callback: function(self)
		{
			self.isLiveChatEnabled = false;
			
			self.GetAddressHeader = function()
			{
				if (!self.model() || !self.model().address)
					return '';
				
				var address = self.model().address.address2();
				
				if (address) address += ', ';
				
				return address + self.model().address.address1();
			};
			
			var UpdateModel = self.UpdateModel;
	
			function PrepareService(item)
			{
				// For showing service detail view
				item.showDetail = false;
				item.guaranteeCertificateUrl = item.guaranteeCertificateUrl || '';
				
				// This is what is used to know the selected time since time is now an object with a time and employee number
				item.rescheduleSelectedTimeObject = null;
				
				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR) {
					item.rescheduleLoaded = true;
				}
				
				item.months = [];
				item._original = $.extend({}, item);
			}
			
			self.UpdateModel = function(json)
			{
				// Add properties that aren't on the back-end
				json.services.forEach(PrepareService);
				
				UpdateModel.call(self, json);
				
				// Mobile support for switching steps and scrolling user to top of new step
				self.model().services().forEach(function(item, index) {
					item.rescheduleStep.subscribe(function(step) {
						if (Viewport.isMobile()) {
							var $row = $module.find('table:visible tr.summary').eq(index);
							
							// If on a step, then scroll to the next row which is the "detail" view
							if (step) {
								$row = $row.next();
							}
							
							var pos = $row.offset() && $row.offset().top || 0;
							
							ScrollTo(pos, 'fast');
						}
					});
				});

				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR) {
					self.model().services().forEach(function(item) {
						self.CreateRescheduleMonths(item);
					});
				}
			};
			
			self.ShowDetail = function(item)
			{
				item.showDetail(!item.showDetail() || item.rescheduleStep() != '');
			};
			
			self.GetModelJSON = function(item)
			{
				var json = ko.mapping.toJS(item);
				
				// Format the rescheduleSelectedDate for the server
				if (json.rescheduleSelectedDate && json.rescheduleSelectedDate.date)
				{
					var date = new Date(json.rescheduleSelectedDate.date);
					
					json.rescheduleSelectedDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
				}
				
				// Reset time variables
				json.rescheduleSelectedTime = null;
				json.rescheduleSelectedEmployeeNumber = null;
				
				// Check if a time object is chosen to populate time variables
				var rescheduleSelectedTimeObject = ko.mapping.toJS(item.rescheduleSelectedTimeObject);
				
				if (rescheduleSelectedTimeObject)
				{
					json.rescheduleSelectedTime = rescheduleSelectedTimeObject.time;
					json.rescheduleSelectedEmployeeNumber = rescheduleSelectedTimeObject.employeeNumber;
				}
				
				return json;
			};
			
			var scheduledServices = ko.observableArray();
			
			CallDWR('my-account-json/getScheduledServicesData.json', '', function(data){
			
				if (data.model && !data.model.errors && data.model.services) {
					data.model.services.forEach(function(service) {
						scheduledServices.push(service);
					});
				} else {
					scheduledServices.push({});
				}
			});

			self.ServiceScheduledSoon = function(item) {
				if (!GetFeatureToggleDefaultTrue("restrictRescheduleByDate"))
					return false;
				
				if (scheduledServices().length > 0) {
					var isServiceScheduledSoon = false;
					var date = new Date();
					var today = date.toLocaleDateString("en-US");

					date.setDate(date.getDate() + 1);
					
					var tomorrow = date.toLocaleDateString("en-US");
					
					scheduledServices().forEach(function(service) {
						var serviceDate = new Date(service.date);
						serviceDate = serviceDate.toLocaleDateString("en-US");
						
						if (service.serviceLine === item.serviceLine() && (serviceDate === today || serviceDate === tomorrow)) {
							isServiceScheduledSoon = true;
						}
					});
					
					return isServiceScheduledSoon;
				} else {
					return true;
				}
			};
			
			self.CanScheduleFollowup = function(item) {
				return item.canReschedule();
			};

			var DateKey = function(date)
			{
				return date ? (date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate()) : '';
			};
			
			self.CreateRescheduleMonths = function(service)
			{
				if (ko.isObservable(service.months))
				{
					service.months([]);
				}
				else
				{
					service.months = ko.observableArray();
				}

				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR && (!service.rescheduleDates() || !service.rescheduleDates().length)) {
					service.rescheduleDates((function() {
						var dates = [];
						var date = new Date(), currentTime = date.getTime(), currentMonth = date.getMonth(), daysIntoMonth = 15;
						var availableTimeSlots = [ 8, 10, 12, 14, 16, 18 ];
	
						// Enough days to span more than 1 month
						do {
							date.setTime(date.getTime() + 86400000);
	
							if (date.getDay() == 0 || date.getDay() == 6)
								continue;
	
							var times = availableTimeSlots.slice();
	
							for (var t = Math.floor(Math.random() * availableTimeSlots.length); t > 0; t--) {
								times.splice(Math.floor(Math.random() * times.length), 1);
							}
	
							dates.push(ko.mapping.fromJS({
								times : times.map(function(time) {
									return {
										time : time,
										employeeNumber : '12345'
									};
								}),
								date : (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
							}));
						} while (date.getMonth() == currentMonth || date.getTime() - (86400000 * 14) < currentTime || date.getDate() < daysIntoMonth);
						
						service.rescheduleTimes(availableTimeSlots.filter(function(timeSlot) {
							return dates.some(function(dateObject) {
								return dateObject.times().some(function(dateTime) {
									return dateTime.time() == timeSlot;
								});
							});
						}));
	
						return dates;
	
					})());
				}
				
				if (!service.rescheduleDates() || !service.rescheduleDates().length)
					return;

				var dates = [],
					months = {},
					monthsArray = [],
					firstDate = null,
					lastDate = null,
					availableDates = {};
				
				var DayItem = function(data)
				{
					var item = $.extend({
						text: '',
						date: null,
						available: false,
						times: []
					}, data);
					
					var key = DateKey(item.date);
					
					if (key in availableDates)
					{
						item.available = true;
						item.times = availableDates[key].times;
					}
					
					return item;
				};
				
				service.rescheduleDates().forEach(function(d)
				{
					var datePieces = d.date().split('/');
					var date = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
					
					if (firstDate == null || firstDate > date)
						firstDate = new Date(date);
					
					if (lastDate == null || lastDate < date)
						lastDate = new Date(date);
					
					dates.push(date);
					
					availableDates[DateKey(date)] = ko.mapping.toJS(d);
				});
				
				dates.forEach(function(date)
				{
					if (!(date.getUTCMonth() in months))
					{
						var month = {
							date: new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), 1)),
							monthName: GetMonthName(date.getUTCMonth() + 1),
							weeks: []
						};
						
						// Build first week
						var d = new Date(month.date);
						var week = { days: [] };
						
						for (var day = 0; day < d.getUTCDay(); day++)
						{
							week.days.push(DayItem());
						}
						
						var thisMonth = d.getUTCMonth();
						
						do
						{
							// Loop through the end of the week
							do
							{
								if (d.getUTCMonth() == thisMonth)
								{
									week.days.push(DayItem({
										text: d.getUTCDate(),
										date: new Date(+d)
									}));
								}
								else
								{
									week.days.push(DayItem());
								}
								
								d.setTime(d.getTime() + 86400000);
							}
							while (d.getUTCDay() > 0);
							
							// Check if this week has any available days
							var hasAvailable = week.days.some(function(day) { return day.available; }),
								firstWeekDay = week.days.find(function(day) { return !!day.date });
							
							if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate)
							{
								// Add week for month
								month.weeks.push(week);
							}
							
							// Reset week
							week = { days: [] };
						}
						while (d.getUTCMonth() == thisMonth);
						
						// Store month
						months[thisMonth] = month;
						monthsArray.push(ko.mapping.fromJS(month));
					}
				});
				
				monthsArray.sort(function(a, b)
				{
					return a.date() < b.date() ? -1 : 1;
				});
				
				service.months(monthsArray);
			};
			
			self.Reschedule = function(item, e)
			{
				if (item.submitting()) return;
				
				if (!item.rescheduleLoaded())
				{
					var json = self.GetModelJSON(item);
					
					item.submitting(true);
					
					CallDWR('my-account-json/scheduleAdditional.json', json, function(data)
					{
						item.submitting(false);
						
						if (data.error)
						{
							item.errorMessages.push({ errorMessage: data.error });
							item.rescheduleStep('error');
							return;
						}
						
						// Update item model
						self.ApplyModelChanges(item, data.model);
						
						// Create the calendars needed with the reschedule dates
						if (item.rescheduleStep() == 'appointment')
						{
							self.CreateRescheduleMonths(item);
						}
						
						// Set it as loaded
						item.rescheduleLoaded(item.rescheduleStep() != 'error');
					});
				}
				else if (item.months().length)
				{
					item.rescheduleStep('appointment');
				}
				else if (item.rescheduleDates())
				{
					// Create the calendars needed with the reschedule dates
					self.CreateRescheduleMonths(item);
					
					item.rescheduleStep('appointment');
				}
				else
				{
					item.rescheduleStep('contact');
				}
			};
			
			self.SetScheduleDay = function(service, day)
			{
				if (day.date() && day.available())
				{
					service.rescheduleSelectedDate(day);
					service.rescheduleSelectedTimeObject(null);
				}
			};
			
			self.GetScheduleDayCSS = function(service, day)
			{
				return {
					disabled: !day.date() || !day.available(),
					active: day.date() && service.rescheduleSelectedDate() && DateKey(service.rescheduleSelectedDate().date()) == DateKey(day.date())
				};
			};
			
			self.IsScheduleTimeAvailable = function(service, time)
			{
				if (!service.rescheduleSelectedDate())
					return false;
				
				var matchingTime = ko.utils.arrayFirst(service.rescheduleSelectedDate().times(), function(t)
				{
					return t.time() == time;
				});
				
				return !!matchingTime;
			};
			
			self.GetRescheduleTimes = function(service)
			{
				var timeObjects = [];
				
				if (service.rescheduleSelectedDate())
				{
					service.rescheduleTimes().forEach(function(time)
					{
						var timeObject = ko.utils.arrayFirst(service.rescheduleSelectedDate().times(), function(t)
						{
							return t.time() == time;
						});
						
						timeObjects.push(timeObject || ko.mapping.fromJS({
							time: time,
							employeeNumber: null
						}));
					});
				}
				else
				{
					service.rescheduleTimes().forEach(function(time)
					{
						timeObjects.push(ko.mapping.fromJS({
							time: time,
							employeeNumber: null
						}));
					});
				}
				
				return timeObjects;
			};
			
			self.GetScheduleTimeDisplay = (function()
			{
				var FormatHour = function(hour)
				{
					return (hour > 12 ? hour - 12 : hour) + (hour < 12 ? 'AM' : 'PM');
				};
				
				return function(time)
				{
					if (!time) return '';
					
					if (time.hasOwnProperty('time'))
						time = time.time;
					
					if (ko.isObservable(time))
						time = time();
					
					return FormatHour(time) + ' - ' + FormatHour(parseInt(time) + 2);
				};
			})();
			
			self.SetNoRescheduleDay = function(service)
			{
				service.rescheduleSelectedDate(null);
				service.rescheduleSelectedTimeObject(null);
				service.rescheduleStep('contact');
				
				if (self.isLiveChatEnabled && GetFeatureToggleDefaultFalse("proactiveChatScheduleCFR")) {
					LiveChat_OpenChat();
				}
			};
			
			self.GetScheduleDateDisplay = function(service, verbose)
			{
				if (service.rescheduleSelectedDate() && service.rescheduleSelectedTimeObject())
				{
					var date = service.rescheduleSelectedDate().date();
					
					if (date)
					{
						if (verbose)
						{
							return GetMonthName(date.getUTCMonth() + 1) + ' ' + date.getUTCDate();
						}
						
						return (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
					}
				}
				
				return '';
			};
			
			self.CancelRescheduleStep = function(item)
			{
				self.RemoveAllErrors(item);
				
				switch (item.rescheduleStep())
				{
				case 'error':
					item.rescheduleStep('');
					break;
					
				case 'appointment':
					item.rescheduleStep('');
					item.rescheduleSelectedDate(null);
					item.rescheduleSelectedTimeObject(null);
					break;
				
				case 'details':
					item.rescheduleStep('appointment');
					break;
				
				case 'contact':
					if (item.months().length)
					{
						item.rescheduleStep('appointment');
					}
					else
					{
						item.rescheduleStep('');
					}
					
					item.rescheduleContactFname(item._original.rescheduleContactFname());
					item.rescheduleContactLname(item._original.rescheduleContactLname());
					item.rescheduleContactPhone(item._original.rescheduleContactPhone());
					item.rescheduleContactDate('');
					item.rescheduleContactTime('');
					break;
				}
			};
			
			var currentDate = new Date(), otherDate;
			
			self.DatepickerOptions = {
				minDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 1) && otherDate,
				maxDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 45) && otherDate,
				beforeShowDay: function(date) {
					// Has to return an array of [isSelectable, CSS class name string for date cell, optional popup tooltip]
					// For date.getDay(), 0 = Sunday
					return [date.getDay() > 0, ''];
				}
			};
			
			self.IsValidContact = function(item)
			{
				return item.rescheduleContactFname() && item.rescheduleContactLname() && item.rescheduleContactPhone() && item.rescheduleContactDate() && item.rescheduleContactTime();
			};
			
			self.ConfirmContact = function(item, e)
			{
				if (item.submitting() || !self.IsValidContact(item)) return;
				
				self.RemoveAllErrors(item);
				
				var json = self.GetModelJSON(item);
				
				item.submitting(true);
				
				CallDWR('my-account-json/scheduleAdditional.json', json, function(data)
				{
					item.submitting(false);
					
					if (data.error)
					{
						item.errorMessages.push({ errorMessage: data.error });
						return;
					}
					
					// Update model
					self.ApplyModelChanges(item, data.model);
					
					// Go to next step if no errors
					if (!item.errorMessages().length && !item.fieldValidationErrors().length)
					{
						item.rescheduleStep('confirmationContact');
					}
				});
			};
			
			self.IsValidReschedule = function(item)
			{
				return item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject();
			};
			
			function ConfirmDetails(item, json, callback)
			{
				json = json || self.GetModelJSON(item);
				
				item.submitting(true);
				
				CallDWR('my-account-json/scheduleAdditional.json', json, function(data)
				{
					item.submitting(false);
					
					if (data.error)
					{
						json.errorMessages = [{ errorMessage: data.error }];
						
						if (typeof callback == 'function')
						{
							callback(json);
						}
						return;
					}
					
					// Ignore rescheduleSelectedDate being set
					if (data.model && 'rescheduleSelectedDate' in data.model)
					{
						delete data.model['rescheduleSelectedDate'];
					}
					
					if (typeof callback == 'function')
					{
						callback(data.model);
					}
				});
			}
			
			self.ConfirmAppointment = function(item, e)
			{
				if (item.submitting() || !self.IsValidReschedule(item)) return;
				
				self.RemoveAllErrors(item);
				
				if (!item.rescheduleSelectedDate() || !item.rescheduleSelectedTimeObject())
				{
					item.errorMessages.push({ errorMessage: "Please choose an appointment date and time." });
				}
				else if (item.serviceLine() == "NMOSQ")
				{
					var json = self.GetModelJSON(item);
					
					json.rescheduleStep = "details";
					
					ConfirmDetails(item, json, function(model)
					{
						// Keep user on appointment step until the error check a few lines below
						model.rescheduleStep = "appointment";
						
						// Update model
						self.ApplyModelChanges(item, model);
						
						// Go to next step if no errors
						if (!item.errorMessages().length && !item.fieldValidationErrors().length)
						{
							item.rescheduleStep('confirmationAppointment');
							
							OnConfirmedAppointment(item, model);
						}
					});
				}
				else
				{
					item.rescheduleStep('details');
				}
			};
			
			self.ConfirmDetails = function(item, e)
			{
				if (item.submitting()) return;
				
				self.RemoveAllErrors(item);
				
				ConfirmDetails(item, null, function(model)
				{
					// Update model
					self.ApplyModelChanges(item, model);
					
					// Go to next step if no errors
					if (!item.errorMessages().length && !item.fieldValidationErrors().length)
					{
						item.rescheduleStep('confirmationAppointment');
						
						OnConfirmedAppointment(item, model);
					}
				});
			};
			
			function OnConfirmedAppointment(item, model)
			{
				// Delete the updated services from the item, for cleanup purposes
				if (ko.isObservable(item.updatedMyServices)) {
					item.updatedMyServices([]);
				}
				
				// Update my services with item.updatedMyServices
				if (model.updatedMyServices && model.updatedMyServices.length) {
					model.updatedMyServices.forEach(function(service) {
						// Find the item to update in the model
						var updateItem = self.model().services().find(function(updateItem) {
							return updateItem.serviceNumber() == service.serviceNumber;
						});
						
						if (updateItem) {
							// Remove information from the service that we don't want to update the item with
							var fieldsToNotUpdate = ['rescheduleSelectedDate', 'rescheduleStep'];
							
							fieldsToNotUpdate.forEach(function(field) {
								if (service.hasOwnProperty(field)) {
									delete service[field];
								}
							});
							
							// Update the item with the new service information
							self.ApplyModelChanges(updateItem, service);
						} else {
							// Add to My Services
							PrepareService(service);
							
							self.model().services.push(ko.mapping.fromJS(service));
						}
					});
				}
				
				TAB.Modules.Reload("scheduled-services");
			}
			
			self.CloseConfirmation = function(item)
			{
				item.rescheduleStep('');
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});