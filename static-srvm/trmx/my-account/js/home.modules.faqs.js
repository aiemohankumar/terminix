/*
 * Module: FAQs
 ***********************************************************************************************************/
TAB.Modules.Add('faqs', function($module, options, onLoaded)
{
	$module.find('.expandable-items > li').each(function()
	{
		var $container = $(this),
			$questionCheckbox = $container.find('.expand-header :checkbox'),
			$answer = $container.find('.expand-content');
		
		var UpdateShowAnswer = function()
		{
			$answer.toggle($questionCheckbox.prop('checked'));
		};
		
		$questionCheckbox.on('change', function(e)
		{
			UpdateShowAnswer();
		});
		
		UpdateShowAnswer();
	});
	
	onLoaded();
});