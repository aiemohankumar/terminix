function ViewModel() {
    var self = this;

    self.model = ko.observable(null);
    self.modelLoaded = ko.observable(false);
    self.submitted = ko.observable(false);

    ViewModel_AddCommonFunctions(self);

    self.LoadData = function () {
        CallDWR('MerryMaidsLoginUIUtils/getLoginData', null, function (data) {
            if (data.error) {
                alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
                return;
            }

            var json = data.model;

            // Check for prepopulating with login from cookie
            json.login = GetCookie("forgotPasswordUsername") || json.login;

            self.model(ko.mapping.fromJS(json));

            if (self.model().redirectURL()) {
                Redirect(self.model().redirectURL());
                return;
            }

            self.modelLoaded(true);

            self.UpdateAutoFocus();
        });
    };

    self.IsValidForm = function () {
        if (!IsValidEmail(self.model().login()))
            return false;

        return true;
    };

    self.Submit = function (m, e) {
        if (!self.IsValidForm() || self.submitted() || self.submitting()) return;

        self.RemoveAllErrors();

        self.submitting(true);

        var json = ko.mapping.toJS(self.model);

        CallDWR('MerryMaidsLoginUIUtils/forgotPassword', json, function (data) {
            if (data.error) {
                self.submitting(false);

                self.model().errorMessages.push({
                    errorMessage: data.error
                });
                return;
            }

            json = data.model;

            // DUMMY DATA
            //json.informationalMessages.push({ infoMessage: 'An e-mail has been sents to ' + json.login + ' with instructions on how to reset your password.' });

            self.model(ko.mapping.fromJS(json));

            if (self.model().redirectURL()) {
                setTimeout(function () {
                    Redirect(self.model().redirectURL());

                }, 1500);
                return;
            }

            self.submitting(false);

            self.submitted(self.model().errorMessages().length == 0 && self.model().fieldValidationErrors().length == 0);

            self.UpdateAutoFocus();
        });
    };
}

$(function () {
    window.vm = new ViewModel();

    ko.applyBindings(vm);

    vm.LoadData();
});
