/*
 * List of DWR calls in MyAccount
 *
 * 'MyAccountTMXContactInfoUIUtils/getContactInfo'
 * 'MyAccountTMXContactInfoUIUtils/changeContactInfo'
 * 'MyAccountTMXContactInfoUIUtils/getContactInfoSmall'
 * 'MyAccountTMXContactInfoUIUtils/changeContactInfoSmall'
 * 'MyAccountTMXPasswordUIUtils/getPasswordModel'
 * 'MyAccountTMXPasswordUIUtils/setPassword'
 * 'MyAccountTMXPaymentUIUtils/getPaymentDueData'
 * 'MyAccountTMXPaymentUIUtils/makePayment'
 * 'MyAccountTMXPaymentUIUtils/getStoredPaymentMethods'
 * 'MyAccountTMXPaymentUIUtils/addStoredPaymentMethod'
 * 'MyAccountTMXPaymentUIUtils/editStoredPaymentMethod'
 * 'MyAccountTMXPaymentUIUtils/getPaymentHistory'
 * 'MyAccountTMXServicesUIUtils/getScheduledServicesData'
 * 'MyAccountTMXServicesUIUtils/rescheduleService'
 * 'MyAccountTMXServicesUIUtils/updateNoteToTechnician'
 * 'MyAccountTMXServicesUIUtils/getMyServicesData'
 * 'MyAccountTMXServicesUIUtils/scheduleAdditional'
 * 'MyAccountTMXServicesUIUtils/getMyServiceHistory'
 *
 */
(function($)
{
	// ViewModel to make dummy models
	var vm = {};
	ViewModel_AddCommonFunctions(vm);
	
	// Helper functions
	var Address = function(data)
	{
		this.address1 = '';
		this.address2 = '';
		this.city = '';
		this.state = '';
		this.zipcode = '';
		
		$.extend(this, data);
	};
	
	var GetStoredPaymentMethods = function(addDefault)
	{
		var storedPaymentMethods = [];
		
		storedPaymentMethods.push(new PaymentMethod(
		{
			type: (addDefault ? 'saved' : 'card'),
			description: 'VISA 1234',
		}));
		
		if (addDefault)
		{
			storedPaymentMethods.push(new PaymentMethod(
			{
				type: 'card',
				description: 'Credit Card'
			}));
			
			storedPaymentMethods.push(new PaymentMethod(
			{
				type: 'bank',
				description: 'Bank Account'
			}));
		}
		
		return storedPaymentMethods;
	};
	
	var PaymentMethod = function(data)
	{
		this.type = '';
		this.description = '';
		this.customerNumber = '';
		this.paymentMethodId = '';
		this.modifyStep = '';
		this.removeError = '';
		this.visibleHelp = '';
		this.cardNumber = '';
		this.cardType = '';
		this.cardSecurity = '';
		this.cardExpireMonth = '';
		this.cardExpireYear = '';
		this.bankRoutingNumber = '';
		this.bankAccountNumber = '';
		this.billingAddress1 = '';
		this.billingAddress2 = '';
		this.billingCity = '';
		this.billingState = '';
		this.billingZipcode = '';
		
		$.extend(this, vm.CreateDummyModel(data));
	};
	
	var EasyPayModel = function(data)
	{
		this.hasEnrolled = false;
		this.salesAgreements = [];
		this.storedPaymentMethods = [];
		this.enrollPaymentMethod = null;
		this.acceptedTerms = false;
		
		$.extend(this, vm.CreateDummyModel(data));
	};
	
	var EasyPaySalesAgreement = function(data)
	{
		this.customerNumber = '';
		this.salesAgreementId = '';
		this.salesAgreementDescription = '';
		this.paymentMethod = new PaymentMethod();
		this.isActive = false;
		
		$.extend(this, vm.CreateDummyModel(data));
	};
	
	var GetContactInfoModel = function()
	{
		return vm.CreateDummyModel(
		{
			editing: false,
			updateAll: false,
			firstName: 'John',
			lastName: 'Doe',
			address1: '123 Main St.',
			address2: 'Suite 201',
			city: 'Memphis',
			state: 'TN',
			zipcode: '38125',
			phone: '1234567890',
			email: 'john.doe@noemail.com',
			svcAddress: new Address(
			{
				address1: '123 Main St.',
				address2: 'Suite 201',
				city: 'Memphis',
				state: 'TN',
				zipcode: '38125'
			})
		});
	};
	
	// All overrides
	OverrideDWR('my-account-json/getContactInfo.json', function(data, callback)
	{
		callback(GetContactInfoModel());
	});
	
	OverrideDWR('MyAccountTMXContactInfoUIUtils/changeContactInfo', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		response.informationalMessages.push({ infoMessage: "Successfully updated contact information." });
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXContactInfoUIUtils/getContactInfoSmall', function(data, callback)
	{
		callback(GetContactInfoModel());
	});
	
	OverrideDWR('MyAccountTMXContactInfoUIUtils/changeContactInfoSmall', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		response.informationalMessages.push({ infoMessage: "Successfully updated contact information." });
		
		callback(response);
	});
	
	OverrideDWR('my-account-json/getStoredPaymentMethods.json', function(data, callback)
	{
		callback(vm.CreateDummyModel(
		{
			storedPaymentMethods: GetStoredPaymentMethods(),
			addPaymentMethod: new PaymentMethod()
		}));
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/addStoredPaymentMethod', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		response.informationalMessages.push({ infoMessage: "Successfully added stored payment." });
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/editStoredPaymentMethod', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		response.informationalMessages.push({ infoMessage: "Successfully updated stored payment." });
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/getPaymentDueData', function(data, callback)
	{
		var response = vm.CreateDummyModel(
		{
			duePayments: [],
			totalAmountDue: 0.0,
			paymentMethods: GetStoredPaymentMethods(true),
			selectedPaymentMethod: new PaymentMethod(),
			visibleHelp: '',
			cardNumber: '',
			cardType: '',
			cardSecurity: '',
			cardExpireMonth: '',
			cardExpireYear: '',
			bankRoutingNumber: '',
			bankAccountNumber: '',
			billingAddress1: '',
			billingAddress2: '',
			billingCity: '',
			billingState: '',
			billingZipcode: '',
			setupEasyPay: false,
			paymentTotal: 0.0,
			paymentSuccess: false,
			savePaymentMethod: false
		});
		
		response.duePayments.push(
		{
			description: '',
			dueDate: '',
			amountDue: 0.0,
			amount: '',
			showDetail: false,
			serviceDate: '',
			invoiceDate: '',
			technician: '',
			phone: '',
			invoiceNumber: '',
			customerNumber: '',
			invoiceTicketUrl: '',
			invoiceAddress: new Address()
		});
		
		response.totalAmountDue = response.duePayments.reduce(function(prev, duePayment)
		{
			return prev + duePayment.amountDue;
		}, 0);
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/makePayment', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		response.informationalMessages.push({ infoMessage: "Your payment is being processed." });
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/getPaymentHistory', function(data, callback)
	{
		callback(vm.CreateDummyModel(
		{
			numViewable: 5,
			history: [
			{
				date: '',
				description: '',
				balance: '',
				status: ''
			}]
		}));
	});
	
	OverrideDWR('my-account-json/getScheduledServicesData.json', function(data, callback)
	{
		callback(vm.CreateDummyModel(
		{
			services: [vm.CreateDummyModel(
			{
				address: '',
				city: '',
				state: '',
				postalCode: '',
				date: '',
				description: '',
				time: '',
				startTime: '',
				endTime: '',
				duration: '',
				price: 0.0,
				technician: '',
				technicianNumber: '',
				phone: '',
				showDetail: false,
				noteForTechnician: '',
				workOrderActivityId: '',
				rescheduleDate: '',
				rescheduleFromTime: '',
				rescheduleToTime: '',
				serviceLine: '',
				submitting: false,
				rescheduleLoaded: false,
				rescheduleStep: '',
				rescheduleDates: [],
				rescheduleTimes: [],
				rescheduleSelectedDate: '',
				rescheduleSelectedTime: '',
				rescheduleSelectedEmployeeNumber: '',
				rescheduleContactFname: '',
				rescheduleContactLname: '',
				rescheduleContactPhone: '',
				rescheduleContactDate: '',
				rescheduleContactTime: ''
			})]
		}));
	});
	
	OverrideDWR('MyAccountTMXServicesUIUtils/rescheduleService', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		switch (response.rescheduleStep)
		{
			case '':
			{
				if (!response.rescheduleLoaded)
				{
					response.rescheduleDates = [];
					
					var currentDate = new Date();
					var validTimes = [8, 10, 12, 14, 16];
					var validEmployeeNumbers = [1, 2, 3];
					
					for (var dateNum = 0; dateNum < 10; dateNum++)
					{
						var rescheduleDate = {};
						
						rescheduleDate.date = (currentDate.getMonth() + 1) + '/' + currentDate.getDate() + '/' + currentDate.getFullYear();
						
						rescheduleDate.times = validTimes.filter((time) => Math.random() < 0.5).map(function(time)
						{
							return { time: time, employeeNumber: validEmployeeNumbers[Math.floor(Math.random() * validEmployeeNumbers.length)] };
						});
						
						response.rescheduleDates.push(rescheduleDate);
					}
					
					response.rescheduleTimes = validTimes.filter(function(time)
					{
						return response.rescheduleDates.some(function(rescheduleDate)
						{
							return rescheduleDate.times.some(function(rescheduleTime)
							{
								return rescheduleTime == time;
							});
						});
					});
				}
				break;
			}
			case 'appointment':
			{
				response.informationalMessages.push({ infoMessage: "Successfully rescheduled service." });
				break;
			}
			case 'contact':
			{
				response.informationalMessages.push({ infoMessage: "You will be contacted soon to set up an appointment." });
				break;
			}
		}
		
		callback(response);
	});
	
	OverrideDWR('my-account-json/getMyServicesData.json', function(data, callback)
	{
		callback(vm.CreateDummyModel(
		{
			address: new Address(),
			services: [vm.CreateDummyModel(
			{
				serviceNumber: '',
				customerNumber: '',
				description: '',
				yearsServiced: '',
				expirationDate: '',
				effectiveDate: '',
				serviceLine: '',
				pestControl: false,
				workOrderActivityId: '',
				scanMasterUrl: '',
				submitting: false,
				canReschedule: false,
				rescheduleLoaded: false,
				rescheduleStep: '',
				rescheduleDates: [],
				rescheduleTimes: [],
				rescheduleSelectedDate: '',
				rescheduleSelectedTime: '',
				rescheduleSelectedEmployeeNumber: '',
				rescheduleContactFname: '',
				rescheduleContactLname: '',
				rescheduleContactPhone: '',
				rescheduleContactDate: '',
				rescheduleContactTime: '',
				reschedulePestsText: '',
				reschedulePests: [],
				rescheduleLocationsText: '',
				rescheduleLocations: [],
				rescheduleNote: ''
			})]
		}));
	});
	
	OverrideDWR('my-account-json/scheduleAdditional.json', function(data, callback)
	{
		var response = $.extend(true, {}, data);
		
		switch (response.rescheduleStep)
		{
			case '':
			{
				if (!response.rescheduleLoaded)
				{
					response.rescheduleDates = [];
					
					var currentDate = new Date();
					var validTimes = [8, 10, 12, 14, 16];
					var validEmployeeNumbers = [1, 2, 3];
					
					for (var dateNum = 0; dateNum < 10; dateNum++)
					{
						var rescheduleDate = {};
						
						rescheduleDate.date = (currentDate.getMonth() + 1) + '/' + currentDate.getDate() + '/' + currentDate.getFullYear();
						
						rescheduleDate.times = validTimes.filter((time) => Math.random() < 0.5).map(function(time)
						{
							return { time: time, employeeNumber: validEmployeeNumbers[Math.floor(Math.random() * validEmployeeNumbers.length)] };
						});
						
						response.rescheduleDates.push(rescheduleDate);
					}
					
					response.rescheduleTimes = validTimes.filter(function(time)
					{
						return response.rescheduleDates.some(function(rescheduleDate)
						{
							return rescheduleDate.times.some(function(rescheduleTime)
							{
								return rescheduleTime == time;
							});
						});
					});
				}
				break;
			}
			case 'details':
			{
				response.informationalMessages.push({ infoMessage: "Successfully scheduled additional service." });
				break;
			}
			case 'contact':
			{
				response.informationalMessages.push({ infoMessage: "You will be contacted soon to set up an appointment." });
				break;
			}
		}
		
		callback(response);
	});
	// https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/getMyServiceHistory.json
	OverrideDWR('MyAccountTMXServicesUIUtils/getMyServiceHistory', function(data, callback)
	{
	
		callback(vm.CreateDummyModel(
		{
			numViewable: 5,
			history: [vm.CreateDummyModel(
			{
				date: '',
				description: '',
				balance: 0.0,
				status: '',
				technician: '',
				technicianNumber: '',
				phone: '',
				customerNumber: '',
				showDetail: false,
				salesAgreementNumber: '',
				serviceLine: '',
				workOrderActivityId: '',
				fullTicketUrl: ''
			})]
		}));
	});
	
	OverrideDWR('my-account-json/getEasyPayModel.json', function(data, callback)
	{
		callback(new EasyPayModel(
		{
			hasEnrolled: true,
			salesAgreements: [
  			    new EasyPaySalesAgreement({ salesAgreementDescription: 'Interior Pest' }),
			    new EasyPaySalesAgreement({ salesAgreementDescription: 'General Pest' }),
			    new EasyPaySalesAgreement(
			    {
			    	salesAgreementDescription: 'Exterior Pest',
			    	paymentMethod: new PaymentMethod({ type: 'saved', description: 'VISA 1234', paymentMethodId: 1 }),
			    	isActive: true
			    })
			],
			storedPaymentMethods: [
   			    new PaymentMethod({ type: 'saved', description: 'VISA 1234', paymentMethodId: 1 }),
			    new PaymentMethod({ type: 'card', description: 'New credit card' }),
			    new PaymentMethod({ type: 'bank', description: 'New bank account' }),
			],
			enrollPaymentMethod: null,
			acceptedTerms: false
		}));
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/enrollEasyPay', function(data, callback)
	{
		var response = new EasyPayModel(data);
		
		if (response.salesAgreements.filter((item) => item.isActive).length == 0)
		{
			response.errorMessages.push({ errorMessage: "You must choose a sales agreement to enroll in AutoPay." });
		}
		else if (!response.enrollPaymentMethod)
		{
			response.errorMessages.push({ errorMessage: "You must choose a payment method to enroll in AutoPay." });
		}
		else if (!response.acceptedTerms)
		{
			response.errorMessages.push({ errorMessage: "You must accept to the terms and conditions to enroll in AutoPay." });
		}
		else if (response.enrollPaymentMethod.type == 'card')
		{
			if (!response.enrollPaymentMethod.cardNumber)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'cardNumber', errorMessage: 'Field is required.' });
			}
			else if (!response.enrollPaymentMethod.cardType)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'cardNumber', errorMessage: 'The entered credit card is not recognized as a valid credit card.' });
			}
			
			if (!response.enrollPaymentMethod.cardExpireMonth)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'cardExpireMonth', errorMessage: 'Field is required.' });
			}
			
			if (!response.enrollPaymentMethod.cardExpireYear)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'cardExpireYear', errorMessage: 'Field is required.' });
			}
		}
		else if (response.enrollPaymentMethod.type == 'bank')
		{
			if (!response.enrollPaymentMethod.bankAccountNumber)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'bankAccountNumber', errorMessage: 'Field is required.' });
			}
			
			if (!response.enrollPaymentMethod.bankRoutingNumber)
			{
				response.enrollPaymentMethod.fieldValidationErrors.push({ field: 'bankRoutingNumber', errorMessage: 'Field is required.' });
			}
		}
		
		if (response.enrollPaymentMethod && response.enrollPaymentMethod.fieldValidationErrors.length && !response.errorMessages.length)
		{
			response.errorMessages.push({ errorMessage: "There were errors with your enrollment." });
		}
		
		if (!response.errorMessages.length)
		{
			response.hasEnrolled = true;
			
			response.salesAgreements.forEach(function(item)
			{
				if (item.isActive)
				{
					item.paymentMethod = response.enrollPaymentMethod;
				}
			});
			
			response.informationalMessages.push({ infoMessage: "You have successfully enrolled in AutoPay." });
		}
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/addEasyPay', function(data, callback)
	{
		var response = new EasyPayModel(data);
		
		//
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/editEasyPay', function(data, callback)
	{
		var response = new EasyPayModel(data);
		
		//
		
		callback(response);
	});
	
	OverrideDWR('MyAccountTMXPaymentUIUtils/removeEasyPay', function(data, callback)
	{
		var response = new EasyPayModel(data);
		
		//
		
		callback(response);
	});
	
})(jQuery);