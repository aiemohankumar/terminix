/*
 * Common Module ViewModel functions
 ***********************************************************************************************************/
function ViewModel_CreateGenericForModule(options)
{
	/*
	 * Properties that are added to the model:
	 *
	 * $scope			jQuery element that the model is associated with. Use that to bind, like: ko.applyBindings(viewModel, $scope)
	 * model			Model retrieved from the server
	 * modelLoaded		true/false; if model has been loaded or not
	 * originalModel	A clone of the original model so it can be referenced after the model has been changing on the page
	 * submitting		true/false; if SubmitDWR has been called. You should set this to true/false when sending the model anywhere
	 * error			false/string; If it is a string, then it contains an error message from the server when calling DWR
	 *
	 *
	 * Functions that are added to the model:
	 *
	 * ReloadModule() // Reloads the current module on the page, specified with the $scope option
	 *
	 * UpdateModel(json) // Updates the model to a new model
	 * - json			The new object to replace the model. This is an actual object and not a JSON string
	 *
	 * ResetOriginalModel() // Resets the current model back to originalModel
	 *
	 * HasModelChanged() // true/false if the current model is different from the originalModel
	 */
	
	/*
	 * Example of Module APIs
	 *
	 * api: {
	 *     setBillingAddress: function(self, data) {
	 *         self.model().billingAddress(data.billingAddress);
	 *     }
	 * }
	 *
	 * Then in another module's code you can do this:
	 *
	 * TAB.Modules.API('contact-info-large', 'setBillingAddress', {
	 *     billingAddress: {
	 *         address1: '1234 My Street',
	 *         address2: '',
	 *         city: 'Memphis',
	 *         state: 'TN',
	 *         zipcode: 38125
	 *     }
	 * });
	 */
	
	options = $.extend({/*
		$module: $(),
		moduleOptions: {},
		callback: function(self){},
		
		api: {}, // Map of APi functions that other modules can call
		
		// Event doc:
		// > Events are only fired if *DWR string is set
		// OnBefore*: function(self){}, // Called before DWR is executed
		// OnGet*: function(self, json){}, // Called when response from DWR is retrieved
		// OnErr*: function(self, error){}, // Called when DWR errors
		// OnAfter*: function(self){}, // Called after DWR is executed
		
		LoadDataDWR: 'UiUtils/getModel',
		OnBeforeLoadData: function(self){},
		OnGetLoadData: function(self, json){},
		OnErrLoadData: function(self, error){},
		OnAfterLoadData: function(self){},
		
		SubmitDWR: 'UiUtils/setModel',
		OnBeforeSubmit: function(self){},
		OnGetSubmit: function(self, json){},
		OnErrSubmit: function(self, error){},
		OnAfterSubmit: function(self){}
	*/}, options);
	
	var self = {};
	
	self.$scope = options.$module;
	
	options.$module.data('vm', self);
	options.$module.data('api', options.api);
	
	ViewModel_AddCommonFunctions(self);
	ViewModel_AddModuleFunctions(self, options.$module, options.moduleOptions);
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	self.originalModel = ko.observable(null);
	
	self.error = ko.observable(false);
	
	self.SetAPI = function(api)
	{
		options.$module.data('api', $.extend({}, options.api, api));
		
		return self;
	};
	
	self.UpdateModel = function(json)
	{
		self.model(ko.mapping.fromJS(json));
		self.originalModel(ko.mapping.fromJS(json));
		
		return self;
	};
	
	self.ResetOriginalModel = function()
	{
		self.originalModel(ko.mapping.fromJS(ko.mapping.toJS(self.model)));
		
		return self;
	};
	
	if (options.LoadDataDWR)
	{
		self.LoadData = function(callback)
		{
			if (typeof options.OnBeforeLoadData === 'function' && options.OnBeforeLoadData(self) === false)
			{
				if (typeof callback === 'function')
				{
					callback();
				}
				
				return;
			}
			
			CallDWR(options.LoadDataDWR, null, function(data)
			{
				if (data.error)
				{
					self.error(data.error);
					
					if (typeof options.OnErrLoadData === 'function')
					{
						options.OnErrLoadData(self, data.error);
					}
					
					if (typeof callback === 'function')
					{
						callback();
					}
					
					return;
				}
				
				var json = data.model;
				
				if (typeof options.OnGetLoadData === 'function')
				{
					options.OnGetLoadData(self, json);
				}

				self.UpdateModel(json);
				
				if (self.model().redirectURL())
				{
					Redirect(self.model().redirectURL());
					return;
				}
				
				self.modelLoaded(true);
				
				if (typeof options.OnAfterLoadData === 'function')
				{
					options.OnAfterLoadData(self);
				}
				
				if (typeof callback === 'function')
				{
					callback();
				}
			});
		};
	}
	
	self.HasModelChanged = function()
	{
		if (!self.model() || !self.originalModel())
			return false;
		
		var curModel = ko.mapping.toJS(self.model),
			oldModel = ko.mapping.toJS(self.originalModel);
		
		return !self.AreModelsEqual(curModel, oldModel);
	};
	
	if (options.SubmitDWR)
	{
		self.Submit = function(m, e)
		{
			if (typeof options.OnBeforeSubmit === 'function' && options.OnBeforeSubmit(self) === false)
			{
				return;
			}
			
			self.RemoveAllErrors();
			
			var json = ko.mapping.toJS(self.model);
			
			self.submitting(true);
			
			CallDWR(options.SubmitDWR, json, function(data)
			{
				self.submitting(false);
				
				if (data.error)
				{
					self.model().errorMessages.push({ errorMessage: data.error });
					
					if (typeof options.OnErrSubmit === 'function')
					{
						options.OnErrSubmit(self, data.error);
					}
					
					return;
				}
				
				json = data.model;
				
				if (typeof options.OnGetSubmit === 'function')
				{
					options.OnGetSubmit(self, json);
				}
				
				self.UpdateModel(json);
				
				if (self.model().redirectURL())
				{
					setTimeout(function()
					{
						Redirect(self.model().redirectURL());
						
					}, 1500);
					return;
				}
				
				if (typeof options.OnAfterSubmit === 'function')
				{
					options.OnAfterSubmit(self);
				}
			});
		};
	}
	
	if (typeof options.callback === 'function')
	{
		options.callback(self);
	}
	
	return self;
}

function ViewModel_AddModuleFunctions(self, $module, options)
{
	self.reloadingModule = ko.observable(false);
	
	self.IsModuleOnPage = function()
	{
		return $module.closest('body').length == 1;
	};
	
	self.ReloadModule = function(m, e)
	{
		var DoReload = function()
		{
			TAB.Modules.Reload($module.data('name'), function($newModule, newOptions)
			{
				self.reloadingModule(false);
			});
		};
		
		self.reloadingModule(true);
		
		if (e && e.target)
		{
			setTimeout(function() { DoReload(); }, 800);
		}
		else
		{
			DoReload();
		}
	};
}