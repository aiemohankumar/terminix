function ViewModel()
{
	var self = this;
	
	ViewModel_AddCommonFunctions(self);
	
	// ViewModel variables
	// =====================================
	
	self.loaded = ko.observable(false);
	self.loadingText = ko.observable('Looking up your service...');
	self.error = ko.observable();
	
	self.service = ko.observable();
	
	
	// Reschedule functionality
	// =====================================
	
	function GetModelJSON()
	{
		var json = ko.mapping.toJS(self.service);
		
		// Format the rescheduleSelectedDate for the server
		if (json.rescheduleSelectedDate && json.rescheduleSelectedDate.date)
		{
			var date = new Date(json.rescheduleSelectedDate.date);
			
			json.rescheduleSelectedDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
		}
		
		// Reset time variables
		json.rescheduleSelectedTime = null;
		json.rescheduleSelectedEmployeeNumber = null;
		
		// Check if a time object is chosen to populate time variables
		if (json.rescheduleSelectedTimeObject)
		{
			json.rescheduleSelectedTime = json.rescheduleSelectedTimeObject.time;
			json.rescheduleSelectedEmployeeNumber = json.rescheduleSelectedTimeObject.employeeNumber;
		}
		
		return json;
	}
	
	function DateKey(date)
	{
		return date ? (date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate()) : '';
	}
	
	function CreateRescheduleMonths()
	{
		var service = self.service();
		
		if (ko.isObservable(service.months))
		{
			service.months([]);
		}
		else
		{
			service.months = ko.observableArray();
		}
		
		if (!service.rescheduleDates() || !service.rescheduleDates().length)
			return;

		var dates = [],
			months = {},
			monthsArray = [],
			availableDates = {},
			firstDate = null,
			lastDate = null;
		
		var DayItem = function(data)
		{
			var item = $.extend({
				text: '',
				date: null,
				available: false,
				times: []
			}, data);
			
			var key = DateKey(item.date);
			
			if (key in availableDates)
			{
				item.available = true;
				item.times = availableDates[key].times;
			}
			
			return item;
		};
		
		service.rescheduleDates().forEach(function(d)
		{
			var datePieces = d.date().split('/');
			var date = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
			
			if (firstDate == null || firstDate > date)
				firstDate = new Date(date);
			
			if (lastDate == null || lastDate < date)
				lastDate = new Date(date);
			
			dates.push(date);
			
			availableDates[DateKey(date)] = ko.mapping.toJS(d);
		});
		
		dates.forEach(function(date)
		{
			if (!(date.getUTCMonth() in months))
			{
				var month = {
					date: new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), 1)),
					monthNumber: date.getUTCMonth(),
					monthName: GetMonthName(date.getUTCMonth() + 1),
					weeks: []
				};
				
				// Build first week
				var d = new Date(month.date);
				var week = { days: [] };
				
				for (var day = 0; day < d.getUTCDay(); day++)
				{
					week.days.push(DayItem());
				}
				
				var thisMonth = d.getUTCMonth();
				
				do
				{
					// Loop through the end of the week
					do
					{
						if (d.getUTCMonth() == thisMonth)
						{
							week.days.push(DayItem({
								text: d.getUTCDate(),
								date: new Date(+d)
							}));
						}
						else
						{
							week.days.push(DayItem());
						}
						
						d.setTime(d.getTime() + 86400000);
					}
					while (d.getUTCDay() > 0);
					
					// Check if this week has any available days
					var hasAvailable = week.days.some(function(day) { return day.available; }),
						firstWeekDay = week.days.find(function(day) { return !!day.date });
					
					if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate)
					{
						// Add week for month
						month.weeks.push(week);
					}
					
					// Reset week
					week = { days: [] };
				}
				while (d.getUTCMonth() == thisMonth);
				
				// Store month
				months[thisMonth] = month;
				monthsArray.push(ko.mapping.fromJS(month));
			}
		});
		
		monthsArray.sort(function(a, b)
		{
			return a.date() < b.date() ? -1 : 1;
		});
		
		service.months(monthsArray);
	}
	
	self.SetScheduleDay = function(item, day)
	{
		if (day.date() && day.available())
		{
			item.rescheduleSelectedDate(day);
			item.rescheduleSelectedTimeObject(null);
		}
	};
	
	self.GetScheduleDayCSS = function(item, day)
	{
		return {
			disabled: !day.date() || !day.available(),
			active: day.date() && item.rescheduleSelectedDate() && DateKey(item.rescheduleSelectedDate().date()) == DateKey(day.date())
		};
	};
	
	self.IsScheduleTimeAvailable = function(item, time)
	{
		if (!item.rescheduleSelectedDate())
			return false;
		
		var matchingTime = ko.utils.arrayFirst(item.rescheduleSelectedDate().times(), function(t)
		{
			return t.time() == time;
		});
		
		return !!matchingTime;
	};
	
	self.GetRescheduleTimes = function(item)
	{
		var timeObjects = [];
		
		if (item.rescheduleSelectedDate())
		{
			item.rescheduleTimes().forEach(function(time)
			{
				var timeObject = ko.utils.arrayFirst(item.rescheduleSelectedDate().times(), function(t)
				{
					return t.time() == time;
				});
				
				timeObjects.push(timeObject || ko.mapping.fromJS({
					time: time,
					employeeNumber: null
				}));
			});
		}
		else
		{
			item.rescheduleTimes().forEach(function(time)
			{
				timeObjects.push(ko.mapping.fromJS({
					time: time,
					employeeNumber: null
				}));
			});
		}
		
		return timeObjects;
	};
	
	self.GetScheduleTimeDisplay = (function()
	{
		var FormatHour = function(hour)
		{
			return (hour > 12 ? hour - 12 : hour) + (hour < 12 ? 'AM' : 'PM');
		};
		
		return function(time)
		{
			if (!time) return '';
			
			if (time.hasOwnProperty('time'))
				time = time.time;
			
			if (ko.isObservable(time))
				time = time();
			
			return FormatHour(time) + ' - ' + FormatHour(parseInt(time) + 2);
		};
	})();
	
	self.SetNoRescheduleDay = function(item)
	{
		item.rescheduleSelectedDate(null);
		item.rescheduleSelectedTimeObject(null);
		item.rescheduleStep('contact');
	};
	
	self.GetScheduleDateDisplay = function(item, verbose, overrideDateString)
	{
		var dateObject, date;
		
		if (overrideDateString)
		{
			date = ('' + overrideDateString).split('/');
			
			if (date.length == 3)
			{
				dateObject = new Date(Date.UTC(parseInt(date[2]), parseInt(date[0]) - 1, parseInt(date[1])));
			}
		}
		else if (item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject())
		{
			dateObject = item.rescheduleSelectedDate().date();
		}
		
		if (dateObject)
		{
			if (verbose)
			{
				return GetMonthName(dateObject.getUTCMonth() + 1) + ' ' + dateObject.getUTCDate();
			}
			
			return (dateObject.getUTCMonth() + 1) + '/' + dateObject.getUTCDate() + '/' + dateObject.getUTCFullYear();
		}
		
		return '';
	};
	
	self.CancelRescheduleStep = function(item)
	{
		self.RemoveAllErrors(item);
		
		switch (item.rescheduleStep())
		{
		case 'error':
			break;
		
		case 'appointment':
			break;
		
		case 'contact':
			if (item.months().length)
			{
				item.rescheduleStep('appointment');
			}
			
			item.rescheduleContactFname(item._original.rescheduleContactFname());
			item.rescheduleContactLname(item._original.rescheduleContactLname());
			item.rescheduleContactPhone(item._original.rescheduleContactPhone());
			item.rescheduleContactDate('');
			item.rescheduleContactTime('');
			break;
		}
	};
	
	var currentDate = new Date(), otherDate;
	
	self.DatepickerOptions = {
		minDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 1) && otherDate,
		maxDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 45) && otherDate,
		beforeShowDay: function(date) {
			// Has to return an array of [isSelectable, CSS class name string for date cell, optional popup tooltip]
			// For date.getDay(), 0 = Sunday
			return [date.getDay() > 0, ''];
		}
	};
	
	self.IsValidContact = function(item)
	{
		return item.rescheduleContactFname() && item.rescheduleContactLname() && item.rescheduleContactPhone() && item.rescheduleContactDate() && item.rescheduleContactTime();
	};
	
	self.ConfirmContact = function(item, e)
	{
		if (item.submitting() || !self.IsValidContact(item)) return;
		
		self.RemoveAllErrors(item);
		
		var json = self.GetModelJSON(item);
		
		item.submitting(true);
		
		CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', json, function(data)
		{
			item.submitting(false);
			
			if (data.error)
			{
				item.errorMessages.push({ errorMessage: data.error });
				return;
			}
			
			// Update model
			self.ApplyModelChanges(item, data.model);
			
			// Go to next step if no errors
			if (!item.errorMessages().length && !item.fieldValidationErrors().length)
			{
				item.rescheduleStep('confirmationContact');
			}
		});
	};
	
	self.IsValidReschedule = function(item)
	{
		return item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject();
	};
	
	self.ConfirmAppointment = function(item, e)
	{
		if (item.submitting() || !self.IsValidReschedule(item)) return;
		
		self.RemoveAllErrors(item);
		
		var json = self.GetModelJSON(item);
		
		item.submitting(true);
		
		CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', json, function(data)
		{
			item.submitting(false);
			
			if (data.error)
			{
				item.errorMessages.push({ errorMessage: data.error });
				return;
			}
			
			// Ignore rescheduleSelectedDate being set
			if (data.model && 'rescheduleSelectedDate' in data.model)
			{
				delete data.model['rescheduleSelectedDate'];
			}
			
			// Update model
			self.ApplyModelChanges(item, data.model);
			
			// Go to next step if no errors
			if (!item.errorMessages().length && !item.fieldValidationErrors().length)
			{
				item.rescheduleStep('confirmationAppointment');
			}
		});
	};
	
	
	// Initialization
	// =====================================
	
	// Override DWR calls for testing
	/*OverrideDWR('MyAccountTMXServicesUIUtils/getScheduledServiceByToken', function(data, callback)
	{
		callback({ "token": $.QueryString.token, "service": {"address":"1304 DILL ST","city":"NEWPORT","state":"AR","postalCode":"72112","date":"06/02/2017","description":"Ext General Pest Control Initial","time":"3:00PM - 5:00PM","startTime":"1500","endTime":"1700","duration":null,"price":129,"technician":"HUTCHISON, TROY L.","technicianNumber":"40783","phone":"8665970094","showDetail":false,"noteForTechnician":"","workOrderActivityId":"15426918448","rescheduleDate":null,"rescheduleFromTime":null,"rescheduleToTime":null,"serviceLine":"EGPC","salesAgreementNumber":"16986445","isConfirmedAppointment":true,"canConfirmAppointment":false,"submitting":false,"rescheduleLoaded":false,"rescheduleStep":"","rescheduleDates":null,"rescheduleTimes":[],"rescheduleSelectedDate":null,"rescheduleSelectedTime":null,"rescheduleSelectedEmployeeNumber":null,"rescheduleContactFname":"Jonathan","rescheduleContactLname":"Riley","rescheduleContactPhone":"9018315115","rescheduleContactDate":null,"rescheduleContactTime":null,"errorMessages":[],"informationalMessages":[],"fieldValidationErrors":[],"redirectRequest":false,"redirectURL":null} });
	});*/
	/*OverrideDWR('MyAccountTMXServicesUIUtils/rescheduleService', function(data, callback)
	{
		callback({"address":"1304 DILL ST","city":"NEWPORT","state":"AR","postalCode":"72112","date":"06/02/2017","description":"Ext General Pest Control Initial","time":"3:00PM - 5:00PM","startTime":"1500","endTime":"1700","duration":null,"price":129,"technician":"HUTCHISON, TROY L.","technicianNumber":"40783","phone":"8665970094","showDetail":false,"noteForTechnician":"","workOrderActivityId":"15426918448","rescheduleDate":null,"rescheduleFromTime":null,"rescheduleToTime":null,"serviceLine":"EGPC","salesAgreementNumber":"16986445","isConfirmedAppointment":true,"canConfirmAppointment":false,"submitting":false,"rescheduleLoaded":false,"rescheduleStep":"appointment","rescheduleDates":[{"date":"06/30/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/15/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"07/11/2017","times":[{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/09/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/12/2017","times":[{"time":10,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"},{"time":8,"employeeNumber":"40783"}]},{"date":"07/19/2017","times":[{"time":10,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/21/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"07/03/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/31/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/22/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/10/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"07/24/2017","times":[{"time":15,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"}]},{"date":"06/15/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"06/08/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/18/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":8,"employeeNumber":"40783"}]},{"date":"06/20/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"07/08/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/23/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/16/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/27/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/28/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/17/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/01/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"07/10/2017","times":[{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/14/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"07/05/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/14/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"}]},{"date":"06/26/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/28/2017","times":[{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/25/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"07/07/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/29/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/12/2017","times":[{"time":15,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/17/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/29/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"},{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/04/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"06/13/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"06/24/2017","times":[{"time":8,"employeeNumber":"40783"},{"time":10,"employeeNumber":"40783"}]},{"date":"06/22/2017","times":[{"time":13,"employeeNumber":"40783"},{"time":15,"employeeNumber":"40783"}]},{"date":"07/06/2017","times":[{"time":15,"employeeNumber":"40783"}]},{"date":"07/27/2017","times":[{"time":15,"employeeNumber":"40783"}]}],"rescheduleTimes":[8,10,13,15],"rescheduleSelectedDate":null,"rescheduleSelectedTime":null,"rescheduleSelectedEmployeeNumber":null,"rescheduleContactFname":"Jonathan","rescheduleContactLname":"Riley","rescheduleContactPhone":"9018315115","rescheduleContactDate":null,"rescheduleContactTime":null,"errorMessages":[],"informationalMessages":[],"fieldValidationErrors":[],"redirectRequest":false,"redirectURL":null});
	});*/
	
	var loadingRescheduleDates = false;
	var loadAttempts = 0;
	
	self.LoadRescheduleDates = function()
	{
		if (loadingRescheduleDates)
			return;
		
		ScrollToTop();
		
		self.RemoveAllErrors(self.service);
		
		loadingRescheduleDates = true;
		loadAttempts++;
		
		self.loaded(false);
		self.loadingText('Finding available appointment dates and times...');
		
		CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', GetModelJSON(), function(data)
		{
			loadingRescheduleDates = false;
			
			if (data.error)
			{
				if (loadAttempts < 2)
				{
					self.service().errorMessages.push({ errorMessage: data.error });
					self.service().rescheduleStep('error');
				}
				else
				{
					self.service().rescheduleStep('contact');
				}
				
				self.loaded(true);
				return;
			}
			
			self.ApplyModelChanges(self.service(), data.model);
			
			CreateRescheduleMonths();
			
			if (self.service().months().length)
			{
				self.service().rescheduleStep('appointment');
			}
			else
			{
				self.service().rescheduleStep('contact');
			}
			
			self.loaded(true);
		});
	};
	
	function RedirectToLogin(params)
	{
		var url = "/my-account/pages/login.jsp";
		
		if (params)
		{
			var paramArray = [];
			
			for (var key in params)
			{
				paramArray.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
			}
			
			url += "?" + paramArray.join("&");
		}
		
		Redirect(url);
	}
	
	function ScrollToTop()
	{
		var offset = $('#page-content .form-container, #page-content .loading-container, #page-content .error-container').offset();
		if (offset && offset.top < $(window).scrollTop())
		{
			ScrollTo(offset.top, 'fast');
		}
	}
	
	if ($.QueryString.token)
	{
		CallDWR('MyAccountTMXServicesUIUtils/getScheduledServiceByToken', { token: $.QueryString.token }, function(data)
		{
			if (data.error)
			{
				self.error(data.error);
				return;
			}
			
			var json = data.model;
			
			if (!json || !json.service)
			{
				RedirectToLogin({ badRescheduleToken: true });
				return;
			}
			
			var service = json.service;
			
			service.rescheduleSelectedTimeObject = null;
			service.months = [];
			service._original = $.extend({}, service);
			
			self.service(ko.mapping.fromJS(service));
			
			var firstTime = true;
			
			self.service().rescheduleStep.subscribe(function()
			{
				if (firstTime)
				{
					firstTime = false;
				}
				else
				{
					ScrollToTop();
				}
			});
			
			self.LoadRescheduleDates();
		});
	}
	else
	{
		RedirectToLogin({ badRescheduleToken: true });
	}
}

$(function()
{
	var vm = new ViewModel();
	
	// Expose globally for debugging
	window.vm = vm;
	
	ko.applyBindings(vm);
});