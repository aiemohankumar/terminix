/*
 * Session Timing Out
 ********************************************************/
ResetTimeout.initialize();

/*
 * Global main OnTabLoad hook
 ********************************************************/
function UpdateURLHash(tabName, tabOptions)
{
	// Update the URL hash with options included
	var hash = '#' + tabName;
	
	if (tabOptions && tabOptions.focusModule)
		hash += '/' + tabOptions.focusModule;
	
	window.location.hash = hash;
}

TAB.OnBeforeTabLoad(function(name, $tab, options)
{
	return TAB.IsValidTab(name);
});

TAB.OnTabLoad(function(name, $tab, options)	
{
	UpdateURLHash(name, options);
	
	var tabRequestId = TAB.GetCurrentRequestID();
	
	// Update the navigation's current selected tab
	$('#myaccount-nav > ul > li.active').removeClass('active');
	$('#myaccount-nav > ul > li.' + name).addClass('active');

	$('#mobile-nav-menu > ul > li.active').removeClass('active');
	$('#mobile-nav-menu > ul > li.' + name).addClass('active');
	
	// Display message if in service notifications pilot group
	if (name == 'my-account')
	{
		CallDWR('my-account-json/getServiceNotificationDashboardMessage.json', '', function(data)
		{
			if (tabRequestId == TAB.GetCurrentRequestID() && data.model && data.model.showServiceNotificationsDashboardMessage)
			{
				CreateAlert({
					color: 'green',
					message: data.model.message,
					messageIsHTML: true,
					button: false,
					closeButton: false
				});
			}
		});
	}
		
	// Display message if web services are down
	CallDWR('my-account-json/getWebServiceStatus.json', '', function(data)
	{
		if (tabRequestId == TAB.GetCurrentRequestID() && data.model && !data.model.webServicesUp) {
			var message = "";
			
			if (data.model.webServiceOutageMessage) {
				message = data.model.webServiceOutageMessage;
			}
			
			CreateAlert({
				color: 'red',
				message: message,
				button: false,
				closeButton: false
			});
		}
	});

	CallDWR('MyAccountTMXPaymentUIUtils/getPaymentDueData', {}, function(data) {		
		if (data.model) {
			if (tabRequestId == TAB.GetCurrentRequestID() && data.model.duePayments.length > 0) {		
				data.model.duePayments.forEach(function(duePayment) {
					if (duePayment.termiteRenewal) {
						CreateAlert({
							color: 'red-termite',
							message: "Your " + duePayment.description + " is set to expire soon!",
							messageIsHTML: true,
							button: true,
							closeButton: true,
							customCloseButton: 'termite-renewal',
							icon: 'icon-warning',
							onCloseAlert: function(e) {
								var jsonData = { messageType: 'TMX_RNL_ALERT', serviceLine: duePayment.serviceLine };
								CallDWR('MyAccountTMXUIUtils/dismissTMXMyAccountMessage', jsonData, function(data) {});
							}
						});
					}	
				})
			}
		}
	});
});

TAB.OnTabLoadError(function(name, options)
{
	UpdateURLHash(name, options);

	// Set no tab is selected
	$('#myaccount-nav > ul > li.active').removeClass('active');
	$('#mobile-nav-menu > ul > li.active').removeClass('active');
});

TAB.IsValidTab = (function()
{
	var TABS = {};
	
	['my-account', 'payments', 'contact-profile', 'faqs'].forEach(function(name)
	{
		TABS[name] = true;
	});
	
	return function(name)
	{
		return name in TABS && (!window.g_HidePaymentsTab || name != 'payments');
	};
})();


/*
 * On page load
 ********************************************************/
$(function()
{
	window.g_CurrentPropertyId = $('#current-property-id').val() || 0;
	window.g_CurrentCustomerId = $('#current-customer-id').val() || 0;
	window.g_HidePaymentsTab = false;
	
	// Check if this account is a national account
	var UpdatePaymentsTabHidden = function(fn)
	{
		CallDWR('MyAccountTMXUIUtils/isHidePaymentsTab', null, function(data)
		{
			g_HidePaymentsTab = !data.error && data.model && data.model.hidePaymentsTab;
			
			$('#myaccount-nav > ul > li.payments').toggle(!g_HidePaymentsTab);
			$('#mobile-nav-menu > ul > li.payments').toggle(!g_HidePaymentsTab);
			
			if (typeof fn == 'function')
				fn();
		});
	};
	
	/*
	 * MyAccount Header Dropdown
	 ********************************************************/
	var ClosePropertyDropdown = function()
	{
		// === DESKTOP =========================
		$('#page-overlay').hide().off('click.myaccount-nav');
		$('body').removeClass('myaccount-nav-dropdown');
		$('#myaccount-header .address-bar .address-dropdown-list').removeClass('loading-new-property removing-property');
		
		// === MOBILE ==========================
		$('#mobile-property-menu > ul').removeClass('removing-property');
		$('#mobile-property-menu > ul > li.removing-property').removeClass('removing-property removing-property-submit');
	};
	
	$('#myaccount-header .address-bar .address-dropdown, #mobile-address-bar .address-dropdown').on('click', function(e)
	{
		if ($('body').hasClass('myaccount-nav-dropdown'))
		{
			if (!$('#myaccount-header .address-bar .address-dropdown-list').is('.loading-new-property, .removing-property'))
			{
				ClosePropertyDropdown();
			}
		}
		else
		{
			$('#page-overlay').show().on('click.myaccount-nav', function(e)
			{
				if ($(e.target).is(this) && !$('#myaccount-header .address-bar .address-dropdown-list').is('.loading-new-property, .removing-property'))
				{
					e.preventDefault();
					
					ClosePropertyDropdown();
				}
			});
			
			$('body').addClass('myaccount-nav-dropdown');
		}
	});
	
	var LoadProperty = function(propertyId)
	{
		$('#myaccount-header .address-bar .address-dropdown-list').addClass('loading-new-property');
		
		CallDWR('MyAccountTMXUIUtils/changeProperty', { propertyId: propertyId }, function(data)
		{
			if (data.error)
			{
				ClosePropertyDropdown();
				
				CreateAlert({
					color: 'red',
					message: "There was an error switching properties.",
					button: {
						text: 'Try Again'
					},
					onButtonClick: function(e)
					{
						// Remove the alert
						$(this).remove();
						
						// Show the property dropdown for loading
						$('body').addClass('myaccount-nav-dropdown');
						
						// Load the next property
						LoadProperty(propertyId);
					}
				});
			}
			else if (data.model.redirectURL)
			{
				Redirect(data.model.redirectURL);
			}
			else if (data.model.propertyId != g_CurrentPropertyId)
			{
				UpdatePaymentsTabHidden(function()
				{
					ClosePropertyDropdown();
					
					// Grab the elements that switch information
					var $headerAddressText = $('#myaccount-header .address-bar .address-text');
					var $oldDropdownAddressItem = $('#myaccount-header .address-bar .address-dropdown-list li[data-property-id="' + g_CurrentPropertyId + '"]');
					var $newDropdownAddressItem = $('#myaccount-header .address-bar .address-dropdown-list li[data-property-id="' + data.model.propertyId + '"]');
					var newAddressText = $newDropdownAddressItem.find('.text p').html();
					
					// Switch the information to the new selected property
					g_CurrentPropertyId = data.model.propertyId;
					g_CurrentCustomerId = data.model.customerId;
					
					// Update the information in Tealium utag data
					if (typeof utag != 'undefined' && typeof utag.view == 'function')
					{
						utag_data.customer_id = g_CurrentCustomerId;
						utag.view({ customer_id: g_CurrentCustomerId });
					}
					
					$headerAddressText.html(newAddressText);
					
					$oldDropdownAddressItem.removeClass('active hidden');
					$newDropdownAddressItem.addClass('active hidden');
					
					// Also switch out the mobile menu item
					$('#mobile-address-bar .address-text').html(newAddressText);
					
					$('#mobile-property-menu ul li.active').removeClass('active');
					$('#mobile-property-menu ul li[data-property-id="' + data.model.propertyId + '"]').addClass('active');
					
					// Reload the current tab
					if (g_HidePaymentsTab && TAB.GetCurrentTab() == 'payments')
					{
						TAB.Load('my-account');
					}
					else
					{
						TAB.Reload();
					}
				});
			}
			else
			{
				ClosePropertyDropdown();
			}
		});
	};
	
	var CancelRemoveProperty = function()
	{
		// === DESKTOP =========================
		// Hide the prompt to remove
		$('#myaccount-header .address-bar .address-dropdown-list').removeClass('removing-property');
		
		// === MOBILE ==========================
		// Hide the prompt
		$('#mobile-property-menu > ul > li').removeClass('removing-property');
		
		// Show all other remove buttons
		$('#mobile-property-menu > ul').removeClass('removing-property');
	};
	
	var ConfirmRemoveProperty = function(propertyId)
	{
		// === DESKTOP =========================
		// Grab the address
		var addressText = $('#myaccount-header .address-bar .address-dropdown-list li[data-property-id="' + propertyId + '"] .text p').html();
		
		// Show the address in the prompt
		$('#myaccount-header .address-bar .address-dropdown-list li.remove-property-confirm .address-text').text(addressText);
		
		// Put the property id on the YES button
		$('#myaccount-header .address-bar .address-dropdown-list li.remove-property-confirm .btn-answer-yes').data('property-id', propertyId);
		
		// Show the prompt to remove
		$('#myaccount-header .address-bar .address-dropdown-list').addClass('removing-property');
		
		// === MOBILE ==========================
		// Show the prompt
		$('#mobile-property-menu > ul > li[data-property-id=' + propertyId + ']').addClass('removing-property');
		
		// Hide all other remove buttons
		$('#mobile-property-menu > ul').addClass('removing-property');
	};
	
	var RemoveProperty = function(propertyId)
	{
		// === DESKTOP =========================
		$('#myaccount-header .address-bar .address-dropdown-list').addClass('loading-new-property');
		
		// === MOBILE ==========================
		$('#mobile-property-menu > ul').addClass('removing-property');
		$('#mobile-property-menu > ul > li[data-property-id=' + propertyId + ']').addClass('removing-property removing-property-submit');
		
		CallDWR('MyAccountTMXUIUtils/removeProperty', { propertyId: propertyId }, function(data)
		{
			if (data.error || data.model.errorMessages && data.model.errorMessages.length)
			{
				ClosePropertyDropdown();
				
				CreateAlert({
					color: 'red',
					message: "There was an error removing this property.",
					button: {
						text: 'Try Again'
					},
					onButtonClick: function(e)
					{
						// Remove the alert
						$(this).remove();
						
						// Show the property dropdown for loading
						$('body').addClass('myaccount-nav-dropdown');
						
						// Remove this property again
						RemoveProperty(propertyId);
					}
				});
			}
			else if (data.model.redirectURL)
			{
				Redirect(data.model.redirectURL);
			}
			else
			{
				// Show an alert with the address text
				var addressText = $('#myaccount-header .address-bar .address-dropdown-list li[data-property-id="' + propertyId + '"] .text p').html();
				
				CreateAlert({
					color: 'green',
					message: "Successfully removed " + addressText + " from your account.",
					button: false,
					closeButton: true
				});
				
				// Remove this property from the dropdown menu
				$('#myaccount-header .address-bar .address-dropdown-list li[data-property-id="' + propertyId + '"]').remove();
				$('#mobile-property-menu > ul > li[data-property-id="' + propertyId + '"]').remove();
				
				// Update the dropdown count
				var numAddresses = $('#myaccount-header .address-bar .address-dropdown-list li:not(.remove-property-confirm)').length;
				$('#myaccount-header .address-bar .address-dropdown .num-addresses').text(numAddresses);
				
				if (numAddresses == 1)
				{
					$('#myaccount-header .address-bar .address-dropdown').remove();
				}
				
				ClosePropertyDropdown();
			}
		});
	};
	
	$('#myaccount-header .address-bar .address-dropdown-list > li:not(.remove-property-confirm)').each(function()
	{
		var $property = $(this);
		var propertyId = $property.data('property-id');
		
		if (!propertyId) return;
		
		var address = $property.find('.text p').html();
		
		var $mobileItem = $('<li></li>')
						  .append($('<a href="#"></a>')
								  .append('<span class="col notification"></span>')
								  .append('<span class="col text"></span>')
								  .append('<span class="col remove"><span class="btn btn-green btn-shadow btn-close">&times;</span></span>')
								  .append($('<span class="col remove-prompt"></span>')
										  .append($('<span class="address"></span>').html(address))
										  .append('<span class="prompt">Are you sure you want to remove this property?</span>')
										  .append('<span class="progress">Removing this property...</span>')
										  .append($('<span class="buttons"></span>')
												  .append('<span class="btn btn-orange btn-skinny btn-taller btn-smalltext btn-answer-yes">Yes, Remove</span>')
												  .append('<span class="btn btn-link btn-skinny btn-taller btn-smalltext btn-answer-no">Cancel</span>'))));
		
		if (propertyId == g_CurrentPropertyId)
		{
			$mobileItem.addClass('active');
		}
		
		$mobileItem.data('property-id', propertyId).attr('data-property-id', propertyId);
		$mobileItem.find('.col.text').html(address);
		
		if ($mobileItem.find('.col.text .notification-text').length > 0)
		{
			$mobileItem.find('.col.notification').addClass('red');
		}
		
		$mobileItem.on('click', function(e)
		{
			e.preventDefault();
			
			if (propertyId && propertyId != g_CurrentPropertyId)
			{
				if ($(e.target).is('.btn-close'))
				{
					ConfirmRemoveProperty(propertyId);
				}
				else if ($(e.target).is('.btn-answer-yes'))
				{
					RemoveProperty(propertyId);
				}
				else if ($(e.target).is('.btn-answer-no'))
				{
					CancelRemoveProperty();
				}
				else
				{
					LoadProperty(propertyId);
				}
			}
			else
			{
				ClosePropertyDropdown();
			}
		});
		
		$('#mobile-property-menu > ul').append($mobileItem);
	})
	.on('click', function(e)
	{
		e.preventDefault();
		
		var propertyId = $(this).data('property-id');
		
		if (propertyId && propertyId != g_CurrentPropertyId)
		{
			if ($(e.target).is('.btn-close'))
			{
				ConfirmRemoveProperty(propertyId);
			}
			else
			{
				LoadProperty(propertyId);
			}
		}
		else
		{
			ClosePropertyDropdown();
		}
	});
	
	$('#myaccount-header .address-bar .address-dropdown-list > li.remove-property-confirm .buttons .btn').on('click', function(e)
	{
		e.preventDefault();
		
		if ($(this).hasClass('btn-answer-yes'))
		{
			var propertyId = $(this).data('property-id');
			
			if (propertyId)
			{
				RemoveProperty(propertyId);
			}
			else
			{
				ClosePropertyDropdown();
				
				CreateAlert({
					color: 'red',
					message: "There was an error removing this property. Refresh the page to try again",
					button: false,
					closeButton: true
				});
			}
		}
		else
		{
			CancelRemoveProperty();
		}
	});
	
	if ($('#myaccount-header .address-bar .address-dropdown-list > li.notification').length)
	{
		$('#myaccount-header .address-bar, #mobile-address-bar').find('.address-alert-icon').show();
	}
	
	var numProperties = $('#myaccount-header .address-bar .address-dropdown-list > li:not(.remove-property-confirm)').length;
	
	if (numProperties > 1)
	{
		$('#myaccount-header .address-bar, #mobile-address-bar').addClass('address-bar-has-dropdown');
		
		$('#mobile-address-bar .address-dropdown').html('(<span class="num-addresses">' + numProperties + '</span>)');
	}
	else
	{
		$('#mobile-address-bar .address-dropdown').addClass('address-dropdown-empty');
	}
	
	$('#mobile-address-bar .address-text').html($('#myaccount-header .address-header .address-bar .address-text').html());
	
	$('#mobile-property-menu > ul').append('<li><a href="/my-account/pages/addProperty.jsp"><span class="col notification"></span><span class="col text">+ Add Property</span></a></li>');
	
	
	UpdatePaymentsTabHidden(function()
	{
		/*
		 * Default tab load
		 ********************************************************/
		var tabToLoad = 'my-account';
		var tabLoadOptions = {};
		var tabStringInURL;
		
		if (window.location.hash && window.location.hash.length > 1)
		{
			tabStringInURL = window.location.hash.substring(1);
		}
		else if ($.QueryString.tab)
		{
			tabStringInURL = $.QueryString.tab;
		}
		else if (IsEmailCampaignPrecheckAutoPay())
		{
			tabStringInURL = "payments";
		}
		
		if (tabStringInURL)
		{
			var tabPieces = tabStringInURL.split('/');
			
			if (TAB.IsValidTab(tabPieces[0]))
			{
				tabToLoad = tabPieces[0];
				
				if (tabPieces.length > 1)
				{
					tabLoadOptions.focusModule = tabPieces[1];
				}
			}
		}
		
		TAB.Load(tabToLoad, tabLoadOptions);
	});
});