/*
 * Module: Moving
 ***********************************************************************************************************/
TAB.Modules.Add('moving', function($module, options, onLoaded) {
	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		LoadDataDWR: 'my-account-json/getMovingModel.json',
		
		callback: function(self) {
			self.hasCheckedIfServiceable = ko.observable(false);
			
			self.CheckIfServiceable = function() {
				if (self.submitting() || !self.model().lookupZipcode()) return;
				
				self.submitting(true);
				self.hasCheckedIfServiceable(false);
				
				self.RemoveAllErrors();
				
				var json = ko.mapping.toJS(self.model);
				
				PromiseDWR({ url: 'my-account-json/isZipServiceable.json', data: json }).then(function(json) {
					self.model(ko.mapping.fromJS(json));
					
					self.hasCheckedIfServiceable(true);
				}).catch(function(error) {
					self.model().errorMessages.push({ errorMessage: error });
				}).then(function() {
					if (self.model().isServiceable() && !self.model().hasPastDueBalance()) {
						// Use session storage if their browser supports it, just to remember the ZIP
						// Otherwise, use the URL parameter
						if (caniuse.sessionStorage) {
							sessionStorage.setItem("MovingModelLookupZipcode", self.model().lookupZipcode());
							window.location.href = "/my-account/pages/moving.html";
						} else {
							window.location.href = "/my-account/pages/moving.html?zipcode=" + self.model().lookupZipcode();
						}
					} else {
						self.submitting(false);
					}
				});
			};
		}
	});

	ko.applyBindings(vm, $module.get(0));

	vm.LoadData(onLoaded);
});