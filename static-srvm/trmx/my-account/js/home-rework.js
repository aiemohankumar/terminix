TAB.OnBeforeTabLoad(function(name, $tab, options) {
	return TAB.IsValidTab(name);
});

TAB.OnTabLoad(function(name, $tab, options) {
	UpdateURLHash(name, options);
	
	// Update the navigation's current selected tab
	propertiesList.tabLoadedName(name);

	var tabRequestId = TAB.GetCurrentRequestID();
	
	// Display message if in service notifications pilot group
	if (name == 'my-account') {
		PromiseDWR('my-account-json/getServiceNotificationDashboardMessage.json').then(function(data) {
			if (tabRequestId == TAB.GetCurrentRequestID() && data && data.showServiceNotificationsDashboardMessage) {
				CreateAlert({
					color: 'green',
					message: data.message,
					messageIsHTML: true,
					button: false,
					closeButton: false
				});
			}
		});
	}
		
	// Display message if web services are down
	PromiseDWR('my-account-json/getWebServiceStatus.json').then(function(data) {
		if (tabRequestId == TAB.GetCurrentRequestID() && !data.webServicesUp) {
			var message = ""; // probably need a default message here

			if (data.webServiceOutageMessage) {
				message = data.webServiceOutageMessage;
			}

			CreateAlert({
				color : 'red',
				message : message,
				button : false,
				closeButton : false
			});
		}
	});
	
	var currentProperty = propertiesList.currentProperty();
	
	currentProperty.RefreshInitialization();
	
	currentProperty.paymentModelSmall.getData().then(function(data) {
		if (tabRequestId == TAB.GetCurrentRequestID() && currentProperty.paymentModelSmall().termiteRenewalData().length > 0) {
			// Create renewal alerts
			var termiteRenewalData = currentProperty.paymentModelSmall().termiteRenewalData();
			
			termiteRenewalData.forEach(function(termiteDatum) {
				if (!termiteDatum.suppressedAlert()) {
					CreateAlert({
						color: 'red-termite',
						message: "Your " + termiteDatum.description() + " is set to expire soon!",
						messageIsHTML: true,
						button: true,
						closeButton: true,
						customCloseButton: 'termite-renewal',
						icon: 'icon-warning',
						onCloseAlert: function(e) {
							var jsonData = { messageType: 'TMX_RNL_ALERT', serviceLine: termiteDatum.serviceLine() };
							CallDWR('MyAccountTMXUIUtils/dismissTMXMyAccountMessage', jsonData, function(data) {});
							termiteDatum.suppressedAlert(true);
						}
					});
				}
			})

			// Trigger Proactive LiveChat
			// if (GetFeatureToggleDefaultFalse("renewalProactiveChat")) {
			// 	currentProperty.liveChatModel.getData().then(function(data) {
			// 		if (tabRequestId == TAB.GetCurrentRequestID()) {
			// 			if (currentProperty.liveChatModel().showLiveChatButton()) {
			// 				LiveChat_OpenChat(); 
			// 			} else {
			// 				var subscriber = currentProperty.liveChatModel().showLiveChatButton.subscribe(function(showLiveChatButton) {
			// 					if (tabRequestId != TAB.GetCurrentRequestID()) {
			// 						subscriber.dispose();
			// 					} else if (showLiveChatButton) {
			// 						subscriber.dispose();
			// 						LiveChat_OpenChat();
			// 					}
			// 				});
			// 			}
			// 		}
			// 	});
			// }
		}
	}).catch(function(error) {
	    console.log(error);
	});
});

TAB.OnTabLoadError(function(name, options) {
	UpdateURLHash(name, options);

	// Set no tab is selected
	propertiesList.tabLoadedName('');
});

TAB.IsValidTab = (function() {
	var tabNames = ['my-account', 'payments', 'contact-profile'/*, 'faqs'*/];

	return function(name) {
		var currentProperty = propertiesList.currentProperty();
		return tabNames.indexOf(name) >= 0 && (!(propertiesList.currentProperty() && propertiesList.currentProperty().isHidePaymentsTab()) || name != 'payments');
	};
})();

function UpdateURLHash(tabName, tabOptions)
{
	// Update the URL hash with options included
	var hash = '#' + tabName;
	
	if (tabOptions && tabOptions.focusModule)
		hash += '/' + tabOptions.focusModule;
	
	window.location.hash = hash;
}

function LoadTab_MyAccount(onReady, options) {
	var currentProperty = propertiesList.currentProperty();
	
	currentProperty.RefreshInitialization();
	
	ko.applyBindingsToElement(currentProperty, $('.tab-my-account'));
	onReady();
}

function LoadTab_Payments(onReady, options) {
	debugger;
	var currentProperty = propertiesList.currentProperty();
	currentProperty.RefreshInitialization();
		
	ko.applyBindingsToElement(currentProperty, $('.tab-payments'));
	onReady();
	
	currentProperty.paymentModel().setIdentityAndOpaqueId();

	var tabRequestId = TAB.GetCurrentRequestID();
	
	currentProperty.paymentModel.getData().then(function() {
		if (tabRequestId == TAB.GetCurrentRequestID()) {
			var hasRenewalDiscount = currentProperty.paymentModel().duePayments().some(function(duePayment) {
				return duePayment.renewalDiscountAmount() > 0;
			});
			
			if (hasRenewalDiscount) {
				LookupMessage("renewalDiscount");
			}
		}
	}).catch(function(error){
		console.log(error);
	});

	// debugger;
	// currentProperty.paymentModel.setIdentityAndOpaqueId();
}

function LoadTab_ContactProfile(onReady, options) {
	var currentProperty = propertiesList.currentProperty();

	currentProperty.RefreshInitialization();
	
	ko.applyBindingsToElement(currentProperty, $('.tab-contact-profile'));
	onReady();
}

$(function() {
	debugger;
	ResetTimeout.initialize();
	
	var propertiesList = window.propertiesList = new PropertyList;

	//propertiesList.setIdentityAndOpaqueId();

	propertiesList.getProperties().catch(function(error) {
		console.log("There was an error loading properties.", error);
	}).then(function() {
		/***************************************************************************
		 * Default tab load
		 **************************************************************************/
		var tabToLoad = 'my-account';
		var tabLoadOptions = {};
		var tabStringInURL;
					
		if (window.location.hash && window.location.hash.length > 1) {
			tabStringInURL = window.location.hash.substring(1);
		}
		else if ($.QueryString.tab) {
			tabStringInURL = $.QueryString.tab;
		}
		else if (IsEmailCampaignPrecheckAutoPay()) {
			tabStringInURL = "payments";
		}
					
		if (tabStringInURL) {
			var tabPieces = tabStringInURL.split('/');
			
			if (TAB.IsValidTab(tabPieces[0])) {
				tabToLoad = tabPieces[0];
							
				if (tabPieces.length > 1) {
					tabLoadOptions.focusModule = tabPieces[1];
				}
			}
		}
		TAB.Load(tabToLoad, tabLoadOptions);
	});

	ko.applyBindings(propertiesList);
});