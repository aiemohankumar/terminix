function WebServicesErrorReportModel() {
	var self = this;

	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	
	self.filter = ko.observable();
	self.filteredResults = ko.computed(function() {
		if (!self.modelLoaded() || !self.model()) return [];
		
		var results = self.model().webServiceUsageWithFailures();
		var filter = self.filter();
		
		if (filter) {
			results = results.filter(function(item) {
				return item.mRequest().indexOf(filter) >= 0;
			});
		}
		
		return results;
	});

	ViewModel_AddCommonFunctions(self);

	self.LoadData = function() {
		CallDWR('MyAccountTMXUIUtils/getWebServicesErrorReport', null, function(data) {
			if (data.error) {
				alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
				return;
			}
			
			var json = data.model;
			
			self.model(ko.mapping.fromJS(json));
			
			if (self.model().redirectURL()) {
				Redirect(self.model().redirectURL());
				return;
			}
			
			self.modelLoaded(true);
			
			self.UpdateAutoFocus();
		});
	};
}

$(function() {
	window.vm = new WebServicesErrorReportModel();

	ko.applyBindings(vm);

	$('.web-services-error-report').show();

	vm.LoadData();
});
