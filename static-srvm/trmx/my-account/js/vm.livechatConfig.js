// MyAccountTMXLiveChatUIUtils

function ViewModel()
{
	var HISTORY_LIMIT = 100;
	var OFFSET_INTERVAL = 5;
	
	var self = this;
	
	ViewModel_AddCommonFunctions(self);
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	self.submitting = ko.observable(false);
	
	self.configTMX = ko.observable(null);
	self.historyOffset = ko.observable(0);
	
	self.CanViewMore = ko.computed(function()
	{
		var offset = self.historyOffset(),
			configTMX = self.configTMX();
		
		if (configTMX === null) return false;
		
		var history = ko.mapping.toJS(configTMX).history;
		
		return history && history.length > offset + OFFSET_INTERVAL;
	});
	
	self.CanViewLess = ko.computed(function()
	{
		return self.historyOffset() > 0;
	});
	
	self.ViewMore = function()
	{
		if (!self.CanViewMore()) return;
		
		self.historyOffset(self.historyOffset() + OFFSET_INTERVAL);
	};
	
	self.ViewLess = function()
	{
		self.historyOffset(0);
	};
	
	self.GetViewableHistory = ko.computed(function()
	{
		var offset = self.historyOffset(),
			configTMX = self.configTMX();
		
		if (configTMX === null) return [];
		
		var history = ko.unwrap(configTMX.history);
		
		if (!history || !history.length) return [];
		
		return history.slice(0, offset + OFFSET_INTERVAL);
	});
	
	var PadNum = function(number, length)
	{
		var out = '' + number;
		
		while (out.length < length)
			out = '0' + out;
		
		return out;
	};
	
	var GetModelJSON = function(options)
	{
		options = options || {};
		
		var json = ko.mapping.toJS(self.model);
		
		json.configTMX = ko.mapping.toJS(self.configTMX);
		
		if (options.addHistory)
		{
			// Don't keep history array in the history array data
			var historyData = JSON.parse(JSON.stringify(json.configTMX));
			delete historyData.history;
			
			var timestamp = new Date();
			
			var timestampString = PadNum(timestamp.getMonth() + 1, 2) + '/' + PadNum(timestamp.getDate(), 2) + '/' + timestamp.getFullYear();
				timestampString += ' @ ' + PadNum(timestamp.getHours(), 2) + ':' + PadNum(timestamp.getMinutes(), 2) + ':' + PadNum(timestamp.getSeconds(), 2);
			
			if (json.configTMX.history.length > HISTORY_LIMIT - 1)
			{
				json.configTMX.history.splice(HISTORY_LIMIT - 1);
			}
			
			json.configTMX.history.unshift({ timestamp: timestampString, user: json.username, data: JSON.stringify(historyData) });
		}
		
		json.configTMX = JSON.stringify(json.configTMX);
		
		return json;
	};
	
	var UpdateModel = function(model)
	{
		self.model(ko.mapping.fromJS(model));
		
		if (CheckRequiredConfigFields(model.configTMX))
		{
			var config = JSON.parse(model.configTMX);
			
			if (!config.history)
			{
				config.history = [];
			}
			
			self.configTMX(ko.mapping.fromJS(config));
		}
	};
	
	var CheckRequiredConfigFields = function(config)
	{
		// Make sure config is a JSON string
		try
		{
			config = JSON.parse(config);
		}
		catch (e)
		{
			return false;
		}
		
		// Make sure config is an object
		if (config === null || typeof config != 'object')
			return false;
		
		// Require the groups configuration fields
		if (!config.hasOwnProperty('groups') || config.groups === null || typeof config.groups != 'object')
			return false;
		
		// Require the Service group configuration fields
		if (!config.groups.hasOwnProperty('Service') || config.groups.Service === null || typeof config.groups.Service != 'object')
			return false;
		
		// Require fields on the Service group configuration
		if (!config.groups.Service.hasOwnProperty('throttlePercent'))
			return false;
		
		return true;
	};
	
	var ValidateConfig = function(configName)
	{
		config = ko.mapping.toJS(self[configName]);
		
		if (config === null)
			return true;
		
		if (config.groups.Service.throttlePercent < 0 || config.groups.Service.throttlePercent > 100)
		{
			self.AddFieldError(configName + '.groups.Service.throttlePercent', 'Percent must be 0-100.');
			return false;
		}
		
		return true;
	};
	
	self.LoadData = function()
	{
		UpdateModel(self.CreateDummyModel(
		{
			// Login process
			username: '',
			password: '',
			isAuthenticated: false,
			
			// Config process
			configTMX: ''
		}));
		
		self.modelLoaded(true);
	};
	
	self.HandleLogin = function(m, e)
	{
		if (self.submitting()) return;
		
		self.submitting(true);
		self.RemoveAllErrors();
		
		var json = GetModelJSON();
		
		CallDWR('MyAccountTMXLiveChatUIUtils/login', json, function(data)
		{
			if (data.error)
			{
				self.model().errorMessages.push({ errorMessage: data.error });
			}
			else
			{
				UpdateModel(data.model);
			}
			
			self.submitting(false);
		});
	};
	
	self.FormatHistoryData = function(data)
	{
		if (ko.isObservable(data))
			data = data();
		
		if (typeof data == 'string') {
			try {
				data = JSON.parse(data);
				
				if (!data || typeof data != 'object')
					data = {};
			}
			catch (e) {
				data = {};
			}
		}
		
		if (!data.groups) data.groups = {};
		if (!data.groups.Service) data.groups.Service = {};
		if (!data.groups.Service.throttlePercent) data.groups.Service.throttlePercent = 0;
		
		return data.groups.Service.throttlePercent + '%';
	};
	
	self.HandleConfig = function(m, e)
	{
		if (self.submitting()) return;
		
		self.submitting(true);
		self.RemoveAllErrors();
		
		var done = function()
		{
			self.submitting(false);
		};
		
		var invalid = !ValidateConfig('configTMX');
		
		if (invalid)
		{
			self.model().errorMessages.push({ errorMessage: 'There are errors with your changes.' });
			return done();
		}
		
		var json = GetModelJSON({ addHistory: true });
		
		CallDWR('MyAccountTMXLiveChatUIUtils/setConfig', json, function(data)
		{
			if (data.error)
			{
				self.model().errorMessages.push({ errorMessage: data.error });
			}
			else
			{
				UpdateModel(data.model);
			}
			
			done();
		});
	};
}

$(function()
{
	window.vm = new ViewModel();
	
	ko.applyBindings(vm);
	
	$('.on-knockout-loaded').show();
	
	vm.LoadData();
});