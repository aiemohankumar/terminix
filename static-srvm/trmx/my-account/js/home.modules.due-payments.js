/*
 * Module: Due Payments
 ***********************************************************************************************************/
TAB.Modules.Add('due-payments', function($module, options, onLoaded) {
	var preCheckAutoPay = IsEmailCampaignPrecheckAutoPay();
	
	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		// Remove self.LoadData = function(){} from the callback() param when
		// setting LoadDataDWR
		LoadDataDWR : 'MyAccountTMXPaymentUIUtils/getPaymentDueData',

		OnGetLoadData : function(self, json) {
			TAB.Modules.API('invoice-history', 'setPaymentDueData', $.extend({}, json));
			TAB.Modules.API('payment-and-invoice-history', 'setPaymentDueData', $.extend({}, json));
			TAB.Modules.API('yia-pay', 'setPaymentDueData', $.extend({}, json));
		},

		api : {
			refreshStoredPaymentMethods : function(self, apiData) {
				var UpdateStoredPaymentMethods = function(storedPaymentMethods) {
					// Find out if there are any new or updated stored payment
					// methods
					if (!self.model())
						return;

					storedPaymentMethods.forEach(function(paymentMethod) {
						if (typeof self.model().paymentMethods === 'function') {
							var existingPaymentMethod = ko.utils.arrayFirst(self.model().paymentMethods(), function(existingPaymentMethod) {
								return existingPaymentMethod.type() == 'saved' && existingPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId;
							});

							if (existingPaymentMethod) {
								existingPaymentMethod.description(paymentMethod.description);
							} else {
								self.model().paymentMethods.push(ko.mapping.fromJS({
									type : 'saved',
									description : paymentMethod.description,
									paymentMethodId : paymentMethod.paymentMethodId,
									customerNumber : paymentMethod.customerNumber
								}));
							}
						}
					});

					// Find out if any stored payment methods were removed
					if (typeof self.model().paymentMethods === 'function') {
						self.model().paymentMethods().forEach(function(paymentMethod) {
							if (paymentMethod.type() != 'saved')
								return;

							var newPaymentMethod = ko.utils.arrayFirst(storedPaymentMethods, function(newPaymentMethod) {
								return newPaymentMethod.paymentMethodId == paymentMethod.paymentMethodId();
							});

							if (!newPaymentMethod) {
								// If this was the selected payment method, then
								// the
								// user has to go back to payment method
								// selection

								if (self.selectedPaymentMethod() && self.selectedPaymentMethod().paymentMethodId() == paymentMethod.paymentMethodId()) {
									self.CancelPayment();
								}

								// Remove this payment method

								self.model().paymentMethods.remove(paymentMethod);
							}
						});
					}
				};

				if (typeof apiData.storedPaymentMethods != 'undefined') {
					UpdateStoredPaymentMethods(apiData.storedPaymentMethods);
				} else {
					CallDWR('my-account-json/getStoredPaymentMethods.json', {}, function(data) {
						if (data.error) {
							// What do.
							return;
						}

						// Check if there is no array to work with
						if (!data.model.storedPaymentMethods || !IsArray(data.model.storedPaymentMethods)) {
							// What do.
							return;
						}

						UpdateStoredPaymentMethods(data.model.storedPaymentMethods);
					});
				}
			},

			updateEasyPaySalesAgreements : function(self, apiData) {
				if (!self.model() || !self.model().duePayments().length)
					return;

				CallDWR('MyAccountTMXPaymentUIUtils/getPaymentDueData', {}, function(data) {
					if (data.model && data.model.duePayments && data.model.duePayments.length) {
						var modelDuePayments = self.model().duePayments();

						data.model.duePayments.forEach(function(duePayment) {
							modelDuePayments.forEach(function(modelDuePayment) {
								if (modelDuePayment.salesAgreementNumber() == duePayment.salesAgreementNumber) {
									modelDuePayment.isEligibleForEasyPay(duePayment.isEligibleForEasyPay);
								}
							});
						});
					}
				});
			}
		},

		callback : function(self) {
			function SetRenewalStep(data) {
				CallDWR("MyAccountTMXPaymentUIUtils/setRenewalStepOnUserSession", data);
			}
			
			self.selectedPaymentMethod = ko.observable(null);

			self.selectedPaymentMethod.subscribe(function(value) {
				self.RemoveAllErrors();
				
				var hasSelectedPaymentMethod = !!(value && ko.unwrap(value.type));
				
				if (hasSelectedPaymentMethod) {
					// Check if starting a renewal
					var renewalDuePayment = self.model().duePayments().find(function(duePayment) {
						return duePayment.termiteRenewal() && parseFloat(duePayment.amount()) > 0;
					});
					
					if (renewalDuePayment) {
						SetRenewalStep({
							step: "step_1",
							salesAgreementNumber: renewalDuePayment.salesAgreementNumber(),
							invoiceId: renewalDuePayment.invoiceNumber()
						});
					}
				}
				
				if (hasSelectedPaymentMethod && self.CanSetupEasyPay() && preCheckAutoPay) {
					self.model().setupEasyPay(true);
				} else {
					self.model().setupEasyPay(false);
				}
			});	
			
			self.CheckStartRenewal = function(duePayment) {
				if (duePayment.termiteRenewal()) {
					SetRenewalStep({
						step: "step_1",
						salesAgreementNumber: duePayment.salesAgreementNumber(),
						invoiceId: duePayment.invoiceNumber()
					});
				}
			};

			self.UpdateSelectedPaymentMethod = function() {
				self.selectedPaymentMethod(null);

				if (self.model().selectedPaymentMethod.type()) {
					var selectedPaymentMethod = self.model().selectedPaymentMethod;

					self.model().paymentMethods().forEach(function(paymentMethod) {
						if (paymentMethod.type() == selectedPaymentMethod.type() && paymentMethod.description() == selectedPaymentMethod.description()) {
							self.selectedPaymentMethod(paymentMethod);
						}
					});
				}
			};
			
			self.termiteRenewalData = ko.computed(function() {
				if(!self.model()) 
					return [];
				
				return self.model().duePayments().filter(function(x) { return x.termiteRenewal });
			});

			self.sortedPaymentMethods = ko.computed(function() {
				if (!self.model())
					return [];

				var paymentMethods = self.model().paymentMethods();

				return paymentMethods.slice(0).sort(function(a, b) {
					var typeA = a.type(), typeB = b.type();

					if (typeA == typeB)
						return ('' + a.description()).localeCompare('' + b.description());
					if (typeA == 'saved' || typeA == '')
						return -1;
					if (typeB == 'saved' || typeB == '')
						return 1;
					if (typeA == 'bank')
						return -1;
					return 1;
				});
			});

			// Override UpdateModel so we can hook due payment amounts to
			// automatically update the total
			var UpdateModel = self.UpdateModel;

			self.UpdateModel = function(json) {
				if (json.duePayments && json.duePayments.length > 0) {
					json.duePayments.forEach(function(item) {
						item.documentId = item.documentId || null;
						item.downloadLink = item.downloadLink || ''; // Only
						// use
						// for
						// testing
						// purposes
						item.loading = false;
						item.showDiscountError = false;
					});
				}
				UpdateModel(json);
				var renewalDiscount = false;
				if (self.model()) {
					if (ko.isObservable(self.model().duePayments)) {
						self.model().duePayments().forEach(function(duePayment) {
							if (duePayment.amount && duePayment.amount.subscribe) {
								duePayment.amount.subscribe(function(newValue) {
									UpdateTotalAmount();

									self.RemoveAllErrors();
								});
								if (duePayment.renewalDiscountAmount() > 0) {// &&
									// duePayment.amountDue()
									// ==
									// duePayment.totalAmount()){
									renewalDiscount = true;
								}
							}
						});
					}

					if (ko.isObservable(self.model().setupEasyPay)) {
						self.model().setupEasyPay.subscribe(function(newValue) {
							self.model().savePaymentMethod(newValue);
						});
					}
					if (renewalDiscount) {
						LookupMessage("renewalDiscount");
					}
				}

				UpdateTotalAmount();
				self.UpdateSelectedPaymentMethod();
			};

			var UpdateTotalAmount = function() {
				if (self.model()) {
					var total = 0;

					self.model().duePayments && self.model().duePayments().forEach(function(duePayment) {
						total += parseFloat(duePayment.amount()) || 0;
					});

					self.model().paymentTotal(total);

					if (total == 0) {
						// Reset any payment method data
						self.CancelPayment();
						self.model().selectedPaymentMethod.type('');
					}
				}
			};

			self.CheckInvoiceLink = function(item, event) {
				// If it already has a link, then allow the link to work
				if (item.downloadLink())
					return true;

				// If already loading the link, do nothing
				if (item.loading())
					return false;

				// Load the link
				item.loading(true);

				var json = ko.mapping.toJS(item);

				CallDWR('MyAccountTMXPaymentUIUtils/getInvoiceDocumentLink', json, function(data) {
					if (data.error) {
						CreateAlert({
							color : 'red',
							button : false,
							closeButton : true,
							message : "There was a problem retrieving the document link for your invoice. Please try again.",
							scrollToAlerts : true,
							$module: $module
						});
					} else if (data.model) {
						item.downloadLink(data.model.downloadLink);

						window.open(item.downloadLink(), '_blank');
					}

					item.loading(false);
				});
			};

			self.ShowDetail = function(item) {
				item.showDetail(!item.showDetail());

				return self;
			};

			self.PayTotalBalance = function() {
				if (self.model()) {
					self.model().duePayments && self.model().duePayments().forEach(function(duePayment) {
						var amount;
						
						if (duePayment.amountDue() == duePayment.totalAmount() && duePayment.renewalDiscountedTotalAmount() > 0) {
							amount = duePayment.renewalDiscountedTotalAmount();
						} else {
							amount = duePayment.amountDue();
						}
						
						// Format amount to 2 decimal places
						duePayment.amount(amount.toFixed(2));
					});
				}

				return self;
			};

			self.updateShowDiscount = function(item) {
				item.showDiscountError(item.renewalDiscountAmount() > 0 && item.amount() > 0 && item.amount() < item.renewalDiscountedTotalAmount());

				return self;
			};

			self.GetAvailableCreditCardYears = function() {
				var years = [];

				for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
					years.push(i);
				}

				return years;
			};

			self.SetVisibleHelp = function(field) {
				self.model().visibleHelp(field);

				return self;
			};

			self.IsVisibleHelp = function(field) {
				return self.model().visibleHelp() == field;
			};

			self.CanSetupEasyPay = function() {
				// Check if we're paying for something
				var duePaymentsBeingPaid = self.model().duePayments().filter(function(duePayment) {
					return duePayment.amount() > 0;
				});

				if (duePaymentsBeingPaid.length == 0) {
					return false;
				}

				// Check if paying more than one sales agreement
				var salesAgreements = duePaymentsBeingPaid.map(function(duePayment) {
					return duePayment.salesAgreementNumber();
				});

				salesAgreements = salesAgreements.filter(function(salesAgreement, index) {
					return salesAgreements.indexOf(salesAgreement) == index;
				});

				if (salesAgreements.length > 1) {
					return false;
				}

				// Check if the sales agreement being paid for is eligible for
				// EasyPay
				return duePaymentsBeingPaid[0].isEligibleForEasyPay();
			};

			self.IsValidPaymentMethod = function() {
				if (!self.selectedPaymentMethod())
					return false;

				var item = self.model();

				switch (self.selectedPaymentMethod().type()) {
				case 'saved':
					return true;

				case 'card':
					if (!item.cardNumber()
					// || !item.cardSecurity() // Disabled from TMA-349
					|| !item.cardExpireMonth() || !item.cardExpireYear()
					|| !item.billingAddress1() || !item.billingCity() || !item.billingState() || !item.billingZipcode())
						return false;
					break;

				case 'bank':
					if (!item.bankRoutingNumber() || !item.bankAccountNumber())
						return false;
					break;
				}

				return true;
			};

			self.CancelPayment = function(m, e) {
				self.RemoveAllErrors();

				// Empty out other fields that may have been populated
				var fieldsToRevert = [];

				if (self.selectedPaymentMethod()) {
					switch (self.selectedPaymentMethod().type()) {
					case 'saved': {
						fieldsToRevert = [ 'visibleHelp' ];
						break;
					}
					case 'onetime': {
						fieldsToRevert = [ 'visibleHelp', 'cardNumber', 'cardType', 'cardSecurity', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1',
								'billingAddress2', 'billingCity', 'billingState', 'billingZipcode' ];
						break;
					}
					case 'bank': {
						fieldsToRevert = [ 'visibleHelp', 'bankRoutingNumber', 'bankAccountNumber', 'billingAddress1', 'billingAddress2', 'billingCity',
								'billingState', 'billingZipcode' ];
						break;
					}
					}

					fieldsToRevert.forEach(function(field) {
						self.model()[field](self.originalModel()[field]());
					});

					self.selectedPaymentMethod(null);
				}

				// Scroll back to payment method choices for mobile view
				var position = $module.find('.payment-summary').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}

				return self;
			};

			self.SubmitPayment = function(m, e) {
				if (!self.IsValidPaymentMethod() || self.submitting())
					return;

				self.RemoveAllErrors();

				var json = ko.mapping.toJS(self.model);

				json.selectedPaymentMethod = ko.mapping.toJS(self.selectedPaymentMethod);

				self.submitting(true);

				if (json.setupEasyPay) {
					self.SignUpForAutoPay(json);
				}

				if (json.savePaymentMethod) {
					self.storePaymentMethod(json);
				}

				CallDWR('MyAccountTMXPaymentUIUtils/makePayment', json, function(data) {
					self.submitting(false);

					if (data.error) {
						self.model().errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					json = data.model;

					self.UpdateModel(json);

					// HACK: Since we have subscribers removing all messages
					// when some fields change like:
					// - due payment amount being paid changes
					// - payment method changes
					// - payment is cancelled
					// So we have to update the messages again after we
					// UpdateModel because those fields above will trigger
					// and will remove all of the messages from the response

					self.model().errorMessages(ko.unwrap(ko.mapping.fromJS(json.errorMessages)));
					self.model().fieldValidationErrors(ko.unwrap(ko.mapping.fromJS(json.fieldValidationErrors)));
					// self.model().informationalMessages(ko.unwrap(ko.mapping.fromJS(json.informationalMessages)));

					// END OF HACK

					if (self.model().redirectURL()) {
						setTimeout(function() {
							Redirect(self.model().redirectURL());

						}, 1500);
					}

					if ((ko.unwrap(ko.mapping.fromJS(json.informationalMessages))).length) {
						var informationalMessages = ko.mapping.toJS(ko.unwrap(ko.mapping.fromJS(json.informationalMessages)));

						self.model().informationalMessages([]);

						informationalMessages.forEach(function(infoMessage, index) {
							if (!infoMessage.messageColor) {
								infoMessage.messageColor = 'green';
							}
							CreateAlert({
								color : infoMessage.messageColor,
								message : infoMessage.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : (index == 0),
								$module: $module
							// scroll to alerts only for first infoMessage so we
							// don't scroll every message created
							});
						});
								
						self.ReloadModule();

						TAB.Modules.Reload('payment-history');
						TAB.Modules.Reload('yia-pay');
						
						$('.red-termite').remove();
						
						CallDWR('MyAccountTMXPaymentUIUtils/getPaymentDueData', {}, function(data) {		
							if (data.model) {	
									data.model.duePayments.forEach(function(duePayment) {
										if (duePayment.termiteRenewal) {
											CreateAlert({
												color: 'red-termite',
												message: "Your " + duePayment.description + " is set to expire soon!",
												messageIsHTML: true,
												button: true,
												closeButton: true,
												customCloseButton: 'termite-renewal',
												icon: 'icon-warning',
												onCloseAlert: function(e) {
													var jsonData = { messageType: 'TMX_RNL_ALERT', serviceLine: duePayment.serviceLine };
													CallDWR('MyAccountTMXUIUtils/dismissTMXMyAccountMessage', jsonData, function(data) {});
												}
											});
										}	
									})
							}
						});

						// TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
						// TAB.Modules.API('*', 'refreshStoredPaymentMethods');
					}
				});

				return self;
			};

			self.SignUpForAutoPay = function(json) {

				CallDWR('MyAccountTMXPaymentUIUtils/setupEasyPay', json, function(data) {
					if (data.error) {
						self.model().errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					var json = data.model;
						
					self.model().errorMessages(ko.unwrap(ko.mapping.fromJS(json.errorMessages)));
					self.model().fieldValidationErrors(ko.unwrap(ko.mapping.fromJS(json.fieldValidationErrors)));
					self.model().informationalMessages(ko.unwrap(ko.mapping.fromJS(json.informationalMessages)));
					if ((ko.unwrap(ko.mapping.fromJS(json.informationalMessages))).length) {
						var informationalMessages = ko.mapping.toJS(ko.unwrap(ko.mapping.fromJS(json.informationalMessages)));

						self.model().informationalMessages([]);

						informationalMessages.forEach(function(infoMessage, index) {
							if (!infoMessage.messageColor) {
								infoMessage.messageColor = 'green';
							}
							CreateAlert({
								color : infoMessage.messageColor,
								message : infoMessage.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : false,
								$module: $module
							// scroll to alerts only for first infoMessage so we
							// don't scroll every message created
							});
						});
					}
				
					TAB.Modules.API('*', 'updateEasyPaySalesAgreements');
				});

			};

			self.storePaymentMethod = function(json) {
				CallDWR('MyAccountTMXPaymentUIUtils/storePaymentMethod', json, function(data) {
					if (data.error) {
						self.model().errorMessages.push({
							errorMessage : data.error
						});
						return;
					}
					
					var json = data.model;
					
					self.model().errorMessages(ko.unwrap(ko.mapping.fromJS(json.errorMessages)));
					self.model().fieldValidationErrors(ko.unwrap(ko.mapping.fromJS(json.fieldValidationErrors)));
					
					if (json.informationalMessages && json.informationalMessages.length) {
						json.informationalMessages.forEach(function(infoMessage, index) {
							if (!infoMessage.messageColor) {
								infoMessage.messageColor = 'green';
							}
							CreateAlert({
								color : infoMessage.messageColor,
								message : infoMessage.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : (index == 0),
								$module: $module
							// scroll to alerts only for first infoMessage so we
							// don't scroll every message created
							});
						});
					}
					
					TAB.Modules.API('*', 'refreshStoredPaymentMethods');
				});
			}
		}
	});

	ko.applyBindings(vm, $module.get(0));

	vm.LoadData(onLoaded);
});