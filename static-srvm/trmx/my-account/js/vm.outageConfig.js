function ViewModel() {
	var self = this;

	ViewModel_AddCommonFunctions(self);
	
	var VIEW_SCHEDULE = self.VIEW_SCHEDULE = 'schedule';
	var VIEW_ADD_FORM = self.VIEW_ADD_FORM = 'add_form';
	var VIEW_EDIT_FORM = self.VIEW_EDIT_FORM = 'edit_form';
	
	self.loaded = ko.observable(false);
	self.error = ko.observable();
	
	var _view = ko.observable(VIEW_SCHEDULE);
	self.view = ko.computed(function() {
		return _view();
	});
	
	self.scheduledOutages = ko.observableArray();
	self.outageAddEdit = ko.observable();
	
	function FormatDateForDisplay(date) {
		var text = '';
		
		if (date) {
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			
			text = months[date.getMonth()] + '. ' + date.getDate();
			
			var hours24 = date.getHours();
			
			text += ' at ' + ((hours24 + 11) % 12 + 1) + ':' + ('00' + date.getMinutes()).substr(-2) + ' ' + (hours24 < 12 ? 'AM' : 'PM');
		}
		
		return text;
	}
	
	self.LoadData = function() {
		setTimeout(function() {
			var json = {
				scheduledOutages: [{
					enabled: true,
					description: 'STAR Refresh 1',
					dates: {
						start: '04/03/2018 22:00:00 -0500',
						stop: ''
					}
				}, {
					enabled: true,
					description: 'STAR Refresh 2',
					dates: {
						start: '06/12/2018 22:00:00 -0500',
						stop: '06/14/2018 06:00:00 -0500'
					}
				}, {
					enabled: false,
					description: 'STAR Refresh 3',
					dates: {
						start: '06/23/2018 22:00:00 -0500',
						stop: '06/25/2018 06:00:00 -0500'
					}
				}],
			};
			
			json.scheduledOutages.forEach(function(item) {
				item.dates_objects = {
					start: item.dates.start && new Date(item.dates.start) || null,
					stop: item.dates.stop && new Date(item.dates.stop) || null
				};
				
				item.dates_display = {
					start: FormatDateForDisplay(item.dates_objects.start),
					stop: FormatDateForDisplay(item.dates_objects.stop),
					combined: 'always'
				};
				
				if (item.dates_display.start) {
					if (item.dates_display.stop) {
						item.dates_display.combined = item.dates_display.start + ' - ' + item.dates_display.stop;
					} else {
						item.dates_display.combined = 'starts at ' + item.dates_display.start;
					}
				} else if (item.dates_display.stop) {
					item.dates_display.combined = 'ends at ' + item.dates_display.start;
				}
			});
			
			self.scheduledOutages(json.scheduledOutages);
			self.loaded(true);
		}, 2000);
	};
	
	function CreateFormItem(data) {
		data = $.extend({
			enabled: false,
			description: '',
		}, data);
		
		self.outageAddEdit(ko.mapping.fromJS(data));
	}
	
	self.ShowAddForm = function() {
		CreateFormItem();
		
		_view(VIEW_ADD_FORM);
	};
	
	self.ShowEditForm = function(item) {
		CreateFormItem(data);
		
		_view(VIEW_EDIT_FORM);
	};
	
	self.CancelAddForm = function() {
		self.outageAddEdit(null);
		
		_view(VIEW_SCHEDULE);
	};
	
	self.CancelEditForm = function() {
		self.outageAddEdit(null);
		
		_view(VIEW_SCHEDULE);
	};
	
	self.SubmitAddForm = function() {
		//
	};
	
	self.SubmitEditForm = function() {
		//
	};
	
	self.CopyOutageMessage = function(item) {
		//
	};
	
	self.RemoveOutageMessage = function(item) {
		//
	};
}

$(function() {
	window.vm = new ViewModel();

	ko.applyBindings(vm);
	
	vm.LoadData();
});
