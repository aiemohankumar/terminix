function ViewModel() {
    var self = this;

    self.model = ko.observable(null);
    self.modelLoaded = ko.observable(false);

    self.recaptchaToken = ko.observable();

    ViewModel_AddCommonFunctions(self);

    var checker = IsChatAvailableChecker(function (data) {
        if (LiveChat_IsEnabledFor('Login')) {
            self.isLiveChatEnabled(data.areAgentsAvailable || data.isVisitorEngaged);
        } else {
            checker.remove();
        }
    });

    self.LoadData = function () {
        CallDWR('MyAccountTMXLoginUIUtils/getLoginData', null, function (data)
            //        CallDWR('getLoginData.json', null, function(data)
            {
                if (data.error) {
                    alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
                    return;
                }

                var json = data.model;

                if ($.QueryString.badRescheduleToken) {
                    json.errorMessages = json.errorMessages || [];
                    json.errorMessages.push({
                        errorMessage: "Your link has expired. Please login to reschedule your appointment."
                    });
                }

                self.model(ko.mapping.fromJS(json));

                if (self.model().redirectURL()) {
                    Redirect(self.model().redirectURL());
                    return;
                }

                self.modelLoaded(true);

                self.UpdateAutoFocus();
            });
    };

    self.IsValidLogin = function () {
        if (!self.model().login() ||
            !self.model().password())
            return false;

        return true;
    };


    self.SetVisibleHelp = function (field) {
        self.model().visibleHelp(field);

        return self;
    };

    self.IsVisibleHelp = function (field) {
        return self.model().visibleHelp() == field;
    };

    self.Login = function (m, e) {
        if (!self.IsValidLogin() || self.submitting()) return;

        self.RemoveAllErrors();

        var model = ko.mapping.toJS(self.model);

        model.recaptchaToken = self.recaptchaToken();
        //model.recaptchaToken = self.recaptchaToken("03ADlfD19DA9a8N0_Je7nO5MQhgPtfU9A_GsOMHZdDA0DCZoVi0EJ0PrkCwjgTst7wjmRr38Ymnpc15ijNXFW1aL6NxOmbCCjdHC2DjUuVBhZBfsKZsM8mfVatQ86yw1Gan0NXhiMSWxESrlJVLqb3vh43T-q7FrWYpEYFQN9xGWpzReBzyOJAC_6dY_WzO1rSytYyCKqCeFzzLIjz8EjBg5iQ9hIjiTfrVL_xX5sBkI-tJRXozPLdXjqxtMuB-oXG5B6dE3eIRmrZ8pIc3YTOXzchao-BGit2Bw");

        if ($("#recaptcha-btn").length && !model.recaptchaToken) {
            $("#recaptcha-btn").click();
            return false;
        }

        self.submitting(true);

        if ($.QueryString.tab) {
            model.tab = $.QueryString.tab;
        }
        // Redirect to payments tab if they saw the email campaign
        else if ($.QueryString.utm_campaign && IsEmailCampaignPrecheckAutoPay($.QueryString.utm_campaign)) {
            model.tab = "payments";
        }

        CallDWR('MyAccountTMXLoginUIUtils/setLoginData', model, function (data)
            //        CallDWR('setLoginData.json', model, function(data)
            {
                self.submitting(false);

                if (data.error) {
                    self.model().errorMessages.removeAll();
                    self.model().errorMessages.push({
                        errorMessage: data.error
                    });
                    return;
                }

                var json = data.model;

                // If recaptcha token was unset on backend, then remove it on front-end
                // Also clear out recaptcha response if there was one
                if (!json.recapchaToken && model.recaptchaToken) {
                    self.recaptchaToken('');

                    if (grecaptcha.getResponse()) {
                        grecaptcha.reset();
                    }
                }

                self.model(ko.mapping.fromJS(json));

                var redirectURL = self.model().redirectURL();

                if ($.QueryString.redirect) {
                    redirectURL = $.QueryString.redirect;
                }

                if (redirectURL) {
                    setTimeout(function () {
                        Redirect(redirectURL);

                    }, 1500);
                    return;
                }

                self.UpdateAutoFocus();
            });
    };
}

function loginRecaptchaCallback(data) {
    vm.recaptchaToken(data);
    vm.Login();
}

$(function () {
    window.vm = new ViewModel();

    ko.applyBindings(vm);

    $('.on-knockout-loaded').show();

    vm.LoadData();

    // Remember the email campaign for later
    document.cookie = "utm_campaign=" + encodeURIComponent($.QueryString.utm_campaign || '') + "; path=/";
});
