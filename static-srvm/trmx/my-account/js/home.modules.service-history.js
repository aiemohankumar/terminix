/*
 * Module: Service History
 ***********************************************************************************************************/
TAB.Modules.Add('service-history', function($module, options, onLoaded)
{

	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,

		 LoadDataDWR: 'my-account-json/getMyServiceHistory.json',
		//https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/getMyServiceHistory.json
		OnGetLoadData: function(self, json)
		{
			// 1. Fix the technician name from being "last, first" to be "first last"
			// 2. Add model information for the document links pulled
			if (json && json.history && json.history.length > 0)
			{
				json.history.forEach(function(item)
				{
					// Split the name by comma and remove trailing/leading whitespace
					var namePieces = item.technician.split(',').map(function(piece) { return piece.trim(); });
					
					// Rearrange name in reverse order
					item.technician = namePieces.reverse().join(' ');
					
					// Document model information
					item.documentModel = {
						loaded: false,
						loading: false,
						hasDocuments: false,
						hasError: false,
						link: '',
					};
				});
			}
		},
		
		callback: function(self)
		{
			self.useNewDocumentDownload = GetFeatureToggleDefaultFalse("serviceHistoryNewDocumentDownload");
			
			self.GetViewableHistory = function()
			{
				return self.model().history().slice(0, self.model().numViewable());
			};
			
			function FetchDocuments(item)
			{
				// Load the document data
				item.documentModel.loading(true);
				
				var json = { workOrderId: item.workOrderActivityId() };
				
				CallDWR('MyAccountTMXUIUtils/checkIfDocumentsExist', json, function(data)
				{
					if (data.error)
					{
						item.documentModel.hasError(true);
					}
					else if (data.model)
					{
						if (data.model.errorMessages && data.model.errorMessages.length)
						{
							item.documentModel.hasError(true);
						}
						else
						{
							item.documentModel.hasDocuments(data.model.hasDocuments);
							item.documentModel.link(data.model.documentDownloadUrl);
						}
					}
					
					item.documentModel.loading(false);
					item.documentModel.loaded(true);
				});
			}
			
			self.ShowDetail = function(item)
			{
				item.showDetail(!item.showDetail());
				
				if (item.showDetail() && self.useNewDocumentDownload && !item.documentModel.loaded() && !item.documentModel.loading())
				{
					FetchDocuments(item);
				}
			};
			
			self.RetryFetchDocuments = function(item)
			{
				if (self.useNewDocumentDownload && item.documentModel.loaded() && !item.documentModel.loading() && item.documentModel.hasError())
				{
					item.documentModel.loaded(false);
					item.documentModel.hasError(false);
					
					FetchDocuments(item);
				}
			};
			
			self.PayNow = function(item)
			{
				TAB.Load('payments', { focusModule: 'due-payments' });
			};
			
			self.CanViewMore = function()
			{
				if (!self.model())
					return false;
				
				return self.model().numViewable() < self.model().history().length;
			};
			
			self.CanViewLess = function()
			{
				if (!self.model() || !self.originalModel())
					return false;
				
				return self.model().numViewable() != self.originalModel().numViewable();
			};
			
			self.ViewMore = function()
			{
				self.model().numViewable(Math.min(self.model().numViewable() + 5, self.model().history().length));
				
				return self;
			};
			
			self.ViewLess = function()
			{
				self.model().numViewable(self.originalModel().numViewable());
				
				return self;
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});