function ViewModel(rememberMeToken)
{
	var self = this;
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	
	self.invalidToken = ko.observable(false);
	self.submitted = ko.observable(false);
	
	debugger;
	ViewModel_AddCommonFunctions(self);
	
	self.passwordIsValid = ko.observable(false);
	self.confirmPasswordIsValid = ko.observable(false);
	
	self.LoadData = function()
	{
		debugger;
		CallDWR('MyAccountTMXLoginUIUtils/checkToken', { token: rememberMeToken }, function(data)
		{
			if (data.error)
			{
				alert('Error loading the page: ' + data.error + "\n\nPlease refresh and try again.");
				return;
			}
			
			var json = data.model;

			self.model(ko.mapping.fromJS(json));
			
			if (self.model().redirectURL())
			{
				Redirect(self.model().redirectURL());
				return;
			}
			
			self.invalidToken(self.model().errorMessages().length > 0);
			
			self.modelLoaded(true);
			
			self.UpdateAutoFocus();
		});
		
		/*if ($.QueryString.autofill)
		{
			self.model(ko.mapping.fromJS(self.CreateDummyModel({
				password: 'aaAA11!!',
				confirmPassword: 'aaAA11!!'
			})));
		}
		else
		{
			self.model(ko.mapping.fromJS(self.CreateDummyModel({
				password: '',
				confirmPassword: ''
			})));
		}
		
		self.modelLoaded(true);
		
		self.UpdateAutoFocus();*/
	};
	
	self.IsValidForm = function()
	{
		if (!self.passwordIsValid() || !self.confirmPasswordIsValid())
			return false;
		
		return true;
	};
	
	self.Submit = function(m, e)
	{
		if (!self.IsValidForm() || self.submitted() || self.submitting()) return;
		
		self.RemoveAllErrors();
		
		self.submitting(true);
		
		var json = ko.mapping.toJS(self.model);
		
		CallDWR('MyAccountTMXLoginUIUtils/resetPassword', json, function(data)
		{
			self.submitting(false);
			
			if (data.error)
			{
				self.model().errorMessages.removeAll();
				self.model().errorMessages.push({ errorMessage: data.error });
				return;
			}
			
			json = data.model;
			
			// DUMMY DATA
			//json.informationalMessages.push({ infoMessage: 'An email has been sent to ' + json.email + ' with instructions on how to reset your password.' });

			self.model(ko.mapping.fromJS(json));
			
			if (self.model().redirectURL())
			{
				setTimeout(function()
				{
					Redirect(self.model().redirectURL());
					
				}, 1500);
				return;
			}
			
			self.submitted(self.model().errorMessages().length == 0 && self.model().fieldValidationErrors().length == 0);
			
			self.UpdateAutoFocus();
		});
	};
}

$(function()
{
	window.vm = new ViewModel($.QueryString.token);
	
	ko.applyBindings(vm);
	
	$('.on-knockout-loaded').show();
	
	vm.LoadData();
});