/*
 * Module: Contact Info Large
 ***********************************************************************************************************/
TAB.Modules.Add('contact-info-large', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,
		
		LoadDataDWR: 'my-account-json/getContactInfo.json',
		
		callback: function(self)
		{
			self.QAS = {
				isChoosingAddress: ko.observable(false),
				isChoosingEmail: ko.observable(false),
				address: ko.observable(null),
				addresses: ko.observableArray(),
				emails: ko.observableArray(),
				selectedAddress: ko.observable(null),
				selectedEmail: ko.observable(null),
				lastValidatedAddress: null,
				lastValidatedEmail: null,
				isVerifying: ko.observable(false),
				
				KeepAddress: function()
				{
					self.QAS.lastValidatedAddress = ko.mapping.toJS(self.model);
					
					self.QAS.isChoosingAddress(false);
					
					self.Submit();
				},
				
				KeepEmail: function()
				{
					self.QAS.lastValidatedEmail = self.model().email();
					
					self.QAS.isChoosingEmail(false);
					
					self.Submit();
				},
				
				UseSelectedAddress: function()
				{
					if (self.QAS.selectedAddress())
					{
						self.model().address1(self.QAS.selectedAddress().address1);
						self.model().address2(self.QAS.selectedAddress().address2);
						self.model().city(self.QAS.selectedAddress().city);
						self.model().state(self.QAS.selectedAddress().state);
						self.model().zipcode(self.QAS.selectedAddress().zipcode);
						
						self.QAS.lastValidatedAddress = self.QAS.selectedAddress();
					}
					
					self.QAS.isChoosingAddress(false);
					
					self.Submit();
				},
				
				UseSelectedEmail: function()
				{
					if (self.QAS.selectedEmail())
					{
						self.model().email(self.QAS.selectedEmail());
						
						self.QAS.lastValidatedEmail = self.QAS.selectedEmail();
					}
					
					self.QAS.isChoosingEmail(false);
					
					self.Submit();
				},
				
				IsLastValidatedAddress: function()
				{
					if (!self.QAS.lastValidatedAddress)
						return false;
					
					var address = ko.mapping.toJS(self.model); // address fields exist on model
					
					return address.address1 == self.QAS.lastValidatedAddress.address1
					    && address.address2 == self.QAS.lastValidatedAddress.address2
					    && address.city     == self.QAS.lastValidatedAddress.city
					    && address.state    == self.QAS.lastValidatedAddress.state
					    && address.zipcode  == self.QAS.lastValidatedAddress.zipcode;
				},
				
				IsLastValidatedEmail: function()
				{
					return self.QAS.lastValidatedEmail == self.model().email();
				}
			};
			
			self.GetAddressLine = function(address)
			{
				var address1 = ko.unwrap(address.address1),
					address2 = ko.unwrap(address.address2);
				
				return address2 ? (address1 + ' ' + address2) : address1;
			};
			
			self.HasModelChanged = function()
			{
				if (!self.model() || !self.originalModel())
					return false;
				
				// Ignore the service address
				
				var inputs = ['firstName', 'lastName', 'address1', 'address2', 'city', 'zipcode', 'email'];
				
				for (var i = 0; i < inputs.length; i++)
				{
					if (self.model()[inputs[i]]() != self.originalModel()[inputs[i]]())
					{
						//console.log('Model is different on ' + inputs[i] + ':', self.model()[inputs[i]](), self.originalModel()[inputs[i]]());
						return true;
					}
				}
				
				// Phone is a special case because sometimes it compares the formatted phone vs unformatted phone
				var oldPhone = ('' + self.originalModel().phone()).replace(/[^\d]/g, ''),
					newPhone = ('' + self.model().phone()).replace(/[^\d]/g, '');
				
				if (oldPhone != newPhone)
				{
					//console.log('Model is different on phone:', newPhone, oldPhone);
					return true;
				}
				
				// State is a special case because the dropdown automatically sets it to an empty string if the value is not a valid option
				// If states are different, or if having a state before is different than now
				var oldState = self.originalModel().state(),
					newState = self.model().state();
				
				if (oldState != newState && !(!oldState && !newState))
				{
					//console.log('Model is different on state:', newState, oldState);
					return true;
				}
				
				return false;
			};
			
			self.GetAddressHeader = function()
			{
				if (!self.model() || !self.model().svcAddress)
					return '';
				
				var address = self.model().svcAddress.address2();
				
				if (address) address += ', ';
				
				return address + self.model().svcAddress.address1();
			};
			
			self.Submit = function(m, e)
			{
				if (!self.HasModelChanged() || self.submitting())
					return;
				
				self.RemoveAllErrors();
				
				var json = ko.mapping.toJS(self.model);

				self.QAS.isChoosingAddress(false);
				self.QAS.isChoosingEmail(false);
				
				if (!self.QAS.IsLastValidatedAddress())
				{
					var address = {
						address1: json.address1,
						address2: json.address2,
						city: json.city,
						state: json.state,
						zipcode: json.zipcode
					};
					
					self.QAS.isVerifying(true);
					self.submitting(true);
					
					QAS_VerifyAddress(address, function(model, error)
					{
						self.QAS.isVerifying(false);
						self.submitting(false);
						
						if (!model || !model.alternativeAddresses || !model.alternativeAddresses.length)
						{
							self.QAS.KeepAddress();
						}
						else
						{
							self.QAS.address(address);
							self.QAS.addresses(model.alternativeAddresses);
							self.QAS.selectedAddress(model.alternativeAddresses[0]);
							
							self.QAS.isChoosingAddress(true);
						}
					});
				}
				else if (!self.QAS.IsLastValidatedEmail())
				{
					self.QAS.isVerifying(true);
					self.submitting(true);
					
					QAS_VerifyEmail(self.model().email(), function(data)
					{
						self.submitting(false);
						self.QAS.isVerifying(false);
						
						if (data.model && data.model.corrections && data.model.corrections.length)
						{
							self.QAS.emails(data.model.corrections);
							self.QAS.selectedEmail(null);
							self.QAS.isChoosingEmail(true);
						}
						else if (data.model && data.model.errorMessages && data.model.errorMessages.length)
						{
							self.model().errorMessages(data.model.errorMessages);
						}
						else
						{
							self.QAS.KeepEmail();
						}
					});
				}
				else
				{
					self.submitting(true);
					
					CallDWR('MyAccountTMXContactInfoUIUtils/changeContactInfo', json, function(data)
					{
						self.submitting(false);
						
						if (data.error)
						{
							self.model().errorMessages.push({ errorMessage: data.error });
							return;
						} else if(data.model.errorMessages.length > 0 || data.model.fieldValidationErrors.length > 0){
							self.model().errorMessages(data.model.errorMessages);
							self.model().fieldValidationErrors(data.model.fieldValidationErrors);
						} else {
							self.UpdateModel(data.model);
							
							if (self.model().redirectURL())
							{
								setTimeout(function()
								{
									Redirect(self.model().redirectURL());
									
								}, 1500);
								return;
							}
							
							// Success message should be an alert
							if (self.model().informationalMessages().length)
							{
								// Update header first and last name on successful saving
								$('#site-header .welcome .fname').text(self.model().firstName());
								$('#site-header .welcome .lname').text(self.model().lastName());
								
								var messages = ko.mapping.toJS(self.model().informationalMessages);
								
								self.model().informationalMessages.removeAll();
								
								messages.forEach(function(message, messageIndex)
								{
									CreateAlert(
									{
										color: 'green',
										message: message.infoMessage,
										button: false,
										closeButton: true,
										scrollToAlerts: messageIndex == 0,
										$module: $module
									});
								});
							}
						}
					});
				}
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});