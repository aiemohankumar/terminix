function ViewModel() {
	var self = this;

	ViewModel_AddCommonFunctions(self);

	self.isClearingCache = ko.observable(false);
	self.hasClearCacheError = ko.observable(false);
	self.hasClearCacheSuccess = ko.observable(false);
	
	self.ClearCache = function() {
		if (self.isClearingCache()) return;
		
		self.isClearingCache(true);
		self.hasClearCacheError(false);
		self.hasClearCacheSuccess(false);
		
		PromiseDWR("MyAccountTMXUIUtils/clearCache").then(function(data) {
			self.hasClearCacheSuccess(true);
		}).catch(function() {
			self.hasClearCacheError(true);
		}).then(function() {
			self.isClearingCache(false);
		});
	};
}

$(function() {
	window.vm = new ViewModel();

	ko.applyBindings(vm);
});