function ChangePassword(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.passwordIsValid = ko.observable(false);
	self.confirmPasswordIsValid = ko.observable(false);
	
	self.currentPassword = ko.observable();
	self.password = ko.observable();
	self.confirmPassword = ko.observable();

	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	
	self.RefreshInitialization = function() {
		self.passwordIsValid(false);
		self.confirmPasswordIsValid(false);
		
		self.currentPassword('');
		self.password('');
		self.confirmPassword('');
		
		self.RemoveAllErrors();
		
		self.submitting(false);
	};
	
	self.IsValidPasswordChange = function() {
		return self.currentPassword() && self.passwordIsValid() && self.confirmPasswordIsValid();
	};
	
	self.Submit = function() {
		if (self.submitting() || !self.IsValidPasswordChange())
			return
			
		self.RemoveAllErrors();
		
		var json = self.toJSON();
		
		self.submitting(true);
		
		PromiseDWR({ url: "MyAccountTMXPasswordUIUtils/setPassword", data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
				self.errorMessages(data.errorMessages);
				self.fieldValidationErrors(data.fieldValidationErrors);
			} else {
				CreateInformationalMessages(data, {
					$module: $('.module-change-password')
				});
			}
		}).catch(function() {
			self.errorMessages.push({ errorMessage: "An error has occurred." });
		}).then(function() {
			self.submitting(false);
		});
	};
	
	return self;
}