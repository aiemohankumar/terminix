function StoredPayments(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable();
	self.loading = ko.observable();
	self.error = ko.observable();
	//self.selectedPaymentMethod = ko.observable(null);
	self.storedPaymentMethods = ko.observableArray();
	self.addPaymentMethod = ko.observable();
	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	self.visibleHelp = ko.observable();
	self.opaqueId = ko.observable('test data');
	self.identityId = ko.observable('IdentityId');
	self.showWallet = ko.observable(false);
	
	
	self.RemoveStoredPaymentMethod = function(paymentMethodId) {
		self.storedPaymentMethods.remove(function(paymentMethod) {
			return paymentMethod.paymentMethodId() == paymentMethodId;
		});
	};
	
	self.UpdateStoredPaymentMethods = function(paymentMethods) {
		// Update payment model stored payment methods
		self.storedPaymentMethods.remove(function(paymentMethod) {
			return paymentMethod.type() != 'card' && paymentMethod.type() != 'bank';
		});

		paymentMethods.forEach(function(paymentMethod) {
			var copiedPaymentMethod = $.extend({}, paymentMethod);
			
			copiedPaymentMethod.submitting = false;
			copiedPaymentMethod._original = $.extend({}, copiedPaymentMethod);
			
			self.storedPaymentMethods.push(ko.mapping.fromJS(copiedPaymentMethod));
		});
	};
	
	self.UpdateDefaultBillingAddress = function(defaultBillingAddress) {
		var addPaymentMethod = self.addPaymentMethod();
		if (addPaymentMethod) {
			var original = ko.mapping.toJS(addPaymentMethod._original);
			
			// Only update the address if the user hasn't updated it already
			if (addPaymentMethod.billingAddress1() == original.billingAddress1) {
				addPaymentMethod.billingAddress1(defaultBillingAddress.address1);
				addPaymentMethod.billingAddress2(defaultBillingAddress.address2);
				addPaymentMethod.billingCity(defaultBillingAddress.city);
				addPaymentMethod.billingState(defaultBillingAddress.state);
				addPaymentMethod.billingZipcode(defaultBillingAddress.zipcode);
			}
			
			original.billingAddress1 = defaultBillingAddress.address1;
			original.billingAddress2 = defaultBillingAddress.address2;
			original.billingCity = defaultBillingAddress.city;
			original.billingState = defaultBillingAddress.state;
			original.billingZipcode = defaultBillingAddress.zipcode;
			
			self.ApplyModelChanges(addPaymentMethod._original, original);
		}
	};
	
	self.IsVisibleHelp = function(field) {
		return self.visibleHelp() == field;
	};
	
	self.SetVisibleHelp = function(field) {
		self.visibleHelp(field);

		return self;
	};
	
	self.GetAvailableCreditCardYears = function() {
		var years = [];

		for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
			years.push(i);
		}

		return years;
	};
	
	self.RefreshInitialization = function() {
		var addPaymentMethod = self.addPaymentMethod();
		if (addPaymentMethod) {
			addPaymentMethod.modifyStep('');
			addPaymentMethod.type(null);

			self.RemoveAllErrors(addPaymentMethod);

			self.ApplyModelChanges(addPaymentMethod, ko.mapping.toJS(addPaymentMethod._original));
		}
		
		self.storedPaymentMethods().forEach(function(storedPaymentMethod) {
			storedPaymentMethod.modifyStep('');

			self.RemoveAllErrors(storedPaymentMethod);

			self.ApplyModelChanges(storedPaymentMethod, ko.mapping.toJS(storedPaymentMethod._original));
		});
	};
	
	self.SetEditMode = function(item) {
		self.RemoveAllErrors(item);

		item.modifyStep('edit');
	};

	self.SetRemoveMode = function(item) {
		self.RemoveAllErrors(item);

		item.modifyStep('remove');

		if (item.removeError()) {
			item.errorMessages.push({
				errorMessage: item.removeError()
			});
		}
	};
	
	self.OpenAddForm = function(type) {
		var item = self.addPaymentMethod();
		if (item) {
			self.CancelAddForm(item);
			item.type(type);
		}
	};

	self.AddStoredPaymentMethod = function() {
		self.showWallet(true);
		self.property.flagPayment("StoredPayment");
	}

	self.CancelAddForm = function(item) {
		item.modifyStep('');
		item.type(null);

		self.RemoveAllErrors(item);

		self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

		// Scroll back to the payment method selection
		var position = $(".module-stored-payments").find('.add-payment-method').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};

	self.CancelEditForm = function(item) {
		item.modifyStep('');

		self.RemoveAllErrors(item);

		self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

		// Scroll back to the table row
		var position = $(".module-stored-payments").find('.stored-payment-methods').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};

	self.CancelRemoveForm = function(item) {
		item.modifyStep('');

		self.RemoveAllErrors(item);

		// Scroll back to the table row
		var position = $(".module-stored-payments").find('.stored-payment-methods').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};

	self.getStoredPayments = function() {
		if (!self.loaded()) {
			self.loading(true);
			wrapper.reset();
		}
	};
	
	self.IsValidStorePaymentMethod = function(item) {
		switch (item.paymentType() || item.type()) {
		case 'card':
			// Only validate card number when adding
			// We cannot edit a card number on an existing stored
			// payment method
			// Require billing address when adding a card
			if (!item.paymentMethodId())
			{
				if (!item.cardNumber()
				|| !item.billingAddress1()
				|| !item.billingCity()
				|| !item.billingState()
				|| !item.billingZipcode())
					return false;
			}

			if (!item.cardExpireMonth() || !item.cardExpireYear())
				return false;
			break;

		case 'bank':
			if (!item.bankRoutingNumber() || !item.bankAccountNumber())
				return false;
			break;
		}

		return true;
	};

	self.SubmitWallet = function() {
		debugger;
		window.frames.wallet.postMessage({
			action: 'submit'
		}, 'https://test.checkout.servicemaster.com');
    }
	
	self.SubmitAddForm = function(item) {
		// if (!self.IsValidStorePaymentMethod(item) || self.submitting())
		// 	return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		//var json = ko.mapping.toJS(item);
		var json = self.toJSON();

		json.addPaymentMethod.paymentMethodId = self.property.paymentMethodId();
        json.addPaymentMethod.paymentType = self.property.paymentMethodType();
		// json.selectedPaymentMethod.firstName = self.property.payFirstName();
		// json.selectedPaymentMethod.lastName = self.property.payLastName();
		// json.selectedPaymentMethod.errorCode = self.property.payErrorCode();
		
        console.log("paymentMethodId=" + json.addPaymentMethod.paymentMethodId);
        console.log("paymentType=" + json.addPaymentMethod.paymentType);
		// console.log("firstName=" + json.selectedPaymentMethod.firstName);
		// console.log("lastName=" + json.selectedPaymentMethod.lastName);
		// console.log("errorCode=" + json.selectedPaymentMethod.errorCode);


		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/addStoredPaymentMethodRework', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
				item.errorMessages(data.errorMessages);
				item.fieldValidationErrors(data.fieldValidationErrors);
			} else {
				CreateInformationalMessages(data, {
					$module: $(".module-stored-payments")
				});
				
				var addPaymentMethod = self.addPaymentMethod();
				if (addPaymentMethod) {
					addPaymentMethod.modifyStep('');
					addPaymentMethod.type(null);

					self.RemoveAllErrors(addPaymentMethod);

					self.ApplyModelChanges(addPaymentMethod, ko.mapping.toJS(addPaymentMethod._original));
				}
				
				self.UpdateStoredPaymentMethods(data.paymentMethods);

				// YIA payment methods are the same as paymentModel's
				property.paymentModel.getData().then(function() {
					property.paymentModel().UpdateStoredPaymentMethods(data.paymentMethods);
				}).catch(function(error) {
					console.log(error);
				});
				
				property.easyPayModel.getData().then(function() {	
					property.easyPayModel().UpdateStoredPaymentMethods(data.paymentMethods);
				}).catch(function(error) {
					console.log(error);
				});
				
				// Check if we need to update the default billing address
				if (data.updateBillingAddress) {
					var defaultBillingAddress = {
						address1: data.billingAddress1,
						address2: data.billingAddress2,
						city: data.billingCity,
						state: data.billingState,
						zipcode: data.billingZipcode
					};
					
					property.easyPayModel.getData().then(function() {
						property.easyPayModel().UpdateDefaultBillingAddress(defaultBillingAddress);
					});
					
					property.paymentModel.getData().then(function() {
						property.paymentModel().UpdateDefaultBillingAddress(defaultBillingAddress);
					});
				}
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				button: false,
				closeButton: true,
				message: "An error has occured.",
				scrollToAlerts: true,
				$module: $(".module-stored-payments")
			});
		}).then(function() {
			item.submitting(false);
		});
	};

	self.SubmitEditForm = function(item) {
		if (!self.IsValidStorePaymentMethod(item) || self.submitting())
			return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		var json = ko.mapping.toJS(item);

		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/editStoredPaymentMethod', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
				item.errorMessages(data.errorMessages);
				item.fieldValidationErrors(data.fieldValidationErrors);
			} else {
				CreateInformationalMessages(data, {
					$module: $(".module-stored-payments")
				});
				
				// Update table with payment method changes
				item.modifyStep('');
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				button: false,
				closeButton: true,
				message: "An error has occured.",
				scrollToAlerts: true,
				$module: $(".module-stored-payments")
			});
		}).then(function() {
			item.submitting(false);
		});
	};

	self.SubmitRemoveForm = function(item) {
		if (item.submitting())
			return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		var json = ko.mapping.toJS(item);

		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/removeStoredPaymentMethod', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
				item.errorMessages(data.errorMessages);
				item.fieldValidationErrors(data.fieldValidationErrors);
			} else {
				// Remove payment method from other models
				// YIA payment methods are the same as paymentModel's
				property.paymentModel().RemoveStoredPaymentMethod(item.paymentMethodId());
				property.easyPayModel().RemoveStoredPaymentMethod(item.paymentMethodId());
				
				CreateInformationalMessages(data, {
					$module: $(".module-stored-payments")
				});
				
				self.storedPaymentMethods.remove(item);
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				button: false,
				closeButton: true,
				message: "An error has occured.",
				scrollToAlerts: true,
				$module: $(".module-stored-payments")
			});
		}).then(function() {
			item.submitting(false);
		});
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getStoredPaymentMethods.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your payments.");
        },
        onResult: function(json) {
			// Handle the property response
        	json.storedPaymentMethods = json.storedPaymentMethods.map(function(item){
				item.submitting = false;
				item._original = $.extend(true, {}, item);
        		return ko.mapping.fromJS(item);
        	});
        	
        	json.addPaymentMethod._original = $.extend(true, {}, json.addPaymentMethod);
        	json.addPaymentMethod.submitting = false;
        	json.addPaymentMethod = ko.mapping.fromJS(json.addPaymentMethod);
        	
        	SetValuesOnModel(self, json);

        	self.loaded(true);
            self.loading(false);
            self.error(null);
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}