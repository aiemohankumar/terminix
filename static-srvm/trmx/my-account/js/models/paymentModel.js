function PaymentModel(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable(false);
	self.loading = ko.observable(false);
	self.error = ko.observable();
	
	self.selectedPaymentMethod = ko.observable(null);
	self.duePayments = ko.observableArray();
	self.dueYiaPayments = ko.observableArray();
	self.setupEasyPay = ko.observable();
	self.savePaymentMethod = ko.observable();
	self.paymentMethods = ko.observableArray();
	self.paymentTotal = ko.observable();
	self.originalModel = ko.observable();
	
	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	
	self.visibleHelp = ko.observable();

	self.cardNumber = ko.observable();
	self.cardType = ko.observable();
	self.cardSecurity = ko.observable();
	self.cardExpireMonth = ko.observable();
	self.cardExpireYear = ko.observable();
	self.bankRoutingNumber = ko.observable();
	self.bankAccountNumber = ko.observable();
	self.billingAddress1 = ko.observable();
	self.billingAddress2 = ko.observable();
	self.billingCity = ko.observable();
	self.billingState = ko.observable();
	self.billingZipcode = ko.observable();
	self.paymentSuccess = ko.observable();
	self.yiaPayment = ko.observable();
	self.reloadInvoiceHistory = true;
	// self.isCustomerIdLoaded = ko.observable(false);
	self.isShowWallet = ko.observable(false);
	


	ViewModel_AddPaymentFunctions(self);

	self.invoiceHistory = new InvoiceHistory(property, self);
	self.paymentYiaModel = new PaymentYiaModel(property, self);

	self.setIdentityAndOpaqueId = function () {
		debugger;
       self.property.identityId("b7515870-a44c-4494-929f-c81e98254668");
	   self.property.opaqueId("95d0bfe3-72ca-4417-8735-3f4570ddda89");
	   self.property.isWalletParamsLoaded(true);
	   self.property.flagPayment('');
	  // console.log("customername"+ self.property.contactInfo().firstName());
        // self.getIdentityAndOpaqueId(function (data) {
        //     if (data != "Error") {
        //         self.customerId(data.identityId);
        //         self.opaqueId(data.opaqueId);
        //     } else {
        //         console.log("Error In getting identity Id");
        //     }
        // });
    };

    self.getIdentityAndOpaqueId = function (callbackfn) {
        $.ajax({
            type: 'POST',
            url: "http://storefront.local/api/cxa/TmxFulfillment/GetPaymentDetails",
            success: function (data) {
                try {
                    callbackfn(JSON.parse(data));
                } catch (e) {
                    console.log("Error from server request: " + e.message);
                    callbackfn("Error")
                }
            },
            error: function (error) {
                console.log("Error from server request: " + error.message);
                callbackfn("Error");
            }
        });
	};
	
	self.showWallet = function() {
		self.isShowWallet(true);
		self.property.flagPayment("PaymentsDue");
	}
	
	self.setPaymentMethod = function(enrlPayment,event) {
		if(event.originalEvent){
			if(self.selectedPaymentMethod().type() == 'AddNewPayment') {
				self.isShowWallet(true);
				self.property.flagPayment("PaymentsDue");
			}
		}
	}

	self.RefreshInitialization = function() {
		self.CancelPayment();
	//	self.setIdentityAndOpaqueId();

		self.invoiceHistory.RefreshInitialization();
		self.paymentYiaModel.RefreshInitialization();
	};
	
	self.getDuePayments = function() {
		if (!self.loaded()) {
			property.paymentModelSmall().reloadInvoiceHistory = self.reloadInvoiceHistory
			property.paymentModelSmall().loaded(false);
			property.paymentModelSmall.reset();
			property.paymentModelSmall.getData().then(function(){
				self.loading(true);
				wrapper.reset();
			});
		}
	};
	
	self.ReloadDuePayments = function(reloadInvoiceHistory) {
		self.reloadInvoiceHistory = reloadInvoiceHistory || false;
		self.CancelPayment();
		self.loaded(false);
		self.getDuePayments();
	};

	self.setupEasyPay.subscribe(function(newValue) {
		self.savePaymentMethod(newValue);
	});
	
	var preCheckAutoPay = IsEmailCampaignPrecheckAutoPay();
	
	function SetRenewalStep(data) {
		return PromiseDWR({ url: "MyAccountTMXPaymentUIUtils/setRenewalStepOnUserSession", data: data });
	}
	
	self.selectedPaymentMethod.subscribe(function(value) {
		self.RemoveAllErrors();
		
		var hasSelectedPaymentMethod = !!(value && ko.unwrap(value.type));
		
		if (hasSelectedPaymentMethod) {
			// Check if starting a renewal
			var renewalDuePayment = self.duePayments().find(function(duePayment) {
				return duePayment.termiteRenewal() && parseFloat(duePayment.amount()) > 0;
			});
			
			if (renewalDuePayment) {
				SetRenewalStep({
					step: "step_1",
					salesAgreementNumber: renewalDuePayment.salesAgreementNumber(),
					invoiceId: renewalDuePayment.invoiceNumber()
				});
			}
		}
		
		if (hasSelectedPaymentMethod && self.CanSetupEasyPay() && preCheckAutoPay) {
			self.setupEasyPay(true);
		} else {
			self.setupEasyPay(false);
		}
	});
	
	self.CheckStartRenewal = function(duePayment) {
		if (duePayment.termiteRenewal()) {
			SetRenewalStep({
				step: "step_1",
				salesAgreementNumber: duePayment.salesAgreementNumber(),
				invoiceId: duePayment.invoiceNumber()
			});
		}
	};
	
	var _CancelPayment = self.CancelPayment;
	self.CancelPayment = function(m) {
		_CancelPayment(m);
		
		if (!m) {
			self.duePayments().forEach(function(duePayment) {
				duePayment.amount(property.customerType() != property.CUSTOMER_TYPE_COMMERCIAL ? duePayment.amountDue().toFixed(2) : '');
				duePayment.showDetail(false);
			});
			
			UpdateTotalAmount();
			
			self.setupEasyPay(false);
		}
	};
	
	self.CancelPaymentDuePayment = function() {
		self.CancelPayment($(".module-due-payments"));
	};
	
	self.SubmitWallet = function() {
		debugger;
		window.frames.wallet.postMessage({
			action: 'submit'
		}, 'https://test.checkout.servicemaster.com');
    }

	self.SubmitPayment = function() {
		debugger;
		if (!self.IsValidPaymentMethod() || self.submitting())
			return;

		self.RemoveAllErrors();

		var json = self.toJSON();

		debugger;
        var testdata = self.property.walletJsonData();
		console.log("The walletJsonData is = " + self.property.walletJsonData() );

		// json.selectedPaymentMethod.paymentMethodId = "3e09903f-4ff0-4103-8f90-983cf1967ec6";
        // json.selectedPaymentMethod.type = "creditcard";
		// json.selectedPaymentMethod.firstName = self.property.contactInfo().firstName();
		// json.selectedPaymentMethod.lastName = self.property.contactInfo().lastName();
		// json.selectedPaymentMethod.errorCode = "No Error";
		// json.billingAddress1 = "2676 Matthews Street";
		// json.billingAddress2 = "Sterling, IL 61081";
		// json.billingCity = "Sterling";
		// json.billingState = "IL";
		// json.billingZipcode = "61081";

		json.selectedPaymentMethod.paymentMethodId = self.property.paymentMethodId();
        json.selectedPaymentMethod.type = self.property.paymentMethodType();
		json.selectedPaymentMethod.firstName = self.property.payFirstName();
		json.selectedPaymentMethod.lastName = self.property.payLastName();
		json.selectedPaymentMethod.errorCode = self.property.payErrorCode();
		
        console.log("paymentMethodId=" + json.selectedPaymentMethod.paymentMethodId);
        console.log("paymentType=" + json.selectedPaymentMethod.type);
		console.log("firstName=" + json.selectedPaymentMethod.firstName);
		console.log("lastName=" + json.selectedPaymentMethod.lastName);
		console.log("errorCode=" + json.selectedPaymentMethod.errorCode);
		// console.log("billingAddress1=" + json.billingAddress1);
        // console.log("billingAddress2=" + json.billingAddress2);
		// console.log("billingCity=" + json.billingCity);
		// console.log("billingState=" + json.billingState);
		// console.log("billingZipcode=" + json.billingZipcode);

		self.submitting(true);
			
		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/makePaymentRework', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
				self.errorMessages(data.errorMessages);
				self.fieldValidationErrors(data.fieldValidationErrors);
			} else {
				CreateInformationalMessages(data, {
					$module: $(".module-due-payments")
				});
				
				if (property.paymentHistory.loaded()) {
					property.paymentHistory.reset();
				}
				
				// If we are going to reset the EasyPayModel here,
				// then there's no reason to update it below since the
				// new data it gets on load will contain this new data.
				var resetEasyPayModel = false;
				
				if (json.setupEasyPay && property.easyPayModel.loaded()) {
					property.easyPayModel.reset();
					resetEasyPayModel = true;
				}
				
				if ((json.setupEasyPay || json.savePaymentMethod) && json.selectedPaymentMethod && json.selectedPaymentMethod.type != 'saved') {
					if (!resetEasyPayModel) {
						property.easyPayModel.getData().then(function() {
							property.easyPayModel().UpdateStoredPaymentMethods(data.paymentMethods);
						});
					}
					
					property.storedPayments.getData().then(function() {
						property.storedPayments().UpdateStoredPaymentMethods(data.paymentMethods);
					});
				}
				
				if (data.updateBillingAddress) {
					var defaultBillingAddress = {
						address1: data.billingAddress1,
						address2: data.billingAddress2,
						city: data.billingCity,
						state: data.billingState,
						zipcode: data.billingZipcode
					};
					
					if (!resetEasyPayModel) {
						property.easyPayModel.getData().then(function() {
							property.easyPayModel().UpdateDefaultBillingAddress(defaultBillingAddress);
						});
					}
					
					property.storedPayments.getData().then(function() {
						property.storedPayments().UpdateDefaultBillingAddress(defaultBillingAddress);
					});
				}
				
				$('.red-termite').remove();
				
				self.ReloadDuePayments();

				property.paymentModelSmall.getData().then(function(data) {
					var termiteRenewalData = property.paymentModelSmall().termiteRenewalData();
					termiteRenewalData.forEach(function(termiteDatum) {
						if (!termiteDatum.suppressedAlert()) {
							CreateAlert({
								color: 'red-termite',
								message: "Your " + termiteDatum.description() + " is set to expire soon!",
								messageIsHTML: true,
								button: true,
								closeButton: true,
								customCloseButton: 'termite-renewal',
								icon: 'icon-warning',
								onCloseAlert: function(e) {
									var jsonData = { messageType: 'TMX_RNL_ALERT', serviceLine: termiteDatum.serviceLine() };
									CallDWR('MyAccountTMXUIUtils/dismissTMXMyAccountMessage', jsonData, function(data) {});
									termiteDatum.suppressedAlert(true);
								}
							});
						}
					});
				});		
			}
		}).catch(function(error) {
			if (!self.errorMessages().length) {
				self.errorMessages({ errorMessage: "An error has occurred." });
			}
		}).then(function() {
			self.submitting(false);
		});
	};
	
	self.PayTotalBalance = function() {
		self.duePayments().forEach(function(duePayment) {
			var amount;
			
			if (duePayment.amountDue() == duePayment.totalAmount() && duePayment.renewalDiscountedTotalAmount() > 0) {
				amount = duePayment.renewalDiscountedTotalAmount();
			} else {
				amount = duePayment.amountDue();
			}
			
			// Format amount to 2 decimal places
			duePayment.amount(amount.toFixed(2));
		});
	};
	
	self.CheckInvoiceLink = function(item, event) {
		// If it already has a link, then allow the link to work
		if (item.downloadLink())
			return true;

		// If already loading the link, do nothing
		if (item.loading())
			return false;

		// Load the link
		item.loading(true);

		var json = ko.mapping.toJS(item);
		
		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/getInvoiceDocumentLink', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || !data.downloadLink) {
				throw "error";
			} else {
				item.downloadLink(data.downloadLink);
				
				window.open(item.downloadLink(), '_blank');
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				button: false,
				closeButton: true,
				message: "There was a problem retrieving the document link for your invoice. Please try again.",
				scrollToAlerts: true,
				$module:  $(".module-due-payments")
			});
		}).then(function() {
			item.loading(false);
		});
	};
	
	self.updateShowDiscount = function(item) {
		item.showDiscountError(item.renewalDiscountAmount() > 0 && item.amount() > 0 && item.amount() < item.renewalDiscountedTotalAmount());
	};
	
	var UpdateTotalAmount = function() {
		var total = 0;

		self.duePayments().forEach(function(duePayment) {
			total += parseFloat(duePayment.amount()) || 0;
		});

		self.paymentTotal(total);
	};
	
	function updateInvoiceHistory(json) {
		if (json.invoiceHistory && json.invoiceHistory.history.length > 0) {
			json.invoiceHistory.history = json.invoiceHistory.history.map(function(item) {
				item.documentId = item.documentId || null;
				item.downloadLink = item.downloadLink || ''; // Only use for testing purposes
				item.loading = false;
				
				return ko.mapping.fromJS(item);
			});
			
			self.invoiceHistory.history(json.invoiceHistory.history);
		}
	}
	
	function updateDueYiaPayments(json) {
		if (json.dueYiaPayments.length > 0) {
			json.dueYiaPayments = json.dueYiaPayments.map(function(item) {
				item.submitting = false;
				item.modifyStep = '';
				item.acceptedTerms = false;
				item.loading = false;

				return ko.mapping.fromJS(item);
			});
			
			self.paymentYiaModel.dueYiaPayments(json.dueYiaPayments);
		}
	}
	
	self.updateDuePayments = function(json) {
		if (json.duePayments && json.duePayments.length > 0) {
			json.duePayments = json.duePayments.map(function(item) {
				item.documentId = item.documentId || null;
				item.downloadLink = item.downloadLink || ''; // Only use for testing purposes
				item.loading = false;
				item.showDiscountError = false;
				
				return ko.mapping.fromJS(item);
			});
		}
		
		self.duePayments(json.duePayments);
		
		//property.paymentModelSmall().duePayments(json.duePayments);
		
		updateInvoiceHistory(json);
		updateDueYiaPayments(json);

		self.duePayments().forEach(function(duePayment) {
			duePayment.amount.subscribe(function(newValue) {
				UpdateTotalAmount();

				self.RemoveAllErrors(duePayment);
				
				if (self.paymentTotal() == 0) {
					self.CancelPaymentDuePayment();
				}
			});
		});
	}
		
	self.UpdateModel = function(json) {
		self.updateDuePayments(json);
		SetValuesOnModel(self, ko.mapping.fromJS(json, {'ignore': ['duePayments']}));
		
		// Update YIA default billing address
		self.paymentYiaModel.billingAddress1(self.billingAddress1());
		self.paymentYiaModel.billingAddress2(self.billingAddress2());
		self.paymentYiaModel.billingCity(self.billingCity());
		self.paymentYiaModel.billingState(self.billingState());
		self.paymentYiaModel.billingZipcode(self.billingZipcode());

		UpdateTotalAmount();
		self.UpdateSelectedPaymentMethod();
	};
	
	self.UpdateDefaultBillingAddress = function(defaultBillingAddress) {
		// Only update if it is unchanged by the user
		var original = ko.mapping.toJS(self.originalModel);

		if (self.billingAddress1() == original.billingAddress1) {
			self.billingAddress1(defaultBillingAddress.address1);
			self.billingAddress2(defaultBillingAddress.address2);
			self.billingCity(defaultBillingAddress.city);
			self.billingState(defaultBillingAddress.state);
			self.billingZipcode(defaultBillingAddress.zipcode);
		}

		if (self.paymentYiaModel.billingAddress1() == original.billingAddress1) {
			self.paymentYiaModel.billingAddress1(defaultBillingAddress.address1);
			self.paymentYiaModel.billingAddress2(defaultBillingAddress.address2);
			self.paymentYiaModel.billingCity(defaultBillingAddress.city);
			self.paymentYiaModel.billingState(defaultBillingAddress.state);
			self.paymentYiaModel.billingZipcode(defaultBillingAddress.zipcode);
		}
		
		original.billingAddress1 = defaultBillingAddress.address1;
		original.billingAddress2 = defaultBillingAddress.address2;
		original.billingCity = defaultBillingAddress.city;
		original.billingState = defaultBillingAddress.state;
		original.billingZipcode = defaultBillingAddress.zipcode;
		
		self.originalModel(ko.mapping.fromJS(original));
	};
	
	self.RemoveStoredPaymentMethod = function(paymentMethodId) {
		if (self.selectedPaymentMethod() && self.selectedPaymentMethod().paymentMethodId() == paymentMethodId) {
			self.selectedPaymentMethod(null);
		}
		
		if (self.paymentYiaModel.selectedPaymentMethod() && self.paymentYiaModel.selectedPaymentMethod().paymentMethodId() == paymentMethodId) {
			self.paymentYiaModel.selectedPaymentMethod(null);
		}
		
		self.paymentMethods.remove(function(paymentMethod) {
			return paymentMethod.paymentMethodId() == paymentMethodId;
		});
	};
	
	self.UpdateStoredPaymentMethods = function(paymentMethods) {
		var selectedPaymentMethod = self.selectedPaymentMethod();
		var selectedPaymentMethodYIA = self.paymentYiaModel.selectedPaymentMethod();
		
		// Update payment model stored payment methods
		self.paymentMethods.remove(function(paymentMethod) {
			return paymentMethod.type() != 'card' && paymentMethod.type() != 'bank';
		});

		paymentMethods.forEach(function(paymentMethod) {
			self.paymentMethods.push(ko.mapping.fromJS($.extend({}, paymentMethod)));
		});
		
		if (selectedPaymentMethod && selectedPaymentMethod.type() != 'card' && selectedPaymentMethod.type() != 'bank') {
			self.selectedPaymentMethod(self.paymentMethods.find(function(paymentMethod) {
				return paymentMethod.paymentMethodId() == selectedPaymentMethod.paymentMethodId();
			}) || null);
		}
		
		if (selectedPaymentMethodYIA && selectedPaymentMethodYIA.type() != 'card' && selectedPaymentMethodYIA.type() != 'bank') {
			self.paymentYiaModel.selectedPaymentMethod(self.paymentMethods.find(function(paymentMethod) {
				return paymentMethod.paymentMethodId() == selectedPaymentMethodYIA.paymentMethodId();
			}) || null);
		}
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getDuePaymentsDataRework.json",
        data: {
        	jsonData: function() {
            	return JSON.stringify(property.paymentModelSmall().toJSON());
            }
        },
        dwr: true,
        onBeforeLoad: function(){
        	if (!property.paymentModelSmall().loaded()) {
        		return false
        	}
        	
        	return true;
        },
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
        	self.loading(false);
            self.error("There was an error loading your payments.");
        },
        onResult: function(json) {
			// Handle the property response
        	self.loaded(true);
        	self.loading(false);
			self.error(null);
    		self.originalModel(ko.mapping.fromJS(json));
			self.UpdateModel(json);
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper
	
}