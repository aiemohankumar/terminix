function PaymentYiaModel(property, paymentModel) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.loaded = paymentModel.loaded;
	self.loading = paymentModel.loading;
	self.error = paymentModel.error;
	
	self.originalModel = paymentModel.originalModel;
	self.paymentMethods = paymentModel.paymentMethods
	self.dueYiaPayments = paymentModel.dueYiaPayments;
	self.sortedPaymentMethods = paymentModel.sortedPaymentMethods;
	self.showDiscountDetail = ko.observable(false);
	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	self.savePaymentMethod = ko.observable();
	self.setupEasyPay = ko.observable();
	self.visibleHelp = ko.observable();
	
	self.cardNumber = ko.observable();
	self.cardType = ko.observable();
	self.cardSecurity = ko.observable();
	self.cardExpireMonth = ko.observable();
	self.cardExpireYear = ko.observable();
	self.bankRoutingNumber = ko.observable();
	self.bankAccountNumber = ko.observable();
	self.billingAddress1 = ko.observable();
	self.billingAddress2 = ko.observable();
	self.billingCity = ko.observable();
	self.billingState = ko.observable();
	self.billingZipcode = ko.observable();
	self.paymentSuccess = ko.observable();

	self.selectedPaymentMethod = ko.observable();
	self.selectedPaymentMethod.subscribe(function(value) {
		self.RemoveAllErrors();
	});

	ViewModel_AddPaymentFunctions(self);
	
	self.RefreshInitialization = function() {
		self.CancelPayment();
	};
	
	self.ShowDetail = function(item) {
		item.showDetail(!item.showDetail());
	};
	
	self.ShowDiscountDetail = function() {
		self.showDiscountDetail(!self.showDiscountDetail());
	};
	
	self.HasEligibleSalesAgreements = ko.computed(function() {
		return self.dueYiaPayments().length > 0;
	});
	
	self.HasSelectedAgreements = ko.computed(function() {
		return self.dueYiaPayments().some(function(item) {
			return item.yiaPaySelected();
		});
	});
	
	self.YiaDiscountSum = ko.computed(function() {
		return self.dueYiaPayments().reduce(function(sum, item) {
			if (item.yiaPaySelected()) {
				sum += item.yiaDiscAmt();
			}
			return sum;
		}, 0);
	});

	self.PaymentTotal = ko.computed(function() {
		return self.dueYiaPayments().reduce(function(sum, item) {
			if (item.yiaPaySelected()) {
				sum += item.yiaTotalAmt();
			}
			return sum;
		}, 0);
	});
	
	var _CancelPayment = self.CancelPayment;
	self.CancelPayment = function(m) {
		_CancelPayment(m);
		
		if (!m) {
			self.dueYiaPayments().forEach(function(dueYiaPayment) {
				dueYiaPayment.yiaPaySelected(false);
				dueYiaPayment.showDetail(false);
			});
			
			self.showDiscountDetail(false);
		}
	};

	self.setPaymentMethod = function(enrlPayment,event) {
		debugger;
		if(event.originalEvent){

			if(self.selectedPaymentMethod().type() == 'AddNewPayment') {
				self.property.flagPayment("PayYearInAdvance");
			}
		}
	}

	self.CancelPaymentYIA = function() {
		self.CancelPayment($(".module-yia-pay"));
	};

	self.SubmitWallet = function() {
		debugger;
		window.frames.wallet.postMessage({
			action: 'submit'
		}, 'https://test.checkout.servicemaster.com');
    }
	
	self.SubmitPayment = function() {
		if (!self.IsValidPaymentMethod() || self.submitting())
			return;

		self.RemoveAllErrors();

		var json = self.toJSON();

		json.selectedPaymentMethod.paymentMethodId = self.property.paymentMethodId();
        json.selectedPaymentMethod.type = self.property.paymentMethodType();
		json.selectedPaymentMethod.firstName = self.property.payFirstName();
		json.selectedPaymentMethod.lastName = self.property.payLastName();
		json.selectedPaymentMethod.errorCode = self.property.payErrorCode();
		
        console.log("paymentMethodId=" + json.selectedPaymentMethod.paymentMethodId);
        console.log("paymentType=" + json.selectedPaymentMethod.type);
		console.log("firstName=" + json.selectedPaymentMethod.firstName);
		console.log("lastName=" + json.selectedPaymentMethod.lastName);
		console.log("errorCode=" + json.selectedPaymentMethod.errorCode);

		json.selectedPaymentMethod = ko.mapping.toJS(self.selectedPaymentMethod);

		self.submitting(true);

		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/makeYiaPaymentRework', data: json }).then(function(data) {
			if (data.informationalMessages.length) {
				CreateInformationalMessages(data, {
					$module: $('.module-yia-pay')
				});
				
				if (data.errorMessages.length) {
					CreateErrorMessages(data, {
						$module: $('.module-yia-pay')
					});
				}
				
				// Check if we should update other sections with the new saved payment method
				if (data.savePaymentMethod && data.paymentMethods && data.paymentMethods.length) {
					property.storedPayments.getData().then(function() {
						property.storedPayments().UpdateStoredPaymentMethods(data.paymentMethods);
					}).catch(function(error) {
						console.log(error);
					});
					
					property.easyPayModel.getData().then(function() {
						property.easyPayModel().UpdateStoredPaymentMethods(data.paymentMethods);
					}).catch(function(error) {
						console.log(error);
					});
				}
				
				// Check if we need to update the default billing address
				if (data.updateBillingAddress) {
					var defaultBillingAddress = {
						address1: data.billingAddress1,
						address2: data.billingAddress2,
						city: data.billingCity,
						state: data.billingState,
						zipcode: data.billingZipcode
					};
					
					property.easyPayModel.getData().then(function() {
						property.easyPayModel().UpdateDefaultBillingAddress(defaultBillingAddress);
					});
					
					property.storedPayments.getData().then(function() {
						property.storedPayments().UpdateDefaultBillingAddress(defaultBillingAddress);
					});
				}
				
				paymentModel.ReloadDuePayments();
				
				self.CancelPayment();
				
				if (property.paymentHistory.loaded()) {
					property.paymentHistory.reset();
				}
			}
			else if (data.errorMessages.length || data.fieldValidationErrors.length) {
				self.errorMessages(data.errorMessages);
				self.fieldValidationErrors(data.fieldValidationErrors);
			}
		}).catch(function() {
			CreateAlert({
				color: "red",
				message: "An error has occurred.",
				button: false,
				closeButton: true,
				scrollToAlerts: true,
				$module: $('.module-yia-pay')
			});
		}).then(function() {
			self.submitting(false);
		});
	};
	
	return self;
}