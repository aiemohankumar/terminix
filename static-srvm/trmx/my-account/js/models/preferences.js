function Preferences(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];

	ViewModel_AddCommonFunctions(self);
	
	self.loading = ko.observable(false);
	self.loaded = ko.observable(false);
	self.error = ko.observable();
	self.originalModel = ko.observable();
	
	self.isPreferencesEnabled = ko.observable();
	
	// Service notification preferences
	self.isServiceNotificationPreferencesEnabled = ko.observable();
	self.serviceNotificationPreferenceEmail = ko.observable();
	self.serviceNotificationPreferenceCall = ko.observable();
	self.serviceNotificationPreferenceText = ko.observable();
	self.serviceNotificationEmail = ko.observable();
	self.serviceNotificationPhoneNumber = ko.observable();
	
	// Scheduling preferences
	self.isSchedulingPreferencesEnabled = ko.observable();
	self.scheduleWeekOfMonth = ko.observable();
	
	self.scheduleDays = ['S', 'M', 'T', 'W', 'TH', 'F', 'SA'];
	self.scheduleTimes = ['AM', 'PM'];
	
	self.EmailValidator = new EmailValidator(self.serviceNotificationEmail, function() {
		self.Submit();
	});

	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	
	self.scheduleDayTimePreferences = ko.observable();
	
	self.RefreshInitialization = function() {
		var original = ko.mapping.toJS(self.originalModel);
		
		self.UpdateModel(original);
	};

	self.HasModelChanged = function() {
		if (!self.originalModel())
			return false;
		
		var originalModel = ko.mapping.toJS(self.originalModel);
		
		if (self.isServiceNotificationPreferencesEnabled()) {
			var inputs = ['serviceNotificationPreferenceEmail', 'serviceNotificationPreferenceCall', 'serviceNotificationPreferenceText', 'serviceNotificationEmail'];
			var hasChanged = inputs.some(function(input) {
				return self[input]() != originalModel[input];
			});
			
			if (hasChanged) {
				return true;
			}
			
			// Phone is a special case because sometimes it compares the formatted phone vs unformatted phone
			var oldPhone = ('' + originalModel.serviceNotificationPhoneNumber).replace(/[^\d]/g, ''),
				newPhone = ('' + self.serviceNotificationPhoneNumber()).replace(/[^\d]/g, '');
			
			if (oldPhone != newPhone) {
				return true;
			}
		}
		
		if (self.isSchedulingPreferencesEnabled()) {
			var changedPreference = self.scheduleDays.some(function(dayKey) {
				return self.scheduleTimes.some(function(timeKey) {
					var originalPreference = originalModel.scheduleDayTimePreferences && originalModel.scheduleDayTimePreferences[dayKey] && !!originalModel.scheduleDayTimePreferences[dayKey][timeKey];
					
					return self.scheduleDayTimePreferences[dayKey][timeKey]() != originalPreference;
				});
			});
			
			if (changedPreference)
				return true;
			
			if (self.scheduleWeekOfMonth() != originalModel.scheduleWeekOfMonth)
				return true;
		}
		
		return false;
	};
	
	self.Submit = function() {
		if (!self.HasModelChanged() || self.submitting())
			return;
		
		self.RemoveAllErrors();
		
		var json = self.toJSON();
		
		self.EmailValidator.isChoosing(false);
		
		if (self.serviceNotificationPreferenceEmail() && !self.EmailValidator.IsLastValidatedEmail()) {
			self.EmailValidator.isVerifying(true);
			self.submitting(true);
			
			QAS_VerifyEmail(self.serviceNotificationEmail(), function(data) {
				self.submitting(false);
				self.EmailValidator.isVerifying(false);
				
				if (data.corrections && data.corrections.length) {
					self.EmailValidator.emails(data.corrections);
					self.EmailValidator.selectedEmail(null);
					self.EmailValidator.isChoosing(true);
				}
				else if (data.errorMessages && data.errorMessages.length) {
					self.errorMessages(data.errorMessages);
				} else {
					self.EmailValidator.KeepEmail();
				}
			});
		} else {
			self.submitting(true);
			
			PromiseDWR({ url: 'MyAccountTMXContactInfoUIUtils/setPreferences', data: json }).then(function(data) {
				var scrollToFieldErrors = false;
				
				if (data.errorMessages && data.errorMessages.length) {
					data.errorMessages = [];
					
					CreateAlert({
						color: 'red',
						message: "Sorry, something went wrong. Please try again.",
						button: false,
						closeButton: true,
						scrollToAlerts: true,
						$module: $('.module-preferences')
					});
				}
				else if (data.fieldValidationErrors && data.fieldValidationErrors.length) {
					scrollToFieldErrors = true;
				}
				
				if (data.informationalMessages && data.informationalMessages.length) {
					CreateInformationalMessages(data, {
						$module: $('.module-preferences')
					});
					
					data.informationalMessages = [];
					
					scrollToFieldErrors = false;
				}
				
				SetValuesOnModel(self, ko.mapping.fromJS(data));
				self.originalModel(ko.mapping.fromJS(data));
				self.updatePreferences(data);
				
				self.EmailValidator.SetDefaultEmail(self.serviceNotificationEmail());
				
				if (scrollToFieldErrors) {
					var $errorInput = $('.module-preferences').find('.error');
					if ($errorInput.length) {
						ScrollTo(Math.max(0, $errorInput.offset().top - 20), 400);
					}
				}
			}).catch(function(error) {
				CreateAlert({
					color: 'red',
					message: "Sorry, something went wrong. Please try again.",
					button: false,
					closeButton: true,
					scrollToAlerts: true,
					$module: $('.module-preferences')
				});
			}).then(function() {
				self.submitting(false);
			});
		}
	};
	
	self.getPreferences = function() {
		if (!self.loaded()) {
			self.loading(true);
			wrapper.reset();
		}
	};
	
	self.updatePreferences = function(json) {
		// Update the day time preferences if it exists
		self.scheduleDays.forEach(function(dayKey) {
			self.scheduleTimes.forEach(function(timeKey) {
				self.scheduleDayTimePreferences()[dayKey][timeKey](json && json.scheduleDayTimePreferences && json.scheduleDayTimePreferences[dayKey] && !!json.scheduleDayTimePreferences[dayKey][timeKey]);
			});
		});
	};
	
	self.UpdateModel = function(json) {
		SetValuesOnModel(self, ko.mapping.fromJS(json));

		var scheduleDayTimePreferences = {};
		
		self.scheduleDays.forEach(function(dayKey) {
			scheduleDayTimePreferences[dayKey] = {};
			
			self.scheduleTimes.forEach(function(timeKey) {
				scheduleDayTimePreferences[dayKey][timeKey] = false;
			});
		});
		
		self.scheduleDayTimePreferences(ko.mapping.fromJS(scheduleDayTimePreferences));
		self.updatePreferences(json);
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getPreferences.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
        	self.loading(false);
            self.error("There was an error loading your preferences.");
        },
        onResult: function(json) {
        	// Handle the property response
        	self.loaded(true);
        	self.loading(false);
        	self.error(null);
    		self.originalModel(ko.mapping.fromJS(json));
    		self.UpdateModel(json);
    		
    		self.EmailValidator.SetDefaultEmail(self.serviceNotificationEmail());
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}