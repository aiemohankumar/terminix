function ScheduledService(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	ViewModel_AddScheduleFunctions(self);
	
	self.services = ko.observableArray();
	self.loaded = ko.observable(false);
	self.loading = ko.observable(false);
	self.error = ko.observable();
	self.scanMasterUrl = ko.observable();
	
	var ignoreRescheduleStepChange = false;

	self.RefreshInitialization = function() {
		self.services().forEach(function(service) {
			service.showDetail(false);
			
			ignoreRescheduleStepChange = true;
			service.rescheduleStep('');
			
			service.submitting(false);
			service.submittingTentative(false);
			service.submittingNote(false);
			
			service.loadRescheduleAttempts = 0;
			
			self.RemoveAllErrors(service);
			
			// appointment step
			service.rescheduleSelectedDate(null);
			service.rescheduleSelectedTimeObject(null);
			
			// contact step
			service.rescheduleContactFname(service._original.rescheduleContactFname());
			service.rescheduleContactLname(service._original.rescheduleContactLname());
			service.rescheduleContactPhone(service._original.rescheduleContactPhone());
			service.rescheduleContactDate('');
			service.rescheduleContactTime('');
		});
	};
	
	self.ConfirmTentativeAppointment = function(item) {
		if (!item.canConfirmAppointment() || item.submittingTentative()) return;
		
		self.RemoveAllErrors(item);
		
		item.submittingTentative(true);
		
		var json = ko.mapping.toJS(item);
		
		// Simulating confirmation
		PromiseDWR({ url: 'MyAccountTMXServicesUIUtils/confirmScheduledService', data: json }).then(function(data) {
			if (data.errorMessages && data.errorMessages.length) {
				CreateErrorMessages(data, {
					$module: $('.module-scheduled-services')
				});
			} else {
				// Update model
				self.ApplyModelChanges(item, data);
				
				// Show confirmation message
				item.rescheduleStep('confirmationTentativeAppointment');
			}
		}).catch(function() {
			CreateAlert({
				color: 'red',
				message: "There was an error confirming your scheduled service. Please try again.",
				button: false,
				closeButton: true,
				scrollToAlerts: true,
				$module: $('.module-scheduled-services')
			});
		}).then(function() {
			item.submittingTentative(false);
		});
	};
	
	self.Reschedule = function(item) {
		var onRescheduleLoaded = function() {
			if (item.months().length) {
				item.rescheduleStep('appointment');
			} else {
				item.rescheduleStep('contact');
			}
		};
		
		if (!item.rescheduleLoaded()) {
			self.RemoveAllErrors(item);
			
			var json = self.GetModelJSON(item);
			
			item.submitting(true);
			item.loadRescheduleAttempts++;
			
			PromiseDWR({ url: 'MyAccountTMXServicesUIUtils/rescheduleService', data: json }).then(function(data) {
				if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
					item.errorMessages(data.errorMessages);
					item.fieldValidationErrors(data.fieldValidationErrors);
				} else {
					// Update item model
					self.ApplyModelChanges(item, data);
					
					// Create the calendars needed with the reschedule dates
					self.CreateRescheduleMonths(item);
					
					// Set it as loaded
					item.rescheduleLoaded(true);
					onRescheduleLoaded();
				}
			}).catch(function() {
				if (item.loadRescheduleAttempts < 2) {
					item.errorMessages.push({ errorMessage: "An error has occurred." });
					item.rescheduleStep('error');
				} else {
					item.rescheduleStep('contact');
				}
			}).then(function() {
				item.submitting(false);
			});
		} else {
			onRescheduleLoaded();
		}
	};
	
	self.FormatTimeRange = function(time) {
		// Remove the :00 from a time and only show the hour
		return ('' + ko.unwrap(time)).replace(/\:00/g, '');
	};
	
	self.ConfirmAppointment = function(item) {
		if (item.submitting() || !self.IsValidReschedule(item)) return;
		
		self.RemoveAllErrors(item);
		
		var json = self.GetModelJSON(item);
		
		item.submitting(true);
		
		PromiseDWR({ url: 'MyAccountTMXServicesUIUtils/rescheduleService', data: json }).then(function(data) {
			data['date'] = data['rescheduleSelectedDate'];
			
			// Ignore rescheduleSelectedDate being set
			if (data && 'rescheduleSelectedDate' in data) {
				delete data['rescheduleSelectedDate'];
			}

			// Update model
			self.ApplyModelChanges(item, data);
			
			// Go to next step if no errors
			if (!item.errorMessages().length && !item.fieldValidationErrors().length) {
				item.rescheduleStep('confirmationAppointment');
				
				// Delete the updated services from the item, for cleanup purposes
				if (ko.isObservable(item.updatedScheduledServices)) {
					item.updatedScheduledServices([]);
				}
				
				// Update my services with item.updatedScheduledServices
				if (data.updatedScheduledServices && data.updatedScheduledServices.length) {
					data.updatedScheduledServices.forEach(function(service) {
						// Find the item to update in the model
						var updateItem = self.services().find(function(updateItem) {
							return updateItem.workOrderActivityId() == service.workOrderActivityId;
						});
						
						if (updateItem) {
							// Remove information from the service that we don't want to update the item with
							var fieldsToNotUpdate = ['rescheduleSelectedDate', 'rescheduleStep'];
							
							fieldsToNotUpdate.forEach(function(field) {
								if (service.hasOwnProperty(field)) {
									delete service[field];
								}
							});
							
							// Update the item with the new service information
							self.ApplyModelChanges(updateItem, service);
						} else {
							// Add to Scheduled Services
							PrepareService(service);
							
							self.services.push(ko.mapping.fromJS(service));
						}
					});
				}
				
				if (property.myServices.loaded()) {
					property.myServices.reset();
				}
			}
		}).catch(function() {
			item.errorMessages.push({ errorMessage: "An error has occurred" });
		}).then(function() {
			item.submitting(false);
		});
	};
	
	self.ConfirmContact = function(item) {
		if (item.submitting() || !self.IsValidContact(item)) return;
		
		self.RemoveAllErrors(item);
		
		var json = self.GetModelJSON(item);
		
		item.submitting(true);
		
		PromiseDWR({ url: 'MyAccountTMXServicesUIUtils/rescheduleService', data: json }).then(function(data) {
			// Update model
			self.ApplyModelChanges(item, data);
			
			// Go to next step if no errors
			if (!item.errorMessages().length && !item.fieldValidationErrors().length) {
				item.rescheduleStep('confirmationContact');
			}
		}).catch(function() {
			item.errorMessages.push({ errorMessage: "An error has occurred" });
		}).then(function() {
			item.submitting(false);
		});
	};
	
	self.SubmitNote = function(item) {
		if (!item.noteForTechnician() || item.submittingNote()) return;
		
		self.RemoveAllErrors(item);
		
		item.submittingNote(true);
		
		var json = ko.mapping.toJS(item);
		
		PromiseDWR({ url: 'MyAccountTMXServicesUIUtils/updateNoteToTechnician', data: json }).then(function(data) {
			self.ApplyModelChanges(item, data);
		}).catch(function(error) {
			item.errorMessages.push({ errorMessage: "An error has occurred" });
		}).then(function() {
			item.submittingNote(false);
		});
	};
	
	self.hasTentativeAppointments = ko.computed(function() {
		return self && self.services() && self.services().length && self.services().some(function(item) {
			return !item.isConfirmedAppointment();
		});
	});
	
	self.hasAppointmentsToConfirm = ko.computed(function() {
		return self && self.services() && self.services().length && self.services().some(function(item) {
			return !item.isConfirmedAppointment() && item.canConfirmAppointment();
		});
	});
	
	self.getScheduledServices = function() {
		if (!self.loaded()) {
			self.loading(true);
			wrapper.reset();
		}
	};
	
	function PrepareService(item) {
		// Tentative appointment message if it is too far in the
		// future to confirm
		item.showFutureAppointmentMessage = false;
		item.futureAppointmentMessage = '';
		
		// Remove seconds to get beginning of the day
		var today = new Date(Date.now() - (Date.now() % 86400000));
		
		if (!item.isConfirmedAppointment || true) {
			var datePieces = item.date.split('/').map(function(p) { return parseInt(p); });
			
			// Remove seconds to get beginning of the day
			var scheduledDate = new Date(datePieces[2], datePieces[0]-1, datePieces[1]);
			scheduledDate.setTime(scheduledDate.getTime() - (scheduledDate.getTime() % 86400000));
			
			var daysAhead = (scheduledDate - today) / 86400000;
			
			if (daysAhead > 5) {
				item.showFutureAppointmentMessage = true;
				item.futureAppointmentMessage = "Your next " + item.description + " will happen in " + GetMonthName(datePieces[0]) + ". You’ll receive a reminder a few days before your next service with the appointment details.";
			}
		}
		
		// This is what is used to know the selected time since
		// time is now an object with a time and employee number
		item.rescheduleSelectedTimeObject = null;
		
		// Split the name by comma and remove trailing/leading
		// whitespace
		var namePieces = item.technician.split(',').map(function(piece) { return piece.trim(); });
		
		// Rearrange name in reverse order
		item.technician = namePieces.reverse().join(' ');
		
		item.submittingNote = false;
		item.submittingTentative = false;
		
		item.loadRescheduleAttempts = 0;
		
		item.months = [];
		item._original = $.extend({}, item);
	}
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getScheduledServicesData.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
            self.loading(false);
			self.error("There was an error loading your scheduled services.");
        },
        onResult: function(json) {
            self.loaded(true);
            self.loading(false);
            
			self.services(json.services.map(function(service) {
				PrepareService(service);
				
				return ko.mapping.fromJS(service);
			}));

			// Mobile support for switching steps and scrolling user to top of new step
			self.services().forEach(function(service, index) {
				service.rescheduleStep.subscribe(function(step) {
					if (ignoreRescheduleStepChange) {
						ignoreRescheduleStepChange = false;
						return;
					}
					
					if (Viewport.isMobile()) {
						var $row = $('.module-scheduled-services table:visible tr.summary').eq(index);
						
						// If on a step, then scroll to the next row which is the "detail" view
						if (step) {
							$row = $row.next();
						}
						
						var pos = $row.offset() && $row.offset().top || 0;
						
						ScrollTo(pos, 'fast');
					}
				});
			});
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}
