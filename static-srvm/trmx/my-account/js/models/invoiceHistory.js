function InvoiceHistory(property, paymentModel) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	self.loaded = paymentModel.loaded;
	self.loading = paymentModel.loading;
	self.error = paymentModel.error;

	self.showInvoiceNumber = ko.observable();
	self.history = ko.observableArray();
	
	self.defaultNumViewable = 4;
	self.numViewableIncrease = 4;
	self.numViewable = ko.observable(self.defaultNumViewable);
	
	self.RefreshInitialization = function() {
		self.numViewable(self.defaultNumViewable);
	};
	
	self.GetViewableHistory = ko.computed(function() {
		return self.history() ? self.history().slice(0, self.numViewable()) : [];
	});

	self.canViewMore = ko.computed(function() {
		return self.numViewable() < self.history().length;
	});

	self.canViewLess = ko.computed(function() {
		return self.numViewable() != self.defaultNumViewable;
	});

	self.ViewMore = function() {
		self.numViewable(Math.min(self.numViewable() + self.numViewableIncrease, self.history().length));
	};

	self.ViewLess = function() {
		self.numViewable(self.defaultNumViewable);
	};
	
	self.CheckInvoiceLink = function(item) {
		// If it already has a link, then allow the link to work
		if (item.downloadLink())
			return true;
		
		// If already loading the link, do nothing
		if (item.loading())
			return false;
		
		// Load the link
		item.loading(true);
		
		var json = ko.mapping.toJS(item);
		debugger;
		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/getInvoiceDocumentLink', data: json }).then(function(data) {
			if (data.errorMessages.length > 0 || !data.downloadLink) {
				throw "error";
			} else {
				item.downloadLink(data.downloadLink);
				
				window.open(item.downloadLink(), '_blank');
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				button: false,
				closeButton: true,
				message: "There was a problem retrieving the document link for your invoice. Please try again.",
				scrollToAlerts: true,
				$module: $('.module-invoice-history')
			});
		}).then(function() {
			item.loading(false);
		});
	};
}