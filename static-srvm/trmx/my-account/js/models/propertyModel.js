function Property(model) {
	var self = this;

	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable(true);

	self.propertyId = ko.observable();
	self.address = ko.observable();
	self.customerId = ko.observable();
	self.customerType = ko.observable();
	self.isHidePaymentsTab = ko.observable();
	self.isHidePaymentsTabCheck = ko.observable();
	//self.opaqId = ko.observable('test data');
	
	SetValuesOnModel(self, model);
	
	self.CUSTOMER_TYPE_RESIDENTIAL = "RES";
	self.CUSTOMER_TYPE_COMMERCIAL = "COM";
	
	self.VIEW_PAYMENT_HISTORY = 'payment-history';
	self.VIEW_INVOICE_HISTORY = 'invoice-history'; 
	self.paymentHistoryView = ko.observable(self.VIEW_PAYMENT_HISTORY);
	
	self.scheduledServices = new ScheduledService(self);
	self.myServices = new MyServices(self);
	self.serviceHistory = new ServiceHistory(self);
	self.paymentModelSmall = new PaymentModelSmall(self);
	self.paymentModel = new PaymentModel(self);
	self.paymentHistory = new PaymentHistory(self);
	self.contactInfo = new ContactInfo(self);
	self.preferences = new Preferences(self);
	self.changePassword = new ChangePassword(self);
	self.easyPayModel = new EasyPayModel(self);
	self.storedPayments = new StoredPayments(self);
	// self.liveChatModel = new LiveChatModel(self);
	self.movingModel = new MovingModel(self);

	self.identityId = ko.observable();
	self.opaqueId = ko.observable();
	self.isWalletParamsLoaded = ko.observable(false);
	self.flagPayment = ko.observable();
	self.walletJsonData = ko.observable();
	self.paymentMethodId = ko.observable();
	self.paymentMethodType = ko.observable();
	self.payFirstName = ko.observable();
	self.payLastName = ko.observable();
	self.payErrorCode = ko.observable();
	
	self.RefreshInitialization = function() {
		self.paymentHistoryView(self.VIEW_PAYMENT_HISTORY);
		
		self.changePassword.RefreshInitialization();
		self.contactInfo.RefreshInitialization();
		self.easyPayModel.RefreshInitialization();
		self.movingModel.RefreshInitialization();
		self.myServices.RefreshInitialization();
		self.paymentModel.RefreshInitialization();
		self.paymentHistory.RefreshInitialization();
		self.preferences.RefreshInitialization();
		self.scheduledServices.RefreshInitialization();
		self.serviceHistory.RefreshInitialization();
		self.storedPayments.RefreshInitialization();
	};
}