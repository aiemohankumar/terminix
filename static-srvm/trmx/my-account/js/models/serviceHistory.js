function ServiceHistory(property) {
    var self = this;

    self.property = property;

    // used to tell self.toJSON() what fields on the model to ignore
    self.__mapping_ignore = ['property'];

    self.history = ko.observableArray();
    self.loaded = ko.observable(false);
    self.loading = ko.observable(false);
    self.error = ko.observable();
    self.numViewable = ko.observable();
    self.originalNumViewable = ko.observable();
    self.useNewDocumentDownload = ko.observable();

    self.RefreshInitialization = function () {
        self.numViewable(self.originalNumViewable());

        self.history().forEach(function (item) {
            item.showDetail(false);
            item.documentModel.hasError(false);
        });
    };

    self.GetViewableHistory = function () {
        return self.history().slice(0, self.numViewable());
    };

    self.ShowDetail = function (item) {
        item.showDetail(!item.showDetail());

        if (item.showDetail() && self.useNewDocumentDownload() && !item.documentModel.loaded() && !item.documentModel.loading()) {
            FetchDocuments(item);
        }
    };

    self.RetryFetchDocuments = function (item) {
        if (self.useNewDocumentDownload() && item.documentModel.loaded() && !item.documentModel.loading() && item.documentModel.hasError()) {
            item.documentModel.loaded(false);
            item.documentModel.hasError(false);

            FetchDocuments(item);
        }
    };

    self.PayNow = function (item) {
        TAB.Load('payments', {
            focusModule: 'due-payments'
        });
    };

    self.CanViewMore = ko.computed(function () {
        return self.numViewable() < self.history().length;
    });

    self.CanViewLess = ko.computed(function () {
        return self.numViewable() != self.originalNumViewable();
    });

    self.ViewMore = function () {
        self.numViewable(Math.min(self.numViewable() + 5, self.history().length));
    };

    self.ViewLess = function () {
        self.numViewable(self.originalNumViewable());
    };

    function FetchDocuments(item) {
        // Load the document data
        item.documentModel.loading(true);

        var json = {
            workOrderId: item.workOrderActivityId()
        };

        PromiseDWR({
            url: "my-account-json/checkIfDocumentsExist.json",
            data: json
        }).then(function (data) {
            if (data.errorMessages && data.errorMessages.length) {
                item.documentModel.hasError(true);
            } else {
                item.documentModel.hasDocuments(data.hasDocuments);
                item.documentModel.link(data.documentDownloadUrl);
            }
        }).catch(function (error) {
            item.documentModel.hasError(true);
        }).then(function () {
            item.documentModel.loaded(true);
            item.documentModel.loading(false);
        });
    }

    self.getServiceHistory = function () {
        if (!self.loaded()) {
            self.loading(true);
            wrapper.reset();
        }
    };

    var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getMyServiceHistory.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function (json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your service history.")
        },
        onResult: function (json) {
            self.loaded(true);

            if (json && json.history && json.history.length > 0) {
                self.history(json.history.map(function (item) {
                    // Split the name by comma and remove
                    // trailing/leading whitespace
                    var namePieces = item.technician.split(',').map(function (piece) {
                        return piece.trim();
                    });

                    // Rearrange name in reverse order
                    item.technician = namePieces.reverse().join(' ');

                    // Document model information
                    item.documentModel = {
                        loaded: false,
                        loading: false,
                        hasDocuments: false,
                        hasError: false,
                        link: '',
                    };

                    return ko.mapping.fromJS(item);
                }));
            }

            self.loading(false);
            self.numViewable(json.numViewable);
            self.originalNumViewable(json.numViewable);
            self.useNewDocumentDownload(GetFeatureToggleDefaultFalse("serviceHistoryNewDocumentDownload"));
        },
        json: true
    });

    wrapper.RefreshInitialization = function () {
        if (self.loaded()) {
            self.RefreshInitialization();
        }
    };

    return wrapper;
}
