function ContactInfo(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.loading = ko.observable(false);
	self.loaded = ko.observable(false);
	self.error = ko.observable();
	
	self.editing = ko.observable();
	self.updateAll = ko.observable();
	self.canNullPhone = ko.observable();
	self.firstName = ko.observable();
	self.lastName = ko.observable();
	self.address1 = ko.observable();
	self.address2 = ko.observable();
	self.city = ko.observable();
	self.state = ko.observable();
	self.zipcode = ko.observable();
	self.phone = ko.observable();
	self.email = ko.observable();
	self.svcAddress = {
		address1: ko.observable(),
		address2: ko.observable(),
		city: ko.observable(),
		state: ko.observable(),
		zipcode: ko.observable()
	};
	self.customerNumber = ko.observableArray();
	self.emailContactId = ko.observable();
	self.phoneContactId = ko.observable();
	self.addressConactId = ko.observable();
	
	self.originalModel = ko.observable();
	
	self.informationalMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.errorMessages = ko.observableArray();
	
	var isValidatingContactInfoLarge = false;
	
	self.EmailValidator = new EmailValidator(self.email, function() {
		if (isValidatingContactInfoLarge) {
			self.SubmitLarge();
		} else {
			self.SubmitSmall();
		}
	});
	
	self.AddressValidator = new AddressValidator({
		address1: self.address1,
		address2: self.address2,
		city: self.city,
		state: self.state,
		zipcode: self.zipcode
	}, function() {
		self.SubmitLarge();
	});
	
	self.RefreshInitialization = function() {
		self.Cancel();
		
		self.EmailValidator.Reset();
		self.AddressValidator.Reset();
	};
	
	self.getContactInfo = function() {
		if (!self.loaded()) {
			self.loading(true);
			wrapper.reset();
		}
	};
	
	self.SetEditMode = function() {
		self.EmailValidator.Reset();
		self.AddressValidator.Reset();
		
		self.editing(true);
		
		var original = self.toJSON();
		delete original.originalModel;
		self.originalModel(original);
	};
	
	self.Cancel = function() {
		SetValuesOnModel(self, self.originalModel());
		self.editing(false);
		
		self.RemoveAllErrors();
	};
	
	self.GetAddressLine = function(address) {
		var address1 = ko.unwrap(address.address1),
			address2 = ko.unwrap(address.address2);
		
		return address2 ? (address1 + ' ' + address2) : address1;
	};
	
	self.HasModelChanged = function() {
		if (!self.originalModel())
			return false;
		
		var inputs = ['firstName', 'lastName', 'address1', 'address2', 'city', 'zipcode', 'email'];
		var hasChanged = inputs.some(function(input) {
			return self[input]() != self.originalModel()[input];
		});
		
		if (hasChanged) {
			return true;
		}
		
		// Phone is a special case because sometimes it compares the formatted phone vs unformatted phone
		var oldPhone = ('' + self.originalModel().phone).replace(/[^\d]/g, ''),
			newPhone = ('' + self.phone()).replace(/[^\d]/g, '');
		
		if (oldPhone != newPhone) {
			return true;
		}
		
		// State is a special case because the dropdown automatically sets it to an empty string if the value is not a valid option
		// If states are different, or if having a state before is different than now
		var oldState = self.originalModel().state,
			newState = self.state();
		
		if (oldState != newState && !(!oldState && !newState)) {
			return true;
		}
		
		return false;
	};
	
	self.GetAddressHeader = function() {
		var address = self.svcAddress.address2();
		
		if (address) address += ', ';
		
		return address + self.svcAddress.address1();
	};
	
	self.SubmitSmall = function() {
		Submit(false);
	};
	
	self.SubmitLarge = function() {
		Submit(true);
	};
	
	function Submit(isLarge) {
		if (!self.HasModelChanged() || self.submitting())
			return;
		
		self.RemoveAllErrors();
		
		var json = self.toJSON();

		isValidatingContactInfoLarge = false;
		self.EmailValidator.isChoosing(false);
		
		if (isLarge) {
			self.AddressValidator.isChoosing(false);
		}
		
		if (isLarge && !self.AddressValidator.IsLastValidatedAddress()) {
			var address = {
				address1: json.address1,
				address2: json.address2,
				city: json.city,
				state: json.state,
				zipcode: json.zipcode
			};
			
			self.AddressValidator.isVerifying(true);
			self.submitting(true);
			
			QAS_VerifyAddress(address, function(model, error) {
				self.AddressValidator.isVerifying(false);
				self.submitting(false);
				
				if (!model.alternativeAddresses || !model.alternativeAddresses.length) {
					self.AddressValidator.KeepAddress();
				} else {
					self.AddressValidator.addresses(model.alternativeAddresses);
					self.AddressValidator.selectedAddress(model.alternativeAddresses[0]);
					
					self.AddressValidator.isChoosing(true);
				}
			});
		}
		else if (!self.EmailValidator.IsLastValidatedEmail()) {
			isValidatingContactInfoLarge = isLarge;
			
			self.EmailValidator.isVerifying(true);
			self.submitting(true);
			
			QAS_VerifyEmail(self.email(), function(data) {
				self.submitting(false);
				self.EmailValidator.isVerifying(false);
				
				if (data.corrections && data.corrections.length) {
					self.EmailValidator.emails(data.corrections);
					self.EmailValidator.selectedEmail(null);
					self.EmailValidator.isChoosing(true);
				}
				else if (data.errorMessages && data.errorMessages.length) {
					self.errorMessages(data.errorMessages);
				} else {
					self.EmailValidator.KeepEmail();
				}
			});
		} else {
			self.submitting(true);
			
			var url = isLarge ? 'MyAccountTMXContactInfoUIUtils/changeContactInfo' : 'MyAccountTMXContactInfoUIUtils/changeContactInfoSmall';
			
			PromiseDWR({ url: url, data: json }).then(function(data) {
				// Success message should be an alert
	    		if (data.errorMessages.length > 0 || data.fieldValidationErrors.length > 0) {
					self.errorMessages(data.errorMessages);
					self.fieldValidationErrors(data.fieldValidationErrors);
	    		} else {
	    			if (data.informationalMessages && data.informationalMessages.length) {
		    			CreateInformationalMessages(data, {
							$module: $('.module-contact-info-large')
						});

		    			data.informationalMessages = [];
		    			data.editing = false;
	    			}
					
					self.originalModel(data);
		    		SetValuesOnModel(self, ko.mapping.fromJS(data));

		    		self.EmailValidator.SetDefaultEmail(self.email());
		    		
		    		if (isLarge) {
			    		self.AddressValidator.SetDefaultAddress({
			    			address1: self.address1(),
			    			address2: self.address2(),
			    			city: self.city(),
			    			state: self.state(),
			    			zipcode: self.zipcode()
			    		});
			    		
						// Update header first and last name on successful saving
						$('#site-header .welcome .fname').text(self.firstName());
						$('#site-header .welcome .lname').text(self.lastName());
		    		}
				}
			}).catch(function(error) {
				self.errorMessages.push({ errorMessage: "An error has occurred." });
			}).then(function() {
				self.submitting(false);
			});
		}
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getContactInfo.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
        	self.loading(false);
            self.error("There was an error loading your contact information.");
        },
        onResult: function(json) {
			// Handle the property response
        	self.loaded(true);
        	self.loading(false);
        	self.error(null);
    		self.originalModel(json);
    		SetValuesOnModel(self, ko.mapping.fromJS(json));
    		
    		self.EmailValidator.SetDefaultEmail(self.email());
    		self.AddressValidator.SetDefaultAddress({
    			address1: self.address1(),
    			address2: self.address2(),
    			city: self.city(),
    			state: self.state(),
    			zipcode: self.zipcode()
    		});
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}