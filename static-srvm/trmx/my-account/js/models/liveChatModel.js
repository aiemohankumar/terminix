function LiveChatModel(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];

	self.loaded = ko.observable(false);
	self.error = ko.observable();
	
	self.isLiveChatEnabled = ko.observable(false);
	self.isLiveChatEnabledForProperty = ko.computed(function() {
		return property.customerType() && property.customerType() != property.CUSTOMER_TYPE_COMMERCIAL;
	});
	self.isLiveChatEnabledForSection = ko.computed(function() {
		return LiveChat_IsEnabledFor(LiveChat_TabToSection(propertiesList.tabLoadedName()));
	});
	
	self.showLiveChatButton = ko.computed(function() {
		return self.isLiveChatEnabled() && self.isLiveChatEnabledForProperty() && self.isLiveChatEnabledForSection();
	});
	
	self.LiveChatButtonClick = function() {
		if (self.showLiveChatButton()) {
			LiveChat_OpenChat();
		}
	};
	
	var wrapper = new AsyncWrapper(self, {
		getter: function() {
			IsChatAvailableChecker(function(data) {
				if (data.areAgentsAvailable || data.isVisitorEngaged) {
					self.isLiveChatEnabled(true);
					
					if (property == propertiesList.currentProperty()) {
						LiveChat_SetCustomVariables({
							customerId: property.customerId()
						});
					}
				} else {
					self.isLiveChatEnabled(false);
				}
			});
			
			self.loaded(true);
		}
	});
	
	return wrapper;
}