function PaymentModelSmall(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.error = ko.observable();
	self.loaded = ko.observable(false);
	self.loading = ko.observable();
	
	self.totalAmountDue = ko.observable();
	self.duePayments = ko.observableArray();
	self.totalAmountDue = ko.observable();
	self.reloadInvoiceHistory = true;
	
	self.termiteRenewalData = ko.computed(function() {
		return self.duePayments().filter(function(x) { return x.termiteRenewal() });
	});
	
	self.ViewPaymentHistory = function() {
		TAB.Load('payments', { focusModule: 'payment-history' });
	};

	self.ViewPayments = function() {
		TAB.Load('payments', { focusModule: 'due-payments' });
	};
	
	self.getDuePaymentsSmall = function() {
		if (!self.loaded()) {
			self.error(null);
			wrapper.reset();
		}
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getPaymentDueDataSmall.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your payments.");
        },
        onResult: function(json) {
			// Handle the property response
//        	debugger;
			self.duePayments(json.duePayments.map(function(item){
				return ko.mapping.fromJS(item);
			}));
			
			self.totalAmountDue(json.totalAmountDue);
			
        	self.loaded(true);
            self.loading(false);
            self.error(null);
		},
        json: true
    });
	
	return wrapper;
}