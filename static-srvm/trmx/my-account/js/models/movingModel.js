function MovingModel(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable(false);
	self.loading = ko.observable(false);
	self.error = ko.observable();
	
	var originalModel = ko.observable();

	self.canMoveProperty = ko.observable();
	self.canMovePropertyByOnline = ko.observable();
	self.canMovePropertyByPhone = ko.observable();
	self.mosquitoOnly = ko.observable();
	
	self.lookupZipcode = ko.observable();
	self.hasCheckedIfServiceable = ko.observable(false);
	self.isServiceable = ko.observable();
	self.franchisePhone = ko.observable();

	self.errorMessage = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.informationalMessages = ko.observableArray();
	
	self.hasPastDueBalance = ko.computed(function() {
		return property.paymentModelSmall.loaded() && property.paymentModelSmall().duePayments().length > 0;
	});
	
	self.RefreshInitialization = function() {
		SetValuesOnModel(self, ko.mapping.fromJS(originalModel()));
		
		self.hasCheckedIfServiceable(false);
	};
	
	self.CheckIfServiceable = function() {
		if (self.submitting() || !self.lookupZipcode()) return;
		
		self.submitting(true);
		self.hasCheckedIfServiceable(false);
		
		self.RemoveAllErrors();
		
		var json = self.toJSON();
		
		PromiseDWR({ url: 'my-account-json/isZipServiceable.json', data: json }).then(function(json) {
			SetValuesOnModel(self, ko.mapping.fromJS(json));
			
			self.hasCheckedIfServiceable(true);
		}).catch(function(error) {
			self.errorMessages.push({ errorMessage: error });
		}).then(function() {
			if (self.isServiceable() && !self.hasPastDueBalance()) {
				// Use session storage if their browser supports it, just to remember the ZIP
				// Otherwise, use the URL parameter
				if (caniuse.sessionStorage) {
					sessionStorage.setItem("MovingModelLookupZipcode", self.lookupZipcode());
					window.location.href = "/my-account/pages/moving.html";
				} else {
					window.location.href = "/my-account/pages/moving.html?zipcode=" + self.lookupZipcode();
				}
			} else {
				self.submitting(false);
			}
		});
	};
	
	var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getMovingModelRework.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
			self.loading(false);		
        },
        onResult: function(json) {
			// Handle the response
        	self.loaded(true);
        	self.loading(false);
			self.error(null);
			
        	originalModel(json);
        	SetValuesOnModel(self, ko.mapping.fromJS(json));
		},
        json: true
    });
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}