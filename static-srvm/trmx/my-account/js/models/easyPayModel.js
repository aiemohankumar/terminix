function EasyPayModel(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);
	
	var _updateOtherModelsWhenLoaded = false;

	self.loaded = ko.observable(false);
	self.loading = ko.observable(false);
	self.error = ko.observable();
	
	self.hasEnrolled = ko.observable(false);
	self.salesAgreements = ko.observableArray();
	self.storedPaymentMethods = ko.observableArray();
	self.enrollPaymentMethod = ko.observable();
	
	self.errorMessages = ko.observableArray();
	self.fieldValidationErrors = ko.observableArray();
	self.informationalMessages = ko.observableArray();

	self.setPaymentMethod = function(enrlPayment,event) {
		debugger;
		if(event.originalEvent){

			if(self.enrollPaymentMethod().type() == 'AddNewPayment') {
				self.property.flagPayment("AutoPay");
			}
		}
	}

	self.RemoveStoredPaymentMethod = function(paymentMethodId) {
		if (self.enrollPaymentMethod() && self.enrollPaymentMethod().paymentMethodId() == paymentMethodId) {
			self.enrollPaymentMethod(null);
		}
		
		self.storedPaymentMethods.remove(function(paymentMethod) {
			return paymentMethod.paymentMethodId() == paymentMethodId;
		});
		
		self.salesAgreements().forEach(function(salesAgreement) {
			if (salesAgreement.selectedPaymentMethod() && salesAgreement.selectedPaymentMethod().paymentMethodId() == paymentMethodId) {
				salesAgreement.selectedPaymentMethod(null);
			}
			
			salesAgreement.storedPaymentMethods.remove(function(paymentMethod) {
				return paymentMethod.paymentMethodId() == paymentMethodId;
			});
		});
	};
	
	self.UpdateStoredPaymentMethods = function(paymentMethods) {

		var enrollPaymentMethod = self.enrollPaymentMethod();
		
		self.storedPaymentMethods.remove(function(paymentMethod) {
			return paymentMethod.type() != 'card' && paymentMethod.type() != 'bank';
		});

		paymentMethods.forEach(function(paymentMethod) {
			var copiedPaymentMethod = $.extend({}, paymentMethod);
			copiedPaymentMethod._original = JSON.parse(JSON.stringify(copiedPaymentMethod));
			
			self.storedPaymentMethods.push(ko.mapping.fromJS(copiedPaymentMethod));
		});
		
		if (enrollPaymentMethod && enrollPaymentMethod.type() != 'card' && enrollPaymentMethod.type() != 'bank') {
			var paymentMethod = self.storedPaymentMethods().find(function(paymentMethod) {
				return enrollPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId();
			});
			
			if (paymentMethod) {
				SetValuesOnModel(paymentMethod, enrollPaymentMethod);
				
				self.enrollPaymentMethod(paymentMethod);
			} else {
				self.enrollPaymentMethod(null);
			}
		}
		
		self.salesAgreements().forEach(function(salesAgreement) {
			var selectedPaymentMethod = salesAgreement.selectedPaymentMethod();
			
			salesAgreement.storedPaymentMethods.remove(function(paymentMethod) {
				return paymentMethod.type() != 'card' && paymentMethod.type() != 'bank';
			});
			
			paymentMethods.forEach(function(paymentMethod) {
				var copiedPaymentMethod = $.extend({}, paymentMethod);
				copiedPaymentMethod._original = JSON.parse(JSON.stringify(copiedPaymentMethod));
				
				salesAgreement.storedPaymentMethods.push(ko.mapping.fromJS(copiedPaymentMethod));
			});
			
			if (selectedPaymentMethod && selectedPaymentMethod.type() != 'card' && selectedPaymentMethod.type() != 'bank') {
				var paymentMethod = salesAgreement.storedPaymentMethods().find(function(paymentMethod) {
					return selectedPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId();
				});
				
				if (paymentMethod) {
					SetValuesOnModel(paymentMethod, selectedPaymentMethod);
					
					salesAgreement.selectedPaymentMethod(paymentMethod);
				} else {
					salesAgreement.selectedPaymentMethod(null);
				}
			}
		});
	};
	
	self.UpdateDefaultBillingAddress = function(defaultBillingAddress) {
		self.storedPaymentMethods().forEach(function(paymentMethod) {
			if (paymentMethod.type() == 'card') {
				var original = ko.mapping.toJS(paymentMethod._original);
				
				// Only update the address if the user hasn't updated it already
				if (paymentMethod.billingAddress1() == original.billingAddress1) {
					paymentMethod.billingAddress1(defaultBillingAddress.address1);
					paymentMethod.billingAddress2(defaultBillingAddress.address2);
					paymentMethod.billingCity(defaultBillingAddress.city);
					paymentMethod.billingState(defaultBillingAddress.state);
					paymentMethod.billingZipcode(defaultBillingAddress.zipcode);
				}
				
				original.billingAddress1 = defaultBillingAddress.address1;
				original.billingAddress2 = defaultBillingAddress.address2;
				original.billingCity = defaultBillingAddress.city;
				original.billingState = defaultBillingAddress.state;
				original.billingZipcode = defaultBillingAddress.zipcode;
				
				SetValuesOnModel(paymentMethod._original, original);
			}
		});
		
		self.salesAgreements().forEach(function(salesAgreement) {
			salesAgreement.storedPaymentMethods().forEach(function(paymentMethod) {
				if (paymentMethod.type() == 'card') {
					var original = ko.mapping.toJS(paymentMethod._original);
					
					// Only update the address if the user hasn't updated it already
					if (paymentMethod.billingAddress1() == original.billingAddress1) {
						paymentMethod.billingAddress1(defaultBillingAddress.address1);
						paymentMethod.billingAddress2(defaultBillingAddress.address2);
						paymentMethod.billingCity(defaultBillingAddress.city);
						paymentMethod.billingState(defaultBillingAddress.state);
						paymentMethod.billingZipcode(defaultBillingAddress.zipcode);
					}
					
					original.billingAddress1 = defaultBillingAddress.address1;
					original.billingAddress2 = defaultBillingAddress.address2;
					original.billingCity = defaultBillingAddress.city;
					original.billingState = defaultBillingAddress.state;
					original.billingZipcode = defaultBillingAddress.zipcode;
					
					SetValuesOnModel(paymentMethod._original, original);
				}
			});
		});
	};
	
	self.RefreshInitialization = function() {
		self.salesAgreements().forEach(function(salesAgreement) {
			var original = $.extend({}, ko.mapping.toJS(salesAgreement._original));
			var paymentMethod;
			
			// Don't update paymentMethod since that'll update below
			if (original.hasOwnProperty('paymentMethod')) {
				paymentMethod = original.paymentMethod;
				delete original.paymentMethod;
			}
			
			SetValuesOnModel(salesAgreement, original);
			
			salesAgreement.storedPaymentMethods().forEach(function(storedPaymentMethod) {
				if (storedPaymentMethod.type() == 'bank' || storedPaymentMethod.type() == 'card') {
					SetValuesOnModel(storedPaymentMethod, ko.mapping.toJS(storedPaymentMethod._original));
				}
			});
			
			if (!paymentMethod) {
				salesAgreement.selectedPaymentMethod(null);
			} else {
				paymentMethod = ko.mapping.fromJS(paymentMethod);
				
				salesAgreement.storedPaymentMethods().some(function(storedPaymentMethod) {
					if (paymentMethod.type() == storedPaymentMethod.type()) {
						if (paymentMethod.type() == 'bank' || paymentMethod.type() == 'card' || paymentMethod.paymentMethodId() == salesAgreement.paymentMethod.paymentMethodId()) {
							SetValuesOnModel(paymentMethod, salesAgreement.paymentMethod);
							
							salesAgreement.selectedPaymentMethod(paymentMethod);
							return true;
						}
					}
				});
			}
		});
		
		self.storedPaymentMethods().forEach(function(storedPaymentMethod) {
			if (storedPaymentMethod.type() == 'bank' || storedPaymentMethod.type() == 'card') {
				SetValuesOnModel(storedPaymentMethod, ko.mapping.toJS(storedPaymentMethod._original));
			}
		});
		
		self.enrollPaymentMethod(null);
		
		self.RemoveAllErrors();
	};
	
	self.sortedSalesAgreements = ko.computed(function() {
		var salesAgreements = self.salesAgreements();
		
		return salesAgreements.filter(function(salesAgreement) {
			return salesAgreement.isActive();
		}).concat(salesAgreements.filter(function(salesAgreement) {
			return !salesAgreement.isActive();
		}));
	});
	
	function SortPaymentMethods(paymentMethods) {
		return paymentMethods.slice(0).sort(function(a, b) {
			var typeA = a.type(), typeB = b.type();
			
			if (typeA == typeB)
				return ('' + a.description()).localeCompare('' + b.description());
			if (typeA == 'saved' || typeA == '')
				return -1;
			if (typeB == 'saved' || typeB == '')
				return 1;
			if (typeA == 'bank')
				return -1;
			return 1;
		});
	}

	self.sortedPaymentMethods = ko.computed(function() {

		return SortPaymentMethods(self.storedPaymentMethods());
	});
	
	self.availableCreditCardYears = [];
	
	for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
		self.availableCreditCardYears.push(i);
	}

	self.SetVisibleHelp = function(item, field) {
		item.visibleHelp(field);
	};

	self.IsVisibleHelp = function(item, field) {
		return item.visibleHelp() == field;
	};
	
	function ScrollToItem(event) {
		var $summary = $(event.target).closest('tr.summary'),
			$detail = $summary.next();
		
		var offset = $summary.offset();
		offset.height = $summary.outerHeight() + $detail.outerHeight();
		offset.bottom = offset.top + offset.height;
		
		var windowTop = $(window).scrollTop();
		var windowHeight = $(window).height();
		var windowBottom = windowTop + windowHeight;
		
		if (offset.top > windowTop + (windowHeight * .2)) {
			ScrollTo(offset.top, 800);
		}
		else if (offset.bottom < windowTop + (windowHeight * .8)) {
			if (offset.height < windowHeight * .6) {
				ScrollTo(offset.top, 800);
			} else {
				ScrollTo(offset.bottom - (windowHeight * .8));
			}
		}
	}

	self.SetAddMode = function(item, event) {
		item.selectedPaymentMethod(null);
		
		item.modifyStep('add');
		
		ScrollToItem(event);
	};

	self.SetEditMode = function(item, event) {
		item.selectedPaymentMethod(item.storedPaymentMethods().find(function(storedPaymentMethod) {
			return storedPaymentMethod.type() == item.paymentMethod.type() && storedPaymentMethod.paymentMethodId() == item.paymentMethod.paymentMethodId();
		}));
		
		item.modifyStep('edit');
		
		ScrollToItem(event);
	};

	/*
	self.SetRemoveMode = function(item) {
		item.modifyStep('remove');
	};
	*/
	
	self.hasAgreementsSelected = ko.computed(function() {
		return self.salesAgreements().some(function(salesAgreement) {
			return salesAgreement.isActive();
		});
	});
	
	self.hasAgreementsSelected.subscribe(function(value) {
		if (self.loaded() && !self.hasEnrolled() && !value) {
			self.CancelEnrollForm();
		}
	});

	self.IsValidEnrollment = ko.computed(function() {
		if (!self.hasAgreementsSelected())
			return false;

		var paymentMethod = self.enrollPaymentMethod();

		if (!paymentMethod)
			return false;
		
		switch (paymentMethod.type()) {
			case 'card': {
				if (!paymentMethod.cardNumber() || !paymentMethod.cardExpireMonth() || !paymentMethod.cardExpireYear())
					return false;
				
				if (!paymentMethod.billingAddress1() || !paymentMethod.billingCity() || !paymentMethod.billingState() || !paymentMethod.billingZipcode())
					return false;
				break;
			}
			case 'bank': {
				if (!paymentMethod.bankRoutingNumber() || !paymentMethod.bankAccountNumber())
					return false;
				break;
			}
		}

		return true;
	});
	
	self.CancelEnrollForm = function() {
		if (self.submitting()) return;
		
		var fieldsToRevert = ['cardNumber', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1', 'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'bankRoutingNumber', 'bankAccountNumber'];
		
		self.storedPaymentMethods().forEach(function(paymentMethod) {
			fieldsToRevert.forEach(function(field) {
				paymentMethod[field]('');
			});
		});
		
		self.enrollPaymentMethod(null);
	};

	self.SubmitWallet = function() {
		debugger;
		window.frames.wallet.postMessage({
			action: 'submit'
		}, 'https://test.checkout.servicemaster.com');
    }
	
	self.SubmitEnrollForm = function() {
		if (!self.IsValidEnrollment() || self.submitting())
			return;

		self.RemoveAllErrors();

		self.submitting(true);

		var json = self.toJSON();
		json.enrollPaymentMethod.paymentMethodId = self.property.paymentMethodId();
        json.enrollPaymentMethod.paymentType = self.property.paymentMethodType();
		// json.enrollPaymentMethod.firstName = self.property.payFirstName();
		// json.enrollPaymentMethod.lastName = self.property.payLastName();
		json.enrollPaymentMethod.error = self.property.payErrorCode();

		console.log("paymentMethodId=" + json.enrollPaymentMethod.paymentMethodId);
        console.log("paymentType=" + json.enrollPaymentMethod.paymentType);
		// console.log("firstName=" + json.enrollPaymentMethod.firstName);
		// console.log("lastName=" + json.enrollPaymentMethod.lastName);
		console.log("errorCode=" + json.enrollPaymentMethod.error);

		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/enrollEasyPay', data: json }).then(function(data) {
			// Check if it was successful
			if (data.informationalMessages.length) {
				CreateInformationalMessages(data, {
					$module: $('.module-easy-pay')
				});
				
				// Make sure other models get updated when we refresh
				_updateOtherModelsWhenLoaded = true;
				
				// Refresh the entire model
				self.loaded(false);
				self.error('');
				wrapper.reset();
			} else {
				UpdateEasyPayModel(data);
			}
		}).catch(function(error) {
			self.errorMessages.push({ errorMessage: error });
		}).then(function() {
			self.submitting(false);
		});
	};

	self.IsValidEasyPay = function(item) {
		var paymentMethod = item.selectedPaymentMethod();
		if (!paymentMethod)
			return false;
		
		switch (paymentMethod.type()) {
			case 'card': {
				if (!paymentMethod.cardNumber() || !paymentMethod.cardExpireMonth() || !paymentMethod.cardExpireYear())
					return false;
				
				if (!paymentMethod.billingAddress1() || !paymentMethod.billingCity() || !paymentMethod.billingState() || !paymentMethod.billingZipcode())
					return false;
				break;
			}
			case 'bank': {
				if (!paymentMethod.bankRoutingNumber() || !paymentMethod.bankAccountNumber())
					return false;
				break;
			}
		}
		
		return true;
	};

	self.HasChangedEasyPay = function(item) {
		if (!item.selectedPaymentMethod()) {
			return false;
		}
		
		if (item.paymentMethod.type() == item.selectedPaymentMethod().type()
		&&  item.paymentMethod.paymentMethodId() == item.selectedPaymentMethod().paymentMethodId()) {
			return false;
		}
		
		return true;
	};
	
	function HandleSubmitSingleAutoPayResponse(item, data) {
		// Make sure the paymentMethod is not updated since that is the display one, not the form one
		// But we still need it on success, so keep it in a separate variable until it's needed
		var paymentMethod = null;
		
		if (data.hasOwnProperty('paymentMethod')) {
			paymentMethod = data.paymentMethod;
			delete data.paymentMethod;
		}
		
		SetValuesOnModel(item, data);

		// Check if it was successful
		if (data.informationalMessages.length) {
			CreateInformationalMessages(data, {
				$module: $('.module-easy-pay')
			});
			
			// Check if adding to AutoPay
			if (!item.isActive()) {
				// Set as active in AutoPay
				item.isActive(true);
				
				UpdateOtherModelsAutoPayStatuses();
			}
			
			// Close the form
			item.modifyStep('');
			
			// Since successful, update the payment method if there was one returned
			if (paymentMethod) {
				data.paymentMethod = paymentMethod;
				
				SetValuesOnModel(item, { paymentMethod: paymentMethod });
			} else {
				// Something went wrong... this should either be the already-stored payment method the user chose, or
				// it should be the new card/bank that the user stored. Must be an issue pulling the new stored method.
				// Refresh the whole model as an attempt to resolve this scenario.
				
				// Make sure other models get updated when we refresh
				_updateOtherModelsWhenLoaded = true;
				
				// Refresh the entire model
				self.loaded(false);
				self.error('');
				wrapper.reset();
				return;
			}
			
			// Update original sales agreement with new information
			SetValuesOnModel(item._original, JSON.parse(JSON.stringify(data)));
			
			if (data.updatedPaymentMethods && data.updatedPaymentMethods.length > 0) {
				// Update all stored payment methods lists
				self.storedPaymentMethods.remove(function(storedPaymentMethod) {
					return storedPaymentMethod.type() != 'card' && storedPaymentMethod.type() != 'bank';
				});
				
				self.salesAgreements().forEach(function(salesAgreement) {
					salesAgreement.storedPaymentMethods.remove(function(storedPaymentMethod) {
						return storedPaymentMethod.type() != 'card' && storedPaymentMethod.type() != 'bank';
					});
				});
				
				data.updatedPaymentMethods.forEach(function(updatedPaymentMethod) {
					// Add the stored payment method to the model's list
					var storedPaymentMethod = ko.mapping.fromJS(updatedPaymentMethod);
					self.storedPaymentMethods.push(storedPaymentMethod);
					
					self.salesAgreements().forEach(function(salesAgreement) {
						// Add the stored payment method to the sales agreement's list
						salesAgreement.storedPaymentMethods.push(storedPaymentMethod);
						
						var selectedPaymentMethod = salesAgreement.selectedPaymentMethod();
						if (selectedPaymentMethod
								&& selectedPaymentMethod.type() == storedPaymentMethod.type()
								&& selectedPaymentMethod.paymentMethodId() == storedPaymentMethod.paymentMethodId()) {
							// Update the selected stored payment method for the sales agreement
							salesAgreement.selectedPaymentMethod(storedPaymentMethod);
						}
					});
				});
				
				UpdateOtherModelsStoredPaymentMethods(data.updatedPaymentMethods);
			}
			
			if (data.updatedBillingAddress && data.updatedBillingAddress.address1) {
				// Update all default billing addresses
				self.storedPaymentMethods().forEach(function(storedPaymentMethod) {
					if (storedPaymentMethod.type() == 'card') {
						storedPaymentMethod.billingAddress1(data.updatedBillingAddress.address1);
						storedPaymentMethod.billingAddress2(data.updatedBillingAddress.address2);
						storedPaymentMethod.billingCity(data.updatedBillingAddress.city);
						storedPaymentMethod.billingState(data.updatedBillingAddress.state);
						storedPaymentMethod.billingZipcode(data.updatedBillingAddress.zipcode);
					}
				});
				
				self.salesAgreements().forEach(function(salesAgreement) {
					salesAgreement.storedPaymentMethods().forEach(function(storedPaymentMethod) {
						if (storedPaymentMethod.type() == 'card') {
							// Only update billing address if user hasn't modified it already
							var original = ko.mapping.toJS(storedPaymentMethod._original);
							
							if (storedPaymentMethod.billingAddress1() == original.billingAddress1) {
								storedPaymentMethod.billingAddress1(data.updatedBillingAddress.address1);
								storedPaymentMethod.billingAddress2(data.updatedBillingAddress.address2);
								storedPaymentMethod.billingCity(data.updatedBillingAddress.city);
								storedPaymentMethod.billingState(data.updatedBillingAddress.state);
								storedPaymentMethod.billingZipcode(data.updatedBillingAddress.zipcode);

								original.billingAddress1 = data.updatedBillingAddress.address1;
								original.billingAddress2 = data.updatedBillingAddress.address2;
								original.billingCity = data.updatedBillingAddress.city;
								original.billingState = data.updatedBillingAddress.state;
								original.billingZipcode = data.updatedBillingAddress.zipcode;
								
								SetValuesOnModel(storedPaymentMethod, { _original: ko.mapping.fromJS(original) });
							}
						}
					});
				});
				
				UpdateOtherModelsDefaultBillingAddress(data.updatedBillingAddress);
			}
		}
	}
	
	function UpdateOtherModelsAutoPayStatuses() {
		property.paymentModel.getData().then(function() {
			if (property.paymentModel.loaded()) {
				property.paymentModel().duePayments().forEach(function(duePayment) {
					var salesAgreement = self.salesAgreements().find(function(salesAgreement) {
						return salesAgreement.salesAgreementId() == duePayment.salesAgreementNumber();
					});
					
					duePayment.isEligibleForEasyPay(salesAgreement && !salesAgreement.isActive());
				});
			}
		}).catch(function(error) {
			console.log(error);
		});
	}
	
	function UpdateOtherModelsStoredPaymentMethods(paymentMethods) {
		property.paymentModel.getData().then(function() {
			property.paymentModel().UpdateStoredPaymentMethods(paymentMethods);
		}).catch(function(error) {
			console.log(error);
		});
		
		property.storedPayments.getData().then(function() {
			property.storedPayments().UpdateStoredPaymentMethods(paymentMethods);
		}).catch(function(error) {
			console.log(error);
		});
	}
	
	function UpdateOtherModelsDefaultBillingAddress(defaultBillingAddress) {
		// Update payment model default billing address
		property.paymentModel.getData().then(function() {
			property.paymentModel().UpdateDefaultBillingAddress(defaultBillingAddress);
		}).catch(function(error) {
			console.log(error);
		});
		
		// Reload contact info for the mailing address, in case it is defaulting to billing
		if (property.contactInfo.loaded()) {
			property.contactInfo.reset();
		}
	}

	self.CancelAddForm = function(item) {
		item.modifyStep('');

		self.RemoveAllErrors(item);
		item.selectedPaymentMethod(null);
		
		SetValuesOnModel(item, ko.mapping.toJS(item._original));

		item.storedPaymentMethods().forEach(function(storedPaymentMethod) {
			if (storedPaymentMethod.type() == 'bank' || storedPaymentMethod.type() == 'card') {
				SetValuesOnModel(storedPaymentMethod, ko.mapping.toJS(storedPaymentMethod._original));
			}
		});
		
		// Scroll back to the table row
		var position = $('.module-easy-pay').find('.sales-agreements-table').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};

	self.SubmitAddForm = function(item) {
		if (!self.IsValidEasyPay(item) || item.submitting())
			return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		var json = ko.mapping.toJS(item);

		json.paymentMethod = json.selectedPaymentMethod;
		json.typeOfAutoPaySubmit = "add"; //used to differentiate between which success message to show
		json.refreshDataOnSuccess = true;
		
		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/editEasyPay', data: json }).then(function(data) {
			HandleSubmitSingleAutoPayResponse(item, data);
		}).catch(function(error) {
			item.errorMessages.push({ errorMessage: error });
		}).then(function() {
			item.submitting(false);
		});
	};

	self.CancelEditForm = function(item) {
		item.modifyStep('');

		self.RemoveAllErrors(item);
		self.RemoveAllErrors(item.selectedPaymentMethod);
		
		SetValuesOnModel(item, ko.mapping.toJS(item._original));
		
		item.selectedPaymentMethod(item.storedPaymentMethods().find(function(storedPaymentMethod) {
			return storedPaymentMethod.type() == item.paymentMethod.type() && storedPaymentMethod.paymentMethodId() == item.paymentMethod.paymentMethodId();
		}));

		item.storedPaymentMethods().forEach(function(storedPaymentMethod) {
			if (storedPaymentMethod.type() == 'bank' || storedPaymentMethod.type() == 'card') {
				SetValuesOnModel(storedPaymentMethod, ko.mapping.toJS(storedPaymentMethod._original));
			}
		});

		// Scroll back to the table row
		var position = $('.module-easy-pay').find('.sales-agreements-table').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};

	self.SubmitEditForm = function(item) {
		if (!self.IsValidEasyPay(item) || !self.HasChangedEasyPay(item) || item.submitting())
			return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		var json = ko.mapping.toJS(item);

		json.paymentMethod = json.selectedPaymentMethod;
		json.typeOfAutoPaySubmit = "edit"; //used to differentiate between which success message to show
		json.refreshDataOnSuccess = true;
		
		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/editEasyPay', data: json }).then(function(data) {
			HandleSubmitSingleAutoPayResponse(item, data);
		}).catch(function(error) {
			item.errorMessages.push({ errorMessage: error });
		}).then(function() {
			item.submitting(false);
		});
	};

	/*
	self.CancelRemoveForm = function(item) {
		item.modifyStep('');

		self.RemoveAllErrors(item);

		// Scroll back to the table row
		var position = $('.module-easy-pay').find('.sales-agreements-table').offset().top;

		if (position < $(window).scrollTop()) {
			ScrollTo(position, 400);
		}
	};
	
	self.SubmitRemoveForm = function(item) {
		if (item.submitting())
			return;

		self.RemoveAllErrors(item);

		item.submitting(true);

		var json = ko.mapping.toJS(item);
		
		json.refreshDataOnSuccess = true;

		PromiseDWR({ url: 'MyAccountTMXPaymentUIUtils/removeEasyPay', data: json }).then(function(data) {
			// Update model
			SetValuesOnModel(item, data);

			// Check if it was successful
			if (data.informationalMessages.length) {
				CreateInformationalMessages(data, {
					$module: $('.module-easy-pay')
				});
			}
		}).catch(function(error) {
			item.errorMessages.push({ errorMessage: error });
		}).then(function() {
			item.submitting(false);
		});
	};
	*/
	
	self.getEasyPayModel = function() {
		if (!self.loaded()) {
			self.error('');
			
			wrapper.reset();
		}
	};
	
	function UpdateEasyPayModel(json) {
		json.salesAgreements.forEach(function(salesAgreement) {
        	salesAgreement.submitting = false;
        	salesAgreement.modifyStep = '';
        	
        	salesAgreement._original = JSON.parse(JSON.stringify(salesAgreement));
        });
		
		json.storedPaymentMethods.forEach(function(storedPaymentMethod) {
			storedPaymentMethod._original = JSON.parse(JSON.stringify(storedPaymentMethod));
		});
		
		var model = ko.mapping.fromJS(json);
		
		model.salesAgreements().forEach(function(salesAgreement) {
			salesAgreement.selectedPaymentMethod = ko.observable();
			
			var storedPaymentMethods = model.storedPaymentMethods().map(function(storedPaymentMethod) {
				// Create a copy of the root's payment methods
				return ko.mapping.fromJS(JSON.parse(JSON.stringify(ko.mapping.toJS(storedPaymentMethod))));
			});
			
			salesAgreement.storedPaymentMethods = ko.observableArray(storedPaymentMethods);

			salesAgreement.sortedPaymentMethods = ko.computed(function() {
				return SortPaymentMethods(salesAgreement.storedPaymentMethods());
			});
			
			// If it is an observable, that means it isn't an object of real data and it is an observable pointing to null
			// Default it to an empty payment method object
			if (ko.isObservable(salesAgreement.paymentMethod)) {
				salesAgreement.paymentMethod = ko.mapping.fromJS({
					type: '',
					paymentMethodId: '',
					description: ''
				});
			} else {
				storedPaymentMethods.some(function(paymentMethod) {
					if (paymentMethod.type() == salesAgreement.paymentMethod.type()) {
						if (paymentMethod.type() == 'bank' || paymentMethod.type() == 'card' || paymentMethod.paymentMethodId() == salesAgreement.paymentMethod.paymentMethodId()) {
							SetValuesOnModel(paymentMethod, salesAgreement.paymentMethod);
							
							salesAgreement.selectedPaymentMethod(paymentMethod);
							return true;
						}
					}
				});
			}
		});
        
    	self.hasEnrolled(model.hasEnrolled());
    	self.salesAgreements(model.salesAgreements());
    	self.storedPaymentMethods(model.storedPaymentMethods());
    	self.errorMessages(model.errorMessages());
    	self.fieldValidationErrors(model.fieldValidationErrors());
    	self.informationalMessages(model.informationalMessages());
    	
		self.enrollPaymentMethod(null);

		if (json.enrollPaymentMethod) {
			debugger;
			self.storedPaymentMethods().some(function(paymentMethod) {
				debugger;
				if (paymentMethod.type() == json.enrollPaymentMethod.type) {
					if (paymentMethod.type() == 'bank' || paymentMethod.type() == 'card' || paymentMethod.paymentMethodId() == json.enrollPaymentMethod.paymentMethodId) {
						SetValuesOnModel(paymentMethod, json.enrollPaymentMethod);
						
						self.enrollPaymentMethod(paymentMethod);
						return true;
					}
				}
			});
		}
		
		// Check if other models need the updated data from loading
		if (_updateOtherModelsWhenLoaded) {
			_updateOtherModelsWhenLoaded = false;
			
			// Update due payments with available AutoPay agreements
			UpdateOtherModelsAutoPayStatuses();
			
			// Filter out the non-stored payment methods since other models only need the list of stored
			var storedPaymentMethods = json.storedPaymentMethods.filter(function(storedPaymentMethod) {
				return storedPaymentMethod.type != 'bank' && storedPaymentMethod.type != 'card';
			});
			
			UpdateOtherModelsStoredPaymentMethods(storedPaymentMethods);
			
			// Get the default billing address from the credit card payment method
			// Since the default billing address is only on card payment method types
			var defaultBillingAddress = {
				address1: '',
				address2: '',
				city: '',
				state: '',
				zipcode: ''
			};
			
			json.storedPaymentMethods.some(function(paymentMethod) {
				if (paymentMethod.type == 'card') {
					defaultBillingAddress.address1 = paymentMethod.billingAddress1;
					defaultBillingAddress.address2 = paymentMethod.billingAddress2;
					defaultBillingAddress.city = paymentMethod.billingCity;
					defaultBillingAddress.state = paymentMethod.billingState;
					defaultBillingAddress.zipcode = paymentMethod.billingZipcode;
					return true;
				}
			});
			
			UpdateOtherModelsDefaultBillingAddress(defaultBillingAddress);
		}
	}
	
	var wrapper = new AsyncWrapper(self, {
		url: "my-account-json/getEasyPayModel.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your Auto Pay agreements.");
        },
        onResult: function(json) {
            self.loading(false);
            self.loaded(true);
            self.error('');
            
            UpdateEasyPayModel(json);
		},
        json: true
	});
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}