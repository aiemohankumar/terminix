function PaymentHistory(property) {
	var self = this;
	
	self.property = property;
	
	// used to tell self.toJSON() what fields on the model to ignore
	self.__mapping_ignore = ['property'];
	
	ViewModel_AddCommonFunctions(self);

	self.loaded = ko.observable(false);
	self.loading = ko.observable(false);
	self.error = ko.observable();
	
	self.history = ko.observableArray();

	self.defaultNumViewable = 4;
	self.numViewableIncrease = 4;
	self.numViewable = ko.observable(self.defaultNumViewable);
	
	self.RefreshInitialization = function() {
		self.numViewable(self.defaultNumViewable);
	};
	
	self.viewableHistory = ko.computed(function() {
		return self.history().slice(0, self.numViewable());
	});
	
	self.canViewMore = ko.computed(function() {
		return self.numViewable() < self.history().length;
	});
	
	self.canViewLess = ko.computed(function() {
		return self.numViewable() > self.defaultNumViewable;
	});
	
	self.ViewMore = function() {
		self.numViewable(Math.min(self.numViewable() + self.numViewableIncrease, self.history().length));
	};
	
	self.ViewLess = function() {
		self.numViewable(self.defaultNumViewable);
	};
	
	self.getPaymentHistory = function() {
		if (!self.loaded()) {
			self.error('');
			
			wrapper.reset();
		}
	};
	
	var wrapper = new AsyncWrapper(self, {
		url: "my-account-json/getPaymentHistoryRework.json",
        data: {
        	jsonData: function() {
        		return JSON.stringify({ customerNumber: property.customerId() });
        	}
        },
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function(json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your payment history.");
        },
        onResult: function(json) {
            self.loading(false);
            self.loaded(true);
            self.error('');
            
            self.history(json.history);
		},
        json: true
	});
	
	wrapper.RefreshInitialization = function() {
		if (self.loaded()) {
			self.RefreshInitialization();
		}
	};
	
	return wrapper;
}