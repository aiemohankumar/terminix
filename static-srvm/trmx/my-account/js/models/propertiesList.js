function PropertyList() {
	var self = this;

	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable(false);
	self.properties = ko.observableArray();
	self.currentProperty = ko.observable();
	self.propertyToRemove = ko.observable();
	self.confirmedRemoveProperty = ko.observable(false);
	self.propertyDropdownOpen = ko.observable(false);
	self.changingProperty = ko.observable(false);
	self.tabLoadedName = ko.observable();

	self.getProperties = function() {
		var self = this;
	
		return new Promise(function(resolve, reject) {
			if (self.loaded()) {
				resolve();
				return;
			}

		//	PromiseDWR("MyAccountTMXLoginUIUtils/getPropertyData").then(function(data) {
			PromiseDWR("my-account-json/getPropertyData.json").then(function(data) {	
		// Handle the property response
				self.properties(data.properties.map(function(propertyData) {
					return new Property(propertyData);
				}));
				
				self.currentProperty(self.properties().find(function(property) {
					return !data.currentPropertyId || property.propertyId() == data.currentPropertyId;
				}));
			}).catch(function(error) {
				// Do something with error?
				reject(error);
			}).then(function() {
				// Finally handle what happened either way
				self.loaded(true);
				resolve();
			});
		});
	};
	
	self.togglePropertyDropdown = function() {
		if (self.propertyDropdownOpen()) {
			if (!(self.changingProperty() || self.propertyToRemove() && self.propertyToRemove().propertyId())) {
				self.propertyDropdownOpen(false);
			}
		} else {
			self.propertyDropdownOpen(true);
		}
	};
	
	self.propertyDropdownOpen.subscribe(function(value) {
		if (!value) {
			self.propertyToRemove(null);
			self.confirmedRemoveProperty(false);
			self.changingProperty(false);
			self.confirmedRemoveProperty(false);
		}
	});

	self.confirmRemoveProperty = function(property) {
		self.propertyToRemove(property);
	};
	
	self.removeProperty = function() {
		var property = self.propertyToRemove();
		if (!property) return;
		
		// === DESKTOP =========================
		self.changingProperty(true);
		// === MOBILE ==========================
		self.confirmedRemoveProperty(true);
		
		var json = { propertyId: property.propertyId() };
		
		PromiseDWR({ url: "MyAccountTMXUIUtils/removeProperty", data: json }).then(function(data) {
			if (data.errorMessages && data.errorMessages.length) {
				throw "error";
			} else {
				// Show an alert with the address text
				CreateAlert({
					color: 'green',
					message: "Successfully removed " + property.address() + " from your account.",
					button: false,
					closeButton: true
				});

				self.properties.remove(property);
			}
		}).catch(function(error) {
			CreateAlert({
				color: 'red',
				message: "There was an error removing this property.",
				button: {
					text: 'Try Again'
				},
				onButtonClick: function(e) {
					// Remove the alert
					$(this).remove();
					
					// Show the property dropdown for loading
					self.propertyDropdownOpen(true);
					
					// Remove this property again
					self.propertyToRemove(property);
					self.removeProperty();
				}
			});
		}).then(function() {
			self.propertyDropdownOpen(false);
		});
	};
	
	self.cancelRemoveProperty = function() {
		self.propertyToRemove(null);
	};
	
	self.loadProperty = function(property) {
		self.changingProperty(true);
		
		var json = { propertyId: property.propertyId() };
		
		PromiseDWR({ url :"my-account-json/changePropertyRework.json", data: json }).then(function(data) {
			if (data.errorMessages && data.errorMessages.length) {
				throw "error";
			} else {
				// Update payments tab hiding fields
				property.isHidePaymentsTab(data.isHidePaymentsTab);
				property.isHidePaymentsTabCheck(data.isHidePaymentsTabCheck);
				
				//May need to add something about updating the paymentTabHidden?
				// Update the information in Tealium utag data
				if (typeof utag != 'undefined' && typeof utag.view == 'function') {
					utag_data.customer_id = data.customerId;
					utag.view({ customer_id: data.customerId });
				}
				
				RemoveAllAlerts();
				
				property.RefreshInitialization();
				
				self.currentProperty(property);
				
				TAB.Reload();
			}
		}).catch(function() {
			CreateAlert({
				color: 'red',
				message: "There was an error switching properties.",
				button: {
					text: 'Try Again'
				},
				onButtonClick: function(e) {
					// Remove the alert
					$(this).remove();
					
					// Show the property dropdown for loading
					self.propertyDropdownOpen(true);
					
					// Remove this property again
					self.loadProperty(property);
				}
			});
		}).then(function() {
			self.propertyDropdownOpen(false);
		});
	};
}