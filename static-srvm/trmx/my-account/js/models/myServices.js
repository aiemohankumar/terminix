function MyServices(property) {
    var self = this;

    self.property = property;

    // used to tell self.toJSON() what fields on the model to ignore
    self.__mapping_ignore = ['property'];

    ViewModel_AddCommonFunctions(self);
    ViewModel_AddScheduleFunctions(self);

    self.loaded = ko.observable(false);
    self.loading = ko.observable(false);
    self.error = ko.observable();
    self.services = ko.observableArray();

    var ignoreRescheduleStepChange = false;

    self.RefreshInitialization = function () {
        self.services().forEach(function (service) {
            service.showDetail(false);

            ignoreRescheduleStepChange = true;
            service.rescheduleStep('');

            service.submitting(false);

            self.RemoveAllErrors(service);

            // appointment step
            service.rescheduleSelectedDate(null);
            service.rescheduleSelectedTimeObject(null);

            // contact step
            service.rescheduleContactFname(service._original.rescheduleContactFname());
            service.rescheduleContactLname(service._original.rescheduleContactLname());
            service.rescheduleContactPhone(service._original.rescheduleContactPhone());
            service.rescheduleContactDate('');
            service.rescheduleContactTime('');
        });
    };

    self.GetAddressHeader = function () {
        return property.address();
    };

    self.CanScheduleFollowup = function (item) {
        if (!GetFeatureToggleDefaultFalse("restrictRescheduleByDate"))
            return item.canReschedule();

        var isServiceScheduledSoon = false;
        var date = new Date();
        var today = date.toLocaleDateString("en-US");

        date.setDate(date.getDate() + 1);

        var tomorrow = date.toLocaleDateString("en-US");

        property.scheduledServices().services().forEach(function (service) {
            var serviceDate = new Date(service.date());

            serviceDate = serviceDate.toLocaleDateString("en-US");

            if (service.serviceLine() === item.serviceLine() && (serviceDate === today || serviceDate === tomorrow)) {
                isServiceScheduledSoon = true;
            }
        });

        return !isServiceScheduledSoon && item.canReschedule();
    };

    self.GetFollowupService = function (item) {
        return self.property.serviceHistory.getData()
            .then(function (data) {
                var svc = ko.mapping.toJS(data.history()).find(function (service) {
                    return ko.mapping.toJS(item.followupServiceLines).find(function (line) {
                            return line == service.serviceLine;
                        }) &&
                        (service.serviceType == "INI" || service.serviceType == "REG");
                });
                return svc;
            });
    };

    self.FormatFollowupServiceDescription = function (desc) {
        return desc && desc.replace(/\sInitial|\sRegular|\sINI|\sREG/, '') || '';
    };

    self.Reschedule = function (item) {
        if (item.submitting()) return;

        if (!item.rescheduleLoaded()) {

            if (GetFeatureToggleDefaultFalse("restrictTechOnScheduleFollowUp") ||
                GetFeatureToggleDefaultFalse("useBundleItemServiceLinesOnScheduleFollowUp")) {
                self.GetFollowupService(item)
                    .then(function (followupService) {
                        if (GetFeatureToggleDefaultFalse("restrictTechOnScheduleFollowUp")) {
                            item.followupTechnicianNumber(followupService && followupService.technicianNumber || '');
                        }
                        if (GetFeatureToggleDefaultFalse("useBundleItemServiceLinesOnScheduleFollowUp")) {
                            item.followupServiceDescription(followupService && followupService.description || '');
                            item.followupServiceLine(followupService && followupService.serviceLine || item.serviceLine());
                        }
                        var json = self.GetModelJSON(item);

                        item.submitting(true);

                        PromiseDWR({
                            url: 'my-account-json/scheduleAdditional.json',
                            data: json
                        }).then(function (data) {
                            // Update item model
                            self.ApplyModelChanges(item, data);

                            // Create the calendars needed with the reschedule dates
                            if (item.rescheduleStep() == 'appointment') {
                                self.CreateRescheduleMonths(item);
                            }

                            // Set it as loaded
                            item.rescheduleLoaded(item.rescheduleStep() != 'error');
                        }).catch(function (error) {
                            console.log("Error in scheduleAdditional:", error);
                            // Do something with error?
                            item.errorMessages.push({
                                errorMessage: "An error has occurred"
                            });
                            item.rescheduleStep('error');
                        }).then(function () {
                            item.submitting(false);
                        });
                    });
            } else {
                var json = self.GetModelJSON(item);

                item.submitting(true);

                PromiseDWR({
                    url: 'my-account-json/scheduleAdditional.json',
                    data: json
                }).then(function (data) {
                    // Update item model
                    self.ApplyModelChanges(item, data);

                    // Create the calendars needed with the reschedule dates
                    if (item.rescheduleStep() == 'appointment') {
                        self.CreateRescheduleMonths(item);
                    }

                    // Set it as loaded
                    item.rescheduleLoaded(item.rescheduleStep() != 'error');
                }).catch(function (error) {
                    console.log("Error in scheduleAdditional:", error);
                    // Do something with error?
                    item.errorMessages.push({
                        errorMessage: "An error has occurred"
                    });
                    item.rescheduleStep('error');
                }).then(function () {
                    item.submitting(false);
                });
            }
        } else if (item.months().length) {
            item.rescheduleStep('appointment');
        } else if (item.rescheduleDates()) {
            // Create the calendars needed with the reschedule dates
            self.CreateRescheduleMonths(item);

            item.rescheduleStep('appointment');
        } else {
            item.rescheduleStep('contact');
        }
    };

    function ConfirmDetailsDWR(item, json) {
        json = json || self.GetModelJSON(item);

        item.submitting(true);

        return new Promise(function (resolve, reject) {
            PromiseDWR({
                url: 'my-account-json/scheduleAdditional.json',
                data: json
            }).then(function (data) {
                item.submitting(false);

                // Ignore rescheduleSelectedDate being set
                if (data && 'rescheduleSelectedDate' in data) {
                    delete data['rescheduleSelectedDate'];
                }

                resolve(data);
            }).catch(function (error) {
                item.submitting(false);

                reject(error);
            });
        });
    }

    self.ConfirmAppointment = function (item) {
        if (item.submitting() || !self.IsValidReschedule(item)) return;

        self.RemoveAllErrors(item);

        if (!item.rescheduleSelectedDate() || !item.rescheduleSelectedTimeObject()) {
            item.errorMessages.push({
                errorMessage: "Please choose an appointment date and time."
            });
        } else if (item.serviceLine() == "NMOSQ") {
            var json = self.GetModelJSON(item);

            json.rescheduleStep = "details";

            ConfirmDetailsDWR(item, json).then(function (model) {
                // Keep user on appointment step until the error check a few lines below
                model.rescheduleStep = "appointment";

                // Update model
                self.ApplyModelChanges(item, model);

                // Go to next step if no errors
                if (!item.errorMessages().length && !item.fieldValidationErrors().length) {
                    item.rescheduleStep('confirmationAppointment');

                    OnConfirmedAppointment(item, model);
                }
            }).catch(function (error) {
                item.errorMessages.push({
                    errorMessage: "An error has occurred."
                });
            });
        } else {
            item.rescheduleStep('details');
        }
    };

    self.ConfirmContact = function (item) {
        if (item.submitting() || !self.IsValidContact(item)) return;

        self.RemoveAllErrors(item);

        var json = self.GetModelJSON(item);

        item.submitting(true);

        PromiseDWR({
            url: 'my-account-json/scheduleAdditional.json',
            data: json
        }).then(function (data) {
            // Update model
            self.ApplyModelChanges(item, data);

            // Go to next step if no errors
            if (!item.errorMessages().length && !item.fieldValidationErrors().length) {
                item.rescheduleStep('confirmationContact');
            }
        }).catch(function (error) {
            item.errorMessages.push({
                errorMessage: "An error has occurred."
            });
        }).then(function () {
            item.submitting(false);
        });
    };

    self.ConfirmDetails = function (item) {
        if (item.submitting()) return;

        self.RemoveAllErrors(item);

        ConfirmDetailsDWR(item).then(function (model) {
            // Update model
            self.ApplyModelChanges(item, model);

            // Go to next step if no errors
            if (!item.errorMessages().length && !item.fieldValidationErrors().length) {
                item.rescheduleStep('confirmationAppointment');

                OnConfirmedAppointment(item, model);
            }
        }).catch(function (error) {
            item.errorMessages.push({
                errorMessage: "An error has occurred."
            });
        });
    };

    function PrepareService(item) {
        // For showing service detail view
        item.showDetail = false;
        item.guaranteeCertificateUrl = item.guaranteeCertificateUrl || '';

        // This is what is used to know the selected time
        // since time is now an object with a time and
        // employee number
        item.rescheduleSelectedTimeObject = null;

        item.months = [];
        item._original = $.extend({}, item);
    }

    function OnConfirmedAppointment(item, model) {
        // Delete the updated services from the item, for cleanup purposes
        if (ko.isObservable(item.updatedMyServices)) {
            item.updatedMyServices([]);
        }

        // Update my services with item.updatedMyServices
        if (model.updatedMyServices && model.updatedMyServices.length) {
            model.updatedMyServices.forEach(function (service) {
                // Find the item to update in the model
                var updateItem = self.services().find(function (updateItem) {
                    return updateItem.serviceNumber() == service.serviceNumber;
                });

                if (updateItem) {
                    // Remove information from the service that we don't want to update the item with
                    var fieldsToNotUpdate = ['rescheduleSelectedDate', 'rescheduleStep'];

                    fieldsToNotUpdate.forEach(function (field) {
                        if (service.hasOwnProperty(field)) {
                            delete service[field];
                        }
                    });

                    // Update the item with the new service information
                    self.ApplyModelChanges(updateItem, service);
                } else {
                    // Add to My Services
                    PrepareService(service);

                    self.services.push(ko.mapping.fromJS(service));
                }
            });
        }

        if (property.scheduledServices.loaded()) {
            property.scheduledServices.reset();
        }
    }

    self.getMyServices = function () {
        if (!self.loaded()) {
            self.loading(true);
            wrapper.reset();
        }
    };

    var wrapper = new AsyncWrapper(self, {
        url: "my-account-json/getMyServicesData.json",
        data: {},
        dwr: true,
        ajaxOptions: {
            type: "GET"
        },
        onError: function (json) {
            self.loaded(false);
            self.loading(false);
            self.error("There was an error loading your services.");
        },
        onResult: function (json) {
            self.loaded(true);
            self.loading(false);

            // Add properties that aren't on the back-end
            self.services(json.services.map(function (service) {
                PrepareService(service);

                return ko.mapping.fromJS(service);
            }));

            // Mobile support for switching steps and scrolling user to top of new step
            self.services().forEach(function (service, index) {
                service.rescheduleStep.subscribe(function (step) {
                    if (ignoreRescheduleStepChange) {
                        ignoreRescheduleStepChange = false;
                        return;
                    }

                    if (Viewport.isMobile()) {
                        var $row = $('.module-my-services table:visible tr.summary').eq(index);

                        // If on a step, then scroll to the next row which is the "detail" view
                        if (step) {
                            $row = $row.next();
                        }

                        var pos = $row.offset() && $row.offset().top || 0;

                        ScrollTo(pos, 'fast');
                    }
                });
            });
        },
        json: true
    });

    wrapper.RefreshInitialization = function () {
        if (self.loaded()) {
            self.RefreshInitialization();
        }
    };

    return wrapper;
}
