function ViewModel() {
	var self = this;
	
	ViewModel_AddCommonFunctions(self);
	
	self.loaded = ko.observable(false);
	self.environments = ko.observableArray();
	self.newEnvironmentName = ko.observable();
	self.showAnalysis = ko.observable(false);
	self.selectedBrand = ko.observable();
	self.isComparing = ko.observable(false);
	self.isExporting = ko.observable(false);
	self.selectedEnvironment = ko.observable();
	self.hasResolvedMissingFeatures = ko.observable(false);
	
	function AddEnvironment(name, url) {
		var environment = {
			name: name,
			url: url,
			json: ko.observable(),
			loaded: ko.observable(false),
			loading: ko.observable(false),
			loadError: ko.observable(false)
		};
		
		environment.json.subscribe(function(value) {
			environment.loaded(false);
			environment.loadError(false);
		});
		
		environment.data = ko.computed(function() {
			var data = null;
			
			try {
				data = JSON.parse(environment.json());
			} catch (e) {}
			
			return data;
		});
		
		environment.error = ko.computed(function() {
			return environment.json() && !(environment.data() && environment.data().brandList && environment.data().brandList.length > 0);
		});
		
		self.environments.push(environment);
	}
	
	var host = location.hostname.replace(/^(.*\.)?([^\.]+\.[^\.]+)$/, "$2");
	
	function GetPrepopulateURL(subdomain) {
		return "https://" + subdomain + host + "/my-account/tools/featureToggleJson.jsp";
	} 

	AddEnvironment("BIZ", GetPrepopulateURL("localhost."));
	AddEnvironment("DEV", GetPrepopulateURL("dev."));
	AddEnvironment("TEST", GetPrepopulateURL("test."));
	AddEnvironment("PROD", GetPrepopulateURL("www."));
	
	self.AddEnvironment = function() {
		if (self.newEnvironmentName()) {
			AddEnvironment(self.newEnvironmentName());
			self.newEnvironmentName('');
		}
	};
	
	self.LoadEnvironmentJSON = function(environment) {
		if (environment.loading()) return;
		
		var handlerName = "LoadEnvironmentJSON_" + environment.name + "_" + Math.floor(Math.random() * 10000000);
		
		window[handlerName] = function(json) {
			environment.json(JSON.stringify(json));
			environment.loading(false);
			environment.loaded(true);
			
			delete window[handlerName];
		};
		
		environment.loading(true);
		environment.loadError(false);
		
		var script = document.createElement('script');
		script.src = environment.url + "?foo=" + handlerName;
		script.onerror = function() {
			environment.loading(false);
			environment.loadError(true);
		};
		document.head.appendChild(script);
	};
	
	self.DeleteEnvironment = function(environment) {
		self.environments.remove(environment);
	};
	
	self.validEnvironments = ko.computed(function() {
		return self.environments().filter(function(environment) {
			return !environment.error() && environment.data();
		});
	});
	
	self.hasEnvironmentsToCompare = ko.computed(function() {
		return self.validEnvironments().length > 1;
	});
	
	self.brandList = ko.computed(function() {
		var brandList = [];
		var brandMap = {}; // used to know which brands have been created without searching the array
		
		var environments = self.validEnvironments();
		
		// Compile a list of brands from all environments and their feature list per environment
		environments.forEach(function(environment) {
			environment.data().brandList.forEach(function(brandObject) {
				var mappedBrandObject = brandObject;
				
				if (brandMap[brandObject.brandName]) {
					mappedBrandObject = brandMap[brandObject.brandName];
				} else {
					brandObject.environments = {};
					
					environments.forEach(function(environment) {
						brandObject.environments[environment.name] = [];
					});
					
					brandMap[brandObject.brandName] = brandObject;
					brandList.push(brandObject);
				}
				
				mappedBrandObject.environments[environment.name] = brandObject.featureMap;
			});
		});
		
		brandList.sort(function(a, b) {
			return a.brandName.toLowerCase().localeCompare(b.brandName.toLowerCase());
		});
		
		// Figure out which brand's features are missing per environment
		brandList.forEach(function(brandObject) {
			var featureList = [];
			var featureMap = {}; // used to know which features have been created without searching the array
			
			Object.keys(brandObject.environments).forEach(function(environmentName) {
				var environmentFeatureList = brandObject.environments[environmentName];
				
				if (environmentFeatureList && environmentFeatureList.length) {
					environmentFeatureList.forEach(function(featureObject) {
						var mappedFeatureObject = featureObject;
						
						if (featureMap[featureObject.featureName]) {
							mappedFeatureObject = featureMap[featureObject.featureName];
						} else {
							featureObject.environments = {};
							featureObject.correctedValue = ko.observable();
							
							environments.forEach(function(environment) {
								featureObject.environments[environment.name] = {
									exists: false,
									enabled: false,
									css: {},
									text: "?"
								};
							});
							
							featureMap[featureObject.featureName] = featureObject;
							featureList.push(featureObject);
						}
						
						var enabled = featureObject.enabled === "true" || featureObject.enabled === true;

						$.extend(mappedFeatureObject.environments[environmentName], {
							exists: true,
							enabled: enabled,
							css: {
								'text-success': enabled,
								'text-danger': !enabled
							},
							text: enabled ? "ON" : "OFF"
						});
					});
				}
			});
			
			featureList.sort(function(a, b) {
				return a.featureName.toLowerCase().localeCompare(b.featureName.toLowerCase());
			});
			
			brandObject.featureList = featureList;
			brandObject.isComparing = ko.observable(false);
			
			brandObject.missingFeatureList = ko.computed(function() {
				if (!self.selectedEnvironment()) return [];
				
				return featureList.filter(function(featureObject) {
					return !featureObject.environments[self.selectedEnvironment().name].exists;
				});
			});
		});
		
		return brandList;
	});
	
	self.environmentHeaders = ko.computed(function() {
		return self.validEnvironments().map(function(environment) {
			return environment.name;
		});
	});
	
	self.compareBrandList = ko.computed(function() {
		return self.brandList().filter(function(brandObject) {
			return brandObject.isComparing();
		});
	});
	
	self.compareFeatureList = ko.computed(function() {
		var featureList = [];
		var featureMap = {}; // used to know which features have been created without searching the array
		
		var environments = self.validEnvironments();
		
		self.compareBrandList().forEach(function(brandObject) {
			brandObject.featureList.forEach(function(featureObject) {
				var mappedFeatureObject = featureObject;
				
				if (featureMap[featureObject.featureName]) {
					mappedFeatureObject = featureMap[featureObject.featureName];
				} else {
					mappedFeatureObject = {
						featureName: featureObject.featureName,
						brands: {}
					};
					
					self.compareBrandList().forEach(function(brandObject) {
						mappedFeatureObject.brands[brandObject.brandName] = {
							brandName: brandObject.brandName,
							environments: {}
						};
						
						environments.forEach(function(environment) {
							mappedFeatureObject.brands[brandObject.brandName].environments[environment.name] = {
								exists: false,
								enabled: false,
								css: {},
								text: "?"
							};
						});
					});
					
					featureMap[featureObject.featureName] = mappedFeatureObject;
					featureList.push(mappedFeatureObject);
				}
				
				environments.forEach(function(environment) {
					$.extend(mappedFeatureObject.brands[brandObject.brandName].environments[environment.name], featureObject.environments[environment.name]);
				});
			});
		});
		
		featureList.sort(function(a, b) {
			return a.featureName.toLowerCase().localeCompare(b.featureName.toLowerCase());
		});
		
		return featureList;
	});
	
	self.hasBrandsToCompare = ko.computed(function() {
		return self.compareBrandList().length > 1;
	});
	
	self.hasMissingFeatureToggles = ko.computed(function() {
		return !self.hasResolvedMissingFeatures() && self.brandList().some(function(brandObject) {
			return brandObject.missingFeatureList().length > 0;
		});
	});
	
	self.featureToggleValueOptions = [{
		text: "?",
		value: "?"
	}, {
		text: "OFF",
		value: false
	}, {
		text: "ON",
		value: true
	}];
	
	self.exportJSON = ko.computed(function() {
		if (!self.selectedEnvironment()) return '';
		
		var openingBraceOnOwnLine = false;
		
		return "{\n    \"brandList\": [" + self.brandList().filter(function(brandObject) {
			return brandObject.featureList.some(function(featureObject) {
				return featureObject.environments[self.selectedEnvironment().name].exists || featureObject.correctedValue() !== "?";
			});
		}).map(function(brandObject) {
			return (openingBraceOnOwnLine ? "\n    " : "") + "{\n        \"brandName\": \"" + brandObject.brandName + "\",\n        \"featureMap\": [" + brandObject.featureList.filter(function(featureObject) {
				return featureObject.environments[self.selectedEnvironment().name].exists || featureObject.correctedValue() !== "?";
			}).map(function(featureObject) {
				var value = featureObject.environments[self.selectedEnvironment().name].exists ? featureObject.environments[self.selectedEnvironment().name].enabled : featureObject.correctedValue();
				
				return (openingBraceOnOwnLine ? "\n        " : "") + "{\n            \"featureName\": \"" + featureObject.featureName + "\",\n            \"enabled\": \"" + value + "\"\n        }"
			}).join("," + (openingBraceOnOwnLine ? "" : " ")) + "]\n    }";
		}).join("," + (openingBraceOnOwnLine ? "" : " ")) + "]\n}";
	});
	
	self.AnalyzeFeatureToggles = function() {
		self.selectedBrand('');
		
		self.showAnalysis(true);
	};
	
	self.GoBack = function() {
		if (self.showAnalysis()) {
			if (self.isComparing()) {
				self.isComparing(false);
			} else if (self.isExporting()) {
				if (self.hasResolvedMissingFeatures()) {
					self.hasResolvedMissingFeatures(false);
				} else if (self.selectedEnvironment()) {
					self.selectedEnvironment('');
				} else {
					self.isExporting(false);
				}
			} else {
				self.showAnalysis(false);
			}
		}
	};
	
	self.ShowCompareBrandsForm = function() {
		self.brandList().forEach(function(brandObject) {
			brandObject.isComparing(false);
		});
		
		self.isComparing(true);
	};
	
	self.ShowExportForm = function() {
		self.selectedEnvironment('');
		
		self.isExporting(true);
	};
	
	self.selectedEnvironment.subscribe(function(environment) {
		self.hasResolvedMissingFeatures(false);
		
		self.brandList().forEach(function(brandObject) {
			brandObject.featureList.forEach(function(featureObject) {
				var value = environment && featureObject.environments[environment.name].exists ? featureObject.environments[environment.name].enabled : "?";
				
				featureObject.correctedValue(value);
			});
		});
	});
	
	self.ResolveMissingFeatures = function() {
		self.hasResolvedMissingFeatures(true);
	};
}

var vm = new ViewModel();

ko.applyBindings(vm);