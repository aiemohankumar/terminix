/*
 * Module: Scheduled Services
 ***********************************************************************************************************/
TAB.Modules.Add('scheduled-services', function($module, options, onLoaded)
{
	// testing flags
	var TESTING_FLAG_CALENDAR = 1;
	var TESTING_FLAGS = 0; // Set to 0 to disable any testing
	
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,
		
		api: {
			setLiveChatEnabled: function(self, isLiveChatEnabled) {
				self.isLiveChatEnabled = isLiveChatEnabled;
			}
		},
		
		LoadDataDWR: 'my-account-json/getScheduledServicesData.json',
		
		callback: function(self)
		{
			self.isLiveChatEnabled = false;
			
			var DateKey = function(date)
			{
				return date ? (date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate()) : '';
			};
			
			var UpdateModel = self.UpdateModel;
			
			function PrepareService(item) {
				// Tentative appointment message if it is too far in the future to confirm
				item.showFutureAppointmentMessage = false;
				item.futureAppointmentMessage = '';
				
				if (!item.isConfirmedAppointment) {
					var datePieces = item.date.split('/').map(function(p) { return parseInt(p); });
					
					// Remove seconds to get beginning of the day
					var scheduledDate = new Date(datePieces[2], datePieces[0]-1, datePieces[1]);
					scheduledDate.setTime(scheduledDate.getTime() - (scheduledDate.getTime() % 86400000));
					
					// Remove seconds to get beginning of the day
					var today = new Date(Date.now() - (Date.now() % 86400000));
					
					var daysAhead = (scheduledDate - today) / 86400000;
					
					if (daysAhead > 45) {
						item.showFutureAppointmentMessage = true;
						item.futureAppointmentMessage = "Your next " + item.description + " service is due in " + GetMonthName(datePieces[0]) + ".";
					}
				}
				
				// This is what is used to know the selected time since time is now an object with a time and employee number
				item.rescheduleSelectedTimeObject = null;
				
				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR) {
					item.rescheduleLoaded = true;
				}
				
				// Split the name by comma and remove trailing/leading whitespace
				var namePieces = item.technician.split(',').map(function(piece) { return piece.trim(); });
				
				// Rearrange name in reverse order
				item.technician = namePieces.reverse().join(' ');
				
				item.submittingNote = false;
				item.submittingTentative = false;
				
				item.loadRescheduleAttempts = 0;
				
				item.months = [];
				
				item._original = $.extend({}, item);
			}
			
			self.UpdateModel = function(json)
			{
				// Add properties that aren't on the back-end
				json.services.forEach(PrepareService);
				
				UpdateModel.call(self, json);
				
				// Mobile support for switching steps and scrolling user to top of new step
				self.model().services().forEach(function(item, index) {
					item.rescheduleStep.subscribe(function(step) {
						if (Viewport.isMobile()) {
							var $row = $module.find('table:visible tr.summary').eq(index);
							
							// If on a step, then scroll to the next row which is the "detail" view
							if (step) {
								$row = $row.next();
							}
							
							var pos = $row.offset() && $row.offset().top || 0;
							
							ScrollTo(pos, 'fast');
						}
					});
				});

				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR) {
					self.model().services().forEach(function(item) {
						self.CreateRescheduleMonths(item);
					});
				}
			};
			
			self.GetModelJSON = function(item)
			{
				var json = ko.mapping.toJS(item);
				
				// Format the rescheduleSelectedDate for the server
				if (json.rescheduleSelectedDate && json.rescheduleSelectedDate.date)
				{
					var date = new Date(json.rescheduleSelectedDate.date);
					
					json.rescheduleSelectedDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
				}
				
				// Reset time variables
				json.rescheduleSelectedTime = null;
				json.rescheduleSelectedEmployeeNumber = null;
				
				// Check if a time object is chosen to populate time variables
				var rescheduleSelectedTimeObject = ko.mapping.toJS(item.rescheduleSelectedTimeObject);
				
				if (rescheduleSelectedTimeObject)
				{
					json.rescheduleSelectedTime = rescheduleSelectedTimeObject.time;
					json.rescheduleSelectedEmployeeNumber = rescheduleSelectedTimeObject.employeeNumber;
				}
				
				return json;
			};
			
			self.hasTentativeAppointments = ko.computed(function() {
				return self.model() && self.model().services() && self.model().services().length && self.model().services().some(function(item) {
					return !item.isConfirmedAppointment();
				});
			});
			
			self.hasAppointmentsToConfirm = ko.computed(function() {
				return self.model() && self.model().services() && self.model().services().length && self.model().services().some(function(item) {
					return !item.isConfirmedAppointment() && item.canConfirmAppointment();
				});
			});
			
			self.CreateRescheduleMonths = function(item)
			{
				if (ko.isObservable(item.months))
				{
					item.months([]);
				}
				else
				{
					item.months = ko.observableArray();
				}

				if (TESTING_FLAGS & TESTING_FLAG_CALENDAR && (!item.rescheduleDates() || !item.rescheduleDates().length)) {
					item.rescheduleDates((function() {
						var dates = [];
						var date = new Date(), currentTime = date.getTime(), currentMonth = date.getMonth(), daysIntoMonth = 15;
						var availableTimeSlots = [ 8, 10, 12, 14, 16, 18 ];
	
						// Enough days to span more than 1 month
						do {
							date.setTime(date.getTime() + 86400000);
	
							if (date.getDay() == 0 || date.getDay() == 6)
								continue;
	
							var times = availableTimeSlots.slice();
	
							for (var t = Math.floor(Math.random() * availableTimeSlots.length); t > 0; t--) {
								times.splice(Math.floor(Math.random() * times.length), 1);
							}
	
							dates.push(ko.mapping.fromJS({
								times : times.map(function(time) {
									return {
										time : time,
										employeeNumber : '123456'
									};
								}),
								date : (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
							}));
						} while (date.getMonth() == currentMonth || date.getTime() - (86400000 * 14) < currentTime || date.getDate() < daysIntoMonth);
						
						item.rescheduleTimes(availableTimeSlots.filter(function(timeSlot) {
							return dates.some(function(dateObject) {
								return dateObject.times().some(function(dateTime) {
									return dateTime.time() == timeSlot;
								});
							});
						}));
	
						return dates;
	
					})());
				}
				
				if (!item.rescheduleDates() || !item.rescheduleDates().length)
					return;

				var dates = [],
					months = {},
					monthsArray = [],
					availableDates = {},
					firstDate = null,
					lastDate = null;
				
				var DayItem = function(data)
				{
					var item = $.extend({
						text: '',
						date: null,
						available: false,
						times: []
					}, data);
					
					var key = DateKey(item.date);
					
					if (key in availableDates)
					{
						item.available = true;
						item.times = availableDates[key].times;
					}
					
					return item;
				};
				
				item.rescheduleDates().forEach(function(d)
				{
					var datePieces = d.date().split('/');
					var date = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
					
					if (firstDate == null || firstDate > date)
						firstDate = new Date(date);
					
					if (lastDate == null || lastDate < date)
						lastDate = new Date(date);
					
					dates.push(date);
					
					availableDates[DateKey(date)] = ko.mapping.toJS(d);
				});
				
				dates.forEach(function(date)
				{
					if (!(date.getUTCMonth() in months))
					{
						var month = {
							date: new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), 1)),
							monthNumber: date.getUTCMonth(),
							monthName: GetMonthName(date.getUTCMonth() + 1),
							weeks: []
						};
						
						// Build first week
						var d = new Date(month.date);
						var week = { days: [] };
						
						for (var day = 0; day < d.getUTCDay(); day++)
						{
							week.days.push(DayItem());
						}
						
						var thisMonth = d.getUTCMonth();
						
						do
						{
							// Loop through the end of the week
							do
							{
								if (d.getUTCMonth() == thisMonth)
								{
									week.days.push(DayItem({
										text: d.getUTCDate(),
										date: new Date(+d)
									}));
								}
								else
								{
									week.days.push(DayItem());
								}
								
								d.setTime(d.getTime() + 86400000);
							}
							while (d.getUTCDay() > 0);
							
							// Check if this week has any available days
							var hasAvailable = week.days.some(function(day) { return day.available; }),
								firstWeekDay = week.days.find(function(day) { return !!day.date });
							
							if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate)
							{
								// Add week for month
								month.weeks.push(week);
							}
							
							// Reset week
							week = { days: [] };
						}
						while (d.getUTCMonth() == thisMonth);
						
						// Store month
						months[thisMonth] = month;
						monthsArray.push(ko.mapping.fromJS(month));
					}
				});
				
				monthsArray.sort(function(a, b)
				{
					return a.date() < b.date() ? -1 : 1;
				});
				
				item.months(monthsArray);
			};
			
			self.FormatTimeRange = function(time)
			{
				// Remove the :00 from a time and only show the hour
				return ('' + ko.unwrap(time)).replace(/\:00/g, '');
			};
			
			self.ShowDetail = function(item)
			{
				item.showDetail(!item.showDetail() || item.rescheduleStep() != '');
			};
			
			self.Reschedule = function(item, e)
			{
				var onRescheduleLoaded = function()
				{
					if (item.months().length)
					{
						item.rescheduleStep('appointment');
					}
					else
					{
						item.rescheduleStep('contact');
					}
				};
				
				if (!item.rescheduleLoaded())
				{
					self.RemoveAllErrors(item);
					
					var json = self.GetModelJSON(item);
					
					item.submitting(true);
					item.loadRescheduleAttempts++;
					
					CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', json, function(data)
					{
						item.submitting(false);
						
						if (data.error)
						{
							if (item.loadRescheduleAttempts < 2)
							{
								item.errorMessages.push({ errorMessage: data.error });
								item.rescheduleStep('error');
							}
							else
							{
								item.rescheduleStep('contact');
							}
							return;
						}
						
						// Update item model
						self.ApplyModelChanges(item, data.model);
						
						// Create the calendars needed with the reschedule dates
						self.CreateRescheduleMonths(item);
						
						// Set it as loaded
						item.rescheduleLoaded(true);
						onRescheduleLoaded();
					});
				}
				else
				{
					onRescheduleLoaded();
				}
			};
			
			self.SetScheduleDay = function(item, day)
			{
				if (day.date() && day.available())
				{
					item.rescheduleSelectedDate(day);
					item.rescheduleSelectedTimeObject(null);
				}
			};
			
			self.GetScheduleDayCSS = function(item, day)
			{
				return {
					disabled: !day.date() || !day.available(),
					active: day.date() && item.rescheduleSelectedDate() && DateKey(item.rescheduleSelectedDate().date()) == DateKey(day.date())
				};
			};
			
			self.IsScheduleTimeAvailable = function(item, time)
			{
				if (!item.rescheduleSelectedDate())
					return false;
				
				var matchingTime = ko.utils.arrayFirst(item.rescheduleSelectedDate().times(), function(t)
				{
					return t.time() == time;
				});
				
				return !!matchingTime;
			};
			
			self.GetRescheduleTimes = function(item)
			{
				var timeObjects = [];
				
				if (item.rescheduleSelectedDate())
				{
					item.rescheduleTimes().forEach(function(time)
					{
						var timeObject = ko.utils.arrayFirst(item.rescheduleSelectedDate().times(), function(t)
						{
							return t.time() == time;
						});
						
						timeObjects.push(timeObject || ko.mapping.fromJS({
							time: time,
							employeeNumber: null
						}));
					});
				}
				else
				{
					item.rescheduleTimes().forEach(function(time)
					{
						timeObjects.push(ko.mapping.fromJS({
							time: time,
							employeeNumber: null
						}));
					});
				}
				
				return timeObjects;
			};
			
			self.GetScheduleTimeDisplay = (function()
			{
				var FormatHour = function(hour)
				{
					return (hour > 12 ? hour - 12 : hour) + (hour < 12 ? 'AM' : 'PM');
				};
				
				return function(time)
				{
					if (!time) return '';
					
					if (time.hasOwnProperty('time'))
						time = time.time;
					
					if (ko.isObservable(time))
						time = time();
					
					return FormatHour(time) + ' - ' + FormatHour(parseInt(time) + 2);
				};
			})();
			
			self.SetNoRescheduleDay = function(item)
			{
				item.rescheduleSelectedDate(null);
				item.rescheduleSelectedTimeObject(null);
				item.rescheduleStep('contact');
				
				if (self.isLiveChatEnabled && GetFeatureToggleDefaultFalse("proactiveChatScheduleCFR")) {
					LiveChat_OpenChat();
				}
			};
			
			self.GetScheduleDateDisplay = function(item, verbose, overrideDateString)
			{
				var dateObject, date;
				
				if (overrideDateString)
				{
					date = ('' + overrideDateString).split('/');
					
					if (date.length == 3)
					{
						dateObject = new Date(Date.UTC(parseInt(date[2]), parseInt(date[0]) - 1, parseInt(date[1])));
					}
				}
				else if (item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject())
				{
					dateObject = item.rescheduleSelectedDate().date();
				}
				
				if (dateObject)
				{
					if (verbose)
					{
						return GetMonthName(dateObject.getUTCMonth() + 1) + ' ' + dateObject.getUTCDate();
					}
					
					return (dateObject.getUTCMonth() + 1) + '/' + dateObject.getUTCDate() + '/' + dateObject.getUTCFullYear();
				}
				
				return '';
			};
			
			self.CancelRescheduleStep = function(item)
			{
				self.RemoveAllErrors(item);
				
				switch (item.rescheduleStep())
				{
				case 'error':
					item.rescheduleStep('');
					item.showDetail(false);
					break;
				
				case 'appointment':
					item.rescheduleStep('');
					item.rescheduleSelectedDate(null);
					item.rescheduleSelectedTimeObject(null);
					item.showDetail(false);
					break;
				
				case 'contact':
					if (item.months().length)
					{
						item.rescheduleStep('appointment');
					}
					else
					{
						item.rescheduleStep('');
						item.showDetail(false);
					}
					
					item.rescheduleContactFname(item._original.rescheduleContactFname());
					item.rescheduleContactLname(item._original.rescheduleContactLname());
					item.rescheduleContactPhone(item._original.rescheduleContactPhone());
					item.rescheduleContactDate('');
					item.rescheduleContactTime('');
					break;
				}
			};
			
			var currentDate = new Date(), otherDate;
			
			self.DatepickerOptions = {
				minDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 1) && otherDate,
				maxDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 45) && otherDate,
				beforeShowDay: function(date) {
					// Has to return an array of [isSelectable, CSS class name string for date cell, optional popup tooltip]
					// For date.getDay(), 0 = Sunday
					return [date.getDay() > 0, ''];
				}
			};
			
			self.IsValidContact = function(item)
			{
				return item.rescheduleContactFname() && item.rescheduleContactLname() && item.rescheduleContactPhone() && item.rescheduleContactDate() && item.rescheduleContactTime();
			};
			
			self.ConfirmContact = function(item, e)
			{
				if (item.submitting() || !self.IsValidContact(item)) return;
				
				self.RemoveAllErrors(item);
				
				var json = self.GetModelJSON(item);
				
				item.submitting(true);
				
				CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', json, function(data)
				{
					item.submitting(false);
					
					if (data.error)
					{
						item.errorMessages.push({ errorMessage: data.error });
						return;
					}
					
					// Update model
					self.ApplyModelChanges(item, data.model);
					
					// Go to next step if no errors
					if (!item.errorMessages().length && !item.fieldValidationErrors().length)
					{
						item.rescheduleStep('confirmationContact');
					}
				});
			};
			
			self.IsValidReschedule = function(item)
			{
				return item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject();
			};
			
			self.ConfirmAppointment = function(item, e)
			{
				if (item.submitting() || !self.IsValidReschedule(item)) return;
				
				self.RemoveAllErrors(item);
				
				var json = self.GetModelJSON(item);
				
				item.submitting(true);
				
				CallDWR('MyAccountTMXServicesUIUtils/rescheduleService', json, function(data)
				{
					item.submitting(false);
					
					if (data.error)
					{
						item.errorMessages.push({ errorMessage: data.error });
						return;
					}
					
					// Ignore rescheduleSelectedDate being set
					if (data.model && 'rescheduleSelectedDate' in data.model)
					{
						delete data.model['rescheduleSelectedDate'];
					}
					
					// Update model
					self.ApplyModelChanges(item, data.model);
					
					// Go to next step if no errors
					if (!item.errorMessages().length && !item.fieldValidationErrors().length)
					{
						item.rescheduleStep('confirmationAppointment');
						
						// Delete the updated services from the item, for cleanup purposes
						if (ko.isObservable(item.updatedScheduledServices)) {
							item.updatedScheduledServices([]);
						}
						
						// Update my services with item.updatedScheduledServices
						if (data.model.updatedScheduledServices && data.model.updatedScheduledServices.length) {
							data.model.updatedScheduledServices.forEach(function(service) {
								// Find the item to update in the model
								var updateItem = self.model().services().find(function(updateItem) {
									return updateItem.workOrderActivityId() == service.workOrderActivityId;
								});
								
								if (updateItem) {
									// Remove information from the service that we don't want to update the item with
									var fieldsToNotUpdate = ['rescheduleSelectedDate', 'rescheduleStep'];
									
									fieldsToNotUpdate.forEach(function(field) {
										if (service.hasOwnProperty(field)) {
											delete service[field];
										}
									});
									
									// Update the item with the new service information
									self.ApplyModelChanges(updateItem, service);
								} else {
									// Add to Scheduled Services
									PrepareService(service);
									
									self.model().services.push(ko.mapping.fromJS(service));
								}
							});
						}
						
						TAB.Modules.Reload("my-services");
					}
				});
			};
			
			self.CloseConfirmation = function(item)
			{
				item.rescheduleStep('');
				item.showDetail(false);
			};
			
			self.SubmitNote = function(item, e)
			{
				if (!item.noteForTechnician() || item.submittingNote()) return;
				
				self.RemoveAllErrors(item);
				
				item.submittingNote(true);
				
				var json = ko.mapping.toJS(item);
				
				CallDWR('MyAccountTMXServicesUIUtils/updateNoteToTechnician', json, function(data)
				{
					item.submittingNote(false);
					
					if (data.error)
					{
						item.errorMessages.push({ errorMessage: data.error });
						return;
					}
					
					self.ApplyModelChanges(item, data.model);
					
					if (item.redirectURL())
					{
						setTimeout(function()
						{
							Redirect(item.redirectURL());
							
						}, 1500);
						return;
					}
				});
			};
			
			self.ConfirmTentativeAppointment = function(item, e)
			{
				if (!item.canConfirmAppointment() || item.submittingTentative()) return;
				
				self.RemoveAllErrors(item);
				
				item.submittingTentative(true);
				
				var json = ko.mapping.toJS(item);
				
				// Simulating confirmation
				CallDWR('MyAccountTMXServicesUIUtils/confirmScheduledService', json, function(data)
				{
					item.submittingTentative(false);
					
					if (data.error)
					{
						CreateAlert({
							color: 'red',
							message: "There was an error confirming your scheduled service. Please try again.",
							button: false,
							closeButton: true,
							scrollToAlerts: true,
							$module: $module
						});
						return;
					}
					
					if (data.model.errorMessages && data.model.errorMessages.length)
					{
						data.model.errorMessages.forEach(function(errorObj, index)
						{
							CreateAlert({
								color: 'red',
								message: errorObj.errorMessage,
								button: false,
								closeButton: true,
								scrollToAlerts: index == 0,
								$module: $module
							});
						});
					}
					else
					{
						// Update model
						self.ApplyModelChanges(item, data.model);
						
						// Show confirmation message
						item.rescheduleStep('confirmationTentativeAppointment');
					}
				});
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});