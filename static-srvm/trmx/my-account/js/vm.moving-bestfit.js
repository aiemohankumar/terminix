function DateKey(date) {
	var MM = ('00' + (date.getUTCMonth() + 1)).substr(-2);
	var dd = ('00' + date.getUTCDate()).substr(-2);
	var yyyy = date.getUTCFullYear();
	
	return MM + '/' + dd + '/' + yyyy;
}

function FormatHour(hour) {
	return (((hour %= 24) + 11) % 12 + 1) + (hour < 12 ? "AM" : "PM"); 
}

function FormatTimeRange(time) {
	// Remove the :00 from a time and only show the hour
	return ('' + ko.unwrap(time)).replace(/\:00/g, '');
}

function ShuffleArray(arr) {
	arr = arr.reduce(function(out, item) {
		out.splice(Math.floor(Math.random() * out.length), 0, item);
		return out;
	}, []);
	
	var offset = Math.floor(Math.random() * arr.length);
	var front = arr.splice(0, offset);
	
	return arr.concat(front);
}

function GenerateDummyScheduleTimes() {
	return ShuffleArray([8,9,10,11,12,13,14,15,16,17]).reduce(function(out, time, index, arr) {
		if (Math.random() < 0.5 - 0.5 / (arr.length - out.length) || out.length == 0 && index + 1 == arr.length) {
			out.push({
				time: time,
				employeeNumber: '' + Math.floor(Math.random() * 13459267)
			});
		}
		return out;
	}, []);
}

function GenerateDummyScheduleDates() {
	var dates = [];
	
	var dateObject = new Date();
	dateObject.setMonth(dateObject.getMonth() + 1);
	
	var lastDateObject = new Date(dateObject);
	lastDateObject.setDate(lastDateObject.getDate() + 30);
	
	while (dateObject < lastDateObject) {
		if (dateObject.getDay() > 0) {
			dates.push({
				date: (dateObject.getMonth() + 1) + '/' + dateObject.getDate() + '/' + dateObject.getFullYear(),
				times: GenerateDummyScheduleTimes()
			});
		}
		
		dateObject.setDate(dateObject.getDate() + 1);
	}
	
	return dates;
}

function ParseScheduleDates(jsonScheduleDates) {
	var scheduleTimes = [];
	var scheduleTimesMap = {};
	var scheduleDates = [];
	var scheduleDatesMap = {};
	var firstDate, lastDate;
	
	// First create a date object for each schedule date
	// And find out our earliest and latest dates
	jsonScheduleDates.forEach(function(scheduleDate) {
		var datePieces = scheduleDate.date.split('/');
		scheduleDate.dateObject = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
		
		if (!firstDate || firstDate > scheduleDate.dateObject)
			firstDate = new Date(scheduleDate.dateObject);
		
		if (!lastDate || lastDate < scheduleDate.dateObject)
			lastDate = new Date(scheduleDate.dateObject);
		
		scheduleDate.times.forEach(function(scheduleTime) {
			if (!(scheduleTime.time in scheduleTimesMap)) {
				var fromTime = FormatHour(scheduleTime.time);
				var toTime = FormatHour(scheduleTime.time + 2);
				
				scheduleTimesMap[scheduleTime.time] = true;
				scheduleTimes.push({
					time: scheduleTime.time,
					text: fromTime + ' - ' + toTime,
					available: false,
					scheduleTime: null
				});
			}
		});
		
		scheduleDates.push(scheduleDate);
		scheduleDatesMap[scheduleDate.date] = scheduleDate;
	});
	
	scheduleTimes.sort(function(a, b) {
		return a.time < b.time ? -1 : 1;
	});
	
	// Update all schedule dates to have all schedule times available
	// But only use the ones that are for the date
	scheduleDates.forEach(function(scheduleDate) {
		scheduleDate.scheduleTimes = scheduleTimes.map(function(scheduleTime) {
			var scheduleDateTime = scheduleDate.times.find(function(scheduleDateTime) {
				return scheduleDateTime.time == scheduleTime.time;
			}) || null;
			
			return $.extend({}, scheduleTime, {
				available: !!scheduleDateTime,
				scheduleTime: scheduleDateTime
			});
		});
	});
	
	return {
		scheduleTimes: scheduleTimes,
		scheduleTimesMap: scheduleTimesMap,
		scheduleDates: scheduleDates,
		scheduleDatesMap: scheduleDatesMap,
		firstDate: firstDate,
		lastDate: lastDate
	};
}

function GenerateMonthsFromScheduleDates(data) {
	var scheduleMonths = [];
	var scheduleMonthsMap = {};
	var scheduleTimes = data.scheduleTimes;
	var scheduleTimesMap = data.scheduleTimesMap;
	var scheduleDates = data.scheduleDates;
	var scheduleDatesMap = data.scheduleDatesMap;
	var firstDate = data.firstDate;
	var lastDate = data.lastDate;
	
	// Build out a shell of months that are included
	var iterDate = new Date(Date.UTC(firstDate.getUTCFullYear(), firstDate.getUTCMonth(), 1));
	
	function DayItem(data) {
		var item = $.extend({
			text: '',
			date: null,
			available: false
		}, data);
		
		if (item.date) {
			var key = DateKey(item.date);
			
			if (key in scheduleDatesMap) {
				item.available = true;
				item.scheduleDate = scheduleDatesMap[key];
			}
		}
		
		return item;
	}
	
	while (iterDate <= lastDate) {
		var scheduleMonthKey = iterDate.getUTCMonth() + "/" + iterDate.getUTCFullYear();
		
		var scheduleMonth = {
			key: scheduleMonthKey,
			date: new Date(Date.UTC(iterDate.getUTCFullYear(), iterDate.getUTCMonth(), 1)),
			monthNumber: iterDate.getUTCMonth(),
			monthName: GetMonthName(iterDate.getUTCMonth() + 1),
			weeks: []
		};
		
		var monthIterDate = new Date(iterDate);
		
		// Build out the first week
		var week = { days: [] };
		
		for (var day = 0; day < monthIterDate.getUTCDay(); day++) {
			week.days.push(DayItem());
		}
		
		do {
			// Loop through the end of the week
			do {
				if (monthIterDate.getUTCMonth() == iterDate.getUTCMonth()) {
					week.days.push(DayItem({
						text: monthIterDate.getUTCDate(),
						date: new Date(monthIterDate)
					}));
				} else {
					week.days.push(DayItem());
				}
				
				monthIterDate.setUTCDate(monthIterDate.getUTCDate() + 1);
			}
			while (monthIterDate.getUTCDay() > 0);
			
			// Check if this week has any available days
			var hasAvailable = week.days.some(function(day) { return day.available; }),
				firstWeekDay = week.days.find(function(day) { return !!day.date });
			
			if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate) {
				scheduleMonth.weeks.push(week);
			}
			
			// Reset week
			week = { days: [] };
		}
		while (monthIterDate.getUTCMonth() == iterDate.getUTCMonth());

		// Only add it if it has weeks to show
		if (scheduleMonth.weeks.length > 0) {
			scheduleMonths.push(scheduleMonth);
			scheduleMonthsMap[scheduleMonthKey] = scheduleMonth;
		}
		
		iterDate.setUTCMonth(iterDate.getUTCMonth() + 1);
	}
	
	return {
		scheduleMonths: scheduleMonths,
		scheduleTimes: scheduleTimes
	};
}

function SchedulerModel() {
	var self = this;
	
	self.months = ko.observableArray();
	self.times = ko.observableArray();
	
	self.selectedDate = ko.observable();
	self.selectedTime = ko.observable();
	self.noDateSelected = ko.observable(false);
	self.preferredServiceDate = ko.observable();
	self.preferredServiceTime = ko.observable();
	
	self.noDateSelected.subscribe(function(noDateSelected) {
		if (noDateSelected) {
			self.selectedDate(null);
			self.selectedTime(null);
		}
	});
	
	self.GetDateCSS = function(day) {
		return {
			disabled: !day.date || !day.available,
			active: self.selectedDate() && self.selectedDate() == day.scheduleDate
		}
	};
	
	self.GetTimes = function() {
		return self.selectedDate() ? self.selectedDate().scheduleTimes : self.times();
	};
	
	self.SetScheduleDate = function(day) {
		if (day.date && day.available) {
			self.selectedTime(null);
			self.selectedDate(day.scheduleDate);
			self.noDateSelected(false);
		}
	};

	self.CreateScheduleMonths = function(jsonScheduleDates) {
		var data;
		
		if (jsonScheduleDates && jsonScheduleDates.length) {
			data = ParseScheduleDates(jsonScheduleDates);
			data = GenerateMonthsFromScheduleDates(data);
		}
		
		self.months(data && data.scheduleMonths || []);
		self.times(data && data.scheduleTimes || []);

		self.selectedTime(null);
		self.selectedDate(null);
		self.noDateSelected(!self.months().length);
		self.preferredServiceDate(null);
		self.preferredServiceTime(null);
	};
	
	self.GetAppointmentDate = function() {
		var dateObject;
		
		if (self.noDateSelected()) {
			if (!self.preferredServiceDate()) {
				return "";
			}
			
			dateObject = new Date(self.preferredServiceDate());
		} else {
			if (!self.selectedDate()) {
				return "";
			}
			
			dateObject = new Date(self.selectedDate().dateObject);
		}
		
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		return days[dateObject.getDay()] + ", " + months[dateObject.getMonth()] + " " + dateObject.getDate();
	};
	
	self.GetAppointmentTime = function() {
		if (self.noDateSelected()) {
			if (!self.preferredServiceTime()) {
				return "";
			} else {
				return self.preferredServiceTime();
			} 
		} else {
			if (!self.selectedTime()) {
				return "";
			} else {
				var fromTime = FormatHour(self.selectedTime().time);
				var toTime = FormatHour(self.selectedTime().time + 2);
				
				return fromTime + ' - ' + toTime;
			}
		}
	};
}

function ViewModel() {
	var self = this;
	
	ViewModel_AddCommonFunctions(self);
	ViewModel_AddStepFunctions(self, {
		stepOrder: ['address', 'my-services', 'schedule-pest', 'schedule-termite']
	});
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	self.error = ko.observable();
	
	self.disableInputs = ko.computed(function() {
		return self.submitting();
	});
	
	self.schedulerPest = new SchedulerModel();
	self.schedulerTerm = new SchedulerModel();
	
	self.visitedStepMyServices = ko.observable(false);
	self.visitedStepSchedulePest = ko.observable(false);
	
	self.LoadData = function() {
		var lookupZipcode = $.QueryString.zipcode;
		
		if (caniuse.sessionStorage && !lookupZipcode) {
			lookupZipcode = sessionStorage.getItem("MovingModelLookupZipcode");
		}
		
		PromiseDWR('my-account-json/getMovingModel.json').then(function(json) {
			if (json.redirectURL) {
				Redirect(json.redirectURL);
				return;
			}
			
			// Check if user doesn't have access to move
			if (!json.canMovePropertyByOnline) {
				Redirect("/my-account/pages/home.jsp#my-account/moving");
				return;
			}
			
			json.lookupZipcode = lookupZipcode;
			json.zipcode = lookupZipcode;

			return json;
		}).then(function(json) {
			if (json) {
				return PromiseDWR({ url: 'my-account-json/isZipServiceable.json', data: json }).then(function(json) {
					if (!json.isServiceable) {
						Redirect("/my-account/pages/home.jsp#my-account/moving");
						return;
					}
					
					return json;
				});
			}
		}).then(function(json) {
			if (json) {
				// Try to get the user's city and state from zipcode
				return GetCityAndStateFromZip(json.lookupZipcode).then(function(data) {
					json.city = data.city;
					json.state = data.state;
				}).catch(function(error) {
					// Ignore errors and don't prepopulate
				}).then(function() {
					return json;
				});
			}
		}).catch(function(error) {
			self.error("There was an error loading the page. Please refresh and try again.");
		}).then(function(json) {
			if (json) {
				self.model(ko.mapping.fromJS(json));
				self.modelLoaded(true);
				
				self.UpdateAutoFocus();
			}
		});
	};
	
	self.GetMoveDatePickerOptions = function() {
		var options = {};
		
		if (self.modelLoaded()) {
			var minDate = new Date();
			minDate.setDate(minDate.getDate() + 1);
			
			options.minDate = minDate;
			/*TMXR-4048 - If the move in date was more than 60 days, 
			 * then the preferred service date picker was ghosted for all dates, 
			 * thats because preferred service date is ghosted after 60 days,
			 * hence restricting the move in date to 60 days window*/
			var lastDateInRange = new Date();
			lastDateInRange.setDate(lastDateInRange.getDate() + 59); // 60 days of range
			options.maxDate = lastDateInRange;
		}
		
		return options;
	};
	
	self.GetPreferredServiceDatePickerOptions = function() {
		var options = {};
		
		if (self.modelLoaded()) {
			var moveDate = self.model().moveDate();
			
			if (/^\d{2}\/\d{2}\/\d{4}$/.test(moveDate)) {
				var datePieces = moveDate.split('/');
				moveDate = new Date(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]);
				
				var currentDate = new Date();
				
				if (moveDate < currentDate) {
					moveDate = currentDate;
				}
				
				var lastDateInRange = new Date();
				lastDateInRange.setDate(lastDateInRange.getDate() + 59); // 60 days of range
				
				options.minDate = moveDate;
				options.maxDate = lastDateInRange;
				options.beforeShowDay = function(date) {
					// Has to return an array of [isSelectable, CSS class name string
					// for date cell, optional popup tooltip]
					// For date.getDay(), 0 = Sunday
					return [date.getDay() > 0, ''];
				};
			}
		}
		
		return options;
	};
	
	self.ShowDetail = function(service) {
		service.showDetail(!service.showDetail());
	};
	
	self.KeepExistingServices = function() {
		self.model().rescheduleUpcomingServices(false);
		self.SubmitStep();
	};
	
	self.RescheduleServices = function() {
		self.model().rescheduleUpcomingServices(true);
		self.SubmitStep();
	};
	
	self.CancelStep = function() {
		if (self.submitting()) return;
		
		switch (self.model().currentStep()) {
			case 'address': {
				break;
			}
			case 'my-services': {
				self.model().currentStep('address');
				break;
			}
			case 'schedule-pest': {
				if (self.schedulerPest.noDateSelected() && self.schedulerPest.months().length) {
					self.schedulerPest.noDateSelected(false);
				} else if (self.model().upcomingServices() && self.model().upcomingServices().length) {
					self.model().currentStep('my-services');
				} else {
					self.model().currentStep('address');
				}
				break;
			}
			case 'schedule-termite': {
				if (self.schedulerTerm.noDateSelected() && self.schedulerTerm.months().length) {
					self.schedulerTerm.noDateSelected(false);
				} else if (self.schedulerPest.months().length) {
					self.model().currentStep('schedule-pest');
				} else if (self.model().upcomingServices() && self.model().upcomingServices().length) {
					self.model().currentStep('my-services');
				} else {
					self.model().currentStep('address');
				}
				break;
			}
		}
	};
	
	self.IsValidStep = function() {
		switch (self.model().currentStep()) {
			case 'address': {
				break;
			}
			case 'my-services': {
				break;
			}
			case 'schedule-pest': {
				if (self.schedulerPest.noDateSelected()) {
					return self.schedulerPest.preferredServiceDate() && self.schedulerPest.preferredServiceTime();
				} else {
					return self.schedulerPest.selectedDate() && self.schedulerPest.selectedTime();
				}
			}
			case 'schedule-termite': {
				if (self.model().optOutFreeTermiteInspection()) {
					return true;
				}
				
				if (self.schedulerTerm.noDateSelected()) {
					return self.schedulerTerm.preferredServiceDate() && self.schedulerTerm.preferredServiceTime();
				} else {
					return self.schedulerTerm.selectedDate() && self.schedulerTerm.selectedTime();
				}
			}
		}
		
		return true;
	};
	
	self.HasStep = function(step) {
		switch (step) {
			case 'address': {
				break;
			}
			case 'my-services': {
				return self.visitedStepMyServices();
			}
			case 'schedule-pest': {
				return self.visitedStepSchedulePest();
			}
			case 'schedule-termite': {
				break;
			}
		}
		
		return true;
	};
	
	function GetSchedulerModelJSON(model, suffix) {
		var json = {};
		
		json['selectedScheduleDate' + suffix] = null;
		json['selectedScheduleTime' + suffix] = null;
		json['preferredServiceDate' + suffix] = null;
		json['preferredServiceTime' + suffix] = null;
		json['noDateSelected' + suffix] = model.noDateSelected();
		
		if (model.noDateSelected()) {
			json['preferredServiceDate' + suffix] = model.preferredServiceDate();
			json['preferredServiceTime' + suffix] = model.preferredServiceTime();
		} else {
			json['selectedScheduleDate' + suffix] = model.selectedDate();
			json['selectedScheduleTime' + suffix] = model.selectedTime();
		}
		
		return json;
	}
	
	self.SubmitStep = function() {
		if (self.disableInputs() || !self.IsValidStep()) return;
		
		self.submitting(true);
		
		self.RemoveAllErrors();
		
		var data = ko.mapping.toJS(self.model);
		
		$.extend(data, GetSchedulerModelJSON(self.schedulerPest, 'Pest'));
		$.extend(data, GetSchedulerModelJSON(self.schedulerTerm, 'Term'));
		
		var lastStep = data.currentStep;
		
		PromiseDWR({ url: 'MyAccountTMXUIUtils/setMovingModel2', data: data }).then(function(json) {
			if (json.redirectURL) {
				Redirect(json.redirectURL);
				return;
			}
			
			switch (json.currentStep) {
				case 'address': {
					self.visitedStepMyServices(false);
					self.visitedStepSchedulePest(false);
					break;
				}
				case 'my-services': {
					self.visitedStepMyServices(true);
					self.visitedStepSchedulePest(false);
					
					json.upcomingServices.forEach(function(service) {
						service.showDetail = false;
					});
					break;
				}
				case 'schedule-pest': {
					self.visitedStepSchedulePest(true);
					
					if (lastStep != json.currentStep) {
						self.schedulerPest.CreateScheduleMonths(json.scheduleDatesPest);
					}
					break;
				}
				case 'schedule-termite': {
					if (lastStep != json.currentStep) {
						//TMXR-3988: If the moving date is more than 30 days past, then send null to function, so that CFR is shown to user
						if((new Date(data.preferredServiceDatePest)-new Date())/(1000*60*60*24) > 30)
							self.schedulerTerm.CreateScheduleMonths(null);
						else
							self.schedulerTerm.CreateScheduleMonths(json.scheduleDatesTerm);
					}
					break;
				}
				case 'confirmation': {
					if (json.googleAnalyticsData) {
						try {
							utag.link(json.googleAnalyticsData);
							
							utag.link({
								ga_event_category: "My Account",
								ga_event_action: "Moving Soon",
								ga_event_label: "Completed moving process"
							});
						} catch (e) {
							console.log(e);
						}
					}
					break;
				}
			}
			
			self.model(ko.mapping.fromJS(json));
			
			self.model().optOutFreeTermiteInspection.subscribe(function(optOutFreeTermiteInspection) {
				if (optOutFreeTermiteInspection) {
					self.schedulerTerm.selectedDate(null);
					self.schedulerTerm.selectedTime(null);
					self.schedulerTerm.preferredServiceDate(null);
					self.schedulerTerm.preferredServiceTime(null);
				}
			});
		}).catch(function(error) {
			console.log(error);
			
			self.model().errorMessages.push({ errorMessage: error });
		}).then(function() {
			self.submitting(false);

			self.UpdateAutoFocus();
		});
	};
}

$(function() {
	window.vm = new ViewModel();
	
	ko.applyBindings(vm);
	
	$('.on-knockout-loaded').show();
	
	vm.LoadData();
});