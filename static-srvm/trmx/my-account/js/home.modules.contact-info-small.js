/*
 * Module: Contact Info Small
 ***********************************************************************************************************/
TAB.Modules.Add('contact-info-small', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,

		// Remove self.LoadData = function(){} from the callback() param when setting LoadDataDWR
		LoadDataDWR: 'MyAccountTMXContactInfoUIUtils/getContactInfoSmall',

		// Remove OnBeforeSubmit to allow SubmitDWR to work. It exists to stop DWR from being called
		SubmitDWR: 'MyAccountTMXContactInfoUIUtils/changeContactInfoSmall',
		
		OnBeforeSubmit: function(self)
		{
			if (self.submitting() || !self.HasModelChanged())
				return false;
			
			self.RemoveAllErrors();
			
			self.QAS.isChoosingEmail(false);
			
			if (!self.QAS.IsLastValidatedEmail(self.model().email()))
			{
				self.QAS.isVerifying(true);
				self.submitting(true);
				
				QAS_VerifyEmail(self.model().email(), function(data)
				{
					self.submitting(false);
					self.QAS.isVerifying(false);
					
					if (data.model && data.model.corrections && data.model.corrections.length)
					{
						self.QAS.emails(data.model.corrections);
						self.QAS.selectedEmail(null);
						self.QAS.isChoosingEmail(true);
					}
					else if (data.model && data.model.errorMessages && data.model.errorMessages.length)
					{
						self.model().errorMessages(data.model.errorMessages);
					}
					else
					{
						self.QAS.lastValidatedEmail = self.model().email();
						
						self.Submit();
					}
				});
				
				return false;
			}
			
			return true;
		},
		
		OnGetSubmit: function(self, json)
		{
			if (json.informationalMessages && json.informationalMessages.length)
			{
				// Successfully updated contact information
				json.informationalMessages.forEach(function(messageObject, index)
				{
					CreateAlert({
						color: 'green',
						message: messageObject.infoMessage,
						messageIsHTML: false,
						button: false,
						closeButton: true,
						scrollToAlerts: index == 0,
						$module: $module
					});
				});
				
				json.informationalMessages = [];
				json.editing = false;
			}
		},
		
		callback: function(self)
		{
			self.QAS = {
				isChoosingEmail: ko.observable(false),
				emails: ko.observableArray(),
				selectedEmail: ko.observable(null),
				lastValidatedEmail: null,
				isVerifying: ko.observable(false),
				
				Reset: function()
				{
					self.QAS.isChoosingEmail(false);
					self.QAS.emails.removeAll();
					self.QAS.selectedEmail(null);
					self.QAS.lastValidatedEmail = null;
					self.QAS.isVerifying(false);
				},
				
				KeepEmail: function(field, submit)
				{
					self.QAS.lastValidatedEmail = ko.unwrap(field);
					
					if (typeof submit == 'function')
					{
						submit();
					}
				},
				
				UseSelectedEmail: function(field, submit)
				{
					if (self.QAS.selectedEmail() && ko.isObservable(field))
					{
						field(self.QAS.selectedEmail());
					}
					
					self.QAS.lastValidatedEmail = self.QAS.selectedEmail();
					
					if (typeof submit == 'function')
					{
						submit();
					}
				},
				
				IsLastValidatedEmail: function(field)
				{
					return ko.unwrap(field) == self.QAS.lastValidatedEmail;
				}
			};
			
			self.SetEditMode = function()
			{
				self.QAS.Reset();
				self.model().editing(true);
				self.ResetOriginalModel();
			};
			
			self.HasModelChanged = function()
			{
				if (!self.model() || !self.originalModel())
					return false;
				
				if (self.model().updateAll())
					return true;
				
				if (self.model().email() != self.originalModel().email())
					return true;
				
				// Sometimes phone number can be null, so default to an empty string for the next few lines to work
				var oldPhone = self.originalModel().phone() || '';
				var newPhone = self.model().phone() || '';
				
				// Remove formatting from phone numbers
				oldPhone = oldPhone.replace(/[^\d]/g, '');
				newPhone = newPhone.replace(/[^\d]/g, '');
				
				return oldPhone != newPhone;
			};
			
			self.Cancel = function(m, e)
			{
				self.RemoveAllErrors();
				
				self.model().phone(self.originalModel().phone());
				self.model().email(self.originalModel().email());
				self.model().updateAll(self.originalModel().updateAll());
				
				self.model().editing(false);
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});