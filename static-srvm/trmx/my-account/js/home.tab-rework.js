/*
 * TAB.Load
 * - Loads a tab on the page
 *
 * @param	tabName - Name of the tab to load
 * @param	options (optional) - Object of options to pass
 * @param	callback (optional)
 *
 * @option	focusModule - Set the name of the module to be focused on tab load
 * @option	module-[moduleName] - Set a value to be passed to the module's load function
 *
 * @notes	Callback params: $contents, options
 */

/*
 * TAB.Reload
 * - Reloads the currently loaded tab with its last loaded options
 *
 * @param	callback (optional)
 *
 * @notes	Callback works the same as TAB.Load
 */

/*
 * TAB.GetCurrentTab
 * - Gets the value of the currently loaded tab
 *
 * @return	String of the current tab name
 */

/*
 * TAB.GetCurrentRequestID
 * - Gets the request ID of the last loaded tab
 *
 * @return	Integer of the last loaded tab request ID
 */

/*
 * TAB.IsLoading
 * - Gets whether a tab is loading or not
 *
 * @return	Boolean showing if tab is loading (true=loading, false=not)
 */

/*
 * TAB.Modules.Add
 * - Adds a module to the list for custom onload
 *
 * @param	moduleName
 * @param	onLoadCallback
 *
 * @notes	Callback params: $module, options, onLoaded
 * @notes	Your callback should call onLoaded() when it is done loading
 *
 * @example
 *
 * TAB.Modules.Add('my-module', function($module, options, onLoaded) {
 *     // do stuff with module/options to set up
 *
 *     onLoaded();
 * });
 */

/*
 * TAB.Modules.Reload
 * - Reloads a module with its last loaded options
 *
 * @param	moduleName - Which module to reload
 * @param	callback - Called when module has finished reloading
 *
 * @notes	Callback params: $module, options
 */

/*
 * TAB.Modules.API
 * - Reloads a module with its last loaded options
 *
 * @param	moduleName - Which module to reload
 * @param	callback - Called when module has finished reloading
 *
 * @notes	Callback params: $module, options
 */

/*
 * TAB.Modules.Focus
 * - Focuses the page on a given module name
 *
 * @param	moduleName - Which module to focus on
 * @param	callback - Called after the module has been focused on
 *
 * @notes	Callback params: $module
 */

/*
 * TAB.OnBeforeTabLoad
 * - Called when the tab contents are initially received but no action has been done to load modules or show the tab.
 *
 * @param	tabName (optional) - Only hook for a specific tab
 * @param	callback - Called when the event is fired
 *
 * @notes	Callback params: tabName, $contents, options
 * @notes	Return false in the callback to disable the tab from loading
 */

/*
 * TAB.OnTabLoad
 * - Called after the tab has finished loading
 *
 * @param	tabName (optional) - Only hook for a specific tab
 * @param	callback - Called when the event is fired
 *
 * @notes	Callback params: tabName, $contents, options
 */

/*
 * TAB.OnTabLoadError
 * - Called when a tab tried to load but an error was given
 *
 * @param	tabName (optional) - Only hook for a specific tab
 * @param	callback - Called when the event is fired
 *
 * @notes	Callback params: tabName, options
 */

/*
 * TAB.OnModuleLoad
 * - Called after the module has finished loading
 *
 * @param	moduleName (optional) - Only hook for a specific tab
 * @param	callback - Called when the event is fired
 *
 * @notes	Callback params: moduleName, $module, options
 */

(function($, window)
{
	var TAB = window.TAB = {};


	var currentRequestId = 0;
	var currentTabName = null;
	var currentTabLoadParams = [];
	var isLoading = false;
	var callbacks = {};
	var modules = {};

	var NUM_LOAD_PARAMS = 3;
	
	TAB.Load = function(name, options, callback)
	{
		var thisRequest = ++currentRequestId;
		var loadParams = Array.prototype.slice.call(arguments, 0);

		options = $.extend({}, options);

		currentTabName = name;
		currentTabLoadParams = loadParams.slice(0, NUM_LOAD_PARAMS - 1); // Remove the callback param

		$('#tab-wrapper').removeClass('error').addClass('loading');
		
		ScrollTo(0, 0);

//	debugger;
	var url = 'https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/my-account-rework.json';
	if(currentTabName == 'contact-profile'){
		url = 'https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/contact-profile-rework.json';
	}
	if(currentTabName == 'payments'){
		url = 'https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/payments-rework.json';
	}

		// https://gitlab.com/aiemohankumar/terminix/raw/master/my-account-json/my-account-rework.json
	//	$.get('/my-account/pages/tabs/' + TabNameToFileName(name) + window.location.search).always(function(data)
		$.get(url).always(function(data)
		{
		
			if (thisRequest != currentRequestId)
				return;
			
			var response;
			
			if (typeof data === 'string')
				response = data;

			if (response)
			{
				var $response = $('<div></div>').html(response).children();

				var continued = TriggerEvent('OnBeforeTabLoad', name, [name, $response, options], true);

				if (!continued)
				{
					$tabWrapper.removeClass('loading');
					return;
				}

				$('#tab-contents').empty().append($response);
				$('#tab-wrapper').removeClass('loading');
				
				// Tell the old-style modules to still load
				var $modules = $('#tab-contents').find('.module');
				var loadedModules = 0;
				var loadedTabFunc = false;

				var FinishLoadingTab = function() {
					if (thisRequest != currentRequestId || !loadedTabFunc || loadedModules < $modules.length) {
						return;
					}
					
					if (options.focusModule) {
						var $module = $('#tab-contents .module-' + options.focusModule);
						if ($module.length) {
							ScrollTo($module.offset().top, 1000);
						}
					}
					
					if (typeof callback == 'function') {
						callback(options);
					}
					
					TriggerEvent('OnTabLoad', name, [name, $response, options]);
				};
				
				$modules.each(function() {
					var $module = $(this);
					var moduleName = $module.data('name');
					var moduleOptions = null;
					
					if (('module-' + moduleName) in options) {
						moduleOptions = options['module-' + moduleName];
					}
					
					var OnModuleLoad = function() {
						TriggerEvent('OnModuleLoad', moduleName, [moduleName, $module, moduleOptions]);
						
						if (++loadedModules === $modules.length) {
							FinishLoadingTab();
						}
					};

					if (moduleName in modules) {
						modules[moduleName].lastLoadOptions = moduleOptions;
						modules[moduleName].$clone = $module.clone(true);

						modules[moduleName].Load($module, moduleOptions, function() {
							if (thisRequest != currentRequestId)
								return;

							OnModuleLoad();
						});
					} else {
						OnModuleLoad();
					}
				});
				
				var fn = 'LoadTab_' + TabNameToFunction(name);
				if (fn in window && typeof window[fn] == 'function') {
					window[fn](function() {
						loadedTabFunc = true;
						FinishLoadingTab();
					}, options);
				} else {
					loadedTabFunc = true;
					FinishLoadingTab();
				}
			}
			else
			{
				$('#tab-wrapper').removeClass('loading').addClass('error');
				
				TriggerEvent('OnTabLoadError', name, [name, options]);
			}
			
			setTimeout(function() { ScrollTo(0, 0); }, 10);
		});

		return this;
	};

	TAB.Reload = function(callback)
	{
		var params = currentTabLoadParams.slice(0);

		while (params.length < NUM_LOAD_PARAMS - 1)
		{
			params.push(undefined);
		}

		params.push(callback);

		return this.Load.apply(this, params);
	};

	TAB.GetCurrentTab = function()
	{
		return currentTabName;
	};
	
	TAB.GetCurrentRequestID = function()
	{
		return currentRequestId;
	};

	TAB.IsLoading = function()
	{
		return $tabWrapper.hasClass('loading');
	};

	TAB.Modules = {};

	TAB.Modules.Add = function(name, onLoad)
	{
		modules[name] = {};

		modules[name].Load = onLoad;
		modules[name].lastLoadOptions = {};
		modules[name].$clone = $();

		return this;
	};

	TAB.Modules.Reload = function(name, callback)
	{
		if (name in modules && modules[name].$clone.length)
		{
			var $oldModule = $tabContents.find('.module.module-' + name);

			if ($oldModule.length)
			{
				var $newModule = modules[name].$clone.clone(true).addClass('loading');

				$oldModule.replaceWith($newModule);

				modules[name].Load($newModule, modules[name].lastLoadOptions, function()
				{
					$newModule.removeClass('loading');
					
					if (typeof callback === 'function')
						callback($newModule, modules[name].lastLoadOptions);
				});
			}
		}

		return this;
	};
	
	TAB.Modules.API = function(name, fn, data)
	{
		if (name in modules)
		{
			var $module = $tabContents.find('.module.module-' + name);
			
			if ($module.length)
			{
				var vm = $module.data('vm');
				var api = $module.data('api');
				
				if (vm && api && api.hasOwnProperty(fn) && typeof api[fn] == 'function')
				{
					api[fn].call(vm, vm, data || {});
				}
			}
		}
		else if (name == '*')
		{
			for (var module in modules)
			{
				TAB.Modules.API(module, fn, data);
			}
		}
		
		return this;
	};

	TAB.Modules.Focus = function(name, callback)
	{
		var $module = $tabContents.find('.module.module-' + name);

		if ($module.length)
		{
			ScrollTo($module.offset().top, 'fast', function()
			{
				if (typeof callback === 'function')
					callback($module);
			});
		}

		return this;
	};

	(function CreateHookableEvents(names)
	{
		names.forEach(function(name, i)
		{
			callbacks[name] = {};

			TAB[name] = function()
			{
				var key = null;
				var callback = null;

				if (arguments.length == 2)
				{
					key = arguments[0];
					callback = arguments[1];
				}
				else if (arguments.length == 1)
				{
					key = '*';
					callback = arguments[0];
				}

				if (typeof key === 'string' && typeof callback === 'function')
				{
					if (!(key in callbacks[name]))
					{
						callbacks[name][key] = [callback];
					}
					else
					{
						callbacks[name][key].push(callback);
					}
				}
			};
		});
	})(['OnBeforeTabLoad', 'OnTabLoad', 'OnTabLoadError', 'OnModuleLoad']);

	function TriggerEvent(type, key, params, stopOnFalse)
	{
		if (type in callbacks)
		{
			var keys = ['*', key];

			for (var i = 0; i < keys.length; i++)
			{
				if (keys[i] in callbacks[type])
				{
					for (var j = 0; j < callbacks[type][keys[i]].length; j++)
					{
						var ret = callbacks[type][keys[i]][j].apply(null, params);

						if (stopOnFalse && ret === false)
							return false;
					}
				}
			}
		}

		return true;
	}

	// Helper functions
	// =========================================================

	function HyphenCaseToCamelCase(str)
	{
		return str.split('-').map(function(piece) { return piece.substring(0, 1).toUpperCase() + piece.substring(1); }).join('')
	}

	function TabNameToFileName(name)
	{
		return name + '-rework.jsp';
	}

	function TabNameToFunction(name)
	{
		return HyphenCaseToCamelCase(name);
	}

	function ModuleNameToFunction(name)
	{
		return HyphenCaseToCamelCase(name);
	}

})(jQuery, window);