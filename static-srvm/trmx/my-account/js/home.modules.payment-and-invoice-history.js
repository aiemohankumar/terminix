/*
 * Module: Payment And Invoice History
 ***********************************************************************************************************/
TAB.Modules.Add('payment-and-invoice-history', function($module, options, onLoaded) {
	var vm = ViewModel_CreateGenericForModule({
		$module: $module,
		moduleOptions: options,

		api: {
			setPaymentDueData: function(self, json) {
				if (json.invoiceHistory && json.invoiceHistory.history) {
					self.invoiceHistory.SetHistory(json.invoiceHistory.history);
					self.invoiceHistory.showInvoiceNumber(json.invoiceHistory.showInvoiceNumber);
					
					self.invoiceHistory.loaded(true);
				} else {
					self.invoiceHistory.error(true);
				}
			}
		},
		
		callback: function(self) {
			self.VIEW_PAYMENT_HISTORY = 'payment-history';
			self.VIEW_INVOICE_HISTORY = 'invoice-history';
			
			self.view = ko.observable(self.VIEW_PAYMENT_HISTORY);
			
			self.paymentHistory = (function(model) {
				model.loaded = ko.observable(false);
				model.error = ko.observable(false);
				
				model.history = ko.observableArray();
				
				model.numViewableDefault = 4;
				model.numViewableIncrease = 4;
				model.numViewable = ko.observable(model.numViewableDefault);
				
				model.viewableHistory = ko.computed(function() {
					return model.history().slice(0, model.numViewable());
				});
				
				model.canViewMore = ko.computed(function() {
					return model.numViewable() < model.history().length;
				});
				
				model.canViewLess = ko.computed(function() {
					return model.numViewable() > model.numViewableDefault;
				});
				
				model.ViewMore = function() {
					if (model.canViewMore()) {
						model.numViewable(Math.min(model.numViewable() + model.numViewableIncrease, model.history().length));
					}
				};
				
				model.ViewLess = function() {
					if (model.canViewLess()) {
						model.numViewable(model.numViewableDefault);
					}
				};
				
				return model;
			})({});
			
			self.invoiceHistory = (function(model) {
				model.loaded = ko.observable(false);
				model.error = ko.observable(false);
				
				model.history = ko.observableArray();
				model.showInvoiceNumber = ko.observable(false);
				
				model.numViewableDefault = 4;
				model.numViewableIncrease = 4;
				model.numViewable = ko.observable(model.numViewableDefault);
				
				model.SetHistory = function(history) {
					history.forEach(function(item) {
						item.loading = ko.observable(false);
						item.downloadLink = ko.observable(item.downloadLink || '');
					});
					
					model.history(history);
				};
				
				model.viewableHistory = ko.computed(function() {
					return model.history().slice(0, model.numViewable());
				});
				
				model.canViewMore = ko.computed(function() {
					return model.numViewable() < model.history().length;
				});
				
				model.canViewLess = ko.computed(function() {
					return model.numViewable() > model.numViewableDefault;
				});
				
				model.ViewMore = function() {
					if (model.canViewMore()) {
						model.numViewable(Math.min(model.numViewable() + model.numViewableIncrease, model.history().length));
					}
				};
				
				model.ViewLess = function() {
					if (model.canViewLess()) {
						model.numViewable(model.numViewableDefault);
					}
				};
				
				model.CheckInvoiceLink = function(item) {
					// If it already has a link, then allow the link to work
					if (item.downloadLink())
						return true;
					
					// If already loading the link, do nothing
					if (item.loading())
						return false;
					
					// Load the link
					item.loading(true);
					
					var json = ko.mapping.toJS(item);
					
					PromiseDWR({ url: "MyAccountTMXPaymentUIUtils/getInvoiceDocumentLink", data: json }).then(function(data) {
						if (data.errorMessages && data.errorMessages.length) {
							throw "error";
						}
						
						item.downloadLink(data.downloadLink);
						
						window.open(data.downloadLink, '_blank');
					}).catch(function() {
						CreateAlert( {
							color: 'red',
							button: false,
							closeButton: true,
							message: "There was a problem retrieving the document link for your invoice. Please try again.",
							scrollToAlerts: true,
							$module: $module
						});
					}).then(function() {
						item.loading(false);
					});
				};
				
				return model;
			})({});
			
			self.LoadData = function(callback) {
				PromiseDWR("MyAccountTMXPaymentUIUtils/getPaymentHistory").then(function(data) {
					self.paymentHistory.history(data.history);
					self.paymentHistory.loaded(true);
				}).catch(function() {
					self.paymentHistory.error(true);
				});
				
				callback();
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});