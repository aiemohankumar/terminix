/*
 * Module: Change Password
 ***********************************************************************************************************/
TAB.Modules.Add('change-password', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,
		
		LoadDataDWR: 'MyAccountTMXPasswordUIUtils/getPasswordModel',
		
		SubmitDWR: 'MyAccountTMXPasswordUIUtils/setPassword',
		
		callback: function(self)
		{
			self.passwordIsValid = ko.observable(false);
			self.confirmPasswordIsValid = ko.observable(false);
			
			self.IsValidPasswordChange = function()
			{
				return self.model().currentPassword() && self.passwordIsValid() && self.confirmPasswordIsValid();
			};
		},
		
		OnBeforeSubmit: function(self)
		{
			return !self.submitting() && self.IsValidPasswordChange();
		},
		
		OnAfterSubmit: function(self)
		{
			if (self.model().informationalMessages().length)
			{
				CreateAlert({
					color: 'green',
					message: self.model().informationalMessages()[0].infoMessage(),
					button: false,
					closeButton: true,
					scrollToAlerts: true,
					$module: $module
				});
			}
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});