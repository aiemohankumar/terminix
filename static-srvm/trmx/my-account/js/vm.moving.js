function ViewModel()
{
	var self = this;
	
	ViewModel_AddCommonFunctions(self);
	ViewModel_AddStepFunctions(self, {
		stepOrder: ['address', 'schedule']
	});
	
	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	self.error = ko.observable();
	
	self.disableInputs = ko.computed(function() {
		return self.submitting();
	});
	
	self.scheduleMonths = ko.observableArray();
	self.scheduleTimes = ko.observableArray();
	
	self.selectedScheduleDate = ko.observable();
	self.selectedScheduleTime = ko.observable();
	self.noDateSelected = ko.observable(false);
	
	self.noDateSelected.subscribe(function(noDateSelected) {
		if (noDateSelected) {
			self.selectedScheduleDate(null);
			self.selectedScheduleTime(null);
		}
	});
	
	self.LoadData = function()
	{
		var lookupZipcode = $.QueryString.zipcode;
		
		if (caniuse.sessionStorage && !lookupZipcode) {
			lookupZipcode = sessionStorage.getItem("MovingModelLookupZipcode");
		}
		
		PromiseDWR('my-account-json/getMovingModel.json').then(function(json) {
			if (json.redirectURL) {
				Redirect(json.redirectURL);
				return;
			}
			
			// Check if user doesn't have access to move
			if (!json.canMovePropertyByOnline) {
				Redirect("/my-account/pages/home.jsp#my-account/moving");
				return;
			}
			
			json.lookupZipcode = lookupZipcode;
			json.zipcode = lookupZipcode;

			return json;
		}).then(function(json) {
			if (json) {
				return PromiseDWR({ url: 'my-account-json/isZipServiceable.json', data: json }).then(function(json) {
					if (!json.isServiceable) {
						Redirect("/my-account/pages/home.jsp#my-account/moving");
						return;
					}
					
					return json;
				});
			}
		}).then(function(json) {
			if (json) {
				// Try to get the user's city and state from zipcode
				return GetCityAndStateFromZip(json.lookupZipcode).then(function(data) {
					json.city = data.city;
					json.state = data.state;
				}).catch(function(error) {
					// Ignore errors and don't prepopulate
				}).then(function() {
					return json;
				});
			}
		}).catch(function(error) {
			self.error("There was an error loading the page. Please refresh and try again.");
		}).then(function(json) {
			if (json) {
				self.model(ko.mapping.fromJS(json));
				self.modelLoaded(true);
				
				self.UpdateAutoFocus();
			}
		});
	};
	
	function DateKey(date)
	{
		var MM = ('00' + (date.getUTCMonth() + 1)).substr(-2);
		var dd = ('00' + date.getUTCDate()).substr(-2);
		var yyyy = date.getUTCFullYear();
		
		return MM + '/' + dd + '/' + yyyy;
	}
	
	function FormatHour(hour)
	{
		return (((hour %= 24) + 11) % 12 + 1) + (hour < 12 ? "AM" : "PM"); 
	}
	
	function CreateScheduleMonths(json)
	{
		var scheduleMonths = [];
		var scheduleTimes = [];
		
		if (json.scheduleDates && json.scheduleDates.length) {
			var scheduleMonthsMap = {};
			var scheduleTimesMap = {};
			
			var scheduleDates = [];
			var scheduleDatesMap = {};
			var firstDate, lastDate;
			
			// First create a date object for each schedule date
			// And find out our earliest and latest dates
			json.scheduleDates.forEach(function(scheduleDate) {
				var datePieces = scheduleDate.date.split('/');
				scheduleDate.dateObject = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
				
				if (!firstDate || firstDate > scheduleDate.dateObject)
					firstDate = new Date(scheduleDate.dateObject);
				
				if (!lastDate || lastDate < scheduleDate.dateObject)
					lastDate = new Date(scheduleDate.dateObject);
				
				scheduleDate.times.forEach(function(scheduleTime) {
					if (!(scheduleTime.time in scheduleTimesMap)) {
						var fromTime = FormatHour(scheduleTime.time);
						var toTime = FormatHour(scheduleTime.time + 2);
						
						scheduleTimesMap[scheduleTime.time] = true;
						scheduleTimes.push({
							time: scheduleTime.time,
							text: fromTime + ' - ' + toTime,
							available: false,
							scheduleTime: null
						});
					}
				});
				
				scheduleDates.push(scheduleDate);
				scheduleDatesMap[scheduleDate.date] = scheduleDate;
			});
			
			scheduleTimes.sort(function(a, b) {
				return a.time < b.time ? -1 : 1;
			});
			
			// Update all schedule dates to have all schedule times available
			// But only use the ones that are for the date
			scheduleDates.forEach(function(scheduleDate) {
				scheduleDate.scheduleTimes = scheduleTimes.map(function(scheduleTime) {
					var scheduleDateTime = scheduleDate.times.find(function(scheduleDateTime) {
						return scheduleDateTime.time == scheduleTime.time;
					}) || null;
					
					return $.extend({}, scheduleTime, {
						available: !!scheduleDateTime,
						scheduleTime: scheduleDateTime
					});
				});
			});
			
			// Build out a shell of months that are included
			var iterDate = new Date(Date.UTC(firstDate.getUTCFullYear(), firstDate.getUTCMonth(), 1));
			
			var DayItem = function(data) {
				var item = $.extend({
					text: '',
					date: null,
					available: false
				}, data);
				
				if (item.date) {
					var key = DateKey(item.date);
					
					if (key in scheduleDatesMap) {
						item.available = true;
						item.scheduleDate = scheduleDatesMap[key];
					}
				}
				
				return item;
			};
			
			while (iterDate <= lastDate) {
				var scheduleMonthKey = iterDate.getUTCMonth() + "/" + iterDate.getUTCFullYear();
				
				var scheduleMonth = {
					key: scheduleMonthKey,
					date: new Date(Date.UTC(iterDate.getUTCFullYear(), iterDate.getUTCMonth(), 1)),
					monthNumber: iterDate.getUTCMonth(),
					monthName: GetMonthName(iterDate.getUTCMonth() + 1),
					weeks: []
				};
				
				var monthIterDate = new Date(iterDate);
				
				// Build out the first week
				var week = { days: [] };
				
				for (var day = 0; day < monthIterDate.getUTCDay(); day++) {
					week.days.push(DayItem());
				}
				
				do {
					// Loop through the end of the week
					do {
						if (monthIterDate.getUTCMonth() == iterDate.getUTCMonth()) {
							week.days.push(DayItem({
								text: monthIterDate.getUTCDate(),
								date: new Date(monthIterDate)
							}));
						} else {
							week.days.push(DayItem());
						}
						
						monthIterDate.setUTCDate(monthIterDate.getUTCDate() + 1);
					}
					while (monthIterDate.getUTCDay() > 0);
					
					// Check if this week has any available days
					var hasAvailable = week.days.some(function(day) { return day.available; }),
						firstWeekDay = week.days.find(function(day) { return !!day.date });
					
					if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate) {
						scheduleMonth.weeks.push(week);
					}
					
					// Reset week
					week = { days: [] };
				}
				while (monthIterDate.getUTCMonth() == iterDate.getUTCMonth());

				// Only add it if it has weeks to show
				if (scheduleMonth.weeks.length > 0) {
					scheduleMonths.push(scheduleMonth);
					scheduleMonthsMap[scheduleMonthKey] = scheduleMonth;
				}
				
				iterDate.setUTCMonth(iterDate.getUTCMonth() + 1);
			}
		}
		
		self.scheduleMonths(scheduleMonths);
		self.scheduleTimes(scheduleTimes);
	}
	
	self.GetScheduleDateCSS = function(day)
	{
		return {
			disabled: !day.date || !day.available,
			active: self.selectedScheduleDate() && self.selectedScheduleDate() == day.scheduleDate
		}
	};
	
	self.GetScheduleTimes = function()
	{
		return self.selectedScheduleDate() ? self.selectedScheduleDate().scheduleTimes : self.scheduleTimes();
	};
	
	self.SetScheduleDate = function(day)
	{
		if (day.date && day.available) {
			self.selectedScheduleTime(null);
			self.selectedScheduleDate(day.scheduleDate);
		}
	};
	
	self.GetPreferredServiceDatePickerOptions = function()
	{
		var options = {};
		
		if (self.modelLoaded()) {
			var moveDate = self.model().moveDate();
			
			if (/^\d{2}\/\d{2}\/\d{4}$/.test(moveDate)) {
				var datePieces = moveDate.split('/');
				moveDate = new Date(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]);
				
				var currentDate = new Date();
				
				if (moveDate < currentDate) {
					moveDate = currentDate;
				}
				
				var lastDateInRange = new Date();
				lastDateInRange.setDate(lastDateInRange.getDate() + 59); // 60 days of range
				
				options.minDate = moveDate;
				options.maxDate = lastDateInRange;
				options.beforeShowDay = function(date) {
					// Has to return an array of [isSelectable, CSS class name string
					// for date cell, optional popup tooltip]
					// For date.getDay(), 0 = Sunday
					return [date.getDay() > 0, ''];
				};
			}
		}
		
		return options;
	};
	
	self.GetMoveDatePickerOptions = function() {
		var options = {};
		
		if (self.modelLoaded()) {
			var minDate = new Date();
			minDate.setDate(minDate.getDate() + 1);
			
			options.minDate = minDate;
		}
		
		return options;
	};
	
	self.IsValidStep = function()
	{
		return true;
	};
	
	self.SubmitStep = function()
	{
		if (self.disableInputs() || !self.IsValidStep()) return;
		
		self.submitting(true);
		
		self.RemoveAllErrors();
		
		var json = ko.mapping.toJS(self.model);
		
		json.selectedScheduleDate = null;
		json.selectedScheduleTime = null;
		json.noDateSelected = self.noDateSelected();
		
		if (self.selectedScheduleDate()) {
			json.selectedScheduleDate = self.selectedScheduleDate();
			
			if (self.selectedScheduleTime()) {
				json.selectedScheduleTime = self.selectedScheduleTime();
			}
		}
		
		var generateScheduleMonths = json.currentStep == 'address';
		
		PromiseDWR({ url: 'MyAccountTMXUIUtils/setMovingModel', data: json }).then(function(json) {
			if (json.redirectURL) {
				Redirect(json.redirectURL);
				return;
			}
			
			if (generateScheduleMonths && json.currentStep == 'schedule') {
				json.selectedScheduleDate = null;
				json.selectedScheduleTime = null;
				json.noDateSelected = false;
				
				CreateScheduleMonths(json);
			}
			
			self.noDateSelected(json.noDateSelected);
			self.selectedScheduleDate(null);
			self.selectedScheduleTime(null);
			
			if (json.selectedScheduleDate) {
				self.scheduleMonths().some(function(scheduleMonth) {
					return scheduleMonth.weeks.some(function(week) {
						return week.days.some(function(day) {
							if (day.available && day.scheduleDate.date == json.selectedScheduleDate.date) {
								self.selectedScheduleDate(day.scheduleDate);
								
								if (json.selectedScheduleTime) {
									day.scheduleDate.scheduleTimes.some(function(scheduleTime) {
										if (scheduleTime.available && scheduleTime.scheduleTime.time == json.selectedScheduleTime.time) {
											self.selectedScheduleTime(scheduleTime.scheduleTime);
											return true;
										}
									});
								}
								return true;
							}
						});
					});
				});
			}
			
			self.model(ko.mapping.fromJS(json));
		}).catch(function(error) {
			self.model().errorMessages.push({ errorMessage: error });
		}).then(function() {
			self.submitting(false);

			self.UpdateAutoFocus();
		});
	};
}

$(function()
{
	window.vm = new ViewModel();
	
	ko.applyBindings(vm);
	
	$('.on-knockout-loaded').show();
	
	vm.LoadData();
});