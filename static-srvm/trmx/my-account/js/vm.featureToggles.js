function ViewModel() {
	var self = this;

	// How often the toggle values are refreshed
	var TOGGLE_REFRESH_RATE = 15 * 60 * 1000; // milliseconds
	var TOGGLE_DAY_OFFSET = 0; // milliseconds - does the toggle reset on the hour, or is it offset?

	ViewModel_AddCommonFunctions(self);

	self.model = ko.observable(null);
	self.modelLoaded = ko.observable(false);
	self.originalModel = ko.observable(null);
	self.submitting = ko.observable(false);
	
	self.isEditEnabled = ko.observable(false);
	self.isEnvOnMultipleServers = ko.observable(false);

	self.brandsList = ko.observableArray([]);
	self.selectedBrand = ko.observable(null);
	
	self.confirmingFeatureToggles = ko.observable(false);
	self.confirmFeatureTogglesList = ko.observableArray();
	
	// If the selected brand ever changes, remove any errors/success messages
	// Also remove the user's unsaved changes
	self.selectedBrand.subscribe(function(value) {
		if (self.model() && !self.submitting()) {
			self.RemoveAllErrors();
			
			ResetFeatureToggles();
		}
	}, self, "beforeChange");
	
	self.hasMadeChanges = ko.computed(function() {
		if (!self.modelLoaded() || !self.selectedBrand()) return false;
		
		var selectedBrand = self.selectedBrand();
		var originalModel = self.originalModel();
		var originalBrand = originalModel && originalModel.toggleMap[selectedBrand.brandName()];
		
		return originalBrand && selectedBrand.featuresList().some(function(featureObject) {
			var originalFeatureObject = originalBrand.find(function(x) {
				return x.featureName == featureObject.featureName();
			});
			
			return originalFeatureObject && originalFeatureObject.enabled != featureObject.enabled();
		});
	});

	self.toggleTimerText = ko.observable('');
	self.toggleTimerBannerCSS = ko.observable({});

	function GetToggleTimeRemaining() {
		var date = new Date(),
			time = date.getTime(),
			start = (new Date(date.getFullYear(), date.getMonth(), date.getDate())).getTime();

		var timeElapsed = (time - start + TOGGLE_DAY_OFFSET) % TOGGLE_REFRESH_RATE,
			timeRemaining = TOGGLE_REFRESH_RATE - timeElapsed;

		return timeRemaining;
	}

	var timeoutUpdateTimerText;
	var lastTimeRemaining = undefined;
	
	self.showRefreshAlert = ko.observable(false);
	
	function UpdateTimerText() {
		if (timeoutUpdateTimerText) {
			clearTimeout(timeoutUpdateTimerText);
		}
		
		var timeRemaining = GetToggleTimeRemaining();
		
		if (typeof lastTimeRemaining != 'undefined' && timeRemaining > lastTimeRemaining) {
			self.showRefreshAlert(true);
		}
		
		lastTimeRemaining = timeRemaining;

		var hours = Math.floor(timeRemaining / 3600000),
			minutes = Math.floor((timeRemaining % 3600000) / 60000),
			seconds = Math.floor((timeRemaining % 60000) / 1000);

		if (hours < 10) hours = '0' + hours;
		if (minutes < 10) minutes = '0' + minutes;
		if (seconds < 10) seconds = '0' + seconds;

		self.toggleTimerText(hours + ':' + minutes + ':' + seconds);

		var bannerCSS = {
			red: false,
			yellow: false,
			green: false
		};

		if (timeRemaining >= TOGGLE_REFRESH_RATE * 2 / 3) {
			bannerCSS.green = true;
		} else if (timeRemaining >= TOGGLE_REFRESH_RATE * 1 / 3) {
			bannerCSS.yellow = true;
		} else {
			bannerCSS.red = true;
		}

		self.toggleTimerBannerCSS(bannerCSS);

		timeoutUpdateTimerText = setTimeout(function() {
			timeoutUpdateTimerText = undefined;
			UpdateTimerText();
		}, 250);
	}

	function GetModelJSON(options) {
		options = options || {};

		var originalModel = self.originalModel();

		var json = ko.mapping.toJS(self.model);

		// Recreate the toggleMap structure on the model
		json.toggleMap = {};

		self.brandsList().forEach(function(brandObject) {
			brandObject = ko.mapping.toJS(brandObject);

			json.toggleMap[brandObject.brandName] = brandObject.featuresList;

			// Set the isUpdating flag if we have an original model to compare to
			if (originalModel && originalModel.toggleMap && brandObject.brandName in originalModel.toggleMap) {
				// Grab the original features to compare against
				var originalFeaturesList = originalModel.toggleMap[brandObject.brandName];

				brandObject.featuresList.forEach(function(featureObject) {
					// Check if we have the original feature data
					var originalFeatureObject = originalFeaturesList.find(function(x) {
						return x.featureName == featureObject.featureName;
					});

					// Flag as updating if the enabled state is different from original
					featureObject.isUpdating = originalFeatureObject && originalFeatureObject.enabled != featureObject.enabled;
				});
			} else {
				// Mark all features as not updating since we don't have the original model, for some reason
				brandObject.featuresList.forEach(function(featureObject) {
					featureObject.isUpdating = false;
				});
			}
		});

		return json;
	}

	function UpdateModel(model) {
		self.originalModel(model);
		self.model(ko.mapping.fromJS(model));

		// Remember what brand was selected before removing
		// Removing all brands will change selectedBrand to undefined
		var selectedBrandName = self.selectedBrand() && self.selectedBrand().brandName() || '';
		
		self.brandsList.removeAll();

		var brandsList = [];

		for (var brandName in model.toggleMap) {
			if (model.toggleMap[brandName] && model.toggleMap[brandName].length) {
				model.toggleMap[brandName].sort(function(a, b) {
					return a.featureName.localeCompare(b.featureName);
				});

				brandsList.push(ko.mapping.fromJS({
					brandName: brandName,
					featuresList: model.toggleMap[brandName]
				}));
			}
		}

		brandsList.sort(function(a, b) {
			return a.brandName().localeCompare(b.brandName());
		});

		self.brandsList(brandsList);

		self.selectedBrand(self.brandsList().find(function(brandObject) {
			return brandObject.brandName() == selectedBrandName;
		}));
	}
	
	function ResetFeatureToggles() {
		var originalModel = self.originalModel();
		if (!originalModel) return;
		
		self.brandsList().forEach(function(brandObject) {
			var originalFeaturesList = originalModel.toggleMap[brandObject.brandName()];
			
			if (originalFeaturesList) {
				brandObject.featuresList().forEach(function(featureObject) {
					var originalFeatureObject = originalFeaturesList.find(function(x) {
						return x.featureName == featureObject.featureName();
					});
					
					if (originalFeatureObject) {
						featureObject.enabled(originalFeatureObject.enabled);
					}
				});
			}
		});
	}
	
	function GetFeatureToggles(callback) {
		CallDWR('MyAccountTMXUIUtils/getFeatureToggles', {}, function(data) {
			var json;

			if (data.error) {
				json = self.CreateDummyModel({
					toggleMap: {},
					errorMessages: [{ errorMessage : data.error }]
				});
			} else {
				json = data.model;

				// Pull the refresh rate and day offset from the model
				if (json.jobInterval) {
					TOGGLE_REFRESH_RATE = json.jobInterval;

					var date = new Date(),
						time = date.getTime(),
						start = new Date(date.getFullYear(), date.getMonth(), date.getDate());

					TOGGLE_DAY_OFFSET = (time - start + json.nextJobTime) % json.jobInterval;
				}

				UpdateTimerText();
			}

			if (typeof callback == 'function') {
				callback(json);
			}
		});
	}

	self.LoadData = function(defaultBrand) {
		GetFeatureToggles(function(json) {
			UpdateModel(json);
			
			if (defaultBrand) {
				self.selectedBrand(self.brandsList().find(function(brandObject) {
					return brandObject.brandName() == defaultBrand;
				}));
			}
			
			self.isEditEnabled(json.isEditEnabled);
			self.isEnvOnMultipleServers(json.isEnvOnMultipleServers);

			self.modelLoaded(true);
		});
	};
	
	self.RefreshCache = function() {
		if (self.submitting())
			return;

		// Reset everything to its original state
		self.modelLoaded(false);
		
		self.confirmingFeatureToggles(false);
		self.confirmFeatureTogglesList.removeAll();
		
		if (timeoutUpdateTimerText) {
			clearTimeout(timeoutUpdateTimerText);
		}
		
		self.showRefreshAlert(false);
		
		// Reload as if page refreshed
		self.LoadData();
	};

	self.HandleSubmit = function() {
		if (self.submitting() || self.confirmingFeatureToggles() || !self.selectedBrand())
			return;
		
		self.submitting(true);
		self.RemoveAllErrors();
		
		GetFeatureToggles(function(json) {
			self.submitting(false);
			
			if (json.errorMessages && json.errorMessages.length) {
				self.model().errorMessages(json.errorMessages);
			} else {
				self.originalModel(json);
				
				var selectedBrand = self.selectedBrand();
				var originalBrand = json.toggleMap && json.toggleMap[selectedBrand.brandName()];
				var featuresList = selectedBrand.featuresList().filter(function(featureObject) {
					var originalFeatureObject = originalBrand && originalBrand.find(function(x) {
						return x.featureName == featureObject.featureName();
					});
					
					return originalFeatureObject && originalFeatureObject.enabled != featureObject.enabled();
				});
				
				self.confirmFeatureTogglesList(featuresList);
				self.confirmingFeatureToggles(true);
			}
		});
	};
	
	self.CancelFeatureToggles = function() {
		if (self.submitting())
			return;
		
		self.confirmingFeatureToggles(false);
		
		ResetFeatureToggles();
	};
	
	self.ConfirmFeatureToggles = function() {
		if (self.submitting() || !self.confirmingFeatureToggles())
			return;
		
		self.submitting(true);
		self.RemoveAllErrors();

		var json = GetModelJSON();

		CallDWR('MyAccountTMXUIUtils/setFeatureToggles', json, function(data) {
			if (data.error) {
				self.model().errorMessages.push({
					errorMessage: data.error
				});
			} else {
				data.model.informationalMessages = [{ infoMessage: "Successfully updated feature toggle cache." }];
				
				UpdateModel(data.model);
			}

			self.submitting(false);
			self.confirmingFeatureToggles(false);
		});
	};
}

$(function() {
	window.vm = new ViewModel();

	ko.applyBindings(vm);

	$('.on-knockout-loaded').show();

	vm.LoadData($.QueryString.brand || 'tmx-myaccount');
});
