if (!window.jQuery) throw 'jQuery required.';

/*
 * Browser feature detection
 * 
 * If more features are added, consider creating an object rather than several global variables.
 **********************************************************************************************/
function IsStorageAvailable(type) {
	try {
		var storage = window[type],
		x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return e instanceof DOMException && (
			// everything except Firefox
			e.code === 22 ||
			// Firefox
			e.code === 1014 ||
			// test name field too, because code might not be present
			// everything except Firefox
			e.name === 'QuotaExceededError' ||
			// Firefox
			e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
			// acknowledge QuotaExceededError only if there's something already stored
			storage.length !== 0;
	}
}

var caniuse = (function(main)
{
	main.history = window.history && typeof history.pushState == 'function' && typeof history.replaceState == 'function' && typeof history.back == 'function';
	
	main.sessionStorage = IsStorageAvailable('sessionStorage');
	main.localStorage = IsStorageAvailable('localStorage');
	
	return main;
})({});

/*
 * Common ViewModel functions
 *****************************************************************/
function ViewModel_AddCommonFunctions(self)
{
	if (!self.$scope)
		self.$scope = $('body');
	
	self.submitting = ko.observable(false);
	self.isLiveChatEnabled = ko.observable(false);
	
	self.canUnmaskPasswords = ko.computed(function()
	{
		return Device.isMobile();
	});
	
	self.unmaskPassword = ko.observable(true); // Default to password not being masked
	
	self.toJSON = function(options) {
		var scope = self;
		var mapping = {};
		
		if (options && options.scope) {
			scope = options.scope;
		}
		if (options && options.mapping) {
			mapping = options.mapping;
		}
		
		if (!mapping.ignore) mapping.ignore = [];
		
		scope = ko.unwrap(scope);
		
		var unmapped = scope;
		
		if (scope) {
			if (IsObject(scope)) {
				unmapped = {};
				
				for (var key in scope) {
					if (mapping.ignore.indexOf(key) >= 0
					||  scope.__mapping_ignore && (key == '__mapping_ignore' || scope.__mapping_ignore.indexOf(key) >= 0)) {
						continue;
					}
					
					var value = ko.unwrap(scope[key]);
					
					if (value && (typeof value == 'function' || value.jquery)) {
						continue;
					}
					
					if (IsObject(value)) {
						value = self.toJSON({ scope: value });
					}
					else if (IsArray(value)) {
						value = value.map(function(item) {
							return self.toJSON({ scope: item });
						});
					}
					
					unmapped[key] = value;
				}
			}
			else if (IsArray(scope)) {
				unmapped = scope.map(function(item) {
					return self.toJSON({ scope: item });
				});
			}
		}
		
		return ko.mapping.toJS(unmapped, mapping);
	};
	
	self.LiveChatButtonClick = function()
	{
		if (self.isLiveChatEnabled())
			LiveChat_OpenChat();
	};
	
	self.CreateDummyModel = function(data)
	{
		return $.extend(true,
		{
			errorMessages: [],
			informationalMessages: [],
			fieldValidationErrors: [],
			redirectURL: false
			
		}, data);
	};
	
	self.RemoveAllErrors = function(scope)
	{
		if (!scope)
			scope = typeof self.model == 'undefined'? self : self.model();
		
		if (ko.isObservable(scope))
			scope = scope();
		
		if (scope) {
			scope.errorMessages && scope.errorMessages.removeAll();
			scope.informationalMessages && scope.informationalMessages.removeAll();
			scope.fieldValidationErrors && scope.fieldValidationErrors.removeAll();
		}
		
		return self;
	};
	
	self.HasError = function(field, scope)
	{
		return !!self.GetErrorMessage(field, scope);
	};
	
	self.GetErrorMessage = function(field, scope)
	{
		if (!scope)
			scope = typeof self.model == 'undefined' ? self : self.model;
		
		if (ko.isObservable(scope))
			scope = scope();
		
		if (!scope || !scope.fieldValidationErrors)
			return '';
		
		var error = ko.utils.arrayFirst(ko.unwrap(scope.fieldValidationErrors), function(error)
		{
			return ko.unwrap(error.field) == field;
		});
		
		return error ? ko.unwrap(error.errorMessage) : '';
	};
	
	self.AreModelsEqual = function(model1, model2)
	{
		if (IsObject(model1) && IsObject(model2))
		{
			for (var key in model1)
			{
				if (model1.hasOwnProperty(key) && model2.hasOwnProperty(key))
				{
					if (typeof model1[key] === 'object' && typeof model2[key] === 'object')
					{
						if (!self.AreModelsEqual(model1[key], model2[key]))
							return false;
					}
					else if (model1[key] != model2[key])
						return false;
				}
			}
		}
		else if (IsArray(model1) && IsArray(model2))
		{
			if (model1.length != model2.length)
				return false;
			
			for (var i = 0; i < model1.length; i++)
			{
				if (typeof model1[i] === 'object' && typeof model2[i] === 'object')
				{
					if (!self.AreModelsEqual(model1[i], model2[i]))
						return false;
				}
				else if (model1[i] != model2[i])
					return false;
			}
		}
		else if (model1 != model2)
			return false;
		
		return true;
	};
	
	self.ApplyModelChanges = function(object, data)
	{
		for (var i in data)
		{
			if (data.hasOwnProperty(i) && object.hasOwnProperty(i))
			{
				var value = data[i];
				
				if (IsArray(value))
				{
					value = ko.mapping.fromJS(value)();
				}
				else if (IsObject(value))
				{
					value = ko.mapping.fromJS(value);
				}
				
				if (ko.isObservable(object[i]))
					object[i](value)
				else
					object[i] = value;
			}
		}
	};
	
	self.UpdateAutoFocus = function()
	{
		// Grab the input available
		self.$scope.find('input:not(:checkbox, :radio), textarea, select').filter(':visible').each(function()
		{
			// Check if this is the first input with no value
			if (!$(this).val())
			{
				// Focus on this input and stop looking here
				$(this).focus();
				return false;
			}
		});
	};
}

function ViewModel_AddPaymentFunctions(self){
	self.sortedPaymentMethods = ko.computed(function() {
		var paymentMethods = self.paymentMethods();

		return paymentMethods.slice(0).sort(function(a, b) {
			var typeA = a.type(), typeB = b.type();

			if (typeA == typeB)
				return ('' + a.description()).localeCompare('' + b.description());
			if (typeA == 'saved' || typeA == '')
				return -1;
			if (typeB == 'saved' || typeB == '')
				return 1;
			if (typeA == 'bank')
				return -1;
			return 1;
		});
	});
	
	self.UpdateSelectedPaymentMethod = function() {

		if (self.selectedPaymentMethod() && self.selectedPaymentMethod().type()) {
			var selectedPaymentMethod = self.selectedPaymentMethod();

			self.paymentMethods().forEach(function(paymentMethod) {
				if (paymentMethod.type() == selectedPaymentMethod.type() && paymentMethod.description() == selectedPaymentMethod.description()) {
					self.selectedPaymentMethod(paymentMethod);
				}
			});
		}
	};
	
	self.IsVisibleHelp = function(field) {
		return self.visibleHelp() == field;
	};
	
	self.SetVisibleHelp = function(field) {
		self.visibleHelp(field);

		return self;
	};
	
	self.CanSetupEasyPay = function() {
		// Check if we're paying for something
		var duePaymentsBeingPaid = self.duePayments().filter(function(duePayment) {
			return duePayment.amount() > 0;
		});

		if (duePaymentsBeingPaid.length == 0) {
			return false;
		}

		// Check if paying more than one sales agreement
		var salesAgreements = duePaymentsBeingPaid.map(function(duePayment) {
			return duePayment.salesAgreementNumber();
		});

		salesAgreements = salesAgreements.filter(function(salesAgreement, index) {
			return salesAgreements.indexOf(salesAgreement) == index;
		});

		if (salesAgreements.length > 1) {
			return false;
		}

		// Check if the sales agreement being paid for is eligible for
		// EasyPay
		return duePaymentsBeingPaid[0].isEligibleForEasyPay();
	};
	
	self.GetAvailableCreditCardYears = function() {
		var years = [];

		for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
			years.push(i);
		}

		return years;
	};
	
	//Reset the model
	self.CancelPayment = function(m, e) {
		self.RemoveAllErrors();

		// Empty out other fields that may have been populated
		var fieldsToRevert = [];

		if (self.selectedPaymentMethod()) {
			switch (self.selectedPaymentMethod().type()) {
			case 'saved': {
				fieldsToRevert = [ 'visibleHelp' ];
				break;
			}
			case 'onetime': {
				fieldsToRevert = [ 'visibleHelp', 'cardNumber', 'cardType', 'cardSecurity', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1',
						'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'savePaymentMethod' ];
				break;
			}
			case 'card': {
				fieldsToRevert =  [ 'visibleHelp', 'cardNumber', 'cardType', 'cardSecurity', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1',
									'billingAddress2', 'billingCity', 'billingState', 'billingZipcode', 'savePaymentMethod' ];
				break;
			}
			case 'bank': {
				fieldsToRevert = [ 'visibleHelp', 'bankRoutingNumber', 'bankAccountNumber', 'billingAddress1', 'billingAddress2', 'billingCity',
						'billingState', 'billingZipcode', 'savePaymentMethod' ];
				break;
			}
			}

			fieldsToRevert.forEach(function(field) {
				self[field](self.originalModel()[field]());
			});

			self.selectedPaymentMethod(null);
		}

		// Scroll back to payment method choices for mobile view
		if(m && m.find('.payment-summary').length > 0){
			var position = m.find('.payment-summary').offset().top;
			
			if (position < $(window).scrollTop()) {
				ScrollTo(position, 400);
			}
		}

		return self;
	};
	
	self.IsValidPaymentMethod = function() {
		if (!self.selectedPaymentMethod())
			return false;

		var item = self;

		switch (self.selectedPaymentMethod().type()) {
		case 'saved':
			return true;

		case 'card':
			if (!item.cardNumber()
			// || !item.cardSecurity() // Disabled from TMA-349
			|| !item.cardExpireMonth() || !item.cardExpireYear() || !item.billingAddress1() || !item.billingCity() || !item.billingState()
					|| !item.billingZipcode())
				return false;
			break;

		case 'bank':
			if (!item.bankRoutingNumber() || !item.bankAccountNumber())
				return false;
			break;
		}

		return true;
	};
	
	self.ShowDetail = function(item) {
		item.showDetail(!item.showDetail());

		return self;
	};
}

function ViewModel_AddScheduleFunctions(self) {
	
	self.ShowDetail = function(item)
	{
		item.showDetail(!item.showDetail() || item.rescheduleStep() != '');
	};

	self.DateKey = function(date)
	{
		return date ? (date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate()) : '';
	};
	
	self.GetModelJSON = function(item)
	{
		var json = ko.mapping.toJS(item);
		
		// Format the rescheduleSelectedDate for the server
		if (json.rescheduleSelectedDate && json.rescheduleSelectedDate.date)
		{
			var date = new Date(json.rescheduleSelectedDate.date);
			
			json.rescheduleSelectedDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
		}
		
		// Reset time variables
		json.rescheduleSelectedTime = null;
		json.rescheduleSelectedEmployeeNumber = null;
		
		// Check if a time object is chosen to populate time variables
		var rescheduleSelectedTimeObject = ko.mapping.toJS(item.rescheduleSelectedTimeObject);
		
		if (rescheduleSelectedTimeObject)
		{
			json.rescheduleSelectedTime = rescheduleSelectedTimeObject.time;
			json.rescheduleSelectedEmployeeNumber = rescheduleSelectedTimeObject.employeeNumber;
		}
		
		return json;
	};
	
	self.CreateRescheduleMonths = function(item)
	{
		if (ko.isObservable(item.months))
		{
			item.months([]);
		}
		else
		{
			item.months = ko.observableArray();
		}
		
		var createFakeCalender = false; // set this to true using developer tools to test creating calendar with fake dates

		if (createFakeCalender && (!item.rescheduleDates() || !item.rescheduleDates().length)) {
			item.rescheduleDates((function() {
				var dates = [];
				var date = new Date(), currentTime = date.getTime(), currentMonth = date.getMonth(), daysIntoMonth = 15;
				var availableTimeSlots = [ 8, 10, 12, 14, 16, 18 ];

				// Enough days to span more than 1 month
				do {
					date.setTime(date.getTime() + 86400000);

					if (date.getDay() == 0 || date.getDay() == 6)
						continue;

					var times = availableTimeSlots.slice();

					for (var t = Math.floor(Math.random() * availableTimeSlots.length); t > 0; t--) {
						times.splice(Math.floor(Math.random() * times.length), 1);
					}

					dates.push(ko.mapping.fromJS({
						times : times.map(function(time) {
							return {
								time : time,
								employeeNumber : '123456'
							};
						}),
						date : (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
					}));
				} while (date.getMonth() == currentMonth || date.getTime() - (86400000 * 14) < currentTime || date.getDate() < daysIntoMonth);
				
				item.rescheduleTimes(availableTimeSlots.filter(function(timeSlot) {
					return dates.some(function(dateObject) {
						return dateObject.times().some(function(dateTime) {
							return dateTime.time() == timeSlot;
						});
					});
				}));

				return dates;

			})());
		}
		
		if (!item.rescheduleDates() || !item.rescheduleDates().length)
			return;

		var dates = [],
			months = {},
			monthsArray = [],
			availableDates = {},
			firstDate = null,
			lastDate = null;
		
		var DayItem = function(data)
		{
			var item = $.extend({
				text: '',
				date: null,
				available: false,
				times: []
			}, data);
			
			var key = self.DateKey(item.date);
			
			if (key in availableDates)
			{
				item.available = true;
				item.times = availableDates[key].times;
			}
			
			return item;
		};
		
		item.rescheduleDates().forEach(function(d)
		{
			var datePieces = d.date().split('/');
			var date = new Date(Date.UTC(datePieces[2], parseInt(datePieces[0]) - 1, datePieces[1]));
			
			if (firstDate == null || firstDate > date)
				firstDate = new Date(date);
			
			if (lastDate == null || lastDate < date)
				lastDate = new Date(date);
			
			dates.push(date);
			
			availableDates[self.DateKey(date)] = ko.mapping.toJS(d);
		});
		
		dates.forEach(function(date)
		{
			if (!(date.getUTCMonth() in months))
			{
				var month = {
					date: new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), 1)),
					monthNumber: date.getUTCMonth(),
					monthName: GetMonthName(date.getUTCMonth() + 1),
					weeks: []
				};
				
				// Build first week
				var d = new Date(month.date);
				var week = { days: [] };
				
				for (var day = 0; day < d.getUTCDay(); day++)
				{
					week.days.push(DayItem());
				}
				
				var thisMonth = d.getUTCMonth();
				
				do
				{
					// Loop through the end of the week
					do
					{
						if (d.getUTCMonth() == thisMonth)
						{
							week.days.push(DayItem({
								text: d.getUTCDate(),
								date: new Date(+d)
							}));
						}
						else
						{
							week.days.push(DayItem());
						}
						
						d.setTime(d.getTime() + 86400000);
					}
					while (d.getUTCDay() > 0);
					
					// Check if this week has any available days
					var hasAvailable = week.days.some(function(day) { return day.available; }),
						firstWeekDay = week.days.find(function(day) { return !!day.date });
					
					if (hasAvailable || firstWeekDay && firstWeekDay.date >= firstDate && firstWeekDay.date <= lastDate)
					{
						// Add week for month
						month.weeks.push(week);
					}
					
					// Reset week
					week = { days: [] };
				}
				while (d.getUTCMonth() == thisMonth);
				
				// Store month
				months[thisMonth] = month;
				monthsArray.push(ko.mapping.fromJS(month));
			}
		});
		
		monthsArray.sort(function(a, b)
		{
			return a.date() < b.date() ? -1 : 1;
		});
		
		item.months(monthsArray);
	};
	
	self.IsValidReschedule = function(item)
	{
		return item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject();
	};
	
	self.CloseConfirmation = function(item)
	{
		item.rescheduleStep('');
		item.showDetail(false);
	};
	
	self.SetScheduleDay = function(item, day)
	{
		if (day.date() && day.available())
		{
			item.rescheduleSelectedDate(day);
			item.rescheduleSelectedTimeObject(null);
		}
	};
	
	self.SetNoRescheduleDay = function(item)
	{
		item.rescheduleSelectedDate(null);
		item.rescheduleSelectedTimeObject(null);
		item.rescheduleStep('contact');
		
		// if (self.property.liveChatModel().showLiveChatButton() && GetFeatureToggleDefaultFalse("proactiveChatScheduleCFR")) {
		// 	LiveChat_OpenChat();
		// }
	};
	
	self.GetRescheduleTimes = function(item)
	{
		var timeObjects = [];
		
		if (item.rescheduleSelectedDate())
		{
			item.rescheduleTimes().forEach(function(time)
			{
				var timeObject = ko.utils.arrayFirst(item.rescheduleSelectedDate().times(), function(t)
				{
					return t.time() == time;
				});
				
				timeObjects.push(timeObject || ko.mapping.fromJS({
					time: time,
					employeeNumber: null
				}));
			});
		}
		else
		{
			item.rescheduleTimes().forEach(function(time)
			{
				timeObjects.push(ko.mapping.fromJS({
					time: time,
					employeeNumber: null
				}));
			});
		}
		
		return timeObjects;
	};
	
	self.GetScheduleDateDisplay = function(item, verbose, overrideDateString)
	{
		var dateObject, date;
		
		if (overrideDateString)
		{
			date = ('' + overrideDateString).split('/');
			
			if (date.length == 3)
			{
				dateObject = new Date(Date.UTC(parseInt(date[2]), parseInt(date[0]) - 1, parseInt(date[1])));
			}
		}
		else if (item.rescheduleSelectedDate() && item.rescheduleSelectedTimeObject())
		{
			dateObject = item.rescheduleSelectedDate().date();
		}
		
		if (dateObject)
		{
			if (verbose)
			{
				return GetMonthName(dateObject.getUTCMonth() + 1) + ' ' + dateObject.getUTCDate();
			}
			
			return (dateObject.getUTCMonth() + 1) + '/' + dateObject.getUTCDate() + '/' + dateObject.getUTCFullYear();
		}
		
		return '';
	};
	
	self.GetScheduleTimeDisplay = (function()
	{
		var FormatHour = function(hour)
		{
			return (hour > 12 ? hour - 12 : hour) + (hour < 12 ? 'AM' : 'PM');
		};
			
		return function(time)
		{
			if (!time) return '';
			
			if (time.hasOwnProperty('time'))
				time = time.time;
					
			if (ko.isObservable(time))
				time = time();
					
			return FormatHour(time) + ' - ' + FormatHour(parseInt(time) + 2);
		};
	})();

	var currentDate = new Date(), otherDate;
	
	self.DatepickerOptions = {
		minDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 1) && otherDate,
		maxDate: (otherDate = new Date(currentDate)).setDate(otherDate.getDate() + 45) && otherDate,
		beforeShowDay: function(date) {
			// Has to return an array of [isSelectable, CSS class name string
			// for date cell, optional popup tooltip]
			// For date.getDay(), 0 = Sunday
			return [date.getDay() > 0, ''];
		}
	};
	
	self.CancelRescheduleStep = function(item)
	{
		self.RemoveAllErrors(item);
		
		switch (item.rescheduleStep())
		{
		case 'error':
			item.rescheduleStep('');
			break;
			
		case 'appointment':
			item.rescheduleStep('');
			item.rescheduleSelectedDate(null);
			item.rescheduleSelectedTimeObject(null);
			break;
		
		case 'details':
			item.rescheduleStep('appointment');
			break;
		
		case 'contact':
			if (item.months().length)
			{
				item.rescheduleStep('appointment');
			}
			else
			{
				item.rescheduleStep('');
			}
			
			item.rescheduleContactFname(item._original.rescheduleContactFname());
			item.rescheduleContactLname(item._original.rescheduleContactLname());
			item.rescheduleContactPhone(item._original.rescheduleContactPhone());
			item.rescheduleContactDate('');
			item.rescheduleContactTime('');
			break;
		}
	};
	
	self.IsValidContact = function(item)
	{
		return item.rescheduleContactFname() && item.rescheduleContactLname() && item.rescheduleContactPhone() && item.rescheduleContactDate() && item.rescheduleContactTime();
	};
	
	
	self.GetScheduleDayCSS = function(item, day)
	{
		return {
			disabled: !day.date() || !day.available(),
			active: day.date() && item.rescheduleSelectedDate() && self.DateKey(item.rescheduleSelectedDate().date()) == self.DateKey(day.date())
		};
	};
	
}

function ViewModel_AddStepFunctions(self, options)
{
	options = $.extend({
		
		stepOrder: [],
		
		GetCurrentStep: function()
		{
			var currentStep = '';
			
			try {
				currentStep = self.model().currentStep();
			}
			catch (e) {}
			
			return currentStep;
		},
		
		SetCurrentStep: function(step)
		{
			try {
				self.model().currentStep(step);
			}
			catch(e) {}
		},
		
		OnChangeStep: function(step)
		{
			//
		}
		
	}, options);
	
	self.IsStepFinished = function(name)
	{
		var currentStep = options.GetCurrentStep();
		
		for (var i = 0; i < options.stepOrder.length; i++)
		{
			if (options.stepOrder[i] == currentStep)
				return false;
			
			if (options.stepOrder[i] == name)
				return true;
		}
		
		return false;
	};
	
	self.GoToStep = function(name)
	{
		if (self.IsStepFinished(name))
		{
			options.SetCurrentStep(name);
			
			self.RemoveAllErrors();
			
			if (typeof options.OnChangeStep == 'function')
			{
				options.OnChangeStep(name);
			}

			self.UpdateAutoFocus();
		}
	};
	
	self.UpdateAutoFocus = function()
	{
		if (!self.lastFocusedStep)
		{
			self.lastFocusedStep = '';
		}
		
		// Make sure the current step is visible to the user
		var currentStep = options.GetCurrentStep();
		
		if (!self.lastFocusedStep && currentStep == 'lookup')
		{
			self.lastFocusedStep = currentStep;
			
			ScrollTo(0, 0);
		}
		else if (currentStep != self.lastFocusedStep)
		{
			self.lastFocusedStep = currentStep;
			
			var $step = self.$scope.find('.step.step-' + currentStep);
			
			var stepOffset = $step.offset();
			stepOffset.height = $step.outerHeight();
			stepOffset.bottom = stepOffset.top + stepOffset.height;
			
			var windowTop = $(window).scrollTop();
			var windowHeight = $(window).height();
			var windowBottom = windowTop + windowHeight;
			
			if (stepOffset.top > windowTop + (windowHeight * .2))
			{
				ScrollTo(stepOffset.top, 800);
			}
			else if (stepOffset.bottom < windowTop + (windowHeight * .8))
			{
				if (stepOffset.height < windowHeight * .6)
				{
					ScrollTo(stepOffset.top, 800);
				}
				else
				{
					ScrollTo(stepOffset.bottom - (windowHeight * .8));
				}
			}
		}
		
		// Grab the input available for this step
		self.$scope.find('.step.step-' + currentStep).find('input:not(:checkbox, :radio), textarea, select').filter(':visible').each(function()
		{
			// Check if this is the first input with no value
			if (!$(this).val())
			{
				// Focus on this input and stop looking here
				$(this).focus();
				return false;
			}
		});
	};
}

/*
 * DWR functions
 *****************************************************************/
function CallDWR(path, model, callback)
{
	path = CallDWR._CleanPathString(path);
	
	var blocked = CallDWR.OnBeforeCallbacks.some(function(onBeforeCallback)
	{
		return onBeforeCallback(path, model) === false;
	});
	
	if (blocked) return;
	
	var HandleCallbackData = function(data)
	{
		if (typeof callback == 'function')
		{
			callback(data);
		}
		
		CallDWR.OnAfterCallbacks.forEach(function(onAfterCallback)
		{
			onAfterCallback(path, model, data);
		});
	};
	
	if (path in CallDWR.Overrides)
	{
		CallDWR.Overrides[path](model, function(response)
		{
			HandleCallbackData({ model: response });
		});
	}
	else
	{
		var token = $('input[name="__RequestVerificationToken"]').val();

		$.ajax(
		{
			type: 'POST',
			url: 'http://test.terminix.com/my-account/dwr/jsonp/' + path,
			data: model ? {__RequestVerificationToken: token, jsonData: JSON.stringify(model), } : {__RequestVerificationToken: token},
			timeout: 2 * 60 * 1000,
			complete: function(data)
			{
				var callbackData;
				
				try
				{
					var escapedText = JSON.parse(data.responseText);
					
					if (escapedText.error)
					{
						callbackData = { error: escapedText.error.message };
					}
					else
					{
						callbackData = { model: JSON.parse(escapedText.reply) };
					}
				}
				catch (e)
				{
					callbackData = { error: "An error has occurred." };
					console.log("Error from server request: " + e.message);
				}
				
				HandleCallbackData(callbackData);
			}
		});
		
		/*$.post('/my-account/dwr/jsonp/' + path, model ? { jsonData: JSON.stringify(model) } : {}).always(function(data)
		{
			
		});*/
	}
}

CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];
CallDWR.Overrides = {};

CallDWR._CleanPathString = function(path)
{
	return path.replace(/^\/*|\/*$/g, '');
};

function OnBeforeCallDWR(callback)
{
	CallDWR.OnBeforeCallbacks.push(callback);
}

function OnAfterCallDWR(callback)
{
	CallDWR.OnAfterCallbacks.push(callback);
}

function OverrideDWR(path, callback)
{
	CallDWR.Overrides[CallDWR._CleanPathString(path)] = callback;
}

/*
 * Common functions
 *****************************************************************/
function IsValidEmail(str)
{
	return /^[^\@]+\@([^\.]+\.)+[^\.]+$/.test(str)
}

function IsValidPhoneNumber(str)
{
	return /^(\D*\d\D*){10}$/.test(str);
}

function Redirect(url, ms)
{
	setTimeout(function()
	{
		if (window.location.href == url)
			window.location.reload();
		else
			window.location.href = url;
	}, ms || 800);
}

function IsObject(a)
{
	return Object.prototype.toString.call(a) == '[object Object]';
}

function IsArray(a)
{
	return Object.prototype.toString.call(a) == '[object Array]';
}

function DivideArray(arr, pieces)
{
	if (!arr || !arr.length) return [];
	if (pieces < 1) return arr;
	
	var out = [];
	var start = 0;
	
	for (var i = 1; i <= pieces; i++)
	{
		var stop = (i == pieces) ? arr.length : Math.ceil(arr.length / pieces * i);
		
		out.push(arr.slice(start, stop));
		
		start = stop;
	}
	
	return out;
}

function IsObjectEmpty(a)
{
	for (var i in a)
	{
		return false;
	}
	
	return true;
}

function RemoveAllAlerts() {
	$("#alerts").empty();
}

function CreateAlertsFromMessages(messages, options) {
	if (!messages || !messages.length)
		return;
	
	options = $.extend({
		color: 'red',
		message: '',
		button: false,
		closeButton: true,
		scrollToAlerts: true
	}, options);
	
	messages.forEach(function(message) {
		var color = options.color;
		
		// If it is a string, then it is the message
		// If it is an object, it could be a back-end message object with an "errorMessage" property or "infoMessage" property
		if (typeof message == 'object') {
			color = message.messageColor || color;
			message = message.errorMessage || message.infoMessage || '';
		}
		
		var alertOptions = $.extend({}, options, {
			color: color,
			message: message
		});
		
		CreateAlert(alertOptions);
		
		if (options.scrollToAlerts) {
			options.scrollToAlerts = false;
		}
	});
}

function CreateInformationalMessages(json, options) {
	CreateAlertsFromMessages(json.informationalMessages, $.extend({
		color: 'green'
	}, options));
}

function CreateErrorMessages(json, options) {
	CreateAlertsFromMessages(json.errorMessages, $.extend({
		color: 'red'
	}, options));
}

function ScrollTo(pos, speed, callback)
{
	// TODO: Check if on mobile view that we need to keep what we're scrolling to under the mobile menu
	/*if ($('#mobile-nav-button').is(':visible'))
	{
		// Use the body padding to know how far to show
		pos = Math.max(0, pos - parseInt($('#page-content').css('padding-top')));
	}*/
	
	$('#site-header, #mobile-address-bar').each(function()
	{
		var $element = $(this);
		
		if ($element.css('position') == 'fixed')
		{
			pos = Math.max(0, pos - $element.outerHeight());
		}
	});
	
	$('html, body').animate({ scrollTop: pos + 'px' }, speed, function()
	{
		if (typeof callback === 'function')
			callback();
	});
}

function EmailValidator(emailAccessor, submitter) {
	if (!ko.isObservable(emailAccessor)) {
		throw "emailAccessor must be an observable.";
	}
	if (typeof submitter != 'function') {
		throw "submitter must be a function.";
	}
	
	var self = this;
	
	self.isChoosing = ko.observable(false);
	self.emails = ko.observableArray();
	self.selectedEmail = ko.observable('');
	self.lastValidatedEmail = ko.observable(emailAccessor());
	self.isVerifying = ko.observable(false);
	
	var original = ko.mapping.toJS(self);
	
	self.Reset = function() {
		SetValuesOnModel(self, original);
	};
	
	self.SetDefaultEmail = function(email) {
		self.lastValidatedEmail(email);
		original.lastValidatedEmail = email;
	};
	
	self.KeepEmail = function() {
		self.lastValidatedEmail(emailAccessor());
		self.isChoosing(false);
		
		submitter();
	};
	
	self.UseSelectedEmail = function() {
		var email = self.selectedEmail();
		if (email) {
			emailAccessor(email);
			self.lastValidatedEmail(email);
		}
		
		self.isChoosing(false);
		
		submitter();
	};
	
	self.IsLastValidatedEmail = function() {
		return emailAccessor() == self.lastValidatedEmail();
	};
}

function AddressValidator(addressAccessors, submitter) {
	if (typeof addressAccessors != 'object') {
		throw "addressAccessors must be an object of observables.";
	} else if (!ko.isObservable(addressAccessors.address1)) {
		throw "addressAccessors.address1 must be an observable.";
	} else if (!ko.isObservable(addressAccessors.address2)) {
		throw "addressAccessors.address2 must be an observable.";
	} else if (!ko.isObservable(addressAccessors.city)) {
		throw "addressAccessors.city must be an observable.";
	} else if (!ko.isObservable(addressAccessors.state)) {
		throw "addressAccessors.state must be an observable.";
	} else if (!ko.isObservable(addressAccessors.zipcode)) {
		throw "addressAccessors.zipcode must be an observable.";
	}
	if (typeof submitter != 'function') {
		throw "submitter must be a function.";
	}
	
	var self = this;
	
	self.isChoosing = ko.observable(false);
	self.addresses = ko.observableArray();
	self.selectedAddress = ko.observable('');
	self.lastValidatedAddress = ko.observable(ko.mapping.toJS(addressAccessors));
	self.isVerifying = ko.observable(false);
	
	var original = ko.mapping.toJS(self);
	
	self.Reset = function() {
		SetValuesOnModel(self, original);
	};
	
	self.SetDefaultAddress = function(address) {
		self.lastValidatedAddress(address);
		original.lastValidatedAddress = address;
	};
	
	self.KeepAddress = function() {
		self.lastValidatedAddress(ko.mapping.toJS(addressAccessors));
		self.isChoosing(false);
		
		submitter();
	};
	
	self.UseSelectedAddress = function() {
		var address = self.selectedAddress();
		if (address) {
			SetValuesOnModel(addressAccessors, address);
			self.lastValidatedAddress(address);
		}
		
		self.isChoosing(false);
		
		submitter();
	};
	
	self.IsLastValidatedAddress = function() {
		var address = ko.mapping.toJS(addressAccessors),
			lastValidatedAddress = self.lastValidatedAddress();
		
		return address && lastValidatedAddress
			&& address.address1 == lastValidatedAddress.address1
			&& address.address2 == lastValidatedAddress.address2
			&& address.city == lastValidatedAddress.city
			&& address.state == lastValidatedAddress.state
			&& address.zipcode == lastValidatedAddress.zipcode;
	};
}

/*
 * Find B2C Override Settings message if applicable.
 *******************************************/
function LookupMessage(messageName, callback) {
	CallDWR('MyAccountTMXUIUtils/lookupMessage', { key: messageName }, function(data) {
		if (data.model && data.model.alert) {
			try {
				var alert = JSON.parse(data.model.alert);
				alert.filterId = messageName;
				
				if (messageName === "renewalDiscount") { //ignore stuff like contact update messages
					alert.onCloseAlert = function(e) {
						var jsonData = { messageType: 'TMX_RNL_MSG', serviceLine: messageName };
						CallDWR('MyAccountTMXUIUtils/dismissTMXMyAccountMessage', jsonData, function(data) {});
					}	
				}

				CreateAlert(alert);
			} catch (e) {
				console.log("Error parsing alert in LookupMessage", e);
			}
		}
		
		if (typeof callback == 'function') {
			callback(data);
		}
	});
}

function IsUserLoggedIn(fnCallback)
{
	if (!$.isFunction(fnCallback)) return;
	
	$.post('/my-account/includes/isUserLoggedIn.jsp', {}).always(function(response)
	{
		response = response ? parseInt(response.replace(/^\s*/, '').replace(/\s*$/, '')) : 0;
		
		fnCallback(response === 1);
	});
}

function TextToHTML(str)
{
	return $('<div></div>').text(str).html();
}

function HTMLtoText(str)
{
	return $('<div></div>').html(str).text();
}

function QAS_VerifyEmail(email, fnCallback)
{
	/*setTimeout(function() {
		if (typeof fnCallback == 'function') {
			fnCallback({
				model: { email: email, corrections: ['correction1@gmail.com', 'correction2@gmail.com']}
			});
		}
	}, 1500);*/
	
	CallDWR('MyAccountTMXContactInfoUIUtils/validateEmail', { email: email }, fnCallback);
}

function QAS_VerifyAddress(address, fnCallback)
{
	CallDWR('MyAccountTMXUIUtils/validateAddress', { address: address }, function(data)
	{
		// HACK: Remove alternativeAddresses results that contain null address fields
		if (data.model && data.model.alternativeAddresses && data.model.alternativeAddresses.length)
		{
			data.model.alternativeAddresses = data.model.alternativeAddresses.filter(function(alternativeAddress)
			{
				return alternativeAddress
					&& alternativeAddress.address1 && alternativeAddress.address1 != 'null null'
					&& alternativeAddress.city     && alternativeAddress.city != 'null'
					&& alternativeAddress.state    && alternativeAddress.state != 'null'
					&& alternativeAddress.zipcode  && alternativeAddress.zipcode != 'null';
			});
		}
		
		if (typeof fnCallback == 'function')
		{
			fnCallback(data.model, data.error);
		}
	});
}

function FormatNumber(value, numDecimals, useCommas)
{
	var decimals = '';
	Math.abs(value);
	if (numDecimals > 0)
	{
		for (var i = 0; i < numDecimals; i++)
			decimals += '0';
	
		var decimalFactor = Math.pow(10, numDecimals);
		decimals += (Math.floor(Math.round(value * decimalFactor)) % decimalFactor);
		
		decimals = '.' + decimals.substr(decimals.length - numDecimals);
	}
	var whole = '';
	if(value >= 0){
		whole += Math.floor(value);
	} else {
		whole += Math.ceil(value);
	}
	whole = Math.abs(whole) + "";
	if (useCommas === true)
	{
		whole = whole.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	return whole + decimals;
}

function GetMonthName(month)
{
	if (!this.monthNames)
	{
		this.monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	}
	
	return this.monthNames[(1 <= month && month <= 12) ? month : 0];
}

function FormatPhoneNumber(value)
{
	return value.replace(/(\d{3})(\d{3})(\d{4})/, '$1.$2.$3');
}

function ClearCache()
{
	$.post('/my-account/dwr/jsonp/MyAccountTMXUIUtils/clearCache', '');
}

function GetCookie(name)
{
	return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(name).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
}

function IsEmailCampaignPrecheckAutoPay(campaign)
{
	// Fallback only if param is not provided
	if (campaign === undefined)
	{
		campaign = $.QueryString.utm_campaign || GetCookie("utm_campaign") || '';
	}
	
	if (!campaign) return false;
	
	// cc_reject.* will match cc_reject_new
	// cc_expired.* will match cc_expired_new
	return /^(cc_reject.*|cc_accountupdater.*|acct_updater|cc_expired.*)$/i.test(campaign);
}

(function()
{
	var timeoutAlert = null;
	var timeoutRedirect = null;
	
	function ResetTimeout()
	{
		// CallDWR('MyAccountTMXUIUtils/getMaxInactiveInterval', null, function(data)
		// {
		// 	if (data.error || !data.model)
		// 	{
		// 		setTimeout(ResetTimeout, 1000);
		// 		return;
		// 	}
			
		// 	var maxInterval  = data.model.maxInactiveInterval;
		// 	var warningLimit = data.model.alertTimeInSeconds;
			
		// 	//will get a 2 minute warning.
		// 	if (maxInterval - warningLimit > 0)
		// 	{
		// 		// Clear out existing timers
		// 		timeoutAlert    && clearTimeout(timeoutAlert);
		// 		timeoutRedirect && clearTimeout(timeoutRedirect);
				
		// 		// Create the alert timer
		// 		timeoutAlert = setTimeout(function()
		// 		{
		// 			// Automatically renew session if on local environment
		// 			/*if (/^localhost\./.test(window.location.hostname))
		// 			{
		// 				timeoutAlert = null;
		// 				ResetTimeout();
		// 				return;
		// 			}*/
					
		// 			CreateAlert({
		// 				color: 'yellow',
		// 				message:  'Your session will expire soon.',
		// 				button: {
		// 					text: 'Stay Logged In',
		// 					link: '#'
		// 				},
		// 				onButtonClick: function(e)
		// 				{
		// 					e.preventDefault();
							
		// 					// Remove the alert
		// 					$(this).remove();
							
		// 					ResetTimeout();
		// 				},
		// 				scrollToAlerts: true
		// 			});
					
		// 			timeoutAlert = null;
					
		// 		}, (maxInterval - warningLimit) * 1000);
				
		// 		// Create the redirect timer
		// 		timeoutRedirect = setTimeout(function()
		// 		{
		// 			CallDWR('MyAccountTMXUIUtils/getMaxInactiveInterval', { sessionExpired: true }, function(data)
		// 			{
		// 				var redirect = "/my-account/pages/login.jsp";
						
		// 				if (window.location.hash)
		// 				{
		// 					redirect += "?tab=" + window.location.hash.substring(1);
		// 				}
		// 				else if (window.location.href.indexOf("addProperty.jsp") > 0)
		// 				{
		// 					redirect += "?redirect=/my-account/pages/addProperty.jsp";
		// 				}
						
		// 				window.location = redirect;
		// 			});
					
		// 			timeoutRedirect = null;
					
		// 		}, maxInterval * 1000);
		// 	}
		// });
	}
	
	ResetTimeout.initialize = function()
	{
		OnAfterCallDWR(function(path, model, data)
		{
			if (path !== 'MyAccountTMXUIUtils/getMaxInactiveInterval')
			{
				ResetTimeout();
			}
		});

		ResetTimeout();
	};
	
	window.ResetTimeout = ResetTimeout;
})();

function CreateAlert(options)
{
	options = $.extend(true,
	{
		color: 'red', // red, yellow, green
		icon: 'default', // Set to false to remove icon
		message: '', // Message defaults to TEXT only, no HTML
		messageIsHTML: false, // Set to true for message to be HTML
		button: // Set to false to disable button
		{
			text: '',
			link: '#',
			target: ''
		},
		autoShow: true,
		closeButton: false,
		scrollToAlerts: false,
		onButtonClick: function(e) {}, // this = $alert
		onCloseAlert: function(e) {}, // this = $alert
		filterId: '',
		$module: null
	}, options);
	
	var $alert = $('<div class="alert"></div>').addClass(options.color);
	
	if (options.filterId != '')
	{
		$alert.attr('id', options.filterId);
		
		if ($('#' + options.filterId).length)
		{
			return;
		}
	}
	
	if (options.$module && options.$module.length && options.$module.jquery)
	{
		$alert.data('module', options.$module.attr('name'));
	}
	
	// Desktop button
	if (options.button && IsObject(options.button))
	{
        var $button = $('<a class="btn alert-button hidden-xs"></a>');
		
		$button.attr('href', options.button.link);
		$button.text(options.button.text);
		
		if (options.button.target)
			$button.attr('target', options.button.target);
		
		$button.appendTo($alert);
		
		if (typeof options.onButtonClick === 'function')
		{
			$button.on('click', function(e)
			{
				options.onButtonClick.call($alert, e);
			});
		}
	}

	if (options.icon !== false)
	{
		$alert.addClass('has-icon');
		
		switch (options.icon)
		{
			case 'icon-warning':
			{
				$alert.append('<span class="icon icon-warning"></span>');
				break;
			}
			default:
			{
				$alert.append('<span class="icon">!</span>');
			}
		}
	}
	
	if (options.closeButton === true)
	{
		var $button;
		
		switch (options.customCloseButton)
		{
			case 'termite-renewal':
			{
				$button = $('<a class="btn close termite-renewal" href="#">DISMISS</a>');
				break;
			}
			default:
			{
				$button = $('<a class="btn close" href="#">x</a>');
			}
		}
		
		$button.click(function(e)
		{
			e.preventDefault();
			
			$alert.remove();
			
			if (typeof options.onCloseAlert === 'function')
			{
				options.onCloseAlert.call($alert, e);
			}
		});
		
		$button.appendTo($alert);
	}
	
	var $message = $('<p class="message"></p>').html(options.messageIsHTML ? options.message : TextToHTML(options.message).split("\n").join('<br>'));
	
	if (options.title)
	{
		$message.addClass('has-title');
		$message.prepend($('<span class="title"></span>').text(options.title));
	}
	
	$alert.append($message);
	
	// Mobile button
	if (options.button && IsObject(options.button))
	{
        var $button = $('<a class="btn alert-button visible-xs"></a>');
		
		$button.attr('href', options.button.link);
		$button.text(options.button.text);
		
		if (options.button.target)
			$button.attr('target', options.button.target);
		
		$button.appendTo($alert);
		
		if (typeof options.onButtonClick === 'function')
		{
			$button.on('click', function(e)
			{
				options.onButtonClick.call($alert, e);
			});
		}
	}
    
	$alert.appendTo('#alerts').hide();

	if (options.autoShow)
		$alert.show();
	
	if (options.scrollToAlerts)
		ScrollTo(0, 800);
	
	return $alert;
}

var Viewport = new (function()
{
	var sizes = [{
		name: 'xs',
		minWidth: 0
	}, {
		name: 'sm',
		minWidth: 768
	}, {
		name: 'md',
		minWidth: 992
	}, {
		name: 'lg',
		minWidth: 1200
	}];
	
	var sizeIndexes = {};
	
	sizes.forEach(function(size, index)
	{
		sizeIndexes[size.name] = index;
	});
	
	this.getSize = function()
	{
		var windowWidth = $(window).width();
		var matchingSizes = sizes.filter(function(size) {
			return size.minWidth <= windowWidth;
		});
		
		if (!matchingSizes.length) throw "Could not determine viewport size.";
		
		return matchingSizes.pop().name;
	};
	
	this.compareSize = function(size)
	{
		var argSizeIndex = size in sizeIndexes ? sizeIndexes[size] : -1;
		if (argSizeIndex == -1) throw "Invalid size given: " + size;
		
		var currentSizeIndex = sizeIndexes[this.getSize()];
		
		return (argSizeIndex == currentSizeIndex) ? 0 : ((argSizeIndex < currentSizeIndex) ? -1 : 1);
	};
	
	this.isSize = function(size)
	{
		return this.compareSize(size) == 0;
	};
	
	this.isMobile = function()
	{
		return this.getSize() == 'xs';
	};
})();

var Device = new (function()
{
	this.isMobile = function()
	{
		return /mobi/i.test(window.navigator.userAgent);
	};
})();

/*
 * Knockout Binding Helpers
 **********************************************************************************************/
ko.bindingHelpers = ko.bindingHelpers || {};

ko.bindingHelpers.extendAllBindings = function(allBindings, newBindings)
{
	var bindings = function()
	{
		return $.extend({}, allBindings(), newBindings);
	};
	
	bindings.get = function(name)
	{
		var data = bindings();
		
		return (name in data) ? data[name] : null;
	};
	
	return bindings;
};

	
/*
 * Masked Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.masked = (function()
{
	var EvaluateMask = function(element, allBindings)
	{
		var mask = {
			format: '',
			options: {}
		};
		
		var preset = allBindings().maskedPreset;
		
		switch (preset)
		{
		case 'phone':
			mask.format = '(000) 000-0000';
			break;
		case 'phone-display':
			mask.format = '000.000.0000';
			break;
		case 'zipcode':
			mask.format = '00000';
			break;
		case 'date':
			mask.format = '00/00/0000';
			mask.options = { placeholder: 'MM/DD/YYYY' };
			break;
		case 'card-number':
			mask.format = '0000000000000000';
			break;
		case 'card-security':
			mask.format = '0000';
			break;		
		case 'routing-number':
			mask.format = 'Z00000000';
			mask.options = { translation: { 'Z': { pattern: /[012346789]/ } } };
			break;	
		}
		
		mask.format = allBindings().maskedFormat || mask.format;
		$.extend(mask.options, allBindings().maskedOptions);
		
		return mask;
	};
	
	var DetectElementType = function(element)
	{
		return $(element).is('input, textarea') ? 'value' : 'text';
	};
	
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			var mask = EvaluateMask(element, allBindings);
			var value = valueAccessor();
			
			if (elementType == 'value')
			{
				ko.bindingHandlers.textInput.init(element, valueAccessor, allBindings, viewModel, bindingContext);
				
				// Apply the mask
				$(element).mask(mask.format, mask.options);
			}
			else
			{
				var $element = $(element).text(ko.unwrap(value));
				
				$element.mask(mask.format, mask.options);
			}
			
			// This is so if the observable is changed not by user input, then we can apply the mask to the new value.
			if (ko.isObservable(value))
			{
				value.subscribe(function(x)
				{
					if ($(element).is(':focus'))
					{
						return;
					}
					
					var masked = $(element).masked(x);
					
					if (value() == masked)
					{
						return;
					}
					
					if (elementType == 'value')
					{
						$(element).val(masked);
					}
					else
					{
						$(element).text(masked);
					}
				});
			}
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			var mask = EvaluateMask(element, allBindings);
			var value = valueAccessor();
			
			if (elementType == 'value')
			{
				// Apply the mask
				$(element).mask(mask.format, mask.options);

				if (ko.isObservable(value))
				{
					value($(element).val());
				}
			}
			else
			{
				var masked = $(element).masked(ko.unwrap(value));
				
				$(element).text(masked);
			}
		}
	};
})();


/*
 * Numeric Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.numeric = (function()
{
	var EvaluateMask = function(element, allBindings)
	{
		var options = $.extend({
			decimal: '.',
			negative: false,
			decimalPlaces: 2
		}, allBindings().numericOptions);
		
		// Apply the mask
		$(element).removeNumeric().numeric(options);
	};
	
	var DetectElementType = function(element)
	{
		return $(element).is('input, textarea') ? 'value' : 'text';
	};
	 
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
			
			ko.bindingHandlers[elementType].init(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			
			// Apply the mask
			EvaluateMask(element, allBindings);
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Apply the right default handler
			var elementType = DetectElementType(element);
			
			var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
			
			ko.bindingHandlers[elementType].update(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			
			// Apply the mask
			EvaluateMask(element, allBindings);
			
			// Update the value if it is an input type
			if (elementType == 'value')
			{
				valueAccessor()($(element).val());
			}
		}
	};
})();


/*
 * Credit Card Detect Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.creditCard = (function()
{
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Grab the fields to use
			var options = valueAccessor(),
				fieldInput = options.input,
				fieldResult = options.result,
				classPrefix = options.classPrefix || 'cardtype-';
			
			// Updates the card type
			var UpdateCardType = function()
			{
				// Grab the current value
				var cardNumber = fieldInput();
				
				// Grab the card data
				var cardData = $.extend({ name: '', value: '' }, GetCreditCardType(cardNumber));
				
				// Update the result field
				fieldResult(cardData.value);
				
				// Update the class
				var $element = $(element);
				
				$element.removeClass(classPrefix + $element.data('currentCardType'));
				$element.addClass(classPrefix + cardData.name);
				$element.data('currentCardType', cardData.name);
			};
			
			// Hook when the value changes
			fieldInput.subscribe(UpdateCardType);
			
			// Update with the default card type
			UpdateCardType(fieldInput);
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			//
		}
	};
})();


/*
 * Tooltip Validator Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.tooltipValidator = (function()
{
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			// Grab the fields to use
			var options = valueAccessor(),
				fieldResult = options.result,
				preset = options.preset || '',
				alignment = options.alignment || '';
			
			var tooltipValidatorOptions = options.options || {};
			
			if (preset && preset in TooltipValidator.presets)
			{
				tooltipValidatorOptions = $.extend({}, TooltipValidator.presets[preset], tooltipValidatorOptions);
			}
			
			var $input = $(element);
			var $container;
			
			// Check if we're binding directly to the input
			if ($input.is('input'))
			{
				// Make sure the input has the proper container and tooltip elements
				$container = $input.closest('.field-validate-container');
				
				if (!$container.length)
				{
					$container = $('<div class="field-validate-container"></div>').insertBefore($input).append($input);
					
					// If the tooltip existed after the input before, move it inside the container with the input
					if ($container.next().hasClass('field-validate-tooltip'))
					{
						$container.next().appendTo($container);
					}
				}
				
				// Align the container if needed
				if (alignment)
				{
					$container.attr('data-field-validate-align', alignment);
				}
			}
			// Check if we're binding to the container that already exists
			else if ($input.is('div.field-validate-container'))
			{
				$container = $input;
				
				// Align the container if needed
				if (alignment)
				{
					$container.attr('data-field-validate-align', alignment);
				}
			}
			else
			{
				// Not a valid scenario so don't initialize
				return;
			}
			
			var onAfterValidate = tooltipValidatorOptions.onAfterValidate;
			
			tooltipValidatorOptions.onAfterValidate = function(valid)
			{
				if (typeof fieldResult === 'function')
				{
					fieldResult(valid);
				}
				
				if (typeof onAfterValidate === 'function')
				{
					onAfterValidate(valid);
				}
			};
			
			$container.TooltipValidator(tooltipValidatorOptions);
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			//
		}
	};
})();

	
/*
 * Masked Password Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.maskedPassword = (function()
{
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			/*if ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
			{
				var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
				
				return ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			}*/
				
			$(element).MaskedPassword(
			{
				onChange: function(value)
				{
					valueAccessor()(value);
				}
			});
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			/*if ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
			{
				var newAllBindings = ko.bindingHelpers.extendAllBindings(allBindings, { valueUpdate: 'afterkeydown' });
				
				return ko.bindingHandlers.value.update(element, valueAccessor, newAllBindings, viewModel, bindingContext);
			}*/
		}
	};
})();


/*
 * Focus Input Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.focusInput = (function()
{
	var FocusInput = function(element)
	{
		$(element).find('input:not(:checkbox, :radio), textarea, select').filter(':visible').each(function()
		{
			var $input = $(this);
			
			if (!$input.val())
			{
				$input.focus();
				return false;
			}
		});
	};
	
	var EvalBinding = function(element, value)
	{
		var binding = {
			enabled: false,
			element: element,
			overrideOtherFocus: false
		};
		
		if (typeof value == 'boolean')
		{
			binding.enabled = value;
		}
		else if (typeof value == 'object' && value)
		{
			if (typeof value.enabled == 'boolean')
			{
				binding.enabled = value.enabled;
			}
			
			if (typeof value.overrideOtherFocus == 'boolean')
			{
				binding.overrideOtherFocus = value.overrideOtherFocus;
			}
			
			if (value.element)
			{
				if (value.element.jquery && value.element.length)
				{
					binding.element = value.element.get(0);
				}
				else if (value.element.nodeType === 1)
				{
					binding.element = value.element;
				}
				else if (typeof value.element == 'string')
				{
					binding.element = $(value.element).get(0);
				}
			}
		}
		
		return binding;
	};
	
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			//
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			var binding = EvalBinding(element, valueAccessor());
			
			var focusedElement = $(':focus').filter('input:not(:checkbox, :radio), textarea, select');
			
			if (binding.enabled && (binding.overrideOtherFocus || !focusedElement.length))
			{
				setTimeout(function() { FocusInput(binding.element); }, 10);
			}
		}
	};
})();


/*
 * Submit Form Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.submitForm = (function()
{
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			$(element).on('keydown', function(event)
			{
				// Ignore events marked as bubbled from other bindings
				if (event.originalEvent.submitFormBubbled) return;
				
				
				var submitFormAllowBubble = ko.unwrap(allBindings.get('submitFormAllowBubble')) || false;
				var submitFormOn = allBindings.get('submitFormOn');
				
				// Check if user hit ENTER to submit
				// Make sure user is not focused on something where ENTER is expected
				if (event.keyCode === 13 && $('textarea:focus').length == 0)
				{
					// Check if the submitFormOn param is blocking the binding
					if (!submitFormOn || typeof submitFormOn == 'function' && submitFormOn())
					{
						// Create fake params to send
						var params = [viewModel, event];
						
						// Call the function
						var fn = valueAccessor();
						
						if (typeof fn == 'function')
						{
							fn.apply(bindingContext, params);
						}
						
						// Don't let parent bindings trigger their functions
						if (!submitFormAllowBubble)
						{
							event.originalEvent.submitFormBubbled = true;
						}
					}
				}
			});
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			//
		}
	};
})();


/*
 * Button Progress Custom Binding
 **********************************************************************************************/
ko.bindingHandlers.buttonProgress = (function()
{
	return {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			//
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext)
		{
			if (ko.unwrap(valueAccessor()))
			{
				var options = $.extend({}, allBindings.get('buttonProgressOptions'));
				options.text = allBindings.get('buttonProgressText') || options.text;
				
				$(element).ShowButtonProgress(options);
			}
			else
			{
				$(element).HideButtonProgress();
			}
		}
	};
})();


/*
 * jQuery Plugins
 **************************************************************************************************/
function GetCreditCardType(cardNumber)
{
	var cardDetectMethods = [{
        name: 'visa',
        regex: /^4/,
        value: 'VISA'
    }, {
        name: 'mastercard',
        regex: /^(5[1-5]|2(2(2[1-9][0-9]{2}|[3-9])|[3-6]|7([01]|20[0-9]{2})))/,
        value: 'MC'
    }, {
        name: 'americanexpress',
        regex: /^3[47]/,
        value: 'AMEX'
    }, {
        name: 'discover',
        regex: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5])|64[4-9]|65)/,
        value: 'DISC'
    }];
	
	for (var i = 0; i < cardDetectMethods.length; i++)
    {
        if (cardDetectMethods[i].regex.test(cardNumber))
        {
        	var method = cardDetectMethods[i];
        	
        	return { name: method.name, value: method.value };
        }
    }
	
	return null;
}

$.fn.DetectCreditCardType = function(options)
{
	options = $.extend({
		$input: $(),
		$resultInput: null,
		classPrefix: 'cardtype-',
		OnResult: function(name, value) {} // this = this from $().DetectCreditCardType
	}, options);
	
    return this.each(function()
    {
        var $container = $(this);
        
        var $input = options.$input;
        
        if (!$input || !$input.length)
        {
        	if ($container.is('input, textarea'))
        		$input = $container;
        	else
        		return;
        }
        
        var UpdateCardType = function()
        {
            $container.removeClass(options.classPrefix + $container.data('currentCardType'));
            
            var cardNumber = $input.val() || '';
            var cardData = $.extend({ name: '', value: '' }, GetCreditCardType(cardNumber));
            
            if (cardData.name)
            {
            	$container.addClass(options.classPrefix + cardData.name);
            	$container.data('currentCardType', cardData.name);
            }
            
            if (options.$resultInput && options.$resultInput.length)
            {
            	options.$resultInput.val(cardData.value);
            }
            
            if (typeof options.OnResult === 'function')
            {
            	options.OnResult.call($container.get(0), cardData.name, cardData.value);
            }
        };
        
        $input.on('change input keypress paste', function(e)
        {
            UpdateCardType();
        });
        
        UpdateCardType();
    });
};

jQuery.QueryString = (function(paramsArray)
{
	var data = {};
	
	for (var i = 0; i < paramsArray.length; i++)
	{
		var pieces = paramsArray[i].split('=');
		
		data[pieces[0]] = (pieces.length > 1) ? decodeURIComponent(pieces[1].replace(/\+/g, ' ')) : true;
	}
	
	return data;
	
})(window.location.search.substring(1).split('&'));

jQuery.fn.ShowButtonProgress = function(options)
{
	options = $.extend({
		text: 'Loading...',
		autosize: true
	}, options);
	
	return this.each(function()
	{
		var $button = $(this);
		
		if ($button.data('in-progress')) return;
		
		// Create container and button
		var $progressContainer = $('<div class="button-progress-container">'), $progressButton = $('<div>');
		
		$progressContainer.append($progressButton);
		
		// Copy over styles to button
		$progressButton.attr('class', $button.attr('class'));
		$progressButton.addClass('show-progress');
		$progressButton.attr('style', $button.attr('style'));
		$progressButton.text(options.text || 'Loading...');
		
		// Copy position styles to container and remove any from the button
		var positionStyles = {
			'margin-bottom': 0,
			'margin-left': 0,
			'margin-right': 0,
			'margin-top': 0,
			'bottom': 0,
			'left': 0,
			'right': 0,
			'top': 0,
			'float': 'none',
			'position': 'relative',
			'transform': 'none'
		};
		
		$button.hide(); // Need to hide to be able to access "auto" margins in FireFox
		
		for (var rule in positionStyles)
		{
			$progressContainer.css(rule, $button.css(rule));
			$progressButton.css(rule, positionStyles[rule]);
		}
		
		$button.show(); // Show again because we need width/height sizes from below
		
		// Grab size of real button so container can match
		var buttonWidth = $button.outerWidth(), buttonDisplay = $button.css('display');
		
		if (options.autosize)
		{
			$progressContainer.height($button.outerHeight());
		}
		
		// Show the container and hide the original button
		$progressContainer.insertBefore($button).show();
		$button.hide();
		
		// Size the container properly
		$progressContainer.css('display', 'inline-block');
		
		if (options.autosize)
		{
			buttonWidth = Math.max(buttonWidth, $progressContainer.outerWidth());
			
			$progressContainer.css('width', buttonWidth + 'px').css('display', buttonDisplay);
		}
		
		// Set that the button has progress
		$button.data('in-progress', true);
	});
};

jQuery.fn.HideButtonProgress = function()
{
	return this.each(function()
	{
		var $button = $(this);
		
		if (!$button.data('in-progress')) return;
		
		$button.prev().remove();
		$button.show();
		
		$button.data('in-progress', false);
	});
};



/*
 * Feature Toggle Functions
 *******************************************************************************************************/
function GetFeatureToggle(featureName, defaultValue) {
	if (typeof g_FeatureToggleMap == 'undefined' || !g_FeatureToggleMap) {
		window.g_FeatureToggleMap = {};
		
		if (typeof g_FeatureToggleList != 'undefined' && g_FeatureToggleList && g_FeatureToggleList.length > 0) {
			g_FeatureToggleList.forEach(function(toggle) {
				g_FeatureToggleMap[toggle.featureName] = toggle.enabled;
			});
		}
	}
	
	return g_FeatureToggleMap.hasOwnProperty(featureName) ? g_FeatureToggleMap[featureName] : defaultValue;
}

function GetFeatureToggleDefaultFalse(featureName) {
	return GetFeatureToggle(featureName, false);
}

function GetFeatureToggleDefaultTrue(featureName) {
	return GetFeatureToggle(featureName, true);
}

ko.applyBindingsToElement = function(data, element) {
	if (element === undefined) return;
	
	if (element.jquery && element.length > 1) {
		return element.each(function() {
			ko.applyBindingsToElement(data, this);
		});
	}
	
	if (element.jquery) {
		element = element.get(0);
		if (element === undefined) return;
	}
	
	var $element = $(element);
	if ($element.data('--knockout-bound')) return;
	
	ko.applyBindings(data, element);
	
	$element.data('--knockout-bound', true);
};

ko.extenders.async = function(target, options) {
	
	/*
	 * Options:
	 *
	 * ajaxOptions (optional)
	 * - An object of options to pass to the AJAX call
	 * - These options fit into the jQuery.ajax syntax
	 *
	 * autoload (default: false)
	 * - true will attempt to AJAX in the data on creation
	 * - false will let it wait to AJAX until the observable is called for a value
	 *
	 * data
	 * - Any data to send along in the AJAX
	 *
	 * dwr (default: false)
	 * - true will prepend '/my-account/dwr/jsonp/' to the url and url should be the 'UIUtils/method' name only
	 *
	 * getter (optional)
	 * - Function to call instead of calling AJAX. Function will be called only if data is not already loaded, or on reset
	 *
	 * ignoreResetTimeout (default: false)
	 * - true will not reset the session timeout with the alert popup
	 *
	 * json (default: false)
	 * - true will attempt to parse the reply from the AJAX call as JSON
	 * - false will return the reply from the AJAX call in its raw data
	 *
	 * onBeforeLoad function()
	 * - Called before the AJAX call happens
	 * - If the function returns false, the AJAX call will not be called
	 *
	 * onError function(error) (optional)
	 * - Callback after the AJAX has an error where error is some sort of JS error text
	 *
	 * onResult function(data)
	 * - Callback after the AJAX is finished where data is the response
	 * - If onError is not supplied, then this function will be called with data set to null
	 *
	 * url
	 * - The URL for the AJAX to load data from
	 */
	
	options = $.extend({
		ajaxOptions : {},
		autoload: false,
		data : '',
		dwr : false,
		ignoreResetTimeout: false,
		json : false,
		onBeforeLoad : /* function(){} */null,
		onError : /* function(errorText){} */null,
		onResult : function(data) {
		},
		url : '',
	}, options);

	options.ajaxOptions = $.extend({
		url : (options.dwr ? '/my-account/dwr/jsonp/' : '') + options.url,
		data : options.data
	}, options.ajaxOptions);

	var init = false;
	
	var waitingOnBeforeLoad = false;

	var ajax = function() {
		// Check if we're already waiting
		if (waitingOnBeforeLoad) return;
		
		// Check if we should block loading
		if (typeof options.onBeforeLoad == 'function') {
			var onBeforeLoad = ko.computed(options.onBeforeLoad);
			
			if (onBeforeLoad() === false) {
				waitingOnBeforeLoad = true;
				
				onBeforeLoad.subscribe(function(readyToLoad) {
					if (readyToLoad !== false) {
						onBeforeLoad.dispose();
						waitingOnBeforeLoad = false;
						
						ajax();
					}
				});
				return;
			}
			
			onBeforeLoad.dispose();
		}

		init = true;
		
		if (typeof options.getter == 'function') {
			options.getter();
		} else {
			$.ajax(options.ajaxOptions).always(function(jqXHR) {
				var error = false;
				
				try {
					var data = JSON.parse(jqXHR.responseText).reply;
	
					if (options.json) {
						data = JSON.parse(data);
					}
				} catch (e) {
					error = e.message;
					
					if (typeof options.onError == 'function') {
						options.onError(e.message);
					} else {
						options.onResult(null);
					}
				}
				
				// Call the onResult here so if there are errors, we don't catch them in the try/catch above
				// Let the onResult handler handle their own errors
				if (!error) {
					options.onResult(data);
				}
				
				if (!options.ignoreResetTimeout && typeof ResetTimeout == 'function') {
					ResetTimeout({ cache: true });
				}
			});
		}
	};
	
	if (options.autoload) {
		ajax();
	}

	var result = ko.pureComputed({
		read : function() {
			if (!init) {
				ajax();
			}
			return target();
		},
		write : target
	}).extend({
		notify : 'always'
	});

	result.reset = ajax;

	return result;
};

function SetValuesOnModel(model, data) {
	if (!data) return;

	for (var key in data) {
		if (model.hasOwnProperty(key)) {
			if (ko.isObservable(model[key])) {
				if (ko.isWriteableObservable(model[key])) {
					model[key](ko.isObservable(data[key]) ? data[key]() : data[key]);
				}
			}
			else if (typeof model[key] == 'object' && typeof data[key] == 'object') {
				SetValuesOnModel(model[key], data[key]);
			}
		}
	}
}

/**
 * Simultaneously run all promises in the array and then resolve when all have
 * resolved or reject if at least 1 rejected
 */
function PromiseList(promises) {
	return new Promise(function(resolve, reject) {
		if (!promises || !promises.length) {
			resolve();
			return;
		}
		
		var completed = 0;
		
		promises.forEach(function(promise) {
			promise.then(function() {
				if (++completed == promises.length) {
					resolve();
				}
			}).catch(reject);
		});
	});
}

/**
 * options is an object of params that matches the $.ajax function
 *
 * the resolve function takes the data from the response
 * the reject function takes an error message param from $.ajax().fail()
 */
function PromiseAjax(options) {
	options = $.extend({
		dataType: 'text',
		type: 'POST'
	}, options);

	return new Promise(function(resolve, reject) {
		$.ajax(options).done(function(data, textStats, jqXHR) {
			resolve(data);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			reject(errorThrown);
		});
	});
}

/**
 * options is an object of params:
 * - url: path to the DWR function
 * - data: data to send to DWR. this will be transformed to { jsonData: JSON.stringify(data) }. set json param to false to not do this
 * - json: true/false - whether to stringify the data - default is true
 *
 * if options is a string, it will be treated as the URL. the data will be null and json will be true
 */
function PromiseDWR(options) {
	if (typeof options == 'string') {
		options = { url: options };
	}

	options.url = CallDWR._CleanPathString(options.url);

	return new Promise(function(resolve, reject) {
		var blocked = CallDWR.OnBeforeCallbacks.some(function(onBeforeCallback) {
			return onBeforeCallback(path, model) === false;
		});

		if (blocked) return;
		
		var HandleCallbackData = function(data) {
			resolve(data);
			
			CallDWR.OnAfterCallbacks.forEach(function(onAfterCallback) {
				onAfterCallback(options.url, options.data, { model: data });
			});
		};
		
		if (options.url in CallDWR.Overrides) {
			CallDWR.Overrides[options.url](options.data, HandleCallbackData);
		} else {
			debugger;
			PromiseAjax({
				// url: "/my-account/dwr/jsonp/" + options.url,
			    // url:"https://test.terminix.com/my-account/dwr/jsonp/" + options.url,
				url:"https://gitlab.com/aiemohankumar/terminix/raw/master/" + options.url,
				type: 'GET',
				data: options.json === false ? options.data : { jsonData: JSON.stringify(options.data) }
			}).then(function(response) {
				
			//	var json = JSON.parse(JSON.parse(response).reply);
				var json = JSON.parse(response).reply;
				
				return { data: json };
			}).catch(function(errorThrown) {
				reject("An error has occurred.");
				
				return null;
			}).then(function(response) {
				if (response) {
					HandleCallbackData(response.data);
				}
			});
		}
	});
}

function AsyncWrapper(data, options, isArray) {
	var observable = isArray ? ko.observableArray(data) : ko.observable(data);
	var wrapper = observable.extend({ async: options });

	//data.asyncWrapper = wrapper;
	
	wrapper.loaded = (data && ko.isObservable(data.loaded)) ? data.loaded : ko.observable(false);
	wrapper.error  = (data && ko.isObservable(data.error))  ? data.error  : ko.observable();
	
	wrapper.getData = function() {
		return new Promise(function(resolve, reject) {
			if (wrapper.loaded()) {
				resolve(observable());
			}
			else {
				var subscriptionLoaded = wrapper.loaded.subscribe(function(loaded) {
					if (loaded) {
						subscriptionLoaded.dispose();
						subscriptionError.dispose();
						
						resolve(observable());
					}
				});
				
				var subscriptionError = wrapper.error.subscribe(function(error) {
					if (error) {
						subscriptionLoaded.dispose();
						subscriptionError.dispose();
						
						reject(error);
					}
				});

				wrapper();
			}
		});
	};

	return wrapper;
}

function AsyncWrapperArray(data, options) {
	return AsyncWrapper(data, options, true);
}

function GetCityAndStateFromZip(zip) {
	return GetFeatureToggleDefaultTrue("enableSmartyZip") ? SmartyStreetsGetCityAndState(zip) : ZiptasticGetCityAndState(zip);
}

function ZiptasticGetCityAndState(zip) {
	return PromiseAjax({
		type: 'GET',
		url: 'https://zip.getziptastic.com/v2/US/' + zip,
		dataType: 'json'
	}).then(function(response) {
		return { city: response.city, state: response.state_short };
	});
}

function SmartyStreetsGetCityAndState(zip) {
	return PromiseAjax({
		type: 'GET',
		url: '/rest/model/svm/services/rest/SmartyStreetZipCodeActor/getCityAndStateFromZip/?zipCode=' + zip,
		dataType: 'json'
	}).then(function(response) {
		if (!response.data.validResponse) throw "Not a valid response.";
		
		return { city: response.data.city, state: response.data.state };
	});
}

/*
 * LiveChat Availability Checker
 *******************************************************************************************************/
(function() {
	// interval between chat availability in milliseconds
	var LIVECHAT_AVAILABILITY_CHECK_INTERVAL = 1500;
	
	var livechatAvailabilityCallbacks = [];
	var isLivechatHooked = false;
	var livechatCheckTimer;
	
	function PromiseIsChatAvailable() {
		return new Promise(function(resolve, reject) {
			if (typeof LIVECHAT_GROUP == 'undefined' || !LIVECHAT_GROUP) {
				reject("LIVECHAT_GROUP is not defined.");
			} else {
				PromiseDWR({
					url: "MyAccountTMXLiveChatUIUtils/isChatAvailable",
					data: { groups: [LIVECHAT_GROUP.id] }
				})
				.then(function(data) {
					data.isVisitorEngaged = LC_API.visitor_engaged();
					resolve(data);
				})
				.catch(reject);
			}
		});
	}
	
	function RunChatAvailabilityCheck() {
		livechatCheckTimer = undefined;
		
		PromiseIsChatAvailable().then(function(data) {
			LIVECHAT_AVAILABILITY_CHECK_INTERVAL = data.nextCallInterval;
			
			livechatAvailabilityCallbacks.forEach(function(callback) {
				callback(data);
			});
		}).catch(function(error) {
			console.log("[RunChatAvailabilityCheck] Error checking if chat is available:", error);
		}).then(function() {
			livechatCheckTimer = setTimeout(RunChatAvailabilityCheck, LIVECHAT_AVAILABILITY_CHECK_INTERVAL);
		});
	}
	
	window.IsChatAvailableChecker = function(callback) {
		if (typeof callback != 'function') {
			console.log("[IsChatAvailableChecker] Error: Callback param is not a function.");
			return;
		}
		
		livechatAvailabilityCallbacks.push(callback);
		
		if (!isLivechatHooked) {
			isLivechatHooked = true;
			
			//LiveChat_OnLoaded(RunChatAvailabilityCheck);
		}
		
		var removed = false;
		
		return {
			remove: function() {
				if (removed) return;
				removed = true;
				
				var index = livechatAvailabilityCallbacks.indexOf(callback);
				if (index >= 0) {
					livechatAvailabilityCallbacks.splice(index, 1);
				}
				
				if (!livechatAvailabilityCallbacks.length) {
					if (livechatCheckTimer) {
						clearTimeout(livechatCheckTimer);
						livechatCheckTimer = undefined;
					}
					
					isLivechatHooked = false;
				}
			}
		};
	};
	
	window.PromiseIsChatAvailable = PromiseIsChatAvailable;
})();

/*
 * MyAccount Navigation
 *******************************************************************************************************/
$(function()
{
	$('#myaccount-nav > ul > li > a').on('click', function(e)
	{
		e.preventDefault();
		
		var $link = $(this), $li = $link.closest('li');
		
		if ($li.hasClass('active'))
			return;
		
		if ($li.hasClass('logout'))
		{
			$('#logout-form [type=submit]').click();
			return;
		}
		
		var target = $link.attr('href');
		
		if (target.charAt(0) == '#' && target.length > 1)
		{
			TAB.Load(target.substring(1));
		}
	});
	
	$('#site-header .mobile-menu-icon').on('click', function(e)
	{
		e.preventDefault();
		
		$('body').toggleClass('mobile-nav-menu');
	});
	
	$('#mobile-nav-menu > ul > li > a').on('click', function(e)
	{
		var $link = $(this), $li = $link.closest('li');
		
		if ($li.hasClass('normal-link'))
			return;
		
		e.preventDefault();
		
		if ($li.hasClass('active'))
			return;
		
		if ($li.hasClass('logout'))
		{
			$('#logout-form [type=submit]').click();
			return;
		}
		
		var target = $link.attr('href');
		
		if (target.charAt(0) == '#' && target.length > 1)
		{
			TAB.Load(target.substring(1));
		}
		
		$('body').removeClass('mobile-nav-menu');
	});
	
	// Footer copyright year
	$('#site-footer .current-year').text(new Date().getFullYear());
});