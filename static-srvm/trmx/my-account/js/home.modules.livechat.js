/*
 * Module: LiveChat
 ***********************************************************************************************************/
TAB.Modules.Add('livechat', function($module, options, onLoaded) {
	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		callback : function(self) {
			PromiseDWR("MyAccountTMXUIUtils/preLoadLivechatData").then(function(livechatData) {
				if (!livechatData.isCommercial) {
					var initiatedProactiveChat = false;
					
					// If feature toggle is turned OFF, then set initiated to true so it isn't initiated later
					if (!GetFeatureToggleDefaultFalse("renewalProactiveChat")) {
						initiatedProactiveChat = true;
					}
					
					var checker = IsChatAvailableChecker(function(data) {
						if (self.IsModuleOnPage() && LiveChat_IsEnabledFor(LiveChat_TabToSection(TAB.GetCurrentTab()))) {
							if (data.areAgentsAvailable || data.isVisitorEngaged) {
								self.isLiveChatEnabled(true);
								
								LiveChat_SetCustomVariables({
									customerId: livechatData.contractNumber
								});
								
								if (livechatData.hasTermiteRenewal && !initiatedProactiveChat) {
									initiatedProactiveChat = true;
									LiveChat_OpenChat();
								}
								
								TAB.Modules.API('*', 'setLiveChatEnabled', true);
							} else {
								self.isLiveChatEnabled(false);
								
								TAB.Modules.API('*', 'setLiveChatEnabled', false);
							}
						} else {
							checker.remove();
						}
					});
				}
			}).catch(function(error) {
				console.log("Error preloading LiveChat data:", error);
			});
		}
	});

	ko.applyBindings(vm, $module.get(0));

	onLoaded();
});