/*
 * Module: Payment History
 ***********************************************************************************************************/
TAB.Modules.Add('payment-history', function($module, options, onLoaded)
{
	var vm = ViewModel_CreateGenericForModule(
	{
		$module: $module,
		moduleOptions: options,

		LoadDataDWR: 'MyAccountTMXPaymentUIUtils/getPaymentHistory',
		
		callback: function(self)
		{
			self.GetViewableHistory = function()
			{
				return self.model().history().slice(0, self.model().numViewable());
			};

			self.CanViewMore = function()
			{
				if (!self.model())
					return false;
				
				return self.model().numViewable() < self.model().history().length;
			};

			self.CanViewLess = function()
			{
				if (!self.model() || !self.originalModel())
					return false;
				
				return self.model().numViewable() != self.originalModel().numViewable();
			};

			self.ViewMore = function()
			{
				self.model().numViewable(Math.min(self.model().numViewable() + 5, self.model().history().length));
				
				return self;
			};

			self.ViewLess = function()
			{
				self.model().numViewable(self.originalModel().numViewable());
				
				return self;
			};
			
			self.GoToMyAccount = function()
			{
				TAB.Load('my-account');
				
				return self;
			};
		}
	});
	
	ko.applyBindings(vm, $module.get(0));
	
	vm.LoadData(onLoaded);
});