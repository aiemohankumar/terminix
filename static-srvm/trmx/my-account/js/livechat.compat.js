// Create all necessary variables and function in case
// LiveChat is not really loaded then these fake functions/vars will work

window.__LiveChatHandlers             = window.__LiveChatHandlers             || [];
window.LC_API                         = window.LC_API                         || {};
window.LiveChat_OnLoaded              = window.LiveChat_OnLoaded              || function(){ __LiveChatHandlers.push(Array.prototype.slice.call(arguments, 0)); };
window.LiveChat_GetToggles            = window.LiveChat_GetToggles            || function(){ return {}; };
window.LiveChat_InitPage              = window.LiveChat_InitPage              || function(){};
window.LiveChat_MyAccount_GetTabGroup = window.LiveChat_MyAccount_GetTabGroup || function(){ return 0; };
window.LiveChat_IsEnabledFor          = window.LiveChat_IsEnabledFor          || function(){ return false; };
window.LiveChat_OpenChat              = window.LiveChat_OpenChat              || function(){};
window.LiveChat_SetCustomVariables    = window.LiveChat_SetCustomVariables    || function(){};
window.LiveChat_TabToSection          = window.LiveChat_TabToSection          || function(){return '';};