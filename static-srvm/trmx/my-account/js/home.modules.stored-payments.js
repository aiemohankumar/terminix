/*
 * Module: Stored Payments
 ***********************************************************************************************************/
TAB.Modules.Add('stored-payments', function($module, options, onLoaded) {
	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		LoadDataDWR : 'my-account-json/getStoredPaymentMethods.json',

		api : {
			refreshStoredPaymentMethods : function(self, apiData) {
				var UpdateStoredPaymentMethods = function(storedPaymentMethods) {
					// Find if there are any new stored payment methods to add
					if (!self.model())
						return;
					
					storedPaymentMethods.forEach(function(paymentMethod) {

						if (typeof self.model().storedPaymentMethods === 'function') {
							var existingPaymentMethod = ko.utils.arrayFirst(self.model().storedPaymentMethods(), function(existingPaymentMethod) {
								return existingPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId;
							});
							paymentMethod.submitting = false;
							paymentMethod._original = $.extend(true, {}, paymentMethod);

							if (!existingPaymentMethod) {
								self.model().storedPaymentMethods.push(ko.mapping.fromJS(paymentMethod));
							}else {
								self.model().storedPaymentMethods.replace(existingPaymentMethod , ko.mapping.fromJS(paymentMethod))
							}
						}
					});
				};

			/*	if (typeof apiData.storedPaymentMethods != 'undefined') {
					UpdateStoredPaymentMethods(apiData.storedPaymentMethods);
				} else {*/
					CallDWR('my-account-json/getStoredPaymentMethods.json', {}, function(data) {
						if (data.error) {
							// What do.
							return;
						}

						// Check if there is no array to work with
						if (!data.model.storedPaymentMethods || !IsArray(data.model.storedPaymentMethods)) {
							// What do.
							return;
						}

						UpdateStoredPaymentMethods(data.model.storedPaymentMethods);
					});
				/*}*/
			}
		},

		callback : function(self) {
			var UpdateModel = self.UpdateModel;

			self.UpdateModel = function(json) {
				// Add properties that aren't on the back-end
				json.addPaymentMethod.submitting = false;
				json.addPaymentMethod._original = $.extend(true, {}, json.addPaymentMethod);

				json.storedPaymentMethods.forEach(function(item) {
					item.submitting = false;
					item._original = $.extend(true, {}, item);
				});

				UpdateModel.call(self, json);
			};

			self.SetEditMode = function(item) {
				self.RemoveAllErrors(item);

				item.modifyStep('edit');
			};

			self.SetRemoveMode = function(item) {
				self.RemoveAllErrors(item);

				item.modifyStep('remove');

				if (item.removeError()) {
					item.errorMessages.push({
						errorMessage : item.removeError()
					});
				}
			};
			
			self.OpenAddForm = function(type) {
				var item = self.model().addPaymentMethod;
				
				self.CancelAddForm(item);
				item.type(type);
			};

			self.CancelAddForm = function(item) {
				item.modifyStep('');
				item.type(null);

				self.RemoveAllErrors(item);

				self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

				// Scroll back to the payment method selection
				var position = $module.find('.add-payment-method').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};

			self.CancelEditForm = function(item) {
				item.modifyStep('');

				self.RemoveAllErrors(item);

				self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

				// Scroll back to the table row
				var position = $module.find('.stored-payment-methods').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};

			self.CancelRemoveForm = function(item) {
				item.modifyStep('');

				self.RemoveAllErrors(item);

				// Scroll back to the table row
				var position = $module.find('.stored-payment-methods').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}
			};

			self.GetAvailableCreditCardYears = function() {
				var years = [];

				for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
					years.push(i);
				}

				return years;
			};

			self.SetVisibleHelp = function(item, field) {
				item.visibleHelp(field);

				return self;
			};

			self.IsVisibleHelp = function(item, field) {
				return item.visibleHelp() == field;
			};

			self.IsValidPaymentMethod = function(item) {
				switch (item.type()) {
				case 'card':
					// Only validate card number when adding
					// We cannot edit a card number on an existing stored
					// payment method
					// Require billing address when adding a card
					if (!item.paymentMethodId())
					{
						if (!item.cardNumber()
						|| !item.billingAddress1()
						|| !item.billingCity()
						|| !item.billingState()
						|| !item.billingZipcode())
							return false;
					}

					if (!item.cardExpireMonth() || !item.cardExpireYear())
						return false;
					break;

				case 'bank':
					if (!item.bankRoutingNumber() || !item.bankAccountNumber())
						return false;
					break;
				}

				return true;
			};

			self.SubmitAddForm = function(item) {
				if (!self.IsValidPaymentMethod(item) || item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				CallDWR('MyAccountTMXPaymentUIUtils/addStoredPaymentMethod', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						// Reset add payment method form
						self.ApplyModelChanges(item, ko.mapping.toJS(item._original));

						// Grab the new list of stored payment methods
						CallDWR('my-account-json/getStoredPaymentMethods.json', {}, function(data) {
							if (data.error) {
								// What do.
								return;
							}

							if (!data.model.storedPaymentMethods || !data.model.storedPaymentMethods.length) {
								// What do.
								return;
							}

							// Update the Stored Payments section with the new
							// stored payment method
							TAB.Modules.API('*', 'refreshStoredPaymentMethods', {
								storedPaymentMethods : data.model.storedPaymentMethods
							});
							
						});
					}
				});
			};

			self.SubmitEditForm = function(item) {
				if (!self.IsValidPaymentMethod(item) || item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				CallDWR('MyAccountTMXPaymentUIUtils/editStoredPaymentMethod', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						// Update table with payment method changes
						item.modifyStep('');

						// Update the Due Payments section with the new stored
						// payment method
						TAB.Modules.API('*', 'refreshStoredPaymentMethods');
						
					}
				});
			};

			self.SubmitRemoveForm = function(item) {
				if (item.submitting())
					return;

				self.RemoveAllErrors(item);

				item.submitting(true);

				var json = ko.mapping.toJS(item);

				CallDWR('MyAccountTMXPaymentUIUtils/removeStoredPaymentMethod', json, function(data) {
					item.submitting(false);

					if (data.error) {
						item.errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					// Update model
					self.ApplyModelChanges(item, data.model);

					// Check if it was successful
					if (item.informationalMessages().length) {
						// Show success alert
						var informationalMessages = ko.mapping.toJS(item.informationalMessages);

						informationalMessages.forEach(function(message, messageIndex) {
							CreateAlert({
								color : 'green',
								message : message.infoMessage,
								button : false,
								closeButton : true,
								scrollToAlerts : messageIndex == 0,
								$module: $module
							});
						});

						// Update table with payment method changes
						self.model().storedPaymentMethods.remove(item);

						// Update the Due Payments section with the removed
						// stored payment method
						TAB.Modules.API('*', 'refreshStoredPaymentMethods');
						
					}
				});
			};
		}
	});

	ko.applyBindings(vm, $module.get(0));

	vm.LoadData(onLoaded);
});