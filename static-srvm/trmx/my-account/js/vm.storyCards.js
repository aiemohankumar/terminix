// ref: http://stackoverflow.com/a/1293163/2343
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray(strData, strDelimiter, hasHeaders) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp((
        // Delimiters.
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

        // Quoted fields.
        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

        // Standard fields.
        "([^\"\\" + strDelimiter + "\\r\\n]*))"
    ), "gi");

    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;

    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);
        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[2]) {
            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[2].replace(new RegExp("\"\"", "g"), "\"");
        } else {
            // We found a non-quoted value.
            strMatchedValue = arrMatches[3];
        }

        // Now that we have our value string, let's add
        // it to the data array.
        arrData[arrData.length - 1].push(strMatchedValue);
    }
    
    // If there are headers, then convert each result into an object
    if (hasHeaders && arrData.length) {
    	var headers = arrData.shift();
    	
    	arrData = arrData.map(function(data) {
    		// Convert to an object with the headers
    		var obj = {};
    		
    		headers.forEach(function(header, index) {
    			if (!Object.prototype.hasOwnProperty.call(obj, header)) {
    				obj[header] = (index < data.length) ? data[index] : '';
    			} else if (index < data.length) {
    				obj[header] = '' + obj[header] + ',' + data[index];
    			}
    		});
    		
    		return obj;
    	});
    }

    // Return the parsed data.
    return arrData;
}

function ViewModel() {
	var self = this;

	ViewModel_AddCommonFunctions(self);

	self.supportedBrowser = typeof FileReader != 'undefined' && 'WebkitLineClamp' in document.documentElement.style;
	
	var supportedFileTypes = [
		'text/csv',
		'application/vnd.ms-excel'
	];
	
	var storyCardFieldMap = {
		"key": ["Key", "Issue key"],
		"issueType": ["Issue Type"],
		"storyPoints": ["Story Points", "Custom field (Story Points)"],
		"summary": ["Summary"],
		"fixVersion": ["Fix Version/s"],
		"linkedIssues": ["Linked Issues"],
		"epicLink": ["Epic Link", "Custom field (Epic Link)"],
		"components": ["Component/s"],
		"isObjective": ["Related to a PI Objective", "Custom field (Related to a PI Objective)"],
		"isUnplannedPIWork": ["Unplanned PI Work", "Custom field (Unplanned PI Work)"],
		"isUnplannedSprintWork": ["Unplanned Sprint Work", "Custom field (Unplanned Sprint Work)"]
	};
	
	var storyCardFields = Object.keys(storyCardFieldMap);
	
	var componentsMap = {
		"AHS B2C" : "AHS",
		"HSA MyAccount" : "HSA",
		"TMX MyAccount" : "TMX"
	};
	
	function Story(data) {
		// Populate story from data object
		for (var field in storyCardFieldMap) {
			var key = storyCardFieldMap[field].find(function(key) {
				return data.hasOwnProperty(key) && data[key];
			});
			
			this[field] = key && data[key] || '';
		}
		
		// Format the fix version
		this.fixVersionDate = this.fixVersion;
		
		if (this.fixVersion) {
			// Parse out the fix version into a date
			if (/^\d{8}/.test(this.fixVersion)) {
				// Fix version is in "YYYYMMDD" format
				var month = this.fixVersion.substr(4, 2),
					day   = this.fixVersion.substr(6, 2);
				
				this.fixVersionDate = parseInt(month) + '/' + parseInt(day);
			} else {
				console.log("Unrecognized fix version format: " + this.fixVersion);
			}
		}
		
		// Make sure story points are whole numbers
		this.storyPoints = parseInt(this.storyPoints) || '';
		
		// Convert Yes/No to observables
		this.isObjective = this.isObjective == "Yes" || this.isObjective == "yes";
		this.isUnplannedPIWork = this.isUnplannedPIWork == "Yes" || this.isUnplannedPIWork == "yes";
		this.isUnplannedSprintWork = this.isUnplannedSprintWork == "Yes" || this.isUnplannedSprintWork == "yes";
		
		// Convert linked issues to an array
		this.linkedIssues = ('' + this.linkedIssues).split(',').map(function(linkedIssue) {
			return linkedIssue.trim();
		}).filter(function(linkedIssue) {
			return !!linkedIssue;
		});
		
		// Convert components to an array
		this.components = ('' + this.components).split(',').map(function(component) {
			component = component.trim();
			
			return component in componentsMap ? componentsMap[component] : component;
		}).filter(function(component) {
			return !!component;
		});
	}
	
	self.file = ko.observable();
	self.stories = ko.observableArray();
	self.errorMessage = ko.observable();
	
	self.unsupportedFile = ko.observable();
	
	self.designs = [
		//{ name: 'Default', value: 'default' },
		{ name: 'New Design 1', value: '1' }
	];
	
	self.selectedDesign = ko.observable(self.designs[0].value);
	self.rememberDesign = ko.observable(false);
	
	self.selectedDesignTemplateName = ko.computed(function() {
		return "ko-template-story-design-" + self.selectedDesign();
	});
	
	// Check if there is a remembered design
	var design = GetCookie("storyCardDesign");
	if (design) {
		var foundDesign = self.designs.find(function(x) {
			return design == x.value;
		});
		
		if (foundDesign) {
			self.selectedDesign(design);
			self.rememberDesign(true);
		}
	}
	
	// When their selected design changes, update the cookie
	self.selectedDesign.subscribe(function(value) {
		if (self.rememberDesign()) {
			document.cookie = "storyCardDesign=" + encodeURIComponent(value);
		}
	});
	
	// When the remember flag changes, update the cookie
	self.rememberDesign.subscribe(function(value) {
		if (value) {
			document.cookie = "storyCardDesign=" + encodeURIComponent(self.selectedDesign());
		} else {
			var expires = new Date();
			expires.setMilliseconds(expires.getMilliseconds() - 86400000);
			
			document.cookie = "storyCardDesign=; expires=" + expires.toUTCString();
		}
	});
	
	// Print options
	self.printOptions = {
		spaceBetweenCards: ko.observable(true),
		referenceKey: ko.observable(true)
	};
	
	try {
		// Try to parse the print options from the cookie
		var printOptions = JSON.parse(GetCookie("storyCardPrintOptions"));
		
		// If successfully parsed the print options
		if (printOptions && typeof printOptions == 'object') {
			// Loop through each option on the object
			Object.keys(printOptions).forEach(function(key) {
				// Check if this option exists on the model
				if (self.printOptions.hasOwnProperty(key) && ko.isObservable(self.printOptions[key])) {
					// Store the saved option on the model
					self.printOptions[key](printOptions[key]);
				}
			});
		}
	}
	catch (e) {}
	
	self.HandleFiles = function(data, event) {
		// Reset fields
		self.file(null);
		self.errorMessage("");
		self.unsupportedFile(null);
		
		// Check if there is a selected file
		if (event.target.files && event.target.files.length) {
			// Check if the type is supported
			var file = event.target.files[0];
			
			if (supportedFileTypes.indexOf(file.type) >= 0) {
				self.file(file);
				
				// Remove all stories since a new file was chosen
				self.stories.removeAll();
			} else {
				self.unsupportedFile({
					type: file.type,
					extension: file.name.replace(/^.*(\.[^\.]+)$/, '$1')
				});
			}
			
			// Remove the selected file
			$(event.target).val('');
		}
	};
	
	self.UploadFile = function(data, event) {
		// If submitting or no file uploaded, don't allow
		if (self.submitting() || !self.file()) {
			return;
		}
		
		// Set as submitting
		self.submitting(true);
		
		// Reset the stories
		self.stories.removeAll();
		self.errorMessage("");
		self.unsupportedFile(null);
		
		// Try to load the file
		var reader = new FileReader();
		
		reader.onerror = function(event) {
			// Set as no longer submitting
			self.submitting(false);
		};
		
		reader.onload = function(event) {
			// Set as no longer submitting
			self.submitting(false);
			
			// Remove the file from the upload field since it completed
			self.file(null);
			
			// Create the stories array
			var data = CSVToArray(event.target.result, ",", true);
			
			// Check if there is data to be shown
			if (data.length) {
				// Convert data to story objects
				var stories = data.map(function(item) {
					return new Story(item);
				}).filter(function(story) {
					return story && story.key;
				});
				
				if (!stories.length) {
					// Set that no stories were found
					self.errorMessage("No stories were found");
				} else {
					// Validate that the file had all required fields
					var missingFields = storyCardFields.filter(function(field) {
						return !stories[0].hasOwnProperty(field);
					});
					
					if (missingFields.length) {
						// Show that some fields are missing
						self.errorMessage("The following fields were missing from the file: '" + missingFields.join("', '") + "'");
					} else {
						self.stories(stories);
					}
				}
			} else {
				self.errorMessage("No data was found from the file.");
			}
		};
		
		reader.readAsText(self.file());
	};
	
	self.SetSelectedDesign = function(data, event) {
		// If submitting or no stories selected or data is not a design object, don't allow
		if (self.submitting() || !self.stories().length || !data.value) {
			return;
		}
		
		// Validate the design choice
		var foundDesign = self.designs.find(function(x) {
			return x.value == data.value;
		});
		
		if (!foundDesign) {
			return;
		}
		
		// Update the selected design
		self.selectedDesign(foundDesign.value);
	};
	
	self.Print = function(data, event) {
		// If submitting or no stories selected, don't allow
		if (self.submitting() || !self.stories().length) {
			return;
		}
		
		// Save the print options
		document.cookie = "storyCardPrintOptions=" + encodeURIComponent(JSON.stringify(ko.mapping.toJS(self.printOptions)));
		
		// Print the cards
		window.print();
	};
}

$(function() {
	window.vm = new ViewModel();

	ko.applyBindings(vm);
});
