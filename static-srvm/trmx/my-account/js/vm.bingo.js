function ViewModel() {
	var self = this;

	ViewModel_AddCommonFunctions(self);

	self.supportedBrowser = typeof FileReader != 'undefined';
	
	var supportedFileTypes = {
		Image: [
	  		'image/png',
	  		'image/jpeg'
		],
		List: [
	  		'text/plain',
			'text/csv',
			'application/vnd.ms-excel'
		]
	};
	
	var ORIENTATION = self.ORIENTATION = {
		LANDSCAPE: 'landscape',
		PORTRAIT: 'portrait'
	};
	
	self.cardTitle = ko.observable('');
	
	self.sizeOptions = [3,5,7,9,11,13].map(function(size) {
		return {
			text: size + 'x' + size,
			value: size
		};
	});
	
	self.selectedSize = ko.observable(5);
	
	self.useFreeSpace = ko.observable(true);
	self.freeSpaceImage = ko.observable();
	self.freeSpaceImageOrientation = ko.observable(ORIENTATION.LANDSCAPE)

	self.numCardsToPrint = ko.observable(1);
	self.parsedNumCardsToPrint = ko.computed(function() {
		return parseInt(self.numCardsToPrint());
	});
	
	self.printMasterSheet = ko.observable(false);

	self.unsupportedFileImage = ko.observable();
	self.unsupportedFileList = ko.observable();
	self.isReadingFile = ko.observable(false);
	
	self.itemsInput = ko.observable('');
	
	self.itemsList = ko.computed(function() {
		// Parse out the items input
		var itemsList = self.itemsInput().split("\n");
		
		// Trim all extra spaces and remove blank lines
		itemsList = itemsList.map(function(item) {
			return ('' + item).trim();
		}).filter(function(item) {
			return item != '';
		});
		
		// Give back the array
		return itemsList;
	});
	
	self.minimumItemsRequired = ko.computed(function() {
		return (self.selectedSize() * self.selectedSize()) - (self.useFreeSpace() ? 1 : 0);
	});
	
	self.errorMessage = ko.observable();
	self.cards = ko.observableArray();
	
	self.HandleFilesImage = function(data, event) {
		// Reset fields
		self.unsupportedFileImage(null);
		self.freeSpaceImage(null);
		
		// Check if there is a selected file
		if (event.target.files && event.target.files.length) {
			// Check if the type is supported
			var file = event.target.files[0];
			
			if (supportedFileTypes.Image.indexOf(file.type) >= 0) {
				// Try to read the file
				var reader = new FileReader();
				
				reader.onerror = function(event) {
					// Set as not reading anymore
					self.isReadingFile(false);
				};
				
				reader.onload = function(event) {
					// Figure out the orientation
					var img = new Image();
					
					img.onerror = function() {
						// Set as not reading anymore
						self.isReadingFile(false);
					};
					
					img.onload = function() {
						// Set as not reading anymore
						self.isReadingFile(false);
						
						if (img.width >= img.height) {
							self.freeSpaceImageOrientation(ORIENTATION.LANDSCAPE);
						} else {
							self.freeSpaceImageOrientation(ORIENTATION.PORTRAIT);
						}
						
						// Put the image in the page
						self.freeSpaceImage(event.target.result);
					};
					
					img.src = event.target.result;
				};
				
				reader.readAsDataURL(event.target.files[0]);
			} else {
				self.unsupportedFileImage({
					type: file.type,
					extension: file.name.replace(/^.*(\.[^\.]+)$/, '$1')
				});
			}
			
			$(event.target).val('');
		}
	};
	
	self.RemoveFreeSpaceImage = function(data, event) {
		self.freeSpaceImage(null);
	};
	
	self.HandleFilesList = function(data, event) {
		// Reset fields
		self.unsupportedFileList(null);
		
		// Check if there is a selected file
		if (event.target.files && event.target.files.length) {
			// Check if the type is supported
			var file = event.target.files[0];
			
			if (supportedFileTypes.List.indexOf(file.type) >= 0) {
				// Set as reading the file
				self.isReadingFile(true);
				
				// Try to read the file
				var reader = new FileReader();
				
				reader.onerror = function(event) {
					// Set as not reading anymore
					self.isReadingFile(false);
				};
				
				reader.onload = function(event) {
					// Set as not reading anymore
					self.isReadingFile(false);
					
					// Create the stories array
					var data = event.target.result.split("\n");
					
					// Check if there is data to be shown
					if (data.length) {
						// Trim all lines and remove empty lines
						data = data.map(function(line) {
							return ('' + line).trim();
						}).filter(function(line) {
							return line != '';
						});
						
						// Populate the input with the lines
						self.itemsInput(data.join("\n"));
					}
				};
				
				reader.readAsText(event.target.files[0]);
			} else {
				self.unsupportedFileList({
					type: file.type,
					extension: file.name.replace(/^.*(\.[^\.]+)$/, '$1')
				});
			}
			
			$(event.target).val('');
		}
	};
	
	self.Print = function(data, event) {
		if (self.isReadingFile()) {
			return;
		}
		
		// Reset everything
		self.errorMessage('');
		self.unsupportedFileImage(null);
		self.unsupportedFileList(null);
		self.cards.removeAll();
		
		// Check if we don't have any to print
		if (!self.parsedNumCardsToPrint() || self.parsedNumCardsToPrint() < 1) {
			self.errorMessage('The minimum number of cards to print is 1.');
			return;
		}
		
		// Check if we have the minimum requirement of items
		if (self.itemsList().length < self.minimumItemsRequired()) {
			self.errorMessage('You are missing ' + (self.minimumItemsRequired() - self.itemsList().length) + ' item(s) to fill out the bingo card.');
			return;
		}
		
		// Generate the cards list
		var cardTitle = self.cardTitle(),
			size = self.selectedSize(),
			itemsList = self.itemsList(),
			useFreeSpace = self.useFreeSpace(),
			freeSpaceIndex = Math.floor(size / 2);
		
		for (var i = self.parsedNumCardsToPrint(); i > 0; i--) {
			// Get a randomly sorted items list
			var items = itemsList.slice();
			items.sort(function(a, b) {
				return Math.random() < 0.5 ? -1 : 1;
			});
			
			// Create the card object
			var card = {
				cardTitle: cardTitle,
				rows: []
			};
			
			// Populate the rows
			for (var j = 0; j < size; j++) {
				var row = [];
				
				for (var k = 0; k < size; k++) {
					// Check for free space
					if (useFreeSpace && j == freeSpaceIndex && k == freeSpaceIndex) {
						row.push({
							isFreeSpace: true,
							freeSpaceImage: self.freeSpaceImage(),
							freeSpaceImageOrientation: self.freeSpaceImageOrientation()
						});
					} else {
						var x = Math.floor(Math.random() * items.length);
						
						row.push({
							isFreeSpace: false,
							text: items[x]
						});
						
						items.splice(x, 1);
					}
				}
				
				card.rows.push(row);
			}
			
			// Add card to list
			self.cards.push(card);
		}
		
		// Open the print view
		window.print();
	};
}

$(function() {
	window.vm = new ViewModel();

	ko.applyBindings(vm);
});
