/*
 * Module: Easy Pay
 ***********************************************************************************************************/
TAB.Modules.Add('yia-pay', function($module, options, onLoaded) {

	var vm = ViewModel_CreateGenericForModule({
		$module : $module,
		moduleOptions : options,

		// LoadDataDWR : 'MyAccountTMXPaymentUIUtils/getPaymentDueData',

		api : {

			setPaymentDueData : function(self, json) {
				self.UpdateModel(json);

				if (self.model().redirectURL()) {
					Redirect(self.model().redirectURL());
					return;
				}

				self.modelLoaded(true);

				onLoaded();
			},

			refreshStoredPaymentMethods : function(self, apiData) {
				var UpdateStoredPaymentMethods = function(storedPaymentMethods) {
					// Find if there are any new stored payment methods to add

					storedPaymentMethods.forEach(function(paymentMethod) {
						if (typeof self.model().paymentMethods === 'function') {
							var existingPaymentMethod = ko.utils.arrayFirst(self.model().paymentMethods(), function(existingPaymentMethod) {
								return existingPaymentMethod.paymentMethodId() == paymentMethod.paymentMethodId;
							});

							if (!existingPaymentMethod) {
								paymentMethod.submitting = false;
								paymentMethod._original = $.extend(true, {}, paymentMethod);

								self.model().paymentMethods.push(ko.mapping.fromJS(paymentMethod));
							}
						}
					});
				};

				if (typeof apiData.storedPaymentMethods != 'undefined') {
					UpdateStoredPaymentMethods(apiData.storedPaymentMethods);
				} else {
					CallDWR('my-account-json/getStoredPaymentMethods.json', {}, function(data) {
						if (data.error) {
							// What do.
							return;
						}

						// Check if there is no array to work with
						if (!data.model.storedPaymentMethods || !IsArray(data.model.storedPaymentMethods)) {
							// What do.
							return;
						}

						UpdateStoredPaymentMethods(data.model.storedPaymentMethods);
					});
				}
			}
		},

		callback : function(self) {
			// Backwards compatibility for when LoadDataDWR is turned off
			self.LoadData = self.LoadData || function() {
			};

			self.UpdateModel = function(json) {
				// Add properties that aren't on the back-end
				json.submitting = false;
				json.showDiscountDetail = false;
				
				if (json.dueYiaPayments) {
					json.dueYiaPayments.forEach(function(item) {
						item.submitting = false;
						item.modifyStep = '';
						item.acceptedTerms = false;

						item._original = $.extend(true, {}, item);
					});
				}
				
				var model = ko.mapping.fromJS(json);

				self.model(model);
				self.originalModel(model);

				if (json.selectedPaymentMethod) {
					var paymentMethods = self.model().paymentMethods();

					for (var i = 0; i < paymentMethods.length; i++) {
						var paymentMethod = paymentMethods[i];

						if (paymentMethod.type() == json.selectedPaymentMethod.type && json.selectedPaymentMethod.paymentMethodId == paymentMethod.paymentMethodId()) {
							self.ApplyModelChanges(paymentMethod, json.selectedPaymentMethod);

							self.selectedPaymentMethod(paymentMethod);
							break;
						}
					}
				}
				self.model().yiaPayment(true);
			};

			self.ShowDetail = function(item) {
				item.showDetail(!item.showDetail());

				return self;
			};

			self.ShowDiscountDetail = function() {
				self.model().showDiscountDetail(!self.model().showDiscountDetail());
				return self;
			};

			self.HasEligibleSalesAgreements = function() {
				return self.model() && self.model().dueYiaPayments().length > 0;
			};
			self.HasSelectedAgreements = function() {
				var bool = false;
				if (self.model() && self.model().dueYiaPayments().length > 0) {
					self.model().dueYiaPayments().forEach(function(item) {
						if (item.yiaPaySelected()) {
							bool = true;
						}
					});
				}
				return bool;
			}
			self.YiaDiscountSum = function() {
				var amt = 0;
				if (self.model() && self.model().dueYiaPayments().length > 0) {
					self.model().dueYiaPayments().forEach(function(item) {
						if (item.yiaPaySelected()) {
							amt += item.yiaDiscAmt();
						}
					});
				}
				return amt;
			}

			self.PaymentTotal = function() {
				var amt = 0;
				if (self.model() && self.model().dueYiaPayments().length > 0) {
					self.model().dueYiaPayments().forEach(function(item) {
						if (item.yiaPaySelected()) {
							amt += item.yiaTotalAmt();
						}
					});
				}
				return amt;
			};

			self.GetAvailableCreditCardYears = function() {
				var years = [];

				for (var i = (new Date()).getFullYear(), stop = i + 10; i < stop; i++) {
					years.push(i);
				}

				return years;
			};

			self.selectedPaymentMethod = ko.observable(null);

			self.selectedPaymentMethod.subscribe(function(value) {
				self.RemoveAllErrors();
			});

			self.sortedPaymentMethods = ko.computed(function() {
				if (!self.model())
					return [];

				var paymentMethods = self.model().paymentMethods();

				return paymentMethods.slice(0).sort(function(a, b) {
					var typeA = a.type(), typeB = b.type();

					if (typeA == typeB)
						return ('' + a.description()).localeCompare('' + b.description());
					if (typeA == 'saved' || typeA == '')
						return -1;
					if (typeB == 'saved' || typeB == '')
						return 1;
					if (typeA == 'bank')
						return -1;
					return 1;
				});
			});

			self.UpdateSelectedPaymentMethod = function() {
				self.selectedPaymentMethod(null);

				if (self.model().selectedPaymentMethod.type()) {
					var selectedPaymentMethod = self.model().selectedPaymentMethod;

					self.model().paymentMethods().forEach(function(paymentMethod) {
						if (paymentMethod.type() == selectedPaymentMethod.type() && paymentMethod.description() == selectedPaymentMethod.description()) {
							self.selectedPaymentMethod(paymentMethod);
						}
					});
				}
			};

			self.SetVisibleHelp = function(field) {
				self.model().visibleHelp(field);

				return self;
			};

			self.IsVisibleHelp = function(field) {
				return self.model().visibleHelp() == field;
			};

			self.CanSetupEasyPay = function() {
				// Check if we're paying for something
				var duePaymentsBeingPaid = self.model().duePayments().filter(function(duePayment) {
					return duePayment.amount() > 0;
				});

				if (duePaymentsBeingPaid.length == 0) {
					return false;
				}

				// Check if paying more than one sales agreement
				var salesAgreements = duePaymentsBeingPaid.map(function(duePayment) {
					return duePayment.salesAgreementNumber();
				});

				salesAgreements = salesAgreements.filter(function(salesAgreement, index) {
					return salesAgreements.indexOf(salesAgreement) == index;
				});

				if (salesAgreements.length > 1) {
					return false;
				}

				// Check if the sales agreement being paid for is eligible for
				// EasyPay
				return duePaymentsBeingPaid[0].isEligibleForEasyPay();
			};

			self.IsValidPaymentMethod = function() {
				if (!self.selectedPaymentMethod())
					return false;

				var item = self.model();

				switch (self.selectedPaymentMethod().type()) {
				case 'saved':
					return true;

				case 'card':
					if (!item.cardNumber()
					// || !item.cardSecurity() // Disabled from TMA-349
					|| !item.cardExpireMonth() || !item.cardExpireYear() || !item.billingAddress1() || !item.billingCity() || !item.billingState()
							|| !item.billingZipcode())
						return false;
					break;

				case 'bank':
					if (!item.bankRoutingNumber() || !item.bankAccountNumber())
						return false;
					break;
				}

				return true;
			};

			self.CancelPayment = function(m, e) {
				self.RemoveAllErrors();

				// Empty out other fields that may have been populated
				var fieldsToRevert = [];
				if (self.selectedPaymentMethod()) {
					switch (self.selectedPaymentMethod().type()) {
					case 'saved': {
						fieldsToRevert = [ 'visibleHelp' ];
						break;
					}
					case 'onetime': {
						fieldsToRevert = [ 'visibleHelp', 'cardNumber', 'cardType', 'cardSecurity', 'cardExpireMonth', 'cardExpireYear', 'billingAddress1',
								'billingAddress2', 'billingCity', 'billingState', 'billingZipcode' ];
						break;
					}
					case 'bank': {
						fieldsToRevert = [ 'visibleHelp', 'bankRoutingNumber', 'bankAccountNumber', 'billingAddress1', 'billingAddress2', 'billingCity',
								'billingState', 'billingZipcode' ];
						break;
					}
					}

					fieldsToRevert.forEach(function(field) {
						self.model()[field](self.originalModel()[field]());
					});

					self.selectedPaymentMethod(null);
				}

				// Scroll back to payment method choices for mobile view
				var position = $module.find('.payment-summary').offset().top;

				if (position < $(window).scrollTop()) {
					ScrollTo(position, 400);
				}

				return self;
			};

			self.SubmitPayment = function(m, e) {
				if (!self.IsValidPaymentMethod() || self.submitting())
					return;

				self.RemoveAllErrors();

				var json = ko.mapping.toJS(self.model);

				json.selectedPaymentMethod = ko.mapping.toJS(self.selectedPaymentMethod);

				self.submitting(true);

				CallDWR('MyAccountTMXPaymentUIUtils/makeYiaPayment', json, function(data) {
					self.submitting(false);

					if (data.error) {
						self.model().errorMessages.push({
							errorMessage : data.error
						});
						return;
					}

					json = data.model;

					self.UpdateModel(json);

					// HACK: Since we have subscribers removing all messages
					// when some fields change like:
					// - due payment amount being paid changes
					// - payment method changes
					// - payment is cancelled
					// So we have to update the messages again after we
					// UpdateModel because those fields above will trigger
					// and will remove all of the messages from the response

					self.model().errorMessages(ko.unwrap(ko.mapping.fromJS(json.errorMessages)));
					self.model().fieldValidationErrors(ko.unwrap(ko.mapping.fromJS(json.fieldValidationErrors)));
					self.model().informationalMessages(ko.unwrap(ko.mapping.fromJS(json.informationalMessages)));

					// END OF HACK

					if (self.model().redirectURL()) {
						setTimeout(function() {
							Redirect(self.model().redirectURL());
						}, 1500);
					}

					if (self.model().informationalMessages().length) {
						// Grab what the informational messages are
						var informationalMessages = ko.mapping.toJS(self.model().informationalMessages());

						// Reload the entire payments tab
						TAB.Reload(function() {
							// Create the success messages once it has loaded
							informationalMessages.forEach(function(infoMessage, index) {
								CreateAlert({
									color : 'green',
									message : infoMessage.infoMessage,
									button : false,
									closeButton : true,
									scrollToAlerts : (index == 0), // scroll to
																	// alerts
																	// only for
																	// first
																	// infoMessage
																	// so we
																	// don't
																	// scroll
																	// every
																	// message
																	// created
									$module : $module
								});
							});
						});
					}
				});

				return self;
			};
		}
	});

	ko.applyBindings(vm, $module.get(0));

	vm.LoadData(onLoaded);
});