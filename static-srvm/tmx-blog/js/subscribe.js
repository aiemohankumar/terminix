/*
 * Subscribe Block & Modal behavior
 */
function subscribeModalDisplay() {
    var $modal = $('#subscribeModal'),
        $modalBody = $modal.find('.modal-dialog');


    if ($.cookie('subscribeModalDisplay') == null) {

        if ($.cookie('subscribeModalShowTimer') != "true") {
            var date = new Date();
            date.setTime(date.getTime() + (60 * 1000));
            console.log(date);
            $.cookie('subscribeModalShowTimer', "true", {
                expires: date,
                path: '/'
            });
        }

        var subscribeModalShowCheck = setInterval(function () {
            if ($.cookie('subscribeModalShowTimer') == null) {
                $modal.modal('show');
                console.log('subscribeModalShowTimer end');
                $.cookie('subscribeModalDisplay', 'canceled', {
                    expires: 1,
                    path: '/'
                });
                clearInterval(subscribeModalShowCheck);
            }
        }, 1000);



        $modal.find("form").on('submit', function () {
            $.cookie('subscribeModalDisplay', 'subscribed', {
                expires: 365,
                path: '/'
            });
            //console.log('subscribe modal subscribed');
        });
        $modal.find(".close").on('click', function () {
            $.cookie('subscribeModalDisplay', 'canceled', {
                expires: 1,
                path: '/'
            });
            //console.log('subscribe modal canceled');
        });

        $modal.on('click', function (e) {
            if (!$modalBody.is(e.target) && $modalBody.has(e.target).length === 0) {
                $.cookie('subscribeModalDisplay', 'canceled', {
                    expires: 1,
                    path: '/'
                });
            }
        });
    }

    if ($.cookie('subscribeModalDisplay') == 'subscribed') {
        $('body').find('section.subscribe-block').each(function (e) {
            $(this).remove();
        });
    }

    $modal.on('shown.bs.modal', function () {
        $('html').addClass('modal-open');
    });
    $modal.on('hidden.bs.modal', function () {
        $('html').removeClass('modal-open');
    });


}

$(document).ready(function () {

    //Subscription functionality
    subscribeModalDisplay();

});
