function videodataloadModal() {
    self.sortBy = ko.observable("new");
    self.count = ko.observable(10);

    self.onRecordsPerPageChange = function (count) {
        self.count(count);
        getVideoData();
    }

    self.onSort = function (sortBy) {
        self.sortBy(sortBy);
        getVideoData();
    }

    self.getVideoData = function () {
        $.ajax({
            url: "http://dev-terminix.com/api/sitecore/Videos/VideoSortandFilter?sortBy=" + sortBy() + "&count=" + count(),
            method: "GET",
            success: function (json) {
                $('#blog_video_data').html("");
                $('#blog_video_data').append($.parseHTML(json));
            },
            error: function (e) {}
        })
    }
    return {
        getVideoData: getVideoData
    }
}

$(document).ready(function () {
    var sm = videodataloadModal();
    ko.applyBindings(sm, document.querySelector("html"));
    sm.getVideoData();
});
