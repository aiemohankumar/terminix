function viewModal() {
    var self = this;
    if ($('#lazyLoad').data('totalcount') != null) {
        self.totalCount = $('#lazyLoad').data('totalcount');
    } else {
        self.totalCount = 1000;
    }

    self.catid = $('.category-content').data('catid');
    self.index = 0;
    self.next = 5;
    //self.totalNoOfRecords = ko.observable(0);
    //self.currentPage = ko.observable(1);
    //self.isSearchInitiated = ko.observable(false);

    //self.totalCount = 87;
    //self.catid = "010C8991-71DF-443C-9AB3-AAD74D6BBDB0";
    //self.index = -5;
    //self.next = 5;
    self.totalNoOfRecords = ko.observable(0);
    self.currentPage = ko.observable(1);
    self.isSearchInitiated = ko.observable(false);
    self.isFacetVideoAvailable = ko.observable(false);
    //self.isResultAvailable = ko.observable(true);
    self.searchKeyword = ko.observable("");
    self.searchKeywordLink = function () {
        return "/blog/search?Ntt=" + self.searchKeyword();
    };
    self.lazySearchEnabled = ko.observable(false);

	self.qArray = ko.observableArray([]);
	
    searchFilter = {
        sortBy: ko.observable('new'),        
		searchKey: ko.observable($('#search-term-val').data('val')),		
        pageNo: ko.observable(0),
        recordsPerPage: ko.observable(10),
		recordsSize: ko.observable(10),
        facets: ko.observable('tmx-article'),
        q: ko.observable(''),
        qc: ko.observable('')
    };

    self.getSearchLink = function(){
        var getLink;
        var currentNttValue;
        var urlRemain;
        var ntt = "Ntt=";
        var searchWithQuestion  = "search?";
        var search = "search";
        var currentUrl = window.location.href;

        if(currentUrl.indexOf(searchWithQuestion) != -1){
            urlRemain = currentUrl.substring(currentUrl.indexOf(searchWithQuestion) + searchWithQuestion.length);
        } else if(currentUrl.indexOf(search) != -1){
            urlRemain = currentUrl.substring(currentUrl.indexOf(search) + search.length);
        }
        var urlPartArray = urlRemain.split('&');
        for(var i = 0; i < urlPartArray.length; i++ ){
            if(urlPartArray[i].indexOf(ntt) != -1){
                currentNttValue = urlPartArray[i].substring(ntt.length);
            }
        }
        if(currentNttValue == $('.input-hm-search').val()){
            getLink = $('.btn-hm-search').attr("data-link-template").toString().replace( /%7BKEYWORD%7D/ , $('.input-hm-search').val());
        }else{
            getLink = "/blog/search?Ntt=" + $('.input-hm-search').val();
        }
        return getLink;
    }

    
    this.numberOfPages = ko.computed(function () {
        return Math.ceil(totalNoOfRecords() / searchFilter.recordsPerPage());
    }, this);

    paginationOptions = {
        first: ko.observable(false),
        last: ko.observable(false),
        prev: ko.observable(false),
        next: ko.observable(false),
        range: ko.observableArray([])
    }

    self.lazyLoadRecurringItems = function () {
        console.log("print lazyload")
        if (totalCount != null && index != null) {
            if (index >= totalCount) {
                return;
            } else {
                index = index + 5;
                next = index + 5;
            }
        }
        $(".lazyLoadRecurringContainer.new").viewportChecker({
            offset: -100,
            classToAddForFullView: "",
            callbackFunction: function (element) {
                var $that = $(element),
                    getLink = $that.attr("data-recurringlink"),
                    $spinner = $("#load-spinner");
                $spinner.addClass("is-visible");
                $.ajax({
                    type: "GET",
                    //   url: "/api/sitecore/Blog/LazyLoadCatBlogs?cat=" + catid + "&index=" + index + "&next=" + next, 
                    url: "http://10.220.9.218/api/sitecore/Blog/LazyLoadCatBlogs?cat=" + catid + "&index=" + index + "&next=" + next,
                    success: function (json) {
                        $spinner.removeClass("is-visible");
                        $that.after('<div class="lazyLoadRecurringContainer new"></div>').removeClass("new").append($.parseHTML(json));
                        lazyLoadRecurringItems()
                    },
                    error: function (e) {}
                })
            }
        })
    }

    self.setFacets = function (facet) {
        self.searchFilter.facets(facet);
        self.getBlogData();
    }

    self.setPaging = function () {
        (currentPage() > 3) ? paginationOptions.first(true): paginationOptions.first(false);
        (currentPage() < numberOfPages() - 2) ? paginationOptions.last(true): paginationOptions.last(false);

        (currentPage() > 1) ? paginationOptions.prev(true): paginationOptions.prev(false);
        (currentPage() < numberOfPages()) ? paginationOptions.next(true): paginationOptions.next(false);

        let startIndex = 1;
        let endIndex = numberOfPages();
        paginationOptions.range([]);

        if (numberOfPages() > 5) {
            //if current page less than 3
            endIndex = 5;
            if (currentPage() >= numberOfPages() - 2) {
                startIndex = numberOfPages() - 4;
                endIndex = numberOfPages();
            }
            if (currentPage() <= numberOfPages() - 3 && currentPage() > 3) {
                startIndex = currentPage() - 2;
                endIndex = currentPage() + 2;
            }
        }
        for (let i = startIndex; i <= endIndex; i++)
            paginationOptions.range.push(i);
    }

    self.onPageClick = function (page) {
        currentPage(page);
        searchFilter.pageNo(page-1);
		if(page-1 == numberOfPages()){
			searchFilter.recordsSize(totalNoOfRecords()%searchFilter.recordsPerPage());
		}else{
			searchFilter.recordsSize(searchFilter.recordsPerPage());
		}
        getBlogData();
    }

    self.onSort = function (data) {
        searchFilter.sortBy(data);
        getBlogData();
    }

    self.onRecordsPerPageChangeBlog = function (recordsPerPage) {
         if (recordsPerPage == 'All')
		 {
            //searchFilter.recordsPerPage(10);
            searchFilter.recordsSize(10);
            searchFilter.pageNo(-1);
            lazySearchEnabled(true);
            $(".search-result-content").html("");
            lazyLoadSearchItems();

		 }
         else{
		 	searchFilter.pageNo(0);
            searchFilter.recordsPerPage(recordsPerPage);
			searchFilter.recordsSize(searchFilter.recordsPerPage());
            getBlogData();
		 }
    }

    self.onSearch = function (data, event) {
        if (event.which == 13){
            debugger;
           // var testLink = "/blog/search?Ntt=pestcontrol";
           // var testLink = "http://10.220.10.25/blog/search?Ntt=pestcontrol";
                var testLink = "http://10.220.9.218/blog/search?Ntt=" + searchFilter.searchKey();
               history.pushState(null, null, testLink);
        
            //var link =  getSearchLink();      
           // history.pushState(null, null, link);
        
            isSearchInitiated(true);
            searchFilter.pageNo(0);
            searchFilter.facets('tmx-article');
            getBlogData();
        }
    }

    self.onSearchClear = function () {
        searchFilter.searchKey('');
        searchFilter.pageNo(0);
        searchFilter.facets('tmx-article');
        getBlogData();
    }

    self.onCategoryTagClick = function (q, qc) {
        if(qArray().includes(q)){
            qArray.remove(q);
        }
        else{
            qArray.push(q);
        }
       
        let qString = "";
        for(var i = 0; i<qArray().length; i++){
            if(i == qArray().length - 1)
                qString += qArray()[i];
            else
                qString += qArray()[i] + "|";
        }
        searchFilter.q(qString);
        searchFilter.qc(qc);
		$(".label-tag__remove").show();
        getBlogData();
    }
	
	self.onCategoryClear = function() {
        qArray([]);
        searchFilter.q("");
        searchFilter.qc("");
        getBlogData();
		$(".label-tag__remove").hide();		 
    }

    self.getBlogData = function () {
        lazySearchEnabled(false);
        $("#load-spinner").addClass("is-visible");
		
		$("#load-spinner-lazyload").removeClass("is-visible");
		
        var $scrollTarget, $content,
            offsetHeight = $('div').is('.search-result-navbar') ? -150 : -200;
        $scrollTarget = $('.search-result-navbar');
        $content = $('.tab-content');
        $content.css('opacity', '0.7');
        $('#blog_data').css('opacity', '0.7');

        let url = "";
        if(searchFilter.q() != "" && searchFilter.facets()!= "tmx-video")
            url =  "http://10.220.9.218/api/sitecore/Search/SearchLazyKeywordLoad?qc=" + searchFilter.qc() + "&q=" + searchFilter.q() + "&res=" + searchFilter.recordsSize() + "&pageNo=" + searchFilter.pageNo() + "&sortBy=" + searchFilter.sortBy()+"&ntt=" + searchFilter.facets();
        else
            url = "http://10.220.9.218/api/sitecore/Search/SearchLazyLoad?searchTerm=" + searchFilter.searchKey() + "&sortBy=" + searchFilter.sortBy() + "&res=" + searchFilter.recordsSize() + "&pageNo=" + searchFilter.pageNo() + "&ntt=" + searchFilter.facets();

        $.smoothScroll({
            offset: offsetHeight,
//            scrollTarget: $scrollTarget,
//            speed: '100',
            afterScroll: function () {
                $.ajax({
                    url: url,
                    method: "GET",
                    success: function (json) {
                        $("#load-spinner").removeClass("is-visible");						
                        $("#blog_data_local").html("");
						//$("#tags-local-data").html("");
						//$("#search-navbar-localdata").html("");
						$(".content-wrapper-local").html("");
                        $('#blog_data').html("");
                        $('#blog_data').append($.parseHTML(json));
                        $('#blog_data').css('opacity', '1');
                        // if ($('#blog_data').find('div').hasClass("search-result__no-result"))
                            // isResultAvailable(false);
                        // else
                            // isResultAvailable(true);

                        self.totalResults = $('.search-result-content').data('totalresults');
                        self.isFacetVideoAvailable(true);
                        totalNoOfRecords(self.totalResults);
                        setPaging();
                        $content.css('opacity', '1');
						if (searchFilter.q() != "") {
							$(".label-tag__remove").show();
						}
						else {
							$(".label-tag__remove").hide();
						}
                    },
                    error: function (e) {}
                })
            }
        });
    }

    self.lazyLoadSearchItems = function () {
        searchFilter.pageNo(searchFilter.pageNo()+1);
        if (searchFilter.pageNo() > numberOfPages() || !lazySearchEnabled()){
            return false;
        }
        $(".search-result-content.new").viewportChecker({
            offset: -100,
            classToAddForFullView: "",

            callbackFunction: function (element) {
                var $that = $(element),
                    getLink = $that.attr("data-recurringlink"),
                    $spinner = $("#load-spinner-lazyload");
					
                if(lazySearchEnabled()){
                    $spinner.addClass("is-visible");
                    $.ajax({
                        type: "GET",
                        url: "http://10.220.9.218/api/sitecore/Search/SearchLoadAllLazy?searchTerm=" + searchFilter.searchKey() + "&sortBy=" + searchFilter.sortBy() + "&res=" + searchFilter.recordsSize() + "&pageNo=" + searchFilter.pageNo() + "&ntt=" + searchFilter.facets(),

                        success: function (json) {
                            $spinner.removeClass("is-visible");
                            $that.after('<div class="search-result-content new"></div>').removeClass('new').append($.parseHTML(json));
                            lazyLoadSearchItems()
                        },
                        error: function (e) {
							$spinner.removeClass("is-visible");
                            console.log(e);
                        }
                    })
                }
            }
        })

    }

    // video-blog landing page start

    self.sortByVideo = ko.observable("new");
    self.count = ko.observable(10);
	self.videoId = ko.observable($('#video-page-id').data('videoid'));
	
    self.onRecordsPerPageChange = function (count) {
        self.count(count);
        self.getVideoData();
    }

    self.onSortVideo = function (sortByVideo) {
        self.sortByVideo(sortByVideo);
        self.getVideoData();
    }

	self.setPagingData = function(){
		 self.totalResults = $('#blog_data_local').data('totalresults');
         totalNoOfRecords(self.totalResults);
         setPaging();
	}

    self.getVideoData = function () {
        var $scrollTarget, $content,
            offsetHeight = $('div').is('.search-result-navbar') ? -150 : -200;
        $scrollTarget = $('.search-result-navbar');
        $content = $('#blog_video_data');
        $content.css('opacity', '0.7');
        $('#blog_video_data').css('opacity', '0.7');
        $.ajax({
            url: "http://10.220.9.218/api/sitecore/Videos/VideoSortandFilter?sortBy=" + sortByVideo() + "&count=" + count()+ "&videoId=" + videoId(),
            method: "GET",
            success: function (json) {
                $('#blog_video_data').html("");
                $('#blog_video_data').append($.parseHTML(json));
                $content.css('opacity', '1');
            },
            error: function (e) {}
        })
    }
	
    // video-blog landing page end
	
    return {
        lazyLoadRecurringItems: lazyLoadRecurringItems,
        getBlogData: getBlogData,
        onPageClick: onPageClick,
        getVideoData: getVideoData,
		setPagingData: setPagingData
		//subscribeModalDisplay : subscribeModalDisplay
    }
}

$(document).ready(function () {
    var vm = viewModal();
    // Expose globally for debugging
    window.vm = vm;
    
    ko.applyBindings(vm, document.querySelector("html"));
    //vm.getBlogData();
    vm.lazyLoadRecurringItems();
	vm.setPagingData();
    vm.getVideoData();
	
	//vm.subscribeModalDisplay();
	setTimeout(function () {
        $("#subscribeModal").modal('show');
    }, 60000);

});