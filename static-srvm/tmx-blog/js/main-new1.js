function viewModal() {
    var self = this;
    self.totalCount = 87;
    self.catid = "010C8991-71DF-443C-9AB3-AAD74D6BBDB0";
    self.index = -5;
    self.next = 5;
    self.totalNoOfRecords = ko.observable(0);
    self.currentPage = ko.observable(1);
    self.isSearchInitiated = ko.observable(false);

    searchFilter = {
        sortBy: ko.observable('new'),
        searchKey: ko.observable(""),
        pageNo: ko.observable(1),
        recordsPerPage: ko.observable(10)
    };

    this.numberOfPages = ko.computed(function () {
        return totalNoOfRecords() / searchFilter.recordsPerPage();
    }, this);

    paginationOptions = {
        first: ko.observable(false),
        last: ko.observable(false),
        prev: ko.observable(false),
        next: ko.observable(false),
        range: ko.observableArray([])
    }

    self.lazyLoadRecurringItems = function () {
        console.log("print lazyload")
        if (totalCount != null && index != null) {
            if (index >= totalCount) {
                return;
            }
            else {
                index = index + 5;
                next = index + 5;
            }
        }
        $(".lazyLoadRecurringContainer.new").viewportChecker({
            offset: -100,
            classToAddForFullView: "",
            callbackFunction: function (element) {
                var $that = $(element),
                    getLink = $that.attr("data-recurringlink"),
                    $spinner = $("#load-spinner");
                $spinner.addClass("is-visible");
                $.ajax({
                    type: "GET",
                    //   url: "http://35.208.117.22/api/sitecore/Blog/LazyLoadCatBlogs?cat=" + catid + "&index=" + index + "&next=" + next, 
                    url: "http://dev-terminix.com/api/sitecore/Blog/LazyLoadCatBlogs?cat=" + catid + "&index=" + index + "&next=" + next,
                    success: function (json) {
                        $spinner.removeClass("is-visible");
                        $that.after('<div class="lazyLoadRecurringContainer new"></div>').removeClass("new").append($.parseHTML(json));
                        lazyLoadRecurringItems()
                    },
                    error: function (e) { }
                })
            }
        })
    }

    self.setPaging = function () {
        (currentPage() > 3) ? paginationOptions.first(true) : paginationOptions.first(false);
        (currentPage() < numberOfPages() - 2) ? paginationOptions.last(true) : paginationOptions.last(false);

        (currentPage() > 1) ? paginationOptions.prev(true) : paginationOptions.prev(false);
        (currentPage() < numberOfPages()) ? paginationOptions.next(true) : paginationOptions.next(false);

        let startIndex = 1;
        let endIndex = numberOfPages();
        paginationOptions.range([]);

        if (numberOfPages() > 5) {
            //if current page less than 3
            endIndex = 5;
            if (currentPage() >= numberOfPages() - 2) {
                startIndex = numberOfPages() - 4;
                endIndex = numberOfPages();
            }
            if (currentPage() <= numberOfPages() - 3 && currentPage() > 3) {
                startIndex = currentPage() - 2;
                endIndex = currentPage() + 2;
            }
        }
        for (let i = startIndex; i <= endIndex; i++)
            paginationOptions.range.push(i);
    }

    self.onPageClick = function (page) {
        currentPage(page);
        searchFilter.pageNo(page);
        getBlogData();
    }

    self.onSort = function (data) {
        searchFilter.sortBy(data);
        getBlogData();
    }

    self.onRecordsPerPageChange = function (recordsPerPage) {
        if (recordsPerPage == 'all')
            searchFilter.recordsPerPage('');
        else
            searchFilter.recordsPerPage(recordsPerPage);
        getBlogData();
    }

    self.onSearch = function (data, event) {
        if (event.which == 13) {
            isSearchInitiated(true);
            getBlogData();
        }
    }

    self.getBlogData = function () {
        $("#load-spinner").addClass("is-visible");

        var $scrollTarget, $content,
        offsetHeight = $('div').is('.search-result-navbar') ? -150 : -200;
        $scrollTarget = $('.search-result-navbar');
        $content = $('.tab-content');
        $content.css('opacity', '0.7');

        $.smoothScroll({
            offset: offsetHeight,
            scrollTarget: $scrollTarget,
            speed: '100',
            afterScroll: function () {
                $.ajax({
                    url: "http://dev-terminix.com/api/sitecore/Search/SearchLazyLoad?searchTerm=" + searchFilter.searchKey() + "&sortBy=" + searchFilter.sortBy() + "&res=" + searchFilter.recordsPerPage() + "&pageNo=" + searchFilter.pageNo(),
                    method: "GET",
                    success: function (json) {
                        $("#load-spinner").removeClass("is-visible");
                        totalNoOfRecords(90);
                        $('#blog_data').html("");
                        $('#blog_data').append($.parseHTML(json));
                        setPaging();
                        
                        $content.css('opacity', '1');
                    },
                    error: function (e) { }
                })
            }
        });
    }

    return {
        lazyLoadRecurringItems: lazyLoadRecurringItems,
        getBlogData: getBlogData,
        onPageClick: onPageClick
    }
}

$(document).ready(function () {
    var vm = viewModal();
    // Expose globally for debugging
    window.vm = vm;
    ko.applyBindings(vm, document.querySelector("html"));
    vm.lazyLoadRecurringItems();
});

