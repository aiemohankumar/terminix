//blog js

var _SCHEDULE_SLOTS = [];

$(document).ready(function () {

    $(".scroll-to-top").click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
    });
    console.log("test12345");


    /*	Global Navigation
    	=========================================*/
    window.nav = new(function () {
        'use strict';

        var self = this,
            $moreListItem = $('li.more'),
            $shrink1 = $('li.shrink-1'),
            $subNav1 = $('.subnav .shrink-1'),
            $shrink2 = $('li.shrink-2'),
            $subNav2 = $('.subnav .shrink-2'),
            $mobileNav = $('.header-container.mobile'),
            $desktopNav = $('.header-container.desktop');

        self.states = [0, 1, 2, 3];
        self.currentState = self.states[2];

        self.SetNav = function () {
            var windowWidth = GetWindowWidth();

            if (windowWidth < 1024) {
                self.currentState = self.states[3];
            } else if (windowWidth < 1380) {
                self.currentState = self.states[2];
            } else if (windowWidth < 1575) {
                self.currentState = self.states[1];
            } else {
                self.currentState = self.states[0];
            }

            self.UpdateNav();
        };

        self.UpdateNav = function () {
            //console.log(self.currentState);
            switch (self.currentState) {
                case 0:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.hide();
                    $shrink1.show();
                    $subNav1.hide();

                    $shrink2.show();
                    $subNav2.hide();
                    break;

                case 1:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.show();
                    $shrink1.hide();
                    $subNav1.show();

                    $shrink2.show();
                    $subNav2.hide();
                    break;

                case 3:
                    $mobileNav.show();
                    $desktopNav.hide();
                    break;

                case 2:
                default:
                    $mobileNav.hide();
                    $desktopNav.show();
                    $moreListItem.show();
                    $shrink1.hide();
                    $subNav1.show();

                    $shrink2.hide();
                    $subNav2.show();
                    break;
            }
        };

        return self;
    })();


    nav.SetNav();
    $(window).resize(nav.SetNav);


    $('.mobile .nav-icon').on('click', ToggleMobileNav);
    var $mobileNavItems = $('.mobile .mainnav > li');

    function ToggleMobileNav() {
        $('.mobile .nav-icon a').parents('.header-container.mobile').toggleClass('open');
    }

    $mobileNavItems.on('click', function (e) {
        var $this = $(this);

        if ($this.hasClass('active')) {
            $this.toggleClass('active');
        } else {
            $mobileNavItems.removeClass('active');
            $this.toggleClass('active');
        }
    });
    /*	=========================================
    	End Global Navigation */

});

/**lifted from tmx.utils.js**/
function GetWindowWidth() {
    return window.innerWidth;
}
