var ctx = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
var imageNoImageBG = ctx + "/img/no-image-bg.jpg";

function getCurrentUrl() {
    return window.location.href
}

function linkForJson(link) {
    var result;
    if (link.indexOf("format=json") == -1) result = link + (link.indexOf("?") == -1 ? "?" : "&") + "format=json";
    else result = link;
    return result
}
$("#menu-toggle, .menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled")
});
$(document).ready(function () {
    $("#spinner").addClass("is-visible");
    if (typeof pageJSON != "undefined") dataReceived(pageJSON);
    else if ($("title").text() == "404 File Not Found") {
        $("#spinner").removeClass("is-visible");
        $(".blog-content").remove()
    }
});

function shareButtonLinks() {
    var siteBaseUrl = $("body").attr("siteUrl");
    $("#FacebookShare").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + siteBaseUrl).attr("target", "_blank");
    $("#TwitterShare").attr("href", "https://twitter.com/intent/tweet?text=" + siteBaseUrl).attr("target", "_blank");
    $("#PinterestShare").attr("href", "https://pinterest.com/pin/create/button/?url=" + siteBaseUrl).attr("target", "_blank");
    $("#LinkedinShare").attr("href", "https://www.linkedin.com/shareArticle?mini=true&url=" +
        siteBaseUrl).attr("target", "_blank")
}
$(window).on("load resize scroll", function () {
    subNavPaddingTop();
    mostRecentAsideHeight()
});
$(document).ready(function () {
    $(".tags-list a").attr("onclick", "startSearchSpinner()");
    $(".form-inline a").attr("onclick", "startSearchSpinner()");
    $("#resultTabs a").attr("onclick", "startArticleOpacity()");
    $(".btn-group").find("select").attr("onchange", "startArticleOpacity()")
});

function startSearchSpinner() {
    $(".bgSpin").css("display", "")
}

function startArticleOpacity() {
    $(".tab-content").css("opacity", "0.6");
    $(".archive").css("opacity", "0.6")
}

function stopArticleOpacity() {
    $(".tab-content").css("opacity", "1")
}

function dataReceived(data) {
    pageContent = data.contents[0];
    $("#spinner").removeClass("is-visible");
    pageContent.mainContent = ko.observable(pageContent.mainContent);
    pageContent.contentBlockLeftView = true;
    pageContent.contentBlockView = function () {
        pageContent.contentBlockLeftView = !pageContent.contentBlockLeftView
    };
    if (pageContent.searchKeyword == undefined) pageContent.searchKeyword = ko.observable("");
    else pageContent.searchKeyword = ko.observable(pageContent.searchKeyword);
    pageContent.searchUrl = ko.observable(pageContent.searchUrl);
    pageContent.searchUrlCancel = ko.observable(pageContent.searchUrlCancel);
    pageContent.itemDateFormat = function (data) {
        var formatedDate = data[0].slice(0, 10).split("-");
        return formatedDate[1] + "-" + formatedDate[2] + "-" + formatedDate[0]
    };
    pageContent.mainContent = ko.observable(pageContent.mainContent);
    pageContent.ajaxReloadPagination = function (data1, event) {
        var getLink = $(event.currentTarget).attr("data-href");
        var reqLink = "" + window.location.origin + window.location.pathname + getLink;
        if (reqLink.indexOf("format=json") ==
            -1) reqLink = reqLink + "&format=json";
        if (getLink.indexOf("format=json") != -1) getLink = getLink.replace("&format=json", "");
        history.pushState(null, null, getLink);
        var $scrollTarget, $content, offsetHeight = $("div").is(".search-result-navbar") ? -150 : -200;
        if (pageContent.pageType == "SearchPage") {
            $scrollTarget = $(".search-result-navbar");
            $content = $(".tab-content")
        } else {
            $scrollTarget = $(".archive-navbar");
            $content = $(".archive")
        }
        $content.css("opacity", "0.7");
        $.smoothScroll({
            offset: offsetHeight,
            scrollTarget: $scrollTarget,
            speed: "100",
            afterScroll: function () {
                $.ajax({
                    type: "GET",
                    url: reqLink,
                    dataType: "JSON",
                    success: function (json) {
                        pageContent.mainContent(json.contents[0].mainContent);
                        if (json.contents[0].searchKeyword == undefined) pageContent.searchKeyword("");
                        else pageContent.searchKeyword(json.contents[0].searchKeyword);
                        pageContent.searchUrl(json.contents[0].searchUrl);
                        $(".selectpicker").selectpicker();
                        $(".main-search").on("submit", "form", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            $(".btn-hm-search").trigger("click")
                        });
                        lazyLoadAllOption();
                        $content.css("opacity", "1")
                    },
                    error: function (e) {}
                })
            }
        })
    };
    pageContent.ajaxReloadMainContent = function (data1, event) {
        startArticleOpacity();
        var getLink = $(event.currentTarget).attr("data-href");
        var reqLink = "" + window.location.origin + window.location.pathname + getLink;
        if (reqLink.indexOf("format=json") == -1) reqLink = reqLink + "&format=json";
        if (getLink.indexOf("format=json") != -1) getLink = getLink.replace("&format=json", "");
        history.pushState(null, null, getLink);
        $.ajax({
            type: "GET",
            url: reqLink,
            dataType: "JSON",
            success: function (json) {
                pageContent.mainContent(json.contents[0].mainContent);
                if (json.contents[0].searchKeyword == undefined) pageContent.searchKeyword("");
                else pageContent.searchKeyword(json.contents[0].searchKeyword);
                pageContent.searchUrl(json.contents[0].searchUrl);
                $(".selectpicker").selectpicker();
                $(".main-search").on("submit", "form", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(".btn-hm-search").trigger("click")
                });
                lazyLoadAllOption();
                $(".tags-list a").attr("onclick", "startSearchSpinner()");
                $(".form-inline a").attr("onclick", "startSearchSpinner()");
                $("#resultTabs a").attr("onclick", "startArticleOpacity()");
                $(".btn-group").find("select").attr("onchange", "startArticleOpacity()")
            },
            error: function (e) {
                stopArticleOpacity()
            }
        })
    };
    pageContent.ajaxReloadMainContentSearch = function (data1, event) {
        startArticleOpacity();
        $(".bgSpin").css("display", "");
        var getLink = getSearchLink();
        var reqLink = window.location.origin + getLink;
        if (reqLink.indexOf("format=json") == -1) reqLink = reqLink + "&format=json";
        else;
        if (getLink.indexOf("format=json") !=
            -1) getLink = getLink.replace("&format=json", "");
        history.pushState(null, null, getLink);
        $.ajax({
            type: "GET",
            url: reqLink,
            dataType: "JSON",
            success: function (json) {
                pageContent.mainContent(json.contents[0].mainContent);
                pageContent.searchKeyword(json.contents[0].searchKeyword);
                pageContent.searchUrlCancel(json.contents[0].searchUrlCancel);
                pageContent.searchUrl(json.contents[0].searchUrl);
                $(".selectpicker").selectpicker();
                $(".main-search").on("submit", "form", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(".btn-hm-search").trigger("click")
                });
                lazyLoadAllOption();
                $(".tags-list a").attr("onclick", "startSearchSpinner()");
                $(".form-inline a").attr("onclick", "startSearchSpinner()");
                $(".btn-group").find("select").attr("onchange", "startArticleOpacity()")
            },
            error: function (e) {
                stopArticleOpacity()
            }
        })
    };

    function getSearchLink() {
        var getLink;
        var currentNttValue;
        var urlRemain;
        var ntt = "Ntt=";
        var searchWithQuestion = "search?";
        var search = "search";
        var currentUrl = window.location.href;
        if (currentUrl.indexOf(searchWithQuestion) != -1) urlRemain = currentUrl.substring(currentUrl.indexOf(searchWithQuestion) +
            searchWithQuestion.length);
        else if (currentUrl.indexOf(search) != -1) urlRemain = currentUrl.substring(currentUrl.indexOf(search) + search.length);
        var urlPartArray = urlRemain.split("&");
        for (var i = 0; i < urlPartArray.length; i++)
            if (urlPartArray[i].indexOf(ntt) != -1) currentNttValue = urlPartArray[i].substring(ntt.length);
        if (currentNttValue == $(".input-hm-search").val()) getLink = $(".btn-hm-search").attr("data-link-template").toString().replace(/%7BKEYWORD%7D/, $(".input-hm-search").val());
        else getLink = "/blog/search?Ntt=" +
            $(".input-hm-search").val();
        return getLink
    }
    pageContent.ajaxReloadMainContentVal = function (data1, event) {
        var that = event.target,
            url = "";
        for (var k = 0; k < that.length; k++)
            if (that[k].selected) url = that[k].value;
        var reqLink = "" + window.location.origin + window.location.pathname + url;
        var getLink = url;
        if (reqLink.indexOf("format=json") == -1) reqLink = reqLink + "&format=json";
        else;
        getLink = getLink.replace("&format=json", "");
        history.pushState(null, null, getLink);
        $.ajax({
            type: "GET",
            url: reqLink,
            dataType: "JSON",
            success: function (json) {
                pageContent.mainContent(json.contents[0].mainContent);
                pageContent.searchUrl(json.contents[0].searchUrl);
                $(".selectpicker").selectpicker();
                $(".main-search").on("submit", "form", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(".btn-hm-search").trigger("click")
                });
                lazyLoadAllOption();
                $(".btn-group").find("select").attr("onchange", "startArticleOpacity()");
                $(".form-inline a").attr("onclick", "startSearchSpinner()")
            },
            error: function (e) {}
        })
    };
    pageContent.clearSearchContent = function (data1, event) {
        pageContent.searchKeyword("");
        $(event.target).closest("form").find("input").val("")
    };
    pageContent.FBCommentsBlockLogic = function (model) {
        $("head").append('<meta property="fb:app_id" content=' + model.applicationID + "/>").append('<meta property="og:type" content="website"/>').append('<meta property="og:title" content="' + document.title + '"/>').append('<meta property="og:url" content="' + window.location.href + '"/>').append('<meta property="og:image" content="' + window.location.origin + imageNoImageBG + '"/>').append('<meta property="og:description" content="' + document.description + '"/>');
        (function (d,
            s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/" + model.language + "/sdk.js#xfbml=1&version=v2.5&appId=" + model.applicationID;
            fjs.parentNode.insertBefore(js, fjs)
        })(document, "script", "facebook-jssdk ")
    };
    ko.options.useOnlyNativeEvents = false;
    ko.applyBindings(pageContent, document.querySelector("html"));
    alreadyLoaded = true;
    articleAnimations();
    mostRecentAsideHeight();
    smoothSearchScroll();
    $(".selectpicker").selectpicker();
    $("select.archive-navbar__sort-select").each(function (i) {
        for (var k = 0; k < this.length; k++)
            if (this[k].selected);
        $(this).on("change", function (e) {})
    });
    $(".home-matters-search .search-result__clear-value").on("click touch", function (e) {
        e.preventDefault();
        $(e.target).closest("input").val("")
    });
    $(".searchFocus").on("click touch", function (e) {
        e.preventDefault();
        $(".input-hm-search").focus()
    });
    $(".main-search").on("submit", "form", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(".btn-hm-search").trigger("click")
    });
    if (navigator.userAgent.indexOf("Safari") != -1) {
        $(".navbar-mobile").on("shown.bs.collapse", function () {
            $("body").addClass("disable-scroll-safari-fix")
        });
        $(".navbar-mobile").on("hidden.bs.collapse", function () {
            $("body").removeClass("disable-scroll-safari-fix")
        })
    }
   
    lazyLoadRecurringItems();
    lazyLoadAllOption();
    categoryHightlight(pageContent.pageType);
    mobileHeaderMenuMove();
    subNavPaddingTop()
}

function subNavPaddingTop() {
    var headerHeight = $(".mainHeader > header").height();
    $("#subNavContainer").css({
        "top": headerHeight - 1
    });
    $("main.blog-content").css({
        "padding-top": headerHeight + $("#subNavContainer").height() - 1
    })
}

function mobileHeaderMenuMove() {
    if ($(window).outerWidth() < 1023) {
        var $collapseNav = $("#collapse-nav");
        $collapseNav.append('<li class="sub-nav-item"><a role="button" data-toggle="collapse" href="#collapseSubNav" aria-expanded="false" aria-controls="collapseSubNav" class="top-nav collapsed">Terminix Blog</a><ul class="collapse list-unstyled" id="collapseSubNav"></ul></li>');
        $("#collapseSubNav").append($("#subnav").find("li:not(.nav__logo,.search)").clone()).append('<li><a href="' + pageContent.searchLink +
            "?Ntt=" + '">Search</a></li>')
    }
}

function mostRecentAsideHeight() {
    if ($(".article-single")) {
        var $article = $(".article-single__content"),
            articleContentHeight = -40,
            $aside = $(".article-single").find(".sidebar"),
            $asideItems = $aside.find(".sidebar__item"),
            asideItemsHeight = 0;
        $article.children().each(function (i, el) {
            articleContentHeight += $(el).outerHeight(true)
        });
        if ($(window).outerWidth() > 768) $asideItems.each(function (index, el) {
            asideItemsHeight += $(el).outerHeight(true);
            if (index == 1 || index == 2) {
                $(el).show();
                return true
            }
            if (asideItemsHeight < articleContentHeight) $(el).show();
            else $(el).hide()
        });
        else $asideItems.each(function (index, el) {
            asideItemsHeight += $(el).outerHeight(true);
            if (index < 4) $(el).css({
                "display": "inline-block"
            });
            else $(el).hide()
        })
    }
}

function categoryHightlight(pageType) {
    setTimeout(function () {
        var $liList = $(".breadcrumb").find("li"),
            $linkList = $(".breadcrumb").find("li a");
        if (pageType != "ErrorPage") {
            var catName = $linkList.get($linkList.length - 1).innerText;
            if (pageType == "CategoryPage") catName = $(".breadcrumb").find("li span").text();
            if (pageType == "AHSBMainPageBlogEntity") {
                catName = $linkList.get($linkList.length - 1).innerText;
                if ($(".breadcrumb").find("li span").text() == "Videos") catName = "Videos"
            }
            $("#subnav").find("li a").each(function () {
                if ($(this).text().toUpperCase() ==
                    catName.toUpperCase()) $(this).addClass("active")
            })
        }
    }, 400)
}

function smoothSearchScroll() {
    var currentPosition = 0;
    $(".search-btn").on("click", function () {
        var $btn = $(".btn-to-content");
        currentPosition = $(window).scrollTop(), $that = $(this);
        $.smoothScroll({
            direction: "top"
        });
        if (currentPosition > 200) $btn.addClass("is-visible");
        else $btn.removeClass("is-visible");
        if ($that.attr("aria-expanded") == "true" && $(window).scrollTop() > 200) return false;
        $("#searchCollapse").on("shown.bs.collapse", function () {
            $(".input-hm-search").focus()
        })
    });
    $(".btn-to-content").on("click", function () {
        $(this).removeClass("is-visible");
        $("html, body").animate({
            scrollTop: currentPosition
        }, 500)
    })
}

function articleAnimations() {
    $(".article").not(".article-single animated").addClass("transparent-hidden").viewportChecker({
        classToAdd: "transparent-visible animated fadeIn",
        offset: -50,
        classToAddForFullView: "",
        removeClassAfterAnimation: true,
        classToRemove: "transparent-hidden"
    });
    $(".item-article").not("animated").addClass("transparent-hidden").viewportChecker({
        classToAdd: "transparent-visible animated slideInUp",
        offset: 50,
        classToAddForFullView: "",
        removeClassAfterAnimation: true,
        classToRemove: "transparent-hidden"
    })
}

function lazyLoadRecurringItems() {
    $(".lazyLoadRecurringContainer.new").viewportChecker({
        offset: -100,
        classToAddForFullView: "",
        callbackFunction: function (element) {
            var $that = $(element),
                getLink = $that.attr("data-recurringlink"),
                $spinner = $("#load-spinner");
            $spinner.addClass("is-visible");
            $.ajax({
                type: "GET",
                url: window.location.origin + getLink + "&format=json",
                dataType: "JSON",
                success: function (json) {
                    var recurringItemsContent = json.contents[0].recurringItems;
                    $spinner.removeClass("is-visible");
                    if (recurringItemsContent.recurringExists) {
                        $that.after('<div class="lazyLoadRecurringContainer new" data-recurringLink="' +
                            recurringItemsContent.recurringLink + '"></div>').removeClass("new").append($.parseHTML(htmlFromRecurringJson(recurringItemsContent)));
                        lazyLoadRecurringItems()
                    } else $spinner.removeClass("is-visible")
                },
                error: function (e) {}
            })
        }
    })
}

function lazyLoadAllOption() {
    $(".lazyLoadAllOption.new").viewportChecker({
        offset: 0,
        classToAddForFullView: "",
        callbackFunction: function (element) {
            var $that = $(element),
                getLink = $that.attr("data-recurringlink"),
                $spinner = $("#load-spinner");
            $spinner.addClass("is-visible");
            if (getLink.indexOf("format=json") == -1) getLink = getLink + "&format=json";
            $.ajax({
                type: "GET",
                url: window.location.origin + window.location.pathname + getLink,
                dataType: "JSON",
                success: function (json) {
                    var allOptionContent, lazyLoadLink;
                    var allOptionContentContainer =
                        json.contents[0].mainContent.container;
                    for (var i = 0; i < allOptionContentContainer.length; i++)
                        if (allOptionContentContainer[i].templateType == "FilteredContent" || allOptionContentContainer[i].templateType == "ResultListContent") {
                            allOptionContent = allOptionContentContainer[i];
                            lazyLoadLink = !!allOptionContentContainer[i].lazyLoadLink ? allOptionContentContainer[i].lazyLoadLink : false
                        } $spinner.removeClass("is-visible");
                    $that.append($.parseHTML(htmlFromAllOptionJson(allOptionContent))).after('<div class="lazyLoadAllOption new" data-recurringLink="' +
                        lazyLoadLink + '"></div>').removeClass("new");
                    if (lazyLoadLink) lazyLoadAllOption();
                    else $spinner.removeClass("is-visible")
                },
                error: function (e) {
                    console.log("Some error!");
                    console.log(e);
                    $spinner.removeClass("is-visible")
                }
            })
        }
    })
}

function htmlFromAllOptionJson(data) {
    var recordsArr = data.records,
        recordsContenerType = data.templateType,
        html = "",
        moreMessage;
    moreMessage = data.recordTypes == "Video" ? "View more" : "Read more";
    for (var i = 0; i < recordsArr.length; i++) {
        var item = recordsArr[i],
            date = ("" + item.attributes["blogEntity.dateOfPublication"]).slice(0, 10).split("-"),
            formatedDate = date[1] + "-" + date[2] + "-" + date[0];
        if (recordsContenerType == "ResultListContent") html = html + '<div class="search-result-content__item"><a href="' + item.attributes["blogEntity.fullUrl"] +
            '" style="background-image: url(\'' + item.attributes["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="search-result-content__item--preview"></a> <div class="search-result-content__item--excerpt"> <div class="item-timestamp">' + formatedDate + '</div><a href="' + item.attributes["blogEntity.fullUrl"] + '"> <h4>' + item.attributes["blogEntity.previewTitle"] + "</h4></a> <p>" + item.attributes["blogEntity.previewExcerpt"] + '</p><a href="' + item.attributes["blogEntity.fullUrl"] + '" class="link-read_more">' +
            moreMessage + "</a> </div> </div>";
        else html = html + '<div class="video-archive__item"><a href="' + item.attributes["blogEntity.fullUrl"] + '" style="background-image: url(\'' + item.attributes["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="video-archive__item--preview"></a> <div class="video-archive__item--excerpt"> <div class="item-timestamp">' + formatedDate + '</div><a href="' + item.attributes["blogEntity.fullUrl"] + '"> <h4>' + item.attributes["blogEntity.previewTitle"] + "</h4></a> <p>" + item.attributes["blogEntity.previewExcerpt"] +
            "</p> </div> </div>"
    }
    return html
}

function htmlFromRecurringJson(data) {
    var containerArr = data.container,
        html = "";
    for (var i = 0; i < containerArr.length; i++) {
        var item = containerArr[i];
        if (item.templateType == "RecurringThreeBlogEntitiesPreview") html = html + htmlFromRecurringJsonThreeArticlesTemplate(item);
        if (item.templateType == "RecurringSingleBlogEntityPreview") html = html + htmlFromRecurringJsonSingleArticleTemplate(item);
        if (item.templateType == "RecurringContentBlock") html = html + htmlFromRecurringJsonContentBlockTemplate(item)
    }
    return html
}

function htmlFromRecurringJsonThreeArticlesTemplate(item) {
    var html = "";
    html = html + '<section class="category-three-col-block">';
    for (var y = 0; y < item.records.length; y++) {
        var record = item.records[y].attributes;
        html = html + '<a href="' + record["blogEntity.fullUrl"] + '" class="category-three-col-block__item" style="background-image: url(\'' + record["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');"><div class="caption"><p>' + record["blogEntity.previewExcerpt"] + "</p></div></a>"
    }
    html = html + "</section>";
    return html
}

function htmlFromRecurringJsonSingleArticleTemplate(item) {
    var html = "";
    html = html + '<div class="container">';
    if (item.headingType == "bigLabel") html = html + '<div class="bigLabel-wrapper"><span class="label-tranding">' + item.headingVerbiage + "</span></div>";
    if (item.headingType == "bigHeading") html = html + "<h2>" + item.headingVerbiage + "</h2>";
    html = html + '<article class="article">';
    if (item.featuredImage) {
        html = html + '<aside class="article__aside-preview">';
        if (item.imageLabel) html = html + '<span data-bind="text: imageVerbiage" class="label-tranding">' +
            item.imageVerbiage + "</span>";
        html = html + '<a href="' + item.records[0].attributes["blogEntity.fullUrl"] + '" style="background-image: url(\'' + item.records[0].attributes["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="img"></a>';
        html = html + "</aside>"
    }
    html = html + '<div class="article__content">';
    if (item.previewTitle != "bigTitle") {
        html = html + '<header class="article-tranding">';
        html = html + '<a href="' + item.records[0].attributes["blogEntity.fullUrl"] + '"><h2>' + item.records[0].attributes["blogEntity.previewTitle"] +
            "</h2></a></header>"
    } else {
        html = html + "<header>";
        html = html + '<a href="' + item.records[0].attributes["blogEntity.fullUrl"] + '"><h1>' + item.records[0].attributes["blogEntity.previewTitle"] + "</h1></a></header>"
    }
    html = html + "<p>" + item.records[0].attributes["blogEntity.previewExcerpt"] + "</p>";
    html = html + '<a href="' + item.records[0].attributes["blogEntity.fullUrl"] + '" class="link-read_more">Read more</a>';
    html = html + "</div></article>";
    html = html + "</div>";
    return html
}


if (navigator.userAgent.search("MSIE") >= 0) {
    var position = navigator.userAgent.search("MSIE") + 5;
    var end = navigator.userAgent.search("; Windows");
    var version = navigator.userAgent.substring(position, end);
    if (parseInt(version) < 10) updateBrowserMsg()
} else if (navigator.userAgent.search("Chrome") >= 0) {
    var position = navigator.userAgent.search("Chrome") + 7;
    var end = navigator.userAgent.search(" Safari");
    var version = navigator.userAgent.substring(position, end);
    if (parseInt(version) < 47) updateBrowserMsg()
} else if (navigator.userAgent.search("Firefox") >=
    0) {
    var position = navigator.userAgent.search("Firefox") + 8;
    var version = navigator.userAgent.substring(position);
    if (parseInt(version) < 42) updateBrowserMsg()
} else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
    var position = navigator.userAgent.search("Version") + 8;
    var end = navigator.userAgent.search(" Safari");
    var version = navigator.userAgent.substring(position, end);
    if (parseInt(version) < 8) updateBrowserMsg()
} else if (navigator.userAgent.search("Opera") >= 0) {
    var position = navigator.userAgent.search("Version") +
        8;
    var version = navigator.userAgent.substring(position);
    if (parseInt(version) < 32) updateBrowserMsg()
}

function updateBrowserMsg() {
    setTimeout(function () {
        $(".updateBrowserMsg").slideDown()
    }, 3E3);
    $(".updateBrowserMsg").on("click", ".close", function (e) {
        $(".updateBrowserMsg").slideUp()
    })
}

function htmlFromRecurringJsonContentBlockTemplate(item) {
    var html = "",
        thisRecord;
    html = html + '<div class="container">';
    html = html + '<div class="articles-grid row">';
    if (item.records.length == 1) {
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>"
    }
    if (item.records.length == 2)
        for (var i = 0; i < item.records.length; i++) {
            thisRecord = item.records[i].attributes;
            html = html + '<div class="item col-sm-6 col-xs-12">';
            html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
            html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
            html = html + "</a></div>"
        }
    if (item.records.length ==
        3) {
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item col-md-6 col-sm-7 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item col-md-3 col-sm-5 col-xs-6">';
        html = html +
            '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item col-md-3 col-sm-5 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] +
            "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>"
    }
    if (item.records.length == 4) {
        html = html + '<div class="col-md-8 col-sm-12 row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item col-md-6 col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item col-md-6 col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div><div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 5 && pageContent.contentBlockLeftView == true) {
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item col-md-4 col-sm-6 col-xs-12">';
        html = html + '<a href="' +
            thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '<div class="col-md-4 col-sm-6 col-xs-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' +
            thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' +
            thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div><div class="col-md-4 col-sm-12 col-xs-12 row">';
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[4].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 5 && pageContent.contentBlockLeftView ==
        false) {
        html = '<div class="container">';
        html = html + '<div class="articles-grid articles-grid--right row">';
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item col-md-4 col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html =
            html + '<div class="col-md-4 col-sm-6 col-xs-12 row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div><div class="col-md-4 col-sm-12 col-xs-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] +
            '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[4].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 6) {
        html = html + '<div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html +
            '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>";
        html = html + '<div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[4].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>";
        html = html + '<div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[5].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] +
            "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 7) {
        html = html + '<div class="row"> <div class="col-md-8 col-sm-12 row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] +
            "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> <div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord =
            item.records[3].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> </div> <div class="row">';
        thisRecord = item.records[4].attributes;
        html = html + '<div class="item half col-sm-4 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[5].attributes;
        html = html + '<div class="item half col-sm-4 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] +
            "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[6].attributes;
        html = html + '<div class="item half col-sm-4 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 8) {
        html = html + '<div class="row">';
        html = html + '<div class="col-md-4 col-sm-6 col-xs-12 row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> <div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[4].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] +
            '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>";
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item col-md-4 col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG +
            ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> <div class="row">';
        thisRecord = item.records[5].attributes;
        html = html + '<div class="item col-md-8 col-sm-7 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[6].attributes;
        html = html + '<div class="item col-md-2 col-sm-5 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[7].attributes;
        html = html + '<div class="item col-md-2 col-sm-5 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>"
    }
    if (item.records.length == 9) {
        html = html + '<div class="row">';
        thisRecord = item.records[0].attributes;
        html = html + '<div class="item col-md-4 col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] +
            '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '<div class="col-md-4 col-sm-6 col-xs-12 row">';
        thisRecord = item.records[1].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] +
            "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[3].attributes;
        html = html + '<div class="item half col-sm-12 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> <div class="col-md-4 col-sm-12 row">';
        thisRecord = item.records[2].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord =
            item.records[4].attributes;
        html = html + '<div class="item half col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + '</div> </div> <div class="row">';
        html = html + '<div class="col-md-8 col-sm-12 row">';
        thisRecord = item.records[5].attributes;
        html = html + '<div class="item col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[6].attributes;
        html = html + '<div class="item col-sm-6 col-xs-12">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' +
            thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>";
        html = html + '<div class="col-md-4 row">';
        thisRecord = item.records[7].attributes;
        html = html + '<div class="item col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" +
            imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] + "</p> </div>";
        html = html + "</a></div>";
        thisRecord = item.records[8].attributes;
        html = html + '<div class="item col-md-12 col-sm-6 col-xs-6">';
        html = html + '<a href="' + thisRecord["blogEntity.fullUrl"] + '" style="background-image: url(\'' + thisRecord["blogEntity.featuredImage"] + "'), url(" + imageNoImageBG + ');" class="item-article">';
        html = html + '<div class="caption"> <p>' + thisRecord["blogEntity.previewTitle"] +
            "</p> </div>";
        html = html + "</a></div>";
        html = html + "</div>";
        html = html + "</div>"
    }
    html = html + "</div>";
    html = html + "</div>";
    pageContent.contentBlockView();
    return html
};
