var utag_data = {
    "imp_ids": []
    , "order_affiliation": ""
    , "imp_lists": []
    , "order_shipping_amt": ""
    , "imp_names": []
    , "order_discount": ""
    , "ga_event_label": ""
    , "ga_event_value": ""
    , "ga_action_option": ""
    , "order_type": ""
    , "customer_city": ""
    , "order_contract_ids": []
    , "customer_address": ""
    , "customer_first_name": ""
    , "order_tax_amt": ""
    , "customer_property_option": ""
    , "ga_ecom_action_type": ""
    , "product_names": []
    , "customer_email": ""
    , "promo_creatives": []
    , "order_total": ""
    , "ga_event_action": ""
    , "order_subtotal": ""
    , "order_id": ""
    , "customer_type": ""
    , "imp_positions": []
    , "product_discounts": []
    , "order_agent": "TMX.COM"
    , "product_prices": []
    , "imp_prices": []
    , "customer_last_name": ""
    , "product_positions": []
    , "order_payment_method": ""
    , "promo_positions": []
    , "promo_ids": []
    , "promo_names": []
    , "customer_state": ""
    , "imp_variants": []
    , "customer_purchase_step": ""
    , "product_variants": []
    , "customer_zip": ""
    , "imp_categories": []
    , "ga_event_category": ""
    , "product_ids": []
    , "order_promo_code": ""
    , "product_categories": []
    , "customer_concern": ""
    , "search_term": ""
    , "order_party_id": ""
    , "customer_id": ""
    , "order_currency_code": ""
}

function procEvent(name, type, a, b) {
    type = typeof type !== 'undefined' ? type : '';
    action = typeof action !== 'undefined' ? action : '';
    a = typeof a !== 'undefined' ? a : '';
    b = typeof b !== 'undefined' ? b : '';
}
(function (a, b, c, d) {
    a = '//tags.tiqcdn.com/utag/servicemaster/trmx/prod/utag.js';
    b = document;
    c = 'script';
    d = b.createElement(c);
    d.src = a;
    d.type = 'text/java' + c;
    d.async = true;
    a = b.getElementsByTagName(c)[0];
    a.parentNode.insertBefore(d, a);
})();