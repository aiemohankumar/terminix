var _LESS_THAN_IE9 = false;
var _MATERIALIZE = false;
var _BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE = false;
var _ENABLE_PROPERTY_INFORMATION_TOGGLE = true;
var _ENABLE_SMARTY_ZIP_TOGGLE = true;
// Shim to fix features on unsupported browsers
webshim.activeLang('en');
webshims.polyfill('es5 es6 forms picture');
webshims.cfg.no$Switch = true;
$(document).ready(function () {
    // Masks for inputs
    $('.mask-zip').mask('00000');
    $('.mask-phone').mask('(000) 000-0000');
    $('.mask-numeric').mask('0#');
    // Scrolling to top of page
    $(".scroll-to-top").on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
    });
});