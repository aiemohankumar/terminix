var _QAS_FEATURE_TOGGLE = true;
var _ENABLE_CITY_STATE_TOGGLE = false;
var _ENABLE_SMARTY_ZIP_TOGGLE = true;
var _AUTO_ADDING_PEST_CONTROL_FEATURE_TOGGLE = false;
var _BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE = false;
var _FAB_FEATURE_TOGGLE = true;
var _HTML5_POLYFILL = true;
var _MATERIALIZE = false;
$(document).ready(function () {
    // Masks for inputs
    $('.mask-zip-short').mask('00000');
    $('.mask-zip').mask('00000-0000');
    $('.mask-phone').mask('(000) 000-0000');
    $('.mask-phone-dash').mask('000-000-0000');
    $('.mask-numeric').mask('0#');
    // Regex patterns for inputs
    $('.patt-email').attr('pattern', function (i, o) {
        return GetGlobalUIPattern('email')
    });
    $('.patt-phone').attr('pattern', function (i, o) {
        return GetGlobalUIPattern('phone')
    });
    if (_HTML5_POLYFILL) {
        // Shim to fix html5 forms and srcset on unsupported browsers
        webshim.activeLang('en');
        webshims.polyfill('forms');
        webshims.polyfill('picture');
        webshims.cfg.no$Switch = true;
    }
    // currently utag_data.cvo_sid coming from telium
    var MAX_ATTEMPT = 100;
    var initCount = 0;
    var cvoSIdInterval = setInterval(function () {
        initCount++;
        if (initCount < MAX_ATTEMPT) {
            var cvoSId = utag_data['cp.cvo_sid1'];
            console.log("cvoSId=" + cvoSId);
            if (cvoSId) {
                var $cvoSId = $("#input_cvoSId");
                if ($cvoSId.length) {
                    $("#input_cvoSId").val(cvoSId);
                }
                clearInterval(cvoSIdInterval);
            }
        }
        else {
            clearInterval(cvoSIdInterval);
            console.log("Exceeded max attempt " + MAX_ATTEMPT + " times. Quit now.");
        }
    }, 500);
});
var consentKey = '1c1ac771-cfd4-4293-bbc3-b4f683030513';
(function () {
    var consentJs = document.createElement('script');
    consentJs.type = 'text/javascript';
    consentJs.async = true;
    consentJs.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'consentprovider.leadexec.net/scripts/requestJS.min.js';
    var consentS = document.getElementsByTagName('script')[0];
    consentS.parentNode.insertBefore(consentJs, consentS);
})();