$(document).ready(function() {
  function HideOfficialLiveChatBubble() { $('#livechat-compact-container').hide(); }
  $(document).on('HideOfficialLiveChatBubble', function(e) { HideOfficialLiveChatBubble(); });
});

window.__lc = window.__lc || {};
window.__lc.license = 6819721;
window.__lc.ga_version = "ga";

(function() {
  var lc = document.createElement('script');
  lc.type = 'text/javascript';
  lc.async = true;
  lc.src = 'https://' + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(lc, s);

})();
var LC_API = LC_API || {};

//
// Custom LC Functions
//
function LC_CUSTOM()
{
  var self = this;

  self.pathname = window.location.pathname;

  self.HasChatOccurred = function() {
    return self.agentHasSentMessage && self.visitorHasSentMessage;
  };

  self.CheckHasChatOccurred = function() {
    // Set the session storage
    sessionStorage.setItem('LiveChatState', JSON.stringify({
      hasChatOccurred: self.HasChatOccurred(),
      visitorId: LC_CUSTOM.GetVisitorId()
    }));
  };

  self.GetVisitorId = function() {
    return LC_API ? LC_API.get_visitor_id() : null;
  };

  self.agentHasSentMessage = false;
  self.visitorHasSentMessage = false;

  self.pages = {
    buyonline : 'buyonline',
    freeinspection : 'free-inspection'
  };

  self.confirmPages = {
    buyonline : 'buyonline/step-four',
    freeinspection : 'free-inspection/step-three'
  };

  self.cookies = {
    started_chat : '_lc_start_chat',
    started_loc : '_lc_start_loc',
    started_timestamp : '_lc_start_timestamp',
    started_by : '_lc_started_by',
    initial_greeting_message : '_lc_initial_greeting_message'
  };

  self.livechat_chat_started = false;
  self.error = $('#error').text();
  self.errorModal = $('#errorMessageModal').length;

  self.minimizeChatPages = [ /*'/buyonline/step-two'*/ ];

  self.checkDay = function() {
  if (new Date().getDay() == '6') {
    return false;
  }
  return true;
  };

  self.checkTime = function() {
  var offset = (new Date().getTimezoneOffset()) / 60;
  var time = ((new Date().getHours() + offset) % 24) - 5;  // Change to eastern standard time

  if (8 < time < 17) {
    return true;
  }
  return false;
  };

  self.checkUrlParam = function(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');
  for (var i = 0; i < sURLVariables.length; i++)
  {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
    return sParameterName[1];
    }
  }
  return false;
  };

  self.checkUrlPath = function(sPage) {
  return self.pathname.indexOf(sPage) > -1;
  };

  self.custom_variables = [ {
  name : 'Error_Info',
  value : self.error
  }, {
  name : 'Date',
  value : new Date()
  }, {
  name : 'Page Viewed',
  value : self.pathname
  }, {
  name : 'Customer Address',
  value : utag_data.customer_address
  }, {
  name : 'Customer City',
  value : utag_data.customer_city
  }, {
  name : 'Customer State',
  value : utag_data.customer_state
  }, {
  name : 'Customer zip',
  value : utag_data.customer_zip
  }, {
  name : 'Customer ID',
  value : utag_data.customer_id
  }, {
  name : 'Customer Email',
  value : utag_data.customer_email
  } ];

  //
  // Google Analytics
  //

  self.gaEventCategory = (function()
  {
  var gaLiveChatSrc = 'Other';

  if (self.checkUrlPath(self.pages.buyonline)) {
    gaLiveChatSrc = 'Pest Control';
  } else if (self.checkUrlPath(self.pages.freeinspection)) {
    gaLiveChatSrc = 'Termite Control';
  }
  return '> LiveChat' + ' : ' + gaLiveChatSrc;
  })();

  self.customProcEvent = function(sLabel)
  {
  //console.log(sLabel);
  procEvent(self.gaEventCategory, self.pathname, sLabel);
  };
}
var LC_CUSTOM = new LC_CUSTOM();

//
// Cookie Util Functions (should be moved in a utils javascript later on)
//

function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

function setSessionCookie(cname, cvalue) {
  var expires = "";
  var path = "/";
  document.cookie = cname + "=" + cvalue + ";" + "expires=" + expires + ";" + "path=" + path;
};

function checkCookie(cname) {
  return getCookie(cname) != "";
};

//
// LC Overrides
//

LC_API.on_before_load = function()
{
  //
  // Predefine custom functions
  //
  LC_CUSTOM.startChatCookie = function(sChatInitiator) {
  setSessionCookie(LC_CUSTOM.cookies.started_timestamp, new Date().getTime());
  setSessionCookie(LC_CUSTOM.cookies.started_chat, 'true');
  setSessionCookie(LC_CUSTOM.cookies.started_by, sChatInitiator);
  setSessionCookie(LC_CUSTOM.cookies.started_loc, LC_CUSTOM.pathname);

  LC_CUSTOM.chat_started = true;
  LC_CUSTOM.chat_started_by = sChatInitiator;
  LC_CUSTOM.chat_started_loc = LC_CUSTOM.pathname;

  var chatType = '';
  if (sChatInitiator.toLowerCase() == 'agent') {
    chatType = 'Proactive';
  } else {
    chatType = 'Reactive';
  }
  LC_CUSTOM.customProcEvent(chatType + ' Chat Initiated By : ' + sChatInitiator);
  LC_CUSTOM.customProcEvent(chatType + ' Chat Active > ' + sChatInitiator);
  }

  LC_CUSTOM.endChatCookie = function() {
  setSessionCookie(LC_CUSTOM.cookies.started_chat, 'false');
  LC_CUSTOM.chat_started = false;

  setSessionCookie(LC_CUSTOM.cookies.initial_greeting_message, 'false');
  LC_CUSTOM.initial_greeting_message = false;
  }

  LC_CUSTOM.chat_started = getCookie(LC_CUSTOM.cookies.started_chat) == 'true';
  LC_CUSTOM.chat_started_by = getCookie(LC_CUSTOM.cookies.started_by);
  LC_CUSTOM.chat_started_loc = getCookie(LC_CUSTOM.cookies.started_loc);
  LC_CUSTOM.initial_greeting_message = getCookie(LC_CUSTOM.cookies.initial_greeting_message) == 'true';

  //
  // Begin before load
  //
  LC_API.set_custom_variables(LC_CUSTOM.custom_variables);

  if (!LC_API.visitor_engaged() && !LC_CUSTOM.chat_started)
  {
  if (LC_CUSTOM.checkUrlParam("fromEmail"))
  {
    if (LC_API.agents_are_available())
    {
    LC_CUSTOM.customProcEvent('Chat is opened from email.');
    LC_API.open_chat_window();
    return false;
    }
    else
    {
    $('#chatNotAvailModal').modal('show');
    LC_API.hide_chat_window();
    return false;
    }
  }

  for (i = 0; i < LC_CUSTOM.minimizeChatPages.length; i++)
  {
    if (LC_CUSTOM.pathname.indexOf(LC_CUSTOM.minimizeChatPages[i]) >= 0 && LC_API.agents_are_available() && LC_CUSTOM.checkDay()) {
    LC_API.minimize_chat_window();
    return false;
    } else if (LC_API.visitor_engaged()) {
    LC_API.minimize_chat_window();
    return false;
    }
    if (!LC_CUSTOM.checkTime()) {
    LC_API.close_chat();
    return false;
    }
  }
  // Hide chat window by default
  LC_API.hide_chat_window();
  }
};

LC_API.on_after_load = function()
{
  LC_CUSTOM.customProcEvent('Active Page Session');

  // Open chat if the chat has already been running
  if (LC_API.chat_running())
  {
  LC_CUSTOM.customProcEvent('Chat continued from previous page');
  }

  // Check if agents are available for chat on page
  if (LC_API.agents_are_available() && !LC_API.visitor_engaged() && !LC_API.chat_window_hidden())
  {
  LC_CUSTOM.customProcEvent('Chat Agents available on page');
  }

  // Open chat if there are errors on the page
  if (LC_API.agents_are_available() && ($("#error").length > 0 || $('#errorMessageModal').length > 0))
  {
    LC_CUSTOM.customProcEvent('Chat is opened from form error.');
  LC_API.open_chat_window();
  }

  // Display Live Chat button to user if agents are available
  if (LC_API.agents_are_available())
  {
  // If on mobile device and the funnel, toggle the FAB live chat buttons
  if (LC_API.mobile_is_detected() && LC_CUSTOM.checkUrlPath(LC_CUSTOM.pages.buyonline))
  {
    //console.log('> Trigger HideOfficialLiveChatBubble');
    $(document).trigger('HideOfficialLiveChatBubble');
    //console.log('> Trigger ShowFABChatButton');
    $(document).trigger('ShowFABChatButton');

      // TODO: REMOVE THIS IF HEADER TRIGGER NO LONGER NEEDED
      //console.log('agents not available');
    // Header live chat button
    $('.livechat_button').hide();
  }
  else if (LC_API.mobile_is_detected())
  {
    $(document).trigger('HideOfficialLiveChatBubble');
    $(document).trigger('ShowMobileChatLink');
  }
  else
  {
    // TODO: REMOVE THIS IF HEADER TRIGGER NO LONGER NEEDED
    // Header live chat button
    $('.livechat_button').show();
    $('.livechat_button a, .livechat_image').on('click', function(e) { e.preventDefault(); LC_API.open_chat_window(); });
  }

  $(document).trigger('ShowGenericChatLink');
  }
  else
  {
    // TODO: REMOVE THIS IF HEADER TRIGGER NO LONGER NEEDED
    //console.log('agents not available');
  // Header live chat button
  $('.livechat_button').hide();

  $(document).trigger('HideGenericChatLink');


  // If on mobile device and the funnel, toggle the FAB live chat buttons
  if (LC_API.mobile_is_detected() && LC_CUSTOM.checkUrlPath(LC_CUSTOM.pages.buyonline))
  {
    //console.log('> Trigger HideOfficialLiveChatBubble');
    $(document).trigger('HideOfficialLiveChatBubble');
    //console.log('> Trigger HideFABChatButton');
    $(document).trigger('HideFABChatButton');
  }
  else if (LC_API.mobile_is_detected())
  {
    $(document).trigger('HideOfficialLiveChatBubble');
  }
  }

  //
  // Google Analytics
  //
  if (LC_CUSTOM.chat_started)
  {
  if (LC_CUSTOM.chat_started_by.toLowerCase() == 'agent')
  {
    LC_CUSTOM.customProcEvent('Proactive Chat Active > ' + LC_CUSTOM.chat_started_by);
  }
  else
  {
    LC_CUSTOM.customProcEvent('Reactive Chat Active > ' + LC_CUSTOM.chat_started_by);
  }
  }

  if (LC_CUSTOM.checkUrlPath(LC_CUSTOM.confirmPages.buyonline) || LC_CUSTOM.checkUrlPath(LC_CUSTOM.confirmPages.freeinspection))
  {
  LC_CUSTOM.customProcEvent('Funnel completed');

  if (LC_CUSTOM.chat_started)
  {
    LC_CUSTOM.customProcEvent('Funnel completed with chat');
    LC_CUSTOM.customProcEvent('Chat started from : ' + LC_CUSTOM.chat_started_loc);
  }
  else
  {
    LC_CUSTOM.customProcEvent('Funnel completed without chat');
  }
  }
};

LC_API.on_chat_window_opened = function() {

}

LC_API.on_chat_started = function() {
};

LC_API.on_chat_ended = function() {
  //console.log('on_chat_ended');
  LC_CUSTOM.endChatCookie();
  LC_CUSTOM.customProcEvent('Chat session ended');

}
LC_API.on_chat_state_changed = function() {
  //console.log('on_chat_state_changed');
}
LC_API.on_chat_window_hidden = function() {
  //console.log('on_chat_window_hidden');
}

LC_API.on_chat_window_minimized = function()
{
  //console.log('on_chat_window_minimized');

  // If on mobile device and the funnel, hide the official live chat button (FAB is used)
  if (LC_API.mobile_is_detected() && LC_CUSTOM.checkUrlPath(LC_CUSTOM.pages.buyonline))
  {
    //console.log('> Trigger HideOfficialLiveChatButton');
    $(document).trigger('HideOfficialLiveChatButton');

    if (LC_API.agents_are_available())
    {
      //console.log('> Trigger ShowFABChatButton');
      $(document).trigger('ShowFABChatButton');

      $(document).trigger('ShowGenericChatLink');
    }
  }
  else if (LC_API.mobile_is_detected())
  {
    //console.log('minimized available');
    $(document).trigger('HideOfficialLiveChatButton');

    if (LC_API.agents_are_available())
    {
      //console.log('agents available - trigger chat link');
      //console.log('> Trigger ShowFABChatButton');
      $(document).trigger('ShowMobileChatLink');

      $(document).trigger('ShowGenericChatLink');
    }
  }
}

LC_API.on_message = function(data)
{
  //console.log('on_message');

  // If the agent sent the message and
  if (data.user_type.toLowerCase() === 'agent') {

    //console.log('[LiveChat] Agent has sent message = true');
    LC_CUSTOM.agentHasSentMessage = true;
    LC_CUSTOM.CheckHasChatOccurred();

    // If the initial greeting isn't set (proactive greeting)
    if (!LC_CUSTOM.initial_greeting_message) {
      // Set initial greeting
      //console.log('> Setting initial greeting message.');
      setSessionCookie(LC_CUSTOM.cookies.initial_greeting_message, 'true');
      LC_CUSTOM.initial_greeting_message = true;

      // Only run if on mobile device and the funnel
      if (LC_API.mobile_is_detected() && LC_CUSTOM.checkUrlPath(LC_CUSTOM.pages.buyonline))
      {
        $(document).trigger('InitialGreetingInitiated', data);
      }
      else if (LC_API.mobile_is_detected())
      {
        //console.log('on message, mobile detected - trigger chat link');
        $(document).trigger('HideOfficialLiveChatButton');
        $(document).trigger('ShowMobileChatLink');
      }

      // Start chat for GA
      LC_CUSTOM.startChatCookie('Agent');
    }
  }

  // If the user initiates the chat
  else if (data.user_type.toLowerCase() === 'visitor') {

    //console.log('[LiveChat] Visitor has sent message = true');
    LC_CUSTOM.visitorHasSentMessage = true;
    LC_CUSTOM.CheckHasChatOccurred();

    // If the initial greeting isn't set (proactive greeting)
    if (!LC_CUSTOM.initial_greeting_message) {
      // Set initial greeting
      //console.log('> Setting initial greeting message.');
      setSessionCookie(LC_CUSTOM.cookies.initial_greeting_message, 'true');
      LC_CUSTOM.initial_greeting_message = true;

      // Start chat for GA
      LC_CUSTOM.startChatCookie('User');
    }
  }
}

LC_API.on_postchat_survey_submitted = function() {
  LC_CUSTOM.customProcEvent('Chat survey was submitted');
}
LC_API.on_prechat_survey_submitted = function() {
  //console.log('on_prechat_survey_submitted');
}
LC_API.on_rating_comment_submitted = function() {
  LC_CUSTOM.customProcEvent('Rating comment was submitted');
}
LC_API.on_rating_submitted = function() {
  LC_CUSTOM.customProcEvent('Rating was submitted');
}
LC_API.on_ticket_created = function() {
  LC_CUSTOM.customProcEvent('Ticket was created');
}