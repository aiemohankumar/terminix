   var _LESS_THAN_IE9 = false
       , _MATERIALIZE = false;
   var _BRANCH_PHONE_NUMBER_COOKIE_FEATURE_TOGGLE = false;
   var _ENABLE_PROPERTY_INFORMATION_TOGGLE = true;
   var _ENABLE_SMARTY_ZIP_TOGGLE = true;
   var _GLOBAL_UI_SETTINGS = {
       "inputRegexPattern": {
           "_comment": "all backslashes need to have a 2nd backslash accompanying it to be a valid JSON"
           , "name": ""
           , "email": "^\\w+([\\.%#$&*+_-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,})+$"
           , "phone": "\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$"
           , "zipcode": ""
           , "creditcard": "^[0-9]{15,16}$"
       }
       , "productRuleSet": {
           "10001": {
               "_name": "Free Termite Inspection"
               , "disabled": []
               , "swap": [
                "10004"
                , "10006"
                , "10007"
            ]
               , "bundles": []
           }
           , "10002": {
               "_name": "Genpest products (quarterly)"
               , "disabled": []
               , "swap": [
                "10005"
            ]
               , "bundles": []
           }
           , "10003": {
               "_name": "Mosquito (ATSB / Quick Guard)"
               , "disabled": []
               , "swap": [
                "10009"
            ]
               , "bundles": [
                   {
                       "productsInCart": [
                        "10004"
                        , "10005"
                    ]
                       , "productToUpgradeTo": "10007"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                }
                        , {
                       "productsInCart": [
                        "10006"
                    ]
                       , "productToUpgradeTo": "10007"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
                }
            ]
           }
           , "10004": {
               "_name": "Termite one time"
               , "disabled": []
               , "swap": [
                "10001"
            ]
               , "bundles": [
                   {
                       "productsInCart": [
                        "10003"
                        , "10005"
                    ]
                       , "productToUpgradeTo": "10007"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                }
                        , {
                       "productsInCart": [
                        "10005"
                    ]
                       , "productToUpgradeTo": "10006"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                }
            ]
           }
           , "10005": {
               "_name": "Silver plan"
               , "disabled": []
               , "swap": [
                "10002"
                , "10006"
                , "10007"
                , "10008"
            ]
               , "bundles": [
                   {
                       "productsInCart": [
                        "10003"
                        , "10004"
                    ]
                       , "productToUpgradeTo": "10007"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your pest, termite and mosquito control by upgrading to Platinum."
                }
                        , {
                       "productsInCart": [
                        "10004"
                    ]
                       , "productToUpgradeTo": "10006"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your pest and termite control by upgrading to Gold."
                }
            ]
           }
           , "10006": {
               "_name": "Gold plan"
               , "disabled": [
                   {
                       "productToDisable": "10002"
                       , "message": false
                }
                        , {
                       "productToDisable": "10004"
                       , "message": true
                }
                        , {
                       "productToDisable": "10008"
                       , "message": false
                }
            ]
               , "swap": [
                "10001"
                , "10002"
                , "10004"
                , "10005"
                , "10007"
                , "10008"
            ]
               , "bundles": [
                   {
                       "productsInCart": [
                        "10003"
                    ]
                       , "productToUpgradeTo": "10007"
                       , "message": "Savings Alert! Save ${{bundleSavings}} on your mosquito, pest and termite control by upgrading to Platinum."
                }
            ]
           }
           , "10007": {
               "_name": "Platinum plan"
               , "disabled": [
                   {
                       "productToDisable": "10002"
                       , "message": false
                }
                        , {
                       "productToDisable": "10003"
                       , "message": true
                }
                        , {
                       "productToDisable": "10004"
                       , "message": true
                }
                        , {
                       "productToDisable": "10008"
                       , "message": false
                }
            ]
               , "swap": [
                "10001"
                , "10002"
                , "10003"
                , "10004"
                , "10005"
                , "10006"
                , "10008"
            ]
               , "bundles": []
           }
           , "10008": {
               "_name": "Genpest one time"
               , "disabled": []
               , "swap": [
                "10005"
            ]
               , "bundles": []
           }
           , "10009": {
               "_name": "Mosquito one time"
               , "disabled": []
               , "swap": [
                "10003"
            ]
               , "bundles": []
           }
       }
   };

 // Shim to fix features on unsupported browsers
        webshim.activeLang('en');
        webshims.polyfill('es5 es6 forms picture');
        webshims.cfg.no$Switch = true;
        $(document).ready(function () {
            // Masks for inputs
            $('.mask-zip').mask('00000');
            $('.mask-phone').mask('(000) 000-0000');
            $('.mask-numeric').mask('0#');
            // Scrolling to top of page
            $(".scroll-to-top").on('click', function () {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
            });
        });