// Define the `salesCounter` module
var salesCounter = angular.module('salesCounter', []);

// Define the `SalesCounterController` controller on the `salesCounter` module
salesCounter.controller('SalesCounterController', SalesCounterController);

function SalesCounterController($scope, $http, $location) {
	var currentDt = new Date();
	var mm = currentDt.getMonth() + 1;
	var dd = currentDt.getDate();
	var yyyy = currentDt.getFullYear();
	var date = mm + '/' + dd + '/' + yyyy;
	$scope.iDays = date;
	$scope.fromDay = date;
	$scope.toDay = date;

	$scope.getSalesForLastDays = function() {
		$scope.iDays = $('#iDays').val();
		var config = {
			url : 'https://' + $location.host() + '/rest/model/tmx/services/rest/PurchasePathRestActor/getTmxSalesDayCount',
			method : 'POST',
			data : {
				'iDay' : $scope.iDays
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : $('#authorization').val()
			}
		}
		$http(config).then(function(response) {
			$scope.salesAgents = response.data;
		});
	}

	$scope.getSalesFromToDays = function() {
		$scope.fromDay = $('#fromDay').val();
		$scope.toDay = $('#toDay').val();

		var config = {
			url : 'https://' + $location.host() + '/rest/model/tmx/services/rest/PurchasePathRestActor/getTmxSalesCountFromToDay',
			method : 'POST',
			data : {
				'fromDay' : $scope.fromDay,
				'toDay' : $scope.toDay
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : $('#authorization').val()
			}
		}
		$http(config).then(function(response) {
			$scope.salesAgents = response.data;
		});
	}
}

$(document).ready(function() {
	$("#fromDay, #toDay, #iDays").datepicker({
		nextText : "",
		prevText : "",
		dayNamesMin : [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ]
	});
});