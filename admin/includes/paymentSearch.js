$(document).ready(function () {
    $("input[name='paymentSite']").change(function () {
        if ($("input[name='paymentSite']:checked").val() == "ecommPayments") {
            $("#ecommPayments").show();
            $("#myAccountPayments").hide();
        } else {
            $("#ecommPayments").hide();
            $("#myAccountPayments").show();
        }
    });

    $("#clear").click(function (e) {
        e.preventDefault();
        $("#ecommPayments input").val("");
        $("#myAccountPayments input").val("");
        $("#ecommPayments select").val("");
        $("#myAccountPayments select").val("");
    });

    $("#search").click(function (e) {
        e.preventDefault();
        $("#search-results").hide();
        $("#search-results").html("");
        if ($("input[name='paymentSite']:checked").val() == "ecommPayments") {
            var params = {
                paymentSite: $("input[name='paymentSite']:checked").val(),
                oId: $("#order_id").val(),
                customerNumber: $("#ecomm_customer_number").val(),
                street: $("#street").val(),
                city: $("#city").val(),
                state: $("#state").val(),
                zip: $("#zip").val(),
                paymentType: $("#ecomm_payment_type").val()
            }
        } else {
            var params = {
                paymentSite: $("input[name='paymentSite']:checked").val(),
                customerNumber: $("#myaccount_customer_number").val(),
                salesAgreeNumber: $("#myaccount_sales_agreement").val()
            }
        }

        $.get("paymentSearchResults.html", params, function (response) {
            $("#search-results").html(response);
            $("#search-results").show();
        });
    });
});
