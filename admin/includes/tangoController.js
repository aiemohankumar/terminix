// Define the `tangoReferral` module
var tangoReferral = angular.module('tangoReferral', []);

// Define the `TangoReferralController` controller on the `tangoReferral` module
tangoReferral.controller('TangoReferralController', TangoReferralController);

function TangoReferralController($scope, $http, $location) {
	$scope.referredDay = "";
	$scope.customerFirstName = "";
	$scope.customerLastName = "";
	$scope.customerEmail = "";
	$scope.customerNumber = "";
	$scope.referredFirstName = "";
	$scope.referredLastName = "";
	$scope.referredCustomerNumber = "";
	$scope.referralCode = "";
	$scope.referredEMail = "";
	$scope.status = "";
	$scope.usedDay = "";
	$scope.used = "";
	$scope.working = false;

	$scope.getReferralReport = function() {

		$scope.referredDay = $('#referredDay').val();
		$scope.customerFirstName = $('#customerFirstName').val();
		$scope.customerLastName = $('#customerLastName').val();
		$scope.customerEmail = $('#customerEmail').val();
		$scope.customerNumber = $('#customerNumber').val();
		$scope.referredFirstName = $('#referredFirstName').val();
		$scope.referredLastName = $('#referredLastName').val();
		$scope.referredCustomerNumber = $('#referredCustomerNumber').val();
		$scope.referralCode = $('#referralCode').val();
		$scope.referredEMail = $('#referredEMail').val();
		$scope.status = $('#status').val();
		$scope.usedDay = $('#usedDay').val();
		$scope.used = $('#used').val();

		var config = {
			url : 'https://' + $location.host() + '/rest/model/tmx/services/rest/PurchasePathRestActor/reteriveReferralRecords',
			method : 'POST',
			data : {
				'createdDate' : $scope.referredDay,
				'customerFirstName' : $scope.customerFirstName,
				'customerLastName' : $scope.customerLastName,
				'customerEmail' : $scope.customerEmail,
				'customerNumber' : $scope.customerNumber,
				'referredFirstName' : $scope.referredFirstName,
				'referredLastName' : $scope.referredLastName,
				'referredCustomerNumber' : $scope.referredCustomerNumber,
				'referralCode' : $scope.referralCode,
				'referredEMail' : $scope.referredEMail,
				'status' : $scope.status,
				'usedDate' : $scope.usedDay,
				'used' : $scope.used
			},
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : $('#authorization').val()
			}
		}
		$http(config).then(function(response) {
			$scope.referralReport = response.data;
			$('#working').addClass('hidden');
		});

		
	}

	$scope.working = function() {
		$('#working').removeClass('hidden');
	}
	
	$scope.worked = function() {
		
	}
	$scope.changeStatus = function() {
		if ($scope.working) {
			$scope.working = false;
		} else {
			$scope.working = true;
		}
	}

	$scope.clear = function() {
		$scope.referredDay = "";
		$scope.customerFirstName = "";
		$scope.customerLastName = "";
		$scope.customerEmail = "";
		$scope.customerNumber = "";
		$scope.referredFirstName = "";
		$scope.referredLastName = "";
		$scope.referredCustomerNumber = "";
		$scope.referralCode = "";
		$scope.referredEMail = "";
		$scope.status = "";
		$scope.usedDay = "";
		$scope.used = "";
	}
}

function checkSubmit(e) {
	if (e && e.keyCode == 13) {
		$('#GetReport').click();
	}
}

$(document).ready(function() {
	$("#referredDay, #usedDay").datepicker({
		nextText : "",
		prevText : "",
		dayNamesMin : [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ]
	});
});