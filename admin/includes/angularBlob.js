var app = angular.module("geolocationApp", []);
// lat:35.1073993
// long:-89.8662004
app.factory("UpdateModel", function($q, $window, $http) {
	var service = {};

	service.generateModel = function(lat, long) {
		var obj = addProcessor(null, "ReverseGeocodeProcessor", {
			"latitude" : lat,
			"longitude" : long
		});
		obj = addProcessor(obj, "TmxPriceProcessor", {
			"jsonpath":true,
			"inputJsonPath":"PROCESSORARRAY[?(@.PROCESSOR == 'ReverseGeocodeProcessor')].PROCESSOROUTPUT",
			"squareFootage" : "3999",
			"lotSize" : "0.5",
			"targetPest" : "GENPEST",
			"templateCode" : [ "PC112", "PC195" ]
		});

		return obj;
	}

	service.postModel = function(data) {
		var deferred = $q.defer();
		var modelData = this.generateModel(data.latitude, data.longitude);
		CallDWR("TmxGenericProcessor/startProcess", modelData, function(data) {
			deferred.resolve(data);
		});
		return deferred.promise;
	}

	return service;
});

app.controller("geolocationCntrl", function($scope, UpdateModel) {
	$scope.model = "";
	$scope.latitude = "35.1073993";
	$scope.longitude = "-89.8662004";
	$scope.update = function() {
		UpdateModel.postModel($scope).then(function(model) {
			$scope.model = model;
		}, function(reason) {
			$scope.model = "Model not updated.";
		});
	}
	$scope.exampleTwo = "buh";
});

function addProcessor(obj, name, input) {
	if (obj == null || obj == undefined) {
		obj = {};
	}
	if (obj.PROCESSORARRAY == undefined) {
		obj.PROCESSORARRAY = [];
	}
	var obj2 = {};
	obj2.PROCESSOR = name;
	obj2.PROCESSORINPUT = input;
	obj.PROCESSORARRAY.push(obj2);

	return obj;
}

function CallDWR(path, model, callback) {
	CallDWR.OnBeforeCallbacks.forEach(function(onBeforeCallback) {
		onBeforeCallback(path, model);
	});

	$.post('https://www.terminix.com/admin/dwr/jsonp/' + path, model ? {
		jsonData : JSON.stringify(model)
	} : {}).always(function(data) {
		var escapedText = JSON.parse(data.responseText);
		var callbackData;

		if (escapedText.error) {
			callbackData = {
				error : escapedText.error.message
			};
		} else {
			callbackData = {
				model : JSON.parse(escapedText.reply)
			};
		}

		callback(callbackData);

		CallDWR.OnAfterCallbacks.forEach(function(onAfterCallback) {
			onAfterCallback(path, model, callbackData);
		});
	});
}

CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];

function OnBeforeCallDWR(callback) {
	CallDWR.OnBeforeCallbacks.push(callback);
}

function OnAfterCallDWR(callback) {
	CallDWR.OnAfterCallbacks.push(callback);
}