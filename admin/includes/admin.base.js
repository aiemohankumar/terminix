function CallDWR(path, model, callback) {
    CallDWR.OnBeforeCallbacks.forEach(function (onBeforeCallback) {
        onBeforeCallback(path, model);
    });

    $.post('/admin/dwr/jsonp/' + path, model ? {
        jsonData: JSON.stringify(model)
    } : {}).always(function (data) {
        var escapedText = JSON.parse(data.responseText);
        var callbackData;

        if (escapedText.error) {
            callbackData = {
                error: escapedText.error.message
            };
        } else {
            callbackData = {
                model: JSON.parse(escapedText.reply)
            };
        }

        callback(callbackData);

        CallDWR.OnAfterCallbacks.forEach(function (onAfterCallback) {
            onAfterCallback(path, model, callbackData);
        });
    });
}

CallDWR.OnBeforeCallbacks = [];
CallDWR.OnAfterCallbacks = [];

function OnBeforeCallDWR(callback) {
    CallDWR.OnBeforeCallbacks.push(callback);
}

function OnAfterCallDWR(callback) {
    CallDWR.OnAfterCallbacks.push(callback);
}
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function refreshPage(data) {
    location.reload();
}

function applyFilters() {
    console.log($('#orderState').val());
    if ($('#orderNumber').val()) {
        $('#fromDate').val("");
        $('#toDate').val("");
        $('#orderState').val("BOTH");
        $('#applicationState').val("ALL");
        $('#salesAgent').val("ALL");
    } else {
        var date1 = new Date($('#fromDate').val());
        var date2 = new Date($('#toDate').val());
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 6) {
            alert('Max Date Filter range can be 6 Days apart.')
            return false;
        }
    }
    CallDWR("ListOrderDroplet/setFilterParameters", {
        fromDate: $('#fromDate').val(),
        toDate: $('#toDate').val(),
        orderState: $('#orderState').val(),
        applicationState: $('#applicationState').val(),
        salesAgent: $('#salesAgent').val(),
        orderNumber: $('#orderNumber').val()
    }, refreshPage);
    return false;
}

function updateOrder() {
    var item = {};
    var jsonObj = [];
    var salesCallid = $('#leadId').val();
    var partyid = $('#partyId').val();
    var customerid = $('#customerNo').val();
    var orderid = $('#orderID').val();
    var applicationState = $('#orderApplicationState').val();
    item["orderNum"] = orderid;
    item["leadId"] = salesCallid;
    item["partyId"] = partyid;
    item["customerNo"] = customerid;
    item["applicationState"] = applicationState;
    var i = 0;
    $(".commerceDiv input").each(function () {

        var j = i;
        var productType = $('#productType' + j).val();
        var proposalid = $('#proposalId' + j).val();
        var agreementid = $('#agreementNo' + j).val();
        var commerceId = $('#commerceItemNo' + j).val();
        var item1;
        if (commerceId != "" && commerceId != null) {
            item1 = {};
            item1["commerceItemId"] = commerceId;
        }
        if (productType != "" && productType != null) {
            item1["productType"] = productType;
        }
        if (proposalid != "" && proposalid != null) {
            item1["proposalId"] = proposalid;
        }
        if (agreementid != "" && agreementid != null) {
            item1["agreementNo"] = agreementid;
        }
        if (item1 != null && item1 != "") {
            jsonObj.push(item1);
        }
        i = i + 1;


    });
    item["commerceItems"] = jsonObj;
    console.log(item);

    CallDWR("OrderDetailsUpdateUIUtils/updateOrder", {
        orderInfo: item
    }, refreshPage);
    return false;
}
var app = angular.module('app', []);

app.controller('OrderDetailsController', ['$scope', function ($scope) {
    $scope.orderDetails = "";
    $scope.json = ""
    $scope.getOrderDetails = function (oNum) {
        CallDWR("OrderDetailDroplet/getOrderDetails", {
            orderNumber: oNum
        }, function (data) {
            var str = JSON.stringify(data, null, 2);
            console.log(str);
            $scope.json = str;
            $('#orderID').val('');
            $('#leadId').val('');
            $('#partyId').val('');
            $('#leadId').prop("disabled", false);
            $('#partyId').prop("disabled", false);
            $('#orderApplicationState').prop("disabled", false);
            $(".commerceDiv").remove();
            $("#pestCustNo").html("");
            if (typeof data.model.orderId != undefined && null != data.model.orderId && "" != data.model.orderId) {
                $('#orderID').val(data.model.orderId);
            }
            if (typeof data.model.salesCallId != undefined && null != data.model.salesCallId && "" != data.model.salesCallId) {
                $('#leadId').val(data.model.salesCallId);
                $('#leadId').prop("disabled", true);
            }
            if (typeof data.model.partyId != undefined && null != data.model.partyId && "" != data.model.partyId) {
                $('#partyId').val(data.model.partyId);
                $('#partyId').prop("disabled", true);
            }
            if (typeof data.model.applicationState != undefined && null != data.model.applicationState && "" != data.model.applicationState) {
                var mySelection;
                var appState = data.model.applicationState;
                if (appState == "MissionFailed" || appState == "MissionPending" || appState == "Complete") {
                    if (appState == "MissionFailed") {
                        mySelection = "MissionFailed";
                    }
                    if (appState == "MissionPending") {
                        mySelection = "MissionPending";
                    }
                    if (appState == "Complete") {
                        mySelection = "Complete";
                        $('#orderApplicationState').prop("disabled", true);
                    }
                    $('#orderApplicationState').val(mySelection).attr("selected", "selected");
                }
            }
            console.log(data.model.cartItems);
            var pestProd = true;
            var customerNoLabel = "CustomerNumber ";
            var proposalLabel = " Proposal ID ";
            var saleAgreementLabel = " SaleAgreementNumber ";
            $.each(data.model.cartItems, function (i, obj) {
                var j = i;
                $('#productType' + j).val('');
                $('#commerceItemNo' + j).val('');
                $('#proposalId' + j).val('');
                $('#agreementNo' + j).val('');
                $('#proposalId' + j).prop("disabled", false);
                $('#agreementNo' + j).prop("disabled", false);
                var commerceDiv = $(document.createElement('div')).attr("class", 'commerceDiv');
                if (typeof obj.itemProductCode != undefined && null != obj.itemProductCode && "" != obj.itemProductCode) {
                    var textfield = $('<input/>').attr({
                        type: "hidden",
                        class: "commerceElement form-control",
                        value: obj.itemProductCode,
                        id: "productType" + j
                    });
                    commerceDiv.append(textfield);
                    if (obj.itemProductCode == "TC") {
                        pestProd = false;
                    } else {
                        var commItemField = $('<input/>').attr({
                            type: "hidden",
                            class: "commerceElement form-control",
                            value: obj.itemId,
                            id: "commerceItemNo" + j
                        });
                        commerceDiv.append(commItemField);
                        var prodField = $('<input/>').attr({
                            type: "text",
                            class: "form-control",
                            value: "Prod: " + obj.itemProductId,
                            disabled: true,
                            id: "productNo" + j
                        });
                        commerceDiv.append(prodField);
                        if (typeof obj.proposalId != undefined && null != obj.proposalId && "" != obj.proposalId) {
                            var textfield = '<input type="text" class="commerceElement form-control" id="proposalId' + j + '" disabled=true value="' + obj.proposalId + '" >';
                            proposalLabel = proposalLabel + textfield;
                            commerceDiv.append(proposalLabel);
                        } else if (typeof obj.proposalId == undefined || null == obj.proposalId || "" == obj.proposalId) {
                            var textfield = '<input type="text" class="commerceElement form-control" id="proposalId' + j + '" value="" >';
                            proposalLabel = proposalLabel + textfield;
                            commerceDiv.append(proposalLabel);
                        }
                        if (typeof obj.salesAgreementNumber != undefined && null != obj.salesAgreementNumber && "" != obj.salesAgreementNumber) {
                            var textfield = '<input type="text" class="commerceElement form-control" id="agreementNo' + j + '" disabled=true value="' + obj.salesAgreementNumber + '" >';
                            saleAgreementLabel = saleAgreementLabel + textfield;
                            commerceDiv.append(saleAgreementLabel);
                        } else if (typeof obj.salesAgreementNumber == undefined || null == obj.salesAgreementNumber || "" == obj.salesAgreementNumber) {
                            var textfield = '<input type="text" class="commerceElement form-control" id="agreementNo' + j + '" value="" >';
                            saleAgreementLabel = saleAgreementLabel + textfield;
                            commerceDiv.append(saleAgreementLabel);
                        }
                    }
                }
                $("#commerceList").append(commerceDiv);
            });
            if (pestProd) {
                if (typeof data.model.customerNumber != undefined && null != data.model.customerNumber && "" != data.model.customerNumber) {
                    var textfield = '<input type="text" class="form-control" id="customerNo" disabled=true value="' + data.model.customerNumber + '" >';
                    customerNoLabel = customerNoLabel + textfield;
                    $("#pestCustNo").append(customerNoLabel);
                } else if (typeof data.model.customerNumber == undefined || null == data.model.customerNumber || "" == data.model.customerNumber) {
                    var textfield = '<input type="text" class="form-control" id="customerNo" value="" >';
                    customerNoLabel = customerNoLabel + textfield;
                    $("#pestCustNo").append(customerNoLabel);
                }
            }
            $scope.orderDetails = data;
            $scope.$apply();
            $('#orderItem').modal('show');
        });
    };
}]);

$(document).ready(function () {
    $("#fromDate, #toDate").datepicker({
        nextText: "",
        prevText: "",
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
    });
});
