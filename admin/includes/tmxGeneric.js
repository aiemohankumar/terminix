// Define the `tmxGeneric` module
var tmxGeneric = angular.module('tmxGeneric', []);

// Define the `TmxGenericController` controller on the `tmxGeneric` module
tmxGeneric.controller('TmxGenericController', TmxGenericController);

function TmxGenericController($scope, $http, $location) {
	$scope.tmxPriceDetail = "";
	$scope.getTmxPriceDetails = function() {
		CallDWR("TmxGenericProcessor/startProcess", {
			'PROCESSORARRAY' : [ {
				'PROCESSOR' : 'TmxPriceProcessor',
				'PROCESSORINPUT' : {
					'houseNumber' : '889',
					'streetName' : 'RidgeLakeBlvd',
					'city' : 'Memphis',
					'zipCode' : '38125-1002',
					'state' : 'TN',
					'squareFootage' : '4999',
					'lotSize' : '0.5',
					'targetPest' : 'GENPEST',
					'templateCode' : [ 'PC112', 'PC195' ]
				}
			} ]
		}, function(data) {
			$scope.tmxPriceDetail = data;
			$('#tmxPrice').html(renderjson.set_icons('+', '-').set_show_to_level(10)(data));
		});
	}

}